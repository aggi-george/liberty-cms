require('./global');

var express = require('express');
var http = require('http');
var async = require('async');
var common = require('../lib/common');
var slack = require('../src/notifications/slack');
var config = require('../config');
var db = require('../lib/db_handler');
var healthManager = require('../src/core/manager/system/healthManager');
var repl = require('net-repl');
var elitecoreConnector = require('../lib/manager/ec/elitecoreConnector');

var webhook = require('../src/api/internal/webhook');
var notifications = require('../src/api/internal/notifications');
var contentApi = require('../src/api/internal/contentApi');
var promotions = require('../src/api/internal/promotionsApi');
var packages = require('../src/api/internal/packages');
var boostApi = require('../src/api/internal/boostApi');
var bonusApi = require('../src/api/internal/bonusApi');
var logsApi = require('../src/api/internal/logsApi');
var scriptsApi = require('../src/api/internal/scriptsApi');
var referralApi = require('../src/api/internal/referralApi');
var simChangeApi = require('../src/api/internal/simChangeApi');
var profileApi = require('../src/api/internal/profileApi');
var configApi = require('../src/api/internal/configApi');
var partnerApi = require('../src/api/internal/partnerApi');
var portInApi = require('../src/api/internal/portInApi');
var accountApi = require('../src/api/internal/accountApi');
var analyticsApi = require('../src/api/internal/analyticsApi');
var healthApi = require('../src/api/internal/healthApi');
var schedulerApi = require('../src/api/internal/schedulerApi');
var winbackApi = require('../src/api/internal/winbackApi');
var validationApi = require('../src/api/internal/validationApi');
var suspensionApi = require('../src/api/internal/suspensionApi');

var blockedTimer = healthManager.checkBlocking("API", 500);

var web = express();

exports.web = web;

var stats = new Array;

setInterval(function () {
    var proto = new Object;
    var ignore = [ new RegExp("/api/1/web/customers/get/cdr") ];
    var count = stats.length;
    if (!count) return;
    for (var i=0; i<count; i++) {
        if (!proto[stats[i].protocol]) proto[stats[i].protocol] = { "total" : 0, "max" : 0, maxPath : "" };
        if ( !ignore.some(function (rx) { return rx.test(stats[i].path) }) ) {
            proto[stats[i].protocol].total += stats[i].time;
            if ( proto[stats[i].protocol].max < stats[i].time ) {
                proto[stats[i].protocol].max = stats[i].time;
                proto[stats[i].protocol].maxPath = stats[i].path;
            }
            proto[stats[i].protocol].avg = proto[stats[i].protocol].total / count;
        } else {
            count--;
        }
        stats[i] = undefined;
    }
    stats = stats.filter(function(o) { return o; });
    Object.keys(proto).forEach(function (key) {
        db.apistats.insert({    "ts" : new Date().getTime(),
                "server" : config.MYFQDN + "_" + key + "_" + process.pid,
                "count" : count, "avg" : proto[key].avg,
                "max" : proto[key].max, "maxPath" : proto[key].maxPath });
    });
}, 60 * 1000);

function gantry(req, res, next) {
    var start = Date.now();
    res.on('finish', function() {
        var time = Date.now() - start;
        stats.push({ "protocol" : req.protocol, "path" : req.path, "time" : time });
    });
    next();
}
web.get('/pm2raw', (req, res) => { 
    healthManager.extractImportantParamsFromPm2((err, result) => {
        if(err){
            res.json({code: -1, error: err});
        }else{
            res.json({code: 0, data: result});
        }
    });
});
web.use(gantry);

packages.init('/api/1', '/packages', web);
webhook.init('/api/1', '/webhook', web);
notifications.init('/api/1', '/notifications', web);
contentApi.init('/api/1', '/contents', web);
promotions.init('/api/1', '/promotions', web);
boostApi.init('/api/1', '/boost', web);
bonusApi.init('/api/1', '/bonus', web);
logsApi.init('/api/1', '/auditlogs', web);
scriptsApi.init('/api/1', '/management', web);
referralApi.init('/api/1', '/referral', web);
simChangeApi.init('/api/1', '/simChange', web);
profileApi.init('/api/1', '/profile', web);
configApi.init('/api/1', '/config', web);
partnerApi.init('/api/1', '/partner', web);
portInApi.init('/api/1', '/portin', web);
accountApi.init('/api/1', '/account', web);
analyticsApi.init('/api/1', '/analytics', web);
healthApi.init('/api/1', '/health', web);
schedulerApi.init('/api/1', '/scheduler', web);
winbackApi.init('/api/1', '/winback', web);
validationApi.init('/api/1', '/validation', web);
suspensionApi.init('/api/1', '/suspension', web);

web.all('*', function (req, res) { res.status(404).send('Not Found'); });

http.globalAgent.maxSockets = 2000;
var server = http.createServer(web).listen(config.WEBPORT,"0.0.0.0");

process.on('SIGINT', function() {
    var tasks = new Array;
    tasks.push(function (callback) {
        clearInterval(blockedTimer);
        callback(undefined, { "err" : undefined, "result" : undefined });
    });
    tasks.push(function (callback) {
        server.close(function (err, result) {
            callback(undefined, { "err" : err, "result" : result });
        });
    });
    new Promise(function (resolve, reject) {
        var timeout = setTimeout(function () {
            common.error("api" , "Shutdown timeout");
            reject(new Error("Timeout"));
        }, 5000);
        async.parallel(tasks, function (err, result) {
            clearTimeout(timeout);
            if (err) reject(err);
            else resolve(result);
        });
    }).then(function(result) {
        common.log("api", "Graceful Shutdown 'Clean'");
        process.exit(0);
    }).catch(function(err) {
        common.error("api", "Graceful Shutdown '" + err + "'");
        process.exit(1);
    });
});
process.on('uncaughtException', function(err) {
    if (err) {
        common.error("Uncaught Exception", err.stack);
        slack.send({
            "username" : "Crash Bot (API) " + config.MYFQDN,
            "text" : err.stack
        });
    }
});
process.addListener('SIGPIPE', function (err) {
    if (err) {
        common.error("SIGPIPE", err.stack);
        slack.send({
            "username" : "Crash Bot (API) " + config.MYFQDN,
            "text" : err.stack
        });
    }
});
process.setMaxListeners(1500);

slack.send({
    "username" : "Process Bot " + config.MYFQDN,
    "text" : process.cwd() + " (API) " + process.env.NODE_ENV + " start [" + process.pid + "]"
});
common.log(process.env.ROLE, process.env.NODE_ENV);

var options = {
	prompt: 'api@' + config.MYFQDN + '>> ',
	deleteSocketOnStart: false
}

if(config.REPL_API){
    var srv = repl.createServer(options).listen('/tmp/api.sock');
}
