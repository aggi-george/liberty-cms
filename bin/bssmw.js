require('./global');

var express = require('express');
var http = require('http');
var https = require('https');
var fs = require('fs');
var async = require('async');
var common = require('../lib/common');
var slack = require('../src/notifications/slack');
var config = require('../config');
var elitecoreConnector = require('../lib/manager/ec/elitecoreConnector');
var healthManager = require('../src/core/manager/system/healthManager');

var privateKey = fs.readFileSync(__dirname + '/../lib/https.key', 'utf8');
var certificate = fs.readFileSync(__dirname + '/../lib/https.crt', 'utf8');

var blockedTimer = healthManager.checkBlocking("BSSMW", 500);

var web = express();
var webs = express();

var customerApi = require('../src/api/bss/customerApi');
var basePath = '/api/1/bss';
customerApi.init(basePath, web);

web.all('*', function (req, res) {
    res.status(404).send('Not Found');
});
webs.all('*', function (req, res) {
    res.status(404).send('Not Found');
});

http.globalAgent.maxSockets = 2000;
https.globalAgent.maxSockets = 2000;
var server = http.createServer(web).listen(config.WEBPORT_BSSMW, "0.0.0.0");
var servers = https.createServer({key: privateKey, cert: certificate}, webs).listen(config.WEBSPORT_BSSMW, "0.0.0.0");

process.on('SIGINT', function () {
    var tasks = new Array;
    tasks.push(function (callback) {
        clearInterval(blockedTimer);
        callback(undefined, { "err" : undefined, "result" : undefined });
    });
    tasks.push(function (callback) {
        server.close(function (err, result) {
            callback(undefined, {"err": err, "result": result});
        });
    });
    tasks.push(function (callback) {
        servers.close(function (err, result) {
            callback(undefined, {"err": err, "result": result});
        });
    });
    new Promise(function (resolve, reject) {
        var timeout = setTimeout(function () {
            common.error("bssws", "Shutdown timeout");
            reject(new Error("Timeout"));
        }, 5000);
        async.parallel(tasks, function (err, result) {
            clearTimeout(timeout);
            if (err) {
                reject(err);
            } else {
                resolve(result);
            }
        });
    }).then(function (result) {
            common.log("bssws", "Graceful Shutdown 'Clean'");
            process.exit(0);
        }).catch(function (err) {
            common.error("bssws", "Graceful Shutdown '" + err + "'");
            process.exit(1);
        });
});

process.on('uncaughtException', function (err) {
    if (err) common.log("Uncaught Exception", err.message, err.stack);
});
process.addListener('SIGPIPE', function (err) {
    if (err) common.log("SIGPIPE", err.message, err.stack);
});
process.setMaxListeners(1500);

slack.send({
    "username" : "Process Bot " + config.MYFQDN,
    "text" : process.cwd() + " (BSSMW) " + process.env.NODE_ENV + " start [" + process.pid + "]"
});

common.log(process.env.ROLE, process.env.NODE_ENV);
