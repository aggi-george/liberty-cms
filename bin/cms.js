require('./global');

var express = require('express');
const hsts = require('hsts');
const frameguard = require('frameguard');
var cookieParser = require('cookie-parser');
var https = require('https');
var fs = require('fs');
var async = require('async');
var common = require('../lib/common');
var slack = require('../src/notifications/slack');
var config = require('../config');
var db = require('../lib/db_handler');
var elitecoreConnector = require('../lib/manager/ec/elitecoreConnector');

var webhook = require('../src/api/internal/webhook');	// used by elitecore
var notifications = require('../src/api/internal/notifications');
var contentApi = require('../src/api/internal/contentApi');
var packages = require('../src/api/internal/packages');
var webAPI = require('../src/api/web/web');
var scriptsApi = require('../src/api/internal/scriptsApi');
var healthManager = require('../src/core/manager/system/healthManager');
var repl = require('net-repl');

var blockedTimer = healthManager.checkBlocking("CMS", 500);

var webs = express();

//To enfore Strict-Transport-Security.
webs.use(hsts({
    maxAge: 15552000, //recommended expiry
    includeSubDomains: true //enforced on all sub domains as well.
}));

// This restricts who can put this site in a frame.
webs.use(frameguard({ action: 'sameorigin' }));

webs.use(cookieParser());

var privateKey	= fs.readFileSync(__dirname + '/../lib/https.key', 'utf8');   // relative to where the bin is executed
var certificate = fs.readFileSync(__dirname + '/../lib/https.crt', 'utf8');   // relative to where the bin is executed
var credentials = { key: privateKey, cert: certificate };

exports.webs = webs;

webs.use('/', express.static(__dirname + '/../gui/static/templates'));
webs.use('/demo', express.static(__dirname + '/../gui/demo/html'));
webs.use('/documentation', express.static(__dirname + '/../gui/demo/documentation'));
webs.use('/css', express.static(__dirname + '/../gui/static/css'));
webs.use('/res', express.static(__dirname + '/../gui/static/res'));
webs.use('/lib', express.static(__dirname + '/../gui/static/lib'));
webs.use('/src', express.static(__dirname + '/../gui/static/src'));
webs.use('/assets', express.static(__dirname + '/../gui/static/assets'));

var stats = new Array;


setInterval(function () {
	var proto = new Object;
	var ignore = [ new RegExp("/api/1/web/customers/get/cdr") ];
	var count = stats.length;
	if (!count) return;
	for (var i=0; i<count; i++) {
		if (!proto[stats[i].protocol]) proto[stats[i].protocol] = { "total" : 0, "max" : 0, maxPath : "" };
		if ( !ignore.some(function (rx) { return rx.test(stats[i].path) }) ) {
			proto[stats[i].protocol].total += stats[i].time;
			if ( proto[stats[i].protocol].max < stats[i].time ) {
				proto[stats[i].protocol].max = stats[i].time;
				proto[stats[i].protocol].maxPath = stats[i].path;
			}
			proto[stats[i].protocol].avg = proto[stats[i].protocol].total / count;
		} else {
			count--;
		}
		stats[i] = undefined;
	}
	stats = stats.filter(function(o) { return o; });
	Object.keys(proto).forEach(function (key) {
		db.apistats.insert({	"ts" : new Date().getTime(),
				"server" : config.MYFQDN + "_" + key + "_" + process.pid,
				"count" : count, "avg" : proto[key].avg,
				"max" : proto[key].max, "maxPath" : proto[key].maxPath });
	});
}, 60 * 1000);

function gantry(req, res, next) {
	var start = Date.now();
	res.on('finish', function() {
		var time = Date.now() - start;
		stats.push({ "protocol" : req.protocol, "path" : req.path, "time" : time });
	});
	next();
}

webs.use(gantry);

webs.get('/test', function (req, res) {
	healthManager.executeBasicHealth(function(result){
		res.json(result);
	});
});

webs.get('/ping', function(req, res){
	async.parallel([function(callback){
		callback(null, {});
	}], function(err, result){
		res.json({"ok": 1});
	});
});

webs.get('/logout', logout);

//////// LEGACY START
webs.use(function(req, res, next) {
    if ( (req.url.indexOf("/api/1/packages") > -1) ||
            (req.url.indexOf("/api/1/webhook") > -1) ||
            (req.url.indexOf("/api/1/notifications") > -1) ||
            (req.url.indexOf("/api/1/contents") > -1) ||
            (req.url.indexOf("/api/1/management") > -1) ) {
        common.error("[DEPRECATED] url=" + req.url, "ip=" + req.ip);
    }
    next();
});
packages.init('/api/1', '/packages', webs);
webhook.init('/api/1', '/webhook', webs);
notifications.init('/api/1', '/notifications', webs);
contentApi.init('/api/1', '/contents', webs);
scriptsApi.init('/api/1', '/management', webs);
//////// LEGACY END

webs.get('/*', secure);
webs.put('/*', secure);
webs.post('/*', secure);
webs.delete('/*', secure);

function secure(req, res, next) { 
	if ( req.url == '/login.html' ) {
		return next();
	} else if ( req.url == '/api/1/web/auth/login' ) {
		return next();
	} else if ( req.url == '/' ) {
		res.redirect('/login.html');
	} else {
		if ( req.cookies.cmscookie && req.cookies.cmsuser ) {
			webAPI.checkSession(req.cookies.cmscookie, req.cookies.cmsuser, function(result) {
				if (result) {
					req.gid = result;
					req.user = req.cookies.cmsuser;
					return next();
				} else {
//					res.redirect('/login.html');
					res.status(403).send("Please <A href='login.html'>login</A> again");
					return false;
				}
			});
		} else {
//			res.redirect('/login.html');
			res.status(403).send("Please <A href='login.html'>login</A> again");
			return false;
		}
	}
}

//empty all the cookies
function logout(req, res) {
	cookie = req.cookies;
    for (var prop in cookie) {
        if (!cookie.hasOwnProperty(prop)) {
            continue;
        }    
        res.cookie(prop, '', {
        	expires: new Date(0),
			secure:true,
			httpOnly:true
		});
    }
    res.redirect('/login.html');
 }

webAPI.init('/api/1', '/web', webs);
webs.use('/', express.static(__dirname + '/../gui/static'));

webs.all('*', function (req, res) { res.status(404).send('Not Found'); });

https.globalAgent.maxSockets = 2000;
var servers = https.createServer(credentials, webs).listen(config.WEBSPORT,"0.0.0.0");

process.on('SIGINT', function() {
	var tasks = new Array;
	tasks.push(function (callback) {
		clearInterval(blockedTimer);
                callback(undefined, { "err" : undefined, "result" : undefined });
	});
	tasks.push(function (callback) {
		servers.close(function (err, result) {
			callback(undefined, { "err" : err, "result" : result });
		});
	});
	new Promise(function (resolve, reject) {
		var timeout = setTimeout(function () {
			common.error("cms" , "Shutdown timeout");
			reject(new Error("Timeout"));
		}, 5000);
		async.parallel(tasks, function (err, result) {
			clearTimeout(timeout);
			if (err) reject(err);
			else resolve(result);
		});
	}).then(function(result) {
		common.log("cms", "Graceful Shutdown 'Clean'");
		process.exit(0);
	}).catch(function(err) {
		common.error("cms", "Graceful Shutdown '" + err + "'");
		process.exit(1);
	});
});
process.on('uncaughtException', function(err) {
    if (err) {
        common.error("Uncaught Exception", err.stack);
        slack.send({
            "username" : "Crash Bot (CMS) " + config.MYFQDN,
            "text" : err.stack
        });
    }
});
process.addListener('SIGPIPE', function (err) {
    if (err) {
        common.error("SIGPIPE", err.stack);
        slack.send({
            "username" : "Crash Bot (CMS) " + config.MYFQDN,
            "text" : err.stack
        });
    }
});
process.setMaxListeners(1500);

slack.send({
    "username" : "Process Bot " + config.MYFQDN,
    "text" : process.cwd() + " (CMS) " + process.env.NODE_ENV + " start [" + process.pid + "]"
});
common.log(process.env.ROLE, process.env.NODE_ENV);

var options = {
	prompt: 'cms@' + config.MYFQDN + '>> ',
	deleteSocketOnStart: false
}

if(config.REPL_CMS){
	var srv = repl.createServer(options).listen('/tmp/cms.sock');
}

