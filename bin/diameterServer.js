require('./global');

var diameter = require('diameter');
var davp = require('diameter-avp-object');
var common = require('../lib/common');
var slack = require('../src/notifications/slack');
var config = require('../config');
var db = require('../lib/db_handler');
var bypassManager = require(__core + '/manager/diameter/diameterManager');
var templates = require(__core + '/manager/diameter/serverTemplates');
var healthManager = require('../src/core/manager/system/healthManager');
var template = require(__core + '/manager/diameter/serverTemplates');

var LOG = config.LOGSENABLED;
var VERBOSE = config.LOGSVERBOSE;

var blockedTimer = healthManager.checkBlocking("DIAMETERSERVER", 500);

var options = new Object;
if (VERBOSE) options.beforeAnyMessage = diameter.logMessage;
if (VERBOSE) options.afterAnyMessage = diameter.logMessage;

function unixTime(){
    var epochMs = new Date().getTime()
    return epochMs
}

function socketHandler(type, socket) {
    socket.on('diameterMessage', function(event) {
        var timeReceived = unixTime()
        var msg = davp.toObject(event.message.body);

        bypassManager.triage(
            type, event.message.command, msg, function (err, result) {
                if (!err && result) {
                    event.response.body = event.response.body.concat(result);
                } else {
                    event.response.body = event.response.body.concat(genericErr);
                }
                event.callback(event.response);
                let sessionId = (typeof msg.sessionId != 'undefined') ? msg.sessionId : undefined;
                let latency = unixTime() - timeReceived;
                let session = sessionId ? '\n' + type + ' Session-Id: '+ sessionId : '';
                common.log(`${type} ${event.message.command}`, `${latency}ms ${session}`);
            });
    });
    socket.on('end', function() {
        common.log(type+' Bypass Client Disconnected',
            socket.diameterConnection.sessionId);
    });
    socket.on('error', function(err) {
        common.error(type+' Bypass Socket Error', err.message);
    });
}

function reset(peer) {
    db.cache_put('diameter', `${peer}WatchDog`, 0, 60 * 1000);
    db.cache_put('diameter', `${peer}TPS`, 0, 5 * 1000);
}

var pcrf = diameter.createServer(options, socket=>{
    socketHandler("PCRF",socket)
    templates.peers.Gx.forEach(reset);
});
pcrf.listen(templates.bypassProfile(config.PCRFBYPASS).portGx, "0.0.0.0");

var ocs = diameter.createServer(options, socket=>{
    socketHandler("OCS", socket)
    templates.peers.Gy.forEach(reset);
});
ocs.listen(templates.bypassProfile(config.OCSBYPASS).portGy, "0.0.0.0");

process.on('SIGINT', function() {
    common.log("cms", "Graceful Shutdown 'Clean'");
    process.exit(0);
});

process.on('uncaughtException', function(err) {
    if (err) {
        common.error("Uncaught Exception", err.stack);
        slack.send({
            "username" : "Crash Bot (OCSBYPASS) " + config.MYFQDN,
            "text" : err.stack
        });
    }
});

process.addListener('SIGPIPE', function (err) {
    if (err) {
        common.error("SIGPIPE", err.stack);
        slack.send({
            "username" : "Crash Bot (OCSBYPASS) " + config.MYFQDN,
            "text" : err.stack
        });
    }
});

process.setMaxListeners(1500);

slack.send({
    "username" : "Process Bot " + config.MYFQDN,
    "text" : process.cwd() + " (OCSBYPASS) " + process.env.NODE_ENV + " start [" + process.pid + "]"
});

common.log(process.env.ROLE, process.env.NODE_ENV);
