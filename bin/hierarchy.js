require('./global');

var async = require('async');
var ec = require('../lib/elitecore');
var common = require('../lib/common');
var slack = require('../src/notifications/slack');
var db = require('../lib/db_handler.js');
var config = require('../config');
var packagesHelper = require('../lib/packages_helper');
var elitecoreConnector = require('../lib/manager/ec/elitecoreConnector');

var ERROR = new Array;

common.log(process.env.ROLE, process.env.NODE_ENV);

var buildKey;
if (config.DEV) buildKey = (config.EC_STAGING) ? "dev_hierarchy_build_staging" : "dev_hierarchy_build";
else buildKey = (config.EC_STAGING) ? "hierarchy_build_staging" : "hierarchy_build";
db.cache_lock("cache", buildKey, 1, 5 * 60 * 1000, function (lock) {
    //if (!lock) return common.log("Hierarchy", "Job already taken");
    // wait 1 second to fetch WSDL
    setTimeout(function () {
        syncElitecoreHierarchy();
    }, 1000);
});

// fetch from elitecore every day
//setInterval(function() { syncElitecoreHierarchy(); }, 24 * 60 * 60 * 1000);

function processSpecific(product) {
    if (!product.productOfferPricingDetail) return undefined;
    var detail = product.productOfferPricingDetail;
    var specs = product.productSpecification;
    var allowance_details = ( detail && detail.allowancePolicie &&
    detail.allowancePolicie[0] &&
    detail.allowancePolicie[0].allowanceDetails ) ?
        detail.allowancePolicie[0].allowanceDetails[0] :
        undefined;
    var sms_local = 0;
    var sms_global = 0;
    var voice_local = 0;
    if (detail && detail.usageChargeDetail) detail.usageChargeDetail.forEach(function (charges) {
        if (charges && charges.usageChargeGroup && charges.usageChargeGroup[0].usageChargeGroupDetail) {
            charges.usageChargeGroup[0].usageChargeGroupDetail.forEach(function (charge) {
                if (charge && charge.usageCharge && charge.usageCharge.usageChargeResult) {
                    charge.usageCharge.usageChargeResult.forEach(function (result) {
                        if ((result.resultAttributeValues[0] == "VoiceIncoming") &&
                            (charge.usageCharge.usageChargeId == "RTC0087")) {
                            voice_local = parseFloat(result.resultAttributeValues[1]) / 100;
                        } else if (result.resultAttributeValues[0] == "SMSLocal") {
                            sms_local = parseFloat(result.resultAttributeValues[1]) / 100;
                        } else if (result.resultAttributeValues[0] == "SMSIDD") {
                            sms_global = parseFloat(result.resultAttributeValues[1]) / 100;
                        }
                    });
                }
            });
        }
    });
    var value = ( allowance_details ) ? allowance_details.resultAttributeValues[2] : 0;
    var recurring;
    if (detail.recurringCharge) {
        for (var k = 0; k < detail.recurringCharge.length; k++) {
            if (detail && detail.recurringCharge && detail.recurringCharge[k] &&
                (detail.recurringCharge[k].productTypeId == "PRT06") &&
                detail.recurringCharge[k].chargeResult[0]) {
                recurring = detail.recurringCharge[k].chargeResult[0].resultAttributeValues[1];
                break;
            }
        }
    }
    var activation = {"total": 0, "list": []};
    var discount = new Array;
    var onetime;
    if (detail.oneTimeCharge) {
        detail.oneTimeCharge.forEach(function (oneItem) {
            if (oneItem.name == "NumberSelection_Charge") {
                oneItem.chargeResult.forEach(function (charge) {
                    if (charge && charge.resultAttributeValues && charge.conditionAttributeValues) {
                        activation.total += parseInt(charge.resultAttributeValues[0]);
                        activation.list.push({
                            "name": oneItem.name.replace(/ /g, "_") + "_" +
                            charge.conditionAttributeValues[0],
                            "price": parseInt(charge.resultAttributeValues[0])
                        });
                    }
                });
            } else {
                activation.total += parseInt(oneItem.chargeResult[0].resultAttributeValues[0]);
                activation.list.push({
                    "name": oneItem.name.replace(/ /g, "_"),
                    "price": parseInt(oneItem.chargeResult[0].resultAttributeValues[0]) / 100
                });
            }
        });
        onetime = activation.total;
    }
    if (detail.discount) {
        detail.discount.forEach(function (disc) {
            discount.push({
                "name": disc.discountedProductName.replace(/ /g, "_"),
                "price": parseInt(disc.discount)
            });
        });
    }
    var obj = new Object;
    obj.id = specs.productOfferId;
    obj.name = specs.productName;
    obj.value = value;
    obj.local_per_sms = (sms_local) ? sms_local : undefined;
    obj.local_per_min = (voice_local) ? voice_local : undefined;
    obj.global_per_sms = (sms_global) ? sms_global : undefined;
    if (onetime != undefined) obj.onetime = onetime;
    if (activation.list.length > 0) {
        obj.activation = activation;
    }
    if (discount && discount.length > 0) {
        obj.discount = discount;
    }
    obj.recurring = recurring;
    if (specs && specs.description) obj.description = specs.description;
    return obj;
}

function syncElitecoreHierarchy() {
    var tasks = new Array;
    var hierarchy = {"base": [], "boost": [], "bonus": [], "general": []};

    ec.getBusinessHierarchyDetail(function (err, result) {
        var retail;
        var postpaid;

        if (err || !result || !result.return) {
            common.error("HierarchyManager", "syncElitecoreHierarchy: FAILED: Abort... Restarting");
            process.exit(1);		// abort! we cannot continue without this
        }

        result.return.forEach(function (level1) {
            if (level1.hierarchyId == 'RETAIL_POSTPAID') {
                postpaid = level1.childHierarchyDetails;
            }
        });

        var base = hierarchy.base;
        var bonus = hierarchy.bonus;
        var boost = hierarchy.boost;
        var general = hierarchy.general;

        var specificTaskPush = function (id) {
            tasks.push(function (callback) {
                ec.getSpecificProdOfferDetails(id, function (err, result) {
                    if (err || !result || !result.return || Object.keys(result.return).length == 0) {
                        var errorMessage = (err ? err.message : "EMPTY RESPONSE");
                        common.error("HierarchyManager", "syncElitecoreHierarchy: product " + id
                        + " details load error, error=" + errorMessage);
                        callback(undefined, {"id": id, "specific": undefined, error: errorMessage});
                    } else {
                        common.log("HierarchyManager", "syncElitecoreHierarchy: product " + id + " details loaded");
                        callback(undefined, {"id": id, "specific": result.return});
                    }
                });
            });
        }

        var findBaseID = function (hbase, id) {
            var found = -1;
            hbase.every(function (rbase, idx) {
                if (rbase.base_id == id) {
                    found = idx;
                    return false;
                }
                return true;
            });
            return found;
        }

        postpaid.forEach(function (stage2) {
            if (stage2.childHierarchyDetails) {
                stage2.childHierarchyDetails.forEach(function (stage3) {
                    var hierarchyId = stage3.hierarchyId.replace("_COMPONENTS", "");
                    var base_id = hierarchyId;
                    if (stage2.hierarchyId == 'BASE_PLAN') {
                        if (stage3 && stage3.associatedProduct) {
                            stage3.associatedProduct.forEach(function (stage4) {
                                var idx = findBaseID(base, base_id);
                                var type = "options";
                                var obj;
                                if (idx > -1) {
                                    obj = base[idx];
                                    if (!obj[type]) obj[type] = new Array;
                                } else {
                                    obj = {"base_id": base_id};
                                    obj[type] = new Array;
                                }
                                obj[type].push({
                                    "id": stage4.productId,
                                    "name": stage4.productName,
                                    "base_id": base_id,
                                    "type": "base"
                                });
                                if (idx == -1) base.push(obj);
                                specificTaskPush(stage4.productId);
                            });
                        }
                    } else if (stage2.hierarchyId == 'BASE_PLAN_COMPONENTS') {
                        if (stage3 && stage3.childHierarchyDetails) {
                            stage3.childHierarchyDetails.forEach(function (stage4) {
                                if (stage4 && stage4.associatedProduct) {
                                    stage4.associatedProduct.forEach(function (stage5) {
                                        var idx = findBaseID(base, base_id);
                                        var type = stage4.name.toLowerCase();
                                        var obj;
                                        if (idx > -1) {
                                            obj = base[idx];
                                            if (!obj[type]) obj[type] = new Array;
                                        } else {
                                            obj = {"base_id": base_id};
                                            obj[type] = new Array;
                                        }
                                        obj[type].push({
                                            "id": stage5.productId,
                                            "name": stage5.productName,
                                            "base_id": base_id,
                                            "component": type,
                                            "type": "base"
                                        });
                                        if (idx == -1) base.push(obj);
                                        specificTaskPush(stage5.productId);
                                    });
                                }
                            });
                        }
                    }
                });
            } else {
                if (stage2 && stage2.associatedProduct) stage2.associatedProduct.forEach(function (stage3) {
                    if (stage2.hierarchyId == 'ADD-ON') {
                        general.push({
                            "id": stage3.productId,
                            "name": stage3.productName,
                            "type": "general"
                        });
                        specificTaskPush(stage3.productId);
                    } else if (stage2.hierarchyId == 'BOOST') {
                        boost.push({
                            "id": stage3.productId,
                            "name": stage3.productName,
                            "type": "boost"
                        });
                        specificTaskPush(stage3.productId);
                    } else if (stage2.hierarchyId == 'BONUS') {
                        bonus.push({
                            "id": stage3.productId,
                            "name": stage3.productName,
                            "type": "bonus"
                        });
                        specificTaskPush(stage3.productId);
                    }
                });
            }
        });
        async.parallelLimit(tasks, 2, function (errGroup, resultGroup) {
            if (!errGroup && resultGroup) {
                resultGroup.forEach(function (result) {			// result.return is an array
                    var obj = (result && result.specific && result.specific[0]) ?
                        processSpecific(result.specific[0]) : undefined;

                    if (result.id == "PRD00442") {
                        common.error("HierarchyManager", "syncElitecoreHierarchy: -------------------------------");
                        common.error("HierarchyManager", "syncElitecoreHierarchy: FAKE PRODUCT USED FOR " + result.id);
                        common.error("HierarchyManager", "syncElitecoreHierarchy: -------------------------------");

                        var product = ec.findPackageOriginal(hierarchy, result.id);
                        product.id = "PRD00442";
                        product.name = "CirclesRegistration";
                        product.base_id = "CIRCLES_REGISTRATION";
                        product.type = "base";
                        product.mins = 300;
                        product.sms = 100;
                        product.onetime_Delivery_Charge = 0;
                        product.onetime_Sim_Charge = 0;
                        product.onetime_NumberSelection_Charge_G = 388;
                        product.onetime_NumberSelection_Charge_S = 53;
                        product.onetime_NumberSelection_Charge_P = 1000;
                        product.onetime_NumberSelection_Charge_N = 0;
                        product.onetime_NumberSelection_Charge_H = 214;
                        product.onetime_Registration_Fees = 0;
                        product.discount_Registration_Fee = 0;
                        product.discount_Sim_Charge = 0;
                        product.discount_Delivery_Charge = 0;
                        product.kb = 3145728;
                        product.price = 0;
                        product.recurrent = true;
                        product.discount_price = 0;
                    } else if (result.id == "PRD00401") {

                        common.error("HierarchyManager", "syncElitecoreHierarchy: -------------------------------");
                        common.error("HierarchyManager", "syncElitecoreHierarchy: FAKE PRODUCT USED FOR " + result.id);
                        common.error("HierarchyManager", "syncElitecoreHierarchy: -------------------------------");

                        var product = ec.findPackageOriginal(hierarchy, result.id);
                        product.id = "PRD00401";
                        product.name = "CirclesOne";
                        product.base_id = "CIRCLESLAUNCH";
                        product.type = "base";
                        product.mins = 100;
                        product.sms = 0;
                        product.local_per_sms = 0.05;
                        product.local_per_min = 0.08;
                        product.global_per_sms = 0.15;
                        product.onetime_Delivery_Charge = 0;
                        product.onetime_Sim_Charge = 0;
                        product.onetime_NumberSelection_Charge_G = 388;
                        product.onetime_NumberSelection_Charge_S = 53;
                        product.onetime_NumberSelection_Charge_P = 1000;
                        product.onetime_NumberSelection_Charge_N = 0;
                        product.onetime_NumberSelection_Charge_H = 214;
                        product.onetime_Registration_Fees = 38;
                        product.discount_Registration_Fee = 0;
                        product.discount_Sim_Charge = 0;
                        product.discount_Delivery_Charge = 0;
                        product.kb = 3145728;
                        product.price = 28;
                        product.recurrent = true;
                        product.discount_price = 0;
                    } else if (!obj) {
                        ERROR.push({"invalid": result.id});
                        common.error("syncElitecoreHierarchy", result.id + " is empty");
                        return false;
                    } else {
                        var product = ec.findPackageOriginal(hierarchy, obj.id);
                        if (!product.component && (product.type == "base")) {
                            if (obj.description) {
                                var voice = obj.description.split('#')[1];
                                var msg = obj.description.split('#')[2];
                                product.mins = ( voice && (voice.split('_')[0] == "VOICE") ) ? parseInt(voice.split('_')[1]) : 0;
                                product.sms = ( msg && (msg.split('_')[0] == "SMS") ) ? parseInt(msg.split('_')[1]) : 0;
                            }
                            if (obj.local_per_sms != undefined) {
                                product.local_per_sms = obj.local_per_sms;
                            }
                            if (obj.local_per_min != undefined) {
                                product.local_per_min = obj.local_per_min;
                            }
                            if (obj.global_per_sms != undefined) {
                                product.global_per_sms = obj.global_per_sms;
                            }
                            if (obj.activation != undefined) {
                                obj.activation.list.forEach(function (act) {
                                    product["onetime_" + act.name] = act.price;
                                });
                            }
                            if (obj.discount != undefined) {
                                obj.discount.forEach(function (disc) {
                                    product["discount_" + disc.name] = disc.price;
                                });
                            }
                        }
                        if ((product.component == "voice") && obj.description) {
                            var voice = obj.description.split('#')[1];
                            product.mins = ( voice && (voice.split('_')[0] == "VOICE") ) ? parseInt(voice.split('_')[1]) : 0;
                        } else {
                            if (obj.value != undefined) {
                                if (product.component == "sms") product.sms = parseInt(obj.value);
                                else product.kb = obj.value * 1024;
                            }
                        }
                        if (obj.recurring != undefined) {
                            product.price = parseInt(obj.recurring) / 100;
                            product.recurrent = true;
                        } else if (obj.onetime != undefined) {
                            product.price = parseInt(obj.onetime) / 100;
                            product.recurrent = false;
                        } else {
                            product.price = 0;
                            product.recurrent = true;
                        }
                        if (obj.discount != undefined) {
                            product.discount_price = obj.discount[0].price;
                        }
                    }
                });

                if (!hierarchy.base) ERROR.push({"invalid": "no base"});
                if (!hierarchy.bonus) ERROR.push({"invalid": "no bonus"});
                if (!hierarchy.boost) ERROR.push({"invalid": "no boost"});
                if (!hierarchy.general) ERROR.push({"invalid": "no general"});
                if (hierarchy.base.length == 0) ERROR.push({"invalid": "base length"});
                if (hierarchy.bonus.length == 0) ERROR.push({"invalid": "bonus length"});
                if (hierarchy.boost.length == 0) ERROR.push({"invalid": "boost length"});
                if (hierarchy.general.length == 0) ERROR.push({"invalid": "general length"});
                if (!hierarchy.general[0]) ERROR.push({"invalid": "general[0]"});
                if (typeof(hierarchy.general[0].price) == 'undefined') ERROR.push({"invalid": "general undefined"});
                if (ERROR.length == 0) updateStore(hierarchy);
                else common.error("syncElitecoreHierarchy", JSON.stringify(ERROR));
            } else {
                common.error("HierarchyManager", "syncElitecoreHierarchy: empty hierarchy response ");
                return false;
            }
        });
    });
}

function updateStore(hierarchy) {
    packagesHelper.loadPackages(0, function (rows) {
        rows.forEach(function (row) {
            packagesHelper.updateProductHierarchy(ec.findPackageOriginal(hierarchy, row.product_id), row);
        });
    });
    var key = (config.EC_STAGING) ? "business_hierarchy_elitecore_staging" : "business_hierarchy_elitecore";
    db.cache_put("account_cache", key, hierarchy, 120 * 24 * 60 * 60 * 1000);		// 120 days in cache
    common.log("HierarchyManager", "syncElitecoreHierarchy: hierarchy saved");
}

slack.send({
    "username" : "Process Bot " + config.MYFQDN,
    "text" : process.cwd() + " (HIERARCHY) " + process.env.NODE_ENV + " start [" + process.pid + "]"
});
