require('./global');
const db = require(`${global.__lib}/db_handler`);
const common = require(`${global.__lib}/common`);
const config = require(`${global.__base}/config`);
const slack = require(`${global.__notifications}/slack`);
const healthManager = require(`${global.__manager}/system/healthManager`);

class Main {

    static init(type) {
        process.on('SIGINT', function () {
            if (type == 'queue') {
                db.queue.shutdown(5000, function (err) {
                    if (err) {
                        common.error(process.env.ROLE, `Graceful Shutdown '${err}'`);
                        process.exit(1);
                    } else {
                        common.log(process.env.ROLE, 'Graceful Shutdown \'Clean\'');
                        process.exit(0);
                    }
                });
            }
        });
        
        process.on('uncaughtException', function (err) {
            if (err) {
                common.error('uncaughtException', err.stack);
                if (type == 'scheduler') {
                    db.scheduler.find({ processed: { result: 'ongoing', error: null } }).toArray((error, items) => {
                        if (error) {
                            healthManager.handleSchedulerUncaughtException(err, []);
                        } else {
                            healthManager.handleSchedulerUncaughtException(err, items);
                        }
                    });
                }
                slack.send({
                    username: `Crash Bot (${process.env.ROLE}) ${config.MYFQDN}`,
                    text: err.stack
                });
            }
        });

        process.on('unhandledRejection', function(err) {
            if (err) {
                common.error('unhandledRejection', err.stack);
                if (type == 'scheduler') {
                    db.scheduler.find({ processed: { result: 'ongoing', error: null } }).toArray((error, items) => {
                        if (error) {
                            healthManager.handleSchedulerUnhandledRejection(err, []);
                        } else {
                            healthManager.handleSchedulerUnhandledRejection(err, items);
                        }
                    });
                }
            }
        });
        
        process.addListener('SIGPIPE', function (err) {
            if (err) {
                common.error('SIGPIPE', err.stack);
                if (type == 'scheduler') {
                    db.scheduler.find({ processed: { result: 'ongoing', error: null } }).toArray((error, items) => {
                        if (error) {
                            healthManager.handleSchedulerUncaughtException(err, []);
                        } else {
                            healthManager.handleSchedulerUncaughtException(err, items);
                        }
                    });
                }
                slack.send({
                    username: `Crash Bot (${process.env.ROLE}) ${config.MYFQDN}`,
                    text: err.stack
                });
            }
        });

        process.setMaxListeners(1500);

        slack.send({
            username: `Process Bot ${config.MYFQDN}`,
            text: `${process.cwd()} (${process.env.ROLE}) ${process.env.NODE_ENV} start [${process.pid}]`
        });

        common.log(process.env.ROLE, process.env.NODE_ENV);
    }

}

module.exports = Main;
