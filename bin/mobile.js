require('./global');

var config = require('../config');
require('../lib/manager/ec/elitecorePackagesService').setAPIKey(config.MOBILE_APP_KEY);

var express = require('express');
var cookieParser = require('cookie-parser');
var fs = require('fs');
var async = require('async');
var http = require('http');
var https = require('https');
var child_process = require('child_process');
var slack = require('../src/notifications/slack');
var common = require('../lib/common');

var elitecoreConnector = require('../lib/manager/ec/elitecoreConnector');

var db = require('../lib/db_handler');
var gantryManager = require(__core + '/manager/system/gantryManager');
var healthManager = require('../src/core/manager/system/healthManager');

var registration = require(__api + '/mobile/registration/registration');
var profile = require(__api + '/mobile/profile/profile');
var google = require(__api + '/mobile/inapp/google');
var apple = require(__api + '/mobile/inapp/apple');
var contacts = require(__api + '/mobile/contacts/contacts');
var account = require(__api + '/mobile/account/account');
var qos = require(__api + '/mobile/qos/qos');
var rate = require(__api + '/mobile/qos/rate');
var debug = require(__api + '/mobile/debug/debug');
var message = require(__api + '/mobile/message/message');
var plan = require(__api + '/mobile/plan/plan');
var settings = require(__api + '/mobile/settings/settings');
var external = require(__api + '/mobile/external/external');
var links = require(__api + '/mobile/link/links');
var statsApi = require(__api + '/mobile/qos/stats');

var blockedTimer = healthManager.checkBlocking("MOBILE", 500);
var gantryTimer = gantryManager.ecTimeoutsCheck();

var web = express();
var webs = express();
web.use(cookieParser());    // for webadmin only, https not needed
//webs.use(cookieParser());
web.set('trust proxy', common.TRUSTEDPROXY);
webs.set('trust proxy', common.TRUSTEDPROXY);

var privateKey  = fs.readFileSync(__dirname + '/../lib/https.key', 'utf8');    // relative to where the bin is executed
var certificate = fs.readFileSync(__dirname + '/../lib/https.crt', 'utf8');    // relative to where the bin is executed
var credentials = { key: privateKey, cert: certificate };

exports.web = web;
exports.webs = webs;

var stats = new Array;

setInterval(function () {
    var proto = new Object;
    var ignore = [ ];
    var count = stats.length;
    if (!count) return;
    for (var i=0; i<count; i++) {
        if (!proto[stats[i].protocol]) proto[stats[i].protocol] = { "total" : 0, "max" : 0, maxPath : "" };
        if ( !ignore.some(function (rx) { return rx.test(stats[i].path) }) ) {
             proto[stats[i].protocol].total += stats[i].time;
             if ( proto[stats[i].protocol].max < stats[i].time ) {
                 proto[stats[i].protocol].max = stats[i].time;
                 proto[stats[i].protocol].maxPath = stats[i].path;
             }
             proto[stats[i].protocol].avg = proto[stats[i].protocol].total / count;
        } else {
             count--;
        }
        stats[i] = undefined;
    }
    stats = stats.filter(function(o) { return o; });
    Object.keys(proto).forEach(function (key) {
        db.cache_keys("session_cache", "*", function(sessions) {
            db.apistats.insert({
                "ts" : new Date().getTime(),
                "server" : config.MYFQDN + "_" + key + "_" + process.pid,
                "count" : count, "avg" : proto[key].avg,
                "max" : proto[key].max, "maxPath" : proto[key].maxPath,
                "sessions" : (sessions ? sessions.length : 0)
            });
        });
    });
}, 60 * 1000);

var count = 0;

function gantry(req, res, next) {
    var start = Date.now();
    res.on('finish', function() {
        var duration = Date.now() - start;
        stats.push({ "protocol" : req.protocol, "path" : req.path, "time" : duration });
    });
    gantryManager.get(function(error, cache) {
        if (!cache) {
            next();
        } else {
            if ( (req.connection.remoteAddress == config.MYSELF) || (parseFloat(cache.throttle) >= 1) ) {
                next();
            } else if (count < (parseFloat(cache.throttle) * 10)) {
                count++;
                next();
            } else {
                if (count > 9) count = 0;
                count++;
                var httpStatus = (cache.httpStatus == 503) ? 503 : (cache.httpStatus == 500) ? 500 : 200;
                delete cache.httpStatus;
                res.status(httpStatus).json(cache);
            }
        }
    });
}

webs.get('/test', function(req, res) { res.send("OK"); });
webs.get('/pm2raw', (req, res) => { 
    healthManager.extractImportantParamsFromPm2(function(err, result){
        if(err){
            res.json({code: -1, error: err});
        }else{
            res.json({code: 0, data: result});
        }
    });
});
webs.get('/revision', function(req, res) {
    child_process.execFile("git", [ 'rev-parse', '--short', 'HEAD'], function(error, stdout, stderr) {
        res.json({ "code" : 0, "revision" : stdout.slice(0,-2) });
    });
});
// DEPRECATED
web.get('/test', function(req, res) { res.send("OK"); });
web.get('/pm2raw', (req, res) => { 
    healthManager.extractImportantParamsFromPm2(function(err, result){
        if(err){
            res.json({code: -1, error: err});
        }else{
            res.json({code: 0, data: result});
        }
    });
});
web.get('/revision', function(req, res) {
    child_process.execFile("git", ['describe'], function(error, stdout, stderr) {
        res.json({ "code" : 0, "revision" : stdout.slice(0,-2) });
    });
});
// DEPRECATED

web.use(gantry);
webs.use(gantry);

account.init('/api/2', '/account', web, webs);
registration.init('/api/2', '/registration', web, webs);
profile.init('/api/2', '/profile', web, webs);
google.init('/api/2', '/google', web, webs);
apple.init('/api/2', '/apple', web, webs);
contacts.init('/api/2', '/contacts', web, webs);
qos.init('/api/2', '/qos', web, webs);
rate.init('/api/2', '/rate', web, webs);
message.init('/api/2', '/message', web, webs);
plan.init('/api/2', '/plan', web, webs);
debug.init('/api', '/debug', web, webs);
settings.init('/api/2', '/settings', web, webs);
external.init('/api/2', '/external', web, webs);
links.init('', '/link', web, webs);
statsApi.init('/api/2', '/stats', web, webs);

web.get('*', function (req, res) {
    res.status(404).send('Not Found');
});
webs.get('*', function (req, res) {
    res.status(404).send('Not Found');
});
web.post('*', function (req, res) {
    res.status(404).send('Not Found');
});
webs.post('*', function (req, res) {
    res.status(404).send('Not Found');
});

http.globalAgent.maxSockets = 2000;
https.globalAgent.maxSockets = 2000;
var server = http.createServer(web).listen(config.MOBILEPORT,"0.0.0.0");
var servers = https.createServer(credentials, webs).listen(config.MOBILESPORT,"0.0.0.0");

process.on('SIGINT', function() {
    var tasks = new Array;
    tasks.push(function (callback) {
        clearInterval(blockedTimer);
        callback(undefined, { "err" : undefined, "result" : undefined });
    });
    tasks.push(function (callback) {
        clearInterval(gantryTimer);
        callback(undefined, { "err" : undefined, "result" : undefined });
    });
    tasks.push(function (callback) {
        server.close(function (err, result) {
            callback(undefined, { "err" : err, "result" : result });
        });
    });
    tasks.push(function (callback) {
        servers.close(function (err, result) {
            callback(undefined, { "err" : err, "result" : result });
        });
    });
    new Promise(function (resolve, reject) {
        var timeout = setTimeout(function () {
            common.error("mobile" , "Shutdown timeout");
            reject(new Error("Timeout"));
        }, 5000);
        async.parallel(tasks, function (err, result) {
            clearTimeout(timeout);
            if (err) reject(err);
            else resolve(result);
        });
    }).then(function(result) {
        common.log("mobile", "Graceful Shutdown 'Clean'");
        process.exit(0);
    }).catch(function(err) {
        common.error("mobile", "Graceful Shutdown '" + err + "'");
        process.exit(1);
   });
});
process.on('uncaughtException', function(err) {
    if (err) {
        common.error("Uncaught Exception", err.stack);
        slack.send({
            "username" : "Crash Bot (Mobile) " + config.MYFQDN,
            "text" : err.stack
        });
    }
});
process.addListener('SIGPIPE', function (err) {
    if (err) {
        common.error("SIGPIPE", err.stack);
        slack.send({
            "username" : "Crash Bot (Mobile) " + config.MYFQDN,
            "text" : err.stack
        });
    }
});
process.setMaxListeners(1500);

if (!config.DEV) slack.send({
    "username" : "Process Bot " + config.MYFQDN,
    "text" : process.cwd() + " (Mobile) " + process.env.NODE_ENV + " start [" + process.pid + "]"
});
common.log(process.env.ROLE, process.env.NODE_ENV);
