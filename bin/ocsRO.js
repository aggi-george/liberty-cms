require('./global');

var davp = require('diameter-avp-object');
var common = require('../lib/common');
var config = require('../config');
var diameter = require('diameter');
var ro = require(__core + '/manager/diameter/roServer');

var LOG = config.LOGSENABLED;

var genericErr = [ ['Result-Code', 'DIAMETER_ERROR_OUT_OF_RESOURCES'] ];

var options = new Object;
if (LOG) options.beforeAnyMessage = diameter.logMessage;
if (LOG) options.afterAnyMessage = diameter.logMessage;

var server = diameter.createServer(options, function(socket) {
	socket.on('diameterMessage', function(event) {
	        var msg = davp.toObject(event.message.body);
		ro.triage(event.message.command, msg, function (err, result) {
			if (!err && result) {
				event.response.body = event.response.body.concat(result);
			} else {
				event.response.body = event.response.body.concat(genericErr);
			}
			event.callback(event.response);
		});
	});
	socket.on('end', function() {
		common.log('ocsRO Client Disconnected', socket.diameterConnection.sessionId);
	});
	socket.on('error', function(err) {
		common.error('ocsRO Socket Error', JSON.stringify(err));
	});
});

server.listen(config.OCSROPORT, "0.0.0.0");

process.on('uncaughtException', function(err) { if (err) common.log("Uncaught Exception", JSON.stringify(err.stack)); });
process.addListener('SIGPIPE', function (err) { if (err) common.log("SIGPIPE", JSON.stringify(err.stack)); });
process.setMaxListeners(150);

common.log(process.env.ROLE, process.env.NODE_ENV);
