const main = require('./main');
const mobileActions = require(`${global.__queue}/mobileActions`);

main.init('queue');
mobileActions.init();
