const main = require('./main');
const push = require(`${global.__queue}/push`);
const email = require(`${global.__queue}/email`);
const sms = require(`${global.__queue}/sms`);

main.init('queue');
push.init();
email.init();
sms.init();

