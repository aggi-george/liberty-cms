const main = require('./main');
const payments = require(`${global.__queue}/payments`);

main.init('queue');
payments.init();
