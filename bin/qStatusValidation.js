const main = require('./main');
const statusValidation = require(`${global.__queue}/statusValidation`);

main.init('queue');
statusValidation.init();
