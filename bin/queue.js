const main = require('./main');
const slow = require(`${global.__queue}/slow`);
const creditCapPayment = require(`${global.__queue}/creditCapPayment`);
const aggregation = require(`${global.__queue}/aggregation`);
const ingress = require(`${global.__queue}/ingress`);

main.init('queue');
slow.init();
creditCapPayment.init();
aggregation.init();
ingress.init();
