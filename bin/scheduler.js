const main = require('./main');
const async = require('async');
const repl = require('net-repl');
const config = require(`${global.__base}/config`);
const db = require(`${global.__lib}/db_handler`);
const common = require(`${global.__lib}/common`);
const ec = require(`${global.__lib}/elitecore`);

const notification = require('../src/core/scheduler/action/notification');
const addon = require('../src/core/scheduler/action/addon');
const accountStatus = require('../src/core/scheduler/action/accountStatus');
const report = require('../src/core/scheduler/action/report');
const zendesk = require('../src/core/scheduler/action/zendesk');
const autorepair = require('../src/core/scheduler/action/autorepair');
const portIn = require('../src/core/scheduler/action/portIn');
const payment = require('../src/core/scheduler/action/payment');
const bi = require('../src/core/scheduler/action/bi');
const customerClassSync = require('../src/core/scheduler/action/customerClassSync');
const sync = require('../src/core/scheduler/action/sync');
const healthManager = require('../src/core/manager/system/healthManager');
const constants = require('../src/core/constants');

const SG_PREFIX = '65';
main.init('scheduler');

const go = () => {
    const ref = new Date();
    const y = ref.getFullYear();
    const m = ref.getMonth();
    const d = ref.getDate();
    const h = ref.getHours();
    const startHour = new Date(y, m, d, h).getTime();        // start of current hour, script can run in mid of hour
    const nextHour = startHour + (60 * 60 * 1000);            // end of hour
    db.cache_lock('cache', `scheduler_${startHour}`, 1, 10 * 60 * 1000, (lock) => {
        if (!lock) return common.log('Scheduler', 'Job already taken');
        verifyCompleted(nextHour);
        verifyPickedUp(nextHour);
        verifyReplicates(startHour);
        getEvents(startHour, nextHour);
    });
    clearInterval(job);
    job = setInterval(go, nextHour - ref.getTime() + (7 * 60 * 1000));    // 7 minutes after start of next hour
};

let job = setInterval(go, 3000);    // initially wait just 2 secs

function getEvents(startHour, nextHour) {
    const query = { start: { '$gte': startHour, '$lt': nextHour }, processed: { '$exists': 0 } };
    db.scheduler.find(query).toArray((err, result) => {
        if (err) {
            common.log('SchedulerManager', `failed to load events, err=${err.message}`);
            return;
        }

        if (!result || result.length == 0) {
            common.log('SchedulerManager', `no tasks found, query=${JSON.stringify(query)}`);
            return;
        }

        let tasks = [];
        let tasksParallelLimitOne = [];
        let longRunning = [];

        result.forEach((tEvent) => {
            if (tEvent.repeat) {
                replicateEvent(tEvent);
            }
            const eventHandler = (callback) => {
                const startTime = new Date();
                processEvent(tEvent, (err, result) => {
                    callback(undefined, { error: (err ? err.message : undefined), result: result, queryTime : (new Date() - startTime) });
                });
            };

            //prioritise tasks
            if (constants.SCHEDULER_LONG_RUNNING_ACTIONS.indexOf(tEvent.action) != -1) {
                longRunning.push(eventHandler);
            } else if (tEvent.parallelLimitOne) {
                tasksParallelLimitOne.push(eventHandler);
            } else {
                tasks.push(eventHandler);
            }
        });

        common.log('SchedulerManager', `run general tasks, count=${tasks.length}`);
        async.parallelLimit(tasks, 3, (err, result) => {
            common.log('SchedulerManager', `general tasks finished, result=${JSON.stringify(result)}`);

            common.log('SchedulerManager', `run tasks with parallel limit 1, count=${tasksParallelLimitOne.length}`);
            async.parallelLimit(tasksParallelLimitOne, 1, (err, result) => {
                common.log('SchedulerManager', `tasks with parallel limit 1 finished, result=${JSON.stringify(result)}`);
                db.cache_get('cache', 'terminate-pending', (value) => {
                    if (value && value.length) {
                        let seriesTasks = [];

                        value.forEach(item => {
                            seriesTasks.push((cb) => {
                                classifyEvent(item, cb);
                            });

                            async.series(seriesTasks, (err) => {
                                if (err) {
                                    common.error('Terminate pending: Error while executing series tasks', err.message);
                                }
                            });
                        });
                    }
                });
                async.parallelLimit(longRunning, 10, (err, result) => {
                    common.log('SchedulerManager', `long running tasks finished, result=${JSON.stringify(result)}`);
                });
                // do nothing
            });
        });
    });
}

function verifyReplicates(endHour){
    const startHour = endHour - 3600000;
    let failedActivities = [];
    db.scheduler.find({ start: { '$gt': startHour, '$lt': endHour } }).toArray((err, items) => {
        async.eachLimit(items, 10, function(item, callback){
            if (item.repeat) {
                let num = item.repeat.split(' ')[0];
                let unit = item.repeat.split(' ')[1];
                const allowedUnit = [ 'hour', 'day', 'month', 'year' ];    // only 'month' and 'year' supported for now
                num = (num == parseInt(num)) ? parseInt(num) : undefined;
                unit = (allowedUnit.indexOf(unit) > -1) ? unit : undefined;
                let findQuery;
                if (item.action) {
                    findQuery = { action: item.action, start: getNextTimestamp(item.start, unit, num) };
                } else if (item.activity) {
                    findQuery = { activity: item.activity, start: getNextTimestamp(item.start, unit, num) };
                }
                if (findQuery) {
                    db.scheduler.findOne(findQuery, (err, newItem) => {
                        if (!newItem) {
                            failedActivities.push([
                                new Date(item.start + 3600000 * 8).toString().substr(0, 24),
                                item.action ? item.action : 'N/A',
                                item.activity ? item.activity : 'N/A',
                                item.serviceInstanceNumber ? item.serviceInstanceNumber : 'N/A',
                                item.number ? item.number : 'N/A',
                                item.repeat ? item.repeat : 'N/A'
                            ]);
                        }
                        callback(null);
                    });
                } else {
                    callback(null);
                }
            }
        }, () => {
            if (failedActivities.length > 0) {
                healthManager.sendAlertWithTable(failedActivities, 'Scheduler', 'Abandoned Items From Replicating');
            }
        });
    });
}

function verifyCompleted(nextHour){
    const startHour = nextHour - constants.SCHEDULER_PENDING_HOUR_GAP * 3600000;
    let pendingItems = [];
    db.scheduler.find({ start: { '$lt': startHour, '$gt': constants.SCHEDULER_PENDING_FROM }, color: 'maroon' }).toArray((err, items) => {
        if (items && items.length > 0) {
            items.forEach((item) => {
                pendingItems.push([
                    new Date(item.start + 3600000 * 8).toString().substr(0, 24),
                    item.action ? item.action : 'N/A',
                    item.activity ? item.activity : 'N/A',
                    item.serviceInstanceNumber ? item.serviceInstanceNumber : 'N/A',
                    item.number ? item.number : 'N/A',
                    item.repeat ? item.repeat : 'N/A'
                ]);
            });
            common.log('verifyCompleted', `${items.length} pending items in the scheduler`);
            healthManager.sendAlertWithTable(pendingItems, 'Scheduler', `Long Pending Items even after ${constants.SCHEDULER_PENDING_HOUR_GAP} hrs`);
        } else {
            common.log('verifyCompleted', 'No pending items in the scheduler');
        }
    });
}

function verifyPickedUp(nextHour){
    const startHour = nextHour - constants.SCHEDULER_PICKED_UP_HOUR_GAP * 3600000;
    let notPickedUpItems = [];
    db.scheduler.find({ start: { '$lt': startHour, '$gt': constants.SCHEDULER_NOT_PICKED_UP_FROM }, color: { '$exists': false } } ).toArray((err, items) => {
        if (items.length > 0) {
            items.forEach((item) => {
                notPickedUpItems.push([
                    new Date(item.start + 3600000 * 8).toString().substr(0, 24), 
                    item.action ? item.action : 'N/A', 
                    item.activity ? item.activity : 'N/A', 
                    item.serviceInstanceNumber ? item.serviceInstanceNumber : 'N/A', 
                    item.number ? item.number : 'N/A',
                    item.repeat ? item.repeat : 'N/A'
                ]);
            });
            common.log('verifyPickedUp', `${items.length} not picked up items in the scheduler`);
            healthManager.sendAlertWithTable(notPickedUpItems, 'Scheduler', `Items not picked up by scheduler even after ${constants.SCHEDULER_PICKED_UP_HOUR_GAP} hrs`);
        } else {
            common.log('verifyPickedUp', 'No pending items in the scheduler');
        }
    });
}

function processEvent(tEvent, callback) {
    // action possible value : notification, addon, terminate, suspend, release
    // condition possible key : activity, status
    if (tEvent.condition) {
        let condition = JSON.parse(JSON.stringify(tEvent.condition));
        if (condition.activity) {
            condition['$or'] = [ { number: tEvent.number },
                { email: tEvent.email } ];
            db.notifications_logbook.find(condition).toArray((err, result) => {
                if ( !err && result && (result.length > 0) ) {
                    const processed = { result: 'ignored', error: undefined };
                    const color = 'orange';
                    db.scheduler.update({ _id: tEvent._id },
                        { '$set': { processed, color, myfqdn: config.MYFQDN } });
                    callback(undefined, { id: tEvent._id, processed });
                } else {
                    db.scheduler.update({ _id: tEvent._id },
                        { '$set': { processed: { result: 'ongoing', error: null }, color: 'maroon', myfqdn: config.MYFQDN } }, () => {
                            classifyEvent(tEvent, callback);
                        });
                }
            });
        } else if (condition.status) {
            const prefix = (tEvent.prefix) ? tEvent.prefix : SG_PREFIX;
            ec.getCustomerDetailsNumber(prefix, tEvent.number, false, (err, cache) => {
                if ( err || (cache.status != condition.status) ) {
                    const processed = { 'result' : 'ignored', 'error' : err };
                    const color = 'orange';
                    db.scheduler.update({ _id: tEvent._id },
                        { '$set': { processed, color, myfqdn: config.MYFQDN } });
                    callback(undefined, { id: tEvent._id, processed });
                } else {
                    db.scheduler.update({ _id: tEvent._id },
                        { '$set': { processed: { result: 'ongoing', error: null }, color: 'maroon', myfqdn: config.MYFQDN } }, () => {
                            classifyEvent(tEvent, callback);
                        });
                }
            });
        }
    } else {
        db.scheduler.update({ _id: tEvent._id }, { '$set': { processed: { result: 'ongoing', error: null }, color: 'maroon', myfqdn: config.MYFQDN } }, () => {
            classifyEvent(tEvent, callback);
        });
    }
}

function classifyEvent(tEvent, callback) {
    if (tEvent.action == 'notification') {
        notification.detail(tEvent, callback);
    } else if (tEvent.action == 'addon') {
        addon.addon(tEvent, callback);
    } else if (tEvent.action == 'portin') {
        portIn.initiatePortIn(tEvent, callback);
    } else if (tEvent.action == 'invoice_payment') {
        payment.initiateInvoicePayment(tEvent, callback);
    } else if (tEvent.action == 'portin_failure_check') {
        portIn.initiatePortInFailureCheck(tEvent, callback);
    } else if (tEvent.action == 'portin_completion_status_check') {
        portIn.initiatePortInCompletionStatusCheck(tEvent, callback);
    } else if ((tEvent.action == 'suspend') ||
        (tEvent.action == 'terminate') ||
        (tEvent.action == 'activate')) {
        accountStatus.changeAccount(tEvent, callback);
    } else if (tEvent.action == 'release') {
        accountStatus.release(tEvent, callback);
    } else if (tEvent.action == 'report') {
        report.detail(tEvent, callback);
    } else if (tEvent.action == 'autorepair') {
        autorepair.detail(tEvent, callback);
    } else if (tEvent.action == 'biRun') {
        bi.executeBiRun(tEvent, callback);
    } else if (tEvent.action == 'sync') {
        sync.detail(tEvent, callback);
    } else if (tEvent.action == 'crashTest') {
        bi.crashTest(tEvent, callback);
    } else if (tEvent.action == 'replicateTest') {
        bi.replicateTest(tEvent, callback);
    } else if (tEvent.action == 'zendesk') {
        zendesk.zendeskTicketsRun(tEvent, callback);
    } else if (tEvent.action == 'customer_class_sync') {
        customerClassSync.syncCustomerClasses(tEvent, callback);
    }
}

function replicateEvent(tEvent) {
    let schedule = JSON.parse(JSON.stringify(tEvent));
    schedule.original_id = db.objectID(tEvent._id);
    delete schedule._id;
    let num = schedule.repeat.split(' ')[0];
    let unit = schedule.repeat.split(' ')[1];
    const allowedUnit = [ 'hour', 'day', 'month', 'year', 'ofmonth'];    // only 'month' and 'year' supported for now
    num = (num == parseInt(num)) ? parseInt(num) : num;
    unit = (allowedUnit.indexOf(unit) > -1) ? unit : undefined;
    if (num && unit) {
        schedule.start = getNextTimestamp(schedule.start, unit, num);
        db.scheduler.update({
            start: schedule.start,
            title: schedule.title,
            action: schedule.action,
            activity: schedule.activity,
        }, schedule, { upsert: true });
    }
}

function getNextTimestamp(startTimestamp, unit, num){
    const start = new Date(startTimestamp);
    let next;
    if ( unit == 'hour' ) {
        next = new Date(start.getFullYear(),
            start.getMonth(),
            start.getDate(),
            start.getHours() + num,
            start.getMinutes()).getTime();
    } else if ( unit == 'day' ) {
        next = new Date(start.getFullYear(),
            start.getMonth(),
            start.getDate() + num,
            start.getHours(),
            start.getMinutes()).getTime();
    } else if ( unit == 'month' ) {
        next = new Date(start.getFullYear(),
            start.getMonth() + num,
            start.getDate(),
            start.getHours(),
            start.getMinutes()).getTime();
    } else if ( unit == 'year' ) {
        next = new Date(start.getFullYear() + num,
            start.getMonth(),
            start.getDate(),
            start.getHours(),
            start.getMinutes()).getTime();
    } else if ( unit == 'ofmonth' ) {
        if (num == 'lastday') {
            next = new Date(start.getFullYear(), start.getMonth() + 2, 0).getTime() + (startTimestamp - new Date(startTimestamp).setHours(0,0,0,0));
        }
    }
    return next;
}

if (config.REPL_SCH) {
    repl.createServer({
        prompt: `sch@${config.MYFQDN}>> `,
        deleteSocketOnStart: false
    }).listen('/tmp/sch.sock');
}
