var groupSelect = "";

function init(focus) {
	$(".username")[0].innerHTML = getCookie("cmsuser");
	ajax("GET", "/api/1/web/admin/get/users", {}, true, function (data) {
		if ( data && (data.code == 0) ) {
			buildGroups(data.result.groups);
			showUsers(data.result);
			showAdmin(data.result);
		} else alert("Fail");
	});
	if (focus) $("#" + focus).focus();
}

function updateUserGroup(input) {
	var name = $(input).parent().siblings()[0].innerHTML;
	input.disabled = true;
	ajax("PUT", "/api/1/web/admin/update/user/group/" + name + "/" + input.value, {}, true, function (data) {
		if ( data && (data.code == 0) ) {
			init("showUsers");
		} else alert("Fail");
	});
	return false;
}

function updateName(input) {
	var id = $(input).parent().attr("data-id");
	var name = input.value;
	input.disabled = true;
	ajax("PUT", "/api/1/web/admin/update/group/name/" + id + "/" + name, {}, true, function (data) { 
		if ( data.code == 0 ) {
			init("showAdmin");
		} else alert("Fail");
	});
	return false;
}

function updatePage(input) {
	var id = $(input).parent().siblings().attr("data-id");
	var page = input.value;
	input.disabled = true;
	if ( page == "" ) {
		init("showAdmin");
		return false;
	}
	$.ajax({
		url: "/api/1/web/admin/update/group/page/" + id + "/" + page,
		type: "PUT",
		dataType: "json",
		data: { },
		success: function(data){
			if ( data.code == 0 ) {
				init("showAdmin");
			} else alert("Fail");
		},
		statusCode: {
			403: function(error){
				location.reload();
			}
		}
	});
	return false;
}

function updateGroup(input) {
	var checked = (input.checked) ? "on" : "off";
	var group = $(input).attr("data-id").replace("_", "");
	var perm = $(input).attr("data-allow");
	input.disabled = true;
	$.ajax({
		url: "/api/1/web/admin/update/group/perms/" + group + "/" + perm + "/" + checked,
		type: "PUT",
		dataType: "json",
		data: { },
		success: function(data){
			if ( data.code == 0 ) {
				init("showAdmin");
			} else alert("Fail");
		},
		statusCode: {
			403: function(error){
				location.reload();
			}
		}
	});
	return false;
}

function addUser(form) {
	var email = form.email.value;
	var password = form.password.value;
	var group = form.group.value;
	if (email == "") return false;
	form.submit.disabled = true;
	$.ajax({
		url: "/api/1/web/admin/add/user/" + group,
		type: "PUT",
		dataType: "json",
		data: { "email" : email, "password" : password },
		success: function(data){
			if ( data.code == 0 ) {
				location.reload();
			} else alert("Fail");
		},
		statusCode: {
			403: function(error){
				location.reload();
			}
		}
	});
	return false;
}

function deleteUser(email) {
	$.ajax({
		url: "/api/1/web/admin/delete/user/" + email,
		type: "DELETE",
		dataType: "json",
		data: { },
		success: function(data){
			if ( data.code == 0 ) {
				location.reload();
			} else alert("Fail");
		},
		statusCode: {
			403: function(error){
				location.reload();
			}
		}
	});
	return false;
}

function addGroup(form) {
	var group = form.group.value;
	form.submit.disabled = true;
	$.ajax({
		url: "/api/1/web/admin/add/group/" + group,
		type: "PUT",
		dataType: "json",
		data: { },
		success: function(data){
			if ( data.code == 0 ) {
				location.reload();
			} else alert("Fail");
		},
		statusCode: {
			403: function(error){
				location.reload();
			}
		}
	});
	return false;
}

function updateUserPassword(form) {
	var password = form.password.value;
	if ( password == "" ) {
		location.reload();
		return false;
	}
	var name = $(form).parent().parent().siblings()[0].innerHTML;
	form.password.disabled = true;
	$.ajax({
		url: "/api/1/web/admin/update/user/password/" + name + "/0",		// 0 as bogus id, unused but required
		type: "PUT",
		dataType: "json",
		data: { "password" : password },
		success: function(data){
			if ( data.code == 0 ) {
				alert("Success!");
				location.reload();
			} else alert("Fail");
		},
		statusCode: {
			403: function(error){
				location.reload();
			}
		}
	});
	return false;
}

function updateUser(email, key, value) {
        $.ajax({
                url: "/api/1/web/admin/update/user/" + key + "/" + email + "/" + value,            // 0 as bogus id, unused but required
                type: "PUT",
                dataType: "json",
                data: { },
                success: function(data){
                        if ( data.code == 0 ) {
                                alert("Success!");
                                location.reload();
                        } else alert("Fail");
                },
                statusCode: {
                        403: function(error){
                                location.reload();
                        }
                }
        });
        return false;
}

function getLogs(input) {
	$(".username")[0].innerHTML = getCookie("cmsuser");
	var parameters = "";
	var query = (input) ? "?search=" + input.search.value : "";
	ajax("GET", "/api/1/web/admin/get/logs" + query, parameters, true, function (data) {
		if ( data && (data.code == 0) ) {
			showAccessLogs(data.list);
		} else alert("Fail");
	});
	return false;
}

function buildGroups(groups) {
	groupSelect = "<SELECT id=groupSelect>";
	Object.keys(groups).forEach(function (key) {
		groupSelect += "<OPTION value=" + key.replace("_", "") + ">" + groups[key].name + "</OPTION>";
	});
	groupSelect += "</SELECT>";
}

function showUsers(obj) {
	if ($("#showUsers").dataTableSettings.length > 0) $("#showUsers").DataTable().fnDestroy();
	$("#showUsers tbody").empty();
	obj.users.forEach(function (user) {
		var userRenew = (user.renew == '1') ? "checked" : "";
		var userEnabled = (user.enabled == '1') ? "checked" : "";
		$("#showUsers > tbody:last-child").append("<TR>"+
			"<TD>" + user.email + "</TD>" +
			"<TD class=editGroup>" + user.groupName + "</TD>" +
			"<TD></TD>" +
			"<TD></TD>" +
			"<TD><INPUT class=toggle data-type=renew data-id=" + user.email + " type=checkbox " + userRenew + "/></TD>" +
			"<TD><INPUT class=toggle data-type=enabled data-id=" + user.email + " type=checkbox " + userEnabled + "/></TD>" +
			"<TD><A href=# class=editPassword><I class='fa fa-lock'></I></A></TD>" +
			"<TD><A href=# onClick='if(confirm(\"Are you sure?\")) deleteUser(\"" + user.email + "\");'><I class='fa fa-trash-o'></I></A></TD>" +
		"</TR>");
	});
	$(".toggle").click(function () {
		$(this).prop("disabled", true);
		var value = (this.checked) ? "1" : "0";
		updateUser($(this).attr("data-id"), $(this).attr("data-type"), value);
	});
	$(".editPassword").click(function () {
		if ( $("#passwordEdit").length > 0 ) return false;
		$(this).removeClass('editPassword');
		this.innerHTML = "<FORM onSubmit='event.preventDefault(); updateUserPassword(this);'><INPUT type=password name=password id=passwordEdit size=9 placeholder='Set Password'></FORM>";
		$("#passwordEdit").focus();
	});
	$(".editGroup").click(function () {
		$(this).removeClass('editGroup');
		var selected = this.innerHTML;
		this.innerHTML = groupSelect;
		$("#groupSelect").val($("#groupSelect option:contains(" + selected + ")").val());
		$("#groupSelect").focus();
		$("#groupSelect").change(function() {
			updateUserGroup(this);
		});
		$("#groupSelect").blur(function() {
			init("showUsers");
		});
	});
	$("#showUsers").DataTable();
}

function showAdmin(obj) {
	$("#showAdmin tbody").empty();
	$("#showAdmin thead tr").empty();
	$("#showAdmin thead tr").append("<TH>Name</TH><TH>Default</TH>");
	var pageSelect = "<SELECT id=pageSelect>"
	obj.permissions.forEach(function (perm) {
		$("#showAdmin thead tr").append("<TH style='height:60px; vertical-align:middle'><DIV class=rotate45 style='width:10px'>" + perm.name + "</DIV></TH>");
		pageSelect += ( pageSelect.indexOf(perm.page) > -1 ) ? "" : "<OPTION>" + perm.page + "</OPTION>";
	});
	pageSelect += "</SELECT>";
	Object.keys(obj.groups).forEach(function (key) {
		$("#showAdmin > tbody:last-child").append("<TR>"+
			"<TD class='editName' data-id=" + key.replace("_", "") + ">" + obj.groups[key].name + "</TD>" +
			"<TD class='editPage'>" + obj.groups[key].default + "</TD>" + fillPerms(obj, key) +
		"</TR>");
	});
	$("#newSelect").html(groupSelect);
	$(".permCheck").click(function () {
		updateGroup(this);
	});
	$(".editName").click(function () {
		$(this).unbind("click");
		$(this).removeClass('editName');
		var name = this.innerHTML;
		this.innerHTML = "<INPUT id=nameEdit type=text value='" + name + "'>";
		$("#nameEdit").focus();
		$("#nameEdit").change(function() {
			updateName(this);
		});
		$("#nameEdit").blur(function() {
			init("showAdmin");
		});
	});
	$(".editPage").click(function () {
		$(this).removeClass('editPage');
		var selected = this.innerHTML;
		this.innerHTML = pageSelect;
		$("#pageSelect").val($("#pageSelect option:contains(" + selected + ")").val());
		$("#pageSelect").focus();
		$("#pageSelect").change(function() {
			updatePage(this);
		});
		$("#pageSelect").blur(function() {
			init("showAdmin");
		});
	});
}

function fillPerms(obj, id) {
	var perms = "";
	obj.permissions.forEach(function (perm) {
		if ( obj.groups[id].list.indexOf(perm.id) > -1 ) perms += "<TD><INPUT class=permCheck type=checkbox checked data-id=" + id + " data-allow=" + perm.id + "></TD>";
		else perms += "<TD><INPUT class=permCheck type=checkbox data-id=" + id + " data-allow=" + perm.id + "></TD>";
	});
	return perms;
}

function showAccessLogs(logs) {
	$("#showLogs tbody").empty();
	logs.forEach(function (log) {
		var date = getDateTime(log.ts);
		$("#showLogs > tbody:last-child").append("<TR>" +
			"<TD>" + date + "</TD>" +
			"<TD>" + log.ip + "</TD>" +
			"<TD>" + log.type + "</TD>" +
			"<TD>" + log.username + "</TD>" +
			"<TD>" + log.code + "</TD>" +
		"</TR>");
	});
}
