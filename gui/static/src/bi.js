var kpiDefKeys = [];
var uiElementKeys = [];
var kpiGenKeys = {};
var userPermissions;
var tabs = [
    {key: 'keyinsights', value: 'Key Insights'},
    {key: 'targets', value: 'targets'},
    {key: 'details', value: 'details'},
    {key: 'favourites', value: 'Favourites'},
    {key: 'zendesk', value: 'Zendesk'},
    {key: 'numbers', value: 'Numbers'}
];
var units = {
    currency: "$",
    number: "",
    percentage: "%",
    megabyte: "MB",
    gigabyte: "GB"
};
var tableTemplate = `<div class="col-sm-12">
                            <div class="row">
                                <div class="col-sm-12">
                                    <section class="panel">
                                        <header class="panel-heading">
                                            {name}
                                            <span class="tools pull-right">
                                                <BUTTON displayType="{displayType}" key="{key}" class='btn action-button addKpiViewItem'>Set</BUTTON>
                                                <BUTTON displayType="{displayType}" key="{key}" class='btn action-button listKpiViewItems'>List</BUTTON>
                                                <a href="javascript:;" class="fa fa-chevron-down"></a></span>
                                        </header>
                                        <div class="panel-body">
                                            <table class="table  table-hover general-table smallFontTable biTable" id="{key}">
                                            </table>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </div>`;
var targetsTableTemplate = `<div class="col-sm-12">
                            <div class="row">
                                <div class="col-sm-12">
                                    <section class="panel">
                                        <header class="panel-heading">
                                            {name}
                                            <span class="tools pull-right">
                                                <BUTTON displayType="{displayType}" key="{key}" class='btn action-button addKpiViewItem'>Set</BUTTON>
                                                <BUTTON displayType="{displayType}" key="{key}" class='btn action-button listKpiViewItems'>List</BUTTON>
                                                <a href="javascript:;" class="fa fa-chevron-down"></a></span>
                                        </header>
                                        <div class="panel-body">
                                            <table class="table  table-hover general-table smallFontTable tableWidthDynamic" id="{key}">
                                            </table>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </div>`;
var listTableTemplate = `<div class="col-sm-12">
                            <div class="row">
                                <div class="col-sm-12">
                                    <section class="panel">
                                        <header class="panel-heading">
                                            {name}
                                            <span class="tools pull-right">
                                                <BUTTON displayType="{displayType}" key="{key}" list="true" class='btn action-button addKpiViewItem'>Set</BUTTON>
                                                <BUTTON displayType="{displayType}" key="{key}" class='btn action-button listKpiViewItems'>List</BUTTON>
                                                <a href="javascript:;" class="fa fa-chevron-down"></a></span>
                                        </header>
                                        <div class="panel-body">
                                            <table class="table  table-hover general-table smallFontTable tableWidthDynamic" id="{key}">
                                            </table>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </div>`;
var graphTemplate = `<div class="col-sm-12">
                            <div class="row">
                                <div class="col-sm-12">
                                    <section class="panel">
                                        <header class="panel-heading">
                                            {name}
                                            <span class="tools pull-right">
                                                <BUTTON displayType="{displayType}" key="{key}" class='btn action-button addKpiViewItem'>Set</BUTTON>
                                                <BUTTON displayType="{displayType}" key="{key}" class='btn action-button listKpiViewItems'>List</BUTTON>
                                                <a href="javascript:;" class="fa fa-chevron-down"></a></span>
                                        </header>
                                        <div class="panel-body">
                                            <div class="chartPanel" id="{key}"></div>
                                        </div>
                                    </section>
                                </div>
                            </div>
                        </div>`;
var numberTemplate = `<div class="col-sm-6">
                        <div id="cart1" class="row ">
                            <div class="col-sm-12">
                                <section class="panel">
                                    <header class="panel-heading">
                                        {name}
                                        <span class="tools pull-right">
                                            <BUTTON displayType="{displayType}" key="{key}" class='btn action-button addKpiViewItem'>Set</BUTTON>
                                            <a href="javascript:;" class="fa fa-chevron-down"></a>
                                            <a href="javascript:;" class="fa fa-times"></a>
                                        </span>
                                    </header>
                                    <div class="definition">{description}<br></div>
                                    <div id="{key}" class="panel-body numberStat"></div>
                                </section>
                            </div>
                        </div>
                    </div>`;
// TODO: FIX THIS! eranga, absolutely horrible -- kelvin
var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
var monthlyBoundKeys = ["May 2016", "Jun 2016", "Jul 2016", "Aug 2016", "Sep 2016", "Oct 2016", "Nov 2016", "Dec 2016", "Jan 2017", "Feb 2017", 
                        "Mar 2017", "Apr 2017", "May 2017", "Jun 2017", "Jul 2017", "Aug 2017", "Sep 2017", "Oct 2017", "Nov 2017", "Dec 2017",
                        "Jan 2018", "Feb 2018", "Mar 2018", "Apr 2018", "May 2018", "Jun 2018",
                        "Jul 2018", "Aug 2018", "Sep 2018", "Oct 2018", "Nov 2018", "Dec 2018"];
var dateObj = new Date();
var currentTime = dateObj.getTime();
var currentMonth = dateObj.getMonth();
var currentYear = dateObj.getFullYear();
var monthString = (currentMonth == 0) ? months[11] + " " + (currentYear - 1) : months[currentMonth-1] + " " + currentYear;
var monthlyBoundsTillNow = monthlyBoundKeys.slice(0, monthlyBoundKeys.indexOf(monthString) + 1);

var monthlyBounds = {
    "May 2016": {month: "May 2016", startDate: '01-05-2016 00:00:00', startTimestamp: 1462060800000, endDate: '01-06-2016 00:00:00', endTimestamp: 1464739200000},
    "Jun 2016": {month: "Jun 2016", startDate: '01-06-2016 00:00:00', startTimestamp: 1464739200000, endDate: '01-07-2016 00:00:00', endTimestamp: 1467331200000},
    "Jul 2016": {month: "Jul 2016", startDate: '01-07-2016 00:00:00', startTimestamp: 1467331200000, endDate: '01-08-2016 00:00:00', endTimestamp: 1470009600000},
    "Aug 2016": {month: "Aug 2016", startDate: '01-08-2016 00:00:00', startTimestamp: 1470009600000, endDate: '01-09-2016 00:00:00', endTimestamp: 1472688000000},
    "Sep 2016": {month: "Sep 2016", startDate: '01-09-2016 00:00:00', startTimestamp: 1472688000000, endDate: '01-10-2016 00:00:00', endTimestamp: 1475280000000},
    "Oct 2016": {month: "Oct 2016", startDate: '01-10-2016 00:00:00', startTimestamp: 1475280000000, endDate: '01-11-2016 00:00:00', endTimestamp: 1477958400000},
    "Nov 2016": {month: "Nov 2016", startDate: '01-11-2016 00:00:00', startTimestamp: 1477958400000, endDate: '01-12-2016 00:00:00', endTimestamp: 1480550400000},
    "Dec 2016": {month: "Dec 2016", startDate: '01-12-2016 00:00:00', startTimestamp: 1480550400000, endDate: '01-01-2017 00:00:00', endTimestamp: 1483228800000},
    "Jan 2017": {month: "Jan 2017", startDate: '01-01-2017 00:00:00', startTimestamp: 1483228800000, endDate: '01-02-2017 00:00:00', endTimestamp: 1485907200000},
    "Feb 2017": {month: "Feb 2017", startDate: '01-02-2017 00:00:00', startTimestamp: 1485907200000, endDate: '01-03-2017 00:00:00', endTimestamp: 1488326400000},
    "Mar 2017": {month: "Mar 2017", startDate: '01-03-2017 00:00:00', startTimestamp: 1488326400000, endDate: '01-04-2017 00:00:00', endTimestamp: 1491004800000},
    "Apr 2017": {month: "Apr 2017", startDate: '01-04-2017 00:00:00', startTimestamp: 1491004800000, endDate: '01-05-2017 00:00:00', endTimestamp: 1493596800000},
    "May 2017": {month: "May 2017", startDate: '01-05-2017 00:00:00', startTimestamp: 1493596800000, endDate: '01-06-2017 00:00:00', endTimestamp: 1496275200000},
    "Jun 2017": {month: "Jun 2017", startDate: '01-06-2017 00:00:00', startTimestamp: 1496275200000, endDate: '01-07-2017 00:00:00', endTimestamp: 1498867200000},
    "Jul 2017": {month: "Jul 2017", startDate: '01-07-2017 00:00:00', startTimestamp: 1498867200000, endDate: '01-08-2017 00:00:00', endTimestamp: 1501545600000},
    "Aug 2017": {month: "Aug 2017", startDate: '01-08-2017 00:00:00', startTimestamp: 1501545600000, endDate: '01-09-2017 00:00:00', endTimestamp: 1504224000000},
    "Sep 2017": {month: "Sep 2017", startDate: '01-09-2017 00:00:00', startTimestamp: 1504224000000, endDate: '01-10-2017 00:00:00', endTimestamp: 1506816000000},
    "Oct 2017": {month: "Oct 2017", startDate: '01-10-2017 00:00:00', startTimestamp: 1506816000000, endDate: '01-11-2017 00:00:00', endTimestamp: 1509494400000},
    "Nov 2017": {month: "Nov 2017", startDate: '01-11-2017 00:00:00', startTimestamp: 1509494400000, endDate: '01-12-2017 00:00:00', endTimestamp: 1512086400000},
    "Dec 2017": {month: "Dec 2017", startDate: '01-12-2017 00:00:00', startTimestamp: 1512086400000, endDate: '01-01-2018 00:00:00', endTimestamp: 1514764800000},
    "Jan 2018": {month: "Jan 2018", startDate: '01-01-2018 00:00:00', startTimestamp: 1514764800000, endDate: '01-02-2018 00:00:00', endTimestamp: 1517443200000},
    "Feb 2018": {month: "Feb 2018", startDate: '01-02-2018 00:00:00', startTimestamp: 1517443200000, endDate: '01-03-2018 00:00:00', endTimestamp: 1519862400000},
    "Mar 2018": {month: "Mar 2018", startDate: '01-03-2018 00:00:00', startTimestamp: 1519862400000, endDate: '01-04-2018 00:00:00', endTimestamp: 1522540800000},
    "Apr 2018": {month: "Apr 2018", startDate: '01-04-2018 00:00:00', startTimestamp: 1522540800000, endDate: '01-05-2018 00:00:00', endTimestamp: 1525132800000},
    "May 2018": {month: "May 2018", startDate: '01-05-2018 00:00:00', startTimestamp: 1525132800000, endDate: '01-06-2018 00:00:00', endTimestamp: 1527811200000},
    "Jun 2018": {month: "Jun 2018", startDate: '01-06-2018 00:00:00', startTimestamp: 1527811200000, endDate: '01-07-2018 00:00:00', endTimestamp: 1530403200000},
    "Jul 2018": {month: "Jul 2018", startDate: '01-07-2018 00:00:00', startTimestamp: 1530403200000, endDate: '01-08-2018 00:00:00', endTimestamp: 1533081600000},
    "Aug 2018": {month: "Aug 2018", startDate: '01-08-2018 00:00:00', startTimestamp: 1533081600000, endDate: '01-09-2018 00:00:00', endTimestamp: 1535760000000},
    "Sep 2018": {month: "Sep 2018", startDate: '01-09-2018 00:00:00', startTimestamp: 1535760000000, endDate: '01-10-2018 00:00:00', endTimestamp: 1538352000000},
    "Oct 2018": {month: "Oct 2018", startDate: '01-10-2018 00:00:00', startTimestamp: 1538352000000, endDate: '01-11-2018 00:00:00', endTimestamp: 1541030400000},
    "Nov 2018": {month: "Nov 2018", startDate: '01-11-2018 00:00:00', startTimestamp: 1541030400000, endDate: '01-12-2018 00:00:00', endTimestamp: 1543622400000},
    "Dec 2018": {month: "Dec 2018", startDate: '01-12-2018 00:00:00', startTimestamp: 1543622400000, endDate: '01-01-2019 00:00:00', endTimestamp: 1546300800000}
};
                        

DEFAULT_RELOAD_CALLBACK = function(){
    showLoadingLabel();
    async.series([loadPermissions, loadKpiDefinitions, loadKpiGenerators, loadKpiViews, populateDataToViews, bindButtonActions], function(err, data){
        hideLoadingLabel();
        if(err){
            showToast("warining", "Error parrelel exection to load BI data");
            console.log(err);
        }else{
            data.forEach(function (record) {
                if(record.code == -1){
                    showToast("warining", record.message);
                    console.log(record.error);
                }
            });
        }
    });
}

function loadPermissions(callback){
    $.ajax({
        "type": "GET",
        "dataType": "json",
        "url": "/api/1/web/bi/get/permissions",
        "data": {},
        "success": function (response) {
            if(response.code == 0){
                userPermissions = response.permissions;
                callback(null, {code: 0});
            }else{
                callback({code: -1, message: "error getting permissons"});
            }
        },
        "error": function (result) {
            showToast("danger", "Loading error");
        }
    });
}

DEFAULT_RELOAD_CALLBACK();

function loadKpiDefinitions(callback){
    $.ajax({
        "type": "GET",
        "dataType": "json",
        "url": "/api/1/web/bi/get/kpiDefinitions",
        "data": {},
        "success": function (response) {
            kpiDefKeys = reloadKpiDefinitionsList(response.list);
            uiElementKeys = reloadUiElementsList();
            if(response.code == 0){
                generateTable("kpiDefinitionList", response.list, [
                    {"sTitle": "Key", "sClass": "twocol-table-column", "mData": "key", "mRender": function(data, type, full){
                            return data;
                        }
                    },
                    {"sTitle": "Name", "sClass": "twocol-table-column", "mData": "name", "mRender": function(data, type, full){
                            return data;
                        }
                    },
                    {"sTitle": "Description", "sClass": "twocol-table-column", "mData": "description", "mRender": function(data, type, full){
                            return data;
                        }
                    },
                    {"sTitle": "Type", "sClass": "twocol-table-column", "mData": "type", "mRender": function(data, type, full){
                            return data;
                        }
                    },
                    {"sTitle": "Data Type", "sClass": "twocol-table-column", "mData": "dataType", "mRender": function(data, type, full){
                            return data;
                        }
                    },
                    {"sTitle": "Actions", "sClass": "twocol-table-column", "mData": "key", "mRender": function(data, type, full){
                            if(userPermissions.write == 1){
                                full.oldKey = full.key;
                                return getTableActionButton('Update', undefined, "ADD_UPDATE_KPI_DEFINITION", full) + 
                                getTableActionButton('Delete', undefined, "DELETE_KPI_DEFINITION", full);
                            }else{
                                return "N/A";
                            }
                        }
                    }
                ], null, true);
                callback(null, {code: 0});
            }else{
                callback(null, {code: -1, error: response.error, message: 'Error fetching kpi defs'});
            }
        },
        "error": function (result) {
            callback(null, {code: -1, error: result, message: 'Error fetching kpi defs'});
        }
    });
}

function loadKpiGenerators(callback){
    showLoadingLabel();
    $.ajax({
        "type": "GET",
        "dataType": "json",
        "url": "/api/1/web/bi/get/kpiGenerators",
        "data": {},
        "success": function (response) {
            hideLoadingLabel();
            if(response.code == 0){
                generateTable("kpiGeneratorList", response.list, [
                    {"sTitle": "Key", "sClass": "twocol-table-column", "mData": "key", "mRender": function(data, type, full){
                            return data;
                        }
                    },
                    {"sTitle": "Definition Key", "sClass": "twocol-table-column", "mData": "definitionKey", "mRender": function(data, type, full){
                            return data;
                        }
                    },
                    {"sTitle": "Period", "sClass": "twocol-table-column", "mData": "period", "mRender": function(data, type, full){
                            return data;
                        }
                    },
                    {"sTitle": "Name", "sClass": "twocol-table-column", "mData": "name", "mRender": function(data, type, full){
                            return data;
                        }
                    },
                    {"sTitle": "Actions", "sClass": "twocol-table-column", "mData": "key", "mRender": function(data, type, full){
                        if(userPermissions.write == 1){
                            full.kpiDefKeys = kpiDefKeys;
                            full.uiElements = uiElementKeys;
                            full.oldKey = full.key;
                            if(full.status !== "pending"){
                                return getTableActionButton('Update', undefined, "ADD_UPDATE_KPI_GENERATOR", full) + 
                                getTableActionButton('Delete', undefined, "DELETE_KPI_GENERATOR", full) + 
                                ((full.period == "monthly") ? getTableActionButton('Regenerate', undefined, "REGENARATE_VALUES_FOR_KPI_GENERATOR", {key: full.key}) : "") + 
                                getTableActionButton('Add Value', undefined, "ADD_KPI_VALUE", {generatorKey: full.key, period: full.period, definitionKey: full.definitionKey}) + 
                                ((full.period == "monthly") ? (full.manual ? "" : getTableActionButton('Regenerate Last', undefined, "REGENARATE_VALUES_FOR_KPI_GENERATOR", {key: full.key, lastMonthOnly: true})) : "");
                            }else{
                                return "Generating..";
                            }
                        }else{
                            return "N/A";
                        }
                    
                        }
                    }
                ], null, true);
                callback(null, {code: 0});
            }else{
                callback(null, {code: -1, error: response.error, message: 'Error fetching kpi defs'});
            }
        },
        "error": function (result) {
            callback(null, {code: -1, error: result, message: 'Error fetching kpi defs'});
        }
    });
}

function loadKpiViews(callback){
    showLoadingLabel();
    $.ajax({
        "type": "GET",
        "dataType": "json",
        "url": "/api/1/web/bi/get/kpiViews",
        "data": {},
        "success": function (response) {
            hideLoadingLabel();
            showKpiViews(response.list);
            if(response.code == 0){
                generateTable("kpiViewsList", response.list, [
                    {"sTitle": "Key", "sClass": "twocol-table-column", "mData": "key", "mRender": function(data, type, full){
                            return data;
                        }
                    },
                    {"sTitle": "Name", "sClass": "twocol-table-column", "mData": "name", "mRender": function(data, type, full){
                            return data;
                        }
                    },
                    {"sTitle": "Type", "sClass": "twocol-table-column", "mData": "viewType", "mRender": function(data, type, full){
                            return data;
                        }
                    },
                    {"sTitle": "Timeline", "sClass": "twocol-table-column", "mData": "timeline", "mRender": function(data, type, full){
                            return data;
                        }
                    },
                    {"sTitle": "Tab", "sClass": "twocol-table-column", "mData": "tab", "mRender": function(data, type, full){
                            return data;
                        }
                    },
                    {"sTitle": "Actions", "sClass": "twocol-table-column", "mData": "key", "mRender": function(data, type, full){
                        if(userPermissions.write == 1){
                            full.tabs = tabs;
                            full.updating = true;
                            return getTableActionButton('Update', undefined, "ADD_UPDATE_KPI_VIEW", full) + 
                            getTableActionButton('Delete', undefined, "DELETE_KPI_VIEW", full);
                        }else{
                            return "N/A"
                        }
                        }
                    }
                ]);
                callback(null, {code: 0});
            }else{
                callback(null, {code: -1, error: response.error, message: 'Error fetching kpi defs'});
            }
        },
        "error": function (result) {
            callback(null, {code: -1, error: result, message: 'Error fetching kpi defs'});
        }
    });
}

function bindButtonActions(callback){
    if(userPermissions.write == 1){
        $("#addKpiDefinition").unbind("click").click(function(){
            if(userPermissions.write == 1){
                generateConfirmationDialog('ADD_UPDATE_KPI_DEFINITION',{}, undefined,
                function(err, data){
                    if(err){
                        showToast("warning", genUserMessage('ERROR_SAVING_KPI_DEFINITION'));
                    }else{
                        showLoadingLabel();
                        async.series([loadKpiDefinitions, loadKpiGenerators], function(err, data){
                            hideLoadingLabel();
                            if(err){
                                showToast("warining", "Error parrelel exection to load BI data");
                                console.log(err);
                            }else{
                                data.forEach(function (record) {
                                    if(record.code == -1){
                                        showToast("warining", record.message);
                                        console.log(record.error);
                                    }
                                });
                            }
                        });
                    }
                });
                setTimeout(function(){
                    $('[name="query"]').parent().css( "height", "100px");
                }, 220);
            }else{
                showToast("warning", "Not Allowed");
            }
        });

        $("#addKpiGenerator").unbind("click").click(function(){
            if(userPermissions.write == 1){
                generateConfirmationDialog('ADD_UPDATE_KPI_GENERATOR',{kpiDefKeys: kpiDefKeys, uiElements: uiElementKeys, upperBound: 0.0, lowerBound: 0.0}, undefined,
                function(err, data){
                    if(err){
                        showToast("warning", genUserMessage('ERROR_SAVING_KPI_GENERATORS'));
                    }else{
                        showLoadingLabel();
                        async.series([loadKpiDefinitions, loadKpiGenerators], function(err, data){
                            hideLoadingLabel();
                            if(err){
                                showToast("warining", "Error parrelel exection to load BI data");
                                console.log(err);
                            }else{
                                data.forEach(function (record) {
                                    if(record.code == -1){
                                        showToast("warining", record.message);
                                        console.log(record.error);
                                    }
                                });
                            }
                        });
                    }
                });
            }else{
                showToast("warning", "Not Allowed");
            }
        });

        $("#addKpiView").unbind("click").click(function(){
            if(userPermissions.write == 1){
                generateConfirmationDialog('ADD_UPDATE_KPI_VIEW', {tabs: tabs}, undefined,
                function(err, data){
                    if(err){
                        showToast("warning", genUserMessage('ERROR_SAVING_KPI_VIEW'));
                    }else{
                        DEFAULT_RELOAD_CALLBACK();
                    }
                });
            }else{
                showToast("warning", "Not Allowed");
            }
        });
    }else{
        $("#addKpiDefinition").hide();
        $("#addKpiGenerator").hide();
        $("#addKpiView").hide();
    }
    callback(null, {code: 0});
}



function addKpiForView(type, displayKey, list){
    list = list ? 'true' : 'false';
    loadActiveKpiGenKeys(function(err, keysMap){
        if(err){
            showToast("warning", "Error loading KPI keys map");
        }else{
            generateConfirmationDialog('ADD_KPI_TO_VIEW',{displayType: type, key: displayKey, generatorList: keysMap[type], list: list}, undefined,
            function(err, data){
                if(err){
                    showToast("warning", "Error saving KPI for view");
                }else{
                    DEFAULT_RELOAD_CALLBACK();
                }
            });
        }
    });
}

function showKpiViews(viewList){
    clearTabs();
    var html;
    viewList.forEach(function (view) {
        switch(view.viewType){
            case "table":
                html = tableTemplate.split("{key}").join(view.key).split("{name}").join(view.name).split("{displayType}").join(view.timeline);
                break;
            case "number":
                html = numberTemplate.split("{key}").join(view.key).split("{name}").join(view.name).split("{description}").join(view.description).split("{displayType}").join(view.timeline);
                break;
            case "graph":
                html = graphTemplate.split("{key}").join(view.key).split("{name}").join(view.name).split("{displayType}").join(view.timeline);
                break;
            case "target":
                html = targetsTableTemplate.split("{key}").join(view.key).split("{name}").join(view.name).split("{displayType}").join(view.timeline);
                break;
            case "list":
                html = listTableTemplate.split("{key}").join(view.key).split("{name}").join(view.name).split("{displayType}").join(view.timeline);
                break;
        }
        $("#" + view.tab + " > div").append(html);
    });
}

function loadActiveKpiGenKeys(callback){
    var kpiGeneratorKeysMap = {};
    $.ajax({
        "type": "GET",
        "dataType": "json",
        "url": "/api/1/web/bi/get/kpiGenKeys",
        "data": {},
        "success": function (response) {
            if(response.code == 0){
                response.list.forEach(function (item) {
                    kpiGeneratorKeysMap[item._id] = item.values;
                });
                callback(null, kpiGeneratorKeysMap);
            }else{
                callback({code: response.code}, null);
            }
        },
        "error": function (result) {
            showToast("danger", "Loading error");
        }
    });
}

function reloadKpiDefinitionsList(list){
    output = [];
    list.forEach(function (item) {
        output.push(item.key);
    });
    return output;
}

function reloadUiElementsList(list){
    return ['totalActiveUsers', 'totalSuspendedUsers', 'totalTerminatedUsers', 'mainKpiTable'];
}


//UI logic
function populateDataToViews(callback){
    $.ajax({
        "type": "GET",
        "dataType": "json",
        "url": "/api/1/web/bi/get/kpiViewsWithValues",
        "data": {},
        "success": function (response) {
            if(response.code == 0){
                response.list.forEach(function (item) {
                    switch(item._id.viewType){
                        case "number":
                            $("#" + item._id.key).append(validateAndFormatValue(item._id.timeline, item.items[0].values[0]));
                            break;
                        case "table":
                            $("#" + item._id.key).append("<thead><tr>");
                            $("#" + item._id.key).append("<th class='biNameColumn'>Name</th>");
                            if(item._id.timeline == "monthly"){
                                monthlyBoundsTillNow.forEach(function (month) {
                                    $("#" + item._id.key).append("<th class='biValueColumn biTableTh'>" + month + "</th>");
                                });
                            }else if(item.items[0]){
                                item.items[0].values.forEach(function (entry, index) {
                                    $("#" + item._id.key).append("<th class='biValueColumn biTableTh'>" + getDate(new Date(entry.startDate)) + "</th>");
                                });
                            }
                            item.items.forEach(function (kpiRow) {
                                var htmlRow = "<tr><td class='biNameColumn'>" + kpiRow.kpiGenName + "</td>";
                                kpiRow.values.forEach(function (entry, index) {
                                    htmlRow += "<td class='biValueColumn'>" + validateAndFormatValue(item._id.timeline, entry, index) + "</td>";
                                });
                                $("#" + item._id.key).append(htmlRow + "</tr>");
                            });
                            break;
                        case "graph":
                            if(item._id.timeline == "monthly"){
                                generateGraph(monthlyBoundsTillNow, item._id, item.items);
                            }else if(item.items[0]){
                                var xAxisData = [];
                                item.items[0].values.forEach(function (entry, index) {
                                    xAxisData.push(getDate(new Date(entry.startDate)));
                                });
                                generateGraph(xAxisData, item._id, item.items);
                            }
                            break;
                        case "target":
                            $("#" + item._id.key).append("<thead><tr><th class='biTargetsColumn'>Name</th><th class='biTargetsColumn'>Lower</th><th class='biTargetsColumn'>Upper</th><th class='biTargetsColumn'>Actual</th></tr></thead>");
                            item.items.forEach(function (kpiRow) {
                                var entry = kpiRow.values[0];
                                var valueClass = (entry.lowerBound[0] <= entry.value && entry.upperBound[0] >= entry.value) ? "biGreenClass" : "biRedClass";
                                $("#" + item._id.key).append("<tr><td class='biTargetsColumn'>" + kpiRow.kpiGenName + "</td><td class='biTargetsColumn'>" + entry.lowerBound[0] + "</td><td class='biTargetsColumn'>" + entry.upperBound[0] + "</td><td class='" + valueClass + "'>"
                                + validateAndFormatValue(item._id.timeline, entry) + "</td></tr>");
                            });
                            break;
                        case "list":
                            $("#" + item._id.key).append("<thead><tr><th>Name</th><th>Value</th></tr></thead>");
                            item.items.forEach(function (kpiRow) {
                                var entry = kpiRow.values[0];
                                $("#" + item._id.key).append("<tr><td>" + kpiRow.kpiGenName + "</td><td>" + validateAndFormatValue(item._id.timeline, entry) + "</td></tr>");
                            });
                            break;
                    }
                });
                $(".addKpiViewItem").click(function(){
                    addKpiForView($(this).attr('displayType'), $(this).attr('key'), $(this).attr('list'));
                });
                 $(".listKpiViewItems").click(function(){
                    $.ajax({
                        "type": "GET",
                        "dataType": "json",
                        "url": "/api/1/web/bi/get/listKpiViewItems",
                        "data": {displayType: $(this).attr('displayType'), key: $(this).attr('key')},
                        "success": function (response) {
                            if(response.list.length > 0){
                                generateTableDialog("biViewList", response.list);
                            }else{
                                showToast("danger", "No Items");
                            }
                        },
                        "error": function (result) {
                            showToast("danger", "Loading error");
                        }
                    });
                });
                callback(null, {code: 0});
            }else{
                callback(null, {code: -1, error: response.error, message: 'Error populating data to views'});
            }
        },
        "error": function (result) {
            $('[data-toggle="tooltip"]').tooltip();
            callback(null, {code: -1, error: response.error, message: 'Error populating data to views'});
        }
    });
}

function generateGraph(xAxisDate, graphMetaData, data){
    var formattedDataForGraph = [];
    data.forEach(function (dataItem) {
        var lineData = {name: dataItem.kpiGenName[0], data: []};
        dataItem.values.forEach(function (value) {
            lineData.data.push(value.value);
        });
        formattedDataForGraph.push(lineData);
    });
    Highcharts.chart(graphMetaData.key, {
        title: {
            text: graphMetaData.name
        },
        subtitle: {
            text: graphMetaData.description
        },
        xAxis: {
            categories: xAxisDate,
            crosshair: true
        },
        yAxis: {
            title: {
                text: 'Amount'
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'middle',
            useHTML:true,
            labelFormatter: function() {
                // do truncation here and return string
                // this.name holds the whole label
                // for example:
                return '<a href="#" data-toggle="tooltip" title="' + this.name + '">' + this.name.slice(0, 10) + '...</a>';
            }
        },
        series: formattedDataForGraph
    });
}

function validateAndFormatValue(period, entry, index){
    switch(period){
        case "monthly":
            if(monthlyBounds[monthlyBoundsTillNow[index]].startTimestamp == entry.startDate && 
            monthlyBounds[monthlyBoundsTillNow[index]].endTimestamp == entry.endDate && !isNaN(entry.value) && entry.value != 0){
                return getValueWithUnit(entry);
            }else{
                return "N/A";
            }
        case "weekly":
            if(!isNaN(entry.value) && entry.value != 0){
                return getValueWithUnit(entry);
            }else{
                return "N/A";
            }
        case "daily":
            if(!isNaN(entry.value) && entry.value != 0){
                return getValueWithUnit(entry);
            }else{
                return "N/A";
            }
        case "alltime":
            if(isNaN(entry.value) || entry.value == 0){
                return "N/A";
            }else{
                return getValueWithUnit(entry);
            }
    }
}

function getValueWithUnit(entry){
    var finalValue = Math.round(entry.value * 100)/100;
    var unit = units[entry.dataType];
    return finalValue + unit;
}

function clearTabs(){
    $("#numbers > div").empty();
    $("#keyinsights > div").empty();
    $("#details > div").empty();
    $("#targets > div").empty();
    $("#zendesk > div").empty();
}

function getRawQueryResults(){
    $.ajax({
        "type": "GET",
        "dataType": "json",
        "url": "/api/1/web/bi/get/rawQuery",
        "data": {query: "select * from crestelcaamlwp.tbltcustomerpackagehistory where rownum < 10"},
        "success": function (response) {
            hideLoadingLabel();
            if(response.code == 0){
                console.log(response.results);
            }else{
                console.log("Error in raw query");
            }
        },
        "error": function (result) {
            showToast("danger", "Loading error");
        }
    });
}

