/**
*
*  Bill related client facing js functions
*
*/

var archivesTableRelations;
var billsTableRelations;
var broadcastingInProgress;
var broadcastingStarted;
var broadcastingTimeout;

function initBlockedAccount() {
    clearTable();
    loadBlockedBillingAccounts();
    $("#addAll").click(function () {
        if (!data || data.length == 0) {
            return showToast("danger", "No data to add");
        }
        addBlockedAccounts(data);
    });
}

function tableifyBillingAccounts() {
    data = tableify($("#addText"), $("#addTable"), ["billing_account_number"]);
    $("#tableify").addClass("disabled");
    $("#addAll").removeClass("disabled");
}

function clearTable() {
    data = undefined;
    $("#addText").val("");
    $("#addText").show();
    $("#addTable").empty();
    $("#tableify").removeClass("disabled");
    $("#addAll").addClass("disabled");
}

function loadBlockedBillingAccounts() {
    customersTableRelations = $("#showBlockedBilling").DataTable({
        "iDisplayLength": 20,
        "sAjaxSource": "/api/1/web/bill/last/blocked",
        "bProcessing": true,
        "bServerSide": true,
        "bFilter": false,
        "bAutoWidth": false,
        "bLengthChange": false,
        "bDestroy": true,
        "aoColumns": [
            {"sTitle": "Id", "sWidth": "10%"},
            {"sTitle": "Billing No.", "sWidth": "30%"},
            {"sTitle": ""},
        ],
        "bLengthChange": false,
        "fnServerData": function (sSource, aaData, fnCallback) {
            $.ajax({
                "type": "GET",
                "dataType": "json",
                "url": sSource,
                "data": aaData,
                "success": function (response) {
                    var tableViewData = {
                        "iTotalRecords": response.iTotalRecords,
                        "iTotalDisplayRecords": response.iTotalDisplayRecords,
                        "aaData": []
                    };

                    response.data.forEach(function (item) {
                        tableViewData.aaData.push([
                            getText(item.id),
                            getText(item.billing_account_number),
                            ""]);
                    });
                    fnCallback(tableViewData);
                },
                "error": function (xhr, status, error) {
                    showToast("danger", "Loading error");
                }
            });
        },
        "fnInitComplete": function (oSettings, json) {
            //$("#spinContainer").hide();
            //$("#archivesContainer").show();
        },
        "fnCreatedRow": function (nRow, aData, iDataIndex) {
        },
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        }
    });
}

function addBlockedAccounts(data) {
    $(".username")[0].innerHTML = getCookie("cmsuser");
    BootstrapDialog.show({
        title: 'Last bill',
        message: 'Do you want to block displaying of the last bill for ' + data.length + ' billing accounts?',
        buttons: [{
            label: 'Add',
            action: function (dialog) {
                dialog.close();
                ajax("PUT", "/api/1/web/bill/last/blocked/add/", {
                    data: JSON.stringify(data)
                }, true, function (data) {
                    if (data.addedCount > 0 || data.skippedCount > 0) {
                        if (data.addedCount > 0) showToast("success", "Blocked " + data.addedCount + " account(s)");
                        if (data.skippedCount > 0) showToast("warning", "Ignored " + data.skippedCount + " account(s)");
                    } else {
                        showToast("warning", "No actions were done");
                    }
                    clearTable();
                    loadBlockedBillingAccounts();
                }, function () {
                    // do nothing
                }, showToast);
            }
        }, {
            label: 'Cancel',
            action: function (dialog) {
                dialog.close();
            }
        }]
    });
}

function clearAllBillingAccounts(data) {
    $(".username")[0].innerHTML = getCookie("cmsuser");
    BootstrapDialog.show({
        title: 'Last bill',
        message: 'Do you want to remove all blocked billing accounts?',
        buttons: [{
            label: 'Add',
            action: function (dialog) {
                dialog.close();
                ajax("DELETE", "/api/1/web/bill/last/blocked/", {
                    data: JSON.stringify(data)
                }, true, function (data) {
                    if (data.deletedCount > 0) {
                        showToast("success", "Removed " + data.deletedCount + " account(s)");
                    } else {
                        showToast("warning", "No actions were done");
                    }
                    clearTable();
                    loadBlockedBillingAccounts();
                }, function () {
                    // do nothing
                }, showToast);
            }
        }, {
            label: 'Cancel',
            action: function (dialog) {
                dialog.close();
            }
        }]
    });
}

function updateProgressIndicator(visible, status, percentage1, percentage2) {
    if (visible) {
        if (!$.trim($('#broadcastingContainer').html()).length) {
            $("#broadcastingContainer").append(
                '<div><span id="broadcastingStatus">' + status + '</span></div>' +
                '<div class="progress progress-striped active">' +
                '<div class="progress-bar progress-bar-warning" id="broadcastingProgress1" style="width:' + (percentage1 / 2) + '%">' +
                '</div>' +
                '<div class="progress-bar progress-bar-success" id="broadcastingProgress2" style="width:' + (percentage2 / 2) + '%">' +
                '</div>' +
                '</div>'
            );
        } else {
            $('#broadcastingStatus')[0].innerHTML = status;
            $('#broadcastingProgress1')[0].setAttribute("style", "width:" + (percentage1 / 2) + "%");
            $('#broadcastingProgress2')[0].setAttribute("style", "width:" + (percentage2 / 2) + "%");
        }
    } else {
        $("#broadcastingContainer")[0].innerHTML = "";
    }
}

function startUpdatingBroadcastingStatus() {
    $.ajax({
        "type": "GET",
        "dataType": "json",
        "url": "/api/1/web/bill/broadcasting/status",
        "success": function (response) {
            if (response.broadcasting) {
                var loaded = parseInt(response.loadedCount);
                var total = parseInt(response.totalCount);
                var emails = parseInt(response.emailsIgnoredCount) + parseInt(response.emailsSentCount);

                if (total > 0) {
                    var loadedPercentage = Math.round((loaded * 100) / total);
                    var emailsPercentage = Math.round((emails * 100) / total);
                    var label = "Loading... (profile loaded " + loadedPercentage + "%, emails sent "
                        + emailsPercentage + "%)";
                    updateProgressIndicator(true, label, loadedPercentage, emailsPercentage);

                    if (broadcastingStarted) {
                        broadcastingStarted = false;
                        showToast("success", total + " valid PDF file(s) found");
                    }
                }
            } else {
                if (broadcastingInProgress) {
                    var label = "Completed  (profiles loaded 100%, emails sent 100%)";
                    updateProgressIndicator(true, label, 100, 100);
                    reloadTablesContent();

                    setTimeout(function () {
                        updateProgressIndicator(false);
                    }, 3000);

                    if (response.totalCount) {
                        showToast("success", response.emailsSentCount + " PDF file(s) sent ("
                            + response.emailsSentCount + " out of " + response.totalCount + ")", 8000);
                        if (response.invalidCount > 0) {
                            showToast("danger", response.invalidCount + " account error(s) (not found or invalid)", 8000);
                        }
                        if (response.emailsIgnoredCount > 0) {
                            showToast("warning", response.emailsIgnoredCount + " email(s) ignored (previously sent)", 8000);
                        }
                    }
                } else {
                    updateProgressIndicator(false);
                }
            }
            clearTimeout(broadcastingTimeout);
            broadcastingInProgress = response.broadcasting;
            broadcastingTimeout = setTimeout(startUpdatingBroadcastingStatus, broadcastingInProgress ? 1000 : 5000);
        },
        "error": function (xhr, status, error) {
            showToast("warning", "Status update failed");
            clearTimeout(broadcastingTimeout);
            broadcastingTimeout = setTimeout(startUpdatingBroadcastingStatus, 5000)
        }
    });
}

function reloadTablesContent() {
    if ($("#archivesContainer").is(":visible")) {
        archivesTableRelations.fnDraw(false);
    }
    if ($("#billsContainer").is(":visible")) {
        billsTableRelations.fnDraw(false);
    }
}

function loadArchives(input, type) {
    $(".username")[0].innerHTML = getCookie("cmsuser");
    $("#archivesContainer").hide();
    $("#billsContainer").hide();
    $("#spinContainer").show();

    startUpdatingBroadcastingStatus();

    archivesTableRelations = $("#showArchives").DataTable({
        "iDisplayLength": 5,
        "sAjaxSource": "/api/1/web/bill/archive?type=" + type,
        "bProcessing": true,
        "bServerSide": true,
        "bFilter": false,
        "bAutoWidth": false,
        "aoColumns": [
            {"sTitle": "Created", "sWidth": "11%"},
            {"sTitle": "Month", "sWidth": "11%"},
            {"sTitle": "Files", "sWidth": "11%"},
            {"sTitle": "Errors"},
            {"sTitle": "Broadcast", "sWidth": "13%"},
        ],
        "bLengthChange": false,
        "fnServerData": function (sSource, aaData, fnCallback) {
            $.ajax({
                "type": "GET",
                "dataType": "json",
                "url": sSource,
                "data": aaData,
                "success": function (response) {
                    var tableViewData = {
                        "iTotalRecords": response.iTotalRecords,
                        "iTotalDisplayRecords": response.iTotalDisplayRecords,
                        "aaData": []
                    };

                    response.data.forEach(function (item) {
                        var broadcast = (response.write) ? "<button type='button' class='btn btn-white btn-sm' " +
                            "style='border-style:none;'  onClick='broadcastAllBillsList(\"" + type + "/" + item.name + "\");'>" +
                            "<i class='fa fa-envelope'></i>  Send All</button>" : "";
                        tableViewData.aaData.push([
                            getText(item.name),
                            getText(item.month),
                            getText(item.countValid),
                            getText(item.countInvalid),
                            broadcast]);
                    });
                    fnCallback(tableViewData);
                },
                "error": function (xhr, status, error) {
                    showToast("danger", "Loading error");
                }
            });
        },
        "fnInitComplete": function (oSettings, json) {
            $("#spinContainer").hide();
            $("#archivesContainer").show();
        },
        "fnCreatedRow": function (nRow, aData, iDataIndex) {
        },
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            $(nRow).find('td').each(function (column, td) {
                if (column < 4) {
                    $(td).attr('data-type', type);
                    $(td).attr('data-id', aData[0]);
                }
            });

            // Row click
            $(nRow).on('click', function () {
            });

            // Cell click
            $('td', nRow).on('click', function () {
                if ($(this).attr("data-id")) {
                    var folder = $(this).attr("data-type") + '/' + $(this).attr("data-id");
                    loadBills(null, folder);
                }
            });
        }
    });
}

function loadBills(input, folder) {
    $(".username")[0].innerHTML = getCookie("cmsuser");
    $("#spinContainer").show();
    $("#billsContainer").hide();

    billsTableRelations = $("#showBills").DataTable({
        "iDisplayLength": 10,
        "sAjaxSource": "/api/1/web/bill/list?folder=" + folder,
        "bProcessing": true,
        "bServerSide": true,
        "bFilter": false,
        "bLengthChange": false,
        "bDestroy": true,
        "aoColumns": [
            {"sTitle": "Billing Account", "sWidth": "8%"},
            {"sTitle": "User Name", "sWidth": "14%"},
            {"sTitle": "User Phone", "sWidth": "10%"},
            {"sTitle": "User Email", "sWidth": "15%"},
            {"sTitle": "Password", "sWidth": "8%"},
            {"sTitle": "PDF File"},
            {"sTitle": "Sent At", "sWidth": "15%"},
            {"sTitle": "(Re)send", "sWidth": "10%"},
        ],
        "bAutoWidth": false,
        "fnServerData": function (sSource, aaData, fnCallback) {
            $.ajax({
                "type": "GET",
                "dataType": "json",
                "url": sSource,
                "data": aaData,
                "success": function (response) {
                    var tableViewData = {
                        "iTotalRecords": response.iTotalRecords,
                        "iTotalDisplayRecords": response.iTotalDisplayRecords,
                        "aaData": []
                    };

                    response.data.forEach(function (item) {
                        var sendButton = (response.write) ? "<button type='button' class='btn btn-white btn-sm' " +
                            "style='border-style:none;'  onClick='sendSingleBill(\"" + folder + "\", \"" + item.pdf + "\");'>" +
                            "<i class='fa fa-envelope-o'></i>  " + (item.sentCount > 0 ? "Resend" : "Send") + "</button>" : "";
                        var fileLink = '<a target="_blank" href="' + item.url + '">' + item.pdf + '</a>';
                        var sentAtDate = getDateTime(item.sentAt);
                        var sentAtText = sentAtDate + (item.sentCount > 1 ? " (" + item.sentCount + ")" : "");

                        tableViewData.aaData.push([
                            getText(item.billingAccount),
                            getText(item.name),
                            getText(item.number),
                            getText(item.email),
                            getText(item.pdfpassword),
                            fileLink,
                            sentAtText,
                            sendButton]);
                    });

                    fnCallback(tableViewData);
                },
                "error": function (xhr, status, error) {
                    showToast("danger", "Loading error");
                }
            });
        },
        "fnInitComplete": function (oSettings, json) {
            $("#spinContainer").hide();
            $("#billsContainer").show();
            $("#billsContainerHeader")[0].innerHTML = "Bills - " + folder;
        },
        "fnCreatedRow": function (nRow, aData, iDataIndex) {
        },
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        }
    });
}

function broadcastAllBillsList(folder) {
    $(".username")[0].innerHTML = getCookie("cmsuser");

    var failedNotification = folder.indexOf("live_failed") >= 0;
    var negativeNotification = folder.indexOf("live_negative") >= 0;
    var partialNotification = folder.indexOf("live_partially") >= 0;
    BootstrapDialog.show({
        title: 'Send PDF bills',
        message: 'Do you want to send ' + (negativeNotification ? "BILL PAYMENT NEGATIVE" : (failedNotification ? '"BILL PAYMENT FAILURE"'
            : (partialNotification ? '"BILL PAYMENT PARTIAL"' : '"BILL PAYMENT SUCCESS"'))) +
        ' notifications along with PDF bills to everyone? Please note that already sent PDF files ' +
        'will be ignored and excluded from broadcasting. If you need to resend please do it manually one-by-one.',
        buttons: [{
            label: 'Send',
            action: function (dialog) {
                dialog.close();

                $.ajax({
                    "type": "POST",
                    "dataType": "json",
                    "url": "/api/1/web/bill/send/all?folder=" + folder,
                    "success": function (response) {
                        clearTimeout(broadcastingTimeout);
                        broadcastingTimeout = setTimeout(startUpdatingBroadcastingStatus, 100);
                        broadcastingInProgress = true;
                        broadcastingStarted = true;
                    },
                    "error": function (xhr, status, error) {
                        showToast("danger", "Sending error");
                    }
                });
            }
        }, {
            label: 'Cancel',
            action: function (dialog) {
                dialog.close();
            }
        }]
    });
}

function sendSingleBill(folder, pdf) {
    $(".username")[0].innerHTML = getCookie("cmsuser");
    var parameters = "folder=" + folder + "&pdf=" + pdf;

    var failedNotification = folder.indexOf("live_failed") >= 0;
    var negativeNotification = folder.indexOf("live_negative") >= 0;
    BootstrapDialog.show({
        title: 'Send PDF bills',
        message: 'Do you want to send ' + (negativeNotification ? "BILL PAYMENT NEGATIVE"
            : (failedNotification ? '"BILL PAYMENT FAILURE"' : '"BILL PAYMENT SUCCESS"'))
        + ' notification along with PDF bill to the customer? Make sure you do not send the same bill only once.',
        buttons: [{
            label: 'Send',
            action: function (dialog) {
                dialog.close();

                $.ajax({
                    "type": "POST",
                    "dataType": "json",
                    "url": "/api/1/web/bill/send/single",
                    "data": parameters,
                    "success": function (response) {
                        if (response.billingAccount) {
                            showToast('success', "Bill has been " + (response.resent ? "resent" : "send") + " to '"
                                + response.name + "' (" + response.number + " / " + response.billingAccount + ")");
                        } else {
                            showToast('warning', "Bill has NOT been sent (account not found or invalid)");
                        }
                        if (billsTableRelations) {
                            billsTableRelations.fnDraw(false);
                        }
                    },
                    "error": function (xhr, status, error) {
                        showToast("danger", "Sending error");
                    }
                });
            }
        }, {
            label: 'Cancel',
            action: function (dialog) {
                dialog.close();
            }
        }]
    });
}

function loadBillLogs(input) {
    $(".username")[0].innerHTML = getCookie("cmsuser");
    var parameters = "";
    var query = ((input) ? "?search=" + input.search.value : "");
    ajax("GET", "/api/1/web/bill/logs" + query, parameters, true, function (data) {
        if (data && (data.code == 0)) {
            showBillLogs(data.list);
        } else {
            showToast("danger", "Loading error");
        }
    });
    return false;
}

// UI functions

function showBillLogs(logs) {
    $("#showLogs tbody").empty();
    logs.forEach(function (log) {
        var date = getDateTime(log.ts);
        var status = log.status;
        var number = log.number;
        var email = log.email;
        var filename = log.filename;

        delete log._id;
        delete log.ts;
        delete log.status;
        delete log.number;
        delete log.email;
        delete log.filename;

        $("#showLogs > tbody:last-child").append("<TR>" +
            "<TD>" + date + "</TD>" +
            "<TD>" + status + "</TD>" +
            "<TD>" + number + "</TD>" +
            "<TD>" + email + "</TD>" +
            "<TD>" + filename + "</TD>" +
            "<TD>" + JSON.stringify(log) + "</TD>" +
            "</TR>");
        });
}

function initManageBills() {
    $("#batch_sync").click(function (){
        date = $("#batch_sync_date").val();
        status = $("#batch_sync_status").val();
        $.ajax({
            "type": "POST",
            "dataType": "json",
            "url": "/api/1/management/billing/sync",
            "data": '{"date":"' + date  + '","status":"' + status  + '"}',
            "success": function (response) {
                console.log(response);
            },
            "error": function (result) {
                showToast("danger", "Loading error");
                console.log(result);
            }
        });
    });
    $("#file_remove").click(function (){
        date = $("#billRemove_date").val();
        status = $("#billRemove_status").val();
        filename = $("#filename").val();
        console.log(date+status+filename);
        $.ajax({
            "type": "POST",
            "dataType": "json",
            "url": "/api/1/management/billing/delete",
            "data": '{"filename":"'+ filename +'","date":"' + date  + '","status":"' + status  + '"}',
            "success": function (response) {
                console.log(response);
            },
            "error": function (result) {
                showToast("danger", "Loading error");
                console.log(result);
            }
        });
    });
}
