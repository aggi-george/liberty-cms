function init() {
    $(".username")[0].innerHTML = getCookie("cmsuser");
    getRecordings();
    getRequests("suspend");
    getRequests("terminate");
}

function getRequests(type) {
    var parameters = { };
    ajax("GET", "/api/1/web/inbound/get/" + type, parameters, true, function (data) {
        if (data && (data.code == 0)) {
            showRequests(data.type, data.list, data.write);
        } else {
            showToast("warning", "Listing Unavailable");
        }
    });
}

function tagRecording(type, filename) {
    var parameters = { "filename" : filename };
    ajax("PUT", "/api/1/web/inbound/tag/recordings", parameters, true, function (data) {
        if (data && (data.code == 0)) {
            document.getElementById("owner_" + data.filename).innerHTML = data.owner;   // cannot use jquery "@" special char
            showToast("success", "Tag OK");
        } else if (data && (data.code == 1)) {
            document.getElementById("owner_" + data.filename).innerHTML = data.owner;   // cannot use jquery "@" special char
            document.getElementById("audio_" + data.filename).controls = false;   // cannot use jquery "@" special char
            showToast("warning", "Already tagged by other Agents");
        } else {
// gid fix            document.getElementById("audio_" + data.filename).load();   // cannot use jquery "@" special char
            showToast("warning", "Tag Failed");
        }
    });
    return false;
}

function getRecordings() {
    var parameters = { };
    ajax("GET", "/api/1/web/inbound/get/recordings", parameters, true, function (data) {
        if (data && (data.code == 0)) {
            showRecordings(data.path, data.list, data.write);
        } else {
            showToast("warning", "Recordings Unavailable");
        }
    });
    return false;
}

function showRecordings(path, list, write) {
    if ($("#showInbound").dataTableSettings.length > 0) $("#showInbound").DataTable().fnDestroy();
    $("#showInbound tbody").empty();
    if (list) {
        list.forEach(function (item) {
            var owner = (item.owner) ? item.owner : "";
            var controls = (!owner) ? "controls" :
                                ((write > 1) || (owner == getCookie("cmsuser"))) ? "controls" : "";
            $("#showInbound tbody").append("<TR>" +
                "<TD>" + item.src + "</TD>" +
                "<TD>" + item.dst + "</TD>" +
                "<TD>" + item.cat + "</TD>" +
                "<TD>" + getDateTime(item.date) + "</TD>" +
                "<TD>" + s2time(item.duration) + "</TD>" +
                "<TD><AUDIO " + controls + " preload=none id='audio_" + item.filename + "' onplay='tagRecording(\"inbound\",\"" + item.filename + "\");'>" +
                "<SOURCE type=audio/wav id='source_" + item.filename + "' src=" + path +"?id=" + item.filename + ">" +
                "</AUDIO></TD>" +
                "<TD id='owner_" + item.filename + "' title='" + item.filename + "'>" + owner + "</TD>" +
                "</TR>");
        });
        $("#showInbound").DataTable({ "aaSorting": [[ 3, 'desc' ]] });
    }
}

function showRequests(type, list, write) {
    if ($("#show" + type).dataTableSettings.length > 0) $("#show" + type).DataTable().fnDestroy();
    $("#show" + type + " tbody").empty();
    if (list) {
        list.forEach(function (item) {
            $("#show" + type + " tbody").append("<TR>" +
                "<TD>" + getDateTime(item.ts) + "</TD>" +
                "<TD>" + item.number + "</TD>" +
                "<TD>" + item.notification.ANI + "</TD>" +
                "<TD>" + item.notification.DNIS + "</TD>" +
                "<TD>" + item.notification.DOB + "</TD>" +
                "<TD>" + item.notification.NRIC + "</TD>" +
            "</TR>");
        });
        $("#show" + type).DataTable();
    }
}
