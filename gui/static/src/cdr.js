
function init(type) {
    $(".username")[0].innerHTML = getCookie("cmsuser");
    if (type && (type == "elitecore" || type == "reassurance")) {
        var now = new Date();
        $("#searchForm :input[name=start]").val(getDateTime(new Date(now.getFullYear(), now.getMonth(), now.getDate())));
        $("#searchForm :input[name=end]").val(getDateTime(new Date(now.getFullYear(), now.getMonth(), now.getDate() + 1 , 0, 0, 0, -1)));
        $("#searchForm :input[name=start]").datetimepicker({
            "format" : "yyyy-mm-dd hh:ii:ss",
            "autoclose" : true
        });
        $("#searchForm :input[name=end]").datetimepicker({
            "format" : "yyyy-mm-dd hh:ii:ss",
            "autoclose" : true
        });
        ajax("GET", "/api/1/web/cdr/get/ec?getRGID=1", undefined, true, function (data) {
            if (data && data.code == 0) {
                if (data.result) data.result.forEach(function(item) {
                    $("#searchForm :input[name=rgid]").append($("<option></option>").attr("value", item.id).text(item.label));
                });
            }
        });
    }
}

function m1Search(form) {
    var imsi = form.imsi.value;
    var mobile = form.mobile.value;
    var month = form.month.value;
    var year = form.year.value;
    var usage_type = form.usage_type.value;
    form.submit.disabled = true;
    //showSummary();
    $("#showSearch").DataTable({
        "iDisplayLength": 10,
        "sAjaxSource": "/api/1/web/cdr/get/m1?imsi=" + imsi + "&mobile=" + mobile + "&usage_type="  + usage_type  + "&month=" + month + "&year=" + year + "queryType=entries",
        "bProcessing": true,
        "bServerSide": true,
        "bFilter": false,
        "bLengthChange": false,
        "bDestroy": true,
        "bAutoWidth": true,
        "aoColumns": [
            {"sTitle": "Record Type", "sWidth": "10%", "bSortable": false},
            {"sTitle": "PLMN Desc", "sWidth": "10%", "bSortable": false},
            {"sTitle": "Sequence Number", "sWidth": "10%", "bSortable": false},
            {"sTitle": "IMSI", "sWidth": "10%", "bSortable": false},
            {"sTitle": "Transaction Date", "sWidth": "10%", "bSortable": true},
            {"sTitle": "Type ID Usage", "sWidth": "10%", "bSortable": false},
            {"sTitle": "Primary Units", "sWidth": "10%", "bSortable": false},
            {"sTitle": "Accounted Units", "sWidth": "10%", "bSortable": false},
            {"sTitle": "Point Origin", "sWidth": "10%", "bSortable": false},
            {"sTitle": "Cost", "sWidth": "10%", "bSortable": false},
        ],
        "bLengthChange": false,
        "fnServerData": function (sSource, aaData, fnCallback) {
            $.ajax({
                "type": "GET",
                "dataType": "json",
                "url": sSource,
                "data": aaData,
                "success": function (response) {
                    if ($("#showSummary").dataTableSettings.length > 0) $("#showSummary").DataTable().fnDestroy();
                    $("#showSummary tbody").empty();
                    $(".fa-spin").hide();
                    if ( response[2] && (response[2].code == 0) ) {
                        response[2].list.forEach(function (item) {	
                            typeIdDef = convertTypeIdUsg(item.typeIdUsg);
                            $("#showSummary").append("<TR>" +
                                "<TD>" + item.typeIdUsg + " (" + typeIdDef + ") </TD>" +
                                "<TD>" + item.totalUnits+ "</TD>" +
                                "<TD>" + item.totalCost + "</TD></TR>");
                            });
                        $("#showSummary").DataTable();
                    }
                    if(response[1] && response[1].total && response[1].total > 0){
                        var tableViewData = {
                            "iTotalRecords": response[1].total,
                            "iTotalDisplayRecords": response[1].total,
                            "aaData": []
                        };
                        form.submit.disabled = false;
                        response[1].list.forEach(function (item) {
                            tableViewData.aaData.push([
                                item.record_type,
                                item.plmn_desc,
                                item.tap_seq_num,
                                item.imsi,
                                item.transaction_date,
                                item.type_id_usg,
                                item.primary_units,
                                item.accounted_duration,
                                item.point_origin,
                                item.cost
                            ]);
                        });
                    } else{
                        var tableViewData = {
                            "iTotalRecords": 0,
                            "iTotalDisplayRecords": 0,
                            "aaData": []
                        };
                    }
                    fnCallback(tableViewData);
                },
                "error": function (xhr, status, error) {
                    showToast("danger", "Loading error");
                }
            });
        },
        "fnInitComplete": function (oSettings, json) {
        },
        "fnCreatedRow": function (nRow, aData, iDataIndex) {
        },
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            $(nRow).on('click', function () {
            });
            $('td', nRow).on('click', function () {
            });
        },
    });
    return false;
}

function convertTypeIdUsg(typeIdUsg){
    var value = "";
    switch (typeIdUsg) {
        case "3300":
            value = "Outgoing Calls to Special Number";
            break;
        case "3600":
            value = "Outgoing Calls to Local";
            break;
        case "3603":
            value = "Outgoing Calls - IDD to SG";
            break;
        case "3604":
            value = "Outgoing Calls - IDD to International";
            break;
        case "3630":
            value = "Outgoing Video to Local";
            break;
        case "3633":
            value = "Outgoing Video - IDD to SG";
            break;
        case "3634":
            value = "Outgoing Video - IDD to International";
            break;
        case "3610":
            value = "SMS Send";
            break;
        case "3366":
            value = "SMS Receive";
            break;
        case "6290":
            value = "Data";
            break;
        case "6470":
            value = "Data";
            break;
        case "6471":
            value = "Data";
            break;
        case "6472":
            value = "Data";
            break;
        case "3606":
            value = "Incoming Call";
            break;
        case "3603":
            value = "Incoming Video";
            break;
    }
    return(value);
}

function ecDownload(form, url, columns) {
    form.download.disabled = true;
    $(".fa-spin").show();
    ajax("GET", url, undefined, true, function (data) {
        if (data && data.code == 0) {
            var dataString = columns.map(function(o) { return o.sTitle }).toString() + "\n";
            if (data.result && data.result.list) data.result.list.forEach(function (item) {
                item.length = columns.length;
                dataString += item.toString() + "\n";
            });
            downloadCSV(dataString, "download.csv", "text/csv");
        }
        form.download.disabled = false;
        $(".fa-spin").hide();
    });
}

function ecForm(form, download) {
    var parameters = "mobile=" + form.mobile.value +
        "&serviceInstanceNumber=" + form.sin.value +
        "&type=" + form.type.value +
        "&rgid=" + form.rgid.value +
        "&start=" + form.start.value +
        "&end=" + form.end.value;
    var url = "/api/1/web/cdr/get/ec?" + parameters;
    var type = form.type.value.split('_')[1];
    var columns = (type == "DATA") ? [
            {"sTitle": "Call Start", "sWidth": "10%", "bSortable": false},
            {"sTitle": "Call End", "sWidth": "10%", "bSortable": false},
            {"sTitle": "Package", "sWidth": "10%", "bSortable": false},
            {"sTitle": "Accounted Cost", "sWidth": "10%", "bSortable": false},
            {"sTitle": "Accounted Data", "sWidth": "10%", "bSortable": false},
            {"sTitle": "Session Data", "sWidth": "10%", "bSortable": false},
            {"sTitle": "Usage Type", "sWidth": "10%", "bSortable": false},
            {"sTitle": "RGID", "sWidth": "10%", "bSortable": false},
    ] : (type == "VOICE") ? [
            {"sTitle": "Call Start", "sWidth": "10%", "bSortable": false},
            {"sTitle": "Call End", "sWidth": "10%", "bSortable": false},
            {"sTitle": "Package", "sWidth": "10%", "bSortable": false},
            {"sTitle": "Accounted Cost", "sWidth": "10%", "bSortable": false},
            {"sTitle": "Accounted Time", "sWidth": "10%", "bSortable": false},
            {"sTitle": "Calling Station ID", "sWidth": "10%", "bSortable": false},
            {"sTitle": "Called Station ID", "sWidth": "10%", "bSortable": false},
    ] : [
            {"sTitle": "Timestamp", "sWidth": "10%", "bSortable": false},
            {"sTitle": "Package", "sWidth": "10%", "bSortable": false},
            {"sTitle": "Accounted Cost", "sWidth": "10%", "bSortable": false},
            {"sTitle": "Calling Station ID", "sWidth": "10%", "bSortable": false},
            {"sTitle": "Called Station ID", "sWidth": "10%", "bSortable": false},
    ];
    if (!download) formSearch(form, type, url, columns, ()=>{ ecForm(form, true) });
    else ecDownload(form, url, columns);
}

function reassuranceDownload(form, url, columns) {
    form.download.disabled = true;
    $(".fa-spin").show();
    ajax("GET", url, undefined, true, function (data) {
        if (data && data.code == 0) {
            const dataColumns = columns.map(function(o) { return o.sTitle });
            let dataString = dataColumns.toString() + "\n";
            if (data.result && data.result.list) data.result.list.forEach(function (item) {
                dataColumns.forEach((column) => {
                    dataString += `${item[column]},`;
                });
                dataString = dataString.slice(0,-1) + "\n";
            });
            downloadCSV(dataString, "download.csv", "text/csv");
        }
        form.download.disabled = false;
        $(".fa-spin").hide();
    });
}

function reassuranceForm(form, download) {
    var parameters = "mobile=" + form.mobile.value +
        "&session=" + form.session.value +
        "&request=" + form.request.value +
        "&type=" + form.type.value +
        "&start=" + new Date(form.start.value).toISOString() +
        "&end=" + new Date(form.end.value).toISOString();
    var url = "/api/1/web/cdr/get/reassurance?" + parameters;
    var type = form.type.value;
    var columns = (type == "DATA") ? [
            {"sTitle": "time", "sWidth": "10%", "bSortable": false},
            {"sTitle": "User-Name", "sWidth": "10%", "bSortable": false},
            {"sTitle": "Session-Id", "sWidth": "10%", "bSortable": false},
            {"sTitle": "CC-Request-Number", "sWidth": "10%", "bSortable": false},
            {"sTitle": "3GPP-SGSN-MCC-MNC", "sWidth": "10%", "bSortable": false},
            {"sTitle": "CC-Total-Octets", "sWidth": "10%", "bSortable": false},
    ] : (type == "VOICE") ? [
            {"sTitle": "time", "sWidth": "10%", "bSortable": false},
            {"sTitle": "Calling-Party-Address", "sWidth": "10%", "bSortable": false},
            {"sTitle": "Called-Party-Address", "sWidth": "10%", "bSortable": false},
            {"sTitle": "Session-Id", "sWidth": "10%", "bSortable": false},
            {"sTitle": "CC-Request-Number", "sWidth": "10%", "bSortable": false},
            {"sTitle": "CC-Request-Type", "sWidth": "10%", "bSortable": false},
            {"sTitle": "EC-VLR-Address", "sWidth": "10%", "bSortable": false},
            {"sTitle": "CC-Time", "sWidth": "10%", "bSortable": false},
    ] : [
            {"sTitle": "time", "sWidth": "10%", "bSortable": false},
            {"sTitle": "Calling-Party-Address", "sWidth": "10%", "bSortable": false},
            {"sTitle": "Called-Party-Address", "sWidth": "10%", "bSortable": false},
            {"sTitle": "Session-Id", "sWidth": "10%", "bSortable": false},
            {"sTitle": "CC-Request-Number", "sWidth": "10%", "bSortable": false},
            {"sTitle": "EC-VLR-Address", "sWidth": "10%", "bSortable": false},
    ];
    if (!download) formSearch(form, type, url, columns, ()=>{ reassuranceForm(form, true) });
    else reassuranceDownload(form, url, columns);
}

function formSearch(form, type, url, columns, dlFunc) {
    $("#showSearch thead").empty();
    $("#showSearch tbody").empty();
    $("#showSummary tbody").empty();
    form.search.disabled = true;
    $(".fa-spin").show();
    $("#showSearch").DataTable({
        "iDisplayLength": 200,
        "sAjaxSource": url,
        "bProcessing": true,
        "bServerSide": true,
        "bFilter": false,
        "bLengthChange": false,
        "bDestroy": true,
        "bAutoWidth": true,
        "aoColumns": columns,
        "bLengthChange": false,
        "fnServerData": function (sSource, aaData, fnCallback) {
            $.ajax({
                "type": "GET",
                "dataType": "json",
                "url": sSource,
                "data": aaData,
                "success": function (response) {
                    form.search.disabled = false;
                    $(".fa-spin").hide();
                    var tableViewData = {
                        "iTotalRecords": 0,
                        "iTotalDisplayRecords": 0,
                        "aaData": []
                    };
                    if (!response || !response.result || !response.result.list || !response.result.summary) {
                        return fnCallback(tableViewData);
                    }
                    var totalRows = (response.result.summary.length > 0) ?
                        response.result.summary.map((o)=>{return o.TOTAL}).reduce((a,b)=>{return a+b}) : 0;
                    if (totalRows>0) {
                        form.download.addEventListener("click", dlFunc);
                        form.download.disabled = false;
                    } else {
                        form.download.removeEventListener("click", dlFunc);
                        form.download.disabled = true;
                    }
                    tableViewData.iTotalRecords = totalRows;
                    tableViewData.iTotalDisplayRecords = totalRows;
                    response.result.list.forEach(function (item) {
                        $("#showSummary tbody").empty();
                        if (type == "DATA") {
                            if (item && item[0]) tableViewData.aaData.push([
                                getDateTime(item[0].replace('Z','')),
                                getDateTime(item[1].replace('Z','')),
                                item[9],
                                item[2],
                                item[3],
                                (Math.round(item[4] * 100)/100) + " MB",
                                item[5],
                                item[6],
                            ]);
                            else tableViewData.aaData.push([
                                getDateTime(item.time),
                                item['User-Name'],
                                item['Session-Id'],
                                item['CC-Request-Number'],
                                item['3GPP-SGSN-MCC-MNC'],
                                item['CC-Total-Octets'],
                            ]);
                            response.result.summary.forEach(function (row) {
                                if (!row.RATINGGROUPID) row.RATINGGROUPID = '';
                                if (!row.ACCOUNTEDCOST) row.ACCOUNTEDCOST = 0;
                                $("#showSummary tbody").append("<TR>" +
                                    "<TD>" + row.RATINGGROUPID + "</TD>" +
                                    "<TD>" + row.TOTAL + "</TD>" +
                                    "<TD>" + (Math.round(row.SESSIONDATA*100)/100) + " GB</TD>" +
                                    "<TD>S$ " + (Math.round(row.ACCOUNTEDCOST*100)/100) + "</TD>" +
                                "</TR>");
                            });
                        } else if (type == "VOICE") {
                            if (item && item[0]) tableViewData.aaData.push([
                                getDateTime(item[0].replace('Z','')),
                                getDateTime(item[1].replace('Z','')),
                                item[9],
                                item[2],
                                s2time(item[3]),
                                item[7],
                                item[8],
                            ]);
                            else tableViewData.aaData.push([
                                getDateTime(item.time),
                                item['Calling-Party-Address'],
                                item['Called-Party-Address'],
                                item['Session-Id'],
                                item['CC-Request-Number'],
                                item['CC-Request-Type'],
                                item['EC-VLR-Address'],
                                item['CC-Time'],
                            ]);
                            response.result.summary.forEach(function (row) {
                                if (!row.ACCOUNTEDCOST) row.ACCOUNTEDCOST = 0;
                                $("#showSummary tbody").append("<TR>" +
                                    "<TD></TD>" +
                                    "<TD>" + row.TOTAL + "</TD>" +
                                    "<TD>" + s2time(row.ACCOUNTEDTIME) + "</TD>" +
                                    "<TD>S$ " + (Math.round(row.ACCOUNTEDCOST*100)/100) + "</TD>" +
                                "</TR>");
                            });
                        } else {
                            if (item && item[0]) tableViewData.aaData.push([
                                getDateTime(item[0].replace('Z','')),
                                item[9],
                                item[2],
                                item[7],
                                item[8],
                            ]);
                            else tableViewData.aaData.push([
                                getDateTime(item.time),
                                item['Calling-Party-Address'],
                                item['Called-Party-Address'],
                                item['Session-Id'],
                                item['CC-Request-Number'],
                                item['EC-VLR-Address'],
                            ]);
                            response.result.summary.forEach(function (row) {
                                if (!row.ACCOUNTEDCOST) row.ACCOUNTEDCOST = 0;
                                $("#showSummary tbody").append("<TR>" +
                                    "<TD></TD>" +
                                    "<TD>" + row.TOTAL + "</TD>" +
                                    "<TD>" + row.TOTAL + "</TD>" +
                                    "<TD>S$ " + (Math.round(row.ACCOUNTEDCOST*100)/100) + "</TD>" +
                                "</TR>");
                            });
                        }
                    });
                    fnCallback(tableViewData);
                },
                "error": function (xhr, status, error) {
                    showToast("danger", "Loading error");
                }
            });
        },
        "fnInitComplete": function (oSettings, json) {
        },
        "fnCreatedRow": function (nRow, aData, iDataIndex) {
        },
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            $(nRow).on('click', function () {
            });
            $('td', nRow).on('click', function () {
            });
        },
    });
    return false;
}

function OCSErrorSearch(form) {
    var msisdn = form.msisdn.value;
    var err = form.err.value;
    var month = form.month.value;
    var year = form.year.value;
    var day = form.day.value;
    var hour_start = form.hour_start.value;
    var hour_end = form.hour_end.value;
    var minute_start = form.minute_start.value;
    var minute_end = form.minute_end.value;
    $("#showSearch").DataTable({
        "iDisplayLength": 10,
        "sAjaxSource": "/api/1/web/cdr/get/ocserrors?msisdn=" + msisdn + "&err=" + err + "&month=" + month + "&year=" + year + "&day=" + day + "&hour_start=" + hour_start + "&minute_start=" + minute_start + "&hour_end=" + hour_end + "&minute_end=" + minute_end,
        "bProcessing": true,
        "bServerSide": true,
        "bFilter": false,
        "bLengthChange": false,
        "bDestroy": true,
        "bAutoWidth": true,
        "aoColumns": [
            {"sTitle": "Event Timestamp", "sWidth": "10%", "bSortable": true},
            {"sTitle": "MSISDN", "sWidth": "10%", "bSortable": false},
            {"sTitle": "Error Code", "sWidth": "10%", "bSortable": false},
            {"sTitle": "Error Description", "sWidth": "10%", "bSortable": false},
            {"sTitle": "Session ID", "sWidth": "10%", "bSortable": false},
            {"sTitle": "MCC/MNC", "sWidth": "10%", "bSortable": false},
        ],
        "bLengthChange": false,
        "fnServerData": function (sSource, aaData, fnCallback) {
            $.ajax({
                "type": "GET",
                "dataType": "json",
                "url": sSource,
                "data": aaData,
                "success": function (response) {
                console.log(response);
                var tableViewData = {
                    "iTotalRecords": response.total,
                    "iTotalDisplayRecords": response.total,
                    "aaData": []
                };
                    form.submit.disabled = false;
                    response.list.forEach(function (item) {
                        tableViewData.aaData.push([
                            item.ets,
                            item.msisdn,
                            item.err,
                            item.errdesc,
                            item.sid,
                            item.gen5
                        ]);
                    });
                    fnCallback(tableViewData);
                },
                "error": function (xhr, status, error) {
                    showToast("danger", "Loading error");
                }
            });
        },
        "fnInitComplete": function (oSettings, json) {
        },
        "fnCreatedRow": function (nRow, aData, iDataIndex) {
        },
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            $(nRow).on('click', function () {
            });
            $('td', nRow).on('click', function () {
            });
        }
    });
    return false;
}

function SCPSearch(form) {
    var aMsisdn = form.aMsisdn.value;
    var bMsisdn = form.bMsisdn.value;
    var month = form.month.value;
    var year = form.year.value;
    var day = form.day.value;
    var hour_start = form.hour_start.value;
    var hour_end = form.hour_end.value;
    var minute_start = form.minute_start.value;
    var minute_end = form.minute_end.value;
    form.submit.disabled = true;
    $("#showSearch").DataTable({
        "iDisplayLength": 10,
        "sAjaxSource": "/api/1/web/cdr/get/scp?aMsisdn=" + aMsisdn + "&bMsisdn=" + bMsisdn + "&month=" + month + "&year=" + year + "&day=" + day + "&hour_start=" + hour_start + "&minute_start=" + minute_start + "&hour_end=" + hour_end + "&minute_end=" + minute_end,
        "bProcessing": true,
        "bServerSide": true,
        "bFilter": false,
        "bLengthChange": false,
        "bDestroy": true,
        "bAutoWidth": true,
        "aoColumns": [
            {"sTitle": "Record Type", "sWidth": "10%", "bSortable": false},
            {"sTitle": "Party A", "sWidth": "10%", "bSortable": false},
            {"sTitle": "Party B", "sWidth": "10%", "bSortable": false},
            {"sTitle": "Transaction Date", "sWidth": "10%", "bSortable": true},
            {"sTitle": "Routing to SIP", "sWidth": "10%", "bSortable": false},
            {"sTitle": "Response Delay", "sWidth": "10%", "bSortable": false},
            {"sTitle": "Total Call Duration", "sWidth": "10%", "bSortable": false},
            {"sTitle": "Call or SMS Status", "sWidth": "10%", "bSortable": false},
            {"sTitle": "Status Description", "sWidth": "10%", "bSortable": false},
        ],
        "bLengthChange": false,
        "fnServerData": function (sSource, aaData, fnCallback) {
            $.ajax({
                "type": "GET",
                "dataType": "json",
                "url": sSource,
                "data": aaData,
                "success": function (response) {
                console.log(response);
                var tableViewData = {
                    "iTotalRecords": response.total,
                    "iTotalDisplayRecords": response.total,
                    "aaData": []
                };
                    form.submit.disabled = false;
                    response.list.forEach(function (item) {
                        tableViewData.aaData.push([
                            item.recordType,
                            item.aMsisdn,
                            item.bMsisdn,
                            item.startDateTime,
                            item.routingToSip,
                            item.responseDelay,
                            item.totalCallDuration,
                            item.callOrSmsStatus,
                            item.statusDescription,
                        ]);
                    });
                    fnCallback(tableViewData);
                },
                "error": function (xhr, status, error) {
                    showToast("danger", "Loading error");
                }
            });
        },
        "fnInitComplete": function (oSettings, json) {
        },
        "fnCreatedRow": function (nRow, aData, iDataIndex) {
        },
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            $(nRow).on('click', function () {
            });
            $('td', nRow).on('click', function () {
            });
        }
    });
    return false;
}

function offlineECSearch(form) {
    var subscriberidentifier = form.subscriberidentifier.value;
    console.log(subscriberidentifier);
    var serviceid = form.serviceid.value;
    console.log(serviceid);
    var month = form.month.value;
    var year = form.year.value;
    var day = form.day.value;
    var hour_start = form.hour_start.value;
    var hour_end = form.hour_end.value;
    var minute_start = form.minute_start.value;
    var minute_end = form.minute_end.value;
    form.submit.disabled = true;
    $("#showSearch").DataTable({
        "iDisplayLength": 10,
        "sAjaxSource": "/api/1/web/cdr/get/offlineeccdrs?subscriberidentifier=" + subscriberidentifier + "&serviceid=" + serviceid + "&month=" + month + "&year=" + year + "&day=" + day + "&hour_start=" + hour_start + "&minute_start=" + minute_start + "&hour_end=" + hour_end + "&minute_end=" + minute_end,
        "bProcessing": true,
        "bServerSide": true,
        "bFilter": false,
        "bLengthChange": false,
        "bDestroy": true,
        "bAutoWidth": true,
        "aoColumns": [
            {"sTitle": "Subscriber Identifier", "sWidth": "10%", "bSortable": false},
            {"sTitle": "Process Date", "sWidth": "10%", "bSortable": true},
            {"sTitle": "Session Time", "sWidth": "10%", "bSortable": false},
            {"sTitle": "Accounted Time", "sWidth": "10%", "bSortable": false},
            {"sTitle": "Accounted Cost", "sWidth": "10%", "bSortable": false},
            {"sTitle": "Session Data", "sWidth": "10%", "bSortable": false},
            {"sTitle": "Accounted Data", "sWidth": "10%", "bSortable": false},
            {"sTitle": "RGID", "sWidth": "10%", "bSortable": false},
            {"sTitle": "Location", "sWidth": "10%", "bSortable": false},
        ],
        "bLengthChange": false,
        "fnServerData": function (sSource, aaData, fnCallback) {
            $.ajax({
                "type": "GET",
                "dataType": "json",
                "url": sSource,
                "data": aaData,
                "success": function (response) {
                console.log(response);
                var tableViewData = {
                    "iTotalRecords": response.total,
                    "iTotalDisplayRecords": response.total,
                    "aaData": []
                };
                    form.submit.disabled = false;
                    response.list.forEach(function (item) {
                        tableViewData.aaData.push([
                            item.subscriberidentifier,
                            item.processdate,
                            item.sessiontime,
                            item.accountedtime,
                            item.accountedcost,
                            item.sessiondatatransfer,
                            item.accounteddatatransfer,
                            item.ratinggroupid,
                            item.general5,
                        ]);
                    });
                    fnCallback(tableViewData);
                },
                "error": function (xhr, status, error) {
                    showToast("danger", "Loading error");
                }
            });
        },
        "fnInitComplete": function (oSettings, json) {
        },
        "fnCreatedRow": function (nRow, aData, iDataIndex) {
        },
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            $(nRow).on('click', function () {
            });
            $('td', nRow).on('click', function () {
            });
        }
    });
    return false;
}
