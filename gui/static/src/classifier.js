var classes = [];
var selectedClassId;

var classTableRelations;
var customersTableRelations;

var selectedClassRow;
var selectedCustomerRow;

var classesMap = {};
var customersMap = {};
var progressInterval;
var progressSendInterval;

function init() {
    loadClasses();
    checkSyncProgress();
    checkSendProgress();
}

function checkSyncProgress() {
    if (progressInterval) clearInterval(progressInterval);
    var loadProgress = () => {
        ajax("GET", "/api/1/web/analytics/classes/resync/progress", {}, true, function (data) {
            var progressText = "";
            if (data.stepsCount >= 0 && data.stepNumber >= 0) {
                if (progressText) progressText += " - ";
                progressText += data.stepNumber + "/" + data.stepsCount;
            }
            if (data.stepProgress >= 0) {
                if (progressText) progressText += " - ";
                progressText += data.stepProgress + "%";
            }
            if (progressText) {
                $("#resyncClasses").show();
                $("#resyncClassesProgress").show();
                $("#resyncClassesProgress").html(getStatusLabel("NEUTRAL", "Sync: " + progressText));
            } else {
                if ($("#resyncClassesProgress").is(":visible")) {
                    reloadCallback();
                }
                $("#resyncClasses").show();
                $("#resyncClassesProgress").hide();
            }
        }, function (code, errorMessage) {
            $("#resyncClasses").hide();
            $("#resyncClassesProgress").show();
        }, function (type, Message) {
            if (type == "danger") {
                $("#resyncClassesProgress").html(getStatusLabel("ERROR", "##Connection Error##"));
            }
        });
    };

    progressInterval = setInterval(loadProgress, 1000);
}

function checkSendProgress() {
    if (progressSendInterval) clearInterval(progressSendInterval);
    var loadProgress = () => {
        if ($("#customersContainer").is(":visible")) {
            ajax("GET", "/api/1/web/analytics/classes/send/notification/progress", {}, true, function (data) {
                var progressText = "";
                if (data.stepsCount >= 0 && data.stepNumber >= 0) {
                    if (progressText) progressText += " - ";
                    progressText += data.stepNumber + "/" + data.stepsCount;
                }
                if (data.stepProgress >= 0) {
                    if (progressText) progressText += " - ";
                    progressText += data.stepProgress + "%";
                }
                if (progressText) {
                    $("#sendNotificationAction").show();
                    $("#downloadReportAction").show();
                    $("#sendNotificationProgress").show();
                    $("#sendNotificationProgress").html(getStatusLabel("NEUTRAL", "Sending: " + progressText));
                } else {
                    if ($("#sendNotificationProgress").is(":visible")) {
                        reloadCallback();
                    }
                    $("#sendNotificationAction").show();
                    $("#downloadReportAction").show();
                    $("#sendNotificationProgress").hide();
                }
            }, function (code, errorMessage) {
                $("#sendNotificationAction").hide();
                $("#downloadReportAction").hide();
                $("#sendNotificationProgress").show();
            }, function (type, Message) {
                if (type == "danger") {
                    $("#sendNotificationProgress").html(getStatusLabel("ERROR", "##Connection Error##"));
                }
            });
        }
    };

    progressSendInterval = setInterval(loadProgress, 1000);
}

function reloadCallback() {
    if ($("#classesContainer").is(":visible")) {
        classTableRelations.fnDraw(false);
    }
    if ($("#customersContainer").is(":visible")) {
        customersTableRelations.fnDraw(false);
    }
}

function loadClasses() {
    DEFAULT_RELOAD_CALLBACK = reloadCallback;
    selectedClassId = undefined;

    $(".username")[0].innerHTML = getCookie("cmsuser");
    $("#classesContainer").hide();
    $("#customersContainer").hide();
    $("#statisticsContainer").hide();
    $("#spinContainer").show();

    var columns = [];
    columns.push({"sTitle": "Id", "sWidth": "8%"});
    columns.push({"sTitle": "Name", "sWidth": "23%"});
    columns.push({"sTitle": "Count", "sWidth": "13%"});
    columns.push({"sTitle": "Description"});

    classTableRelations = $("#showClasses").DataTable({
        "iDisplayLength": 10,
        "sAjaxSource": "/api/1/web/analytics/get/classes",
        "bFilter": true,
        "bAutoWidth": false,
        "bProcessing": true,
        "bServerSide": true,
        "aoColumns": columns,
        "bLengthChange": true,
        "bSort": false,
        "bDestroy": true,
        "aaSorting": [[0, 'desc']],
        "fnServerData": function (sSource, aaData, fnCallback) {
            $.ajax({
                "type": "GET",
                "dataType": "json",
                "url": sSource,
                "data": aaData,
                "success": function (response) {
                    var tableViewData = {
                        "iTotalRecords": response.iTotalRecords,
                        "iTotalDisplayRecords": response.iTotalDisplayRecords,
                        "aaData": []
                    };

                    classes = response.data;

                    if (response.data) {
                        response.data.forEach(function (item) {
                            classesMap[item.id] = item;

                            tableViewData.aaData.push([
                                getText(item.id),
                                getStatusLabel("NEUTRAL", item.type),
                                getText(item.count ? item.count : "0"),
                                getText(item.name)
                            ]);
                        });
                    }
                    fnCallback(tableViewData);
                },
                "error": function (xhr, status, error) {
                    showToast("danger", "Loading error");
                }
            });
        },
        "fnInitComplete": function (oSettings, json) {
            $("#spinContainer").hide();
            $("#classesContainer").show();
        },
        "fnCreatedRow": function (nRow, aData, iDataIndex) {
        },
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            $(nRow).find('td').each(function (column, td) {
                if (column < 5) {
                    $(td).attr('data-id', aData[0]);
                }
            });

            // Row click
            $(nRow).on('click', function () {
            });

            // Cell click
            $('td', nRow).on('click', function () {
                selectedClassRow = nRow;
                if ($(this).attr("data-id")) {
                    var id = $(this).attr("data-id");
                    if (id != selectedClassId) {
                        loadCustomers(id);
                        loadClassStats(id);
                    } else {
                        reloadCallback();
                    }
                }
            });
        }
    });
}

function reloadStats() {
    loadClassStats(selectedClassId, true);
}

function loadClassStats(id, reload) {
    if (!id) {
        return;
    }

    if (!reload) {
        $("#statisticsContainer").hide();
    }

    var classItem = classesMap[id];
    $("#statisticsHeader").html(classItem && classItem.type ? getStatusLabel("NEUTRAL", classItem.type) : "");

    var params = $("#interval").val().split("_");
    var daysCount = parseInt(params[0]);
    var points = parseInt(params[1]);

    var start = new Date(new Date().getTime() - daysCount * 24 * 60 * 60 * 1000);
    var end = new Date();

    var classItem = classesMap[id];
    $.ajax({
        "type": "GET",
        "dataType": "json",
        "url": "/api/1/web/analytics/get/classes/stats/" + classItem.type + "?start=" + start.toISOString() + "&end=" + end.toISOString() + "&points=" + points,
        "success": function (response) {
            var categories = [];
            var data = [];

            if (response && response.data && response.data[0] && response.data[0].list) {
                response.data[0].list.forEach((item) => {
                    categories.push(getDate(new Date(item.blockTime)));
                    data.push(parseInt(item.totalCount * 100) / 100);
                });
            }

            $("#statisticsContainer").show();
            Highcharts.chart('graph', {
                chart: {
                    type: 'line'
                },
                exporting: {
                    enabled: false
                },
                title: {
                    text: classItem.name,
                    style: {
                        color: '#767676',
                        fontWeight: 'bold',
                        fontSize: '14px',
                        fontFamily: 'Open Sans'
                    }
                },
                xAxis: {
                    categories: categories
                },
                yAxis: {
                    title: {
                        text: 'Customers Count'
                    }
                },
                plotOptions: {
                    line: {
                        dataLabels: {
                            enabled: true
                        },
                        enableMouseTracking: false
                    }
                },
                series: [{
                    name: 'Accounts',
                    data: data
                }]
            });

        },
        "error": function (xhr, status, error) {
            showToast("danger", "Loading error");
        }
    });
}

function loadCustomers(id) {
    if (!id) {
        return;
    }

    selectedClassId = id;
    $(".username")[0].innerHTML = getCookie("cmsuser");
    $("#customersContainer").hide();
    $("#spinContainer").show();

    var classItem = classesMap[id];
    $("#customersHeader").html(classItem && classItem.type ? getStatusLabel("NEUTRAL", classItem.type) : "");

    var columns = [
        {"sTitle": "Id", "sWidth": "8%", "sSortable": false},
        {"sTitle": "SI No.", "sWidth": "11%"},
        {"sTitle": "Name", "sWidth": "22%"},
        {"sTitle": "Number", "sWidth": "11%"},
        {"sTitle": "Email"}
    ]

    if (classItem.data1Title) {
        columns.push({"sTitle": classItem.data1Title, "sWidth": "11%"})
    } else {
        columns.push({"sTitle": "", "sWidth": "0%"})
    }

    if (classItem.dataInt1Title) {
        columns.push({"sTitle": classItem.dataInt1Title, "sWidth": "11%"})
    } else {
        columns.push({"sTitle": "", "sWidth": "0%"})
    }

    customersTableRelations = $("#showCustomers").DataTable({
        "iDisplayLength": 10,
        "sAjaxSource": "/api/1/web/analytics/get/customers/" + id,
        "bFilter": true,
        "bAutoWidth": false,
        "bProcessing": true,
        "bServerSide": true,
        "bLengthChange": true,
        "aoColumns": columns,
        "bDestroy": true,
        "bSort": false,
        "aaSorting": [[0, 'desc']],
        "fnServerData": function (sSource, aaData, fnCallback) {
            $.ajax({
                "type": "GET",
                "dataType": "json",
                "url": sSource,
                "data": aaData,
                "success": function (response) {
                    var tableViewData = {
                        "iTotalRecords": response.iTotalRecords,
                        "iTotalDisplayRecords": response.iTotalDisplayRecords,
                        "aaData": []
                    };

                    if (classItem.dataInt1Title) {
                        $("#averageDataInt1").html(getStatusLabel("OK",
                            "AVG (" + classItem.dataInt1Title + ") = " + response.dataInt1Average));
                    } else {
                        $("#averageDataInt1").html("");
                    }

                    if (response.data) {
                        response.data.forEach(function (item) {
                            customersMap[item.id] = item;

                            var values = [
                                getText(item.id),
                                (item.service_instance_number ? getCustomerAccountLink(item.service_instance_number) : "N/A"),
                                (item.name ? getText(item.name) : "N/A"),
                                (item.primaryNumber ? getCustomerNumberLink(item.primaryNumber) : "N/A"),
                                (item.email ? getText(item.email) : "N/A")
                            ];

                            if (classItem.data1Title) {
                                values.push(getText(item.data1));
                            } else {
                                values.push("");
                            }

                            if (classItem.dataInt1Title) {
                                values.push(getFormattedInt(item.dataInt1));
                            } else {
                                values.push("");
                            }

                            tableViewData.aaData.push(values);
                        });
                    }
                    fnCallback(tableViewData);
                },
                "error": function (xhr, status, error) {
                    showToast("danger", "Loading error");
                }
            });
        },
        "fnInitComplete": function (oSettings, json) {
            $("#spinContainer").hide();
            $("#customersContainer").show();
        },
        "fnCreatedRow": function (nRow, aData, iDataIndex) {
        },
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            $(nRow).find('td').each(function (column, td) {
                if (column < 10) {
                    $(td).attr('data-id', aData[0]);
                }
            });

            // Row click
            $(nRow).on('click', function () {
            });

            // Cell click
            $('td', nRow).on('click', function () {
                selectedCustomerRow = nRow;
                if ($(this).attr("data-id")) {
                    var id = $(this).attr("data-id");
                }
            });
        }
    });
}

function downloadCustomersReport() {
    $.ajax({
        "type": "GET",
        "dataType": "json",
        "url": "/api/1/web/analytics/get/customers/" + selectedClassId,
        "success": function (response) {
            var classObj = classesMap[selectedClassId];
            var classItem = classesMap[selectedClassId];

            var dataString = "";
            var rows = 0;
            var part = 1;

            var self = this;
            response.data.forEach(function (item) {

                if (!self.dataString) {
                    self.dataString = "sin can san ban name email number status ";
                    if (classObj.data1Title) self.dataString += classObj.data1Title.toLowerCase().replace(/ /g, "_") + " ";
                    if (classObj.dataInt1Title) self.dataString += classObj.dataInt1Title.toLowerCase().replace(/ /g, "_") + " ";
                    self.dataString += "\n";
                }

                rows++;
                self.dataString += (item.service_instance_number ? item.service_instance_number : "N/A") + " "
                + (item.customerAccountNumber ? item.customerAccountNumber : "N/A") + " "
                + (item.serviceAccountNumber ? item.serviceAccountNumber : "N/A") + " "
                + (item.billingAccountNumber ? item.billingAccountNumber : "N/A") + " "
                + (item.name ? item.name.replace(/ /g, '_') : "N/A") + " "
                + (item.email ? item.email : "N/A") + " "
                + (item.primaryNumber ? item.primaryNumber : "N/A") + " "
                + (item.status ? item.status : "N/A") + " ";

                if (classObj.data1Title) self.dataString += item.data1.replace(/ /g, "_") + " ";
                if (classObj.dataInt1Title) self.dataString += item.dataInt1 + " ";
                self.dataString += "\n";

                if (rows >= 5000) {
                    downloadCSV(self.dataString, "customers_" + classItem.type.replace(/ /g, "_")
                    + "_" + (new Date().toISOString()) + (part > 0 ? "_part_" + part : "") + "_rows_" + rows + ".csv", "text/csv");

                    part++;
                    rows = 0;
                    self.dataString = undefined;
                }
            });

            downloadCSV(self.dataString, "customers_" + classItem.type.replace(/ /g, "_")
            + "_" + (new Date().toISOString()) + (part > 1 ? "_part_" + part : "") + "_rows_" + rows + ".csv", "text/csv");
        },
        "error": function (xhr, status, error) {
            showToast("danger", "Loading error");
        }
    });
}

function showResyncClassesDialog() {
    generateConfirmationDialog("RESYNC_CLASSES", {}, reloadCallback);
}

function sendNotificationToCustomers() {
    if (!selectedClassId) {
        return showToast("danger", "No Selected Classes");
    }
    var classItem = classesMap[selectedClassId];
    if (!classItem) {
        return showToast("danger", "Selected Class is Not Found");
    }

    generateConfirmationDialog("SEND_CLASS_NOTIFICATION", {
        classId: classItem.id,
        className: classItem.name
    }, reloadCallback);
}