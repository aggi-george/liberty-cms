var bonusesTableRelations;
var billTableRelations;
var notificationTableRelations;
var circlescareTableRelations;
var dbsTableRelations;


function init_configmap() {
    $(".username")[0].innerHTML = getCookie("cmsuser");
    bonusesTableRelations = loadConfig("#bonusesTable", "bonus");
    notificationTableRelations = loadConfig("#notificationsTable", "notification");
    circlescareTableRelations = loadConfig("#circlescareTable", "selfcare");
    billTableRelations = loadConfig("#billTable", "bill");
    dbsTableRelations = loadConfig("#dbsTable", "dbs");
}