var number = 0;

function saveSetting(form) {
    var params = {
        key: form.name
    }
    for (var i = 0; i < form.length; i++) {
        var item = form[i];
        if (item.name) {
            params[item.name] = item.value;
        }
    }
    ajax("PUT", "/api/1/web/configmap/update", params, true, function () {
        showToast("success", "Successfully updated.");
    }, function () {
        // do nothing
    }, showToast);
}

function loadConfig(tableId, type) {
    $(".username")[0].innerHTML = getCookie("cmsuser");
    return $(tableId).DataTable({
        "sAjaxSource": "/api/1/web/configmap/list?type=" + type,
        "bProcessing": true,
        "bServerSide": true,
        "bFilter": false,
        "bSort":false,
        "bAutoWidth": false,
        "bPaginate": false,
        "bInfo": false,
        "aoColumns": [
            {"sTitle": ""},
        ],
        "bDestroy": true,
        "bLengthChange": false,
        "fnServerData": function (sSource, aaData, fnCallback) {
            ajax("GET", sSource, aaData, true,
                function (response) {
                    var tableViewData = {
                        "iTotalRecords": response.iTotalRecords,
                        "iTotalDisplayRecords": response.iTotalDisplayRecords,
                        "aaData": []
                    };

                    response.data.forEach(function (item) {

                        if (item.metadata) {
                            tableViewData.aaData.push(["<label class='status-large status-success'>" + item.metadata.display + "</label>"]);

                            if (item.metadata.optionKeys) {

                                var updateForm = "";
                                updateForm += "<form name='" + item.key + "' onSubmit='event.preventDefault(); return saveSetting(this);'>";
                                updateForm += "<div>";

                                item.metadata.optionKeys.forEach(function (optionKey) {
                                    if (item.options.metadata[optionKey]) {
                                        var optionMetadata = item.options.metadata[optionKey];
                                        var optionDisplayName = optionMetadata.display;
                                        var optionType = optionMetadata.type;
                                        var optionValue = item.options[optionKey];

                                        updateForm += "<div class='container'>";
                                        updateForm += "<div class='cell'>";

                                        updateForm += "<label style='padding-top: " + (optionDisplayName.length > 50 ? 0 : 5) + "pt; " +
                                        "font-weight: 400;font-family: 'Open Sans',sans-serif;'>" + optionDisplayName + "</label>";
                                        updateForm += "</div>";
                                        updateForm += "<div class='cell'>";
                                        if (optionType === "Integer") {
                                            updateForm += "<input type=number style='width:100%' name=" +
                                            optionKey + " value='" + optionValue + "' class=form-control id=selectInteger>";
                                        } else if (optionType === "String") {
                                            updateForm += "<input type=text style='width:100%' name=" +
                                            optionKey + " value='" + optionValue + "' class=form-control id=selectString>";
                                        } else if (optionType === "Boolean") {
                                            updateForm += "<select class=form-control style='width:100%' name=" +
                                            optionKey + " id=selectBoolean>" +
                                            "<option value='0'>NO</option>" +
                                            "<option value='1' " + (optionValue ? "selected='selected'" : "") + ">YES</option>" +
                                            "</select>";
                                        } else if (optionType === "ProductId") {
                                            updateForm += "<select class=form-control style='width:100%' name=" +
                                            optionKey + " id=selectProductId>" +
                                            getProductOptions(optionValue, optionMetadata, response.packages) +
                                            "</select>";
                                        } else if (optionType === "PromoCategoryId") {
                                            updateForm += "<select class=form-control style='width:100%' name=" +
                                            optionKey + " id=selectCategoryId>" +
                                            getCategoryOptions(optionValue, optionMetadata, response.promoCategories) +
                                            "</select>";
                                        } else if (optionType === "DateTime") {
                                            var name = "selectTime" + (number++);
                                            updateForm += "<input type='text' style='width:100%' class=form-control name=" + optionKey +
                                            " value='" + (optionValue ? getDateTime(optionValue) : "") + "'" +
                                            " id='" + name + "'/>";
                                            setTimeout(function () {
                                                $('#' + name).datetimepicker();
                                            }, 1000);
                                        } else if (optionType === "Time") {
                                            var name = "selectTime" + (number++);
                                            updateForm += "<input type='text' style='width:100%' class=form-control name=" + optionKey +
                                            " value='" + (optionValue ? optionValue : "") + "' id='" + name + "'/>";
                                        }
                                        updateForm += "</div>";
                                        updateForm += "</div>";
                                    }
                                });

                                updateForm += "<div class='container'><div class='cell'></div><div class='cell'>";
                                updateForm += "<input type=submit class='btn btn-default' style='width:100%; height: 100%;' value=Update></div>";
                                updateForm += "</div>";
                                updateForm += "</div>";
                                updateForm += "</form>";
                                updateForm += "";
                                tableViewData.aaData.push([updateForm]);
                            }
                        }
                    });
                    if (type == 'elitecore') {
                        tableViewData.aaData.push(["* propagation to all instances may take 5-10 sec"]);
                    }
                    fnCallback(tableViewData);
                },
                function () {
                    showToast("danger", "Loading error")
                }, showToast);
        },
        "fnInitComplete": function (oSettings, json) {
        },
        "fnCreatedRow": function (nRow, aData, iDataIndex) {
        },
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
        }
    });
}

function getProductOptions(selectedProductId, productMetadata, productMap) {

    var options = "<option value=''>NONE</option>";
    if (productMap && productMap[productMetadata.product.category]) {
        var sorted = productMap[productMetadata.product.category].sort(packageSort);
        sorted.forEach(function (productItem) {
            var ignore = false;

            if (!ignore && productMetadata.product.hasOwnProperty("recurrent")) {
                ignore = productItem.recurrent != productMetadata.product.recurrent;
            }

            if (!ignore && productMetadata.product.hasOwnProperty("free")) {
                var free = productMetadata.product.free;
                ignore = (free && productItem.price > 0) || (!free && productItem.price == 0);
            }

            if (!ignore) {
                options += "<option value='" + productItem.id + "' "
                + (selectedProductId === productItem.id ? "selected='selected'" : "") + ">"
                + productItem.name + "</option>";
            }
        });
    }
    return options;
}

function getCategoryOptions(selectedCategoryId, productMetadata, categories) {

    var options = "<option value=''>NONE</option>";
    if (categories) {
        var sorted = categories.sort(function packageSort(a, b) {
            return a.name < b.name ? -1 : 1;
        });

        sorted.forEach(function (category) {
            options += "<option value='" + category.id + "' "
            + (selectedCategoryId === category.id ? "selected='selected'" : "") + ">"
            + category.name + "</option>";
        });
    }
    return options;
}