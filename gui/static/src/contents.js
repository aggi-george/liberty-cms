function init(type) {
	$(".username")[0].innerHTML = getCookie("cmsuser");
	if ( type == "terms" ) {
		getContents("terms", "general");
		getContents("terms", "mobile");
		getContents("terms", "payment");
		getContents("terms", "website");
		getContents("terms", "dataprotection");
		getContents("terms", "copyright");
		getContents("terms", "circlescare");
		getContents("terms", "circlestalk");
		getContents("terms", "partnerships");
		getContents("terms", "promo_nationalday");
	} else if ( type == "rates" ) {
		getContents("rates", "circlestalk");
		getContents("rates", "idd002");
		getContents("rates", "idd021");
		getContents("rates", "ppurroaming");
	} else if ( type == "faq" ) {
		getContents("faq", "general");
		getContents("faq", "myplan");
		getContents("faq", "technical");
		getContents("faq", "partnerships");
		getContents("faq", "promo_nationalday");
	}
	$.ajax({
		url: "/api/1/web/api/get/contents",
		type: "GET",
		dataType: "json",
		data: {},
		success: function(data){
			if ( data.code == 0 ) {
				showAPI(data.list);
			} else alert("Fail");
		},
		statusCode: {
			403: function(error){
				location.reload();
			}
		}
	});
	$("body").tooltip({
	    selector: '.tooltip',
	    container: 'body'
	});
}

function submitNew(form) {
	var question = form.newInput.value;
	var cat = form.category.value;
	if (question == "") return false;
	form.newInput.disabled = true;
	var parameters = { "question" : question };
	ajax("PUT", "/api/1/web/contents/add/new/" + cat, parameters, true, function (data) {
		if ( data && (data.code == 0) ) {
			alert("Success!");
			location.reload();
		} else alert("Fail");
	});
	return false;
}

function deleteQuestion(id) {
	if ( !confirm("Are you sure?") ) return false;
	var parameters = "";
	ajax("DELETE", "/api/1/web/contents/delete/faq/" + id, parameters, true, function (data) {
		if ( data && (data.code == 0) ) {
			alert("Success!");
			location.reload();
		} else alert("Fail");
	});
	return false;
}

function getContents(cat, type) {
    var parameters = {};
    ajax("GET", "/api/1/web/contents/get/" + cat + "/" + type, parameters, true, function (data) {
        if (data && (data.code == 0)) {
            showContents(data.result, data.write);
        } else {
            showToast("Warning", "Fail");
        }
    });
}

function uploadContent(input) {
	var cat = $(input).attr("data-cat");
	var type = $(input).attr("data-id");
	var xmlhttp;			// bug: move to jquery
	if ( window.XMLHttpRequest ) xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange = function() {
		if ( xmlhttp.readyState == 4 ) {
			if ( xmlhttp.status == 403 ) {
				location.reload();
			} else if ( xmlhttp.status == 200 ) {
				var data = JSON.parse(xmlhttp.responseText);
				if ( data.code == 0 ) {
					alert("Success");
					getContents(data.cat, data.type);
				} else alert("Fail");
			} else alert("Failed");
		}
	}
	var fData = new FormData();
	fData.append("content", input.files[0]);
	xmlhttp.open("PUT", "/api/1/web/contents/upload/" + cat + "/" + type, true);
	xmlhttp.send(fData);

	return false;
}

function updateFAQ(input) {
	var id = $(input).parent().parent().attr("data-id");
	var type = $(input).parent().attr("data-type");
	var text = input.value;
	if (text == "") return false;
	$.ajax({
		url: "/api/1/web/contents/update/faq/" + type + "/" + id,
		type: "PUT",
		dataType: "json",
		data: { "text" : text},
		success: function(data){
			if ( data.code == 0 ) {
				init("faq");
			} else alert("Fail");
		},
		statusCode: {
			403: function(error){
				location.reload();
			}
		}
	});
}

function showISO(iso) {
    var parameters = { "iso" : iso };
    ajax("GET", "/api/1/web/contents/get/rates/circlestalk", parameters, true, function (data) {
        if (data && (data.code == 0)) {
//            if ($("#contentModal").dataTableSettings.length > 0) $("#contentModal").DataTable().fnDestroy();
            $("#contentModal th").eq(0).text("Destination");
            $("#contentModal th").eq(1).text("Rate SS/min");
            $("#contentModal tbody").empty();
            if (data.result) data.result.forEach(function (item) {
                $("#contentModal > tbody:last-child").append("<TR>" +
                    "<TD>" + item.destination + "</TD>" +
                    "<TD>" + item.rate + "</TD>" +
                "</TR>");
            });
//            $("#contentModal").DataTable();
            $("#myModal").modal();
        } else {
            showToast("warning", "Get iso rates failed");
        }
    });
}

function showContents(obj, write) {
	$("#show" + obj.type + " > tbody").empty();
	if (!write) $("#add" + obj.type).hide();
	if ( obj.cat == "rates" ) {
		if (obj.header) $("#" + obj.type + "_label").html(obj.header.label);
		if (obj.tooltip) $("#" + obj.type + "_label").attr("title", obj.tooltip.label);
		if (obj.header) {
			Object.keys(obj.header).forEach(function (key) {
				$("#" + obj.type + "_" + key).html(obj.header[key]);
			});
		}
		if (obj.tooltip) {
			Object.keys(obj.header).forEach(function (key) {
				$("#" + obj.type + "_" + key).attr("title", obj.tooltip[key]);
			});
		}
		if ( obj.type == "circlestalk" ) {
			obj.content.forEach(function (item) {
				$("#show" + obj.type + " > tbody:last-child").append("<TR><TD><A href=# onClick='showISO(\"" + item.iso + "\");'>" + item.country + "</A></TD>" +
										"<TD>" + item.low + "</TD>" +
										"<TD>" + item.high + "</TD>" +
										"<TD>" + item.ave + "</TD></TR>");
			});
		} else if ( obj.type == "idd002" ) {
			obj.content.forEach(function (item) {
				$("#show" + obj.type + " > tbody:last-child").append("<TR><TD>" + item.country + "</TD>" +
										"<TD>" + item.peak + "</TD>" +
										"<TD>" + item.nonPeak + "</TD></TR>");
			});
		} else if ( obj.type == "idd021" ) {
			obj.content.forEach(function (item) {
				$("#show" + obj.type + " > tbody:last-child").append("<TR><TD>" + item.country + "</TD>" +
										"<TD>" + item.peak + "</TD>" +
										"<TD>" + item.nonPeak + "</TD></TR>");
			});
		} else if ( obj.type == "ppurroaming" ) {
			obj.content.forEach(function (item) {
				var discountoperator = (item.discountoperator) ? item.discountoperator : "";
				var discountdata = (item.discountdata) ? item.discountdata : "";
				$("#show" + obj.type + " > tbody:last-child").append("<TR><TD>" + item.country + "</TD>" +
										"<TD>" + item.ppurdatamms + "</TD>" +
										"<TD>" + discountoperator + "</TD>" +
										"<TD>" + discountdata + "</TD>" +
										"<TD>" + item.ppurvoicesg + "</TD>" +
										"<TD>" + item.ppurvoiceintl + "</TD>" +
										"<TD>" + item.ppurvoicelocal + "</TD>" +
										"<TD>" + item.ppurvoiceincoming + "</TD>" +
										"<TD>" + item.ppursmssend + "</TD></TR>");
			});
		}
		$("#show" + obj.type).DataTable();
	} else if ( obj.cat == "faq" ) {
		if (write) $(".showAdd").show();
		if (obj.content) obj.content.forEach(function (item) {
			var edit = (write) ? "<button type='button' class='btn btn-white btn-sm' style='border-style:none;'  onClick='deleteQuestion(\"" + item.id + "\");'><i class='fa fa-trash-o'></i></button>" : "";
			if (item) $("#show" + obj.type + " > tbody:last-child").append("<TR data-id=" + item.id + ">" +
									"<TD data-type=orderID class='editFAQ'>" + item.orderID + "</TD>" +
									"<TD data-type=question class='editFAQ'>" + item.question + "</TD>" +
									"<TD data-type=answer class='editFAQ'>" + item.answer + "</TD>" +
									"<TD>" + edit + "</TD></TR>");
		});
		if (write) $(".editFAQ").click(function () {
			if ($("#faqEdit").length > 0) return false;
			$(this).unbind("click");
			$(this).removeClass('editFAQ');
			var value = this.innerHTML;
			if ( $(this).attr("data-type") == "orderID" ) {
				this.innerHTML = "<INPUT id=faqEdit type=number min=0 max=99 value='" + value + "'>";
			} else {
				this.innerHTML = "<TEXTAREA id=faqEdit type=textarea rows=4 cols=32>" + value + "</TEXTAREA>";
			}
			$("#faqEdit").focus();
			$("#faqEdit").change(function() {
				updateFAQ(this);
			});
			$("#faqEdit").blur(function() {
				init("faq");
			});
		});
	} else {
		$("#show" + obj.type + " > tbody:last-child").append("<TR><TD><DIV class=editInline>" + obj.content + "</DIV></TD></TR>");
	}

//	if (write) {
	if (false) {		// do not allow edit
		$(".editInline").click(function(){
			var editor;
			if (!CKEDITOR.instances.editor1) {
				CKEDITOR.config.removePlugins = 'wsc,scayt';
				editor = CKEDITOR.replace(this);
			}
			editor.on("instanceReady", function(ev) {
				if ( this.document.getBody().getHtml() == "<p>( not set )</p>" ) {
					this.setData('');
				}
				ev.editor.focus();
			});
			editor.on("blur", function() {
//				updateNotification(details.activity, { "html" : this.document.getBody().getHtml() });
				editor.destroy();
			});
			editor.focus();
		});
	}
}
