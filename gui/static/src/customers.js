var prefix = "65";
var timerID;
var cache;
var loadingFromNavigation;
var pairedNumbers = [];
var unpairedNumbers = [];
var numbersToICCID = {};
var numberChangeCategory;
var roamingOption;

hideUnnecessary("all");

function init() {
    window.addEventListener("popstate", function() {
      if(location.href.indexOf("customers.html?") > -1) {
          loadingFromNavigation = true;
          WEB_loadingFromNavigation = true;
          setTimeout(function(){
              loadingFromNavigation = false;
              WEB_loadingFromNavigation = false;
          }, 7000);
        //replaces first element of last element of stack with google.com/gmail so can be used further
        location.reload();
      }
    }, false);

    $(".username")[0].innerHTML = getCookie("cmsuser");
    if (window.location.href.split("?").length > 1) {
        var params = window.location.href.split("?")[1].split("&");
        params.forEach(function (keypair) {
            var key = keypair.split("=")[0];
            var value = keypair.split("=")[1];
            if (key == "number") {
                selectPill(1);
                document.forms.searchFormNumber.mobile.value = value;
                search("mobile", prefix, value, undefined, undefined, undefined, function () {
                });
            } else if (key == "account") {
                selectPill(3);
                document.forms.searchFormAccountNumber.acctnumber.value = value;
                $("#numberspin").show();
                search("acctnumber", prefix, undefined, undefined, value, undefined, function () {
                    $("#numberspin").hide();
                });
            } else if (key == "referralCode") {
                selectPill(4);
                document.forms.searchFormReferralCode.referralcode.value = value;
                $("#referralspin").show();
                search("referralcode", prefix, undefined, undefined, undefined, value, function () {
                    $("#referralspin").hide();
                });
            }
        });
    }
}



function selectPill(activeIndex){
    var changeState = function(id, active, tabContent) {
        if (active) {
            $(id).addClass(tabContent ? "active" : "tab-pane fade in active");
        } else {
            $(id).removeClass(tabContent ? "active" : "in active");
        }
    }

    changeState("#smenuTab1", activeIndex == 1, true);
    changeState("#smenuTab2", activeIndex == 2, true);
    changeState("#smenuTab3", activeIndex == 3, true);
    changeState("#smenuTab4", activeIndex == 4, true);
    changeState("#smenuTab5", activeIndex == 5, true);

    changeState("#smenu1", activeIndex == 1);
    changeState("#smenu2", activeIndex == 2);
    changeState("#smenu3", activeIndex == 3);
    changeState("#smenu4", activeIndex == 4);
    changeState("#smenu5", activeIndex == 5);
}



function search(type, prefix, number, accountName, accountNumber, referralCode, callback) {
    showLoadingLabel();
/*    if (prefix && number) {
        $("#cdrspin").show();
        ajax("GET", "/api/1/web/customers/get/cdr/" + prefix + "/" + number, parameters, true, function (data) {
            if (data && (data.code == 0)) {
                $("#cdrspin").hide();
                showCDR(data.list);
            }
        });
    }
*/
    var parameters = {"prefix": prefix, "number": number, "accountName":
        accountName, "accountNumber": accountNumber, "referralCode" : referralCode};
    
    if(type != "mobile"){
        $('input[name=mobile]').val('');
    }
    ajax("GET", "/api/1/web/customers/search/" + type, parameters, true, function (data) {
        if (data && (data.code == 0)) {
            if (data.result.searchName) {
                if(data.result.searchName.length == 0){
                    hideUnnecessary("all");
                    showToast("warning", "Not Available");
                }else{
                    generateTableDialog("customerNameSearch", data.result.searchName);
                }
            } else if (data.result.searchNumber) {
                if(data.result.searchNumber.length == 0){
                    hideUnnecessary("all");
                    showToast("warning", "Not Available");
                }else{
                    generateTableDialog("customerNameSearch", data.result.searchNumber);
                }
            } else {
                showAll(data.result, data.write);
            }
            if (callback) callback();
            hideLoadingLabel();
            if (data.result.cache && data.result.cache.account) {
                ajax("GET", "/api/1/web/customers/bills", {accountNumber: data.result.cache.account}, true, function (data) {
                    if (data && (data.code == 0) && data.result) {
                        showBills(data.result.bills, data.result.pdfpassword, data.write);
                    }else{
                        showToast("warning", genUserMessage('BILL_FETCH_FAILED'));
                    }
                });
            }

        } else {
            showInfoDialog("Failure", "Failed");
            hideLoadingLabel();
            if (callback) callback();
        }
    });
}

function reloadPage(err, data){
    showLoadingLabel();
    if(cache && cache.account){
        search("acctnumber", undefined, undefined, undefined, cache.account);
    }else{
        search("mobile", data.prefix, data.number);
    }
}

DEFAULT_RELOAD_CALLBACK = reloadPage;

function addBonusHistory(callback) {
    var type = [
        {key: 'selfcare_install', value: "CirclesCare Install"},
        {key: 'gentwo_install', value: "CirclesTalk Install"},
        {key: 'loyalty', value: "Loyalty Bonus"},
        {key: 'portin', value: "PortIn Bonus"},
        {key: 'birthday', value: "Birthday Bonus"},
        {key: 'surprise', value: "Surprise Bonus"},
        {key: 'referral', value: "Referral"},
        {key: 'promo', value: "General Promo Bonus"},
        {key: 'promo_zalora', value: "Promo ZALORA Bonus"},
        {key: 'promo_lazada', value: "Promo LAZADA Bonus"},
        {key: 'promo_nrod', value: "Promo #NeverRunOutOfData Bonus"},
        {key: 'promo_grab', value: "Promo GRAB Bonus"},
        {key: 'promo_extra_portin', value: "Promo Extra PortIn Bonus"},
        {key: 'promo_national_day', value: "Promo National Day Bonus"},
        {key: 'promo_uber_driver', value: "Promo Uber Bonus"},
        {key: 'promo_dbs', value: "DBS Promo"},
        {key: 'promo_shopee', value: "Shopee Promo"},
        {key: 'promo_2017_may_giveaway_bonus', value: "Promo May Giveaway Bonus"},
        {key: 'promo_poems', value: "Promo POEMS"},
        {key: 'promo_contract_buster', value: "Promo Contract Buster"},
        {key: 'promo_standard_chartered', value: "STANDCHART bonus"},
        {key: 'promo_carousell', value: "Carousell Bonus"},
        {key: 'promo_amex', value: "Amex Bonus"},
        {key: 'promo_paktor', value: "Paktor Bonus"},
        {key: 'promo_fave', value: "Fave Bonus"},
        {key: 'promo_2017_july_giveaway_bonus', value: "Circles.Life Birthday bonus"}
    ];

    var sizeKb = [
        {key: '0', value: "0MB"},
        {key: '51200', value: "50MB"},
        {key: '102400', value: "100MB"},
        {key: '153600', value: "150MB"},
        {key: '204800', value: "200MB"},
        {key: '256000', value: "250MB"},
        {key: '307200', value: "300MB"},
        {key: '409600', value: "400MB"},
        {key: '512000', value: "500MB"},
        {key: '614400', value: "600MB"},
        {key: '716800', value: "700MB"},
        {key: '768000', value: "750MB"},
        {key: '819200', value: "800MB"},
        {key: '921600', value: "900MB"},
        {key: '1048576', value: "1GB"},
        {key: '1572864', value: "1.5GB"},
        {key: '2097152', value: "2GB"},
        {key: '2621440', value: "2.5GB"},
        {key: '3145728', value: "3GB"},
        {key: '3670016', value: "3.5GB"},
        {key: '4194304', value: "4GB"},
        {key: '5242880', value: "5GB"},
        {key: '6291456', value: "6GB"},
        {key: '7340032', value: "7GB"},
        {key: '8388608', value: "8GB"},
        {key: '9437184', value: "9GB"},
        {key: '10485760', value: "10GB"},
        {key: '20971520', value: "20GB"},
        {key: '41943040', value: "40GB"},
        {key: '62914560', value: "60GB"},
        {key: '83886080', value: "80GB"},
        {key: '104857600', value: "100GB"}
    ];

    var options = [];
    options.push({display: 'Type', key: 'bonus_type', value: undefined, type: 'KeyValue', items: type, mandatory: true});
    options.push({display: 'SubType', key: 'bonus_sub_type', value: undefined, type: 'String'});
    options.push({display: 'Size', key: 'bonus_kb', value: undefined, type: 'KeyValue', items: sizeKb, mandatory: true});
    options.push({display: 'Display Until', key: 'display_until_ts', type: 'DateTime'});

    var params = {
        title: 'Bonus History',
        key: 'historyForm',
        type: 'Create',
        options: options
    };

    createFormDialog(params, function (values, close, dialogRef) {
        if (!values) {
            return dialogRef.close();
        }

        values.prefix = cache.prefix;
        values.number = cache.number;
        values.service_instance_number = cache.serviceInstanceNumber;

        ajax("PUT", "/api/1/web/promo/add/history/" + cache.account + "/" + cache.serviceInstanceNumber,
            values, true, function (data) {
                dialogRef.close();
                showToast("success", "Added History Item");
                search("mobile", data.prefix, data.number);
                if (callback) callback();
            }, function () {
                // do nothing
            }, showToast);
    });

    setTimeout(function(){
        $("[name='bonus_type']").change(function(){
            if($(this).val() == "loyalty"){
                $("[name='bonus_kb']").empty().append(getOptions(sizeKb));
            }
        });
    }, 500);
}

// effect - 0 is immediate
// effect - 1 is tomorrow
// effect - 2 is next month
function bonusUnSub(prefix, number, historyId, productId) {
    generateConfirmationDialog('BONUS_UNSUBSCRIBE', {
        prefix: prefix, 
        number: number, 
        historyId: historyId, 
        product: productId
    }, reloadPage);
}


function alignBonusHistory() {
    generateConfirmationDialog('ALIGN_BONUS_HISTORY', {
        number: cache.number, 
        prefix: cache.prefix, 
        serviceInstanceNumber: cache.serviceInstanceNumber,
        account: cache.account,
        current: true
    }, reloadPage);
}

function addGoldenTicket() {
    generateConfirmationDialog('ADD_GOLDEN_TICKET', {
        serviceInstanceNumber: cache.serviceInstanceNumber,
        account: cache.account
    }, reloadPage);
}

function resubAddon() {
    generateConfirmationDialog('RE_SUBSC_ADDONS', {
        number: cache.number, 
        prefix: cache.prefix, 
        serviceInstanceNumber: cache.serviceInstanceNumber,
        account: cache.account
    }, reloadPage);
}

function useReferalCode() {
    generateConfirmationDialog('USE_REFERRAL_CODE', {
        number: cache.number, 
        prefix: cache.prefix
    }, reloadPage);
}

function updateProfile() {
    generateConfirmationDialog("UPDATE_PROFILE", {
        name: cache.billingFullName,
        idType: cache.customerIdentity.typeFormatted.toUpperCase(),
        idNumber: cache.customerIdentity.number.toUpperCase(),
        portedStatus: cache.customerPortedStatus,
        orderRefNo: cache.orderReferenceNumber,
        account: cache.account,
        serviceInstanceNumber: cache.serviceInstanceNumber,
        birthday: cache.birthday
    }, reloadPage);
}

function useBonus(prefix, number, account, serviceInstanceNumber, id, callback) {

    generateConfirmationDialog('USE_BONUS', {
        number: number, 
        prefix: prefix, 
        serviceInstanceNumber: serviceInstanceNumber,
        account: account,
        id: id
    }, reloadPage, callback);
}

function payNow(prefix, number, account, serviceInstanceNumber, outstanding, invoiceId, invoicesInfo, callback) {

    invoicesInfo = invoicesInfo ? JSON.parse(invoicesInfo.replace(/%27/g, "'")) : undefined
    if (invoicesInfo) {
        outstanding = 0;
        invoiceId = "";
        invoicesInfo.forEach((item) => {
            if (invoiceId) invoiceId += ", ";
            invoiceId += item.invoiceId;
            outstanding += item.amount;
        })
    }

    generateConfirmationDialog('PAY_NOW', {
        number: number, 
        prefix: prefix, 
        serviceInstanceNumber: serviceInstanceNumber,
        outstanding: outstanding,
        account: account,
        invoiceId: invoiceId,
        amount: outstanding,
        invoicesInfo: invoicesInfo ? JSON.stringify(invoicesInfo) : undefined
    }, reloadPage, callback);
}

function updateRoamingCap(cache, limit, callback) {

    generateConfirmationDialog('UPDATE_ROAMING_CAP', {
        number: cache.number, 
        prefix: cache.prefix, 
        limit: limit,
        serviceInstanceNumber: cache.serviceInstanceNumber,
        account: cache.account
    }, reloadPage, callback);
}

function addAutoboost(cache, checkData, enable, callback) {

    generateConfirmationDialog('ADD_AUTO_BOOST', {
        number: cache.number, 
        prefix: cache.prefix, 
        checkData: checkData,
        enable: enable,
        serviceInstanceNumber: cache.serviceInstanceNumber,
        account: cache.account
    }, reloadPage, callback);
}

function createUpdateCCLink(cache, callback) {

    generateConfirmationDialog('CREATE_UPDATE_CC_LINK', {
            number: cache.number, 
            prefix: cache.prefix,
            account: cache.account,
        },
        reloadPage,
        function(err, data){
            if(!err){
                showInfoDialog(genUserMessage('CC_URL_VALID_TITLE'), `${data.url}&returnurl=${data.returnUrl}`);
                if(callback) callback(err, data);
            }
        }
    );
}

function addPortInBonus(cache, extra, callback) {

    generateConfirmationDialog('ADD_PORTIN_BONUS', {
        number: cache.number, 
        prefix: cache.prefix, 
        extra: extra,
        serviceInstanceNumber: cache.serviceInstanceNumber,
        account: cache.account
    }, reloadPage, callback);
}

function monkeySet(select, account, number) {
    var parameters = {"number": number, "prefix": prefix};
    ajax("PUT", "/api/1/web/customers/update/monkey/" + account + "/" + select.value, parameters, true, function (data) {
        if (data && (data.code == 0)) {
            showInfoDialog("", "Success");
            search("mobile", data.prefix, data.number);
        } else showInfoDialog("", "Fail");
    });
}

function validateCirlcesTalk(){
    if($('#circlesTalkPhoneNumber').val()){
        return true;
    }else{
        showToast("warning", genUserMessage('PLEASE_SELECT_PHONE_NUMBER'));
        return false;
    }
}

function pay200(number) {
    var parameters = "";
    ajax("POST", "/api/1/web/customers/pay200/" + prefix + "/" + number, parameters, true, function (data) {
        if (data && (data.code == 0)) {
            search("mobile", data.prefix, data.number);
            if (data.errorMessage) {
                showInfoDialog("Error", data.errorMessage);
            } else {
                showInfoDialog("Success", genUserMessage('PAY_200_CONFIRMATION_MESSAGE'));
            }
        } else {
            showInfoDialog("", "Fail");
        }
    });
}

function recache(number, account, reload) {
    $(".recache").prop("disabled", true);
    var parameters = {"reload": reload};
    ajax("PUT", "/api/1/web/customers/update/cache/" + account + "/" + prefix + "/" + number, parameters, true, function (data) {
        if (data && (data.code == 0)) {
            $(".recache").prop("disabled", false);
            showInfoDialog("", "Success!");
            if (data.reload == "false") location.reload();
            else search("mobile", data.prefix, data.number);
        } else showInfoDialog("", "Fail!");
    });
}

function updateDOB(form, account, number) {
    var dob = form.dob.value;
    form.dob.disabled = true;
    var parameters = {"number": number, "prefix": prefix, "dob": dob};
    showLoadingLabel();
    ajax("PUT", "/api/1/web/customers/update/dob/" + account, parameters, true, function (data) {
        if (data && (data.code == 0)) {
            showToast("success", "Success!");
            search("mobile", data.prefix, data.number);
        } else showToast("warning", "Failed!");
    });
}

function checkNumberAvail(number, callback) {
    var parameters = "";
    ajax("GET", "/api/1/web/customers/get/number/availability/" + prefix + "/" + number, parameters, true, function (data) {
        if (data && (data.code == 0)) {
            callback(data.status);
        } else {
            showInfoDialog("", "Fail!");
            callback();
        }
    });
    return false;
}

function releaseNumber(number) {
    var parameters = "";
    ajax("PUT", "/api/1/web/customers/update/number/release/" + prefix + "/" + form.number.value, parameters, true, function (data) {
        if (data && (data.code == 0)) {
            showNumber(data);
        } else showInfoDialog("", "Fail!");
    });
    return false;
}


function deleteReferral(prefix, number, referrer, id, callback) {

    generateConfirmationDialog('DELETE_REFERRAL', {
        number: number, 
        referrer: referrer, 
        prefix: cache.prefix,
        id: id
    }, reloadPage, callback);
}

function deleteBonusHistory(prefix, number, id, callback) {

    generateConfirmationDialog('DELETE_BONUS_HISTORY', {
        number: number,
        prefix: cache.prefix,
        id: id
    }, reloadPage, callback);
}

function testPush(type, app) {
    var parameters = new Object;
    if (type == "vpush") {
        parameters.token = app.vapn;
        parameters.token_type = "vapn";
    } else {
        parameters.token = app.token;
        parameters.token_type = app.token_type;
    }
    parameters.flavor = app.flavor;
    parameters.app_type = app.app_type;
    parameters.serviceInstanceNumber = cache.serviceInstanceNumber;
    ajax("PUT", "/api/1/web/customers/apps/push", parameters, true, function (data) {
        if (data && (data.code == 0)) {
            showInfoDialog("", "Success!");
        } else showInfoDialog("", "Fail!");
        $("a.push").removeAttr('disabled');
    });
    return false;
}

function addCreditNotes(cache, callback) {
    var options = [];
    options.push({display: 'Amount ($)', key: 'amount', value: 0, type: 'Float', gt: 0, mandatory: true});
    options.push({display: 'Reason', key: 'reason', value: '', type: 'String', mandatory: true});

    var params = {
        title: 'Credit Notes',
        key: 'creditNotesForm',
        type: 'Create',
        confirmation: true,
        options: options
    };

    createFormDialog(params, function (values, close) {
        values.prefix = cache.prefix;
        values.number = cache.number;
        ajax("PUT", "/api/1/web/customers/bill/add/credit/notes",
            values, true, function (data) {
                close();
                showToast("success", "Credit Notes (" + values.amount + "$) has been added for " + values.number);
                if (callback) callback();
            }, function () {
                // do nothing
            }, showToast);
    });
}

function blockNotif(checked, number) {
    var parameters = { "block" : checked };
    ajax("PUT", "/api/1/web/customers/notifications/block/" + prefix + "/" + number, parameters, true, function (data) {
        var notificationActivities = "NONE";
        if (data.list) {
            notificationActivities = "";
            data.list.forEach(function (item) {
                if (notificationActivities.length > 0) notificationActivities += ' \n ';
                notificationActivities += item;
            });
        }
        if (checked) {
            showToast("success", "Notifications are blocked for "
            + JSON.stringify(data.timeout / 1000) + " secs (" + notificationActivities + ")");
        } else {
            showToast("success", "Notifications are unblocked");
        }
    }, function() {
        // no nothing
    }, showToast);
    return false;
}

function overrideNotif(ts, activity, number) {
    var parameters = { "ts" : ts, "activity" : activity };
    ajax("PUT", "/api/1/web/customers/notifications/override/" + prefix + "/" + number, parameters, true, function (data) {
        if (data && (data.code == 0)) {
                search("mobile", data.prefix, data.number);
                showInfoDialog("", "Success!");
        } else showInfoDialog("", "Fail!");
    });
    return false;
}

function addSchedules(events) {
    var parameters = { "events" : JSON.stringify({ "list" : events }) };
    ajax("PUT", "/api/1/web/customers/add/schedules", parameters, false, function (data) {
        if (data && (data.code == 0)) {
            showToast("success", "Events Added");
            clearTableify();
            $('#calendar').fullCalendar('refetchEvents');
        } else showToast("warning", "Events Not Added");
    });
    return false;
}

function getFreeICCID(callback) {
    var parameters = { };
    ajax("GET", "/api/1/web/customers/get/available/iccid", parameters, true, function (data) {
        if (data && (data.code == 0)) {
            callback(data.iccid);
        } else {
            callback("");
        }
    });
    return false;
}

function ecommNotify(type, prefix, number, serviceInstanceNumber) {
    if (!type || (!number && !serviceInstanceNumber)) return;
    var parameters = { "prefix" : prefix, "number" : number, "serviceInstanceNumber" : serviceInstanceNumber };
    ajax("PUT", "/api/1/web/customers/ecomm/notification/" + type, parameters, true, function (data) {
        if (data && (data.code == 0)) {
            showToast("success", "Notification Sent");
        } else {
            showToast("warning", "Notification Failed " + data.message);
        }
    });
}

function searchForm(form) {
    if (!form || !$(form).valid()) return false;
    $("#avail").html("");
    if (form.mobile && form.mobile.value && (form.mobile.value != "") &&
        (parseInt(form.mobile.value) == form.mobile.value)) {
        form.mobile.disabled = true;
        search("mobile", form.prefix.value, form.mobile.value, undefined, undefined, undefined, function () {
            form.mobile.disabled = false;
            try{
                var currrentBaseUrl = window.location.href.split("?")[0];
                window.history.pushState('', 'Customers', currrentBaseUrl + "?number=" + form.mobile.value);
            }catch(e){
                console.log();
            }
        });
    } else if (form.acctname && form.acctname.value && (form.acctname.value != "")) {
        form.acctname.disabled = true;
        search("acctname", undefined, undefined, form.acctname.value, undefined, undefined, function () {
            form.acctname.disabled = false;
        });
    } else if (form.acctnumber && form.acctnumber.value && (form.acctnumber.value != "")) {
        form.acctnumber.disabled = true;
        search("acctnumber", undefined, undefined, undefined, form.acctnumber.value, undefined, function () {
            form.acctnumber.disabled = false;
            try{
                var currrentBaseUrl = window.location.href.split("?")[0];
                window.history.pushState('', 'Customers', currrentBaseUrl + "?account=" + form.acctnumber.value);
            }catch(e){
                console.log();
            }
        });
    } else if (form.referralcode && form.referralcode.value && (form.referralcode.value != "")) {
        form.referralcode.disabled = true;
        search("referralcode", undefined, undefined, undefined, undefined, form.referralcode.value, function () {
            form.referralcode.disabled = false;
            try{
                var currrentBaseUrl = window.location.href.split("?")[0];
                window.history.pushState('', 'Customers', currrentBaseUrl + "?referralCode=" + form.referralcode.value);
            }catch(e){
                console.log();
            }
        });
    } else if (form.avail && form.avail.value && (form.avail.value != "")) {
        form.avail.disabled = true;
        checkNumberAvail(form.avail.value, function(stat) {
            form.avail.disabled = false;
            $("#avail").html(stat);
        });
    } else {
        return false;
    }
}

function fixMB(value) {
    if (!value) return "0 MB";
    else if (value < 1024) return value + " MB";
    else if (value < (1024 * 1024)) return (Math.round((value / 1024) * 10) / 10) + " GB";
}

function fixB(value) {
    if (!value) return "0 B";
    else if (value < 1024) return value + " B";
    else if (value < (1024 * 1024)) return (Math.round((value / 1024) * 10) / 10) + " KB";
    else if (value < (1024 * 1024 * 1024)) return (Math.round((value / 1024 / 1024) * 10) / 10) + " MB";
    else return (Math.round((value / 1024 / 1024 / 1024) * 10) / 10) + " GB";
}

function hideUnnecessary(type){
    switch(type){
        case "none":
            $("#tabSet").show();
            $("#profileUpdate").hide();
            $("#generalDetailsCart").show();
            $("#referalsCart").show();
            $("#generalActionsCart").show();
            $("#circlesTalkActionsCart").show();
            $("#portInCart").show();
            $("#terminateSuspendCart").show();
            $("#creditCapCart").show();
            $("#currentCycleCart").show();
            $("#nextCycleCart").show();
            $("#profileUpdate").show();
            $("#accountNumbersPanel").show();
            $("#phoneNumbersPanel").show();
            $("#customerAccountPanel").show();
            $("#billingAccountPanel").show();
            $("#servieInstancePanel").show();
            $("#showSelfcarePanel").show();
            $("#showCommercialPanel").show();
            $("#generalActionsPanel").show();
            break;
        case "all":
            $("#tabSet").hide();
            $("#generalDetailsCart").hide();
            $("#referalsCart").hide();
            $("#generalActionsCart").hide();
            $("#circlesTalkActionsCart").hide();
            $("#portInCart").hide();
            $("#currentCycleCart").hide();
            $("#nextCycleCart").hide();
            $("#terminateSuspendCart").hide();
            $("#creditCapCart").hide();
            break;
        case "careOnly":
            $("#generalDetailsCart").show();
            $("#circlesTalkActionsCart").show();
            $("#generalActionsCart").hide();
            $("#profileUpdate").hide();
            $("#accountNumbersPanel").hide();
            $("#phoneNumbersPanel").hide();
            $("#customerAccountPanel").hide();
            $("#billingAccountPanel").hide();
            $("#servieInstancePanel").hide();
            $("#showSelfcarePanel").hide();
            $("#showCommercialPanel").hide();
            $("#referalsCart").hide();
            $("#generalActionsPanel").hide();
            $("#portInCart").hide();
            $("#currentCycleCart").hide();
            $("#nextCycleCart").hide();
            $("#terminateSuspendCart").hide();
            $("#creditCapCart").hide();
            break;
    }
}

function showBills(bills, pdfpassword, write) {
    $("#showBills tbody").empty();

    if (bills) bills.forEach(function (bill) {
        var status = getStatusLabel(bill.sendStatus, "NO:" +
            (!bill.sendStatus ? "NONE" : bill.sendStatus)) +
            " " + getStatusLabel(bill.syncStatus, "SY:" +
            (!bill.syncStatus ? "NONE" : bill.syncStatus)) +
            " " + getStatusLabel(bill.contentStatus, "CO:" +
            (!bill.contentStatus ? "NONE" : bill.contentStatus)) +
            " " + getStatusLabel(bill.paymentStatus, "PA:" +
            (!bill.paymentStatus ? "NONE" : bill.paymentStatus)) + " ";

        $("#showBills").append("<TR>" +
        "<TD>" + bill.id + "</TD>" +
        "<TD>" + getDate(bill.monthDate) + "</TD>" +
        "<TD>" + getStatusLabel("NEUTRAL", bill.monthName) + "</TD>" +
        "<TD>" + getPdfBillUrl(bill.invoiceId, bill.filePath) + "</TD>" +
        "<TD>" + getText(bill.syncedPwd) + "</TD>" +
        "<TD>" + getStatusLabel(bill.type) + "</TD>" +
        "<TD>" + status + "</TD>" +
        "<TD></TD>" +
        "</TR>");
    });
}

function showAll(customer, write) {
    if(!customer.cache){
        if(((!customer.apps || customer.apps.length == 0) && !customer.credit) && (!customer.pins || !customer.pins[0] || customer.pins[0].pin.length == 0)){
            hideUnnecessary("all");
            showToast("warning", "Not Available");
            return;
        }else{
            hideUnnecessary("careOnly");
        }
    }else{
        hideUnnecessary("none");
    }
    if($('input[name=mobile]').val() != ""){
        $("#customerTestsCart").show();
    }else{
        $("#customerTestsCart").hide();
    }
    $("#showComponentsCurrent tbody").empty();
    $("#showComponentsNext tbody").empty();
    $("#showBoosts tbody").empty();
    $("#showFreeBoosts tbody").empty();
    $("#showBonusesOnDemand tbody").empty();
    $("#showBonus tbody").empty();
    $("#showBonusFuture tbody").empty();
    $("#showGeneralsActive tbody").empty();
    $("#showGeneralsFuture tbody").empty();
    $("#showInvalidActive tbody").empty();
    $("#showInvalidFuture tbody").empty();
    $("#showPAYG tbody").empty();
    $("#showBills tbody").empty();
    $("#showInvoices tbody").empty();
    $("#showBssBills tbody").empty();
    $("#showCreditDebits tbody").empty();
    $("#generalActionsTable tbody").empty();
    $("#showCommercial tbody").empty();
    $("#showSelfcareSettings tbody").empty();
    $("#showPortInRequests tbody").empty();
    $("#showPortInRequests thead").empty();
    $("#showPortInNotifications tbody").empty();
    $("#showPortInNotifications thead").empty();
    $("#showReferredBy tbody").empty();
    $("#showBonusHistory tbody").empty();
    $("#showBonusHistoryReferrals tbody").empty();
    //clear data tables
    $("#accountNumbersTable").empty();
    $("#phoneNumbersTable").empty();
    $("#pinNumbersTable").empty();
    $("#pinNumbersTable thead").empty();
    $("#customerAccountTable").empty();
    $("#billingAccountTable").empty();
    $("#servieInstanceTable").empty();
    $("#activeTotal").text("");
    $("#leaderboardTotal").text("");
    $("#referralsTotal").text("");
    $("#showBonusTotal").text("");
    $("#showBonusFutureTotal").text("");
    $(".dataTables_paginate").closest(".row-fluid").remove();
    pairedNumbers = [];
    unPairedNumbers = [];
    numbersToICCID = {};

    if (customer.apps) showApps(customer.apps);
    cache = customer.cache;
    var appview = new Object;
    if (customer.appview) customer.appview.forEach(function (view) {
        if (view.type == "data") appview.data = view;
        else if (view.type == "other") appview.other = view;
        else if (view.type == "bills") appview.bills = view;
    });
    var base_data_left = 0;
    var base_data_total = 0;
    var base_data_prorated = 0;
    var base_voice_left = 0;
    var base_voice_total = 0;
    var base_sms_left = 0;
    var base_sms_total = 0;
    var extra_data_left = 0;
    var extra_data_total = 0;
    var extra_data_prorated = 0;
    var extra_voice_left = 0;
    var extra_voice_total = 0;
    var extra_sms_left = 0;
    var extra_sms_total = 0;
    var bonus_data_left = 0;
    var bonus_data_total = 0;
    var boost_data_left = 0;
    var boost_data_total = 0;
    var plus_data_left = 0;
    var plus_data_total = 0;
    if (appview && appview.data) {
        if (appview.data.basic) {
            base_data_left = parseInt(parseInt(appview.data.basic.left) / 1024);
            base_data_total = parseInt((parseFloat(appview.data.basic.left) + parseFloat(appview.data.basic.used)) / 1024);
                if (appview.data.basic.prorated_kb > 0) {
                        base_data_prorated = parseInt(parseFloat(appview.data.basic.prorated_kb) / 1024);
                }
        }
        if (appview.data.extra) {
            extra_data_left = parseInt(parseInt(appview.data.extra.left) / 1024);
            extra_data_total = parseInt((parseFloat(appview.data.extra.left) + parseFloat(appview.data.extra.used)) / 1024);
                if (appview.data.extra.prorated_kb > 0) {
                        extra_data_prorated = parseInt(parseFloat(appview.data.extra.prorated_kb) / 1024);
                }
        }
        if (appview.data.bonus) {
            bonus_data_left = parseInt(parseInt(appview.data.bonus.left) / 1024);
            bonus_data_total = parseInt((parseFloat(appview.data.bonus.left) + parseFloat(appview.data.bonus.used)) / 1024);
        }
        if (appview.data.boost) {
            boost_data_left = parseInt(parseInt(appview.data.boost.left) / 1024);
            boost_data_total = parseInt((parseFloat(appview.data.boost.left) + parseFloat(appview.data.boost.used)) / 1024);
        }
        if (appview.data.plus) {
            plus_data_left = parseInt(parseInt(appview.data.plus.left) / 1024);
            plus_data_total = parseInt((parseFloat(appview.data.plus.left) + parseFloat(appview.data.plus.used)) / 1024);
        }
    }
    if (appview && appview.other) {
        base_voice_left = parseInt(appview.other.basic.calls.base_left);
        base_voice_total = parseInt(appview.other.basic.calls.base_left) + parseInt(appview.other.basic.calls.base_used);
        base_sms_left = parseInt(appview.other.basic.sms.base_left);
        base_sms_total = parseInt(appview.other.basic.sms.base_left) + parseInt(appview.other.basic.sms.base_used);
        if (appview.other.basic && appview.other.basic.calls) {
            extra_voice_left = parseInt(appview.other.basic.calls.extra_left);
            extra_voice_total = parseInt(appview.other.basic.calls.extra_left) + parseInt(appview.other.basic.calls.extra_used);
        }
        if (appview.other.basic && appview.other.basic.sms) {
            extra_sms_left = parseInt(appview.other.basic.sms.extra_left);
            extra_sms_total = parseInt(appview.other.basic.sms.extra_left) + parseInt(appview.other.basic.sms.extra_used);
        }
    }
    var circlsTalkNumbers = [];
    customer.pins.forEach(function(item){
        circlsTalkNumbers.push(item.number);
    });
    if(customer && customer.pins){
        generateTable("pinNumbersTable", customer.pins, [
            {"sTitle": "Phone Number", "sClass": "twocol-table-column", "mData": "number", "mRender": function(data, type, full){
                    return data;
                }
            },
            {"sTitle": "PIN (Care/Talk)", "sClass": "twocol-table-column", "mData": "pin", "mRender": function(data, type, full){
                    return data ? data : "None";
                }
            }
        ]);
    }

    if (cache && cache.account) {

        if(write > 2){
            $("#profileUpdate").show();
            $("#addHistoryEntry").show();
            $("#terminateSuspendCart").show();
            $("#creditCapCart").show();
            $("#createPortinReq").show();
            $("#cancelPortinReq").show();
            $("#circlesTalkActionsCart").show();
        }else if(write > 1){
            $("#profileUpdate").hide();
            $("#addHistoryEntry").hide();
            $("#terminateSuspendCart").show();
            $("#creditCapCart").show();
            $("#createPortinReq").show();
            $("#cancelPortinReq").show();
            $("#circlesTalkActionsCart").show();
        }else{
            $("#profileUpdate").hide();
            $("#addHistoryEntry").hide();
            $("#terminateSuspendCart").hide();
            $("#creditCapCart").show();
            $("#createPortinReq").hide();
            $("#cancelPortinReq").hide();
            $("#circlesTalkActionsCart").hide();
        }

        generateTable("accountNumbersTable", [[
            cache.customerAccountNumber,
            cache.billingAccountNumber,
            cache.serviceAccountNumber,
            cache.serviceInstanceNumber
        ]], [
            {"sTitle": "CA No.", "sClass": "fourcol-table-column"},
            {"sTitle": "BA No.", "sClass": "fourcol-table-column"},
            {"sTitle": "SA No.", "sClass": "fourcol-table-column"},
            {"sTitle": "SI No.", "sClass": "fourcol-table-column"}
        ]);

        //Phone Numbers table
        var allNumbersData = [];

        generateTable("phoneNumbersTable", cache.serviceInstanceInventories, [
            {"sTitle": "Number", "sClass": "fourcol-table-column", "mData": "number", "mRender": function(data, type, full){
                    return ((cache.prefix != "65") ? "(" + cache.prefix + ")": "") + data;
                }
            },
            {"sTitle": "ICCID", "sClass": "fourcol-table-column", "mData": "number", "mRender": function(data, type, full){
                    return full.iccid ? full.iccid : getStatusLabel('MISSING', 'Unpaired');
                }
            },
            {"sTitle": "IMSI", "sClass": "fourcol-table-column", "mData": "number", "mRender": function(data, type, full){
                    return full.imsi ? full.imsi : getStatusLabel('MISSING', 'Not defined');
                }
            },
            {"sTitle": "Status", "sClass": "fourcol-table-column", "mData":"status", "mRender": function(data, type, full){
                    return data;
                }
            }
        ]);

        //Customer Account table
        var customerAccountData = [
            ["Name", cache.customerAccountFullName],
            ["Email", cache.customerAccountEmail],
            ["Birthday",  (cache.birthday ? cache.birthday : "None")],
            ["ID Type", cache.customerIdentity ? cache.customerIdentity.typeFormatted : ""],
            ["ID Number", cache.customerIdentity ? cache.customerIdentity.number : ""]
        ];

        customerAccountData.push(["Cache Source", cache.backupData ? getStatusLabel("WARNING", "DR (NOT REAL-TIME)") :  "BSS API"])

        $("#customerAccountTable").html("<thead><tr><th class='keyvalue-table-column'>Customer Account</th><th class='keyvalue-table-column'>" + cache.customerAccountNumber + "</th></tr></thead>");
        generateKeyValueTable("customerAccountTable", customerAccountData);

        //Billing Account table
        var billingAccountData = [
            ["Name", cache.billingFullName],
            ["Email", cache.customerAccountEmail],
            ["Cycle Start Day", cache.billingCycleStartDay],
            ["Address Line 1", cache.billingAddress ? cache.billingAddress.addressOne : ""],
            ["Address Line 2", cache.billingAddress ? cache.billingAddress.addressTwo : ""],
            ["City", cache.billingAddress ? cache.billingAddress.city : ""],
            ["Country", cache.billingAddress ? cache.billingAddress.country : ""],
            ["Postal Code", cache.billingAddress ? cache.billingAddress.postcode : ""],
            ["Account Full Token", cache.billingAddress ? cache.billingCreditCard.token : ""],
            ["Account Short Token", cache.billingAddress
                ? (cache.billingCreditCard.tokenShort ? cache.billingCreditCard.tokenShort : getStatusLabel("MISSING", "Missing")) : ""],
            ["Credit Card Bank", cache.billingAddress ? cache.billingCreditCard.bank : ""],
            ["Credit Card Type", cache.billingAddress ? cache.billingCreditCard.type : ""],
            ["Credit Card Number", cache.billingCreditCard ? "**** **** **** "+ cache.billingCreditCard.last4Digits : ""],
            ["Blacklist", customer.ecommBlacklist],
            ["Customer Bank Name", cache.billingCreditCard && cache.billingCreditCard.customerBankName ? cache.billingCreditCard.customerBankName : ""]
        ];

        $("#billingAccountTable").html("<thead><tr><th class='keyvalue-table-column'>Billing Account</th><th class='keyvalue-table-column'>" + cache.billingAccountNumber + "</th></tr></thead>");
        generateKeyValueTable("billingAccountTable", billingAccountData);

        if (cache.status) {
            cache.status = cache.status.toUpperCase();
        }
        if (cache.customerPortedStatus) {
            cache.customerPortedStatus = cache.customerPortedStatus.toUpperCase();
        }

        //Service Instance table
        var serviceInstanceData = [
            ["Name", cache.billingFullName],
            ["Email", cache.customerAccountEmail],
            ["Status", ((cache.status == "ACTIVE")? getStatusLabel('ACTIVE', cache.status) : getStatusLabel('NOT_ACTIVE', cache.status))],
            ["Checkout Date", cache.checkoutDate ? getDate(new Date(cache.checkoutDate)) : getStatusLabel('ERROR', 'EMPTY')],
            ["Instance Creation Date", cache.serviceInstanceCreationDate ? getDate(new Date(cache.serviceInstanceCreationDate)) : getStatusLabel('ERROR', "NONE")],
            ["Plan Activation Date", cache.activationDate ? getDate(new Date(cache.activationDate)) : getStatusLabel('ERROR', 'EMPTY')],
            ["Last Plan Switch Date", cache.serviceInstancePackageChangeDate ? getDate(new Date(cache.serviceInstancePackageChangeDate)) : getStatusLabel('ERROR', "NONE")],
            ["Earliest Termination Date", getDate(new Date(new Date(cache.serviceInstanceEarliestTerminationDate).getTime() - (8 * 60 * 60 * 1000) - 1000))],
            ["Roaming Cap", (cache.serviceInstanceRoaming ? "$ "+ cache.serviceInstanceRoaming.limit : "0")],
            ["Order Reference Number", cache.orderReferenceNumber ? cache.orderReferenceNumber : getStatusLabel('ERROR', 'EMPTY')],
            ["Port-In Status",  cache.customerPortedStatus ? getStatusLabel('NEUTRAL', cache.customerPortedStatus) : "None"],
            ["Port-Out Status",  customer.portOut ? getStatusLabel('WARNING', 'PORTED OUT') : getStatusLabel('NEUTRAL', 'NONE')],
            ["Referral Code Used", (cache.referralCode) ? cache.referralCode : "None"]
        ];

        $("#servieInstanceTable").html("<thead><tr><th class='keyvalue-table-column'>Service Instance</th><th class='keyvalue-table-column'>" + cache.serviceInstanceNumber + "</th></tr></thead>");
        generateKeyValueTable("servieInstanceTable", serviceInstanceData);

        //Components table
        var currentComponentsData = [];
        currentComponentsData.push([
            cache.base ? cache.base.name : "",
            fixMB(base_data_left) + " / " + fixMB(base_data_total),
            parseInt(base_voice_left / 60) + " / " + parseInt(base_voice_total / 60) + " Mins",
            base_sms_left + " / " + base_sms_total + " SMS"
        ]);
        if (base_data_prorated > 0) {
                currentComponentsData.push([
                    cache.base ? (cache.base.name + " Prorated") : "",
                    fixMB(base_data_left) + " / " + fixMB(base_data_prorated),
                    "None",
                    "None"
                ]);
        }
        currentComponentsData.push([
            "Components Names",
            (cache.extra_current ? ((cache.extra_current.data) ? cache.extra_current.data.name : "None") : ""),
            (cache.extra_current ? ((cache.extra_current.voice) ? cache.extra_current.voice.name : "None") : ""),
            (cache.extra_current ? ((cache.extra_current.sms) ? cache.extra_current.sms.name : "None") : "")
        ]);
        currentComponentsData.push([
            "Components Usage",
            fixMB(extra_data_left) + " / " + fixMB(extra_data_total),
            parseInt(extra_voice_left / 60) + " / " + parseInt(extra_voice_total / 60) + " Mins",
            extra_sms_left + " / " + extra_sms_total + " SMS"
        ]);
        currentComponentsData.push([
            "Bonus",
            fixMB(bonus_data_left) + " / " + fixMB(bonus_data_total),
            "",
            ""
        ]);
        currentComponentsData.push([
            "Boost",
            fixMB(boost_data_left) + " / " + fixMB(boost_data_total),
            "",
            ""
        ]);
        currentComponentsData.push([
            "Plus",
            fixMB(plus_data_left) + " / " + fixMB(plus_data_total),
            "",
            ""
        ]);

        generateTable("showComponentsCurrent", currentComponentsData, [
            {"sTitle": "Plan", "sClass": "fourcol-table-column"},
            {"sTitle": "Data Left / Data total", "sClass": "fourcol-table-column"},
            {"sTitle": "Voice Left / Voice Total", "sClass": "fourcol-table-column"},
            {"sTitle": "SMS Left / SMS Total)", "sClass": "fourcol-table-column"}
        ]);
        generateTable("showComponentsNext", [[
                "Components",
                (cache.extra ? ((cache.extra.data) ? cache.extra.data.name : "") : ""),
                (cache.extra ? ((cache.extra.voice) ? cache.extra.voice.name : "") : ""),
                (cache.extra ? ((cache.extra.sms) ? cache.extra.sms.name : "") : "")
            ]],
            [
            {"sTitle": "Plan", "sClass": "fourcol-table-column"},
            {"sTitle": "Data (left/total)", "sClass": "fourcol-table-column"},
            {"sTitle": "Voice (left/total)", "sClass": "fourcol-table-column"},
            {"sTitle": "SMS (left/total)", "sClass": "fourcol-table-column"}
        ]);

        if (cache.boost) cache.boost.sort(function (a, b) {
            if (a.billStartDate < b.billStartDate) return 1;
            else return -1;
        });

        generateTable("showBoosts", cache.boost ? cache.boost : "", [
            {"sTitle": "Name", "sClass": "fivecol-table-column", "mData": "name", "mRender": function(data, type, full){
                    return data;
                }
            },
            {"sTitle": "Start Date", "sClass": "fivecol-table-column", "mData": "billStartDate", "mRender": function(data, type, full){
                    return getDateTime(new Date(data)).replace(" ", "<br>");
                }
            },
            {"sTitle": "End Date", "sClass": "fivecol-table-column", "mData": "expiryDate", "mRender": function(data, type, full){
                    return getDateTime(new Date(data)).replace(" ", "<br>");
                }
            },
            {"sTitle": "History Id", "sClass": "fivecol-table-column", "mData": "packageHistoryId", "mRender": function(data, type, full){
                    return data;
                }
            },
            {"sTitle": "Action", "sClass": "fivecol-table-column", "mData": "id", "mRender": function(data, type, full){
                    return (full.packageHistoryId && full.packageHistoryId != "None" && write > 2) ? 
                    getTableActionButton('Unsub', undefined, "BONUS_UNSUBSCRIBE", {
                        number: cache.number, 
                        prefix: cache.prefix, 
                        product: data,
                        serviceInstanceNumber: cache.serviceInstanceNumber,
                        account: cache.account,
                        historyId: full.packageHistoryId,
                        productName: full.name
                    }) : "None";
                }
            }
        ], 10);

        if (customer.freeBoost) {
                generateTable("showFreeBoosts", customer.freeBoost, [
                    {"sTitle": "Id/Name", "sClass": "fivecol-table-column", "mData": "name", "mRender": function(data, type, full){
                            return full.id + "<br>" + data;
                        }
                    },
                    {"sTitle": "Added Date", "sClass": "fivecol-table-column", "mData": "added_ts", "mRender": function(data, type, full){
                            return ((new Date(data)).getTime() > 0 ? getDateTime(new Date(data)).replace(" ", "<br>") : "");
                        }
                    },
                    {"sTitle": "Used Date", "sClass": "fivecol-table-column", "mData": "used_ts", "mRender": function(data, type, full){
                            return ((new Date(data)).getTime() > 0 ? getDateTime(new Date(data)).replace(" ", "<br>") : "");
                        }
                    },
                    {"sTitle": "Action", "sClass": "fivecol-table-column", "mData": "id", "mRender": function(data, type, full){
                            return ((full.used && full.used != "None") ? "USED" : (full.processing && full.processing != "None") ? "PROC." : " - ");
                        }
                    }
                ], 5);
        }
        if (customer.bonusesOnDemand) {
                generateTable("showBonusesOnDemand", customer.bonusesOnDemand, [
                    {"sTitle": "Id/Name", "sClass": "fivecol-table-column", "mData": "product_name", "mRender": function(data, type, full){
                            return full.id + "<br>" + data;
                        }
                    },
                    {"sTitle": "Added Date", "sClass": "fivecol-table-column", "mData": "added_ts", "mRender": function(data, type, full){
                            return getDateTime(new Date(data)).replace(" ", "<br>");
                        }
                    },
                    {"sTitle": "Valid Date", "sClass": "fivecol-table-column", "mData": "valid_until_ts", "mRender": function(data, type, full){
                            return getDateTime(new Date(data)).replace(" ", "<br>");
                        }
                    },
                    {"sTitle": "Action", "sClass": "fivecol-table-column", "mData": "used", "mRender": function(data, type, full){
                            return (write > 2) ? ((data && data != "None") ? "USED" : (full.processing && full.processing != "None") ? "PROC." : getTableActionButton('Use', undefined, "USE_BONUS", {
                                number: cache.number, 
                                prefix: cache.prefix, 
                                id: full.id,
                                serviceInstanceNumber: cache.serviceInstanceNumber,
                                account: cache.account
                            })) : "None";
                        }
                    }
                ], 5);
        }

        generateGeneralActive("showGeneralsActive", "generalCurrent",
            cache.general ? cache.general : [], "generalsActiveTotal", write);
        generateGeneralActive("showGeneralsFuture", "generalFuture",
            cache.general_future ? cache.general_future : [], "generalsFutureTotal", write);
        generateGeneralActive("showBonus", "bonusCurrent",
            cache.bonus ? cache.bonus : [], "showBonusTotal", write);
        generateGeneralActive("showBonusFuture", "bonusFuture",
            cache.bonus_future ? cache.bonus_future : [], "showBonusFutureTotal", write);

        if (cache.invalid && cache.invalid.addons) {
            if (cache.invalid.addons.current) {
                cache.invalid.addons.current.sort(function (a, b) {
                        if (a.name + " " + a.billStartDate < b.name + " " + b.billStartDate) return -1; else return 1;
                });

                generateTable("showInvalidActive", cache.invalid.addons.current, [
                    {"sTitle": "Invalid Addons", "sClass": "fivecol-table-column", "mData": "name", "mRender": function(data, type, full){
                            return getStatusLabel("INVALID", data);
                        }
                    },
                    {"sTitle": "Start Date", "sClass": "fivecol-table-column", "mData": "billStartDate", "mRender": function(data, type, full){
                            return getDateTime(new Date(data));
                        }
                    },
                    {"sTitle": "End Date", "sClass": "fivecol-table-column", "mData": "expiryDate", "mRender": function(data, type, full){
                            return getDateTime(new Date(data));
                        }
                    },
                    {"sTitle": "History Id", "sClass": "fivecol-table-column", "mData": "id", "mRender": function(data, type, full){
                            return full.packageHistoryId ? full.packageHistoryId : "None";
                        }
                    },
                    {"sTitle": "Action", "sClass": "fivecol-table-column", "mData": "id", "mRender": function(data, type, full){
                            return (full.packageHistoryId && full.packageHistoryId != "None" && write > 2) ? 
                            getTableActionButton('Unsub', undefined, "ADDON_UNSUBSCRIBE", {
                                number: cache.number, 
                                prefix: cache.prefix, 
                                product: data,
                                serviceInstanceNumber: cache.serviceInstanceNumber,
                                account: cache.account,
                                historyId: full.packageHistoryId,
                                productName: full.name
                            }) : "None";
                        }
                    }
                ], 10);
            }
            if (cache.invalid.addons.future) {
                generateTable("showInvalidFuture", cache.invalid.addons.future, [
                    {"sTitle": "Invalid Addons", "sClass": "fivecol-table-column", "mData": "name", "mRender": function(data, type, full){
                            return getStatusLabel("INVALID", data);
                        }
                    },
                    {"sTitle": "Start Date", "sClass": "fivecol-table-column", "mData": "billStartDate", "mRender": function(data, type, full){
                            return getDateTime(new Date(data));
                        }
                    },
                    {"sTitle": "End Date", "sClass": "fivecol-table-column", "mData": "expiryDate", "mRender": function(data, type, full){
                            return getDateTime(new Date(data));
                        }
                    },
                    {"sTitle": "History Id", "sClass": "fivecol-table-column", "mData": "id", "mRender": function(data, type, full){
                            return full.packageHistoryId ? full.packageHistoryId : "None" ;
                        }
                    },
                    {"sTitle": "Action", "sClass": "fivecol-table-column", "mData": "name", "mRender": function(data, type, full){
                            return (full.packageHistoryId && full.packageHistoryId != "None" && write > 2) ? 
                            getTableActionButton('Unsub', undefined, "ADDON_UNSUBSCRIBE", {
                                number: cache.number, 
                                prefix: cache.prefix, 
                                product: data,
                                serviceInstanceNumber: cache.serviceInstanceNumber,
                                account: cache.account,
                                historyId: full.packageHistoryId,
                                productName: full.name
                            }) : "None";



                        }
                    }
                ], 10);
            }
        }
    }
    if (customer.selfcaresettings) {
        var selfcareColumnDefs = [
            {
                "sTitle": "Circles Care", "sClass": "twocol-table-column", "mData": "option_id", "mRender": function (data, type, full) {
                return data;
            }
            },
            {
                "sTitle": getTableActionButton("Update", "EDIT", "UPDATE_PROFILE_SETTINGS", {
                    serviceInstanceNumber: cache.serviceInstanceNumber,
                    account: cache.account,
                }),
                "sClass": "twocol-table-column", "mData": "value", "mRender": function (data, type, full) {
                return data;
            }
            }
        ];
        generateTable("showSelfcareSettings", customer.selfcaresettings, selfcareColumnDefs);
    }

    if (customer.referrals && customer.referrals.code) {
        addRows("showSelfcareSettings", {option_id : 'Referral Code', value : customer.referrals.code}, selfcareColumnDefs);
        if(customer.referrals.codeOld){
            addRows("showSelfcareSettings", {option_id : 'Old Referral Code', value : customer.referrals.codeOld}, selfcareColumnDefs);
        }
    }
    
    var commercialData = [];
    if (customer.creditcard) {
        commercialData.push(['Credit Card Number', "**** **** **** "+ customer.creditcard.credit_card_number]);
        commercialData.push(['Account Token', customer.creditcard.recurrent_token]);
    }
    var address = customer.billingaddress;
    if ( address ) {
        commercialData.push(['Address', address[0].hse_blk_tower + ","+ address[0].street_building_name + ", #" + address[0].floor_no +
         "-" + address[0].unit_no]);
        commercialData.push(['Address Updated',  getDateTime(address[0].updated_at)]);
    }


    $("#showCommercial").html("<thead><tr><th class='keyvalue-table-column'>Commercial</th><th class='keyvalue-table-column'></th></tr></thead>");
    generateKeyValueTable("showCommercial", commercialData);


    var as_use = (appview && appview.other) ? appview.other.pay_as_you_go : undefined;
    var asUseData = [];
    if (as_use) {
        var calls_incoming = (as_use.calls) ? 
                (as_use.calls.incoming.value / 60) + " Mins (" + as_use.calls.incoming.price_total.prefix + " " + as_use.calls.incoming.price_total.value.toFixed(2) + ")" : "None";
        var calls_outgoing = (as_use.calls) ?
                (as_use.calls.outgoing.value / 60) + " Mins (" + as_use.calls.outgoing.price_total.prefix + " " + as_use.calls.outgoing.price_total.value.toFixed(2) + ")" : "None";
        var sms = (as_use.sms) ?
                as_use.sms.total.value + " SMS (" + as_use.sms.total.price_total.prefix + " " + as_use.sms.total.price_total.value.toFixed(2) + ")" : "";
        asUseData.push(['Local', 'None', calls_incoming, calls_outgoing, sms]);

        var calls_idd_incoming = (as_use.calls_idd) ?
                (as_use.calls_idd.incoming.value / 60) + " Mins (" + as_use.calls_idd.incoming.price_total.prefix + " " + as_use.calls_idd.incoming.price_total.value.toFixed(2) + ")" : "None";
        var calls_idd_outgoing = (as_use.calls_idd) ?
                (as_use.calls_idd.outgoing.value / 60) + " Mins (" + as_use.calls_idd.outgoing.price_total.prefix + " " + as_use.calls_idd.outgoing.price_total.value.toFixed(2) + ")" : "None";
        var sms_idd = (as_use.sms_idd) ? as_use.sms_idd.total.value + " SMS (" + as_use.sms_idd.total.price_total.prefix + " " + as_use.sms_idd.total.price_total.value.toFixed(2) + ")" : "None";
        asUseData.push(['IDD', 'None', calls_idd_incoming, calls_idd_outgoing, sms_idd]);

        var data_roam = "None";
        if (as_use.data_roam) {
                var roaming_data = fixB(as_use.data_roam.total.value);
                data_roam =  roaming_data + " (" + as_use.data_roam.total.price_total.prefix + " " + as_use.data_roam.total.price_total.value.toFixed(2) + ")";
        }
        var calls_roam_incoming = (as_use.calls_roam) ?
                as_use.calls_roam.incoming.value + " Secs (" + as_use.calls_roam.incoming.price_total.prefix + " " + as_use.calls_roam.incoming.price_total.value.toFixed(2) + ")" : "None";
        var calls_roam_outgoing = (as_use.calls_roam) ?
                as_use.calls_roam.outgoing.value + " Secs (" + as_use.calls_roam.outgoing.price_total.prefix + " " + as_use.calls_roam.outgoing.price_total.value.toFixed(2) + ")" : "None";
        var sms_roam = (as_use.sms_roam) ?
                as_use.sms_roam.total.value + " SMS (" + as_use.sms_roam.total.price_total.prefix + " " + as_use.sms_roam.total.price_total.value.toFixed(2) + ")" : "None";
        asUseData.push(['Roaming', data_roam, calls_roam_incoming, calls_roam_outgoing, sms_roam]);
    }
    generateTable("showPAYG", asUseData,[
        {"sTitle": "PAYG", "sClass": "fivecol-table-column"},
        {"sTitle": "Data", "sClass": "fivecol-table-column"},
        {"sTitle": "Voice In", "sClass": "fivecol-table-column"},
        {"sTitle": "Voice Out", "sClass": "fivecol-table-column"},
        {"sTitle": "SMS", "sClass": "fivecol-table-column"}
    ]);

    if (customer
        && customer.payments
        && customer.payments.bills
        && customer.payments.instant_charges) {

        var appendRow = function (bill) {

            var button = !bill.payNow || bill.payNow.value <= 0
                ? ""
                : "<BUTTON class='btn btn-primary btn-xs' onClick='payNow(" +
                "\"" + cache.prefix + "\", " +
                "\"" + cache.number + "\", " +
                "\"" + cache.account + "\", " +
                "\"" + cache.serviceInstanceNumber + "\", " +
                "\"" + bill.payNow.value + "\", " +
                "\"" + bill.id + "\", " +
                "\"\");'>Pay</BUTTON>";

            $("#showInvoices").append("<TR>" +
            "<TD>" + getDate(new Date(bill.date)) + "</TD>" +
            "<TD>" + bill.label + "</TD>" +
            "<TD>" + bill.type + "</TD>" +
            "<TD>" + bill.id + "</TD>" +
            "<TD>" + bill.price.prefix + " " + bill.price.value.toFixed(2) + "</TD>" +
            "<TD>" + bill.correctedPrice.prefix + " " + bill.correctedPrice.value.toFixed(2) + "</TD>" +
            "<TD>" + bill.price.prefix + " " + (bill.price.value - bill.paid.value).toFixed(2) + "</TD>" +
            "<TD>" + button + "</TD>" +
            "<TD></TD>" +
            "</TR>");
        }

        var appendBssBillRow = function (bill) {
            $("#showBssBills").append("<TR>" +
            "<TD>" + getDate(new Date(bill.billDate)) + "</TD>" +
            "<TD>" + bill.billID + "</TD>" +
            "<TD>" + bill.billNumber + "</TD>" +
            "<TD>" + bill.billStatus + "</TD>" +
            "<TD>" + "$" + (bill.amount / 100) + "</TD>" +
            "<TD>" + "$" + (bill.unpaidAmount / 100) + "</TD>" +
            "<TD></TD>" +
            "</TR>");
        }

        if (customer.payments) {
            var allNeedToPay = [];
            if (customer.payments.pending && customer.payments.pending.bill) {
                if (customer.payments.pending.bill.payNow.value > 0) {
                    allNeedToPay.push({invoiceId: customer.payments.pending.bill.id,
                        amount: customer.payments.pending.bill.payNow.value});
                }
                appendRow(customer.payments.pending.bill);
            }

            if (customer.payments.bills) {
                customer.payments.bills.forEach(function (bill) {
                    if (bill.payNow && bill.payNow.value > 0) {
                        allNeedToPay.push({invoiceId: bill.id, amount: bill.payNow.value})
                    }
                    appendRow(bill);
                });
            }

            if (allNeedToPay.length > 0) {
                $("#paymentsActions").html("<BUTTON class='btn btn-primary btn-xs' onClick='payNow(" +
                "\"" + cache.prefix + "\", " +
                "\"" + cache.number + "\", " +
                "\"" + cache.account + "\", " +
                "\"" + cache.serviceInstanceNumber + "\", " +
                "\"\", " +
                "\"\"," +
                "\"" + JSON.stringify(allNeedToPay).replace(/'/g, "%27").replace(/\"/g, "\\\"") + "\");'>Pay All</BUTTON>");
            } else {
                $("#paymentsActions").html("");
            }

            if (customer.payments.bssBills) customer.payments.bssBills.forEach(function (bill) {
                appendBssBillRow(bill);
            });
        }

        var total = 0;
        if (customer.payments.debit_credit_history && customer.payments.debit_credit_history.list) {
            customer.payments.debit_credit_history.list.forEach(function (item) {
                $("#showCreditDebits").append("<TR>" +
                "<TD>" + getDate(new Date(item.postDate)) + "</TD>" +
                "<TD>" + item.type + "</TD>" +
                "<TD>" + item.documentNumber + "</TD>" +
                "<TD>" + item.documentType + "</TD>" +
                "<TD>" + (item.type === "Credit" && item.documentType !== "Cancel Payment"
                    ? "$ +" + parseInt(item.amount) / 100 : "") + "</TD>" +
                "<TD>" + (item.type === "Debit" || item.documentType === "Cancel Payment"
                    ? "$ " + (parseInt(item.amount) > 0 ? "-" : "") + parseInt(item.amount) / 100 : "")  + "</TD>" +
                "</TR>");


                if (item.type === "Credit") {
                    total += parseInt(item.amount);
                } else {
                    total -= parseInt(item.amount);
                }
            });
        }

        $("#titleCreditDebits").html("  (" + (total > 0 ? "Overpaid" : "Outstanding") + ": $ " + (parseInt(total) / 100) + ")");
    }

    var referrerData = [];
    if (customer.referrals && customer.referrals.referrer) {
        var item = customer.referrals.referrer;
        referrerData.push({
            id: item.id,
            serviceInstanceNumber: item.service_instance_number,
            name: item.name,
            prefix: item.prefix,
            number: item.number
        });
    }

    generateTable("showReferredBy", referrerData, [
        {
            "sTitle": "Referred By", "sClass": "fivecol-table-column", "mData": "id", "mRender": function (data, type, full) {
            return data;
        }
        },
        {
            "sTitle": "", "sClass": "fivecol-table-column", "mData": "id", "mRender": function (data, type, full) {
            return "";
        }
        },
        {
            "sTitle": "Number", "sClass": "fivecol-table-column", "mData": "id", "mRender": function (data, type, full) {
            return full.number ? getCustomerNumberLink(full.number) : "None";
        }
        },
        {
            "sTitle": "SIN", "sClass": "fivecol-table-column", "mData": "id", "mRender": function (data, type, full) {
            return getCustomerAccountLink(full.serviceInstanceNumber);
        }
        },
        {
            "sTitle": "Action", "sClass": "fivecol-table-column", "mData": "id", "mRender": function (data, type, full) {
            return (write > 2) ? getTableActionButton('Delete', undefined, "DELETE_BONUS_HISTORY", {
                number: full.number,
                prefix: full.prefix,
                id: data
            }) : "None";
        }
        }
    ], 10);

    if (customer.bonusHistory) {

        var total = 0;
        var totalLeaderboard = 0;
        var totalReferrlas = 0;
        var referralsList = [];

        customer.bonusHistory.forEach(function (item) {
            var leaderboard = item.leaderboard_included && item.active;

            if (item.active) {
                total += getCorrectedKB(item.product_kb);
            }
            if (leaderboard) {
                totalLeaderboard += getCorrectedKB(item.product_kb);
            }

            if (item.product_type === "referral") {
                referralsList.push(item);
                if (!item.deactivated_on) {
                    totalReferrlas += getCorrectedKB(item.product_kb);
                }
            }
        });

        generateTable("showBonusHistory", customer.bonusHistory, [
            {"sTitle": "Id / Data", "sClass": "fourcol-table-column", "mData": "id", "mRender": function(data, type, full){
                    var leaderboard = full.leaderboard_included && full.active;
                    return data + " /<br>" + (full.product_kb / 1024) + "MB" + (leaderboard ? " *" : "");
                }
            },
            {"sTitle": "Type / Subtype", "sClass": "fourcol-table-column", "mData": "id", "mRender": function(data, type, full){
                return full.product_type + (full.product_sub_type ? " /<br>" +  full.product_sub_type : "");
            }
            },
            {"sTitle": "Start Date", "sClass": "fourcol-table-column", "mData": "added_date", "mRender": function(data, type, full){
                    return getDateTime(new Date(data)).replace(" ", "<br>");
                }
            },
            {"sTitle": "End Date", "sClass": "fourcol-table-column", "mData": "id", "mRender": function(data, type, full){
                    return ((full.valid_until && full.valid_until != "None") ? getDateTime(new Date(full.valid_until)).replace(" ", "<br>")
                    : ((full.deactivated_on && full.valid_until != "None") ? getDateTime(new Date(full.deactivated_on)).replace(" ", "<br>") : ""));
                }
            },
            {"sTitle": "Action", "sClass": "fourcol-table-column", "mData": "id", "mRender": function(data, type, full){
                    return (write > 2) ? getTableActionButton('Delete', undefined, "DELETE_BONUS_HISTORY", {
                        number: cache.number, 
                        prefix: cache.prefix,
                        id: data
                    }) : "None";
                }
            }
        ], 10);

        generateTable("showGoldenTicketsHistory", customer.goldenTicketsHistory, [
            {"sTitle": "Id", "sClass": "fourcol-table-column", "mData": "id", "mRender": function(data, type, full){
                return data;
            }
            },
            {"sTitle": "Month Key", "sClass": "fourcol-table-column", "mData": "id", "mRender": function(data, type, full){
                return getStatusLabel("NEUTRAL", full.monthKey);
            }
            },
            {"sTitle": "Status", "sClass": "fourcol-table-column", "mData": "added_date", "mRender": function(data, type, full){
                return getStatusLabel(full.status);
            }
            },
            {"sTitle": "Added Date", "sClass": "fourcol-table-column", "mData": "id", "mRender": function(data, type, full){
                return getDateTime(new Date(full.addedAt)).replace(" ", "<br>");
            }
            },
            {"sTitle": "Action", "sClass": "fourcol-table-column", "mData": "id", "mRender": function(data, type, full){
                return (write > 2) ? getTableActionButton('Delete', undefined, "DELETE_GOLDEN_TICKET", {
                    id: data
                }) : "None";
            }
            }
        ], 10);

        generateTable("showBonusHistoryReferrals", referralsList, [
            {"sTitle": "Id / Data", "sClass": "fivecol-table-column", "mData": "id", "mRender": function(data, type, full){
                var leaderboard = full.leaderboard_included && full.active;
                return data + " / " + (full.product_kb / 1024) + "MB " + (leaderboard ? "*" : "");
                }
            },
            {"sTitle": "Type", "sClass": "fivecol-table-column", "mData": "id", "mRender": function(data, type, full){
                return full.product_type + (full.product_sub_type ? " " + full.product_sub_type : "") ;
            }
            },
            {"sTitle": "Number", "sClass": "fivecol-table-column", "mData": "id", "mRender": function(data, type, full){
                return ((full.metadata3 && full.metadata3 != "None") ? getCustomerNumberLink(full.metadata3) : "None");
            }
            },
            {"sTitle": "SIN", "sClass": "fivecol-table-column", "mData": "id", "mRender": function(data, type, full){
                return ((full.metadata6 && full.metadata6 != "None") ? getCustomerAccountLink(full.metadata6) : "None") ;
            }
            },
            {"sTitle": "Action", "sClass": "fivecol-table-column", "mData": "id", "mRender": function(data, type, full) {
                return (write > 2) ? getTableActionButton('Delete', undefined, "DELETE_BONUS_HISTORY", {
                    number: cache.number,
                    prefix: cache.prefix,
                    id: data
                }) : "None";
            }
            }
        ], 10);

        if (total) {
            $("#activeTotal").text("Total: " + convertKToGb(total) + "GB (active)");
        }

        if (customer.bonusAvail) {
            $("#bonusAvailed").text("Total bonus Availed : " + customer.bonusAvail + " GB");
        }

        if (totalLeaderboard) {
            $("#leaderboardTotal").text("Total*: " + convertKToGb(totalLeaderboard) + "GB (leaderboard)");
        }

        if (totalReferrlas) {
            $("#referralsTotal").text("Total*: " + convertKToGb(totalReferrlas) + "GB (leaderboard)");
        }
    }

    if (write) {
        if (cache && cache.account) {
            unpairedNumbers = [];
            pairedNumbers = [];
            if (cache.serviceInstanceInventories && cache.serviceInstanceInventories.length > 0) {
                cache.serviceInstanceInventories.forEach(function(item) {
                    if (!item.iccid) {
                        if(unpairedNumbers.indexOf(item.number) == -1){
                            unpairedNumbers.push(item.number);
                        }
                    }else{
                        if(pairedNumbers.indexOf(item.number) == -1){
                            pairedNumbers.push(item.number);
                            numbersToICCID[item.number] = item.iccid;
                        }
                    }
                });
            }
            var genOptions = [];
            var freeBoostOptions = [];
            var generalDefaultValues = {};
            customer.generalAvailable.forEach(function (general) {
                genOptions.push({key: general.id, value: general.name});
                generalDefaultValues[general.id] = {overrideEffect: (general.sub_effect) ? general.sub_effect.toString(): "", overrideRecurrent: ((general.recurrent == true) ? "true" : "false")};
            });

            customer.freeBoostAvailable.forEach(function (freeBoost) {
                    for (var i = 0; i < 5; i++) {
                        freeBoostOptions.push({
                            key: freeBoost.id + ":x:" + (i + 1),
                            value: freeBoost.name + " x " + (i + 1)
                        });
                    }
            });

            var bonusesOptionsHtml = getOptionSelect('bonusesOptions', [
                {key: 'CARE_INSTALL_BONUS', value: 'Add Selfcare Install Bonus'},
                {key: 'ADD_PORTIN_BONUS', value: 'Add Portin Bonus'},
                {key: 'ADD_PORTIN_BONUS_EXTRA', value: 'Add Portin Extra Bonus'},
                {key: 'ADD_CONTRACT_BUSTER_BONUS', value: 'Add Contract Breakup Bonus'},
                {key: 'BIRTHDAY_BONUS', value: 'Add Birthday Bonus'},
                {key: 'ADD_LOYALITY_BONUS', value: 'Add Loyalty Bonus'},
                {key: 'ADD_LAZADA_BONUS', value: 'Add Lazada Bonus'},
                {key: 'ADD_PENDING_BONUS', value: 'Add Pending Bonus'},
                {key: 'DEACTIVATE_REFERRER_BONUS', value: 'Deactivate Referrer Bonus'},
                {key: 'WINBACK_BONUS', value: 'Add Windback Bonus'}
            ], 'bonusesOptions');
            //general actions table
            var generalActionsData = [
                ["Customer Cache", getTableActionButtonWithoutCallback('deleteCache', 'Delete', undefined) +
                getTableActionButton('Reload', undefined, 'RECACHE', {
                    reload: true,
                    prefix: cache.prefix,
                    number: cache.number,
                    account: cache.account,
                    serviceInstanceNumber: cache.serviceInstanceNumber
                })]
            ];

            generalActionsData.push(["Credit Card Update Link", getTableActionButtonWithoutCallback('ccUpdateLinkBtn','Generate', undefined)]);
            if(write > 1){
                var autoboostActionSet = [];

                autoboostActionSet.push(getTableActionButton('Add', undefined, 'ADD_AUTO_BOOST', {
                    number: cache.number, 
                    prefix: cache.prefix, 
                    checkData: true,
                    enable: false,
                    serviceInstanceNumber: cache.serviceInstanceNumber,
                    account: cache.account
                }));

                autoboostActionSet.push(getTableActionButton('Force Add', undefined, 'ADD_AUTO_BOOST', {
                    number: cache.number, 
                    prefix: cache.prefix, 
                    checkData: false,
                    enable: false,
                    serviceInstanceNumber: cache.serviceInstanceNumber,
                    account: cache.account
                }));

                autoboostActionSet.push(getTableActionButton('Enable', undefined, 'ADD_AUTO_BOOST', {
                    number: cache.number, 
                    prefix: cache.prefix, 
                    checkData: false,
                    enable: true,
                    serviceInstanceNumber: cache.serviceInstanceNumber,
                    account: cache.account
                }));

                generalActionsData.push(["Autoboost", autoboostActionSet.join("")]);

                generalActionsData.push(["Consume Data", getTableActionButton('Consume', undefined, 'CONSUME_DATA', {
                    number: cache.number, 
                    prefix: cache.prefix,
                    serviceInstanceNumber: cache.serviceInstanceNumber,
                    account: cache.account
                })]);

                generalActionsData.push(["Credit Notes", getTableActionButton('Add', undefined, 'ADD_CREDIT_NOTES', {
                    number: cache.number, 
                    prefix: cache.prefix,
                    serviceInstanceNumber: cache.serviceInstanceNumber,
                    account: cache.account,
                    amount: 0
                })]);

                if(write > 2){
                    generalActionsData.push(["Number Change", getTableActionButtonWithoutCallback('numberChange', "Change", undefined)]);

                    generalActionsData.push(["SIM Change", getTableActionButtonWithoutCallback('simChange', "Change", undefined)]);
                    
                    // generalActionsData.push(["SIM Damage", getTableActionButton('Confirm', undefined, 'CONFIRM_SIM_DAMAGE', {
                    //     serviceInstanceNumber: cache.serviceInstanceNumber
                    // })]);

                }
            }

            if (customer.referrals && customer.referrals.code && write > 1) {
                $("#useReferalCode").show();
            }else{
                $("#useReferalCode").hide();
            }
            if(write > 1){
                generalActionsData.push(["Bonuses", bonusesOptionsHtml]);
            }

            if(write >= 1){
                

                var roamingDropDown = [
                    {key: 'off', value: 'Off'},
                    {key: 'voicesms', value: 'Voice and SMS'},
                    {key: 'datavoicesms', value: 'Data, Voice and SMS'},
                    {key: 'pending', value: 'Request Pending'}
                ];

                if(customer.customerOptions["roaming_auto"] && customer.customerOptions["roaming_auto"] == "1" && customer.customerOptions["roaming_data"] && customer.customerOptions["roaming_data"] == "1"){
                    roamingOption = "datavoicesms";
                }else if(customer.customerOptions["roaming_data"] && customer.customerOptions["roaming_data"] != "1" && customer.customerOptions["roaming_data"] != "0"){
                    roamingOption = "pending";
                }else if(customer.customerOptions["roaming_auto"] && customer.customerOptions["roaming_auto"] != "1" && customer.customerOptions["roaming_auto"] != "0"){
                    roamingOption = "pending";
                }else if(customer.customerOptions["roaming_auto"] && customer.customerOptions["roaming_auto"] == "1"){
                    roamingOption = "voicesms";
                }else{
                    roamingOption = "off";
                }

                generalActionsData.push(["Roaming Settings", getOptionSelectWithDisabledOptions('roamingSettings', roamingDropDown, 'optionSelect', roamingOption, true, ['pending'])]);

                
            }

            if(write > 1){
                generalActionsData.push(["Roaming Cap", getOptionSelect('roamingCap', [
                    {key: '100', value: '100'},
                    {key: '300', value: '300'},
                    {key: '600', value: '600'},
                    {key: '900', value: '900'},
                    {key: '1200', value: '1200'}
                ], 'optionSelect')]);

                generalActionsData.push(["Add Free Boost", getOptionSelect('freeBoost', freeBoostOptions, 'optionSelect')]);

                generalActionsData.push(["Add General", getOptionSelect('addGeneral', genOptions, 'optionSelect')]);

                generalActionsData.push(["Send Notification", getOptionSelect('notifier', [
                    {key: 'DEL_FAIL', value: 'Delivery Failed'},
                    {key: 'DOC_APPROVAL_FAIL', value: 'Document Approval Failed'},
                    {key: 'PORTIN', value: 'Portin'}
                ], 'optionSelect')]);

                generalActionsData.push(["Block Notifications", getOptionSelect('blockNot', [
                    {key: '120', value: '120 sec'}
                ], 'optionSelect', (customer.noNotif ? "120" : ""))]);
            }
            
            $("#generalActionsTable").html("<thead><tr><th class='keyvalue-table-column'>Action</th><th class='keyvalue-table-column'></th></tr></thead>");
            generateKeyValueTable("generalActionsTable", generalActionsData);

            if(write > 1){
                terminateSuspendTable = [];
                var terminationDate = cache.serviceInstanceEarliestTerminationDate ? new Date(new Date(cache.serviceInstanceEarliestTerminationDate).getTime() - (16 * 60 * 60 * 1000) - 1000) : undefined;
                var customValidator = function(fields, params){
                    if(fields && params){
                        for (var i = 0; i < fields.length; i++) {
                            var field = fields[i];
                            var item = field.children[1].children[0];
                            var key = item.getAttribute('name');
                            if(key == "verificationNumber"){
                                var verificationNumber = item.value;
                                break;
                            }
                        }
                        return (verificationNumber == params.number) ? true : genUserMessage('TERMINATION_VERIFICATION_FAILED', 
                        {searched: params.number, typed: verificationNumber});
                    }else{
                        return true;
                    }
                }
                if(customer.termination){
                    terminateSuspendTable.push(['Termination', getStatusLabel("INVALID", "Termination Scheduled " + customer.termination.slice(0, -1))]);
                }else{
                    var terminateRow = getTableActionButtonWithoutCallback("terminateNumber", 'Terminate', undefined);
                    if(write > 2){
                        terminateRow += getTableActionButtonWithoutCallback("terminateNumberImmediately", 'Terminate NOW', undefined);
                    }
                    terminateSuspendTable.push(["Termination", terminateRow]);
                }
                if(cache.status === "ACTIVE"){
                    terminateSuspendTable.push(["Suspend", getTableActionButtonWithoutCallback("suspendNumber", 'Suspend', undefined)]);
                }else{
                    terminateSuspendTable.push(["Activate", getTableActionButton('Activate', undefined, 'ACTIVATE_ACCOUNT', {
                        number: cache.number, 
                        prefix: cache.prefix,
                        serviceInstanceNumber: cache.serviceInstanceNumber,
                        account: cache.account,
                        months: 0,
                        billing_cycle: cache.billing_cycle,
                        type: "Active",
                        earliestTerminationDate: cache.serviceInstanceEarliestTerminationDate
                    })]);
                }
                terminateSuspendTable.push(["Validate Status", getTableActionButtonWithoutCallback("validateStatus", 'Validate', undefined)]);
                generateKeyValueTable("terminateSuspendTable", terminateSuspendTable);

                if(cache.status === "ACTIVE"){
                    $("#suspendNumber").click(function(){
                        generateConfirmationDialog('SUSPEND_ACCOUNT', {
                            number: cache.number, 
                            prefix: cache.prefix,
                            serviceInstanceNumber: cache.serviceInstanceNumber,
                            account: cache.account,
                            billing_cycle: cache.billing_cycle,
                            type: "Suspended",
                            earliestTerminationDate: cache.serviceInstanceEarliestTerminationDate,
                            customValidator: customValidator
                        }, reloadPage);
                    });
                }

                $("#validateStatus").click(function(){
                    generateConfirmationDialog('VALIDATE_ACCOUNT_STATUS', {
                        number: cache.number,
                        prefix: cache.prefix,
                        serviceInstanceNumber: cache.serviceInstanceNumber,
                        account: cache.account
                    }, reloadPage);
                });
                
                if(!customer.termination){
                    $("#terminateNumber").click(function(){
                        generateConfirmationDialog('TERMINATE_ACCOUNT', {
                            number: cache.number, 
                            prefix: cache.prefix,
                            serviceInstanceNumber: cache.serviceInstanceNumber,
                            account: cache.account,
                            billing_cycle: cache.billing_cycle,
                            type: 'Terminated',
                            terminationMessage: genUserMessage('SCHEDULE_TERMINATION', {number: cache.number}) + getDate(terminationDate),
                            effective: terminationDate,
                            volunteer: "false",
                            customValidator: customValidator
                        }, reloadPage);
                    });
                    if(write > 2){
                        $("#terminateNumberImmediately").click(function(){
                            generateConfirmationDialog('TERMINATE_ACCOUNT', {
                                number: cache.number, 
                                prefix: cache.prefix,
                                serviceInstanceNumber: cache.serviceInstanceNumber,
                                account: cache.account,
                                billing_cycle: cache.billing_cycle,
                                type: 'Terminated',
                                terminationMessage: genUserMessage('IMMIDATE_TERMINATION', {number: cache.number}),
                                effective: new Date("1970-01-01"),
                                volunteer: "true",
                                customValidator: customValidator
                            }, reloadPage);
                        });
                    }
                }
            }

            var chargesAmount = customer.pay200Charges && customer.pay200Charges.amountCents
                ? (customer.pay200Charges.amountCents / 100) : 0;
            var paymentsAmount = customer.pay200Payments && customer.pay200Payments.paymentsCents
                ? (customer.pay200Payments.paymentsCents / 100) : 0;

            var creditCapActions = [];
            creditCapActions.push(["Credit Usage", "$" + chargesAmount]);
            creditCapActions.push(["Credit Payments equal $200", "$" + paymentsAmount]);
            creditCapActions.push(["Credit Cap Payment",
                getTableActionButton('Make Payment', undefined, 'MAKE_CREDIT_CAP_PAYMENT', {
                    number: customer.number,
                    prefix: customer.prefix
                })]);
            generateKeyValueTable("creditCapActions", creditCapActions);

            if(customer.pay200 && customer.pay200.length > 0){
                generateTable("creditCapHistoryTable", customer.pay200, [
                    {"sTitle": "Date", "sClass": "fourcol-table-column", "mData": "ts", "mRender": function(data, type, full){
                            return getDateTime(data).replace(" ", "<br>");
                        }
                    },
                    {"sTitle": "Status", "sClass": "fourcol-table-column", "mData": "activity", "mRender": function(data, type, full){
                            return getStatusLabel(full.status);
                        }
                    },
                    {"sTitle": "Amount", "sClass": "fourcol-table-column", "mData": "addonprice", "mRender": function(data, type, full){
                            return "$" + full.amount;
                        }
                    }
                ], 5);
            }

            if(write > 2){
                $('#numberChange').click(function(){
                    phoneNumberOrSimChange("number");
                });

                $('#simChange').click(function(){
                    showLoadingLabel();
                    getFreeICCID(function (iccid) {
                        hideLoadingLabel();
                        phoneNumberOrSimChange("sim", iccid);
                    });

                });

                function phoneNumberOrSimChange(changeType, iccid){
                    var paramList = {
                        prefix: cache.prefix,
                        serviceInstanceNumber: cache.serviceInstanceNumber,
                        account: cache.account,
                        email: cache.email,
                        numberList: pairedNumbers,
                        iccidMap: JSON.stringify(numbersToICCID),
                        billing_cycle: cache.billing_cycle,
                        category: numberChangeCategory,
                        type: changeType,
                        customValidator: function(fields, params){
                            if(fields && params){
                                for (var i = 0; i < fields.length; i++) {
                                    var field = fields[i];
                                    var item = field.children[1].children[0];
                                    var key = item.getAttribute('name');
                                    if(key == "newNumber" && params.type == "number" && (item.value < 10000000 || item.value > 99999999)){
                                        return genUserMessage("INVALID_PHONE_NUMBER");
                                    }else if(key == "newIccid" && params.type == "sim" && (item.value < 896503000000000000 || item.value > 896503999999999999)){
                                        return genUserMessage("INVALID_SIM_NUMBER");
                                    }
                                }
                                return true;
                            }else{
                                return true;
                            }
                        }
                    };
                    if(changeType == "sim"){
                        var dialogKey = "SIM_CHANGE";
                        paramList.newIccid = iccid;
                        paramList.charge = "true";
                        paramList.name = cache.billingFullName;
                    }else{
                        var dialogKey = "PHONE_NUMBER_CHANGE";
                    }
                    generateConfirmationDialog(dialogKey, paramList,
                        undefined,
                        function(err, data, cancel){
                            if (!cancel && data && (data.code == 0)) {
                                if (typeof(data.category) != "undefined") {
                                    if (data.category != "") {
                                        $("#numberChange").html(data.category);
                                        numberChangeCategory = data.category;
                                        showToast("success", genUserMessage('NUMBER_CHANGE_CATEGORY', {category: data.category}));
                                    } else {
                                        showToast("warning", "Unknown Category");
                                        search("mobile", data.prefix, data.number);
                                    }
                                } else {
                                    showToast("success", "Success!");
                                    search("mobile", data.prefix, data.number);
                                }
                            } else if(!cancel){
                                showToast("warning", JSON.stringify(data.error));
                            }
                        }
                    );
                }
            }


            $('#ccUpdateLinkBtn').click(function(){
                generateConfirmationDialog('CREATE_UPDATE_CC_LINK', {
                        number: cache.number, 
                        prefix: cache.prefix,
                        account: cache.account,
                    },
                    undefined,
                    function(err, data){
                        if(!err){
                            showInfoDialog(genUserMessage('CC_URL_VALID_TITLE'), `${data.url}&returnurl=${data.returnUrl}`);
                        }
                    }
                );
            });

            $('#deleteCache').click(function(){
                generateConfirmationDialog('RECACHE', {
                    reload: false,
                    prefix: cache.prefix,
                    number: cache.number,
                    account: cache.account,
                    serviceInstanceNumber: cache.serviceInstanceNumber
                }, function(){
                    location.href = "/customers/customers.html";
                });
            });

            $('#roamingSettings').change(function (){
                var idOfThis = this.id;
                var roamingSetMsg = (roamingOption == "pending") ? "There is a pending roaming request. Do you want to overide" : "Do you want to set roaming to " + this.value + "";
                generateConfirmationDialog('UPDATE_ROAMING_SETTINGS', {
                    number: cache.number, 
                    prefix: cache.prefix, 
                    type: this.value,
                    serviceInstanceNumber: cache.serviceInstanceNumber,
                    account: cache.account,
                    roamingSetMsg: roamingSetMsg
                }, reloadPage, function(){
                    $("#" + idOfThis).val($('#' + idOfThis + ' option[value=' + roamingOption + ']').val());
                });
            });

            $('#roamingCap').change(function (){
                var idOfThis = this.id;
                generateConfirmationDialog('UPDATE_ROAMING_CAP', {
                    number: cache.number, 
                    prefix: cache.prefix, 
                    limit: this.value,
                    serviceInstanceNumber: cache.serviceInstanceNumber,
                    account: cache.account
                }, reloadPage, function(){
                    $("#" + idOfThis).val($("#" + idOfThis + " option:first").val());
                });
            });

            $('#freeBoost').change(function (){
                var name = $("#freeBoost option:selected").text();;
                var params = this.value.split(":x:");
                var id = params[0];
                var count = params[1];
                var idOfThis = this.id;
                generateConfirmationDialog('ADD_FREE_BOOST', {
                    name: name, 
                    id: id, 
                    count: count,
                    number: cache.number,
                    prefix: cache.prefix,
                    account: cache.account,
                    serviceInstanceNumber: cache.serviceInstanceNumber
                }, reloadPage, function(){
                    $("#" + idOfThis).val($("#" + idOfThis + " option:first").val());
                });
            });

            $('#addGeneral').change(function (){
                var idOfThis = this.id;
                var productId = $(this).find("option:selected").attr('value');
                var productName = $(this).find("option:selected").text();
                var paramsForDialog = {
                    number: cache.number, 
                    prefix: cache.prefix, 
                    product: this.value,
                    overrideEffect: generalDefaultValues[productId].overrideEffect,
                    overrideRecurrent: generalDefaultValues[productId].overrideRecurrent,
                    productName: productName,
                    serviceInstanceNumber: cache.serviceInstanceNumber,
                    account: cache.account
                };
                ajax("GET", "/api/1/web/customers/addon/exist/" + cache.prefix + "/" + cache.number + "/" + productId, {}, true, function (data) {
                    if ( data && data.code == 0 && data.subscribed) {
                        showDecisionDialog("Addon Status", genUserMessage("ADDON_ALREADY_ADDED_FOR_MONTH"), function(){
                            generateConfirmationDialog('ADDON_UPDATE', paramsForDialog, reloadPage, function(){
                                $("#" + idOfThis).val($("#" + idOfThis + " option:first").val());
                            });
                        });
                    }else{
                        generateConfirmationDialog('ADDON_UPDATE', paramsForDialog, reloadPage, function(){
                            $("#" + idOfThis).val($("#" + idOfThis + " option:first").val());
                        });
                    }
                });
            });

            $("#blockNot").change(function () {
                var status = this.value == "120" ? true: false;
                generateConfirmationDialog('BLOCK_NOTIFICATIONS', {
                    number: cache.number, 
                    prefix: cache.prefix, 
                    block: status,
                    serviceInstanceNumber: cache.serviceInstanceNumber,
                    account: cache.account
                }, reloadPage, function(){
                    $("#blockNot").val($("#target option:first").val());
                });
            });

            $('#notifier').change(function (){
                var params = {
                    number: cache.number, 
                    prefix: cache.prefix,
                    serviceInstanceNumber: cache.serviceInstanceNumber,
                    account: cache.account
                };
                switch(this.value){
                    case "DEL_FAIL":
                        params.type = "delivery";
                        generateConfirmationDialog('RESEND_FROM_ECOMM', params, reloadPage, selectEmptyNotifier);
                        break;
                    case "DOC_APPROVAL_FAIL":
                        params.type = "document";
                        generateConfirmationDialog('RESEND_FROM_ECOMM', params, reloadPage, selectEmptyNotifier);
                        break;
                    case "PORTIN":
                        params.portInPrefix = cache.prefix;
                        params.unpairedNumbers = unpairedNumbers;
                        params.customValidator = function(fields, params){
                            if(fields){
                                var unpairedSelected;
                                var unpairedTyped;
                                for (var i = 0; i < fields.length; i++) {
                                    var field = fields[i];
                                    var item = field.children[1].children[0];
                                    var key = item.getAttribute('name');
                                    if(key == "portInNumberSelected"){
                                        unpairedSelected = item.value;
                                    }else if(key == "portInNumberTyped"){
                                        unpairedTyped = item.value;
                                    }
                                }
                                if(!unpairedSelected && !unpairedTyped){
                                    return genUserMessage("SELECT_OR_TYPE_NUMBER");
                                }else{
                                    return true;
                                }
                            }else{
                                return true;
                            }
                        }
                        generateConfirmationDialog('SEND_PORT_IN_NOTIFICATION', params, reloadPage, selectEmptyNotifier);
                        break;
                    default:
                        break;
                }
            });

            function selectEmptyNotifier(){
                $('#notifier').val($("#target option:first").val());
            };

            $('#bonusesOptions').change(function (){
                var params = {
                    number: cache.number, 
                    prefix: cache.prefix, 
                    onegb: false,
                    serviceInstanceNumber: cache.serviceInstanceNumber,
                    account: cache.account
                };
                switch(this.value){
                    case "CARE_INSTALL_BONUS":
                        params.onegb = false;
                        generateConfirmationDialog('CARE_INSTALL_BONUS', params, reloadPage, selectEmpty);
                        break;
                    case "ADD_PORTIN_BONUS":
                        params.extra = false;
                        generateConfirmationDialog('ADD_PORTIN_BONUS', params, reloadPage, selectEmpty);
                        break;
                    case "ADD_PORTIN_BONUS_EXTRA":
                        params.extra = true;
                        generateConfirmationDialog('ADD_PORTIN_BONUS', params, reloadPage, selectEmpty);
                        break;
                    case "ADD_CONTRACT_BUSTER_BONUS":
                        generateConfirmationDialog('ADD_CONTRACT_BUSTER_BONUS', params, reloadPage, selectEmpty);
                        break;
                    case "BIRTHDAY_BONUS":
                        generateConfirmationDialog('BIRTHDAY_BONUS', params, reloadPage, selectEmpty);
                        break;
                    case "ADD_LOYALITY_BONUS":
                        generateConfirmationDialog('ADD_LOYALITY_BONUS', params, reloadPage, selectEmpty);
                        break;
                    case "ADD_LAZADA_BONUS":
                        generateConfirmationDialog('ADD_LAZADA_BONUS', params, reloadPage, selectEmpty);
                        break;
                    case "ADD_PENDING_BONUS":
                        params.orderReferenceNumber = cache.orderReferenceNumber;
                        params.order_reference_number = cache.orderReferenceNumber;
                        generateConfirmationDialog('ADD_PENDING_BONUS', params, reloadPage, selectEmpty);
                        break;
                    case "DEACTIVATE_REFERRER_BONUS":
                        generateConfirmationDialog('DEACTIVATE_REFERRER_BONUS', {
                            number: cache.number, 
                            prefix: cache.prefix
                        }, reloadPage, selectEmpty);
                        break;
                    case "WINBACK_BONUS":
                        generateConfirmationDialog('WINBACK_BONUS', params, reloadPage, selectEmpty);
                        break;
                    default:
                        break;
                }
            });

            function selectEmpty(){
                $('#bonusesOptions').val($("#target option:first").val());
            };

            // Port-In related buttons / logic
            // Requires access level 2

            if (write > 1) {

                var hourTs =  60 * 60 * 1000;
                var startDates = [];

                var dateIn1h = new Date(new Date().getTime() + 1 * hourTs);
                startDates.push({key: 'IN_ONE_HOUR', value: 'In 1 hour'});

                for (var i = 1; i < 15; i++) {
                    var date = new Date(new Date().getTime() + i * 24 * hourTs);
                    date.setHours(13,0,0,0);
                    startDates.push({key: date.toISOString(), value: getDate(date)});
                }

                $("#createPortinReq").unbind().click(function(){
                    var params = {
                        number: cache.number, 
                        prefix: cache.prefix,
                        serviceInstanceNumber: cache.serviceInstanceNumber,
                        account: cache.account,
                        portInPrefix: cache.prefix,
                        unpairedNumbers: unpairedNumbers,
                        startDatesList: startDates,
                        type: "REINIT",
                        customValidator: function(fields, params){
                            if(fields){
                                for (var i = 0; i < fields.length; i++) {
                                    var field = fields[i];
                                    var item = field.children[1].children[0];
                                    var key = item.getAttribute('name');
                                    switch(key){
                                        case "portInNumberSelected":
                                            var portInNumberSelected = item.value;
                                            break;
                                        case "portInNumberTyped":
                                            var portInNumberTyped = item.value;
                                            break;
                                        case "donorNetworkCode":
                                            var donorNetworkCode = item.value;
                                            break;
                                        case "startDate":
                                            var startDate = item.value;
                                            break;
                                    }
                                }
                                if(!portInNumberSelected && !portInNumberTyped){
                                    return genUserMessage("SELECT_OR_TYPE_NUMBER");
                                }else{
                                    if(!donorNetworkCode){
                                        return genUserMessage("SELECT_DONOR_FOR_PORTIN");
                                    }else if(!startDate){
                                        return genUserMessage("SELECT_START_DATE_FOR_PORTIN");
                                    }
                                }
                                return true;
                            }else{
                                return true;
                            }
                        }
                    };
                    generateConfirmationDialog('CREATE_PORTIN', params, reloadPage);
                });

                $("#cancelPortinReq").unbind().click(function(){
                    var params = {
                        number: cache.number, 
                        prefix: cache.prefix,
                        serviceInstanceNumber: cache.serviceInstanceNumber,
                        account: cache.account,
                        portInPrefix: cache.prefix,
                        unpairedNumbers: unpairedNumbers,
                        startDatesList: startDates,
                        type: "CANCEL",
                        customValidator: function(fields, params){
                            if(fields){
                                for (var i = 0; i < fields.length; i++) {
                                    var field = fields[i];
                                    var item = field.children[1].children[0];
                                    var key = item.getAttribute('name');
                                    switch(key){
                                        case "portInNumberSelected":
                                            var portInNumberSelected = item.value;
                                            break;
                                        case "portInNumberTyped":
                                            var portInNumberTyped = item.value;
                                            break;
                                    }
                                }
                                if(!portInNumberSelected && !portInNumberTyped){
                                    return genUserMessage("SELECT_OR_TYPE_NUMBER");
                                }
                                return true;
                            }else{
                                return true;
                            }
                        }
                    };
                    generateConfirmationDialog('CANCEL_PORTIN', params, reloadPage);
                });

                $("#showPortinLogs").unbind().click(function(){
                    loadLogsDialog({
                        filterType: "SIN",
                        matchType: "EXACT",
                        searchString: cache.serviceInstanceNumber,
                        period: "ALL"
                    }, 'logs_portin');
                });

                $("#showStatusChangeLogs").unbind().click(function(){
                    loadLogsDialog({
                        filterType: "NUMBER",
                        matchType: "EXACT",
                        searchString: cache.number,
                        period: "ALL"
                    }, 'logs_account_status');
                });

                $("#showStatusChangeSchedules").unbind().click(function(){
                    ajax("GET", "/api/1/web/customers/statusChangeSchedules/" + cache.serviceInstanceNumber, {}, true, function (data) {
                        if (data && (data.code == 0)) {
                            generateTableDialog("customerStatusChangeSchedules", data.list);
                        } else {
                            showInfoDialog("Failure", "Failed");
                            hideLoadingLabel();
                        }
                    });
                });

                $("#showBillingLogs").unbind().click(function(){
                    loadLogsDialog({
                        filterType: "BAN",
                        matchType: "EXACT",
                        searchString: cache.billingAccountNumber,
                        period: "ALL"
                    }, 'logs_bill_sent');
                });

                $("#showPayNowLogs").unbind().click(function(){
                    loadLogsDialog({
                        filterType: "BAN",
                        matchType: "EXACT",
                        searchString: cache.billingAccountNumber,
                        period: "ALL"
                    }, 'logs_pay_now');
                });

            }

            generateTable("showPortInNotifications", customer.porting, [
                {"sTitle": "Notifications Date", "sClass": "twocol-table-column", "mData": "ts", "mRender": function(data, type, full){
                        return getDateTime(new Date(data));
                    }
                },
                {"sTitle": "Notifications Activity", "sClass": "twocol-table-column", "mData": "activity", "mRender": function(data, type, full){
                        return data;
                    }
                }
            ], 5);


            generateTable("showPortInRequests", customer.portInRequests, [
                {"sTitle": "Start Date", "sClass": "fourcol-table-column", "mData": "start_ts", "mRender": function(data, type, full){
                        return getDate(new Date(data));
                    }
                },
                {"sTitle": "Status", "sClass": "fourcol-table-column", "mData": "start_ts", "mRender": function(data, type, full){
                        var status = (full.status === "FAILED" && full.error_status ? full.error_status : full.status);
                        return getStatusLabel(status, status);
                    }
                },
                {"sTitle": "Number", "sClass": "fourcol-table-column", "mData": "number", "mRender": function(data, type, full){
                        return data;
                    }
                },
                {"sTitle": "Donor", "sClass": "fourcol-table-column", "mData":"donor_network", "mRender": function(data, type, full){
                        return data;
                    }
                }
            ], 5);

            //----------------
        }
        if($('input[name=mobile]').val() != ""){
            var customerTestActions = [];
            customerTestActions.push(["SMS Test", getTableActionButtonWithoutCallback('smsTest', 'Send', undefined)]);
            generateKeyValueTable("customerTestsTable", customerTestActions);
            $('#smsTest').click(function() {
                this.disabled = true;
                var prefix = $("input[name=prefix]").val();
                var number = $('input[name=mobile]').val();
                if(prefix && number){
                    ajax("PUT", "/api/1/web/customers/notifications/test/sms/" + prefix + "/" + number, {}, true, function (data) { 
                        if ( data && (data.code == 0) ) {
                            $("#smsTest").prop("disabled", false);
                            showToast("success", "OK");
                        }
                    });
                }else{
                    showToast("warning", "Please key in prefix and number");
                }
            });
        }
        if (write > 1) {
            var circlesTalkAction = [];
            var redirectOptionsList = [{key: 0, value: "Off"}, {key: 1, value: "On"}];
            circlesTalkAction.push(['Phone Number', getOptionSelect('circlesTalkPhoneNumber',circlsTalkNumbers, 'optionSelect')]);
            circlesTalkAction.push(['Credits', '<span id=ctCreditString></span>']);
            if(write > 2){
                circlesTalkAction.push(['App Registration', getTableActionButtonWithoutCallback('removeAppRegistration','Remove', undefined)]); 
                circlesTalkAction.push(['Add Credit', getTableActionButtonWithoutCallback('addCTCredit-1','$0.99', undefined) + 
                getTableActionButtonWithoutCallback('addCTCredit-5','$4.99', undefined) + 
                getTableActionButtonWithoutCallback('addCTCredit-10','$9.99', undefined) +
                getTableActionButtonWithoutCallback('addCustomCTCredit' , 'Add', undefined)]);
                circlesTalkAction.push(['Call Plan', getOptionSelect('callPlanList', [], 'optionSelect')]);
                circlesTalkAction.push(['Redirect MT', getOptionSelect('mtSelect', redirectOptionsList, 'optionSelect')]);
                circlesTalkAction.push(['Redirect MO', getOptionSelect('moSelect', redirectOptionsList, 'optionSelect')]);
            }

            generateKeyValueTable("circlesTalkActions", circlesTalkAction);

            $("#circlesTalkActions").find("input,button,textarea,select").prop("disabled", true);
            $("#circlesTalkPhoneNumber").prop("disabled", false);

            if(write > 2){
                $("#removeAppRegistration").click(function() {
                    if(validateCirlcesTalk()){
                        generateConfirmationDialog('REMOVE_APP_REGISTRATION', {
                            prefix: cache.prefix,
                            number: $('#circlesTalkPhoneNumber').val(),
                            app: "gentwo"
                        }, reloadPage);
                    }
                });

                $("#addCustomCTCredit").click(function(){
                    if(validateCirlcesTalk()){
                        generateConfirmationDialog('CIRCLES_TALK_ADD_CUSTOM_CREDIT', {
                            number: $('#circlesTalkPhoneNumber').val(), 
                            prefix: $("input[name=prefix]").val(),
                            type: "ADD_CREDIT"
                        }, reloadPage);
                    }
                });

                $("button[id^='addCTCredit']").click(function(){
                    if(validateCirlcesTalk()){
                        generateConfirmationDialog('MODIFY_CIRCLES_TALK', {
                            number: $('#circlesTalkPhoneNumber').val(), 
                            prefix: $("input[name=prefix]").val(),
                            amount: $(this).attr('id').split("-")[1],
                            displayAmount: $(this).text(),
                            type: "ADD_CREDIT"
                        }, reloadPage);
                    }
                });

                $("#callPlanList").change(function(){
                    if(validateCirlcesTalk()){
                        generateConfirmationDialog('MODIFY_CIRCLES_TALK', {
                            number: $('#circlesTalkPhoneNumber').val(), 
                            prefix: $("input[name=prefix]").val(),
                            plan: this.value,
                            planName: $(this).find('option:selected').text(),
                            type: "CHANGE_PLAN"
                        }, reloadPage);
                    }
                });

                $("#mtSelect").change(function(){
                    var selectedMtVal = $(this).find(":selected").val();
                    if(validateCirlcesTalk() && selectedMtVal){
                        generateConfirmationDialog('MODIFY_CIRCLES_TALK', {
                            number: $('#circlesTalkPhoneNumber').val(), 
                            prefix: $("input[name=prefix]").val(),
                            mt: selectedMtVal,
                            type: "REDIRECT_MT"
                        }, reloadPage, function(err, data, cancel){
                            if(cancel){
                                setOptionSelectValue('mtSelect', selectedMtVal == "0" ? "1" : "0");
                            }
                        });
                    }
                });

                $("#moSelect").change(function(){
                    var selectedMoVal = $(this).find(":selected").val();
                    if(validateCirlcesTalk() && selectedMoVal){
                        generateConfirmationDialog('MODIFY_CIRCLES_TALK', {
                            number: $('#circlesTalkPhoneNumber').val(), 
                            prefix: $("input[name=prefix]").val(),
                            mo: selectedMoVal,
                            type: "REDIRECT_MO"
                        }, reloadPage, function(err, data, cancel){
                            if(cancel){
                                setOptionSelectValue('moSelect', selectedMtVal == "0" ? "1" : "0");
                            }
                        });
                    }
                });
            }
            

            $("#circlesTalkPhoneNumber").change(function () {
                if(this.value && this.value.length > 0){
                    if(write > 2){
                        $("#circlesTalkActions").find("input,button,textarea,select").prop("disabled", false);
                    }
                    updateCirclesTalkTable(this.value);
                }else{
                    $("#ctCreditString").text('');
                    $("#circlesTalkActions").find("input,button,textarea,select").prop("disabled", true);
                    $("#circlesTalkPhoneNumber").prop("disabled", false);
                }
            });

            

            
        }else{
            $("#circlesTalkActionsCart").hide();
        }

        $('input[type="checkbox"],[type="radio"]').not('#create-switch').bootstrapSwitch();
    }
}

function updateCirclesTalkTable(selectedNumber){
    var parameters = { "prefix": $("input[name=prefix]").val(), "number": selectedNumber };
    showLoadingLabel();
    ajax("GET", "/api/1/web/customers/search/gentwo", parameters, true, function (data) { 
        var credit = (data.result.credit) ? parseInt(data.result.credit * 100)/100 : 0;
        var creditSGD = (data.result.creditSGD) ? parseInt(data.result.creditSGD * 100)/100 : 0;
        $("#ctCreditString").text(" " + credit + "USD (" + creditSGD + " SGD)");
        var planList = [];
        if(data.result.plans){
            data.result.plans.forEach(function (item) {
                planList.push({key: item.planID, value: item.name});
            });
        }
        replaceOptionsOfSelect('callPlanList', planList, data.result.plan_id);
        setOptionSelectValue('mtSelect', (data.result.mt ? "1" : "0"));
        setOptionSelectValue('moSelect', (data.result.mo ? "1" : "0"));
        $("#moSelect").prop("disabled", true);
        hideLoadingLabel();
    });
}

function generateGeneralActive(id, type, data, totalHolder, accessLevel){
    var kbBonusActive = 0;
    var kbBonusFuture = 0;
    data.sort(function (a, b) {
        return packageSort(a, b, true);
    });
    var mainKey;
    var dialogKey;
    switch(type){
        case "generalCurrent":
            mainKey = "Addons";
            dialogKey = "ADDON_UNSUBSCRIBE";
            break;
        case "generalFuture":
            mainKey = "Addons";
            dialogKey = "ADDON_UNSUBSCRIBE";
            break;
        case "bonusCurrent":
            mainKey = "Current";
            dialogKey = "BONUS_UNSUBSCRIBE";
            break;
        case "bonusFuture":
            mainKey = "Future";
            dialogKey = "BONUS_UNSUBSCRIBE";
            break;
    }
    generateTable(id, data, [
        {"sTitle": mainKey, "sClass": "fivecol-table-column", "mData": "name", "mRender": function(data, type, full){
                return data + (data == "Bonus 0MB R" && full.kb > 0 ? " (Bucket " + convertKToGb(getCorrectedKB(full.kb)) + "GB)" : "");
            }
        },
        {"sTitle": "Start", "sClass": "fivecol-table-column", "mData": "billStartDate", "mRender": function(data, type, full){
                return getDateTime(new Date(data)).replace(" ", "<br>");
            }
        },
        {"sTitle": "End", "sClass": "fivecol-table-column", "mData": "expiryDate", "mRender": function(data, type, full){
                return getDateTime(new Date(full.expiryDate)).replace(" ", "<br>");
            }
        },
        {"sTitle": "History Id", "sClass": "fivecol-table-column", "mData": "packageHistoryId", "mRender": function(data, type, full){
                return data;
            }
        },
        {"sTitle": "Action", "sClass": "fivecol-table-column", "mData": "id", "mRender": function(data, type, full){
            if (full.name == "Bonus 0MB R") {
                return "BLOCKED";
            }

            return (accessLevel > 2 && full.packageHistoryId && full.packageHistoryId != "None") ? getTableActionButton('Unsub', undefined, dialogKey, {
                number: cache.number,
                prefix: cache.prefix,
                product: data,
                serviceInstanceNumber: cache.serviceInstanceNumber,
                account: cache.account,
                historyId: full.packageHistoryId,
                productName: full.name
            }) : "None";
        }
        }
    ], 10);
    if (type === "bonusCurrent" || type === "bonusFuture") {
        $("#" + totalHolder).text("TOTAL: " + convertKToGb(generateTotalData(data, "kb")) + "GB");
    }
}

function generateTotalData(data, key){
    var total = 0;
    data.forEach(function (item) {
        total += getCorrectedKB(item[key]);
    });
    return total;
}

function generateCurrentAndFutureBonus(prefix, number, type, current, future){

}

function showApps(apps) {
    $("#showApps tbody").empty();
    if (apps && apps.length) apps.forEach(function (app) {
        var device_id = (!app.device_id) ? "" : (app.device_id.length <= 16) ? app.device_id : "... " + app.device_id.slice(-16);
        var token = (!app.token) ? "" : (app.token.length <= 16) ? app.token : "... " + app.token.slice(-16);
        var vapn = (!app.vapn) ? "" : (app.vapn.length <= 16) ? app.vapn : "... " + app.vapn.slice(-16);
        $("#showApps > tbody:last-child").append("<TR>" +
                "<TD>" + app.app_type + "</TD>" +
                "<TD>" + device_id + "</TD>" +
                "<TD>" + app.token_type + "</TD>" +
                "<TD>" + app.flavor + "</TD>" +
                "<TD><A href=# class=push data-id=push>" + token + "</A></TD>" +
                "<TD><A href=# class=push data-id=vpush>" + vapn + "</A></TD>" +
                "<TD>" + app.status + "</TD>" +
                "<TD>" + 
                getTableActionButton('Remove', undefined, "REMOVE_APP_REGISTRATION_VIA_APP_MANAGER", {
                    userKey: app.user_key
                }) +
                "</TD></TR>");
        $("a.push").click(function () {
                if ( $(event.target).is("a[disabled]") ) return false;
                $(this).attr("disabled", "disabled");
                event.preventDefault();
                testPush($(this).attr("data-id"), app);
        });
    });
}

function showNumber(details) {
    $("#showNumberManage tbody").empty();
    var button = "";
    if ( false && (details.status == "Released") ) { 	// disable release
        button = "<BUTTON class='btn btn-primary btn-sm' onClick='releaseNumber('" + details.number + "');'>Release</BUTTON>";
    }
    $("#showNumberManage > tbody:last-child").append("<TR>" +
    "<TD>Status : </TD>" +
    "<TD>" + details.status + "</TD>" +
    "<TD>" + button + "</TD>" +
    "</TR>");
}

function showCDR(list) {
    if ($("#showCDR").dataTableSettings.length > 0) $("#showCDR").DataTable().fnDestroy();
    $("#showCDR tbody").empty();
    if (list) list.forEach(function (item) {
       var datetime = (item.answer_time) ?
           getDateTime(new Date(item.answer_time)) :
           getDateTime(new Date(item.start_time));
       var cost = (item.egress_cost) ? item.egress_cost : 0;
       var charge = (item.ingress_cost) ? item.ingress_cost : 0;
        $("#showCDR > tbody:last-child").append("<TR>" +
        "<TD>" + datetime + "</TD>" +
        "<TD>" + item.orig_ani + "</TD>" +
        "<TD>" + item.orig_dst_number + "</TD>" +
        "<TD>" + item.egress_carrier_name + "</TD>" +
        "<TD>" + item.term_code_name + "</TD>" +
        "<TD>" + item.ingress_billtime + "</TD>" +
        "<TD>" + cost + "</TD>" +
        "<TD>" + charge + "</TD>" +
        "<TD>" + ((item.credit) ? item.credit : "" ) + "</TD>" +
        "<TD>" + item.release_side + "</TD>" +
        "<TD>" + item.mos + "</TD>" +
        "</TR>");
    });
    $("#showCDR").DataTable({ "aaSorting" : [] });
}

function showCalendar() {
    $(".username")[0].innerHTML = getCookie("cmsuser");
    var now = new Date();
    $("#schedSearchForm :input[name=start]").val(getDateTime(new Date(now.getFullYear(), now.getMonth(), now.getDate())));
    $("#schedSearchForm :input[name=end]").val(getDateTime(new Date(now.getFullYear(), now.getMonth(), now.getDate() + 1 , 0, 0, 0, -1)));
    $("#schedSearchForm :input[name=start]").datetimepicker({
        "format" : "yyyy-mm-dd hh:ii:ss",
        "autoclose" : true
    });
    $("#schedSearchForm :input[name=end]").datetimepicker({
        "format" : "yyyy-mm-dd hh:ii:ss",
        "autoclose" : true
    });
    searchSched($("#schedSearchForm")[0]);

    var nowHour = getDateTime(new Date(now.getFullYear(), now.getMonth(), now.getDate(), now.getHours() + 1));
    $(".form_datetime").val(nowHour);		// datetime picker initial state
    $(".form_datetime").datetimepicker({
        "format" : "yyyy-mm-dd hh:ii:ss",
        "autoclose" : true,
        "startDate" : nowHour });
}

function tableifyScheduler() {
    var events = tableify($("#addEventsText"), $("#addEventsTable"), ["action"]);
    $("#tableify").addClass("disabled");
    if (events) {
        var error = false;
        events.forEach(function(item) {
            var title = (item.activity && (item.activity != "")) ? item.activity : item.action;
            if (item.number) {
                item.title = title + " " + item.number;
            } else if (item.email) {
                item.title = title + " " + item.email;
            } else {
                error = true;
            }
            item.start = new Date($(".form_datetime").val()).getTime();
            item.allDay = false;
            item.trigger = getCookie("cmsuser");
        });

        $("#addScheduler").removeClass("disabled");
        $("#addScheduler").click(function () {
            if (!error) addSchedules(events);
            else showToast("warning", "Events Not Added, Email or Number required");
        });
    }
}

function clearTableify() {
    $(this).unbind("click");
    $("#addEventsText").val("");
    $("#addEventsText").show();
    $("#addEventsTable").empty();
    $("#tableify").removeClass("disabled");
    $("#addScheduler").unbind("click");
    $("#addScheduler").addClass("disabled");
}

function searchSched(form) {
    var parameters = {
        "start" : new Date(form.start.value).toISOString(),
        "end" : new Date(form.end.value).toISOString() };

    if ((new Date(parameters.end) - new Date(parameters.start)) > 7 * 24 * 60 * 60 * 1000) {
        showToast("warning", "Error Max search 7 days!");
        return false;
    }
    if (form.action) parameters.action = form.action.value;
    if (form.activity) parameters.activity = form.activity.value;
    $(form.search).prop("disabled", true);
    $(form.download).prop("disabled", true);
    $(form.download).unbind("click");

    // sunday has index 0!
    var dayOfWeek = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

    ajax("GET", "/api/1/web/customers/get/schedules/all", parameters, true, function (data) {
	if ($("#showSchedules").dataTableSettings.length > 0) $("#showSchedules").DataTable().fnDestroy();
        $("#showSchedules tbody").empty();
        if ( data && (data.length>0) ) {
            var arr = new Array;
            data.forEach(function (item) {
                var start = new Date(item.start);
                var sched = {
                    "date" : getDateTime(start),
                    "action" : item.action,
                    "activity" : ((item.activity) ? item.activity : ""),
                    "serviceInstanceNumber" : ((item.serviceInstanceNumber) ? item.serviceInstanceNumber : ""),
                    "number" : ((item.number) ? item.number : ""),
                    "email" : ((item.email) ? item.email : ""),
                    "trigger" : item.trigger,
                    "status" : (item.color ? item.color : "blue")
                };
                var day = dayOfWeek[start.getDay()];
                $("#showSchedules tbody").append("<TR>" +
                    "<TD>" + sched.date + " " + day + "</TD>" +
                    "<TD>" + sched.action + "</TD>" +
                    "<TD>" + sched.activity + "</TD>" +
                    "<TD>" + sched.serviceInstanceNumber + "</TD>" +
                    "<TD>" + sched.number + "</TD>" +
                    "<TD>" + sched.email + "</TD>" +
                    "<TD>" + sched.trigger + "</TD>" +
                    "<TD><A href=# id=status_" + item.id + "><SPAN class=badge style='background-color:" + sched.status + "'>&nbsp;</SPAN></TD>" +
                    "<TD><A href=# id=execute_" + item.id + "><I class='fa fa-play'></I></A>&nbsp;&nbsp;&nbsp;" +
                        "<A href=# id=trash_" + item.id + "><I class='fa fa-trash-o'></I></A>&nbsp;&nbsp;&nbsp;" +
                        "<A href=# id=refresh_" + item.id + "><I class='fa fa-refresh'></I></A></TD>" +
                "</TR>");
                $("#refresh_" + item.id).attr("processed", JSON.stringify(item.processed));
                $("#refresh_" + item.id).attr("color", item.color);
                $("#refresh_" + item.id).attr("start", item.start);
                $("#refresh_" + item.id).click(function () {
                    $("#refresh_" + item.id).datetimepicker({
                        "format" : "yyyy-mm-dd hh:ii:ss",
                        "autoclose" : true,
                        "startDate" : new Date(),
                    }).on('changeDate', function (ev) {
                        if ( confirm("Move to " + new Date(ev.date).toISOString() + "?") ) {
                            var parameters = { "datetime" : new Date(ev.date).getTime() - (8 * 60 * 60 * 1000),
                                "processed" : $(this).attr("processed"), "color" : $(this).attr("color"), "start" : $(this).attr("start") };
                            ajax("POST", "/api/1/web/customers/move/schedules/" + $(this).attr("id").split("_")[1], parameters, true, function (data) {
                                if (data && (data.code == 0)) {
                                    searchSched($("#schedSearchForm")[0]);
                                }
                            });
                        }
                    });
                    $("#refresh_" + item.id).datetimepicker("show");
                });
                $("#status_" + item.id).click(function () {
                    event.preventDefault();
                    showInfoDialog("Title: " + item.title, JSON.stringify(item.processed));

                });
                $("#trash_" + item.id).click(function() {
                    event.preventDefault();
                    if ( confirm("Are you sure?") ) {
                        ajax("DELETE", "/api/1/web/customers/delete/schedules/" + item.id, { }, true, function (data) {
                            if (data && (data.code == 0)) {
                                searchSched($("#schedSearchForm")[0]);
                            } else {
                                showToast("warning", data.error ? data.error : "Failed");
                            }
                        });
                    }
                });
                $("#execute_" + item.id).click(function() {
                    event.preventDefault();
                    if ( confirm("Are you sure?") ) {
                        ajax("PUT", "/api/1/web/customers/execute/schedules/" + item.id, { }, true, function (data) {
                            if (data && (data.code == 0)) {
                                searchSched($("#schedSearchForm")[0]);
                            } else {
                                showToast("warning", data.error ? data.error : "Failed");
                            }
                        });
                    }
                });

                arr.push(sched);
            });
            $("#showSchedules").DataTable();
            $(form.search).prop("disabled", false);
            $(form.download).prop("disabled", false);
            $(form.download).click(function () {
                searchDownload(arr);
            });
        } else {
            $(form.search).prop("disabled", false);
        }
    });
}
/*
    This piece of code lets you store the information user just explored on the panel
    in form of CSV. 
    A sample CSV format will be 
    __________________________________________________________________________________________________________________________
    | Date                | Action | Activity  |  Number  | Service Instance |      Email       |     Trigger       | Status |    
    |_____________________|________|___________|__________|__________________|__________________|___________________|________|
    | 2018-01-18 08:30:00 | portin |           | 88088329 |    LW0381656     | test@circle.asia | [ECOMM][INTERNAL] |  red   |
    |_____________________|________|___________|__________|__________________|__________________|___________________|________|
*/
function searchDownload(arr) {
    var dataString = "Date,Action,Activity,Number,Service Instance,Email,Trigger,Status\n";
    arr.forEach(function (item) {
        dataString += item.date + "," +
            item.action + "," +
            item.activity + "," +
            item.number + "," +
            item.serviceInstanceNumber + "," +
            item.email + "," +
            item.trigger + "," +
            item.status + "\n";
    });
    downloadCSV(dataString, "download.csv", "text/csv");
}
