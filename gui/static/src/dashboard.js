var timer = new Object;

function init(type) {
    toggleAdvanceQueues(false);
    $(".username")[0].innerHTML = getCookie("cmsuser");
    if (type == "hud") {
        showLoadingLabel();
        getStatsPM2();
        setTimeout(function(){
            setInterval(() => {
                getStatsPM2();
            }, 60000);
        }, 5000);
    }else if (type == "kirk") {
        getNotifications();
        getStatsCMS();
        getQueueStats();
    } else if (type == "mobile") {
        getStatsMobile();
        getGentwo("online");
        setInterval(function run() {
            getGentwo("active");
            return run;
        }(), 10 * 1000);
    } else if (type == "elitecore") {
        $("#startDate").val(formatDataDatePicker(new Date(new Date().getTime() - 24 * 60 * 60 * 1000)));
        $("#endDate").val(formatDataDatePicker(new Date()));
        $("#points").val(24);
        reloadStatsEC();
    } else if (type == "ecomm") {
        getEcomm();
    }
}

function getGentwo(type) {
    $.ajax({    // use this to avoid popup
        "dataType": "json",
        "type": "GET",
        "url": "/api/1/web/gentwo/get/" + type,
        "data": "",
        "success": function (data) {
            if ( data && (data.code == 0) ) {
                if (type == "active") showActive(data.list);
                else if (type == "online") showOnline(data.list);
            } else showToast("warning", "Fail " + type);
        },
    });
}

function getStatsMobile() {
    $.ajax({
        url: "/api/1/web/dashboard/get/statsMobile",
        type: "GET",
        dataType: "json",
        data: {},
        success: function (data) {
            if (data.code == 0) {
                showStats(data.result, "Mobile", data.write);
                showSessions(data.result, data.write);
            } else alert("Fail");
        },
        statusCode: {
            //			403: function(error){		// this will cause auto reloads
            //				location.reload();
            //			}
        }
    });
}

function getStatsPM2() {
    $.ajax({
        url: "/api/1/web/dashboard/get/statsPm2",
        type: "GET",
        dataType: "json",
        data: {},
        success:  (data) => {
            if (data.code == 0) {
                hideLoadingLabel();
                showPm2Stats(data.result);
            } else alert("Fail");
        },
        statusCode: {

        }
    });
}

function getStatsCMS() {
    $.ajax({
        url: "/api/1/web/dashboard/get/statsCMS",
        type: "GET",
        dataType: "json",
        data: {},
        success: function (data) {
            if (data.code == 0) {
                showStats(data.result, "CMS", data.write);
                toggleAdvanceQueues(data.admin);
            } else alert("Fail");
        },
        statusCode: {
            //			403: function(error){		// this will cause auto reloads
            //				location.reload();
            //			}
        }
    });
}

function toggleAdvanceQueues(show){
    if(show){
        $("#invoiceQ").show();
        $("#capPayQ").show();
        $("#statusQ").show();
    }else{
        $("#invoiceQ").show();
        $("#capPayQ").show();
        $("#statusQ").show();
    }
}

function bulkAction(type, state, action) {
    event.preventDefault();
    var putParams = { "type" : type, "state" : state };
    if(action == "bulkRefresh"){
        putParams.bulkRefresh = true;
        var messageAction = "Refresh"
    }else if(action == "bulkDelete"){
        putParams.bulkDelete = true;
        var messageAction = "Delete"
    }
    var dialog = BootstrapDialog.show({
        title: `${messageAction} items`,
        message: `You are about to ${messageAction} queue items in bulk. This might affect customers. Are you sure ?`,
        buttons: [{
            label: `${messageAction}`,
            action: function(dialogItself){
                putQueueStats(putParams, function (err, data) {
                    if (data && data.code == 0) {
                        getQueueStats();
                        showToast("success", "Success!");
                    } else {
                        showToast("warning", "Failed!");
                    }
                });
                dialogItself.close();
            }
        },
        {
            label: 'Close',
            action: function(dialogItself){
                dialogItself.close();
            }
        }]
    });
}

function putQueueStats(params, callback) {
    ajax("PUT", "/api/1/web/dashboard/put/queueStats", params, true, function (data) {
        callback(undefined, data);
    });
}

function getQueueStats() {
    var parameters = { };
    ajax("GET", "/api/1/web/dashboard/get/queueStats", parameters, true, function (data) {
        if (data && (data.code == 0)) {
            Object.keys(data.result).forEach(function (type) {
                Object.keys(data.result[type]).forEach(function (id) {
                    $("#" + type + "" + id).html(data.result[type][id]);
                    $("#" + type + "" + id).unbind("click");
                    $("#" + type + "" + id).click(function () {
                        var params = { "type" : type, "id" : id };
                        ajax("GET", "/api/1/web/dashboard/get/queueStats", params, true, function (list) {
                                if ($("#qStatsShow").dataTableSettings.length > 0) $("#qStatsShow").DataTable().fnDestroy();
                                $("#qStatsShow tbody").empty();
                                if (list.code == 0) list.result.forEach(function (item) {
                                    var id = (item.data.to) ? item.data.to :
                                            item.data.args ? item.data.args.msisdn :
                                            item.data.number ? item.data.number : item.data.msisdn ? item.data.msisdn : item.data.billingAccountNumber ? item.data.billingAccountNumber : "";
                                    $("#qStatsShow tbody").append("<TR>" +
                                        "<TD>" + item.type + "</TD>" +
                                        "<TD><A href=# id=id_" + item.id + ">" + id + "</A></TD>" +
                                        "<TD>" + new Date(parseInt(item.created_at)).toLocaleString() + "</TD>" +
                                        "<TD>" + new Date(parseInt(item.promote_at)).toLocaleString() + "</TD>" +
                                        "<TD>" + new Date(parseInt(item.updated_at)).toLocaleString() + "</TD>" +
                                        "<TD>" + item.attempts.remaining + "</TD>" +
                                        "<TD><A href=# id=error_" + item.id + ">" + item.state + "</A></TD>" +
                                        "<TD><A href=# id=trash_" + item.id + "><I class='fa fa-trash-o'></I></A>&nbsp;&nbsp;&nbsp;" +
                                            "<A href=# id=refresh_" + item.id + "><I class='fa fa-refresh'></I></A></TD>" +
                                    "</TR>");
                                    $("#id_" + item.id).unbind("click");
                                    $("#id_" + item.id).click(function () {
                                        event.preventDefault();
                                        var title = (item.type == "email") ?
                                                "From : " + item.data.from + "\nTo : " + item.data.to + "\nSubject : " + item.data.subject :
                                                (item.type == "sms") ?
                                                "From : " + item.data.from + "\nTo : " + item.data.to + "\nActivity : " + item.data.activity :
                                                (item.type == "hlr") ?
                                                "Cmd : " + item.data.cmd + "\nNumber : " + item.data.args.msisdn :
                                                (item.type == "aggregation") ?
                                                "Initiator : " + item.data.initiator + "\nNumber : " + item.data.number :
                                                (item.type == "invoicePayment") ?
                                                "Made Attempts : " + item.attempts.made + "\nMax Attempts : " + item.attempts.max :
                                                (item.type == "creditCapPayment") ?
                                                "Made Attempts : " + item.attempts.made + "\nMax Attempts : " + item.attempts.max :
                                                (item.type == "statusValidation") ?
                                                "Made Attempts : " + item.attempts.made + "\nMax Attempts : " + item.attempts.max :
                                                (item.type == "ingress") ?
                                                "Made Attempts : " + item.attempts.made + "\nMax Attempts : " + item.attempts.max :
                                                "" ;
                                        BootstrapDialog.show({
                                            "size" : BootstrapDialog.SIZE_WIDE,
                                            "title" : title,
                                            "message" : ((item.type == "email") ? item.data.html :
                                                    (item.type == "sms") ? item.data.plain :
                                                    (item.type == "hlr") ? JSON.stringify(item.data.args) :
                                                    (item.type == "aggregation") ? JSON.stringify(item.data) :
                                                    (item.type == "invoicePayment") ? JSON.stringify(item.error) :
                                                    (item.type == "creditCapPayment") ? JSON.stringify(item.error) :
                                                    (item.type == "statusValidation") ? JSON.stringify(item.error) :
                                                    (item.type == "ingress") ? JSON.stringify(item.error) :
                                                    ""),
                                            "buttons" : [ ]
                                        });
                                    });
                                    $("#error_" + item.id).unbind("click");
                                    $("#error_" + item.id).click(function () {
                                        event.preventDefault();
                                        BootstrapDialog.show({
                                            "size" : BootstrapDialog.SIZE_WIDE,
                                            "title" : "Error",
                                            "message" : item.error,
                                            "buttons" : [ ]
                                        });
                                    });
                                    $("#trash_" + item.id).unbind("click");
                                    $("#trash_" + item.id).click(function () {
                                        event.preventDefault();
                                        putQueueStats({ "trash" : item.id }, function (err, trashData) {
                                            if (trashData && trashData.code == 0) {
                                                getQueueStats();
                                                $("#" + item.type + "" + item.state).trigger("click");
                                                showToast("success", "Success!");
                                            } else {
                                                showToast("warning", "Failed!");
                                            }
                                        });
                                    });
                                    $("#refresh_" + item.id).unbind("click");
                                    $("#refresh_" + item.id).click(function () {
                                        event.preventDefault();
                                        putQueueStats({ "refresh" : item.id }, function (err, refreshData) {
                                            if (refreshData && refreshData.code == 0) {
                                                getQueueStats();
                                                $("#" + item.type + "" + item.state).trigger("click");
                                                showToast("success", "Success!");
                                            } else {
                                                showToast("warning", "Failed!");
                                            }
                                        });
                                    });
                                });
                                $("#qStatsShow").DataTable();
                        });
                    });
                });
            });
        } else alert("Fail");
    });
}

function getNotifications() {
    $.ajax({
        url: "/api/1/web/dashboard/get/notifications",
        type: "GET",
        dataType: "json",
        data: {},
        success: function (data) {
            if (data.code == 0) {
                showNotifications(data.result, data.write);
            } else alert("Fail");
        },
        statusCode: {
            //			403: function(error){		// this will cause auto reloads
            //				location.reload();
            //			}
        }
    });
}

function getEcomm() {
    $.ajax({
        url: "/api/1/web/dashboard/get/ecomm",
        type: "GET",
        dataType: "json",
        data: {},
        success: function (data) {
            if (data.code == 0) {
                //showEcommReg(data.result.registrations);
                showEcommPayments(data.result.stats.payments, data.result.stats.referrals);

                if (Gauge) showEcommGauges(data.result.stats);
            } else alert("Fail");
        },
        statusCode: {
            //			403: function(error){		// this will cause auto reloads
            //				location.reload();
            //			}
        }
    });
}

function ecommList(link, obj) {
    if (!link) showEcommList(obj);
    else {
        $.blockUI();
        var parameters = {"link": link}
        ajax("GET", "/api/1/web/dashboard/get/ecomm", parameters, true, function (data) {
            if (data && (data.code == 0)) {
                showEcommList(data.result);
            } else {
                alert("Fail");
            }
            $.unblockUI();
        });
    }
}

function showOnline(list) {
    if ($("#showOnline").dataTableSettings.length > 0) $("#showOnline").DataTable().fnDestroy();
    $("#showOnline tbody").empty();
    if (list.length>0) list.forEach(function (rpc) {
        if (rpc && rpc.result && rpc.result.AoRs) rpc.result.AoRs.forEach(function (aor) {
            if (aor.Info && aor.Info.Contacts) aor.Info.Contacts.forEach(function (item) {
                if (item && item.Contact) $("#showOnline > tbody:last-child").append("<TR>" +
                    "<TD>" + aor.Info.AoR + "</TD>" +
                    "<TD>" + item.Contact.Address + "</TD>" +
                    "<TD>" + new Date(item.Contact["Last-Modified"] * 1000) + "</TD>" +
                    "<TD>" + s2time(item.Contact.Expires) + "</TD>" +
                    "</TR>");
            });
        });
    });
    $("#showOnline").DataTable();
}

function showActive(list) {
//    if ($("#showActive").dataTableSettings.length > 0) $("#showActive").DataTable().fnDestroy();
    $("#showActive tbody").empty();
    if (list.length>0) list.forEach(function (item) {
        clearInterval(timer[item.call_id.replace(/[^a-z0-9]+|\s+/gmi, "")]);
        var diff = s2time(Math.ceil((new Date().getTime() - item.start_time) / 1000));
        $("#showActive > tbody:last-child").append("<TR>" +
            "<TD>" + item.orig_ani + "</TD>" +
            "<TD>" + item.orig_dst_number + "</TD>" +
            "<TD id=timer_" + item.orig_ani + ">" + diff + "</TD>" +
            "<TD>" + item.server + "</TD>" +
            "</TR>");
        timer[item.call_id.replace(/[^a-z0-9]+|\s+/gmi, "")] = setInterval(function () {
            var prev = time2s($("#timer_" + item.orig_ani).html());
            $("#timer_" + item.orig_ani).html(s2time(prev + 1));
        }, 1000);
    });
//    $("#showActive").DataTable();
}

function showNotifications(result, write) {
    var flotSet = new Object;
    result.forEach(function (grouping) {
        grouping.list.forEach(function (item) {
            if (grouping.group == "transport") {
                Object.keys(item).forEach(function (key) {
                    if (key != "_id") {
                        if (!flotSet.transport) flotSet.transport = new Array;
                        var map = flotSet.transport.map(function (o) {
                            return o.label
                        });
                        if (map.indexOf(key) == -1) {
                            flotSet.transport.push({
                                "data": [[item._id.stamp, item[key]]],
                                "label": key
                            });
                        } else {
                            var idx = map.indexOf(key);
                            flotSet.transport[idx].data.push([item._id.stamp, item[key]]);
                        }
                    }
                });
            } else {
                if (!flotSet[grouping.group]) flotSet[grouping.group] = new Array;
                var idx = flotSet[grouping.group].map(function (o) {
                    if (o.label == item._id[grouping.group]) return o.label;
                }).indexOf(item._id[grouping.group]);
                if (idx > -1) {
                    flotSet[grouping.group][idx].data.push([item._id.stamp, item.notification_count]);
                } else {
                    flotSet[grouping.group].push({
                        "data": [[item._id.stamp, item.notification_count]],
                        "label": item._id[grouping.group]
                    });
                }
            }
        });
    });
    Object.keys(flotSet).forEach(function (group) {
        var maxLen = 0;
        var full;
        flotSet[group].forEach(function (set) {
            if (set.data.length > maxLen) {
                maxLen = set.data.length;
                full = set.data.map(function (o) {
                    return o[0]
                });
            }
        });
        flotSet[group].forEach(function (set) {
            if (set.data.length < maxLen) {
                full.forEach(function (day) {
                    if (set.data.map(function (o) {
                            return o[0]
                        }).indexOf(day) == -1) {
                        set.data.push([day, 0]);
                    }
                });
            }
            set.data.sort(function (a, b) {
                if (a[0] < b[0]) return -1;
                else return 1;
            });
        });
    });
    var flotOptions = {
        "series": {
            "points": {"show": true, "radius": 1},
            "splines": {"show": true, "fill": true}
        },
        "xaxis": {
            "mode": "categories",
            "tickLength": 17,
        },
        "grid": {"hoverable": true, "clickable": true},
        "tooltip": true,
        "tooltipOpts": {"defaultTheme": false, "content": "%s %y"}
    };
    if (flotSet.teamName && flotSet.teamName.length > 0) $.plot($("#team"), flotSet.teamName, flotOptions);
    if (flotSet.transport && flotSet.transport.length > 0) $.plot($("#transport"), flotSet.transport, flotOptions);
    flotOptions.legend = {"show": false};
    if (flotSet.activity && flotSet.activity.length > 0) $.plot($("#activity"), flotSet.activity, flotOptions);
}

function showStats(result, label, write) {
    var avg = new Array;
    var max = new Array;
    var apis = new Array;
    result.forEach(function (item) {
        avg.push([item._id.time, item._id.avg]);
        apis.push(item.paths);
        max.push([item._id.time, item._id.max]);
    });
    var tooltipCB = function (label, xval, yval, flotItem) {
        var y = (yval < 1000) ? (Math.ceil(yval * 100) / 100) + " msecs" :
        (Math.ceil(yval / 1000 * 100) / 100) + " secs";
        return label + "<br>" + xval + ":00 UTC<br>" + y;
    }
    var flotOptions = {
        "series": {
            "points": {"show": true, "radius": 1},
            "splines": {"show": true, "fill": true}
        },
        "xaxis": {
            "ticks" : [],
            "mode": "categories",
            "tickLength": 0
        },
        "grid": {"hoverable": true, "clickable": true},
        "tooltip": true,
        "tooltipOpts": {"defaultTheme": false, "content": tooltipCB}
    };

    $.plot($("#api"), [{"data": avg, "label": label + " avg"},
        {"data": max, "label": label + " max"}], flotOptions);
    $("#api").bind("plotclick", function (event, pos, item) {
        if (item) {
            generateTableDialog("apiList", apis[item.dataIndex]);
        }
    });
}

function showPm2Stats(data){
    $("#pm2Items").empty();
    data.forEach((item) => {
        var template = `<div class="col-sm-3">
            <div id="cart1" class="row ">
                <div class="col-sm-12">
                    <section class="panel">
                        <header class="panel-heading">
                            ${item.name == 'others' ? 'External Sources' : item.name}
                            <span class="tools pull-right">
                                <a href="javascript:;" class="fa fa-chevron-down"></a>
                                <a href="javascript:;" class="fa fa-times"></a>
                            </span>
                        </header>
                        <div><table id="${item.name}_dataTable" class"pm2-data-table"></table></div>
                    </section>
                </div>
            </div>
        </div>`;
        $("#pm2Items").append(template);
        if(item.data){
            if(item.name == 'others'){
                generateTable(item.name + "_dataTable", item.data, [
                    {"sTitle": "Name", "sClass": "twocol-table-column pm2-table-cell", "mData": "name", "mRender": (data, type, full) => {
                        return data;
                    }
                    },
                    {"sTitle": "Latency", "sClass": "twocol-table-column pm2-table-cell", "mData": "latency", "mRender": (data, type, full) => {
                        return data;
                    }
                    },
                    {"sTitle": "Errored", "sClass": "twocol-table-column pm2-table-cell", "mData": "errorCode", "mRender": (data, type, full) => {
                        if(data != 0){
                            return "<p class='pm2-warning-small'>True</p>";
                        }
                        return "False";
                    }
                    }
                ]);
            } else {
                generateTable(item.name + "_dataTable", item.data, [
                    {"sTitle": "Name", "sClass": "fourcol-table-column pm2-table-cell", "mData": "name", "mRender": (data, type, full) => {
                        return data;
                    }
                    },
                    {"sTitle": "Status", "sClass": "fourcol-table-column pm2-table-cell", "mData": "status", "mRender": (data, type, full) => {
                        if(data != "online"){
                            return "<p class='pm2-warning-small'>" + data + "</p>";
                        }
                        return data;
                    }
                    },
                    {"sTitle": "Uptime", "sClass": "fourcol-table-column pm2-table-cell", "mData": "uptime", "mRender": (data, type, full) => {
                        var seconds = (new Date().getTime() - data) / 1000;
                        if(seconds/3600 >= 1){
                            uptime = Math.round(seconds/3600) + " Hours";
                        }else if(seconds/60 >= 1){
                            uptime = Math.round(seconds/60) + " Mins";
                        }else{
                            uptime = Math.round(seconds) + " Secs";
                        }
                        return uptime;
                    }
                    },
                    {"sTitle": "CPU %", "sClass": "fourcol-table-column pm2-table-cell", "mData": "cpu", "mRender": (data, type, full) => {
                        return data;
                    }
                    }
                ]);
            }
        }else{
            $("#" + item.name + "_dataTable").html("<tr><th><td class='pm2-warning-big'>Warning : Data Missing</td><th></tr>");
        }
    });
}

function showSessions(result, write) {
    var sessions = new Array;
    var apis = new Array;
    if (result) result.forEach(function (item) {
        sessions.push([ item._id.time, (item._id.sessions ? item._id.sessions : 0) ]);
        apis.push(item.paths);
    });
    var tooltipCB = function (label, xval, yval, flotItem) {
        var y = yval + " sessions";
        return "Sessions<br>" + xval + ":00 UTC<br>" + y;
    }
    var flotOptions = {
        "series": {
            "points": {"show": true, "radius": 1},
            "splines": {"show": true, "fill": true}
        },
        "xaxis": {
            "ticks" : [],
            "mode": "categories",
            "tickLength": 0
        },
        "grid": {"hoverable": true, "clickable": true},
        "tooltip": true,
        "tooltipOpts": {"defaultTheme": false, "content": tooltipCB}
    };
    $.plot($("#sessions"), [{"data": sessions, "label": "Sessions"}], flotOptions);
    $("#sessions").bind("plotclick", function (event, pos, item) {
        if (item) {
            generateTableDialog("apiList", apis[item.dataIndex]);
        }
    });
}

var token;
function reloadStatsEC() {
    var start = new Date($("#startDate").val()).toISOString();
    var end = new Date($("#endDate").val()).toISOString();
    var end = new Date($("#endDate").val()).toISOString();
    var points = parseInt($("#points").val());
    var apiMethod = $("#apiMethod").val();
    var host = $("#host").val();
    var points = parseInt($("#points").val());
    var delay = token ? 300 : 0;

    if (points < 5) points = 5;
    if (points > 60) points = 60;

    points +=  1;

    if (token) clearTimeout(token);
    token = setTimeout(function() {
        ajax("GET", "/api/1/web/stats/ec?points=" + points
            + "&start=" + start + "&end=" + end
            + (apiMethod ? "&method=" + apiMethod : "")
            + (host ? "&host=" + host : ""), {}, true,
            function (response) {
                showStatsV2(response.data);
            },
            function () {
            }, showToast);
    }, delay);
}

function formatDataDatePicker(m) {
    return (m.getYear() + 1900) + "-" +
        ("0" + (m.getMonth() + 1)).slice(-2) + "-" +
        ("0" + m.getDate()).slice(-2) + " " +
        ("0" + m.getHours()).slice(-2) + ":" +
        ("0" + m.getMinutes()).slice(-2);
}

function showStatsV2(data) {
    $('#startDate').datetimepicker();
    $('#endDate').datetimepicker();
    $("#apiSections").html("");

    data.forEach(function (item) {
        var label = item.api ? item.api : "";
        var requestsCount = [];
        var timeoutsCount = [];
        var errorsCount = [];
        var avg = [];
        var max = [];

        var minTime = new Date(item.list[0].blockTime);
        var maxTime = new Date(item.list[item.list.length - 1].blockTime);
        var timeDiff = maxTime.getTime() - minTime.getTime();

        var formatDate = function (dateString) {
            var m = new Date(dateString);
            if (timeDiff > item.list.length * 24 * 60 * 60 * 1000) {
                return ("0" + (m.getMonth() + 1)).slice(-2) + "/" +
                    ("0" + m.getDate()).slice(-2);
            } else if (timeDiff > 12 * 60 * 60 * 1000) {
                return ("0" + (m.getMonth() + 1)).slice(-2) + "/" +
                    ("0" + m.getDate()).slice(-2) + " " +
                    ("0" + m.getHours()).slice(-2) + ":" +
                    ("0" + m.getMinutes()).slice(-2);
            } else {
                return ("0" + m.getHours()).slice(-2) + ":" +
                    ("0" + m.getMinutes()).slice(-2) + "." +
                    ("0" + m.getSeconds()).slice(-2);
            }
        }

        item.list.forEach(function (point) {
            avg.push([formatDate(point.blockTime), point.avgTime]);
            max.push([formatDate(point.blockTime), point.maxTime]);
            requestsCount.push([formatDate(point.blockTime), point.totalRequests]);
            timeoutsCount.push([formatDate(point.blockTime), point.totalTimeouts]);
            errorsCount.push([formatDate(point.blockTime), point.totalErrors]);
        });

        var plot = $("#plot").val();
        var type = $("#type").val();

        var tooltipCB = function (label, xval, yval, flotItem) {
            if (type === 'time') {
                var y = (yval < 1000) ? (Math.ceil(yval * 100) / 100) + " msecs" :
                (Math.ceil(yval / 1000 * 100) / 100) + " secs";
                return label + "<br>" + xval + "<br>" + y;
            } else {
                return label + "<br>" + xval + "<br>" + yval;
            }
        }

        var flotOptions = {
            "series": {
                "points": {"show": true, "radius": 1},
                "splines": {"show": true, "fill": true}
            },
            "xaxis": {
                "mode": "categories",
                "tickLength": 0
            },
            "grid": {"hoverable": true, "clickable": true},
            "tooltip": true,
            "tooltipOpts": {"defaultTheme": false, "content": tooltipCB}
        };

        var viewId = label === '' ? 'app_plot' : label + '_plot';
        var section =
            '<section class="panel">' +
            '   <header class="panel-heading">' +
            '       API stats (' + (label === '' ? 'All-In-One' : label) + ')' +
            '       <span class="tools pull-right">' +
            '           <a href="javascript:;" class="fa fa-chevron-down"></a>' +
            '           <a href="javascript:;" class="fa fa-times"></a>' +
            '       </span>' +
            '   </header>' +
            '   <div class="panel-body">' +
            '       <div id="' + viewId + '" style="width: 100%;height:300px; text-align: center; margin:0 auto;">' +
            '       </div>' +
            '   </div>' +
            '</section>';

        var points = [];
        if (type === 'time') {
            if (plot === 'all' || plot === 'average') {
                points.push({"data": avg, color:"#00e100", "label": label + " avg"});
            }
            if (plot === 'all' || plot === 'spikes') {
                points.push({"data": max, color:"#f4b200", "label": label + " max"});
            }
        } else {
            if (plot === 'all' || plot === 'requests') {
                points.push({"data": requestsCount, color:"#328dfe", "label": label + " requests count"});
            }
            if (plot === 'all' || plot === 'timeouts' || plot === 'timeouts_errors') {
                points.push({"data": timeoutsCount, color:"#f40028", "label": label + " timeouts count"});
            }
            if (plot === 'all' || plot === 'errors' || plot === 'timeouts_errors') {
                points.push({"data": errorsCount, color:"#f4a600", "label": label + " errors count"});
            }
        }

        $("#apiSections").append(section);
        $.plot($("#" + viewId), points, flotOptions);
    });
}

function showEcommGauges(stats, write) {
    var opts = {
        "lines": 12, // The number of lines to draw
        "angle": 0, // The length of each line
        "lineWidth": 0.48, // The line thickness
        "pointer": {
            "length": 0.6, // The radius of the inner circle
            "strokeWidth": 0.03, // The rotation offset
            "color": '#464646' // Fill color
        },
        "limitMax": 'true', // If true, the pointer will not go past the end of the gauge
        "colorStart": '#fa8564', // Colors
        "colorStop": '#fa8564', // just experiment with them
        "strokeColor": '#F1F1F1', // to see which ones work best for you
        "generateGradient": true
    };

    if (stats.customers_registered_last_week && stats.delivered) {
        $("#registrations-bar").height(parseInt(stats.customers_registered_last_week.count / stats.delivered.count * 100) + "%");
    }
    if (stats.orders_created_last_week_payment_fail && stats.orders_created_last_week) {
        $("#orders-failed-bar").height(parseInt(stats.orders_created_last_week_payment_fail.count / stats.orders_created_last_week.count * 100) + "%");
    }
    if (stats.orders_created_last_week_payment_success && stats.orders_created_last_week) {
        $("#orders-success-bar").height(parseInt(stats.orders_created_last_week_payment_success.count / stats.orders_created_last_week.count * 100) + "%");
    }
    if (stats.customers_registered_last_week) {
        $("#registrations-value").html(stats.customers_registered_last_week.count);
    }
    if (stats.orders_created_last_week_payment_fail) {
        $("#orders-failed-value").html(stats.orders_created_last_week_payment_fail.count);
    }
    if (stats.orders_created_last_week_payment_success) {
        $("#orders-success-value").html(stats.orders_created_last_week_payment_success.count);
    }
    if (stats.delivered) {
        $("#customers-count").html(stats.delivered.count);
    }

    var gauges = [];

    if (stats.not_yet_customers_signed_on && stats.all_customers) {
        gauges.push({
            "gauge": "signed",
            "link": stats.not_yet_customers_signed_on.link,
            "countx": stats.not_yet_customers_signed_on.count,
            "county": stats.all_customers.count,
            "labely": "registrations"
        });
    }
    if (stats.not_yet_ordered && stats.customers_signed_on) {
        gauges.push({
            "gauge": "ordered",
            "link": stats.not_yet_ordered.link,
            "countx": stats.not_yet_ordered.count,
            "county": stats.customers_signed_on.count,
            "labely": "logins"
        });
    }
    if (stats.not_yet_paid && stats.all_orders) {
        gauges.push({
            "gauge": "finished",
            "link": stats.not_yet_paid.link,
            "countx": stats.not_yet_paid.count,
            "county": stats.all_orders.count,
            "labely": "orders"
        });
    }
    if (stats.pending_approval_orders && stats.payment_success) {
        gauges.push({
            "gauge": "document",
            "link": stats.pending_approval_orders.link,
            "countx": stats.pending_approval_orders.count,
            "county": stats.payment_success.count,
            "labely": "paid"
        });
    }
    if (stats.for_reupload && stats.payment_success) {
        gauges.push({
            "gauge": "reupload",
            "link": stats.for_reupload.link,
            "countx": stats.for_reupload.count,
            "county": stats.payment_success.count,
            "labely": "paid"
        });
    }
    if (stats.cancelled && stats.payment_success) {
        gauges.push({
            "gauge": "disapproved",
            "link": stats.cancelled.link,
            "countx": stats.cancelled.count,
            "county": stats.payment_success.count,
            "labely": "paid"
        });
    }
    if (stats.terminated && stats.payment_success) {
        gauges.push({
            "gauge": "terminated",
            "link": stats.terminated.link,
            "countx": stats.terminated.count,
            "county": stats.payment_success.count,
            "labely": "paid"
        });
    }
    if (stats.so_error && stats.approved) {
        gauges.push({
            "gauge": "so",
            "link": stats.so_error.link,
            "countx": stats.so_error.count,
            "county": stats.approved.count,
            "labely": "approved"
        });
    }
    if (stats.porting_not_initiated && stats.all_port_in) {
        gauges.push({
            "gauge": "initiated",
            "link": stats.porting_not_initiated.link,
            "countx": stats.porting_not_initiated.count,
            "county": stats.all_port_in.count,
            "labely": "port-in"
        });
    }
    if (stats.porting_not_approved && stats.porting_initiated) {
        gauges.push({
            "gauge": "accepted",
            "list": stats.porting_not_approved.list,
            "countx": stats.porting_not_approved.count,
            "county": stats.porting_initiated.count,
            "labely": "port-in initiated"
        });
    }
    if (stats.porting_fail && stats.porting_approved) {
        gauges.push({
            "gauge": "failedport",
            "list": stats.porting_fail.list,
            "countx": stats.porting_fail.count,
            "county": stats.porting_approved.count,
            "labely": "port-in accepted"
        });
    }
    if (stats.not_yet_dispatched && stats.accepted) {
        gauges.push({
            "gauge": "dispatch",
            "link": stats.not_yet_dispatched.link,
            "countx": stats.not_yet_dispatched.count,
            "county": stats.accepted.count,
            "labely": "accepted SO"
        });
    }
    if (stats.not_yet_delivered && stats.dispatched) {
        gauges.push({
            "gauge": "delivered",
            "link": stats.not_yet_delivered.link,
            "countx": stats.not_yet_delivered.count,
            "county": stats.dispatched.count,
            "labely": "dispatched"
        });
    }
    if (stats.for_redelivery && stats.dispatched) {
        gauges.push({
            "gauge": "redelivery",
            "link": stats.for_redelivery.link,
            "countx": stats.for_redelivery.count,
            "county": stats.dispatched.count,
            "labely": "dispatched"
        });
    }
    if (stats.no_install && stats.delivered) {
        gauges.push({
            "gauge": "app",
            "link": stats.no_install.link,
            "countx": stats.no_install.count,
            "county": stats.delivered.count,
            "labely": "delivered",
            "list": stats.no_install.list
        });
    }
    if (stats.no_bonus && stats.installs) {
        gauges.push({
            "gauge": "bonus",
            "link": stats.no_bonus.link,
            "countx": stats.no_bonus.count,
            "county": stats.installs.count,
            "labely": "installs",
            "list": stats.no_bonus.list
        });
    }
    if (stats.no_referral_bonus && stats.no_referral_bonus) {
        gauges.push({
            "gauge": "referral",
            "list": stats.no_referral_bonus.list,
            "countx": stats.no_referral_bonus.count,
            "county": stats.referral_bonus.count + stats.no_referral_bonus.count,
            "labely": "referral"
        });
    }
    if (stats.customers_signed_on && stats.all_customers) {
        gauges.push({
            "gauge": "signed-plus",
            "link": stats.customers_signed_on.link,
            "countx": stats.customers_signed_on.count,
            "county": stats.all_customers.count,
            "labely": "registrations"
        });
    }
    if (stats.already_ordered && stats.customers_signed_on) {
        gauges.push({
            "gauge": "ordered-plus",
            "link": stats.already_ordered.link,
            "countx": stats.already_ordered.count,
            "county": stats.customers_signed_on.count,
            "labely": "logins"
        });
    }
    if (stats.payment_success && stats.all_orders) {
        gauges.push({
            "gauge": "finished-plus",
            "link": stats.payment_success.link,
            "countx": stats.payment_success.count,
            "county": stats.all_orders.count,
            "labely": "orders"
        });
    }
    if (stats.approved && stats.payment_success) {
        gauges.push({
            "gauge": "document-plus",
            "link": stats.approved.link,
            "countx": stats.approved.count,
            "county": stats.payment_success.count,
            "labely": "paid"
        });
    }
    if (stats.accepted && stats.approved) {
        gauges.push({
            "gauge": "so-plus",
            "link": stats.accepted.link,
            "countx": stats.accepted.count,
            "county": stats.approved.count,
            "labely": "approved"
        });
    }
    if (stats.porting_initiated && stats.all_port_in) {
        gauges.push({
            "gauge": "initiated-plus",
            "link": stats.porting_initiated.link,
            "countx": stats.porting_initiated.count,
            "county": stats.all_port_in.count,
            "labely": "port-in"
        });
    }
    if (stats.porting_approved && stats.porting_initiated) {
        gauges.push({
            "gauge": "accepted-plus",
            "list": stats.porting_approved.list,
            "countx": stats.porting_approved.count,
            "county": stats.porting_initiated.count,
            "labely": "port-in initiated"
        });
    }
    if (stats.porting_success && stats.porting_approved) {
        gauges.push({
            "gauge": "failedport-plus",
            "list": stats.porting_success.list,
            "countx": stats.porting_success.count,
            "county": stats.porting_approved.count,
            "labely": "port-in accepted"
        });
    }
    if (stats.dispatched && stats.accepted) {
        gauges.push({
            "gauge": "dispatch-plus",
            "link": stats.dispatched.link,
            "countx": stats.dispatched.count,
            "county": stats.accepted.count,
            "labely": "accepted SO"
        });
    }
    if (stats.delivered && stats.dispatched) {
        gauges.push({
            "gauge": "delivered-plus",
            "list": stats.delivered.list,
            "countx": stats.delivered.count,
            "county": stats.dispatched.count,
            "labely": "dispatched"
        });
    }
    if (stats.installs && stats.installs) {
        gauges.push({
            "gauge": "app-plus",
            "link": stats.installs.link,
            "countx": stats.installs.count,
            "county": stats.delivered.count,
            "labely": "delivered",
            "list": stats.installs.list
        });
    }
    if (stats.bonus && stats.installs) {
        gauges.push({
            "gauge": "bonus-plus",
            "link": stats.bonus.link,
            "countx": stats.bonus.count,
            "county": stats.installs.count,
            "labely": "installs",
            "list": stats.bonus.list
        });
    }
    if (stats.referral_bonus && stats.no_referral_bonus) {
        gauges.push({
            "gauge": "referral-plus",
            "list": stats.referral_bonus.list,
            "countx": stats.referral_bonus.count,
            "county": stats.referral_bonus.count + stats.no_referral_bonus.count,
            "labely": "referral"
        });
    }

    gauges.forEach(function (item) {
        var gauge = undefined;
        var target = $("#" + item.gauge + "-gauge")[0];
        if (item.gauge.slice(-5) == "-plus") {
            opts.lineWidth = 0.1;
            opts.angle = 0.5;
            gauge = new Donut(target).setOptions(opts);
        } else {
            gauge = new Gauge(target).setOptions(opts);
        }
        gauge.maxValue = item.county;
        gauge.set(item.county);				// to avoid breaking when actual value is 0
        gauge.set(item.countx);
        gauge.setTextField($("#" + item.gauge + "-text")[0]);
        $("#" + item.gauge + "-text").siblings(1)[0].innerHTML = "out of " + item.county + " " + item.labely;
        $("#" + item.gauge + "-gauge").click(function () {
            ecommList(item.link, item.list)
        });
    });
}

function download(arr) {
    var dataString = "";
    if (arr[0].reference_number) {
        dataString += "EMAIL REFERENCE NUMBER\n\n";
    } else if (arr[0].referral_code) {
        dataString += "NUMBER NAME CODE \n\n";
    }

    arr.forEach(function (item) {
        if (typeof(item) == "object") {
            var customer_name = item.customer_name ? item.customer_name.replace(/ /g, "_") : "";
            if (item.reference_number) {
                dataString += item.email + " " + item.reference_number + " " + item.number + "\n";
            } else if (item.referral_code) {
                dataString += item.number + " " + customer_name + " " + item.referral_code + "\n";
            } else if (item.created_at) {
                dataString += item.email + ",'" + item.created_at + "'\n";
            } else {
                dataString += item.email + "," + item.date + "\n";
            }
        } else {
            dataString += item + "\n";
        }
    });
    downloadCSV(dataString, "download.csv", "text/csv");
}

function downloadECApiReport(arr) {
    var start = formatDataDatePicker(new Date($("#startDate").val()));
    var end = formatDataDatePicker(new Date($("#endDate").val()));
    var apiMethod = $("#apiMethod").val();
    var points = parseInt($("#points").val());

    var options = [];
    options.push({display: 'Start:', key: 'start', value: start, type: 'DateTime', mandatory: true});
    options.push({display: 'End:', key: 'end', value: end, type: 'DateTime', mandatory: true});
    options.push({display: 'Points', key: 'points', value: points, type: 'Integer', mandatory: true});
    options.push({display: 'Method', key: 'method', value: apiMethod, type: 'String'});

    var params = {
        title: 'Api Report',
        key: 'reportForm',
        type: 'Generate',
        options: options
    };

    createFormDialog(params, function (values, close, dialogRef, buttonRef) {

        var startDate = new Date(values.start);
        var endDate = new Date(values.end);
        var points = parseInt(values.points);
        var method = values.method;

        var maxPoints = (endDate.getTime() - startDate.getTime()) / (60 * 1000); // per 5 min
        if (maxPoints < points) {
            showToast("danger", "Invalid amount of points (" + maxPoints + " max)");
            return;
        }

        points += 1;

        buttonRef.disable();
        buttonRef.spin();
        dialogRef.setClosable(false);

        setTimeout(function () {

            ajax("GET", "/api/1/web/stats/ec?points=" + points + "&start="
                + startDate.toISOString() + "&end=" + endDate.toISOString() + (method ? "&method=" + method : ""), {}, true,
                function (response) {
                    close();

                    var dataString = "API TIME REQ_COUNT TIMEOUTS_COUNT ERRORS_COUNT REQ_AVG_MSEC REQ_MAX_MSEC\n";
                    response.data.forEach(function (item) {
                        var apiName = item.api ? item.api : "All-In-One";
                        item.list.forEach(function (item) {
                            dataString += apiName + " " + item.blockTime + " " + parseInt(item.totalRequests)
                            + " " + parseInt(item.totalTimeouts) + " " + parseInt(item.totalErrors)
                            + " " + parseInt(item.avgTime) + " " + parseInt(item.maxTime) + "\n";
                        });
                    });
                    downloadCSV(dataString, "ec_api_" + startDate.toISOString() + "-" + endDate.toISOString() + ".csv", "text/csv");
                },
                function () {
                    buttonRef.enable();
                    buttonRef.stopSpin();
                    dialogRef.setClosable(true);
                }, showToast);

        }, 1000);
    });
}

function showEcommList(obj) {
    if ($("#ecommList").dataTableSettings.length > 0) $("#ecommList").DataTable().fnDestroy();
    $("#ecommList tbody").empty();
    $("#ecommList thead tr").empty();
    var arr;
    if (obj && obj[0] && obj[0].reference_number) {
        arr = obj;
        if (arr && arr[0] && (typeof(arr[0].reinitiated) != "undefined"))
            $("#ecommList thead tr").append("<TH>Email</TH><TH>Date</TH><TH>Order Reference</TH><TH>MSISDN</TH><TH>Reinitiated</TH>");
        else
            $("#ecommList thead tr").append("<TH>Email</TH><TH>Date</TH><TH>Order Reference</TH><TH>MSISDN</TH>");
        $("#download").click(function () {
            download(obj);
        });
    } else if (obj && obj[0] && obj[0].referral_code) {
        arr = obj;
        $("#ecommList thead tr").append("<TH>Joined MSISDN</TH><TH>Referral Code</TH><TH>Date</TH><TH>Reuse Code</TH>");
        $("#download").click(function () {
            download(obj);
        });
    } else if (obj && obj[0] && obj[0].email) {
        arr = obj;
        $("#ecommList thead tr").append("<TH>Email</TH><TH>Date</TH>");
        $("#download").click(function () {
            download(obj);
        });
    }
    if (!arr || (arr.length == 0)) return false;
    arr.forEach(function (item) {
        if (typeof(item.reinitiated) != "undefined") {
            var reinit = (item.reinitiated) ? "<INPUT type=checkbox checked disabled>" : "<INPUT type=checkbox disabled>";
            $("#ecommList > tbody:last-child").append("<TR>" +
            "<TD>" + item.email + "</TD>" +
            "<TD>" + getDate(new Date(item.date)) + "</TD>" +
            "<TD>" + item.reference_number + "</TD>" +
            "<TD>" + getCustomerNumberLink(item.number) + "</TD>" +
            "<TD>" + reinit + "</TD>" +
            "</TR>");
        } else if (typeof(item.delivery_date) != "undefined") {
            var delivery = (!item.delivery_date) ? "" : item.delivery_date;
            $("#ecommList > tbody:last-child").append("<TR>" +
            "<TD>" + item.email + "</TD>" +
            "<TD>" + delivery + "</TD>" +
            "<TD>" + item.reference_number + "</TD>" +
            "<TD>" + getCustomerNumberLink(item.number) + "</TD>" +
            "</TR>");
        } else if (item.reference_number) {
            $("#ecommList > tbody:last-child").append("<TR>" +
            "<TD>" + item.email + "</TD>" +
            "<TD>" + getDate(new Date(item.date)) + "</TD>" +
            "<TD>" + item.reference_number + "</TD>" +
            "<TD>" + getCustomerNumberLink(item.number) + "</TD>" +
            "</TR>");
        } else if (item.referral_code) {
            var button = "<BUTTON class='btn btn-primary btn-xs' onClick='userReferralCode(" +
                "\"" + item.number + "\", " +
                "\"" + item.referral_code + "\");'>Use</BUTTON>";
            $("#ecommList > tbody:last-child").append("<TR>" +
            "<TD>" + getCustomerNumberLink(item.number) + "</TD>" +
            "<TD>" + getCustomerReferralCodeLink(item.referral_code) + "</TD>" +
            "<TD>" + getDate(new Date(item.ts)) + "</TD>" +
            "<TD>" + ((item.referral_code && item.referral_code != '-') ? button : "") + "</TD>" +
            "</TR>");
        } else {
            $("#ecommList > tbody:last-child").append("<TR>" +
            "<TD>" + item.email + "</TD>" +
            "<TD>" + getDate(new Date(item.created_at)) + "</TD>" +
            "</TR>");
        }
    });
    $("#myModal").modal();
    $("#ecommList").DataTable({"bAutoWidth": false});
}

function showEcommReg(result) {
    var flotOptions = {
        "series": {
            "points": {"show": true, "radius": 1},
            "splines": {"show": true, "fill": true}
        },
        "xaxis": {
            "mode": "categories",
            "tickLength": 17,
        },
        "grid": {"hoverable": true, "clickable": true},
        "tooltip": true,
        "tooltipOpts": {"defaultTheme": false, "content": "%s %y"}
    };
    var graph = new Array;
    if (result) Object.keys(result).forEach(function (key) {
        var data = new Array;
        result[key].count.forEach(function (c, idx) {
            data.push([result[key].dates[idx], c]);
        });
        data.sort(function (a, b) {
            if (a[0] < b[0]) return -1;
            else return 1;
        });
        for (i = 0; i < data.length - 14; i++) {
            delete data[i];
        }
        graph.push({"data": data, "label": key, "emails": result[key].emails});
    });
    $.plot($("#registrationStats"), graph, flotOptions);
    $("#registrationStats").bind("plotclick", function (event, pos, item) {
        if (item && item.series && item.series.emails) {
            $("#showEmails tbody").empty();
            item.series.emails[item.dataIndex].forEach(function (email) {
                $("#showEmails > tbody:last-child").append("<TR>" +
                "<TD>" + email + "</TD>" +
                "<TD>" + item.series.label + "</TD>" +
                "</TR>");
            });
        }
    });
}

function showEcommPayments(result, resultTop) {
    var plotOptions = {
        "series": {
            "points": {"show": true, "radius": 1},
            "splines": {"show": true, "fill": true}
        },
        "xaxis": {
            "mode": "categories",
            "tickLength": 17
        },
        "grid": {"hoverable": true, "clickable": true},
        "tooltip": true,
        "legend": {"show": false},
        "tooltipOpts": {"defaultTheme": false, "content": "%s %y"}
    };
    var graph = [];

    if (result) {
        Object.keys(result).forEach(function (key) {
            var data = [];
            result[key].forEach(function (item, idx) {
                data.push([item._id, item.count, {numbers: item.numbers}]);
            });
            var label = key;
            if (key === "listTotal") {
                label = "New Joined Customers";
            } else if (key === "listReferrals") {
                label = "Used Referrals Codes";
            } else if (key === "listGrabCodes") {
                label = "Used Grab Codes";
            } else if (key === "listLazadaCodes") {
                label = "Used LAZADA Codes";
            }

            graph.push({"data": data, "label": label});
        });
    }

    $.plot($("#registrationStats"), graph, plotOptions);
    $("#registrationStats").bind("plotclick", function (event, pos, item) {
        var meta = item.series.data[item.dataIndex][2];
        var numbers = meta.numbers;
        numbers.forEach(function (item) {
            if (!item.referral_code) {
                item.referral_code = "-";
            }
        });
        showEcommList(numbers)
    });

    $("#showReferralTopCodes tbody").empty();
    if (resultTop && resultTop.listTop100) {
        var count = 0;
        resultTop.listTop100.forEach(function (item) {
            if (count < 10) { //temp workaround until pagination is added
                $("#showReferralTopCodes").append("<TR>" +
                "<TD>" + getCustomerReferralCodeLink(item._id) + "</TD>" +
                "<TD>" + item.count + "</TD>" +
                "</TR>");
            }
            count++;
        });
    }
}

function flipGauge(event, id, negative) {
    event.preventDefault();
    if (negative) {
        $("#" + id + "-div").hide();
        $("#" + id + "-plus-div").show();
    } else {
        $("#" + id + "-div").show();
        $("#" + id + "-plus-div").hide();
    }
}

function userReferralCode(number, code, callback) {
    if (!code || code.length == 0) {
        if (callback) callback();
        return showToast("danger", "Code is empty");
    }

    BootstrapDialog.show({
        title: 'Referral Code',
        message: 'Do you want to simulate usage of code ' + code + ' for ' + number + '?',
        onshown: function (dialog) {
            var tier = $('.bootstrap-dialog').length - 1;
            dialog.$modal.prev(".modal-backdrop")
                .css("z-index", 1060 + tier * 30);
            dialog.$modal
                .css("z-index", 1070 + tier * 30);
        },
        buttons: [{
            label: 'Use',
            action: function (dialog) {
                var $button = this; // 'this' here is a jQuery object that wrapping the <button> DOM element.
                $button.disable();
                $button.spin();
                dialog.setClosable(false);

                var parameters = {
                    "number": number,
                    "prefix": 65
                };
                ajax("PUT", "/api/1/web/customers/referral/use/" + code, parameters, true, function (data) {
                    if (data.added) {
                        showToast("success", "Referral code has been used");
                    } else {
                        showToast("warning", "Referral code has NOT been used");
                    }
                    $button.enable();
                    $button.stopSpin();
                    dialog.close();
                    if (callback) callback();
                }, function () {
                    $button.enable();
                    $button.stopSpin();
                    dialog.close();
                }, showToast);
            }
        }, {
            label: 'Cancel',
            action: function (dialog) {
                dialog.close();
                if (callback) callback(0);
            }
        }]
    });
}
