function initBannedEmails(input) {
    $(".username")[0].innerHTML = getCookie("cmsuser");
    var query = (input) ? "?search=" + input.search.value : "";
    ajax("GET", "/api/1/web/admin/emails/banned/get" + query, {}, true, function (data) {
        if (data && (data.code == 0)) {
            showBannedEmails(data.list);
        } else {
            alert("Fail");
        }
    });
}

function banEmail(input) {
    $(".username")[0].innerHTML = getCookie("cmsuser");
    var email = input.banNew.value;

    if(confirm("Are you sure you want to ban '" + email + "'?")) {
        input.banNew.disabled = true;

        ajax("PUT", "/api/1/web/admin/emails/banned/" + email, {}, true, function (data) {
            if (data && (data.code == 0)) {
                initBannedEmails();
                input.banNew.disabled = false;
                input.banNew.value = '';
            } else {
                alert("Fail");
            }
        });
    }
}

function unblockBannedEmails(email) {
    $(".username")[0].innerHTML = getCookie("cmsuser");
    ajax("DELETE", "/api/1/web/admin/emails/banned/" + email, {}, true, function (data) {
        if (data && (data.code == 0)) {
            initBannedEmails();
        } else {
            alert("Fail");
        }
    });
}

function showBannedEmails(list) {
    $("#showLogs tbody").empty();
    list.forEach(function (item) {
        $("#showLogs > tbody:last-child").append("<TR>" +
        "<TD>" + item.email + "</TD>" +
        "<TD><A href=# onClick='if(confirm(\"Are you sure?\")) " + "unblockBannedEmails(\"" + item.email + "\");'>" +
        "<I class='fa fa-trash-o'></I></A>" +
        "</TD>" +
        "</TR>");
    });
}