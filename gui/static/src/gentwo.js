var timer = new Object;

function init(type) {
    $(".username")[0].innerHTML = getCookie("cmsuser");
    if ( type == "admin" ) {
        getFinance();
        getGentwo("plans");
        getGentwo("topup");
        getRates(document.forms.searchForm);
        return false;
    } else if ( type == "cdr" ) {
        getCDR();
    }
}

function getGentwo(type) {
    $.ajax({	// use this to avoid popup
        "dataType": "json",
        "type": "GET",
        "url": "/api/1/web/gentwo/get/" + type,
        "data": "",
        "success": function (data) {
            if ( data && (data.code == 0) ) {
                if (type == "active") showActive(data.list);
                else if (type == "online") showOnline(data.list);
                else if (type == "plans") showPlans(data.plans, data.providers);
                else if (type == "topup") showTopup(data.list);
            } else showToast("warning", "Fail " + type);
        },
    });
}

function getCDR(date) {
    var parameters = (date) ? "date=" + date : "";
    ajax("GET", "/api/1/web/gentwo/get/cdr", parameters, true, function (data) {
        if ( data && (data.code == 0) ) {
            showCDR(data.list);
        } else showToast("warning", "Fail cdr");
    });
}

function getTrace(call_id) {
    var parameters = { "call_id" : call_id };
    ajax("GET", "/api/1/web/gentwo/get/cdr", parameters, true, function (data) {
        if ( data && (data.code == 0) ) {
            showTrace(data.list);
        } else showToast("warning", "Fail cdr trace");
    });
}

function getFinance(month, year) {
    var parameters = { "month" : month, "year" : year };
    ajax("GET", "/api/1/web/gentwo/get/finance", parameters, true, function (data) {
        if ( data && (data.code == 0) ) {
            $("#monthSelect").removeAttr('disabled');
            showFinance(data);
        } else showToast("warning", "Fail finance");
    });
}

function getRates(form) {
    if (form.search.value == "") return false;
    var parameters = new Object;
    parameters.lcr = form.lcr.checked;
    parameters.prefix = form.search.value;
    parameters.provider = (form.provider.value == "") ? undefined : form.provider.value;
    ajax("GET", "/api/1/web/gentwo/get/rates", parameters, true, function (data) {
        if ( data && (data.code == 0) ) {
            showRatesheet(data.rates);
            showProviders(data.providers);
            if (parameters.lcr) {
                $('#provider').attr('disabled', 'disabled');
                $('.toggle').attr('disabled', 'disabled');
                $("#providerEdit").html("");
            } else {
                $("#provider").removeAttr('disabled');
                if (form.provider.value) $(".toggle").removeAttr('disabled');
                else $('.toggle').attr('disabled', 'disabled');
            }
        } else showToast("warning", "Fail getRates");
    });
}

function uploadRates(id, form) {
    var xmlhttp;            // bug: move to jquery
    if ( window.XMLHttpRequest ) xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if ( xmlhttp.readyState == 4 ) {
            form.update.disabled = false;
            $("#submitSpin").hide();
            if ( xmlhttp.status == 403 ) {
                location.reload();
            } else if ( xmlhttp.status == 200 ) {
                var data = JSON.parse(xmlhttp.responseText);
                if ( data.code == 0 ) {
                    showToast("success", "Success");
                    $("#myModal").modal('hide');
                    getRates(document.forms.searchForm);
                    window.location.hash = '#showRates';
                } else showToast("warning", "Fail upload parse");
            } else showToast("warning", "Failed upload ");
        }
    }
    var fData = new FormData();
    fData.append("name", form.provider.value);
    fData.append("host", form.host.value);
    fData.append("prefix", form.prefix.value);
    fData.append("rounding", form.rounding.value);
    if (form.file.files[0]) {
        fData.append("content", form.file.files[0]);
    }
    form.update.disabled = true;
    $("#submitSpin").show();
    xmlhttp.open("PUT", "/api/1/web/gentwo/put/rates/" + id, true);
    xmlhttp.send(fData);

    return false;
}

function updatePlan(plan, provider, enable) {
    var parameters = { "providerID" : provider, "enable" : enable };
    ajax("PUT", "/api/1/web/gentwo/put/plan/" + plan, parameters, true, function (data) {
        if ( data && (data.code == 0) ) {
            getGentwo("plans");
        } else showToast("warning", "Fail plan update");
    });
}

function toggleWeight(form, enable) {
    var parameters = { "prefix" : form.search.value, "enable" : enable };
    ajax("PUT", "/api/1/web/gentwo/put/weight/" + form.provider.value, parameters, true, function (data) {
        if ( data && (data.code == 0) ) {
            getRates(form);
        } else showToast("warning", "Fail toggle weight");
    });
}

function validateInapp(type, id, token, sku) {
    var parameters = { "type" : type, "id" : id, "token" : token, "sku" : sku };
    ajax("GET", "/api/1/web/gentwo/get/inapp", parameters, true, function (data) {
        if ( data && (data.code == 0) ) {
            var obj = data && data.list && data.list[0] ? data.list[0] : undefined;
            if (obj) {
                if (obj.code == 0)  {
                    showToast("success", "Token Valid");
                } else if (obj.code == -1) {
                    if (confirm("Invalid or refunded token. Reverse in-app purchase?")) {
                        var parameters = { "type" : type, "token" : token, "sku" : sku };
                        ajax("PUT", "/api/1/web/gentwo/put/inapp/reverse", parameters, true, function (data) {
                            if ( data && (data.code == 0) ) {
                                getGentwo("topup");
                                showToast("success", "Reversed in-app purchase");
                            } else {
                                showToast("warning", "Reverse failed");
                            }
                        });
                    } else {
                        showToast("warning", "Ignored refund");
                    }
                } else {
                    showToast("warning", "Unknown Status");
                }
            } else showToast("warning", "Fail validate inapp");
        } else showToast("warning", "Fail validate inapp");
    });
}

function showMonths(monthSelected, yearSelected) {
    var getMonth = [ "January", "February", "March", "April", 
            "May", "June", "July", "August", 
            "September", "October", "November", "December" ];
        $("#monthSelect").empty();
    var currentMonth = new Date().getMonth();
    var year = new Date().getFullYear();
    for (var i=0; i<12; i++) {
        var month = currentMonth - i;
        if (month < 0) {
            year-=1;
            month+=12;
        }
        if (year < 2016) break;
        var selected = ((month == monthSelected) && (year == yearSelected)) ? "selected" : "";
            $("#monthSelect").append("<OPTION value='" + month + ";" + year + "' " + selected + ">" + getMonth[month] + " " + year + "</OPTION>");
    }
    $("#monthSelect").change(function () {
        $(this).attr('disabled', 'disabled');
        $(this).unbind('change');
        getFinance(this.value.split(";")[0], this.value.split(";")[1]);
    });
}

function showOnline(list) {
    if ($("#showOnline").dataTableSettings.length > 0) $("#showOnline").DataTable().fnDestroy();
    $("#showOnline tbody").empty();
    if (list.length>0) list.forEach(function (rpc) {
        if (rpc && rpc.result && rpc.result.AoRs) rpc.result.AoRs.forEach(function (aor) {
            if (aor.Info && aor.Info.Contacts) aor.Info.Contacts.forEach(function (item) { 
                if (item && item.Contact) $("#showOnline > tbody:last-child").append("<TR>" +
                    "<TD>" + aor.Info.AoR + "</TD>" +
                    "<TD>" + item.Contact.Address + "</TD>" +
                    "<TD>" + new Date(item.Contact["Last-Modified"] * 1000) + "</TD>" +
                    "<TD>" + s2time(item.Contact.Expires) + "</TD>" +
                    "</TR>");
            });
        });
    });
    $("#showOnline").DataTable();
}

function showActive(list) {
//    if ($("#showActive").dataTableSettings.length > 0) $("#showActive").DataTable().fnDestroy();
    $("#showActive tbody").empty();
    if (list.length>0) list.forEach(function (item) {
        clearInterval(timer[item.call_id.replace(/[^a-z0-9]+|\s+/gmi, "")]);
        var diff = s2time(Math.ceil((new Date().getTime() - item.start_time) / 1000));
        $("#showActive > tbody:last-child").append("<TR>" +
            "<TD>" + item.orig_ani + "</TD>" +
            "<TD>" + item.orig_dst_number + "</TD>" +
            "<TD id=timer_" + item.orig_ani + ">" + diff + "</TD>" +
            "<TD>" + item.server + "</TD>" +
            "</TR>");
        timer[item.call_id.replace(/[^a-z0-9]+|\s+/gmi, "")] = setInterval(function () {
            var prev = time2s($("#timer_" + item.orig_ani).html());
            $("#timer_" + item.orig_ani).html(s2time(prev + 1));
        }, 1000);
    });
//    $("#showActive").DataTable();
} 

function showFinance(data) {
    showMonths(data.month, data.year);
    $("#showFinance tbody").empty();
    var future = data.list.filter(function (o) { if (o.future_usage) return o; else return undefined; });
    data.list.forEach(function (item) {
        var value = ((item.value == "System Liability") && future && future[0]) ? item.value + future[0].future_usage : item.value;
        if (item.name) $("#showFinance > tbody:last-child").append("<TR>" +
            "<TD>" + item.name + "</TD>" +
            "<TD>" + item.date + "</TD>" +
            "<TD>" + value + "</TD>" +
        "</TR>");
    });
}

function showPlans(plans, providers) {
    var getProvider = function(id) {
        providers.forEach(function(item) {
            if (item.providerID == id) return item;
        });
    }
    $("#showPlans tbody").empty();
    $("#showPlans thead").empty();
    $("#showPlans thead").append("<TR><TH>ID</TH><TH>Plan</TH></TR>");
    providers.forEach(function(item) {
        $("#showPlans thead tr").append("<TH><A href=# id=edit_" + item.providerID + ">" + item.name + "</A></TH>");
        $("#edit_" + item.providerID).click(function () {
            showProvider(item);
        });
    });
    plans.forEach(function(item) {
        if (item) {
            var row = "<TD>" + item.planID + "</TD><TD>" + item.name + "</TD>";
            var pList = (item.providerID) ? item.providerID.split(',') : [];
            providers.forEach(function(p) {
                if ( pList.indexOf(p.providerID.toString()) > -1 ) {
                    row += "<TD><INPUT class=planSelect type=checkbox checked provider=" + p.providerID + " plan=" + item.planID + "></TD>";
                } else {
                    row += "<TD><INPUT class=planSelect type=checkbox provider=" + p.providerID + " plan=" + item.planID + "></TD>";
                }
            });
            $("#showPlans > tbody:last-child").append("<TR>" + row + "</TR>");
        }
    });
    $(".planSelect").change(function() {
        updatePlan($(this).attr('plan'), $(this).attr('provider'), $(this).is(':checked'));
    });
}

function showTopup(list) {
    if ($("#showTopup").dataTableSettings.length > 0) $("#showTopup").DataTable().fnDestroy();
    $("#showTopup tbody").empty();
    list.forEach(function (item) {
        var id = item && item.id ? item.id.replace(".","").replace("-","") : "";
        $("#showTopup tbody").append("<TR>" +
            "<TD>" + getDateTime(item.ts) + "</TD>" +
            "<TD>" + item.type + "</TD>" +
            "<TD>" + item.number + "</TD>" +
            "<TD>" + (item.id ? item.id.slice(-5) : item.id) + "</TD>" +
            "<TD>" + item.amount + "</TD>" +
            "<TD>" + item.credited + "</TD>" +
            "<TD><A id=" + id + " href=#>" + item.voided + "</TD>" +
        "</TR>");
        if (id) $("#" + id).click(function () {
            event.preventDefault();
            if (item.id == "") return;
            if (parseInt(item.voided) == 1) return;
            validateInapp(item.type, item.id, item.purchase_token, item.sku);
        });
    });
    $("#showTopup").DataTable({"aaSorting": []});
}

function showTrace(list) {
    $("#gentwoModal tbody").empty();
    list.forEach(function (item) {
        var dt = new Date(new Date(item.time_stamp).getTime() + (8 * 60 * 60 * 1000)).toISOString();
        $("#gentwoModal > tbody:last-child").append("<TR>" +
            "<TD>" + dt + "<BR>" +
            "     SRC : " + item.fromip + "<BR>" +
            "     DST : " + item.toip + "</TD>" +
            "<TD>" + item.msg.replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/\r?\n/g, '<br />') + "</TD>" +
            "</TR>");
    });
    $("#myModal").modal();
}

function showCDR(list) {
    if ($("#showCDR").dataTableSettings.length > 0) $("#showCDR").DataTable().fnDestroy();
    $("#showCDR tbody").empty();
    list.forEach(function (item) {
        var datetime = (item.answer_time) ?
                getDateTime(new Date(item.answer_time)) :
                getDateTime(new Date(item.start_time));
        if (item.top_up) {
            $("#showCDR > tbody:last-child").append("<TR>" +
                "<TD>" + datetime + "</TD>" +
                "<TD>" + item.orig_ani + "</TD>" +
                "<TD>Top Up</TD>" +
                "<TD></TD>" +
                "<TD></TD>" +
                "<TD></TD>" +
                "<TD></TD>" +
                "<TD>" + ((item.top_up < 0) ? Math.abs(item.top_up) : "- " + item.top_up) + "</TD>" +
                "<TD>" + item.credit + "</TD>" +
                "<TD></TD>" +
                "<TD></TD>" +
                "<TD></TD>" +
            "</TR>");
        } else {
            var cost = (item.egress_cost) ? item.egress_cost : 0;
            var charge = (item.ingress_cost) ? item.ingress_cost : 0;
            var media = (item.media) ? item.media : "";
            $("#showCDR > tbody:last-child").append("<TR>" +
                "<TD>" + datetime + "</TD>" +
                "<TD>" + item.orig_ani + "</TD>" +
                "<TD>" + item.orig_dst_number + "</TD>" +
                "<TD>" + (item.egress_carrier_name ? item.egress_carrier_name : "") + "</TD>" +
                "<TD>" + (item.term_code_name ? item.term_code_name : "") + "</TD>" +
                "<TD>" + (item.ingress_billtime ? item.ingress_billtime : 0) + "</TD>" +
                "<TD>" + cost + "</TD>" +
                "<TD>" + charge + "</TD>" +
                "<TD>" + (item.credit ? parseInt(item.credit * 1000000) / 1000000 : 0 ) + "</TD>" +
                "<TD>" + media + "</TD>" +
                "<TD><A href=# id=" + item.call_id.replace(/[^a-z0-9]+|\s+/gmi, "") + ">" + item.release_side + "</A></TD>" +
                "<TD>" + item.release_cause + "</TD>" +
            "</TR>");
            $("#" + item.call_id.replace(/[^a-z0-9]+|\s+/gmi, "")).click(function () {
                event.preventDefault();
                getTrace(item.call_id);
            });
        }
    });
    $("#showCDR").DataTable({"aaSorting": []});
}

function showProvider(provider) {
    $("#gentwoModal tbody").empty();
    $("#gentwoModal > tbody:last-child").append("<FORM class=form-horizontal onSubmit='event.preventDefault(); uploadRates(" + provider.providerID + ", this);'>" +
            "<DIV class=form-group><LABEL for=provider class='col-xs-3 control-label'>Name : </LABEL><DIV class=col-xs-9><INPUT type=text id=provider name=provider class=form-control value='" + provider.name + "' /></DIV></DIV>" +
            "<DIV class=form-group><LABEL for=host class='col-xs-3 control-label'>Host : </LABEL><DIV class=col-xs-9><INPUT type=text id=host name=host class=form-control value='" + provider.host + "' /></DIV></DIV>" +
            "<DIV class=form-group><LABEL for=prefix class='col-xs-3 control-label'>Prefix : </lABEL><DIV class=col-xs-9><INPUT type=text id=prefix name=prefix class=form-control value='" + provider.prefix + "' /></DIV></DIV>" +
            "<DIV class=form-group><LABEL for=rounding class='col-xs-3 control-label'>Rounding : </LABEL><DIV class=col-xs-9><INPUT type=number id=rounding name=rounding max=10 min=0 class=form-control value='" + provider.rounding + "' /></DIV></DIV>" + 
            "<DIV class=form-group><LABEL for=file class='col-xs-3 control-label'>Ratesheet : </LABEL><DIV class='btn-group col-xs-9'><a href=# type='button' class='btn btn-sm fileinput-button'><i class='fa fa-upload'></i>Upload Ratesheet<INPUT id=file name=file type='file'></a></DIV></DIV>" +
            "<DIV class=form-group><LABEL class=col-xs-3>&nbsp;</LABEL><DIV class=col-xs-9><INPUT type=submit class='btn btn-primary' name=update value=Update><i id=submitSpin class='fa fa-spinner fa-spin' style='display:none'></i></DIV></DIV></FORM>");
    $("#myModal").modal();
}

function showProviders(providers) {
    if ( $("#provider").children('option').length > 0 ) return false;
    $("#provider").empty();
    $("#provider").append("<OPTION></OPTION>");
    providers.forEach(function (item) {
        $("#provider").append("<OPTION value=" + item.providerID + ">" + item.name + "</OPTION>");
    });
}

function showRatesheet(list) {
    if ($("#showRates").dataTableSettings.length > 0) $("#showRates").DataTable().fnDestroy();
    $("#showRates tbody").empty();
    list.forEach(function (item) {
        var checked = item.weight ? "checked" : "";
        $("#showRates > tbody:last-child").append("<TR>" +
            "<TD><INPUT type=checkbox " + checked + " onClick='return false;'></TD>" +
            "<TD>" + item.prefix + "</TD>" +
            "<TD>" + item.destination + "</TD>" +
            "<TD>" + item.buyrate + "</TD>" +
            "<TD>" + item.sellrate + "</TD>" +
            "<TD>" + item.provider + "</TD>" +
            "<TD>" + item.billstart + "</TD>" +
            "<TD>" + item.billincrement + "</TD>" +
            "<TD>" + item.gname + "</TD>" +
            "<TD>" + item.crstatus + "</TD>" +
            "<TD>" + item.eff + "</TD>" +
        "</TR>");
    });
    $("#showRates").DataTable();
} 
