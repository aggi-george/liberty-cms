function init(type) {
	$(".username")[0].innerHTML = getCookie("cmsuser");
}

var DEFAULT_RELOAD_CALLBACK = function(){
    loadIccids();
}

DEFAULT_RELOAD_CALLBACK();
function loadIccids(){
    $.ajax({
        "type": "GET",
        "dataType": "json",
        "url": "/api/1/web/iccids/get/iccids",
        "data": {},
        "success": function (response) {
            if(response.code == 0){
                generateTable("iccidsTable", response.list, [
                    {"sTitle": "Id", "sClass": "twocol-table-column", "mData": "_id", "mRender": function(data, type, full){
                            return data;
                        }
                    },
                    {"sTitle": "Status", "sClass": "twocol-table-column", "mData": "status", "mRender": function(data, type, full){
                            return data;
                        }
                    },
                    {"sTitle": "Purpose", "sClass": "twocol-table-column", "mData": "purpose", "mRender": function(data, type, full){
                            return data;
                        }
                    }
                ], 10, true);
                
            }else{
                showToast("warning", "Error fetching iccids list");
            }
        },
        "error": function (result) {
            showToast("warning", "Error fetching iccids list");
        }
    });
}

$('#addIccids').click(function(){
    var customValidator = function(fields, params){
        var errorMessage = "";
        var start;
        var end;
        if(fields && params){
            for (var i = 0; i < fields.length; i++) {
                var field = fields[i];
                var item = field.children[1].children[0];
                var key = item.getAttribute('name');
                if((key == "start" || key == "end")){
                    if(parseInt(item.value) == NaN || parseInt(item.value).toString().length != 18){
                        errorMessage = genUserMessage('INVALID_ICCID_RANGE');
                        break;
                    }
                    if(key == "start"){
                        start = parseInt(item.value);
                    }else if(key == "end"){
                        end = parseInt(item.value);
                    }
                    
                }
            }
            if(errorMessage.length == 0 && start > end){
                errorMessage = genUserMessage('INVALID_ICCID_RANGE');
            }
            return (errorMessage.length == 0) ? true : errorMessage;
        }else{
            return true;
        }
    }
    generateConfirmationDialog('ADD_ICCIDS', {customValidator: customValidator}, undefined,
    function(err, data){
        if(err){
            showToast("warning", "Error saving iccids");
        }else{
            DEFAULT_RELOAD_CALLBACK();
        }
    });
    setTimeout(function(){
        $('[name="list"]').parent().css( "height", "100px");
    }, 220);
});