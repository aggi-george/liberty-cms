var selectedMonthId;
var selectedBillId;
var selectedStatusId;

var statusTableRelations;
var monthsTableRelations;
var billsTableRelations;
var selectedStatusRow;
var selectedMonthRow;
var selectedBillRow;
var progressInterval;

var monthMap = {};
var billsMap = {};
var statusMap = {};

function init() {
    $(".username")[0].innerHTML = getCookie("cmsuser");
    loadStatusLogs();
    loadMonths();
    checkSyncProgress();

    setTimeout(() => {
        loadBills();
    }, 1000);
}

function checkSyncProgress() {
    if (progressInterval) clearInterval(progressInterval);
    var loadProgress = () => {
        ajax("GET", "/api/1/web/invoices/sync/progress", {}, true, function (data) {
            var progressText = "";
            if (data.stepNumber >= 0) {
                progressText += "#" + data.stepNumber + " - ";
            }
            if (data.stepProgress >= 0) {
                progressText += data.stepProgress + "%";
            }
            if (progressText) {
                //$("#syncInvoices").hide();
                //$("#uploadPayment").hide();
                //$("#sendNonPaymentReminders").hide();
                $("#syncInvoicesProgress").show();
                $("#syncInvoicesProgress").html(getStatusLabel("NEUTRAL", "Syncing... " + progressText));
            } else {
                if ($("#syncInvoicesProgress").is(":visible")) {
                    reloadCallback();
                }
                $("#syncInvoices").show();
                $("#syncUnpaidInvoices").show();
                $("#batchPayment").show();
                $("#uploadPayment").show();
                $("#sendNonPaymentReminders").show();
                $("#syncInvoicesProgress").hide();
            }
        }, function (code, errorMessage) {
            $("#syncInvoices").hide();
            $("#syncUnpaidInvoices").hide();
            $("#batchPayment").hide();
            $("#uploadPayment").hide();
            $("#sendNonPaymentReminders").hide();
            $("#syncInvoicesProgress").show();
        }, function (type, Message) {
            if (type == "danger") {
                $("#syncInvoicesProgress").html(getStatusLabel("ERROR", "##Sync Error##"));
            }
        });
    };

    progressInterval = setInterval(loadProgress, 1000);
}

function showBatchPaymentDialog() {
    generateConfirmationDialog("MAKE_BATCH_PAYMENTS", {}, reloadCallback);
}

function showSyncBillsDialog() {
    generateConfirmationDialog("SYNC_BILLS", {}, reloadCallback);
}

function showSyncUnpaidDialog() {
    generateConfirmationDialog("SYNC_UNPAID_BILLS", {}, reloadCallback);
}

function showUpdatePaymentDialog() {
    generateConfirmationDialog("UPLOAD_PAYMENTS_CSV", {}, reloadCallback);
}

function showSendNonPaymentReminderDialog() {
    generateConfirmationDialog("SEND_BILL_PAYMENT_REMINDER", {}, reloadCallback);
}

function downloadBillsReport() {
    var type = $("#billType").val();
    var status = $("#billStatus").val();
    var dependencyType = $("#paymentDependencyType").val();

    $.ajax({
        "type": "GET",
        "dataType": "json",
        "url": "/api/1/web/invoices/get/invoices/" + (selectedMonthId ? selectedMonthId : "")
        + "?type=" + type + "&status=" + status + "&dependencyType=" + dependencyType,
        "success": function (response) {

            var dataString = "";
            var rows = 0;
            var part = 1;

            var monthItem = selectedMonthId ? monthMap[selectedMonthId] : undefined;
            var monthName = monthItem && monthItem.monthName ? monthItem.monthName : "all";

            var self = this;
            response.data.forEach(function (item) {
                if (!self.dataString) {
                    self.dataString = "ID MonthName BillingAccount InvoiceId Type TotalAmount LeftAmount " +
                    "Email Name Number SyncStatus ContentStatus PaymentStatus PaymentDate SendStatus SentDate\n";
                }

                rows++;
                self.dataString += (item.id ? item.id : "N/A") + " "
                + (item.monthName ? item.monthName : "N/A") + " "
                + (item.billingAccountNumber ? item.billingAccountNumber : "N/A") + " "
                + (item.invoiceId ? item.invoiceId : "N/A") + " "
                + (item.type ? item.type : "N/A") + " "
                + (item.totalAmount >= 0 ? item.totalAmount : "-1") + " "
                + (item.leftAmount >= 0 ? item.leftAmount : "-1") + " "
                + (item.syncedEmail ? item.syncedEmail.replace(/ /g, '_') : "N/A") + " "
                + (item.syncedName ? item.syncedName.replace(/ /g, '_') : "N/A") + " "
                + (item.syncedNumber ? item.syncedNumber.replace(/ /g, '_') : "N/A") + " "
                + (item.syncStatus ? item.syncStatus : "UNKNOWN") + " "
                + (item.contentStatus ? item.contentStatus : "UNKNOWN") + " "
                + (item.paymentStatus ? item.paymentStatus : "UNKNOWN") + " "
                + (item.paymentAt ? item.paymentAt.replace(/ /g, '_') : "UNKNOWN") + " "
                + (item.sendStatus ? item.sendStatus : "UNKNOWN") + " "
                + (item.sentAt ? item.sentAt.replace(/ /g, '_') : "UNKNOWN") + " ";
                self.dataString += "\n";

                if (rows >= 5000) {
                    downloadCSV(self.dataString, "invoices_" + monthName.replace(/ /g, "_")
                    + "_" + (new Date().toISOString()) + (part > 0 ? "_part_" + part : "") + "_rows_" + rows + ".csv", "text/csv");

                    part++;
                    rows = 0;
                    self.dataString = undefined;
                }
            });

            downloadCSV(self.dataString, "invoices_" + monthName.replace(/ /g, "_")
            + "_" + (new Date().toISOString()) + (part > 1 ? "_part_" + part : "") + "_rows_" + rows + ".csv", "text/csv");
        },
        "error": function (xhr, status, error) {
            showToast("danger", "Failed to generate a report");
        }
    });
}

function reloadCallback() {
    if ($("#monthsContainer").is(":visible")) {
        monthsTableRelations.fnDraw(false);
    }
    if ($("#billsContainer").is(":visible")) {
        billsTableRelations.fnDraw(false);
    }
    if ($("#statusContainer").is(":visible")) {
        statusTableRelations.fnDraw(false);
    }
}

function reloadLogs() {
    if ($("#statusContainer").is(":visible") && statusTableRelations) {
        statusTableRelations.fnDraw(false);
    }
}

function reloadMonths() {
    if ($("#monthsContainer").is(":visible") && monthsTableRelations) {
        monthsTableRelations.fnDraw(false);
    }
}

function reloadBills() {
    if ($("#billsContainer").is(":visible") && billsTableRelations) {
        billsTableRelations.fnDraw(false);
    }
}

function loadStatusLogs() {
    DEFAULT_RELOAD_CALLBACK = reloadCallback;
    selectedStatusId = undefined;

    $(".username")[0].innerHTML = getCookie("cmsuser");
    //$("#statusContainer").hide();

    var columns = [];
    columns.push({"sTitle": "Date", "sWidth": "18%"});
    columns.push({"sTitle": "Type", "sWidth": "20%"});
    columns.push({"sTitle": "Status"});
    columns.push({"sTitle": "Initiator", "sWidth": "22%"});
    columns.push({"sTitle": "Action", "sWidth": "8%"});

    statusTableRelations = $("#showStatuses").DataTable({
        "iDisplayLength": 10,
        "sAjaxSource": "/api/1/web/invoices/get/sync/logs",
        "bFilter": false,
        "bAutoWidth": false,
        "bProcessing": true,
        "bServerSide": true,
        "aoColumns": columns,
        "bLengthChange": false,
        "bSort": false,
        "bPaginate": false,
        "bInfo": false,
        "bDestroy": true,
        "aaSorting": [[0, 'desc']],
        "aoColumnDefs": [{"bSortable": false, "aTargets": [0, 1, 2]}],
        "fnServerData": function (sSource, aaData, fnCallback) {
            $.ajax({
                "type": "GET",
                "dataType": "json",
                "url": sSource,
                "data": aaData,
                "success": function (response) {
                    var tableViewData = {
                        "iTotalRecords": response.iTotalRecords,
                        "iTotalDisplayRecords": response.iTotalDisplayRecords,
                        "aaData": []
                    };

                    if (response.data) {
                        response.data.forEach(function (item) {
                            statusMap[item._id] = item;

                            var params = {
                                id: item._id,
                                monthDate: item.monthDate,
                                monthName: item.monthName
                            }

                            tableViewData.aaData.push([
                                getText(getDateTime(new Date(item.ts))),
                                getText(getStatusLabel("NEUTRAL", item.type)),
                                getText(getStatusLabel(item.status)),
                                getText(getInitiatorLabel(item.initiator)),
                                getTableActionButton("VIEW", "VIEW", undefined, params, "viewStatusLog")
                            ]);
                        });
                    }
                    fnCallback(tableViewData);
                },
                "error": function (xhr, status, error) {
                    //showToast("danger", "Failed to load statuses");
                }
            });
        },
        "fnInitComplete": function (oSettings, json) {
            $("#statusContainer").show();
        },
        "fnCreatedRow": function (nRow, aData, iDataIndex) {
        },
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            $(nRow).find('td').each(function (column, td) {
                if (column < 10) {
                    $(td).attr('data-id', aData[0]);
                }
            });

            // Row click
            $(nRow).on('click', function () {
            });

            // Cell click
            $('td', nRow).on('click', function () {
                selectedStatusRow = nRow;
                if ($(this).attr("data-id")) {
                    var id = $(this).attr("data-id");
                    if (id != selectedStatusId) {

                    } else {
                        // reloadCallback();
                    }
                }
            });
        }
    });
}


function loadMonths() {
    DEFAULT_RELOAD_CALLBACK = reloadCallback;
    selectedMonthId = undefined;

    $(".username")[0].innerHTML = getCookie("cmsuser");
    $("#monthsContainer").hide();
    $("#billsContainer").hide();
    $("#spinContainer").show();

    var columns = [];
    columns.push({"sTitle": "Id", "sWidth": "6%"});
    columns.push({"sTitle": "Date", "sWidth": "10%"});
    columns.push({"sTitle": "Name", "sWidth": "10%"});
    columns.push({"sTitle": "Total", "sWidth": "10%"});
    columns.push({"sTitle": "Sending/Sent", "sWidth": "10%"});
    columns.push({"sTitle": "Sent Errors", "sWidth": "10%"});
    columns.push({"sTitle": "Sync Errors", "sWidth": "10%"});
    columns.push({"sTitle": "Content Errors", "sWidth": "10%"});
    columns.push({"sTitle": "Payment Errors"});
    columns.push({"sTitle": "Action", "sWidth": "8%"});

    monthsTableRelations = $("#showMonths").DataTable({
        "iDisplayLength": 10,
        "sAjaxSource": "/api/1/web/invoices/get/months",
        "bFilter": true,
        "bAutoWidth": false,
        "bProcessing": true,
        "bServerSide": true,
        "aoColumns": columns,
        "bLengthChange": true,
        "bSort": false,
        "bDestroy": true,
        "aaSorting": [[0, 'desc']],
        "aoColumnDefs": [{"bSortable": false, "aTargets": [0, 1, 2]}],
        "fnServerData": function (sSource, aaData, fnCallback) {
            $.ajax({
                "type": "GET",
                "dataType": "json",
                "url": sSource,
                "data": aaData,
                "success": function (response) {
                    var tableViewData = {
                        "iTotalRecords": response.iTotalRecords,
                        "iTotalDisplayRecords": response.iTotalDisplayRecords,
                        "aaData": []
                    };

                    if (response.data) {
                        response.data.forEach(function (item) {
                            monthMap[item.id] = item;

                            var params = {
                                monthId: item.id,
                                monthDate: item.monthDate,
                                monthName: item.monthName
                            }

                            tableViewData.aaData.push([
                                getText(item.id),
                                getText(getDateText(item.monthDate)),
                                getText(item.monthName),
                                getText(item.invoicesCount + ""),
                                getText(item.sendingCount + "") + "/" + getText(item.sentCount + ""),
                                getText(item.sendErrorsCount + ""),
                                getText(item.syncErrorsCount + ""),
                                getText(item.contentErrorsCount + ""),
                                getText(item.paymentErrorsCount + ""),
                                getTableActionButton("SEND", "SENDING", "SEND_ALL_BILLS", params)
                            ]);
                        });
                    }
                    fnCallback(tableViewData);
                },
                "error": function (xhr, status, error) {
                    showToast("danger", "Loading error");
                }
            });
        },
        "fnInitComplete": function (oSettings, json) {
            $("#spinContainer").hide();
            $("#monthsContainer").show();
        },
        "fnCreatedRow": function (nRow, aData, iDataIndex) {
        },
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            $(nRow).find('td').each(function (column, td) {
                if (column < 9) {
                    $(td).attr('data-id', aData[0]);
                }
            });

            // Row click
            $(nRow).on('click', function () {
            });

            // Cell click
            $('td', nRow).on('click', function () {
                selectedMonthRow = nRow;
                if ($(this).attr("data-id")) {
                    var id = $(this).attr("data-id");
                    if (id != selectedMonthId) {
                        loadBills(id);
                    } else {
                        reloadCallback();
                    }
                }
            });
        }
    });
}

function loadBills(id, reload) {
    selectedMonthId = id;
    $(".username")[0].innerHTML = getCookie("cmsuser");
    //if (!reload) {
    //    $("#billsContainer").hide();
    //    $("#spinContainer").show();
    //}

    if (id) {
        var month = monthMap[id];
        $("#billsHeader").html(month && month.monthName ? getStatusLabel("NEUTRAL", month.monthName +
        " <i class='fa fa-times' onclick='removeMonthFilter(" + id + ")'></i>") : "");
    } else {
        $("#billsHeader").html(getStatusLabel("NEUTRAL", "ALL MONTHS"));
    }
    ajax
    $.ajax({
        "type": "GET",
        "dataType": "json",
        "url": "/api/1/web/invoices/get/report/" + (id ? id : ""),
        "data": {},
        "success": function (response) {
            if (response.data) {
                $("#reportContainer").show();

                var typeStatuses = [];
                var sendStatuses = [];
                var paymentFailureStatuses = [];
                var paymentFailuresCount = 0;

                Object.keys(response.data).forEach((status) => {
                    var count = response.data[status];
                    if (status.indexOf("TYPE_") == 0) {
                        typeStatuses.push({count: count, status: status.replace("TYPE_", "")});
                    } else if (status.indexOf("SEND_STATUS_") == 0) {
                        sendStatuses.push({count: count, status: status.replace("SEND_STATUS_", "")});
                    } else if (status.indexOf("PAYMENT_ERROR_") == 0) {
                        paymentFailureStatuses.push({count: count, status: status.replace("PAYMENT_ERROR_", "")});
                        paymentFailuresCount += count;
                    }
                });

                var countOfErrorsWithoutStatus = (response.data.TYPE_UNPAID - paymentFailuresCount);
                paymentFailureStatuses.push({count: countOfErrorsWithoutStatus , status: "UNKNOWN"});

                showPieCharts("invoice_type", "Invoice Type", typeStatuses);
                showPieCharts("send_status", "Send Status", sendStatuses);
                showPieCharts("payment_failure_status", "Payment Failures", paymentFailureStatuses);
            }
        },
        "error": function (xhr, status, error) {
            showToast("danger", "Failed to load report");
        }
    });

    billsTableRelations = $("#showBills").DataTable({
        "iDisplayLength": 10,
        "sAjaxSource": "/api/1/web/invoices/get/invoices/" + (id ? id : ""),
        "bFilter": true,
        "bAutoWidth": false,
        "bProcessing": true,
        "bServerSide": true,
        "bLengthChange": true,
        "aoColumns": [
            {"sTitle": "Id", "sWidth": "6%"},
            {"sTitle": "Billing No.", "sWidth": "10%"},
            {"sTitle": "Invoice Id", "sWidth": "14%"},
            {"sTitle": "Status", "sWidth": "18%"},
            {"sTitle": "Type", "sWidth": "10%"},
            {"sTitle": "Number", "sWidth": "10%"},
            {"sTitle": "Name", "sWidth": "10%"},
            {"sTitle": "Email"},
            {"sTitle": "Action", "sWidth": "8%"},
        ],
        "aoColumnDefs": [{"bSortable": false, "aTargets": [0, 1, 2, 3, 4, 5, 6, 7]}],
        "bDestroy": true,
        "aaSorting": [[0, 'desc']],
        "fnServerData": function (sSource, aaData, fnCallback) {

            var type = $("#billType").val();
            var status = $("#billStatus").val();
            var dependencyType = $("#paymentDependencyType").val();
            var paymentType = $("#paymentProcessedType").val();
            aaData.push({name: "type", value: type});
            aaData.push({name: "status", value: status});
            aaData.push({name: "dependencyType", value: dependencyType});
            aaData.push({name: "paymentType", value: paymentType});

            $.ajax({
                "type": "GET",
                "dataType": "json",
                "url": sSource,
                "data": aaData,
                "success": function (response) {
                    var tableViewData = {
                        "iTotalRecords": response.iTotalRecords,
                        "iTotalDisplayRecords": response.iTotalDisplayRecords,
                        "aaData": []
                    };

                    if (response.data) {
                        response.data.forEach(function (item) {
                            billsMap[item.id] = item;

                            var params = {
                                id: item.id,
                                invoiceId: item.invoiceId,
                                syncedName: item.syncedName,
                                billingAccountNumber: item.billingAccountNumber
                            }

                            var status = getStatusLabel(item.sendStatus, "NO:" +
                                (!item.sendStatus ? "NONE" : item.sendStatus)) +
                                " " + getStatusLabel(item.syncStatus, "SY:" +
                                (!item.syncStatus ? "NONE" : item.syncStatus)) +
                                " " + getStatusLabel(item.contentStatus, "CO:" +
                                (!item.contentStatus ? "NONE" : item.contentStatus)) +
                                " " + getStatusLabel(item.paymentStatus, "PA:" +
                                (!item.paymentStatus ? "NONE" : item.paymentStatus)) + " ";

                            tableViewData.aaData.push([
                                getText(item.id),
                                (item.billingAccountNumber ? getCustomerAccountLink(getText(item.billingAccountNumber)) : "N/A") +
                                (selectedMonthId ? "" : "<br>" + getStatusLabel("NEUTRAL", item.monthName)),
                                (item.filePath ? getPdfBillUrl(item.invoiceId, item.filePath) : getText(item.invoiceId))
                                + "<br>pwd: " + getText(item.syncedPwd),
                                status,
                                getText(getStatusLabel(item.type)) + (item.batchStatus && item.batchStatus != 'OK' ? getStatusLabel("UNFINISHED") : ""),
                                getText(item.syncedNumber),
                                getText(item.syncedName),
                                getText(item.syncedEmail),
                                getTableActionButton("SEND", "SENDING", "SEND_SINGLE_BILL", params) +
                                getTableActionButton("VIEW", "VIEW", undefined, params, "viewBillLog")
                            ]);
                        });
                    }
                    fnCallback(tableViewData);
                },
                "error": function (xhr, status, error) {
                    showToast("danger", "Loading error");
                }
            });
        },
        "fnInitComplete": function (oSettings, json) {
            $("#spinContainer").hide();
            $("#billsContainer").show();
        },
        "fnCreatedRow": function (nRow, aData, iDataIndex) {
        },
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            $(nRow).find('td').each(function (column, td) {
                if (column < 10) {
                    $(td).attr('data-id', aData[0]);
                }
            });

            // Row click
            $(nRow).on('click', function () {
            });

            // Cell click
            $('td', nRow).on('click', function () {
                selectedMonthRow = nRow;
                if ($(this).attr("data-id")) {
                    var id = $(this).attr("data-id");
                    if (id != selectedBillId) {
                        // do nothing
                    } else {
                        reloadCallback();
                    }
                }
            });
        }
    });
}

function getDateText(date, showTime) {
    return date && date !== "0000-00-00 00:00:00" ? (showTime ? getDateTime(date).replace(" ", "<br>")
        : getDateTime(date).split(" ")[0]) : "NONE";
}

function viewBillLog(dialogId, paramsText) {
    var params = parseButtonParams(paramsText);
    if (params && params.id && billsMap && billsMap[params.id]) {
        BootstrapDialog.show({
            size: BootstrapDialog.SIZE_WIDE,
            title: "Invoice Preview",
            message: JSON.stringify(billsMap[params.id], null, 4).replace(new RegExp(' ', 'g'), "&nbsp&nbsp"),
            buttons: []
        });
    } else {
        showToast("danger", "Invoice is not found");
    }
}

function viewStatusLog(dialogId, paramsText) {
    var params = parseButtonParams(paramsText);
    if (params && params.id && statusMap && statusMap[params.id]) {
        BootstrapDialog.show({
            size: BootstrapDialog.SIZE_WIDE,
            title: "Status Preview",
            message: JSON.stringify(statusMap[params.id], null, 4).replace(new RegExp(' ', 'g'), "&nbsp&nbsp"),
            buttons: []
        });
    } else {
        showToast("danger", "Status is not found");
    }
}

function removeMonthFilter(monthId) {
    loadBills(undefined, true);
}

function showPieCharts(id, name, data) {
    var chartData = [];
    var totalCount = 0
    data.forEach((item) => {
        totalCount += item.count > 0 ? item.count : 0;
    });
    data.forEach((item) => {
        chartData.push({
            name: item.status ? item.status.replace('PF_', '').replace(/_/g, ' ')
            + " (" + (item.count > 0 ? item.count : 0) + ")" : undefined,
            y: item.count > 0 ? (item.count / totalCount) * 100 : 0,
            color: item.status ? getColorForStatus(item.status) : undefined
        });
    });

    Highcharts.chart(id, {
        chart: {type: 'pie'},
        title: {
            text: name.toUpperCase(),
            style: {
                color: '#767676',
                fontWeight: 'bold',
                fontSize: '14px',
                fontFamily: 'Open Sans'
            }
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        legend: {
            itemStyle: {
                color: '#767676',
                fontWeight: 'bold',
                fontSize: '10px',
                fontFamily: 'Open Sans'
            }
        },
        exporting: {
            enabled: false
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        },
        series: [{
            name: 'Brands',
            colorByPoint: true,
            data: chartData
        }]
    });

}