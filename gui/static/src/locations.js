var selectedProviderId;
var selectedPointId;
var providersTableRelations;
var pointsTableRelations;
var promoTableRelations;
var logsTableRelations;
var providersMap = {};
var pointsMap = {};
var redemptionsMap = {};
var logsMap = {};

function showLocationProviderEditDialog() {
    generateConfirmationDialog("EDIT_GEOLOCATION_PROVIDER", {}, reloadCallback);
}

function showLocationPointEditDialog() {
    generateConfirmationDialog("EDIT_GEOLOCATION_POINT", {providerId: selectedProviderId}, reloadCallback);
}

function init() {
    loadCustomersForProviders();
    loadCustomersForGeoLocations();
    loadRedeemedCustomersForProviders();
    loadRedeemedCustomersForGeoLocations();
    loadProviders();
    loadPoints();
    loadLogs();
    loadPromos();
}

function reloadCallback() {
    if ($("#providersContainer").is(":visible")) {
        providersTableRelations.fnDraw(false);
    }
    if ($("#pointsContainer").is(":visible")) {
        pointsTableRelations.fnDraw(false);
    }
    if ($("#logsContainer").is(":visible")) {
        logsTableRelations.fnDraw(false);
    }
    if ($("#redemptionsContainer").is(":visible")) {
        promoTableRelations.fnDraw(false);
    }
}

function loadCustomersForProviders(){
    $.ajax({
        "type": "GET",
        "dataType": "json",
        "url": "/api/1/web/locations/customersForProviders",
        "data": {},
        "success": function (response) {
            if(response.code == 0){
                var total = 0;
                response.list.forEach(function(item){
                    total += item.total;
                });
                response.list.push({"name": "TOTAL", "total": total});
                generateTable("totalCustomersForProvidersTable", response.list, [
                    {"sTitle": "Provider", "sClass": "twocol-table-column", "mData": "name", "mRender": function(data, type, full){
                            return data;
                        }
                    },
                    {"sTitle": "Total Notifications", "sClass": "twocol-table-column", "mData": "total", "mRender": function(data, type, full){
                            return data;
                        }
                    }
                ], 10, true);
                
            }else{
                showToast("warning", "Error fetching customersForProviders list");
            }
        },
        "error": function (result) {
            showToast("warning", "Error fetching customersForProviders list");
        }
    });
}

function loadCustomersForGeoLocations(){
    $.ajax({
        "type": "GET",
        "dataType": "json",
        "url": "/api/1/web/locations/customersForGeoLocations",
        "data": {},
        "success": function (response) {
            if(response.code == 0){
                var total = 0;
                response.list.forEach(function(item){
                    total += item.total;
                });
                response.list.push({"name": "TOTAL", "label": "", "total": total});
                generateTable("totalCustomersForGeoLocationsTable", response.list, [
                    {"sTitle": "Provider Name", "sClass": "twocol-table-column", "mData": "name", "mRender": function(data, type, full){
                            return data;
                        }
                    },
                    {"sTitle": "Location Name", "sClass": "twocol-table-column", "mData": "label", "mRender": function(data, type, full){
                            return data;
                        }
                    },
                    {"sTitle": "Total Notifications", "sClass": "twocol-table-column", "mData": "total", "mRender": function(data, type, full){
                            return data;
                        }
                    }
                ], 10, true);
                
            }else{
                showToast("warning", "Error fetching customersForGeoLocations list");
            }
        },
        "error": function (result) {
            showToast("warning", "Error fetching customersForGeoLocations list");
        }
    });
}

function loadRedeemedCustomersForProviders(){
    $.ajax({
        "type": "GET",
        "dataType": "json",
        "url": "/api/1/web/locations/redeemedCustomersForProviders",
        "data": {},
        "success": function (response) {
            if(response.code == 0){
                var total = 0;
                response.list.forEach(function(item){
                    total += item.total;
                });
                response.list.push({"name": "TOTAL", "total": total});
                generateTable("totalRedeemedCustomersForProvidersTable", response.list, [
                    {"sTitle": "Provider", "sClass": "twocol-table-column", "mData": "name", "mRender": function(data, type, full){
                            return data;
                        }
                    },
                    {"sTitle": "Total Notifications", "sClass": "twocol-table-column", "mData": "total", "mRender": function(data, type, full){
                            return data;
                        }
                    }
                ], 10, true);
                
            }else{
                showToast("warning", "Error fetching customersForProviders list");
            }
        },
        "error": function (result) {
            showToast("warning", "Error fetching customersForProviders list");
        }
    });
}

function loadRedeemedCustomersForGeoLocations(){
    $.ajax({
        "type": "GET",
        "dataType": "json",
        "url": "/api/1/web/locations/redeemedCustomersForGeoLocations",
        "data": {},
        "success": function (response) {
            if(response.code == 0){
                var total = 0;
                response.list.forEach(function(item){
                    total += item.total;
                });
                response.list.push({"name": "TOTAL", "label": "", "total": total});
                generateTable("totalRedeemedCustomersForGeoLocationsTable", response.list, [
                    {"sTitle": "Provider Name", "sClass": "twocol-table-column", "mData": "name", "mRender": function(data, type, full){
                            return data;
                        }
                    },
                    {"sTitle": "Location Name", "sClass": "twocol-table-column", "mData": "label", "mRender": function(data, type, full){
                            return data;
                        }
                    },
                    {"sTitle": "Total Notifications", "sClass": "twocol-table-column", "mData": "total", "mRender": function(data, type, full){
                            return data;
                        }
                    }
                ], 10, true);
                
            }else{
                showToast("warning", "Error fetching customersForGeoLocations list");
            }
        },
        "error": function (result) {
            showToast("warning", "Error fetching customersForGeoLocations list");
        }
    });
}

function loadProviders() {
    DEFAULT_RELOAD_CALLBACK = reloadCallback;
    selectedProviderId = undefined;

    $(".username")[0].innerHTML = getCookie("cmsuser");
    $("#providersContainer").hide();
    $("#pointsContainer").hide();
    $("#logsContainer").hide();
    $("#createLocation").hide();
    $("#spinContainer").show();

    var columns = [];
    columns.push({"sTitle": "Id", "sWidth": "8%"});
    columns.push({"sTitle": "Name", "sWidth": "20%"});
    columns.push({"sTitle": "Notification", "sWidth": "14%"});
    columns.push({"sTitle": "Redeem Dialog"});
    columns.push({"sTitle": "Enabled", "sWidth": "12%"});
    columns.push({"sTitle": "Actions", "sWidth": "8%"});

    providersTableRelations = $("#showProviders").DataTable({
        "iDisplayLength": 10,
        "sAjaxSource": "/api/1/web/locations/providers",
        "bFilter": true,
        "bAutoWidth": false,
        "bProcessing": true,
        "bServerSide": true,
        "aoColumns": columns,
        "bLengthChange": true,
        "bSort": false,
        "bDestroy": true,
        "aaSorting": [[0, 'desc']],
        "aoColumnDefs": [{"bSortable": false, "aTargets": [0, 1, 2, 3, 4]}],
        "fnServerData": function (sSource, aaData, fnCallback) {
            $.ajax({
                "type": "GET",
                "dataType": "json",
                "url": sSource,
                "data": aaData,
                "success": function (response) {
                    var tableViewData = {
                        "iTotalRecords": response.iTotalRecords,
                        "iTotalDisplayRecords": response.iTotalDisplayRecords,
                        "aaData": []
                    };

                    if (response.data) {
                        response.data.forEach(function (item) {
                            providersMap[item.id] = item;

                            var name = item.name && item.name.length > 30
                                ? item.name.substring(0, 27) + "..." : item.name;
                            var redemptionTitle = item.redemptionTitle && item.redemptionTitle.length > 20
                                ? item.redemptionTitle.substring(0, 17) + "..." : item.redemptionTitle;
                            var redemptionMessage = item.redemptionMessage && item.redemptionMessage.length > 20
                                ? item.redemptionMessage.substring(0, 17) + "..." : item.redemptionMessage;

                            var fileLink = item.redemptionImageId ? "<a href='/api/1/web/file/resource/" +
                            item.redemptionImageId + "' target='_blank'>Link</a>" : undefined;
                            var redeemDialog = getStatusLabel(item.generateRedemptionCode == 1 ? "ON" : "OFF")
                                + (fileLink ? getStatusLabel("NEUTRAL", "IMAGE: " + fileLink) : "")
                                + (redemptionTitle ? "<br>" + getStatusLabel("NEUTRAL", "TITLE: " + redemptionTitle) : "")
                                + (redemptionMessage ? getStatusLabel("NEUTRAL", "MESSAGE: " + redemptionMessage) : "");

                            tableViewData.aaData.push([
                                getText(item.id),
                                getText(name),
                                getText(item.activity),
                                redeemDialog,
                                getStatusLabel(item.enabled == 1 ? "YES" : "NO"),
                                getTableActionButton("Update", "EDIT", "EDIT_GEOLOCATION_PROVIDER", item) +
                                getTableActionButton("Remove", "REMOVE", "REMOVE_GEOLOCATION_PROVIDER", item)
                            ]);
                        });
                    }
                    fnCallback(tableViewData);
                },
                "error": function (xhr, status, error) {
                    showToast("danger", "Loading error");
                }
            });
        },
        "fnInitComplete": function (oSettings, json) {
            $("#spinContainer").hide();
            $("#providersContainer").show();
        },
        "fnCreatedRow": function (nRow, aData, iDataIndex) {
        },
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            $(nRow).find('td').each(function (column, td) {
                if (column < 4) {
                    $(td).attr('data-id', aData[0]);
                }
            });

            // Row click
            $(nRow).on('click', function () {
            });

            // Cell click
            $('td', nRow).on('click', function () {
                if ($(this).attr("data-id")) {
                    var id = $(this).attr("data-id");
                    if (id != selectedProviderId) {
                        loadPoints(id);
                        loadLogs(id);
                        loadPromos(id);
                    } else {
                        if ($("#pointsContainer").is(":visible")) {
                            pointsTableRelations.fnDraw(false);
                            logsTableRelations.fnDraw(false);
                        }
                    }
                }
            });
        }
    });
}

function loadPoints(id) {
    if (!id) {
        id = "ALL";
    }

    selectedProviderId = id;
    if (id && id != "ALL") {
        $("#createLocation").show();
    }
    $(".username")[0].innerHTML = getCookie("cmsuser");

    var category = providersMap[id];
    $("#pointsHeader").html(category && category.name ? getStatusLabel("NEUTRAL", category.name) : "");

    pointsTableRelations = $("#showPoints").DataTable({
        "iDisplayLength": 10,
        "sAjaxSource": "/api/1/web/locations/points/" + id,
        "bFilter": true,
        "bAutoWidth": false,
        "bProcessing": true,
        "bServerSide": true,
        "bLengthChange": true,
        "aoColumns": [
            {"sTitle": "Id", "sWidth": "8%"},
            {"sTitle": "Label", "sWidth": "16%"},
            {"sTitle": "Provider ID", "sWidth": "10%"},
            {"sTitle": "Latitude", "sWidth": "10%"},
            {"sTitle": "Longitude", "sWidth": "10%"},
            {"sTitle": "Radius", "sWidth": "10%"},
            {"sTitle": "Enabled", "sWidth": "10%"},
            {"sTitle": "Notification"},
            {"sTitle": "Action", "sWidth": "10%"},
        ],
        "aoColumnDefs": [{"bSortable": false, "aTargets": [0, 1, 2, 3, 4, 5, 6, 7]}],
        "bDestroy": true,
        "aaSorting": [[0, 'desc']],
        "fnServerData": function (sSource, aaData, fnCallback) {
            aaData.push({name: "providerId", value: selectedProviderId ? selectedProviderId : undefined});

            $.ajax({
                "type": "GET",
                "dataType": "json",
                "url": sSource,
                "data": aaData,
                "success": function (response) {
                    var tableViewData = {
                        "iTotalRecords": response.iTotalRecords,
                        "iTotalDisplayRecords": response.iTotalDisplayRecords,
                        "aaData": []
                    };

                    if (response.data) {
                        response.data.forEach(function (item) {
                            pointsMap[item.id] = item;

                            var codeLabel = ""
                            if (item.redemptionCode) {
                                if (item.generateRedemptionCode) {
                                    codeLabel = getStatusLabel("ON", "ON: " + item.redemptionCode)
                                } else {
                                    codeLabel = getStatusLabel("OFF", "OFF: " + item.redemptionCode)
                                }
                            }

                            tableViewData.aaData.push([
                                getText(item.id),
                                getText(item.label) + (item.redemptionCode ? "<br>" + codeLabel: ""),
                                getText(item.providerId),
                                getText(item.latitude),
                                getText(item.longitude),
                                getText(item.radius),
                                getStatusLabel(item.enabled == 1 ? "YES" : "NO"),
                                getText(item.activity),
                                getTableActionButton("Update", "EDIT", "EDIT_GEOLOCATION_POINT", item) +
                                getTableActionButton("Remove", "REMOVE", "REMOVE_GEOLOCATION_POINT", item)
                            ]);
                        });
                    }
                    fnCallback(tableViewData);
                },
                "error": function (xhr, status, error) {
                    showToast("danger", "Loading error");
                }
            });
        },
        "fnInitComplete": function (oSettings, json) {
            $("#spinContainer").hide();
            $("#pointsContainer").show();
        },
        "fnCreatedRow": function (nRow, aData, iDataIndex) {
        },
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            $(nRow).find('td').each(function (column, td) {
                if (column < 7) {
                    $(td).attr('data-id', aData[0]);
                }
            });

            // Row click
            $(nRow).on('click', function () {
            });

            // Cell click
            $('td', nRow).on('click', function () {
                if ($(this).attr("data-id")) {
                    var id = $(this).attr("data-id");
                    if (id != selectedPointId) {
                        loadLogs(selectedProviderId, id);
                    } else {
                        if ($("#pointsContainer").is(":visible")) {
                            logsTableRelations.fnDraw(false);
                        }
                    }
                }
            });
        }
    });
}

function loadPromos(providerId) {
    if (!providerId) {
        providerId = "ALL";
    }

    selectedProviderId = providerId;
    $(".username")[0].innerHTML = getCookie("cmsuser");

    var provider = providersMap[providerId];
    $("#redemptionsProviderHeader").html(provider && provider.name ? getStatusLabel("NEUTRAL", provider.name) : "");

    promoTableRelations = $("#showRedemptions").DataTable({
        "iDisplayLength": 10,
        "sAjaxSource": "/api/1/web/locations/promo/" + providerId,
        "bFilter": true,
        "bAutoWidth": false,
        "bProcessing": true,
        "bServerSide": true,
        "bLengthChange": true,
        "aoColumns": [
            {"sTitle": "Id", "sWidth": "8%"},
            {"sTitle": "Provider ID", "sWidth": "8%"},
            {"sTitle": "Create Date", "sWidth": "12%"},
            {"sTitle": "Sent Date", "sWidth": "12%"},
            {"sTitle": "Redeem Date", "sWidth": "16%"},
            {"sTitle": "SI No.", "sWidth": "12%"},
            {"sTitle": "Used Code"},
            {"sTitle": "Action", "sWidth": "10%"},
        ],
        "aoColumnDefs": [{"bSortable": false, "aTargets": [0, 1, 2, 3, 4, 5, 6]}],
        "bDestroy": true,
        "aaSorting": [[0, 'desc']],
        "fnServerData": function (sSource, aaData, fnCallback) {
            aaData.push({name: "providerId", value: selectedProviderId ? selectedProviderId : undefined});

            $.ajax({
                "type": "GET",
                "dataType": "json",
                "url": sSource,
                "data": aaData,
                "success": function (response) {
                    var tableViewData = {
                        "iTotalRecords": response.iTotalRecords,
                        "iTotalDisplayRecords": response.iTotalDisplayRecords,
                        "aaData": []
                    };

                    if (response.data) {
                        response.data.forEach(function (item) {
                            redemptionsMap[item.id] = item;

                            tableViewData.aaData.push([
                                getText(item.id),
                                getText(item.providerId),
                                getDateText(item.createdAt, true),
                                getDateText(item.sentAt, true),
                                getDateText(item.redeemAt, true),
                                getCustomerAccountLink(item.serviceInstanceNumber),
                                item.redeemCode ? getText(item.redeemCode) : "NONE",
                                (item.redeemAt == "0000-00-00 00:00:00" ? getTableActionButton("Redeem", "EDIT", "REDEEM_GEOLOCATION_PROMO", item) : "") +
                                getTableActionButton("Remove", "REMOVE", "REMOVE_GEOLOCATION_PROMO", item)
                            ]);
                        });
                    }
                    fnCallback(tableViewData);
                },
                "error": function (xhr, status, error) {
                    showToast("danger", "Loading error");
                }
            });
        },
        "fnInitComplete": function (oSettings, json) {
            $("#spinContainer").hide();
            $("#redemptionsContainer").show();
        },
        "fnCreatedRow": function (nRow, aData, iDataIndex) {
        },
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            $(nRow).find('td').each(function (column, td) {
                if (column < 7) {
                    $(td).attr('data-id', aData[0]);
                }
            });

            // Row click
            $(nRow).on('click', function () {
            });

            // Cell click
            $('td', nRow).on('click', function () {
                if ($(this).attr("data-id")) {
                    var id = $(this).attr("data-id");
                }
            });
        }
    });
}

function getDateText(date, showTime) {
    return date && date !== "0000-00-00 00:00:00" ? (showTime ? getDateTime(date).replace(" ", "<br>")
        : getDateTime(date).split(" ")[0]) : "NONE";
}

function loadLogs(providerId, pointId) {
    if (!providerId) {
        providerId = "ALL";
    }
    if (!pointId) {
        pointId = "ALL";
    }

    selectedProviderId = providerId;
    selectedPointId = pointId;
    $(".username")[0].innerHTML = getCookie("cmsuser");

    var provider = providersMap[providerId];
    var point = pointsMap[pointId];
    $("#logsProviderHeader").html(provider && provider.name ? getStatusLabel("NEUTRAL", provider.name) : "");
    $("#logsPointsHeader").html(point && point.label ? getStatusLabel("NEUTRAL", point.label) : "");

    logsTableRelations = $("#showLogs").DataTable({
        "iDisplayLength": 10,
        "sAjaxSource": "/api/1/web/locations/logs/" + providerId + "/" + pointId,
        "bFilter": true,
        "bAutoWidth": false,
        "bProcessing": true,
        "bServerSide": true,
        "bLengthChange": true,
        "aoColumns": [
            {"sTitle": "Date", "sWidth": "10%"},
            {"sTitle": "Provider ID", "sWidth": "10%"},
            {"sTitle": "Point ID", "sWidth": "10%"},
            {"sTitle": "Latitude", "sWidth": "12%"},
            {"sTitle": "Longitude", "sWidth": "12%"},
            {"sTitle": "Service No.", "sWidth": "12%"},
            {"sTitle": "Number"},
            {"sTitle": "Action", "sWidth": "10%"},
        ],
        "aoColumnDefs": [{"bSortable": false, "aTargets": [0, 1, 2, 3, 4, 5, 6]}],
        "bDestroy": true,
        "aaSorting": [[0, 'desc']],
        "fnServerData": function (sSource, aaData, fnCallback) {
            aaData.push({name: "pointId", value: selectedPointId ? selectedPointId : undefined});
            aaData.push({name: "providerId", value: selectedProviderId ? selectedProviderId : undefined});

            $.ajax({
                "type": "GET",
                "dataType": "json",
                "url": sSource,
                "data": aaData,
                "success": function (response) {
                    var tableViewData = {
                        "iTotalRecords": response.iTotalRecords,
                        "iTotalDisplayRecords": response.iTotalDisplayRecords,
                        "aaData": []
                    };

                    if (response.data) {
                        response.data.forEach(function (item) {
                            logsMap[item._id] = item;

                            var messageText = (item.activity ? getStatusLabel("NEUTRAL", item.activity) : "");
                            tableViewData.aaData.push([
                                getText(getDateTime(new Date(item.ts)).replace(" ", "<br>")),
                                getText(item.providerId),
                                getText(item.pointId),
                                getText(item.latitude),
                                getText(item.longitude),
                                item.serviceInstanceNumber ? getCustomerAccountLink(item.serviceInstanceNumber) : "NONE",
                                item.number ? getCustomerNumberLink(item.number) : "NONE",
                                getTableActionButton("Details", "VIEW", undefined, {id: item._id}, "viewAllDetails")
                            ]);
                        });
                    }
                    fnCallback(tableViewData);
                },
                "error": function (xhr, status, error) {
                    showToast("danger", "Loading error");
                }
            });
        },
        "fnInitComplete": function (oSettings, json) {
            $("#spinContainer").hide();
            $("#logsContainer").show();
        },
        "fnCreatedRow": function (nRow, aData, iDataIndex) {
        },
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            $(nRow).find('td').each(function (column, td) {
                if (column < 10) {
                    $(td).attr('data-id', aData[0]);
                }
            });

            // Row click
            $(nRow).on('click', function () {
            });

            // Cell click
            $('td', nRow).on('click', function () {
                if ($(this).attr("data-id")) {
                    var id = $(this).attr("data-id");
                }
            });
        }
    });
}

function viewAllDetails(dialogId, paramsText) {
    var params = parseButtonParams(paramsText);
    if (params && params.id && logsMap && logsMap[params.id]) {
        BootstrapDialog.show({
            size: BootstrapDialog.SIZE_WIDE,
            title: "Log Details",
            message: JSON.stringify(logsMap[params.id], null, 4).replace(new RegExp(' ', 'g'), "&nbsp&nbsp"),
            buttons: []
        });
    } else {
        showToast("danger", "Not found");
    }
}