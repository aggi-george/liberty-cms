function init_noc() {
    $(".username")[0].innerHTML = getCookie("cmsuser");
    getSync();
    getStressProgress(true);
    bssStatus();
    gantryStatus();
    serviceStatus();
    bypassStats();
    checkBonusAnomalyProgress();
}

function getSync() {
    ajax("GET", "/api/1/web/noc/get/env", undefined, true, function (data) {
        if (data && (data.code == 0)) {
            var env = data.staging ? "staging" : "production";
            $("#" + env + "_sync tbody").append("<TR>" +
                "<TD>Elitecore DR</TD>" +
                "<TD><BUTTON class='btn btn-primary btn-xs ctrlsync' data-id=dr data-type=" + env + ">Sync</button></td>" +
            "</TR>");
            $(".ctrlsync").click(function() {
                sync(this);
            });
        }
    });
}

function sync(btn) {
    var id = $(btn).attr("data-id");
    var type = $(btn).attr("data-type");
    $(btn).prop("disabled", true);
    var parameters = { "type" : type };
    ajax("PUT", "/api/1/web/noc/put/sync/" + id, parameters, true, function (data) {
        $(btn).prop("disabled", false);
    });
}

function auditLogsSearch(form) {
    var month = form.month.value;
    var year = form.year.value;
    var day = form.day.value;
    var hour_start = form.hour_start.value;
    var hour_end = form.hour_end.value;
    var minute_start = form.minute_start.value;
    var minute_end = form.minute_end.value;
    $("#showSearch").DataTable({
        "iDisplayLength": 10,
        "sAjaxSource": "/api/1/web/noc/get/auditlogs?month=" + month + "&year=" + year + "&day=" + day + "&hour_start=" + hour_start + "&minute_start=" + minute_start + "&hour_end=" + hour_end + "&minute_end=" + minute_end,
        "bProcessing": true,
        "bServerSide": true,
        "bFilter": false,
        "bLengthChange": false,
        "bDestroy": true,
        "bAutoWidth": false,
        "aoColumns": [
            {"sTitle": "Timestamp", "sWidth": "10%", "bSortable": true},
            {"sTitle": "User", "sWidth": "7%", "bSortable": false},
            {"sTitle": "PTY Owner", "sWidth": "8%", "bSortable": false},
            {"sTitle": "Working Directory", "sWidth": "15%", "bSortable": false},
            {"sTitle": "Hostname", "sWidth": "10%", "bSortable": false},
            {"sTitle": "Connection Details", "sWidth": "15%", "bSortable": false},
            {"sTitle": "Command", "sWidth": "35%", "bSortable": false},
        ],
        "bLengthChange": false,
        "fnServerData": function (sSource, aaData, fnCallback) {
            $.ajax({
                "type": "GET",
                "dataType": "json",
                "url": sSource,
                "data": aaData,
                "success": function (response) {
                    console.log(response);
                    var tableViewData = {
                        "iTotalRecords": response.total,
                        "iTotalDisplayRecords": response.total,
                        "aaData": []
                    };
                    form.submit.disabled = false;
                    response.list.forEach(function (item) {
                        tableViewData.aaData.push([
                            item.date,
                            item.user,
                            item.ptyOwner,
                            item.workingDir,
                            item.hostname,
                            item.connDetail,
                            item.command
                        ]);
                    });
                    fnCallback(tableViewData);
                },
                "error": function (xhr, status, error) {
                    showToast("danger", "Loading error");
                }
            });
        },
        "fnInitComplete": function (oSettings, json) {
        },
        "fnCreatedRow": function (nRow, aData, iDataIndex) {
        },
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            $(nRow).on('click', function () {
            });
            $('td', nRow).on('click', function () {
            });
        }
    });
    return false;
}

function syncBills(btn) {
    event.preventDefault();
    var server = $(btn).attr("data-id");
    $(btn).prop("disabled", true);
    $("#syncSpin_" + server).show();
    if (confirm("Are you sure you want to sync bills from " + server.toUpperCase() + " ?")) {
        var parameters = { "server" : server };
        ajax("PUT", "/api/1/web/noc/put/syncBills/" + server, parameters, true, function (data) {
            if (data && (data.code == 0)) {
                showToast("success", "syncBills of " + server.toUpperCase() + " - " + data.result.total + " files");
                if (data.result.total == 0) {
                    $(btn).prop("disabled", false);
                    $("#syncSpin_" + server).hide();
                    return;
                } else {
                    syncBillsCheck(server, function() {
                        $(btn).prop("disabled", false);
                        $("#syncSpin_" + server).hide();
                    });
                }
            } else {
                showToast("warning", data.status);
                $(btn).prop("disabled", false);
                $("#syncSpin_" + server).hide();
            }
        });
    }
}

function syncBillsCheck(server, callback) {
    var checker = setInterval(function () {
        var parameters = { "server" : server, "check" : 1 };
        ajax("PUT", "/api/1/web/noc/put/syncBills/" + server, parameters, true, function (data) {
            if (data && data.result && (data.code == 0)) {
                if (data.result.remaining === 0) {
                    if (!init) showToast("success", "Done! " + data.result.total + " synced");
                    clearInterval(checker);
                }
                var percent = Math.round((data.result.total - data.result.remaining) / data.result.total * 100);
                $("#syncBillsProgress").css("width", percent + "%");
                if (percent >= 100) {
                    showToast("success", "Done!");
                    clearInterval(checker);
                    callback();
                }
            } else {
                showToast("warning", "Oops! Check failed!");
                clearInterval(checker);
                callback();
            }
        });
    }, 1000);
}

function bindControlButton() {
    $(".control").unbind("click");
    $(".control").click(function () {
        var type = $(this).attr("data-type");
        var server = $(this).attr("data-server");
        var action = $(this).attr("data-action");
        var args = $(this).attr("data-args");
        if (!server) return;
        if (confirm("Are you sure you want to " + action.toUpperCase() + " " + server.toUpperCase() + " ?")) {
            var parameters = { "server" : server, "action" : action, "args" : args };            
            if ( type == "gantry" ) {
                if (action == "block") parameters.args = $("#gantryBlockType").val();
                else if (action == "throttle") parameters.args = $("#gantryThrottleType").val();
                parameters.throttle = $("#gantryThrottle").val();
            }
            ajax("PUT", "/api/1/web/noc/put/" + type + "/" + server, parameters, true, function (data) {
                if (data && (data.code == 0)) {
                    if (type == "bss") bssStatus();
                    if (type == "gantry") gantryStatus();
                    else serviceStatus();
                    showToast("success", action.toUpperCase() + " of " + server.toUpperCase() + " initiated!");
                } else {
                    showToast("warning", data.status);
                }
            });
        }
    });
}

function bssStatus() {
    $("#bss_control tbody").empty(); 
    ajax("GET", "/api/1/web/noc/get/bssStatus", undefined, true, function (data) {
        if (data && (data.code == 0)) {
            if (data.write < 2) return;
            data.list.forEach(function(bss) {
                if (bss && typeof(bss.status) == "object") {
                    var server = bss.bss;
                    var health = bss.status.allow.length && bss.status.block.length ? false : true;
                    var state = !bss.status.allow.length ? "block" : "allow";
                    var htmlActions = "";
                    [ "allow", "block" ].forEach(function(action) {
                        var btn = (state == action) ? "disabled" : "";
                        htmlActions += "<BUTTON class='btn btn-primary btn-xs control " + btn + "' " +
                            " data-type=bss" +
                            " data-server=" + server + 
                            " data-action=" + action +
                            " data-args=bss" +
                            " > " + action + "</BUTTON>&nbsp;";
                    });
                    $("#bss_control tbody").append("<TR>" +
                        "<TD>" + (health ?
                            "<BUTTON class='btn btn-success btn-xs disabled'>OK - " + state + "</BUTTON>" :
                            "<BUTTON class='btn btn-warning btn-xs disabled'>WARN - " + state + "</BUTTON>") + "</TD>" +
                        "<TD><strong>" + server.toUpperCase() + "</strong></TD><TD>" + htmlActions + "</TD>" +
                        "<TD><BUTTON class='btn btn-primary btn-xs' id=sync_" + server + " data-id='" + server + "' onClick='syncBills(this)'>" +
                            "Bills Sync - " + bss.status.bills.count +
                        "</BUTTON>&nbsp;" + bss.status.bills.latest + "&nbsp;<I id=syncSpin_" + server + " class='fa fa-spinner fa-spin' style='display:none'></I></TD></TR>");
                    if (bss.syncBills) {
                        $("#sync_" + server).prop("disabled", true);
                        $("#syncSpin_" + server).show();
                        syncBillsCheck(server, function() {
                            $("#sync_" + server).prop("disabled", false);
                            $("#syncSpin_" + server).hide();
                        });
                    }
                }
            });
            bindControlButton();
        } else {
            showToast("warning", "BSS status fail");
        }
    });
}

function gantryStatus() {
    $("#gantry_control tbody").empty(); 
    ajax("GET", "/api/1/web/noc/get/gantryStatus", undefined, true, function (data) {
        if (data && (data.code == 0)) {
            if (data.write < 1) return;
            var throttleTypes = "<OPTION></OPTION>";
            var blockTypes = "<OPTION></OPTION>";
            if (data.gantryTypes) data.gantryTypes.forEach(function(type) {
                if ( type.httpStatus == 503 ) blockTypes += "<OPTION>" + type.id + "</OPTION>";
                else throttleTypes += "<OPTION>" + type.id + "</OPTION>";
            });
            var th = [ 0.9, 0.8, 0.7, 0.6, 0.5, 0.4, 0.3, 0.2, 0.1, 0.0 ];
            var throttle = "<OPTION></OPTION>";
            th.forEach(function(item) {
                throttle += "<OPTION value=" + item + ">allow " + (item * 100) + "%" + "</OPTION>";
            });
            var htmlGantry = function(type, state) {
                var gantry = [ type, "allow" ];
                var html = "";
                gantry.forEach(function(action) {
                    var btn = "disabled";
                    if ( state && state.throttle && (action != "throttle") ) { 
                        btn = "";
                    } else if ( state && !state.throttle && (action != "block") ) {
                        btn = "";
                    } else if (!state && type==action) btn = "";
                    html += "<BUTTON class='btn btn-primary btn-xs control " + btn + "' " +
                        " data-type=gantry" +
                        " data-server=mobile" +
                        " data-action=" + action +
                        " data-args=''" +
                        " > " + action + "</BUTTON>&nbsp;";
                });
                return html;
            }
            $("#gantry_control tbody").append("<TR>" +
                "<TD><strong>GANTRY</strong></TD><TD>" + htmlGantry("throttle", data.gantryStatus) + "</TD>" +
                "<TD><SELECT id=gantryThrottle class='form-control input-sm'>" + throttle + "</SELECT></TD>" +
                "<TD><SELECT id=gantryThrottleType class='form-control input-sm'>" + throttleTypes + "</SELECT></TD></TR>");
            $("#gantry_control tbody").append("<TR>" +
                "<TD></TD><TD>" + htmlGantry("block", data.gantryStatus) + "</TD>" +
                "<TD></TD>" +
                "<TD><SELECT id=gantryBlockType class='form-control input-sm'>" + blockTypes + "</SELECT></TD></TR>");
            if (data.gantryStatus) {
                if (data.gantryStatus.throttle) {
                    $("#gantryThrottle").val(data.gantryStatus.throttle);
                    $("#gantryThrottleType").val(data.gantryStatus.id);
                } else $("#gantryBlockType").val(data.gantryStatus.id);
            }
            bindControlButton();
        } else {
            showToast("warning", "Service status fail");
        }
    });
}

function serviceStatus() {
    $("#production_control tbody").empty(); 
    $("#staging_control tbody").empty(); 
    ajax("GET", "/api/1/web/noc/get/serviceStatus", undefined, true, function (data) {
        if (data && (data.code == 0)) {
            if (data.write < 2) return;
            var findServerProcs = function(id, type, list) {
               return list.filter(function(o) { if (o[type] == id) return o; else return undefined; })[0];
            }; 
            var htmlActions = function(server, args, state) {
                var actions = [ "reload", "restart", "start", "stop" ];
                var html = "";
                actions.forEach(function(action) {
                    var btn = (state == action) ? "disabled" : "";
                    html += "<BUTTON class='btn btn-primary btn-xs control " + btn + "' " +
                        " data-type=control" +
                        " data-server=" + server +
                        " data-action=" + action +
                        " data-args=" + args +
                        " > " + action + "</BUTTON>&nbsp;";
                });
                return html;
            };
            var production = data.production;
            var staging = data.staging;
            production.forEach(function(server) {
                var args = "all";
                var serverProcs = findServerProcs(server, "production", data.list);
                var count0 = 0;
                var count1 = 0;
                var health = (serverProcs && serverProcs.processes && (serverProcs.processes.length > 0)) ?
                    serverProcs.processes.map(function(o) {
                        if ( o && (o.status == "online") ) {
                            count1++;
                            return 1;
                        } else {
                            count0++;
                            return 0;
                        }
                    }).reduce(function(a,b) { return a & b }) : -1;
                if (health >= 0) {
                    var count = count0+count1 ? count1 + " / " + (count1+count0) : 0;
                    var pState = count0 ? "stop" : "start";
                    var gState = serverProcs.gantry;
                    $("#production_control tbody").append("<TR>" +
                        "<TD>" + (health ?
                            "<BUTTON class='btn btn-success btn-xs disabled'>OK - " + count + "</BUTTON>" :
                            "<BUTTON class='btn btn-warning btn-xs disabled'>WARN - " + count + "</BUTTON>") + "</TD>" +
                        "<TD><strong>" + server.toUpperCase() + "</strong></TD>" +
                        "<TD>" + htmlActions(server, args, pState) + "</TD></TR>");
                }
            });
            staging.forEach(function(server) {
                var args = "all";
                var serverProcs = findServerProcs(server, "staging", data.list);
                var count0 = 0;
                var count1 = 0;
                var health = (serverProcs && serverProcs.processes && (serverProcs.processes.length > 0)) ?
                    serverProcs.processes.map(function(o) {
                        if ( o && (o.status == "online") ) {
                            count1++;
                            return 1;
                        } else {
                            count0++;
                            return 0;
                        }
                    }).reduce(function(a,b) { return a & b }) : -1;
                if (health >= 0) {
                    var count = count0+count1 ? count1 + " / " + (count1+count0) : 0;
                    var pState = count0 ? "stop" : "start";
                    var gState = serverProcs.gantry;
                    $("#staging_control tbody").append("<TR>" +
                        "<TD>" + (health ?
                            "<BUTTON class='btn btn-success btn-xs disabled'>OK - " + count + "</BUTTON>" :
                            "<BUTTON class='btn btn-warning btn-xs disabled'>WARN - " + count + "</BUTTON>") + "</TD>" +
                        "<TD><strong>" + server.toUpperCase() + "</strong></TD>" +
                        "<TD>" + htmlActions(server, args, pState) + "</TD>");
                }
            });
            bindControlButton();
        } else {
            showToast("warning", "Service status fail");
        }
    });
}

function bypassStats(){
    $("#showBypassStats thead").empty();
    $("#showBypassStats tbody").empty();
    ajax("GET", "/api/1/web/noc/get/bypassStats", undefined, true, function (data) {
        if (data && (data.code == 0)) {
            $("#showBypassStats thead").append(
                "<TR><TH></TH><TH>TPS (5s)</TH><TH>Sessions</TH><TH>WatchDog</TH><TH>Errors (1h)</TH> "+
                "<TH>Initial</TH>"+
                "<TH>Update</TH>"+
                "<TH>Terminate</TH>"+
                "<TH>Suspended Count</TH>"+
                "<TH>CDR Count</TH>"+
                "</TR>"
            );
            ([ "mocGx", "mocGy", "rocGx", "rocGy" ]).forEach(function(origin) {
                $("#showBypassStats tbody").append("<TR>" +
                    "<TD>" + origin + "</TD>" +
                    "<TD>" + data[origin + "TPS"] + "</TD>" +
                    "<TD>" + data[origin + "Sessions"] + "</TD>" +
                    "<TD>" + data[origin + "WatchDog"] + "</TD>" +
                    "<TD>" + data[origin + "Errors"] + "</TD>"+
                    "<TD>" + data[origin + "Initial"] + "</TD>"+
                    "<TD>" + data[origin + "Update"] + "</TD>"+
                    "<TD>" + data[origin + "Terminate"] +"</TD>"+
                    "<TD>" + data[origin + "Suspended"] +"</TD>"+
                    "<TD>" + data[origin + "CDR"] + "</TD>"+
                    "</TR>");
            });
            if (data.write < 3) return;
                $("tbody#activate_bypass").empty().append("<TR>" +
                    "<TD>Bypass Service (PROD)</TD>" +
                    "<TD><BUTTON class='btn btn-primary btn-xs' id='restart_bypass'>Restart</BUTTON>&nbsp;<BUTTON class='btn btn-primary btn-xs' id='start_bypass'>Start</BUTTON>&nbsp;<BUTTON class='btn btn-primary btn-xs' id='stop_bypass'>Stop</BUTTON></TD>" +
                    "<TD></TD>" +
                    "</TR>" + 
                    "<TD>Bypass Service (STAG)</TD>" +
                    "<TD><BUTTON class='btn btn-primary btn-xs' id='restart_testbypass'>Restart</BUTTON>&nbsp;<BUTTON class='btn btn-primary btn-xs' id='start_testbypass'>Start</BUTTON>&nbsp;<BUTTON class='btn btn-primary btn-xs' id='stop_testbypass'>Stop</BUTTON></TD>" +
                    "<TD></TD>" +
                    "</TR>" + 
                    "<TD>Primary OCS/PCRF</TD>" +
                    "<TD><BUTTON class='btn btn-primary btn-xs' id=epc01_divert>Divert Traffic to Bypass</BUTTON>&nbsp;<BUTTON class='btn btn-primary btn-xs' id=epc01_tcpkill>Kill TCP Connections</BUTTON>&nbsp;<BUTTON class='btn btn-primary btn-xs' id=epc01_flushiptables>Stop Bypass (back to normal)</BUTTON></TD>" +
                    "<TD></TD>" +
                    "</TR>" + 
                    "<TR>" +
                    "<TD>Secondary OCS/PCRF</TD>" +
                    "<TD><BUTTON class='btn btn-primary btn-xs' id=epc02_divert>Divert Traffic to Bypass</BUTTON>&nbsp;<BUTTON class='btn btn-primary btn-xs' id=epc02_tcpkill>Kill TCP Connections</BUTTON>&nbsp;<BUTTON class='btn btn-primary btn-xs' id=epc02_flushiptables>Stop Bypass (back to normal)</BUTTON></TD>" +
                    "<TD></TD>" +
                    "</TR>" +
                    "<TD>Test Server</TD>" +
                    "<TD><BUTTON class='btn btn-primary btn-xs' id=testserver_divert>Divert Traffic to Bypass</BUTTON>&nbsp;<BUTTON class='btn btn-primary btn-xs' id=testserver_tcpkill>Kill TCP Connections</BUTTON>&nbsp;<BUTTON class='btn btn-primary btn-xs' id=testserver_flushiptables>Stop Bypass (back to normal)</BUTTON></TD>" +
                    "<TD></TD>" +
                    "</TR>"
                );
                $("#restart_bypass").click(function () {
                    if (confirm("Are you sure you want to RESTART Bypass Service?")) {
                        ajax("POST", "/api/1/management/bypass/"+ form.key.value  + "/restart/bypass_server", "parameters", true, function (data) {
                            if (data && (data.code == 0)) {
                                alert("Restart of Bypass Service initiated!");
                            } else {
                                console.log("Oops! Something went wrong!");
                            }
                        });
                    }
                });
                $("#start_bypass").click(function () {
                    if (confirm("Are you sure you want to START Bypass Service?")) {
                        ajax("POST", "/api/1/management/bypass/"+ form.key.value  + "/start/bypass_server", "parameters", true, function (data) {
                            if (data && (data.code == 0)) {
                                alert("Starting Bypass Service..");
                            } else {
                                console.log("Oops! Something went wrong!");
                            }
                        });
                    }
                });
                $("#stop_bypass").click(function () {
                    if (confirm("Are you sure you want to STOP Bypass Service?")) {
                        ajax("POST", "/api/1/management/bypass/"+ form.key.value  + "/stop/bypass_server", "parameters", true, function (data) {
                            if (data && (data.code == 0)) {
                                alert("Stopping Bypass Service..");
                            } else {
                                console.log("Oops! Something went wrong!");
                            }
                        });
                    }
                });
                $("#restart_testbypass").click(function () {
                    if (confirm("Are you sure you want to RESTART Bypass Service?")) {
                        ajax("POST", "/api/1/management/bypass_test/"+ form.key.value  + "/restart/bypass_server", "parameters", true, function (data) {
                            if (data && (data.code == 0)) {
                                alert("Restart of Bypass Service initiated!");
                            } else {
                                console.log("Oops! Something went wrong!");
                            }
                        });
                    }
                });
                $("#start_testbypass").click(function () {
                    if (confirm("Are you sure you want to START Bypass Service?")) {
                        ajax("POST", "/api/1/management/bypass_test/"+ form.key.value  + "/start/bypass_server", "parameters", true, function (data) {
                            if (data && (data.code == 0)) {
                                alert("Starting Bypass Service..");
                            } else {
                                console.log("Oops! Something went wrong!");
                            }
                        });
                    }
                });
                $("#stop_testbypass").click(function () {
                    if (confirm("Are you sure you want to STOP Bypass Service?")) {
                        ajax("POST", "/api/1/management/bypass_test/"+ form.key.value  + "/stop/bypass_server", "parameters", true, function (data) {
                            if (data && (data.code == 0)) {
                                alert("Stopping Bypass Service..");
                            } else {
                                console.log("Oops! Something went wrong!");
                            }
                        });
                    }
                });
                $("#epc01_divert").click(function () {
                    if (confirm("Are you sure you want to DIVERT traffic from EPC01?")) {
                        ajax("POST", "/api/1/management/bypass/"+ form.key.value  + "/divert/epc01", "parameters", true, function (data) {
                            if (data && (data.code == 0)) {
                                alert("Diverting Traffic from EPC01 to Bypass initiated!");
                            } else {
                                console.log("Oops! Something went wrong!");
                            }
                        });
                    }
                });
                $("#epc01_tcpkill").click(function () {
                    if (confirm("Are you sure you want to KILL all ESTABLISHED TCP in EPC01 from GGSN?")) {
                        ajax("POST", "/api/1/management/bypass/"+ form.key.value  + "/tcpkill/epc01", "parameters", true, function (data) {
                            if (data && (data.code == 0)) {
                                alert("Killing all established connection initiated!!");
                            } else {
                                console.log("Oops! Something went wrong!");
                            }
                        });
                    }
                });
                $("#epc01_flushiptables").click(function () {
                    if (confirm("Are you sure you want to FLUSH IPTables Rules?")) {
                        ajax("POST", "/api/1/management/bypass/"+ form.key.value  + "/flush/epc01", "parameters", true, function (data) {
                            if (data && (data.code == 0)) {
                                alert("Flushing IPTables initiated!");
                            } else {
                                console.log("Oops! Something went wrong!");
                            }
                        });
                    }
                });
                $("#epc02_divert").click(function () {
                    if (confirm("Are you sure you want to DIVERT traffic from EPC02?")) {
                        ajax("POST", "/api/1/management/bypass/"+ form.key.value  + "/divert/epc02", "parameters", true, function (data) {
                            if (data && (data.code == 0)) {
                                alert("Diverting Traffic from EPC02 to Bypass initiated!");
                            } else {
                                console.log("Oops! Something went wrong!");
                            }
                        });
                    }
                });
                $("#epc02_tcpkill").click(function () {
                    if (confirm("Are you sure you want to KILL all ESTABLISHED TCP in EPC02 from GGSN?")) {
                        ajax("POST", "/api/1/management/bypass/"+ form.key.value  + "/tcpkill/epc02", "parameters", true, function (data) {
                            if (data && (data.code == 0)) {
                                alert("Killing all established connection initiated!!");
                            } else {
                                console.log("Oops! Something went wrong!");
                            }
                        });
                    }
                });
                $("#epc02_flushiptables").click(function () {
                    if (confirm("Are you sure you want to FLUSH IPTables Rules?")) {
                        ajax("POST", "/api/1/management/bypass/"+ form.key.value  + "/flush/epc02", "parameters", true, function (data) {
                            if (data && (data.code == 0)) {
                                alert("Flushing IPTables initiated!");
                            } else {
                                console.log("Oops! Something went wrong!");
                            }
                        });
                    }
                });
                $("#testserver_divert").click(function () {
                    if (confirm("Are you sure you want to DIVERT traffic from Test Server?")) {
                        ajax("POST", "/api/1/management/bypass_test/"+ form.key.value  + "/divert/test_server", "parameters", true, function (data) {
                            if (data && (data.code == 0)) {
                                alert("Diverting Traffic from Test Server to Bypass initiated!");
                            } else {
                                console.log("Oops! Something went wrong!");
                            }
                        });
                    }
                });
                $("#testserver_tcpkill").click(function () {
                    if (confirm("Are you sure you want to KILL all ESTABLISHED TCP in Test Server from GGSN?")) {
                        ajax("POST", "/api/1/management/bypass_test/"+ form.key.value  + "/tcpkill/test_server", "parameters", true, function (data) {
                            if (data && (data.code == 0)) {
                                alert("Killing all established connection initiated!!");
                            } else {
                                console.log("Oops! Something went wrong!");
                            }
                        });
                    }
                });
                $("#testserver_flushiptables").click(function () {
                    if (confirm("Are you sure you want to FLUSH IPTables Rules?")) {
                        ajax("POST", "/api/1/management/bypass_test/"+ form.key.value  + "/flush/test_server", "parameters", true, function (data) {
                            if (data && (data.code == 0)) {
                                alert("Flushing IPTables initiated!");
                            } else {
                                console.log("Oops! Something went wrong!");
                            }
                        });
                    }
                });
        }
    });
}

function getStressProgress(init) {
    var checker = setInterval(function () {
        var parameters = {};
        ajax("GET", "/api/1/web/noc/get/ecStress", parameters, true, function (data) {
            if (data && (data.code == 0)) {
                if (data.write < 3) $("#stressForm :input").prop("disabled", true);
                if (data.percent == 0) {
                    if (!init) showToast("success", "Done! Please check elitecore dashboard for result");
                    clearInterval(checker);
                }
                $("#stressProgress").css("width", data.percent + "%");
            } else {
                showToast("warning", "Oops! Check failed!");
                clearInterval(checker);
            }
        });
    }, 1000);
}

function handleStress(form) {
    if (!$(form).valid()) return false;
    var parameters = { "mobile" : form.mobile.value, "perbatch" : form.perbatch.value, "batches" : form.batches.value };
    ajax("PUT", "/api/1/web/noc/put/ecStress/request", parameters, true, function (data) {
        if (data && (data.code == 0)) {
            getStressProgress();
        } else {
            showToast("warning", "Oops! Start failed");
        }
    });
}

$("#corporatePortin").click(function(){
    generateConfirmationDialog('CREATE_CORPORATE_PORTIN', {});
});

function checkBonusAnomalyProgress(future) {
    let interval = setInterval(()=>{
        const parameters = { future, statusCheck: true };
        ajax('GET', '/api/1/web/noc/get/bonusAnomaly', parameters, true, (data) => {
            if (data && data.status && data.code == 0) {
                if (typeof(data.status.future) == 'undefined') {
                    $('#getCurrent').attr('disabled', false);
                    $('#getFuture').attr('disabled', false);
                    return clearInterval(interval);
                }
                if (typeof(future) == 'undefined') future = data.status.future;
                const fix = data.status.fix;
                const putValue = function (item) {
                    const key = future ? `${item}Future` : `${item}Current`;
                    if (item != 'undersub' && item != 'oversub') {
                        $(`#${key}`).html(data.status[item]);
                    } else {
                        $(`#${key}`).html(`<a href=# id=${key}Click>${data.status[item]}</a>`);
                        const list = data.status[`${item}List`];
                        if (typeof(list) == 'object') $(`#${key}Click`).click(() => {
                            generateTableDialog('anomalyList', list);
                        });
                    }
                }
                if (data.status.bucket) putValue('bucket');
                if (data.status.history) putValue('history');
                if (data.status.zero) putValue('zero');
                if (data.status.unattached) putValue('unattached');
                if (data.status.oversub) putValue('oversub');
                if (data.status.undersub) putValue('undersub');
                if (data.status.time) putValue('time');
                if (data.status.progress == 100) {
                    $('#anomalyProgress').css('width', `${data.status.progress}%`);
                    clearInterval(interval);
                    $('#getCurrent').attr('disabled', false);
                    $('#getFuture').attr('disabled', false);
                    if (!fix) {
                        if (future) $('#fixFuture').attr('disabled', false);
                        else $('#fixCurrent').attr('disabled', false);
                    }
                } else {
                    $('#anomalyProgress').css('width', `${data.status.progress}%`);
                }
            } else {
                clearInterval(interval);
                $('#getCurrent').attr('disabled', false);
                $('#getFuture').attr('disabled', false);
            }
        });
    }, 1000);
}

function getBonusAnomaly(future) {
    $('#getCurrent').attr('disabled', true);
    $('#getFuture').attr('disabled', true);
    $('#fixCurrent').attr('disabled', true);
    $('#fixFuture').attr('disabled', true);
    const suffix = future ? 'Future' : 'Current';
    $(`#bucket${suffix}`).html(0);
    $(`#history${suffix}`).html(0);
    $(`#zero${suffix}`).html(0);
    $(`#unattached${suffix}`).html(0);
    $(`#oversub${suffix}`).html(0);
    $(`#undersub${suffix}`).html(0);
    $(`#time${suffix}`).html(0);
    const parameters = { future };
    ajax('GET', '/api/1/web/noc/get/bonusAnomaly', parameters, true, (data) => {
        if (data && data.code == 0) checkBonusAnomalyProgress(future);
    });
}

function fixBonusAnomaly(future) {
    $('#getCurrent').attr('disabled', true);
    $('#getFuture').attr('disabled', true);
    const suffix = future ? 'Future' : 'Current';
    $(`#fix${suffix}`).attr('disabled', true);
    $(`#time${suffix}`).html(0);
    const parameters = { future }
    ajax('PUT', '/api/1/web/noc/put/bonusAnomaly/fix', parameters, true, (data) => {
        if (data && data.code == 0) checkBonusAnomalyProgress(future);
    });
}
