function init() {
	$(".username")[0].innerHTML = getCookie("cmsuser");
	getAll();
	var parameters = "";
	ajax("GET", "/api/1/web/api/get/notifications", parameters, true, function (data) {
		if ( data && (data.code == 0) ) {
			showAPI(data.list);
		} else alert("Fail");
	});
}

function getAll(activity) {
	var parameters = "";
	ajax("GET", "/api/1/web/notifications/get/all", parameters, true, function (data) {
		if ( data && (data.code == 0) ) {
			showNotifications(data.list, data.write);
			if (activity) getNotification(activity);
		} else alert("Fail");
	});
}

function getNotification(activity) {
	var parameters = "";
	ajax("GET", "/api/1/web/notifications/get/notification/" + activity, parameters, true, function (data) {
		if ( data && (data.code == 0) ) {
			$("#" + data.result.activity).css("background-color", "#F0F0F0");
			showDetails(data.result, data.write);
			showGallery(data.result.activity, data.result.assets, data.write);
		} else alert("Fail");
	});
}

function updateTeam(input, name) {
	input.disabled = true;
	var checked = (input.checked) ? "on" : "off";
	var parameters = input.name + "=" + checked;
	ajax("PUT", "/api/1/web/notifications/update/team/" + name, parameters, true, function (data) {
		if ( data && (data.code == 0) ) {
			getNotification(data.activity);
		} else alert("Fail");
	});
}

function updateTransport(input, name) {
	input.disabled = true;
	var checked = (input.checked) ? "on" : "off";
	var parameters = input.name + "=" + checked;
	ajax("PUT", "/api/1/web/notifications/update/transport/" + name, parameters, true, function (data) {
		if(data.smsblocks){
			input.disabled = false;
			input.checked = false;
			alert("changing sms transport not allowed in staging");
		} else if ( data && (data.code == 0) ) {
			getNotification(data.activity);
		} else alert("Fail");
	});
}

function updateRestriction(event, input, name) {
	var char = event.which || event.keyCode;
	if(char == 13){
		input.disabled = true;
		var parameters = input.name.split("_")[1] + "=" + input.value;
		ajax("PUT", "/api/1/web/notifications/update/restriction/" + name, parameters, true, function (data) {
			if ( data && (data.code == 0) ) {
				alert("Success");
				input.disabled = false;
				getNotification(data.activity);
			} else {
				alert("Fail");
			}
		});
	}
}

function updateGroup(input, activity, groupId) {
	input.disabled = true;
	ajax("PUT", "/api/1/web/notifications/update/group/" + activity, {
		activity: activity,
		groupId: groupId
	}, true, function (data) {
		if ( data && (data.code == 0) ) {
			getNotification(data.activity);
		} else alert("Fail");
	});
}

function updateNotification(activity, postdata) {
	var parameters = postdata;
	ajax("PUT", "/api/1/web/notifications/update/notification/" + activity, parameters, true, function (data) {
		if ( data && (data.code == 0) ) {
			getNotification(data.activity);
		} else alert("Fail");
	});
	return false;
}

function deleteNotification(activity) {
	if ( !confirm("Are you sure?") ) return false;
	var parameters = "";
	ajax("DELETE", "/api/1/web/notifications/delete/notification/" + activity, parameters, true, function (data) {
		if ( data && (data.code == 0) ) {
			location.reload();
		} else alert("Fail");
	});
	return false;
}

function submitNew(form) {
	if ( !confirm("Are you sure?") ) return false;
	var activity = form.newInput.value;
	if (!checkInput(activity)) return false;
	form.newInput.disabled = true;
	var parameters = "";
	ajax("PUT", "/api/1/web/notifications/add/new/" + activity, parameters, true, function (data) {
		if ( data && (data.code == 0) ) {
			alert("Success!");
			location.reload();
		} else alert("Fail");
	});
	return false;
}

function getLogs(input) {
	$(".username")[0].innerHTML = getCookie("cmsuser");
	var parameters = "";
	var query = (input) ? "?search=" + input.search.value : "";
	ajax("GET", "/api/1/web/notifications/get/logs" + query, parameters, true, function (data) {
		if ( data && (data.code == 0) ) {
			showLogs(data.list);
		} else alert("Fail");
	});
	return false;
}

function getWebhookLogs(input) {
	$(".username")[0].innerHTML = getCookie("cmsuser");
	var parameters = "";
	var query = (input) ? "?type=webhook&search=" + input.search.value : "?type=webhook";
	ajax("GET", "/api/1/web/notifications/get/logs" + query, parameters, true, function (data) {
		if ( data && (data.code == 0) ) {
			showWebHookLogs(data.list);
		} else alert("Fail");
	});
	return false;
}

function getEmailLogs(input) {
	$(".username")[0].innerHTML = getCookie("cmsuser");
	var parameters = "";
	var query = (input) ? "?type=email&search=" + input.search.value : "?type=email";
	ajax("GET", "/api/1/web/notifications/get/logs" + query, parameters, true, function (data) {
		if ( data && (data.code == 0) ) {
			showEmailLogs(data.list);
		} else alert("Fail");
	});
	return false;
}

function getSMSLogs(input) {
	$(".username")[0].innerHTML = getCookie("cmsuser");
	var parameters = "";
	var query = (input) ? "?type=sms&search=" + input.search.value : "?type=sms";
	ajax("GET", "/api/1/web/notifications/get/logs" + query, parameters, true, function (data) {
		if ( data && (data.code == 0) ) {
			showSMSLogs(data.list);
		} else alert("Fail");
	});
	return false;
}

function uploadAsset(input, activity) {
	var xmlhttp;			// bug: move to jquery
	if ( window.XMLHttpRequest ) xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange = function() {
		if ( xmlhttp.readyState == 4 ) {
			if ( xmlhttp.status == 403 ) {
				location.reload();
			} else if ( xmlhttp.status == 200 ) {
				var data = JSON.parse(xmlhttp.responseText);
				if ( data.code == 0 ) {
					getNotification(data.activity);
				} else alert("Fail");
			} else alert("Failed");
		}
	}
	var fData = new FormData();
	fData.append("content", input.files[0]);
	xmlhttp.open("PUT", "/api/1/web/notifications/upload/asset/" + activity, true);
	xmlhttp.send(fData);
	return false;
}
function uploadTemplate(input) {
	if ( !input || ((input.files[0].name != "email-header.html") && (input.files[0].name != "email-footer.html")) ) {
		alert("Wrong Filename");
		return false;
	}
	var xmlhttp;			// bug: move to jquery
	if ( window.XMLHttpRequest ) xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange = function() {
		if ( xmlhttp.readyState == 4 ) {
			if ( xmlhttp.status == 403 ) {
				location.reload();
			} else if ( xmlhttp.status == 200 ) {
				var data = JSON.parse(xmlhttp.responseText);
				if ( data.code == 0 ) {
					alert("Success");
				} else alert("Fail");
			} else alert("Failed");
		}
	}
	var fData = new FormData();
	fData.append("content", input.files[0]);
	xmlhttp.open("PUT", "/api/1/web/notifications/upload/template", true);
	xmlhttp.send(fData);
	return false;
}


function deleteAsset(activity) {
	var files = "";
	var assets = $('.assetSelect:checkbox:checked');
	for ( var i=0; i<assets.length; i++ ) {
		files += assets[i].value;
	};
	var parameters = "delete=" + files;
	ajax("DELETE", "/api/1/web/notifications/delete/asset/" + activity, parameters, true, function (data) {
		if ( data && (data.code == 0) ) {
			getNotification(data.activity);
		} else alert("Fail");
	});
	return false;
}

function testSend(form) {
	var activity = form.activity.value;
	var elitecore = form.elitecore.checked ? "elitecore" : "";
	var email = form.email.value;
	var number = form.number.value;
	var name = form.name.value;
	var params = (form.params.value != "") ? "&" + form.params.value : "";

	if (!activity || (!number && !email)) {
		return false;
	}

	form.elitecore.disabled = true;
	form.email.disabled = true;
	form.number.disabled = true;
	form.name.disabled = true;
	form.params.disabled = true;

	var parameters = "type=" + elitecore + "&email=" + email + "&number=" + number + "&name=" + name + "" + params;
	ajax("POST", "/api/1/web/notifications/test/send/" + activity, parameters, false, function (data) {
		form.elitecore.disabled = false;
		form.email.disabled = false;
		form.number.disabled = false;
		form.name.disabled = false;
		form.params.disabled = false;
		if ( data && (data.code == 0) ) {
			alert("Success!");
		} else {
			alert("Fail");
		}
	});
}

function delLog(id) {
	var parameters = { };
	ajax("DELETE", "/api/1/web/notifications/delete/log/" + id, parameters, false, function (data) {
		if ( data && (data.code == 0) ) {
			alert("Success!");
			getLogs();
		}
	});
}

function checkInput(str) {
	if ( str == "" ) {
		$("#warning")[0].innerHTML = "";
		return true;
	} else if (!str.match(/^[0-9a-z_]+$/)) {
		$("#warning")[0].innerHTML = "Invalid";
		return false;
	} else {
		$("#warning")[0].innerHTML = "";
		return true;
	}
}

function showNotifications(notifications, write) {
	$('#showAll tbody').empty();
	if (write) $(".showAdd").show();
	notifications.forEach(function(item) {
		var edit = (write) ? "<button type='button' class='btn btn-white btn-sm' style='border-style:none;'  onClick='deleteNotification(\"" + item.activity + "\");'><i class='fa fa-trash-o'></i></button>" : "";
		$("#showAll > tbody:last-child").append("<TR id=" + item.activity + " class='notifs'>" +
			"<TD>" + item.activity + "</TD>" +
			"<TD>" + edit + "</TD>" +
		"</TR>");
		$("#" + item.activity).click(function() {
			getNotification(item.activity);
		});
	});
	$('#showAll').DataTable();
}

function showDetails(details, write) {
	$('#showTeams tbody').empty();
	$('#showTransports tbody').empty();
	$('#showRestrictions tbody').empty();
	$('#showClientDetails tbody').empty();
	$('#showInternalDetails tbody').empty();
	$('#showActions tbody').empty();
	$('#showGroups tbody').empty();

	var plain = (details.plain) ? details.plain : " ( not set ) ";
	var app = (details.app) ? details.app: " ( not set ) ";
	var subject = (details.subject) ? details.subject : " ( not set ) ";
	var html = (details.html) ? details.html : " ( not set ) ";
	var email_internal_1 = (details.emailInternal1) ? details.emailInternal1 : " ( not set ) ";
	var subject_internal_1 = (details.subjectInternal1) ? details.subjectInternal1 : " ( not set ) ";
	var html_internal_1 = (details.htmlInternal1) ? details.htmlInternal1 : " ( not set ) ";

	//client facing information
	$("#showClientDetails > tbody:last-child").append("<TR id=" + details.activity + ">" +
			"<TD>SMS</TD>" +
			"<TD><DIV class=toggle_ data-id='plain'>" + escapeHtml(plain) + "</DIV>" +
		"<DIV class=error_ style='color:#FF0000' data-id='format_error'>" + checkSms(details.plain) + "</DIV></TD>" +
			"<TD><DIV class=count_ data-id='symbols_count'>" + ((details.plain) ? details.plain.length : 0) + "</DIV></TD></TR>");
	$("#showClientDetails > tbody:last-child").append("<TR id=" + details.activity + ">" +
			"<TD>App Push</TD>" +
			"<TD><DIV class=toggle_ data-id='app'>" + escapeHtml(app) + "</DIV>" +
		"<DIV class=error_ style='color:#FF0000' data-id='format_error'>" + checkAppPushFormat(details.app) + "</DIV></TD>" +
			"<TD><DIV class=count_ data-id='symbols_count'>" + getAppPushTextLength(details.app) + "</DIV></TD></TR>");
	$("#showClientDetails > tbody:last-child").append("<TR id=" + details.activity + ">" +
			"<TD>Subject</TD>" +
			"<TD><DIV class=toggle_ data-id='subject'>" + escapeHtml(subject) + "</DIV></TD><TD></TD></TR>");
	$("#showClientDetails > tbody:last-child").append("<TR id=" + details.activity + ">" +
			"<TD>HTML</TD>" +
			"<TD><DIV class=editInline data-id='html'>"+ html + "</DIV></TD><TD></TD><TR>");

	//internal information
	$("#showInternalDetails > tbody:last-child").append("<TR id=" + details.activity + ">" +
			"<TD>Receiver email address</TD>" +
			"<TD><DIV class=toggle_ data-id='email_internal_1'>" + escapeHtml(email_internal_1) + "</DIV></TD><TD></TD></TR>");
	$("#showInternalDetails > tbody:last-child").append("<TR id=" + details.activity + ">" +
			"<TD>Subject</TD>" +
			"<TD><DIV class=toggle_ data-id='subject_internal_1'>" + escapeHtml(subject_internal_1) + "</DIV></TD><TD></TD></TR>");
	$("#showInternalDetails > tbody:last-child").append("<TR id=" + details.activity + ">" +
			"<TD>HTML</TD>" +
			"<TD><DIV class=editInline data-id='html_internal_1'>"+ html_internal_1 + "</DIV></TD><TD></TD><TR>");

	if (write) {
		$("#showActions tbody").append("<TR><TD><DIV class='col-sm-12'><FORM id=testSend class=form-horizontal onSubmit='event.preventDefault(); testSend(this);'>" +
			"<DIV class=form-group>" +
				"<LABEL for=activity class='col-sm-3 control-label'>Activity : </LABEL>" +
				"<LABEL class='col-sm-5 control-label' style='text-align:left'>" + details.activity + "</LABEL>" +
				"<DIV class=col-sm-2>"+
					"<INPUT type=checkbox name='elitecore' disabled> elitecore</INPUT>" +
				"</DIV>" +
				"<DIV class=col-sm-2>"+
					"<INPUT class='form-control btn-default btn-xs' type=submit value=Send>" +
					"<INPUT id=activity class=col-sm-8 type=hidden name=activity value=" + details.activity+ ">" +
				"</DIV>" +
			"</DIV>" +
			"<DIV class=form-group>" +
				"<LABEL for=inputEmail class='col-sm-3 control-label'>Email : </LABEL>" +
				"<DIV class=col-sm-9><INPUT id=inputEmail class=form-control type=email name=email></DIV>" +
			"</DIV>" +
			"<DIV class=form-group>" +
				"<LABEL for=inputNumber class='col-sm-3 control-label'>Number (No Prefix) : </LABEL>" +
				"<DIV class=col-sm-9><INPUT id=inputNumber class=form-control type=number name=number></DIV>" +
			"</DIV>" +
			"<DIV class=form-group>" +
				"<LABEL for=inputName class='col-sm-3 control-label'>Name : </LABEL>" +
				"<DIV class=col-sm-9><INPUT id=inputName class=form-control type=text name=name></DIV>" +
			"</DIV>" +
			"<DIV class=form-group>" +
				"<LABEL for=inputParams class='col-sm-3 control-label'>Params : </LABEL>" +
				"<DIV class=col-sm-9><INPUT id=inputParams class=form-control type=text name=params></DIV>" +
			"</DIV></FORM></DIV></TD></TR>");
		$(".editInline").click(function(){
			this.focus();
			var editor;
			if (!CKEDITOR.instances.editor1) {
				CKEDITOR.config.removePlugins = 'wsc,scayt';
				editor = CKEDITOR.replace(this);
			}
			if (editor) {
				editor.on("instanceReady", function(ev) {
					if ( this.document.getBody().getHtml() == "<p>( not set )</p>" ) {
						this.setData('');
					}
					ev.editor.focus();
				});

				var name = $(this).attr('data-id');
				editor.on("blur", function() {
					if (this.document) {
						var obj = new Object;
						obj[name] = this.document.getBody().getHtml();
						updateNotification(details.activity, obj);
						editor.destroy();
					}
				});
			}
		});
		$(".toggle_").click(function(){
	    if ($("#editTextInput").length > 0) return;
			this.outerHTML = "<textarea name='" + $(this).attr('data-id')
			+ "' rows=4 id=editTextInput style='width: "
			+ $(".toggle_")[1].clientWidth + "px; height: 200px;'>";

			if ( $(this)[0].innerHTML != " ( not set ) ") {
				var name = $("#editTextInput").attr("name");
				if (name === 'app') {
					$("#editTextInput").val(app);
				} else if (name === 'plain') {
					$("#editTextInput").val(plain);
				} else if (name === 'subject') {
					$("#editTextInput").val(subject);
				} else if (name === 'email_internal_1') {
					$("#editTextInput").val(email_internal_1);
				} else if (name === 'subject_internal_1') {
					$("#editTextInput").val(subject_internal_1);
				} else {
					$("#editTextInput").val(unescapeHtml($(this)[0].innerHTML));
				}
			}
			$("#editTextInput").focus();

			$("#editTextInput").blur(function() {
				var name = $("#editTextInput").attr("name");
				var value = $("#editTextInput").val();
				var obj = new Object;
				obj[name] = value;
				updateNotification(details.activity, obj);
			});

			$("#editTextInput").keyup(function() {
				var name = $("#editTextInput").attr("name");
				var counter = $(this).parent().siblings()[1].querySelector('[data-id=symbols_count]');
				var value = $(this).val();

		if (counter) {
		    counter.innerHTML = "N/A";
		    if (name === 'app') {
			var formatError = $(this).siblings()[0];
			formatError.innerHTML = checkAppPushFormat(value);
			counter.innerHTML = getAppPushTextLength(value);
		    } else if (name === 'plain') {
			var formatError = $(this).siblings()[0];
			formatError.innerHTML = checkSms(value);
			counter.innerHTML = '' + value.length;
		    }
				}
			});
		});
	}

	details.teams.forEach(function (team) {
		var checked = ( parseInt(team.allowed) == 0 ) ? "" : "checked";
		var edit = (write) ? "onChange='updateTeam(this,\"" + details.activity + "\");'" : "disabled";
		$("#showTeams > tbody:last-child").append("<TR>" +
			"<TD>" + team.name + "</TD>" +
			"<TD><INPUT type=checkbox name=team_" + team.id + " " + edit + " " + checked + "></TD>" +
		"<TR>");
		if ((team.name == "Elitecore") && (checked == "checked")) $("#testSend input[name=elitecore]").prop("disabled", false);
	});
	details.transports.forEach(function (transport) {
		var disabled = ( transport.name == "USSD" ) ? "disabled" : "";
		var checked = ( parseInt(transport.enabled) == 0 ) ? "" : "checked";
		var edit = (write) ? "onChange='updateTransport(this,\"" + details.activity + "\");'" : "disabled";
		$("#showTransports > tbody:last-child").append("<TR>" +
			"<TD>" + transport.name + "</TD>" +
			"<TD><INPUT type=checkbox name=transport_" + transport.id + " " + edit + " " + checked + " " + disabled + "></TD>" +
		"<TR>");
	});
	details.restrictions.forEach(function (restriction) {
		var disabled = (write) ? "" : "disabled";
		var edit = (write) ? "onkeypress='updateRestriction(event, this,\"" + details.activity + "\");'" : "";
		var val = (restriction.value == "0") ? "" : restriction.value;
		$("#showRestrictions > tbody:last-child").append("<TR>" +
			"<TD>" + restriction.name + "</TD>" +
			"<TD><INPUT type=textbox name=restriction_" + restriction.id + " value='" + val + "' " + edit + " " + disabled + "></TD>" +
		"<TR>");
	});

	var hasSelectedGroup = false;
	details.groups.forEach(function (group) {
		if (group.selected) {
			hasSelectedGroup = true;
		}
	});
	$("#showGroups > tbody:last-child").append("<TR>" +
	"<TD>" + "NONE" + "</TD>" +
	"<TD>" + '<input type="radio" id="radio-1" ' + (!hasSelectedGroup ? 'checked="true"' : '') +
	' onclick="updateGroup(this,\'' + details.activity + '\', 0);" name="radios" value="0" />' +
	'<label for="radio-1"></label>' + "</TD>" +
	"<TR>");
	details.groups.forEach(function (group) {
		$("#showGroups > tbody:last-child").append("<TR>" +
		"<TD>" + group.type + "</TD>" +
		"<TD>" + '<input type="radio" id="radio-1" ' + (group.selected ? 'checked="true"' : '') +
		' onclick="updateGroup(this,\'' + details.activity + '\', ' + group.id + ');" name="radios" value="' + group.id + '" />' +
		'<label for="radio-1"></label>' + "</TD>" +
		"<TR>");
	});
}

function checkAppPushFormat(value) {
    if (!value) return "";
    try {
	var json = JSON.parse(value);
	if (!json.text) {
	    return '"text" field is missing or empty';
	} else if (!json.mime_type) {
	    return '"mime_type" field is missing or empty';
	} else if (!json.title) {
	    return '"title" field is missing or empty';
	} else if (json.text.length > 320){
	    return '"text" field is too long, limit is 320 symbols (mind length of dynamic variables)';
	} else {
	    return "";
	}
    } catch(e) {
	return 'app push is not a valid json object';
    }
}

function checkSms(value) {
    if (!value) return "";
    if (value.length > 320){
	return 'message is too long, limit is 320 symbols (mind length of dynamic variables)';
    } else {
	return "";
    }
}

function getAppPushTextLength(value) {
    if (!value) return "0";
    try {
	var json = JSON.parse(value);
	return '' + json.text.length;
    } catch(e) {
	return '-1';
    }
}

function checkSmsLength(value) {
	if (!value) return "";
	if (value.length > 320){
		return 'message is too long, limit is 320 symbols (mind length of dynamic variables)';
	} else {
		return "";
	}
}

function escapeHtml(value) {
	return value.replace(/</g, "&lt;").replace(/>/g, "&gt;");
}

function unescapeHtml(value) {
	return value.replace("&lt;", /</g).replace("&gt;", />/g);
}

var logContent = {};

function showLogs(logs) {
	logContent = {};

	$("#showLogs tbody").empty();
	logs.forEach(function (log) {
		var id = log._id;
		logContent[id] = log;

		var date = getDateTime(log.ts);
		var source = log.ip;
		var activity = log.activity;
		var number = log.number;
		var email = log.email;
		var transport = log.transport;

		$("#showLogs > tbody:last-child").append("<TR>" +
			"<TD itemId=" + id + " class=tdclick>" + date + "</TD>" +
			"<TD itemId=" + id + " class=tdclick>" + (source ? source : "NONE")  + "</TD>" +
			"<TD itemId=" + id + " class=tdclick>" + (number ? number : "NONE") + "</TD>" +
			"<TD itemId=" + id + " class=tdclick>" + (email ? email : "NONE") + "</TD>" +
			"<TD itemId=" + id + " class=tdclick>" + (activity ? activity : "NONE") + "</TD>" +
			"<TD itemId=" + id + " class=tdclick>" + (transport && transport.length > 0 ? transport : "NONE") + "</TD>" +
			"<TD><A href=# class=delLog data-id=" + id + "><I class='fa fa-trash-o'></I></A></TD>" +
		"</TR>");
	});

	$(".delLog").click(function() {
		$(this).attr("disabled", "disabled");
		delLog($(this).attr('data-id'));
	});
	$(".tdclick").click(function() {
		showLogContent($(this).attr('itemId'));
	});
}

function showLogContent(logId) {
	var emailObj = logId && logContent ? logContent[logId] : undefined;
	if (emailObj) {
		BootstrapDialog.show({
			size: BootstrapDialog.SIZE_WIDE,
			title: "ACTIVITY: " + emailObj.activity,
			message: JSON.stringify(emailObj, null, 4),
			buttons: []
		});
	} else {
		showToast("danger", "Log content is not found");
	}
}
function showWebHookLogs(logs) {
	$("#showLogs tbody").empty();
	logs.forEach(function (log) {
		var date = getDateTime(log.ts);
		var source = log.ip + " " + log.number + "";
		var activity = log.activity;
		var status = log.status;
		delete log._id;
		delete log.ts;
		delete log.ip;
		delete log.activity;
		delete log.status;
		delete log.number;
		$("#showLogs > tbody:last-child").append("<TR>" +
		"<TD>" + date + "</TD>" +
		"<TD>" + source + "</TD>" +
		"<TD>" + status + "</TD>" +
		"<TD>" + activity + "</TD>" +
		"<TD>" + JSON.stringify(log) + "</TD>" +
		"</TR>");
	});
}

var emailHtmlContent = {};
var emailErrorContent = {};

function showEmailLogs(logs) {
	emailHtmlContent = {};
	emailErrorContent = {};

	$("#showLogs tbody").empty();
	logs.forEach(function (log) {
		var id = log._id;
		emailHtmlContent[id] = {
			html: log.html,
			to: log.to,
			from: log.from,
			subject: log.subject
		};
		emailErrorContent[id] = {
			to: log.to,
			error: log.error,
			status: log.status
		}

		var date = getDateTime(log.ts);
		var to = log.to;
		var from = log.from;
		var status = log.status;
		var activity = log.activity;
		var subject = log.subject;
		var files = log.files;

		$("#showLogs > tbody:last-child").append("<TR>" +
		"<TD itemId=" + id + ">" + date + "</TD>" +
		"<TD itemId=" + id + ">" + to + "</TD>" +
		"<TD itemId='" + id + " " + (status !== "OK" ? "errorId=" + id : "" )+ " '>" + status + "</TD>" +
		"<TD itemId=" + id + ">" + activity + "</TD>" +
		"<TD itemId=" + id + ">" + subject + (files ? " >> FILES: " + JSON.stringify(files) : "") + " </TD>" +
		"</TR>");
	});

	$("td").click(function() {
		if ($(this).attr('errorId')) {
			showErrorContent($(this).attr('errorId'));
		} else {
			showEmailContent($(this).attr('itemId'));
		}
	});
}

function showEmailContent(logId) {
	var emailObj = logId && emailHtmlContent ? emailHtmlContent[logId] : undefined;
	if (emailObj) {
		BootstrapDialog.show({
			size: BootstrapDialog.SIZE_WIDE,
			title: "TO: " + emailObj.to + "  FROM: " + emailObj.from +  "\nSUBJECT: " + emailObj.subject,
			message: emailObj.html,
			buttons: []
		});
	} else {
		showToast("danger", "Email content is not found");
	}
}

function showErrorContent(logId) {
	var emailObj = logId && emailErrorContent ? emailErrorContent[logId] : undefined;
	if (emailObj) {
		BootstrapDialog.show({
			size: BootstrapDialog.SIZE_WIDE,
			title: "TO: " + emailObj.to + "\nSTATUS: " + emailObj.status,
			message: emailObj.error,
			buttons: []
		});
	} else {
		showToast("danger", "Email content is not found");
	}
}

function showGallery(name, assets, write) {
	$("#addAsset").empty();
	$('#gallery').empty();
	if (write) {
		$("#addAsset").append("<button type='button' class='btn btn-white btn-sm' onClick='if (confirm(\"Are you sure?\")) deleteAsset(\"" + name + "\");'><i class='fa fa-trash-o'></i> Delete</button>" +
					"<button type='button' class='btn btn-white btn-sm' id=selectAll><i class='fa fa-check-square-o'></i> Select all</button>" +
					"<a href='#addAsset' type='button' class='btn pull-right btn-sm fileinput-button'><i class='fa fa-upload'></i> Upload New File" +
					"<INPUT type='file' onChange='event.preventDefault(); return uploadAsset(this, \"" + name + "\");'></a>");
		$("#selectAll").click(function() {
			if ($('.assetSelect').prop('checked'))
				$('.assetSelect').prop('checked', false);
			else
				$('.assetSelect').prop('checked', true);
		});
	}
	if ( !assets || (assets.length == 0) ) return false;
	assets.forEach(function (asset) {
		var type;
		var select = (write) ? "<INPUT type=checkbox class=assetSelect value=" + asset.link.replace(/^.*[\\\/]/, '') + ">" : "";
		if ( asset.mime.split('/')[0] == "image" ) type = "images";
		if (type) $("#gallery").append(
			"<DIV class='" + type + " item ' >" +
					"<IMG src='" + asset.link + "' alt='' />" +
				select + "<P>" + asset.link.replace(/^.*[\\\/]/, '') + "</P>" +
			"</DIV>");
	});
	var $container = $('#gallery');
	if ($container.hasClass('isotope')) $container.isotope('destroy');
	$container.isotope({
		itemSelector: '.item',
		animationOptions: {
			duration: 750,
			easing: 'linear',
			queue: false
		}
	});
	// filter items when filter link is clicked
	$('#filters a').click(function() {
		var selector = $(this).attr('data-filter');
		$container.isotope({filter: selector});
		return false;
	});
}

function showSMSLogs(logs) {

	emailErrorContent = {};

	$("#showLogs tbody").empty();
	logs.forEach(function (log) {
		var id = log._id;

		emailErrorContent[id] = {
			to: log.to,
			error: log.error,
			status: log.status
		}

		var date = getDateTime(log.ts);
		var prefix = (log.prefix) ? log.prefix : "";
		var to = log.to;
		var from = log.from;
		var status = log.status;
		var activity = log.activity;
		var plain = log.plain

		$("#showLogs > tbody:last-child").append("<TR>" +
		"<TD itemId=" + id + ">" + date + "</TD>" +
		"<TD itemId=" + id + ">" + prefix + "</TD>" +
		"<TD itemId=" + id + ">" + to + "</TD>" +
		"<TD itemId=" + id + " " + (status !== "OK" ? "errorId=" + id + "": "" ) + ">" + status + "</TD>" +
		"<TD itemId=" + id + ">" + activity + "</TD>" +
		"<TD itemId=" + id + ">" + plain + " </TD>" +
		"</TR>");
	});

	$("td").click(function() {
		showErrorContent($(this).attr('itemId'));
	});
}
