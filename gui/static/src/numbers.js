function init(type) {
	$(".username")[0].innerHTML = getCookie("cmsuser");
}

var DEFAULT_RELOAD_CALLBACK = function(){
    loadNumbers();
}

DEFAULT_RELOAD_CALLBACK();
function loadNumbers(){
    $.ajax({
        "type": "GET",
        "dataType": "json",
        "url": "/api/1/web/numbers/get/numbers",
        "data": {},
        "success": function (response) {
            if(response.code == 0){
                generateTable("numbersTable", response.list, [
                    {"sTitle": "Id", "sClass": "twocol-table-column", "mData": "_id", "mRender": function(data, type, full){
                            return data;
                        }
                    },
                    {"sTitle": "Status", "sClass": "twocol-table-column", "mData": "type", "mRender": function(data, type, full){
                            return data;
                        }
                    },
                    {"sTitle": "Actions", "sClass": "twocol-table-column", "mData": "type", "mRender": function(data, type, full){
                            return getTableActionButton('Delete', undefined, "DELETE_NUMBER_FROM_POOL", {number: full._id});
                        }
                    }
                ], 10, true);
                
            }else{
                showToast("warning", "Error fetching numbers list");
            }
        },
        "error": function (result) {
            showToast("warning", "Error fetching numbers list");
        }
    });
}

$('#addNumbers').click(function(){
    generateConfirmationDialog('ADD_NUMBERS', {}, undefined,
    function(err, data){
        if(err){
            showToast("warning", "Error saving numbers");
        }else{
            DEFAULT_RELOAD_CALLBACK();
        }
    });
    setTimeout(function(){
        $('[name="list"]').parent().css( "height", "100px");
    }, 220);
});