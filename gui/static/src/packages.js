function init() {
	$(".username")[0].innerHTML = getCookie("cmsuser");
	$.ajax({
		url: "/api/1/web/packages/business/hierarchy",
		type: "GET",
		dataType: "json",
		data: {},
		success: function(data){
			if ( data.code == 0 ) {
				buildTree(data.result);
			} else alert("Fail");
		},
		statusCode: {
			403: function(error){
				location.reload();
			}
		}
	});
	$.ajax({
		url: "/api/1/web/api/get/packages",
		type: "GET",
		dataType: "json",
		data: {},
		success: function(data){
			if ( data.code == 0 ) {
				showAPI(data.list);
			} else alert("Fail");
		},
		statusCode: {
			403: function(error){
				location.reload();
			}
		}
	});
}

//function update_package(id, name) {
function update_package(form) {
	$.ajax({
		url: "/api/1/web/packages/update/package/" + form.id.value,
		type: "PUT",
		dataType: "json",
//		  data: $("#formPackageAttr-"+name).serialize(),
		data: $(form).serialize(),
		success: function(data){
			if ( data.code == 0 ) {
				showPackage(data.id);
			} else alert("Fail");
		},
		statusCode: {
			403: function(error){
				location.reload();
			}
		}
	});
	return false;
}

function updateAction(input, id) {
	input.disabled = true;
	var checked = (input.checked) ? "on" : "off";
	$.ajax({
		url: "/api/1/web/packages/update/package/" + id,
		type: "PUT",
		dataType: "json",
//		  data: $("#formPackageAttr-"+name).serialize(),
		data: input.name + "=" + checked,
		success: function(data){
			if ( data.code == 0 ) {
				showPackage(data.id);
			} else alert("Fail");
		},
		statusCode: {
			403: function(error){
				location.reload();
			}
		}
	});
	return false;
}

function updateTeam(input, name) {
	input.disabled = true;
	var checked = (input.checked) ? "on" : "off";
	$.ajax({
		url: "/api/1/web/packages/update/team/" + name,
		type: "PUT",
		dataType: "json",
		data: input.name + "=" + checked,
		success: function(data){
			if ( data.code == 0 ) {
				showPackage(data.id);
			} else alert("Fail");
		},
		statusCode: {
			403: function(error){
				location.reload();
			}
		}
	});
}

function uploadAsset(input, name) {
	var xmlhttp;			// bug: move to jquery
	if ( window.XMLHttpRequest ) xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange = function() {
		if ( xmlhttp.readyState == 4 ) {
			if ( xmlhttp.status == 403 ) {
				location.reload();
			} else if ( xmlhttp.status == 200 ) {
				var data = JSON.parse(xmlhttp.responseText);
				if ( data.code == 0 ) {
					showPackage(data.id);
				} else alert("Fail");
			} else alert("Failed");
		}
	}
	var fData = new FormData();
	fData.append("content", input.files[0]);
	xmlhttp.open("PUT", "/api/1/web/packages/upload/asset/" + name, true);
	xmlhttp.send(fData);
	return false;
}

function deleteAsset(name) {
	var files = "";
	var assets = $('.assetSelect:checkbox:checked');
	for ( var i=0; i<assets.length; i++ ) {
		files += assets[i].value;
	};
	$.ajax({
		url: "/api/1/web/packages/delete/asset/" + name,
		type: "DELETE",
		dataType: "json",
		data: "delete=" + files,
		success: function(data){
			if ( data.code == 0 ) {
				showPackage(data.id);
			} else alert("Fail");
		},
		statusCode: {
			403: function(error){
				location.reload();
			}
		}
	});
	return false;
}

function getBoostsLogs(input) {
	$(".username")[0].innerHTML = getCookie("cmsuser");
	var parameters = "";
	var query = (input) ? "?search=" + input.search.value : "";
	ajax("GET", "/api/1/web/packages/get/boosts/logs" + query, parameters, true, function (data) {
		if ( data && (data.code == 0) ) {
			showPackageLogs(data.list);
		} else alert("Fail");
	});
	return false;
}

function getAddonLogs(input) {
	$(".username")[0].innerHTML = getCookie("cmsuser");
	var parameters = "";
	var query = (input) ? "?search=" + input.search.value : "";
	ajax("GET", "/api/1/web/packages/get/addon/logs" + query, parameters, true, function (data) {
		if ( data && (data.code == 0) ) {
			showPackageLogs(data.list);
		} else alert("Fail");
	});
	return false;
}

function showPackage(name) {
	$.ajax({
		url: "/api/1/web/packages/get/package/" + name,
		type: "GET",
		dataType: "json",
		data: {},
		success: function(data){
			if ( data.code == 0 ) {
				$('#showPackage tbody').empty();
				$('#showTeams tbody').empty();
				showGallery(data.result.cache.id, data.result.assets, data.write);
				showPackageDetails(data.result, data.write);
			} else alert("Fail");
		},
		statusCode: {
			403: function(error){
				location.reload();
			}
		}
	});
}

function showGallery(name, assets, write) {
	$("#addAsset").empty();
	$('#gallery').empty();
	if (write) {
		$("#addAsset").append("<button type='button' class='btn btn-white btn-sm' onClick='if (confirm(\"Are you sure?\")) deleteAsset(\"" + name + "\");'><i class='fa fa-trash-o'></i> Delete</button>" +
					"<button type='button' class='btn btn-white btn-sm' id=selectAll><i class='fa fa-check-square-o'></i> Select all</button>" +
					"<a href='#addAsset' type='button' class='btn pull-right btn-sm fileinput-button'><i class='fa fa-upload'></i> Upload New File" + 
					"<INPUT type='file' onChange='event.preventDefault(); return uploadAsset(this, \"" + name + "\");'></a>");
		$("#selectAll").click(function() {
			if ($('.assetSelect').prop('checked')) 
				$('.assetSelect').prop('checked', false);
			else
				$('.assetSelect').prop('checked', true);
		});
	}
	if ( !assets || (assets.length == 0) ) return false;
	assets.forEach(function (asset) {
		var type;
		var select = (write) ? "<INPUT type=checkbox class=assetSelect value=" + asset.link.replace(/^.*[\\\/]/, '') + ">" : "";
		if ( asset.mime.split('/')[0] == "image" ) type = "images"; 
		if (type) $("#gallery").append(
			"<DIV class='" + type + " item ' >" +
					"<IMG src='" + asset.link + "' alt='' />" +
				select + "<P>" + asset.link.replace(/^.*[\\\/]/, '') + "</P>" +
			"</DIV>");
	});
	var $container = $('#gallery');
	if ($container.hasClass('isotope')) $container.isotope('destroy');
	$container.isotope({
		itemSelector: '.item',
		animationOptions: {
			duration: 750,
			easing: 'linear',
			queue: false
		}
	});
	// filter items when filter link is clicked
	$('#filters a').click(function() {
		var selector = $(this).attr('data-filter');
		$container.isotope({filter: selector});
		return false;
	});
}

function showPackageDetails(details, write) {
	Object.keys(details.cache).forEach(function (key) {
		$("#showPackage > tbody:last-child").append("<TR>" +
			"<TD>" + key + "</TD>" +
			"<TD>" + details.cache[key] + "</TD>" +
		"<TR>");
	});
	Object.keys(details).forEach(function(key) {
		if ( key == "teams" ) {
			details.teams.forEach(function (team) {
				var checked = ( parseInt(team.available) == 0 ) ? "" : "checked";
				var edit = (write) ? "onChange='updateTeam(this,\"" + details.cache.id + "\");'" : "disabled";
				$("#showTeams > tbody:last-child").append("<TR>" +
					"<TD>" + team.name + "</TD>" +
					"<TD><INPUT type=checkbox name=team_" + team.id + " " + edit + " " + checked + "></TD>" +
				"<TR>");
			});
		} else if ( key == "action" || key == "beta" || key == "advanced_payment") {
			var checked = ( parseInt(details[key]) == 0 ) ? "" : "checked";
			var edit = (write) ? "onChange='updateAction(this,\"" + details.cache.id + "\");'" : "disabled";
			$("#showPackage > tbody:last-child").append("<TR>" +
				"<TD>" + key + "</TD>" +
				"<TD><INPUT type=checkbox name=" + key + " " + edit + " " + checked + "></TD>" +
			"<TR>");
		} else if ( (key == "sub_effect") || (key == "unsub_effect") ) {
			var options = "";
			details.effective.forEach(function (option, idx) {
				var selected = (details[key] == idx) ? "selected" : "";
				options += "<OPTION value=" + idx + " " + selected + ">" + option + "</OPTION>";
			});
			$("#showPackage > tbody:last-child").append("<TR id=" + key + ">" +
				"<TD>" + key + "</TD>" +
				"<TD><FORM><INPUT type=hidden name=id value='" + details.cache.id + "'><SELECT name='" + key + "' class=form-control onChange='update_package(this.form);'>" + options + "</SELECT></FORM></TD>" +
			"</TR>");
		} else if ( (key !== "cache") && (key !== "teams") && (key !== "assets") && (key !== "effective") ) {
			$("#showPackage > tbody:last-child").append("<TR id=" + key + ">" +
				"<TD>" + key + "</TD>" +
				"<TD><LABEL id=toggle_" + key + ">" + details[key] + "</TD>" +
			"<TR>");
			if (write) {
				$("#" + key).click(function(){
					if ( $("#editText").length > 0 ) return; 
					var text = $("#toggle_" + key)[0].innerHTML;
//					this.outerHTML = "<form id = 'formPackageAttr-" + key + "' class= 'formPackageAttr'><INPUT name='" + key + "' class='packageAttrValue' data-id='" + name + "' type=text value='" + text + "'></form>";
					$("#toggle_" + key)[0].outerHTML = "<FORM id=editText onSubmit='event.preventDefault(); return update_package(this);'><INPUT class='form-control' name='" + key + "' type=text value='" + text + "' id=editTextInput><INPUT type=hidden name=id value='" + details.cache.id + "'></FORM>";
					$("#editTextInput").focus();
					$("#editTextInput").blur(function() {
						update_package($("#editText")[0]);
					});
				});
			}
		}
	});
}

function buildTree(bhier) {
	var DataSourceTree = function (options) {
		this._data  = options.data;
		this._delay = options.delay;
	};

	DataSourceTree.prototype = {
		data: function (options, callback) {
			var self = this;
			setTimeout(function () {
				if (options.data) {
					callback({ data: options.data, start: 0, end: 0, count: 0, pages: 0, page: 0 });
				} else {
					callback({ data: self._data, start: 0, end: 0, count: 0, pages: 0, page: 0 });
				}
			}, this._delay)
		}
	};

	var treeData1 = new Array;
	Object.keys(bhier).sort().forEach(function (level1, index1) {
		var i = index1++;
		var treeData2 = new Array;
		var bhierLevel1 = bhier[level1].sort(packageSort);
		bhierLevel1.forEach(function (level2, index2) {
			var j = index2++;
			var treeData3 = new Array;
			if (level2.base_id) {
				Object.keys(level2).sort().forEach(function (level3, index3) {
					var k = index3++;
					if ( level3 != "base_id" ) {
						var treeData4 = new Array;
						var bhierLevel2 = level2[level3].sort(packageSort);
						bhierLevel2.forEach(function (level4, index4) {
							var l = index4++;
							treeData4.push({ 
								"name" : level4.name + " <div class='tree-actions'></div>",
								"type" : "item",
								"additionalParameters" : { "id" : "I" + i + j + k + l, "packageID" : level4.id }
							});
						});
						treeData3.push({ 
							"name" : level3 + " <div class='tree-actions'></div>",
							"type" : "folder",
							"additionalParameters" : { "id" : "F" + i + j + k},
							"data" : treeData4
						});
					}
				});
				treeData2.push({ 
					"name" : level2.base_id + " <div class='tree-actions'></div>",
					"type" : "folder",
					"additionalParameters" : { "id": "F" + i + j},
					"data" : treeData3
				});
			} else {
				treeData2.push({ 
					"name" : level2.name + " <div class='tree-actions'></div>",
					"type" : "item",
					"additionalParameters" : { "id" : "I" + i + j, "packageID" : level2.id }
				});
			}
		}); 
		treeData1.push({ 
			"name" : level1 + " <div class='tree-actions'></div>",
			"type" : 'folder',
			"additionalParameters" : { "id" : "F" + i }, 
			"data" : treeData2
			});
	});

	var treeDataSource = new DataSourceTree({
		data: treeData1,
		delay: 200
	});
	
	$('#FlatTree').tree({
		dataSource: treeDataSource,
		loadingHTML: '<img src="../../res/images/input-spinner.gif"/>',
	}).on('selected.tree-item', function(evt, data) {
		showPackage(data.info[0].additionalParameters.packageID);
	}).on('deselected.fu.tree', function(evt, data) {
		$('#showPackage tbody').remove
    	});
}

function showPackageLogs(logs) {
	$("#showLogs > tbody").empty();
	logs.forEach(function (log) {
		var date = getDateTime(log.ts);
		var number = log.number;
		delete log._id;
		delete log.ts;
		delete log.number;
		$("#showLogs > tbody:last-child").append("<TR>" +
			"<TD>" + date + "</TD>" +
			"<TD>" + number + "</TD>" +
			"<TD>" + JSON.stringify(log) + "</TD>" +
		"</TR>");
	});
}
