var data;
var selectedCustomerRow;
var customersTableRelations;

function loadExternalCustomers() {
    $(".username")[0].innerHTML = getCookie("cmsuser");

    clearTable();
    loadCustomer();
    $("#addExternalCustomers").click(function () {
        if (!data || data.length == 0) {
            return showToast("danger", "No data to add");
        }
        var partner = $(".partner").val();
        if (!partner || partner === "") {
            return showToast("danger", "Partner is mandatory");
        }
        addCustomers(data, partner);
    });
}

function loadCustomer() {
    customersTableRelations = $("#showCustomers").DataTable({
        "iDisplayLength": 10,
        "sAjaxSource": "/api/1/web/partners/customers",
        "bProcessing": true,
        "bLengthChange": true,
        "bFilter": true,
        "bServerSide": true,
        "bAutoWidth": false,
        "bDestroy": true,
        "aoColumns": [
            {"sTitle": "Id", "sWidth": "8%"},
            {"sTitle": "Partner", "sWidth": "12%"},
            {"sTitle": "Code", "sWidth": "12%"},
            {"sTitle": "Usage", "sWidth": "8%"},
            {"sTitle": "Email", "sWidth": "14%"},
            {"sTitle": "Number", "sWidth": "12%"},
            {"sTitle": "NRIC", "sWidth": "12%"},
            {"sTitle": ""},
            {"sTitle": "Delete", "sWidth": "8%"},
        ],
        "fnServerData": function (sSource, aaData, fnCallback) {
            $.ajax({
                "type": "GET",
                "dataType": "json",
                "url": sSource,
                "data": aaData,
                "success": function (response) {
                    var tableViewData = {
                        "iTotalRecords": response.iTotalRecords,
                        "iTotalDisplayRecords": response.iTotalDisplayRecords,
                        "aaData": []
                    };

                    response.data.forEach(function (item) {
                        var deleteButton = (response.write) ? "<button type='button' class='btn btn-white btn-sm' " +
                        "style='border-style:none;'  onClick='deleteCustomer(\"" + item.id + "\");'>" +
                        "<i class='fa fa-trash-o'></i></button>" : "";

                        var usedCount = 0;
                        if (item.usageCountL1) {
                            usedCount += parseInt(item.usageCountL1);
                        }
                        if (item.usageCountL2) {
                            usedCount += parseInt(item.usageCountL2);
                        }
                        if (item.usageCountL3) {
                            usedCount += parseInt(item.usageCountL3);
                        }

                        var codes = "";
                        if (item.promoCode) {
                            codes += getStatusLabel("NEUTRAL", item.promoCode);
                        }
                        if (item.promoCodeL2) {
                            codes += getStatusLabel("NEUTRAL", item.promoCodeL2);
                        }
                        if (item.promoCodeL3) {
                            codes += getStatusLabel("NEUTRAL", item.promoCodeL3);
                        }

                        tableViewData.aaData.push([
                            getText(item.id),
                            getText(item.source),
                            getText(codes),
                            getText(usedCount + ""),
                            getText(item.email),
                            getText(item.number),
                            getText(item.nric),
                            "",
                            deleteButton]);
                    });
                    fnCallback(tableViewData);
                },
                "error": function (xhr, status, error) {
                    showToast("danger", "Loading error");
                }
            });
        },
        "fnInitComplete": function (oSettings, json) {
            $("#spinContainer").hide();
            $("#archivesContainer").show();
        },
        "fnCreatedRow": function (nRow, aData, iDataIndex) {
        },
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            $(nRow).find('td').each(function (column, td) {
                if (column < 3) {
                    $(td).attr('data-id', aData[0]);
                }
            });

            // Row click
            $(nRow).on('click', function () {
            });

            // Cell click
            $('td', nRow).on('click', function () {
                selectedCustomerRow = nRow;
                if ($(this).attr("data-id")) {
                    var id = $(this).attr("data-id");
                }
            });
        }
    });
}

function deleteCustomer(id) {
    $(".username")[0].innerHTML = getCookie("cmsuser");
    BootstrapDialog.show({
        title: 'Delete external customer',
        message: 'Do you want to delete a customer with id ' + id + '?',
        buttons: [{
            label: 'Delete',
            action: function (dialog) {
                dialog.close();
                ajax("DELETE", "/api/1/web/partners/customers/delete/" + id, {}, true, function (data) {
                    showToast("success", "Customer has been removed");
                    customersTableRelations.fnDeleteRow(selectedCustomerRow);
                }, function () {
                    // do nothing
                }, showToast);
            }
        }, {
            label: 'Cancel',
            action: function (dialog) {
                dialog.close();
            }
        }]
    });
}

function tableifyPartnerUsers() {
    data = tableify($("#addText"), $("#addTable"), ["nric"]);
    $("#tableify").addClass("disabled");
    $("#addExternalCustomers").removeClass("disabled");
}

function clearTable() {
    data = undefined;
    $("#addText").val("");
    $("#addText").show();
    $("#addTable").empty();
    $("#tableify").removeClass("disabled");
    $("#addExternalCustomers").addClass("disabled");
}

function addCustomers(data, partner) {
    $(".username")[0].innerHTML = getCookie("cmsuser");
    BootstrapDialog.show({
        title: 'Add external users',
        message: 'Do you want add ' + data.length + ' customers provided by ' + partner.toUpperCase() + '?',
        buttons: [{
            label: 'Add',
            action: function (dialog) {
                dialog.close();
                ajax("PUT", "/api/1/web/partners/customers/add/", {
                    data: JSON.stringify(data),
                    partner: partner
                }, true, function (data) {
                    if (data.addedCount > 0 || data.skippedCount > 0) {
                        if (data.addedCount > 0) showToast("success", "Added " + data.addedCount + " customer(s)");
                        if (data.skippedCount > 0) showToast("warning", "Ignored " + data.skippedCount + " customer(s)");
                    } else {
                        showToast("warning", "No actions were done");
                    }
                    clearTable();
                    loadCustomer();
                }, function () {
                    // do nothing
                }, showToast);
            }
        }, {
            label: 'Cancel',
            action: function (dialog) {
                dialog.close();
            }
        }]
    });
}
