var mTableRelations;
var mTableWarningsRelations;
var mTableReportRelations;
var mContent = {};
var mContentWarnings = {};
var mContentReports = {};
var mStatus = "ACTIVE";

var startDate;
var endDate;

function reloadCallback() {
    if (mTableRelations) {
        mTableRelations.fnDraw(false);
    }
    if (mTableWarningsRelations) {
        mTableWarningsRelations.fnDraw(false);
    }
    if (mTableReportRelations) {
        mTableReportRelations.fnDraw(false);
    }
}

function reloadRequestsTableData() {
    if (mTableRelations) {
        mTableRelations.fnDraw(false);
    }
}

function loadPortInRequests() {
    $("#logContainer").hide();
    $("#warningsContainer").hide();
    $("#spinContainer").show();

    DEFAULT_RELOAD_CALLBACK = reloadCallback;

    reloadReportData();
    reloadWarningsData();
    reloadData("portin");

    setTimeout(() => {
        $("#dateStartTable").datetimepicker();
        $("#dateEndTable").datetimepicker();
    }, 1000);
}

function loadPortOutRequests() {
    $("#logContainer").hide();
    $("#spinContainer").show();

    DEFAULT_RELOAD_CALLBACK = reloadCallback;

    reloadData("portout");

    setTimeout(() => {
        $("#dateStartTable").datetimepicker();
        $("#dateEndTable").datetimepicker();
    }, 1000);
}

function reloadReportData() {
    $(".username")[0].innerHTML = getCookie("cmsuser");

    var reportTeamType = $("#teamType").val();
    var pastDays = reportTeamType == 'MARKETING' ? 8 : 4;
    var futureDays = reportTeamType == 'MARKETING' ? 0 : 4;

    startDate = new Date(new Date().getTime() - pastDays * 24 * 60 * 60 * 1000).toISOString().split("T")[0];
    endDate = new Date(new Date().getTime() + futureDays * 24 * 60 * 60 * 1000).toISOString().split("T")[0];

    var columns = [];
    columns.push({"sTitle": "Status", sWidth: "14%"});

    var currentDateColumn = new Date(startDate);
    var startDateColumn = new Date(new Date(startDate).getTime() + 24 * 60 * 60 * 1000);
    var endDateColumn = new Date(new Date(endDate).getTime() + 24 * 60 * 60 * 1000);

    while (currentDateColumn.getTime() < endDateColumn.getTime()) {
        columns.push({"sTitle": currentDateColumn.toISOString().split("T")[0]});
        currentDateColumn = new Date(currentDateColumn.getTime() + 24 * 60 * 60 * 1000);
    }

    columns.forEach(function (item) {
        if (!item.sWidth) {
            item.sWidth = ((100 - 14) / (columns.length - 1)) + "%";
        }
    });

    mTableReportRelations = $("#showReport").DataTable({
        "iDisplayLength": 10,
        "sAjaxSource": "/api/1/web/portin/requests/report/statuses/",
        "bFilter": false,
        "bAutoWidth": false,
        "bProcessing": true,
        "bServerSide": true,
        "aoColumns": columns,
        "bLengthChange": false,
        "bPaginate": false,
        "bInfo": false,
        "bDestroy": true,
        "bSort": false,
        "aaSorting": [[0, 'desc']],
        "fnServerData": function (sSource, aaData, fnCallback) {
            aaData.push({name: "startDate", value: startDateColumn.toISOString().split("T")[0]});
            aaData.push({name: "endDate", value: endDateColumn.toISOString().split("T")[0]});

            var reportDonorCode = $("#reportType").val();
            var reportTeamType = $("#teamType").val();
            var reportSource = $("#reportSource").val();
            var uniqueType = $("#uniqueType").val();
            var reportErrorGrouped = $("#reportErrorGrouped").val();
            aaData.push({name: "reportDonorCode", value: reportDonorCode});
            aaData.push({name: "reportSource", value: reportSource});
            aaData.push({name: "uniqueType", value: uniqueType});
            aaData.push({name: "teamType", value: reportTeamType});

            $.ajax({
                "type": "GET",
                "dataType": "json",
                "url": sSource,
                "data": aaData,
                "success": function (response) {
                    var tableViewData = {
                        "iTotalRecords": response.iTotalRecords,
                        "iTotalDisplayRecords": response.iTotalDisplayRecords,
                        "aaData": []
                    };

                    if (response.data) {
                        var columnLabels = response.columnLabels;
                        var columnKeys = response.columnKeys;
                        var pastKeys = response.pastKeys;
                        var futureKeys = response.futureKeys;
                        var todaysKeys = response.todaysKeys;

                        showPieCharts("total_requests", "All", response.data);
                        var handleValue = function (item) {

                            if (item.status == "FAILED") {
                                if (reportTeamType != "MARKETING") showPieCharts("failed_requests", "Failures", item.errors);
                                if (reportErrorGrouped == "FAILURES_NOT_GROUPED") {
                                    if (response.errors) response.errors.forEach(handleValue);
                                    return;
                                }
                            }
                            if (item.status == "ORDER_SOURCE") {
                                if (reportTeamType == "MARKETING") showPieCharts("failed_requests", "Order Source", response.orderSources);
                                if (response.orderSources) response.orderSources.forEach(handleValue);
                                return;
                            }

                            var values = [];
                            var status = item.status;
                            var subStatus;

                            var sourceStatuses = status.split(" / ");
                            if (sourceStatuses.length == 2) {
                                status = (sourceStatuses[0].length > 12 ? sourceStatuses[0].substr(0, 12) + "..." : sourceStatuses[0])
                                subStatus = (sourceStatuses[1].length > 12 ? sourceStatuses[1].substr(0, 12) + "..." : sourceStatuses[1])
                            }

                            values.push(status ? ((subStatus ? getStatusLabel("NEUTRAL", status) : getStatusLabel(status))
                            + (subStatus ? getStatusLabel("NEUTRAL", subStatus) : "")): "");

                            for (var i = 0; i < columns.length - 1; i++) {
                                values.push("");
                            }

                            if (status) columnKeys.forEach((key) => {
                                var value = item[key] ? parseInt(item[key]) : 0;
                                if (pastKeys.indexOf(key) >= 0) {
                                    if ((value > 0 && (status != "FAILED" && status != "CANCELED" && status != "DONE"))) {
                                        value = getStatusLabel((reportTeamType == "MARKETING" ? "NONE" : "ERROR"), value + "");
                                    } else {
                                        value = getStatusLabel("NONE", value + "");
                                    }
                                } else if (futureKeys.indexOf(key) >= 0) {
                                    if ((value > 0 && (status == "WAITING_APPROVE"))) {
                                        value = getStatusLabel((reportTeamType == "MARKETING" ? "NONE" : "WARNING"), value + "");
                                    } else {
                                        value = getStatusLabel("NONE", value + "");
                                    }
                                } else {
                                    if ((value > 0 && (status == "WAITING" || status == "WAITING_APPROVE"
                                        || status == "WAITING_RETRY" || status == "IN_PROGRESS"))) {
                                        value = getStatusLabel((reportTeamType == "MARKETING" ? "NONE" : "WARNING"), value + "");
                                    } else {
                                        value = getStatusLabel("NONE", value + "");
                                    }
                                }

                                values[columnKeys.indexOf(key) + 1] = value;
                            });

                            tableViewData.aaData.push(values);
                        }

                        response.data.forEach(handleValue);
                    }
                    fnCallback(tableViewData);
                },
                "error": function (xhr, status, error) {
                    $("#spinContainer").hide();
                    showToast("danger", "Loading error (" + JSON.stringify(error) + ")");
                }
            });
        },
        "fnInitComplete": function (oSettings, json) {
            $("#spinContainer").hide();
            $("#reportContainer").show();
        },
        "fnCreatedRow": function (nRow, aData, iDataIndex) {
        },
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            // Cell click
            $('td', nRow).on('click', function () {
            });
        }
    });
}

function showPieCharts(id, name, data) {
    var chartData = [];
    var totalCount = 0
    data.forEach((item) => {
        totalCount += item.count > 0 ? item.count : 0;
    });
    data.forEach((item) => {
        if (item.status != "ORDER_SOURCE" && item.status) {
            chartData.push({
                name: item.status ? item.status.replace('ERROR_', '').replace(/_/g, ' ')
                + " (" + (item.count > 0 ? item.count : 0) + ")" : undefined,
                y: item.count > 0 ? (item.count / totalCount) * 100 : 0,
                color: item.status ? getColorForStatus(item.status) : undefined
            });
        }
    });

    Highcharts.chart(id, {
        chart: {type: 'pie'},
        title: {
            text: name.toUpperCase(),
            style: {
                color: '#767676',
                fontWeight: 'bold',
                fontSize: '14px',
                fontFamily: 'Open Sans'
            }
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        legend: {
            itemStyle: {
                color: '#767676',
                fontWeight: 'bold',
                fontSize: '10px',
                fontFamily: 'Open Sans'
            }
        },
        exporting: {
            enabled: false
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        },
        series: [{
            name: 'Brands',
            colorByPoint: true,
            data: chartData
        }]
    });

}


function reloadWarningsData() {
    $(".username")[0].innerHTML = getCookie("cmsuser");
    $("#sectionWarningsHeader")[0].innerHTML = "Warnings";

    var columns = [];
    columns.push({"sTitle": "", sWidth: "0%"});
    columns.push({"sTitle": "Id", sWidth: "6%"});
    columns.push({"sTitle": "Update Date", sWidth: "10%"});
    columns.push({"sTitle": "Start Date", sWidth: "10%"});
    columns.push({"sTitle": "Status", sWidth: "12%"});
    columns.push({"sTitle": "Service No.", sWidth: "10%"});
    columns.push({"sTitle": "Number", sWidth: "10%"});
    columns.push({"sTitle": "Warning Date", sWidth: "10%"});
    columns.push({"sTitle": "Warning Status"});
    columns.push({"sTitle": "Edit", sWidth: "10%"});

    mTableWarningsRelations = $("#showWarnings").DataTable({
        "iDisplayLength": 10,
        "sAjaxSource": "/api/1/web/portin/requests/warnings/" + mStatus + "/",
        "bFilter": false,
        "bAutoWidth": false,
        "bProcessing": true,
        "bServerSide": true,
        "aoColumns": columns,
        "bLengthChange": false,
        "aoColumnDefs": [{"bSortable": false, "aTargets": [0, 4, 5, 6, 8]}],
        "bDestroy": true,
        "aaSorting": [[0, 'desc']],
        "fnServerData": function (sSource, aaData, fnCallback) {
            $.ajax({
                "type": "GET",
                "dataType": "json",
                "url": sSource,
                "data": aaData,
                "success": function (response) {
                    var tableViewData = {
                        "iTotalRecords": response.iTotalRecords,
                        "iTotalDisplayRecords": response.iTotalDisplayRecords,
                        "aaData": []
                    };

                    if (response.data) {
                        response.data.forEach(function (item) {
                            mContentWarnings["" + item.id] = item;
                            item.formId = "showWarnings";
                            var dates = getDates(item);
                            var params = getParams(item);

                            var values = [];
                            values.push("");
                            values.push(getText(item.id + ""));
                            values.push(dates.updateDate);
                            values.push(dates.startDate);
                            values.push(getPortInStatusLabel(item));
                            values.push(getCustomerAccountLink(item.service_instance_number));
                            values.push(getCustomerNumberLink(item.number)
                            + "<br>" + getText(item.donor_network) + " (" + getText(item.donor_network_code) + ")");
                            values.push(dates.warningDate);
                            values.push(getStatusLabel(item.warning_type));
                            values.push(getActionButtons(item, params));
                            tableViewData.aaData.push(values);
                        });
                    }
                    fnCallback(tableViewData);
                },
                "error": function (xhr, status, error) {
                    $("#spinContainer").hide();
                    showToast("danger", "Loading error (" + JSON.stringify(error) + ")");
                }
            });
        },
        "fnInitComplete": function (oSettings, json) {
            $("#spinContainer").hide();
            $("#warningsContainer").show();
        },
        "fnCreatedRow": function (nRow, aData, iDataIndex) {
        },
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            $(nRow).find('td').each(function (column, td) {
                if (column < 4) {
                    $(td).attr('data-id', aData[1]);
                }
            });

            // Row click
            $(nRow).on('click', function () {
            });

            // Cell click
            $('td', nRow).on('click', function () {
                if ($(this).attr("data-id")) {
                    var id = $(this).attr("data-id");
                    if (id) {
                        loadLogsDialog({
                            filterType: "REQUEST_ID",
                            matchType: "EXACT",
                            searchString: id,
                            period: "ALL"
                        }, 'logs_portin');
                    }
                }
            });
        }
    });
}

function reloadData(type) {
    $(".username")[0].innerHTML = getCookie("cmsuser");
    if (type != "portout") $("#sectionHeader")[0].innerHTML = "PortIn Request";

    var columns = [];
    columns.push({"sTitle": "", sWidth: "0%"});
    columns.push({"sTitle": "Id", sWidth: "6%"});
    columns.push({"sTitle": "Update Date", sWidth: "9%"});
    columns.push({"sTitle": "Start Date", sWidth: "9%"});
    columns.push({"sTitle": "Status", sWidth: "11%"});
    columns.push({"sTitle": "Service No.", sWidth: "9%"});
    columns.push({"sTitle": "Number", sWidth: "9%"});
    columns.push({"sTitle": "Plan", sWidth: "8%"});
    if (type != "portout") {
        columns.push({"sTitle": "Temp", sWidth: "9%"});
        columns.push({"sTitle": "Source", sWidth: "9%"});
        columns.push({"sTitle": "Docs Update"});
        columns.push({"sTitle": "Edit", sWidth: "9%"});
    } else {
        columns.push({"sTitle": "Warning Status", sWidth: "10%"});
    }

    mTableRelations = $("#showLogs").DataTable({
        "iDisplayLength": 6,
        "sAjaxSource": "/api/1/web/" + type + "/requests",
        "bFilter": true,
        "bAutoWidth": false,
        "bProcessing": true,
        "bServerSide": true,
        "aoColumns": columns,
        "bLengthChange": true,
        "aoColumnDefs": [{"bSortable": false, "aTargets": [0, 4, 5, 6]}],
        "bDestroy": true,
        "aaSorting": [[0, 'desc']],
        "fnServerData": function (sSource, aaData, fnCallback) {
            var statusTable = $("#statusTable").val();
            var typeTable = $("#typeTable").val();
            var sourceTable = $("#sourceTable").val();
            var uniqueTypeTable = $("#uniqueTypeTable").val();
            var dateStartTable = $("#dateStartTable").val();
            var dateEndTable = $("#dateEndTable").val();
            aaData.push({name: "status", value: statusTable});
            aaData.push({name: "donorCode", value: typeTable});
            aaData.push({name: "source", value: sourceTable});
            aaData.push({name: "uniqueType", value: uniqueTypeTable});
            aaData.push({name: "startDate", value: dateStartTable ? new Date(dateStartTable).toISOString() : undefined});
            aaData.push({name: "endDate", value: dateEndTable ? new Date(dateEndTable).toISOString() : undefined});

            $.ajax({
                "type": "GET",
                "dataType": "json",
                "url": sSource,
                "data": aaData,
                "success": function (response) {
                    var tableViewData = {
                        "iTotalRecords": response.iTotalRecords,
                        "iTotalDisplayRecords": response.iTotalDisplayRecords,
                        "aaData": []
                    };

                    if (response.data) {
                        response.data.forEach(function (item) {                            
                            mContent["" + item.id] = item;
                            var dates = getDates(item);
                            var params = getParams(item);

                            var values = [];
                            values.push("");
                            values.push(getText(item.id + ""));
                            values.push(dates.updateDate);
                            values.push(dates.startDate);
                            values.push(getPortInStatusLabel(item));
                            values.push(getCustomerAccountLink(item.service_instance_number));
                            values.push(getCustomerNumberLink(item.number)
                            + "<br>" + getText(item.donor_network) + " (" + getText(item.donor_network_code) + ")");
                            values.push(item.planName ? item.planName : "Not Switch");
                            if (type != "portout") {
                                values.push(item.temp_number ? getCustomerNumberLink(item.temp_number) + "<br>Circles.Life" : "N/A");
                                values.push(getText(item.source));
                                values.push(getStatusLabel(item.doc_update_status));
                                values.push(getActionButtons(item, params));
                            } else {
                                values.push(getStatusLabel(item.warning_type));
                            }
                            tableViewData.aaData.push(values);
                        });
                    }
                    fnCallback(tableViewData);
                },
                "error": function (xhr, status, error) {
                    $("#spinContainer").hide();
                    showToast("danger", "Loading error (" + JSON.stringify(error) + ")");
                }
            });
        },
        "fnInitComplete": function (oSettings, json) {
            $("#spinContainer").hide();
            $("#logContainer").show();
        },
        "fnCreatedRow": function (nRow, aData, iDataIndex) {
        },
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            $(nRow).find('td').each(function (column, td) {
                if (column < 4) {
                    $(td).attr('data-id', aData[1]);
                }
            });

            // Row click
            $(nRow).on('click', function () {
            });

            // Cell click
            $('td', nRow).on('click', function () {
                if ($(this).attr("data-id")) {
                    var id = $(this).attr("data-id");
                    if (id) {
                        //loadLogsDialog({
                        //    filterType: "REQUEST_ID",
                        //    matchType: "EXACT",
                        //    searchString: id,
                        //    period: "ALL"
                        //}, 'logs_portin');
                    }
                }
            });
        }
    });
}

function getParams(item) {
    return {
        id: item.id,
        portInPrefix: "65",
        portInNumber: item.number,
        serviceInstanceNumber: item.service_instance_number,
        fileIdBack: item.file_id_back,
        fileIdFront: item.file_id_front,
        fileAuthForm: item.file_auth_form,

        ownerName: item.owner_name,
        ownerIdType: item.owner_id_type,
        ownerIdNumber: item.owner_id_number,
        customerName: item.customer_name,
        customerIdType: item.customer_id_type,
        customerIdNumber: item.customer_id_number
    }
}


function onUploadGAReport() {
    generateConfirmationDialog("UPLOAD_PORTIN_GA_INFO_CSV", {}, reloadCallback);
}

function onDownloadList(type) {
    if (!type) type = "portin";

    var params = new Object;
    params.status = $("#statusTable").val(); 
    params.uniqueType = $("#typeTable").val();
    params.startDate = $("#dateStartTable").val();
    params.endDate = $("#dateEndTable").val();

    ajax("GET", "/api/1/web/" + type + "/requests", params, true, function (response) {
        if (response && response.data) {
            var dataString = "";
            response.data.forEach(function(item) {
                dataString += item.id + "," + item.status_update_ts + "," + item.start_ts + "," + item.status + "," +
                    item.service_instance_number + "," + item.number + "," + item.donor_network + "\n";
            });
            downloadCSV(dataString, "port_out_requests" + "_" + params.startDate + "-" + params.endDate
                + "_" + (new Date().toISOString()) + ".csv", "text/csv");
        }
    });
}

function onDownloadReport() {
    var form = {};
    form.title = 'Download Report';
    form.key = 'dialogForm';
    form.type = 'Create';
    form.options = [
        {
            display: 'Team', key: 'teamType', type: 'KeyValue', items: [
            {key: 'OPS', value: "OPS"},
            {key: 'MARKETING', value: "MARKETING"}
        ], value: "OPS", mandatory: false
        },
        {display: 'Start Date', key: 'startDate', type: 'Date', value: startDate, mandatory: false},
        {display: 'End Date', key: 'endDate', type: 'Date', value: endDate, mandatory: false},
        {
            display: 'Unique Type', key: 'uniqueType', type: 'KeyValue', items: [
            {key: 'CUSTOMER', value: "CUSTOMER"},
            {key: 'REQUEST', value: "REQUEST"}
        ], value: "CUSTOMER", mandatory: false
        },
        {display: 'Failure Reason', key: 'failureReason', type: 'Boolean', mandatory: false}
    ];

    createFormDialog(form, function (values, close, dialogRef, buttonRef, canceled) {
        if (canceled) {
            return;
        }

        ajax("GET", "/api/1/web/portin/requests/report/all/statuses/", values, true, function (response) {

            var dataString = "";

            response.data.forEach((tableGroup) => {
                dataString += tableGroup.type;
                response.columnLabels.forEach(function (column) {
                    dataString += " " + column;
                });
                dataString += ' __TOTAL__\n';
                tableGroup.data.forEach(function (item) {
                    if (item.status && item.status != "ORDER_SOURCE") {
                        dataString += item.status;
                        var total = 0;
                        response.columnKeys.forEach(function (key) {
                            if (item.status) total += parseInt(item[key]);
                            dataString += item.status ? " " + item[key] : " ";
                        });
                        dataString += item.status ? " " + total + '\n' : " \n";
                    }
                });
                dataString += '\n';

                if (values.failureReason) {
                    dataString += tableGroup.type + "_FAILURES";
                    response.columnLabels.forEach(function (column) {
                        dataString += " " + column;
                    });
                    dataString += ' __TOTAL__\n';
                    tableGroup.errors.forEach(function (item) {
                        dataString += item.status.replace(/ /g, '_');
                        var total = 0;
                        response.columnKeys.forEach(function (key) {
                            if (item.status) total += parseInt(item[key]);
                            dataString += item.status ? " " + item[key] : " ";
                        });
                        dataString += item.status ? " " + total + '\n' : " \n";
                    });
                    dataString += '\n';
                }

                if (values.teamType == 'MARKETING') {
                    dataString += tableGroup.type + "_ORDER_SOURCE";
                    response.columnLabels.forEach(function (column) {
                        dataString += " " + column;
                    });
                    dataString += ' __TOTAL__\n';
                    tableGroup.orderSources.forEach(function (item) {
                        dataString += item.status.replace(/ /g, '_');
                        var total = 0;
                        response.columnKeys.forEach(function (key) {
                            if (item.status && item[key]) total += parseInt(item[key]);
                            dataString += item.status ? " " + (item[key] ? item[key] : 0) : " ";
                        });
                        dataString += item.status ? " " + total + '\n' : " \n";
                    });
                    dataString += '\n';
                }
            });

            downloadCSV(dataString, "port_in_requests" + "_" + values.startDate + "-" + values.endDate
            + "_" + (new Date().toISOString()) + ".csv", "text/csv");

            close();
        }, function (code, errorMessage) {
        }, showToast);
    });
}

function getPortInStatusLabel(item) {
    return getStatusLabel(item.status) + (item.error_status ? " " + getStatusLabel(item.error_status) : "")
}

function getDates(item) {
    var result = {};
    result.updateDate = item.status_update_ts !== "0000-00-00 00:00:00" ?
        getDateTime(new Date(item.status_update_ts)).replace(" ", "<br>") : "N/A";
    result.startDate = item.start_ts !== "0000-00-00 00:00:00" ?
        getDateTime(new Date(item.start_ts)).replace(" ", "<br>") : "N/A";
    result.warningDate = item.warning_after_ts !== "0000-00-00 00:00:00" ?
        getDateTime(new Date(item.warning_after_ts)).replace(" ", "<br>") : "N/A";
    return result;
}

function getActionButtons(item, params) {
    var needDocUpdate = item.doc_update_status == "PENDING";
    var needRetry = item.status == "WAITING_RETRY";
    var needDocsPresented = (item.file_id_front);
    const formID = item.formId;    

    return getVerifyButton(formID, item, params) 
        + getTableActionButton("Status", "EDIT", "UPDATE_PORT_IN_STATUS", params)
        + getTableActionButton("Details", "VIEW", undefined, {id: item.id}, "viewAllDetails")
        + (needDocUpdate ? getTableActionButton("Documents", "EDIT", "UPDATE_PORT_IN_TEMP_NAME", params)
            : (needDocsPresented ? getTableActionButton("Documents", "VIEW", "VIEW_PORT_IN_DOCS", params) : ""))
        + (needRetry ? getTableActionButton("MNP", "EDIT", "RESEND_PORT_IN_MNP_NOTIFICATION", params) : "")
        + (needRetry ? getTableActionButton("Provision", "EDIT", "RESEND_PORT_IN_PROVISIONING_NOTIFICATION", params) : "")
        + (needRetry ? getTableActionButton("Timeout", "EDIT", "MOVE_PORT_IN_TO_IN_PROGRESS_STATUS", params) : "")
        + (item.start_ts == '2038-01-01T00:00:00.000Z' ? getTableActionButton("ACTIVATE", "EDIT", "ACTIVATE_PORT_IN",
            {id: item.id, service_instance_number: item.service_instance_number}) : "");
}

function getVerifyButton(id, item, params) {
    if (id === "showWarnings" && (item.status === "DONOR_APPROVED" || item.status === "IN_PROGRESS")) {
        return getTableActionButton("Verify", "VIEW", "VERIFY_PORT_IN_STATUS", params, "getActualPortInStatus");
    }
    return "";
}

function viewAllDetails(dialogId, paramsText) {
    var params = parseButtonParams(paramsText);
    if (params && params.id && mContent && mContent[params.id]) {
        BootstrapDialog.show({
            size: BootstrapDialog.SIZE_WIDE,
            title: "Port-In Request Details",
            message: JSON.stringify(mContent[params.id], null, 4).replace(new RegExp(' ', 'g'), "&nbsp&nbsp"),
            buttons: []
        });
    } else {
        showToast("danger", "Status is not found");
    }
}

function getActualPortInStatus(dialogId, paramsText) {
    const params = {id: JSON.parse(paramsText).id};
    const url = "/api/1/web/portin/verify/status";

    ajax("GET", url, params, true, (response) => {
      generateConfirmationDialog("VERIFY_PORT_IN_STATUS", response.data, reloadCallback)
    }, 
    (code, errorMessage) => {
      console.log("Error => ", errorMessage);
    });
}
