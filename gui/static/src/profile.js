function init() {
	$(".username")[0].innerHTML = getCookie("cmsuser");
	$(".username")[1].innerHTML = getCookie("cmsuser");
}

function updatePassword(form) {
	var password = form.password.value;
	form.password.disabled = true;
	$.ajax({
		url: "/api/1/web/profile/update/password",
		type: "PUT",
		dataType: "json",
		data: { "password" : password },
		success: function(data){
			if ( data.code == 0 ) {
				alert("Success! Please log in again.");
				location.reload();
			} else alert("Fail");
		},
		statusCode: {
			403: function(error){
				location.reload();
			}
		}
	});
	return false;
}
