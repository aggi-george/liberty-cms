var products = [];
var generalFree = [];
var categories = [];
var availablePartners = {};
var availableBonusTypes = {};
var availableSecondVerifTypes = {};
var selectedCategoryId;
var selectedCodeId;
var selectedCode;

var partnersItems = [];
var bonusDataTypesItems = [];
var secondVerifTypesItems = [];

var categoriesTableRelations;
var codesTableRelations;
var logsTableRelations;
var selectedCategoryRow;
var selectedCodeRow;
var selectedLogRow;

var categoriesMap = {};
var codesMap = {};
var logsMap = {};


function init() {
    $(".username")[0].innerHTML = getCookie("cmsuser");

    ajax("GET", "/api/1/web/promo/get/products", {}, true, function (data) {
        if (data.valid_bonuses) products = data.valid_bonuses;
        if (data.valid_free_general) generalFree = data.valid_free_general;
        if (data.valid_partners) availablePartners = data.valid_partners;
        if (data.valid_bonus_types) availableBonusTypes = data.valid_bonus_types;
        if (data.valid_second_verification_types) availableSecondVerifTypes = data.valid_second_verification_types;

        if (availablePartners) Object.keys(availablePartners).forEach((item) => {
            partnersItems.push({key: item, value: (availablePartners[item].configTitle
                ? availablePartners[item].configTitle : item.toUpperCase())})
        });

        if (availableBonusTypes) Object.keys(availableBonusTypes).forEach((item) => {
            bonusDataTypesItems.push({key: item, value: (availableBonusTypes[item].configTitle
                ? availableBonusTypes[item].configTitle : item.toUpperCase())})
        });

        if (availableSecondVerifTypes) availableSecondVerifTypes.forEach((item) => {
            secondVerifTypesItems.push({key: item, value: item.replace(/_/g, ' ')})
        });
    }, function () {
        // do nothing
    }, showToast);

    loadCategories();
}

function showCategoryEditDialog() {
    generateConfirmationDialog("EDIT_PROMO_CATEGORY", {partnersItems: partnersItems}, reloadCallback);
}

function showCodeCopyDialog() {
    generateConfirmationDialog("COPY_PROMO_CODE", {categoryId: selectedCategoryId}, reloadCallback);
}

function reloadCallback() {
    if ($("#categoriesContainer").is(":visible")) {
        categoriesTableRelations.fnDraw(false);
    }
    if ($("#codesContainer").is(":visible")) {
        codesTableRelations.fnDraw(false);
    }
    if ($("#codesLogsContainer").is(":visible")) {
        logsTableRelations.fnDraw(false);
    }
}

function loadCategories() {
    DEFAULT_RELOAD_CALLBACK = reloadCallback;
    selectedCategoryId = undefined;

    $(".username")[0].innerHTML = getCookie("cmsuser");
    $("#categoriesContainer").hide();
    $("#codesContainer").hide();
    $("#codesLogsContainer").hide();
    $("#spinContainer").show();

    var columns = [];
    columns.push({"sTitle": "Id", "sWidth": "5%"});
    columns.push({"sTitle": "Name / Type", "sWidth": "10%"});
    columns.push({"sTitle": "Start", "sWidth": "8%"});
    columns.push({"sTitle": "End", "sWidth": "8%"});
    columns.push({"sTitle": "Max L1 / L2 / L3 / Usage", "sWidth": "10%"});
    columns.push({"sTitle": "Enabled", "sWidth": "8%"});
    columns.push({"sTitle": "Message", "sWidth": "15%"});
    columns.push({"sTitle": "Action", "sWidth": "8%"});

    categoriesTableRelations = $("#showCategories").DataTable({
        "iDisplayLength": 10,
        "sAjaxSource": "/api/1/web/promo/get/categories",
        "bFilter": true,
        "bAutoWidth": false,
        "bProcessing": true,
        "bServerSide": true,
        "aoColumns": columns,
        "bLengthChange": true,
        "bSort": false,
        "bDestroy": true,
        "aaSorting": [[0, 'desc']],
        "aoColumnDefs": [{"bSortable": false, "aTargets": [0, 1, 2, 3, 4, 5, 6, 7]}],
        "fnServerData": function (sSource, aaData, fnCallback) {
            $.ajax({
                "type": "GET",
                "dataType": "json",
                "url": sSource,
                "data": aaData,
                "success": function (response) {
                    var tableViewData = {
                        "iTotalRecords": response.iTotalRecords,
                        "iTotalDisplayRecords": response.iTotalDisplayRecords,
                        "aaData": []
                    };

                    categories = response.data;
                    var webPartnersUrl = response.webPartnersUrl;

                    if (response.data) {
                        response.data.forEach(function (item) {
                            categoriesMap[item.id] = item;

                            var fileLink = item.image1
                                ? '<a target="_blank" href="' + item.image1 + '">Link</a>' : "";
                            var webLink = webPartnersUrl && item.registration_url_path
                                ? '<a target="_blank" href="' + webPartnersUrl + "/" + item.registration_url_path + '">Link</a>' : "";
                            var title = item.title && item.title.length > 20
                                ? item.title.substring(0, 17) + "..." : item.title;
                            var message = item.message && item.message.length > 20
                                ? item.message.substring(0, 17) + "..." : item.message;
                            var publicName = item.public_name && item.public_name.length > 20
                                ? item.public_name.substring(0, 17) + "..." : item.public_name;

                            var type = getStatusLabel("OK", item.type === "internal" ? "APP"
                                : (item.type === "partner" ? "E-COMMERCE" : (item.type === "internal_ecomm" ? "E-COMMERCE + APP": "UNKNOWN")));
                            var startDate = (item.start && item.start !== "0000-00-00 00:00:00"
                                ? getDateTime(item.start).replace(" ", "<br>") : "NONE");
                            var endDate = (item.end && item.end !== "0000-00-00 00:00:00"
                                ? getDateTime(item.end).replace(" ", "<br>") : "NONE");
                            var messageText = (title ? getStatusLabel("NEUTRAL", "TITLE: " + title) : "")
                                + (message ? getStatusLabel("NEUTRAL", "MESSAGE: " + message) : "")
                                + (fileLink ? getStatusLabel("NEUTRAL", "IMAGE: " + fileLink) : "")
                                + (publicName ? getStatusLabel("NEUTRAL", "E-TITLE: " + publicName) : "")
                                + (webLink ? getStatusLabel("NEUTRAL", "WEB: " + webLink) : "");

                            var params = {
                                id: item.id,
                                name: item.name,
                                type: item.type,
                                tag: item.tag,
                                partner: item.partner,
                                max: item.max,
                                maxL2: item.max_level_2,
                                maxL3: item.max_level_3,
                                multipleCodesAllowed: item.multiple_codes_allowed + "",
                                start: item.start,
                                end: item.end,
                                enabled: item.enabled,
                                title: item.title,
                                message: item.message,
                                image1: item.image1,
                                publicName: item.public_name,
                                webRegistration: item.enable_web_registration ? true : false,
                                codePrefix: item.code_prefix,
                                codeValidDays: "" + item.code_valid_days,
                                codePatternL1: item.code_pattern_level_1,
                                codePatternL2: item.code_pattern_level_2,
                                codePatternL3: item.code_pattern_level_3,
                                partnersItems: partnersItems,
                                customMNumber: item.customMNumber,
                                bankName: item.bankName
                            }

                            var webParams = {
                                id: item.id,
                                pathUrl: item.registration_url_path,
                                tacUrl: item.tac_url,
                                faqUrl: item.faq_url,
                                headerImageUrl: item.header_image_url,
                                footerImageUrl: item.footer_image_url,
                                backgroundImageUrl: item.background_image_url,
                                successImageUrl: item.success_image_url,
                                fieldPhone: item.field_phone,
                                fieldName: item.field_name,
                                fieldEmail: item.field_email,
                                fieldPassword: item.field_password,
                                fieldNric: item.field_nric,
                                fieldCorporateEmail: item.field_corporate_email,
                                registrationType: item.registration_type,
                                registrationButtonText: item.registration_button_text,
                                registrationButtonTextLoggedIn: item.registration_button_text_logged_in,
                                registrationOAuth20Url: item.registration_oauth20_url,
                                generateActivity: item.generate_activity,
                                usageActivity: item.usage_activity,
                                welcomeActivity: item.welcome_activity,
                                emailExtension: item.email_extension,
                                uniqueField: item.unique_field,
                                reminderActivity1: item.reminder_activity_1,
                                reminderActivity2: item.reminder_activity_2,
                                reminderDate1: item.reminder_date_1,
                                reminderDate2: item.reminder_date_2,
                                reminderOnDay1: item.reminder_on_day_1,
                                reminderOnDay2: item.reminder_on_day_2
                            }

                            var usageText = getStatusLabel("NEUTRAL", "L1: " + (item.max == -1 ? "U"
                                    : item.max == 0 ? "D" : item.max)) +
                                getStatusLabel("NEUTRAL", "L2: " + (item.max_level_2 == -1 ? "U"
                                    : (item.max_level_2 == 0 ? "D" : item.max_level_2))) +
                                getStatusLabel("NEUTRAL", "L3: " + (item.max_level_3 == -1 ? "U"
                                    : (item.max_level_3 == 0 ? "D" : item.max_level_3))) +
                                getStatusLabel("NEUTRAL", "USAGE: " + (item.multiple_codes_allowed ? "MULTIPLE" : "SINGLE"));

                            tableViewData.aaData.push([
                                getText(item.id),
                                getText(item.name) + "<br>" + type
                                + (item.tag ? "<br>" + getStatusLabel("PENDING", item.tag) : "")
                                + (item.partner ? "<br>" + getStatusLabel("PENDING", item.partner.toUpperCase()) : ""),
                                getText(startDate),
                                getText(endDate),
                                getText(usageText),
                                getStatusLabel(item.enabled == 1 ? "YES" : "NO"),
                                messageText,
                                getTableActionButton("Update", "EDIT", "EDIT_PROMO_CATEGORY", params) +
                                (item.enable_web_registration ? getTableActionButton("Web Page", "EDIT", "EDIT_PROMO_WEB_PAGE", webParams) : "") +
                                getTableActionButton("Remove", "REMOVE", "REMOVE_PROMO_CATEGORY", params)
                            ]);
                        });
                    }
                    fnCallback(tableViewData);
                },
                "error": function (xhr, status, error) {
                    showToast("danger", "Loading error");
                }
            });
        },
        "fnInitComplete": function (oSettings, json) {
            $("#spinContainer").hide();
            $("#categoriesContainer").show();
        },
        "fnCreatedRow": function (nRow, aData, iDataIndex) {
        },
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            $(nRow).find('td').each(function (column, td) {
                if (column < 10) {
                    $(td).attr('data-id', aData[0]);
                }
            });

            // Row click
            $(nRow).on('click', function () {
            });

            // Cell click
            $('td', nRow).on('click', function () {
                selectedCategoryRow = nRow;
                if ($(this).attr("data-id")) {
                    var id = $(this).attr("data-id");
                    if (id != selectedCategoryId) {
                        loadCodes(id);
                        loadLogs(id);
                    } else {
                        if ($("#codesContainer").is(":visible")) {
                            codesTableRelations.fnDraw(false);
                        }
                        loadLogs(id);
                    }
                }
            });
        }
    });
}

function loadCodes(id) {
    if (!id) {
        return;
    }

    selectedCategoryId = id;
    $(".username")[0].innerHTML = getCookie("cmsuser");
    $("#codesLogsContainer").hide();
    $("#spinContainer").show();

    var category = categoriesMap[id];
    $("#codesHeader").html(category && category.name ? getStatusLabel("NEUTRAL", category.name) : "");

    codesTableRelations = $("#showCodes").DataTable({
        "iDisplayLength": 10,
        "sAjaxSource": "/api/1/web/promo/get/codes/" + id,
        "bFilter": true,
        "bAutoWidth": false,
        "bProcessing": true,
        "bServerSide": true,
        "bLengthChange": true,
        "aoColumns": [
            {"sTitle": "Id", "sWidth": "6%"},
            {"sTitle": "Code", "sWidth": "10%"},
            {"sTitle": "Product", "sWidth": "12%"},
            {"sTitle": "Waiver ₵", "sWidth": "7%"},
            {"sTitle": "Discount ₵", "sWidth": "7%"},
            {"sTitle": "Sku", "sWidth": "7%"},
            {"sTitle": "Start", "sWidth": "8%"},
            {"sTitle": "End", "sWidth": "8%"},
            {"sTitle": "Limit", "sWidth": "6%"},
            {"sTitle": "Used ", "sWidth": "6%"},
            {"sTitle": "Enabled", "sWidth": "6%"},
            {"sTitle": "Update", "sWidth": "5%"},
            {"sTitle": "Delete", "sWidth": "5%"},
        ],
        "aoColumnDefs": [{"bSortable": false, "aTargets": [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]}],
        "bDestroy": true,
        "aaSorting": [[0, 'desc']],
        "fnServerData": function (sSource, aaData, fnCallback) {
            $.ajax({
                "type": "GET",
                "dataType": "json",
                "url": sSource,
                "data": aaData,
                "success": function (response) {
                    var tableViewData = {
                        "iTotalRecords": response.iTotalRecords,
                        "iTotalDisplayRecords": response.iTotalDisplayRecords,
                        "aaData": []
                    };

                    if (response.data) {
                        response.data.forEach(function (item) {
                            codesMap[item.id] = item;

                            var updateButton = (response.write) ? "<button type='button' class='btn btn-white btn-sm' " +
                            "style='border-style:none;'  onClick='updateCode(\"" + item.id + "\");'>" +
                            "<i class='fa fa-pencil'></i></button>" : "";

                            var deleteButton = (response.write) ? "<button type='button' class='btn btn-white btn-sm' " +
                            "style='border-style:none;'  onClick='deleteCode(\"" + item.code + "\");'>" +
                            "<i class='fa fa-trash-o'></i></button>" : "";

                            var max = item.code && item.code.indexOf("#") > 0 ? "N/A"
                                : (item.max == -1 ? "UNLIMITED" : (item.max == 0 ? "DISABLED" : getText(item.max)));

                            tableViewData.aaData.push([
                                getText(item.id),
                                getText(item.code) + (item.secondVerificationType ?
                                "<br>" + getStatusLabel("NEUTRAL", item.secondVerificationType) : ""),
                                getProductText(item),
                                getWaiverText(item),
                                getDiscountText(item),
                                item.skuKey1 ? getText(item.skuKey1) : "NONE",
                                getDateText(item.start),
                                getDateText(item.end),
                                max,
                                item.usedCount ? getText(item.usedCount) : "0",
                                (item.enabled == 1 ? "YES" : "NO"),
                                updateButton,
                                deleteButton
                            ]);
                        });
                    }
                    fnCallback(tableViewData);
                },
                "error": function (xhr, status, error) {
                    showToast("danger", "Loading error");
                }
            });
        },
        "fnInitComplete": function (oSettings, json) {
            $("#spinContainer").hide();
            $("#codesContainer").show();
            $("#downloadCodesReport").click(function () {
                download(obj);
            });
        },
        "fnCreatedRow": function (nRow, aData, iDataIndex) {
        },
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            $(nRow).find('td').each(function (column, td) {
                if (column < 10) {
                    $(td).attr('data-id', aData[0]);
                }
            });

            // Row click
            $(nRow).on('click', function () {
            });

            // Cell click
            $('td', nRow).on('click', function () {
                selectedCodeRow = nRow;
                if ($(this).attr("data-id")) {
                    var id = $(this).attr("data-id");
                    if (id != selectedCodeId) {
                        loadLogs(selectedCategoryId, id);
                    } else {
                        if ($("#codesLogsContainer").is(":visible")) {
                            logsTableRelations.fnDraw(false);
                        }
                    }
                }
            });
        }
    });
}

function getProductText(item) {
    var product = findProductName(products, item.productId);
    var freeAddon1 = findProductName(generalFree, item.freeAddon1);
    var text = "";
    var pos = 1;
    var notEmpty = false;

    // add bonus text
    if (item.productId) {
        if (notEmpty) text += "<br>";
        notEmpty = true;

        text += (pos + ") ") + (product ? product.name : item.productId) +
        (item.monthsCount == -1 ? " / forever" : " / " + item.monthsCount + " month(s)"
        + (item.extraBillDay > 0 ? " (+1 after day " + item.extraBillDay + ")" : ""));
        pos++;
    }

    // add free addon text
    if (item.freeAddon1) {
        if (notEmpty) text += "<br>";
        notEmpty = true;

        text += (pos + ") ") + (freeAddon1 ? freeAddon1.name : item.freeAddon1) +
        (item.freeAddonMonths1 == -1 ? " / forever" : " / " + item.freeAddonMonths1 + " month(s)"
        + (item.extraBillDay > 0 ? " (+1 after day " + item.extraBillDay + ")" : ""));
        pos++;
    }

    // add general bonus data
    if (item.bonusData && parseInt(item.bonusData) > 0) {
        if (notEmpty) text += "<br>";
        notEmpty = true;

        var dataText;
        if (parseInt(item.bonusData) / 1024 >= 1020) {
            dataText = parseInt(item.bonusData) / 1024 / 1024 + "GB";
        } else {
            dataText = parseInt(item.bonusData) / 1024 + "MB";
        }

        var dataTypeText;
        if (item.bonusDataType) {
            if (availableBonusTypes[item.bonusDataType].configTitle) {
                dataTypeText = availableBonusTypes[item.bonusDataType].configTitle;
            } else {
                dataTypeText = "Bonus '" + item.bonusDataType + "'";
            }
        } else {
            dataTypeText = "NO_TYPE";
        }

        text += (pos + ") ") + dataTypeText + " " + dataText +
        (item.monthsCount == -1 ? " / forever" : " / " + item.monthsCount + " month(s)");
        pos++;
    }

    if (!notEmpty) {
        text += "NONE";
    }

    return text;
}

function getDiscountText(item) {
    var text = "";
    var pos = 1;
    var notEmpty = false;

    // add bonus text
    if (item.regDiscountCents) {
        if (notEmpty) text += "<br>";
        notEmpty = true;

        text += (pos + ") ") + item.regDiscountCents + " (R)";
        pos++;
    }

    // add free addon text
    if (item.deviceDiscountCents) {
        if (notEmpty) text += "<br>";
        notEmpty = true;

        text += (pos + ") ") + item.deviceDiscountCents + " (D)";
        pos++;
    }

    if (!notEmpty) {
        text += "0";
    }

    return text;
}

function getWaiverText(item) {
    return getText(item.waiverCents + "") + (item.waiverCents > 0
            ? (item.waiverMonths > 1 ? " / each mo. for " + getText(item.waiverMonths + "") + " mo." : " / once") : "");
}

function getDateText(date) {
    return date && date !== "0000-00-00 00:00:00" ? getDateTime(date.replace(" ", "<br>")) : "NONE";
}

function loadLogs(categoryId, id) {
    if (!id && !categoryId) {
        return;
    }

    $(".username")[0].innerHTML = getCookie("cmsuser");
    //$("#codesLogsContainer").hide();
    $("#spinContainer").show();

    var code = codesMap[id];
    selectedCodeId = id;
    selectedCode = code;

    var category = categoriesMap[categoryId];
    $("#logsHeader").html((category && category.name ? getStatusLabel("NEUTRAL", category.name) : "") + " " +
    (code && code.code ? getStatusLabel("NEUTRAL2", code.code) : ""));

    logsTableRelations = $("#showCodesLogs").DataTable({
        "iDisplayLength": 10,
        "sAjaxSource": "/api/1/web/promo/get/logs/" + categoryId + "/" + (!code ? "all" : code.code.replace("#", "%23")),
        "bFilter": true,
        "bAutoWidth": false,
        "bProcessing": true,
        "bServerSide": true,
        "bLengthChange": true,
        "aoColumns": [
            {"sTitle": "Id", "sWidth": "6%", "sSortable": false},
            {"sTitle": "Date", "sWidth": "8%"},
            {"sTitle": "Code", "sWidth": "10%"},
            {"sTitle": "Product", "sWidth": "12%"},
            {"sTitle": "Waiver ₵", "sWidth": "7%"},
            {"sTitle": "Status", "sWidth": "10%"},
            {"sTitle": "SI No.", "sWidth": "8%"},
            {"sTitle": "Order No."},
            {"sTitle": "Action", "sWidth": "8%"},
        ],
        "aoColumnDefs": [{"bSortable": false, "aTargets": [1, 2, 3, 4, 5, 6, 7, 8]}],
        "bDestroy": true,
        "aaSorting": [[0, 'desc']],
        "fnServerData": function (sSource, aaData, fnCallback) {
            $.ajax({
                "type": "GET",
                "dataType": "json",
                "url": sSource,
                "data": aaData,
                "success": function (response) {
                    var tableViewData = {
                        "iTotalRecords": response.iTotalRecords,
                        "iTotalDisplayRecords": response.iTotalDisplayRecords,
                        "aaData": []
                    };

                    if (response.data) {
                        response.data.forEach(function (item) {
                            logsMap[item.id] = item;

                            var params = {
                                id: item.id
                            };

                            var status = item.status == 0 ? getStatusLabel("IN_PROGRESS", "APPLYING")
                                : (item.status == 1 ? getStatusLabel("OK", "REDEEMED")
                                    : (2 ? getStatusLabel("WAITING", "PENDING") : getStatusLabel("FAILED", "NOT ADDED")));
                            var date = item.usedDate !== "0000-00-00 00:00:00" ?
                                getDateTime(new Date(item.usedDate)).replace(" ", "<br>") : "N/A";

                            tableViewData.aaData.push([
                                getText(item.id),
                                date,
                                getText(item.code),
                                getProductText(item),
                                getWaiverText(item),
                                status,
                                (item.serviceInstanceNumber ? getCustomerAccountLink(item.serviceInstanceNumber) : "N/A"),
                                (item.orderReferenceNumber ? getText(item.orderReferenceNumber) : "N/A"),
                                getTableActionButton("Remove", "REMOVE", "REMOVE_PROMO_CODE_LOG", params)
                            ]);
                        });
                    }
                    fnCallback(tableViewData);
                },
                "error": function (xhr, status, error) {
                    showToast("danger", "Loading error");
                }
            });
        },
        "fnInitComplete": function (oSettings, json) {
            $("#spinContainer").hide();
            $("#codesLogsContainer").show();
            //categoryHeader
        },
        "fnCreatedRow": function (nRow, aData, iDataIndex) {
        },
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            // Cell click
            $('td', nRow).on('click', function () {
                selectedLogRow = nRow;
            });
        }
    });
}

function deleteCode(code) {
    $(".username")[0].innerHTML = getCookie("cmsuser");
    BootstrapDialog.show({
        title: 'Delete promo code',
        message: 'Do you want to delete code ' + code + '?',
        buttons: [{
            label: 'Delete',
            action: function (dialog) {
                dialog.close();
                ajax("DELETE", "/api/1/web/promo/delete/code/", {code: code}, true, function (data) {
                    showToast("success", "Code has been removed");
                    reloadCallback();
                }, function () {
                    // do nothing
                }, showToast);
            }
        }, {
            label: 'Cancel',
            action: function (dialog) {
                dialog.close();
            }
        }]
    });
}

function findProductName(products, id) {
    var found;
    products.forEach(function (item) {
        if (item.id === id) {
            found = item;
            return;
        }
    });
    return found;
}

function updateCode(id, callback) {
    var item = codesMap[id];
    if (!item) {
        return;
    }
    showPromoCodeEditDialog(id, item.code, item.category, item.productId, item.bonusData, item.bonusDataType, item.max,
        item.waiverCents, item.waiverMonths, item.regDiscountCents, item.deviceDiscountCents, item.monthsCount,
        item.skuName1, item.skuKey1, item.skuQuantity1, item.freeAddon1, item.freeAddonMonths1, item.extraBillDay,
        item.secondVerificationType, item.start, item.end, item.enabled, item.promoCodeType, callback);
}

function showPromoCodeEditDialog(id, code, category, productId,  bonusData, bonusDataType, maxLimit, waiverCents, waiverMonths,
                                 regDiscountCents, deviceDiscountCents, monthsCount, skuName1, skuKey1, skuQuantity1, freeAddon1,
                                 freeAddonMonths1, extraBillDay, secondVerificationType, startDate, endDate, enabled, promoCodeType, callback) {
    var productsItems = [];
    products.forEach(function (item) {
        productsItems.push({key: item.id, value: item.name});
    });

    var generalFreeItems = [];
    generalFree.forEach(function (item) {
        generalFreeItems.push({key: item.id, value: item.name});
    });

    var categoriesItems = [];
    categories.forEach(function (item) {
        categoriesItems.push({key: item.id, value: item.name});
    });

    var monthsItems = [
        {key: '-1', value: "FOREVER"},
        {key: '1', value: "1 MONTH"},
        {key: '2', value: "2 MONTHS"},
        {key: '3', value: "3 MONTHS"},
        {key: '4', value: "4 MONTHS"},
        {key: '5', value: "5 MONTHS"},
        {key: '6', value: "6 MONTHS"},
        {key: '7', value: "7 MONTHS"},
        {key: '8', value: "8 MONTHS"},
        {key: '9', value: "9 MONTHS"},
        {key: '10', value: "10 MONTHS"},
        {key: '11', value: "11 MONTHS"},
        {key: '12', value: "12 MONTHS"}
    ];

    var waiverCount = [
        {key: '1', value: "SINGLE"},
        {key: '2', value: "EACH MO. FOR 2 MO."},
        {key: '3', value: "EACH MO. FOR 3 MO."},
        {key: '4', value: "EACH MO. FOR 4 MO."},
        {key: '5', value: "EACH MO. FOR 5 MO."},
        {key: '6', value: "EACH MO. FOR 6 MO."}
    ];

    var bonusDataItems = [
        {key: '512000', value: "500 MB"},
        {key: '1048576', value: "1 GB"},
        {key: '1572864', value: "1.5 GB"},
        {key: '2097152', value: "2 GB"},
        {key: '20971520', value: "20 GB"}
    ];

    var promoCodeTypes = [
        {key: 'PORT_IN', value: "Port In"},
        {key: 'DEFAULT', value: "Default"}
    ];

    var options = [];
    if (!id) options.push({
        display: 'Category',
        key: 'category',
        value: category,
        type: 'KeyValue',
        items: categoriesItems,
        mandatory: true
    });

    options.push({display: 'Code', key: 'code', value: code, type: 'String', mandatory: true});
    options.push({display: 'Type', key: 'type', value: promoCodeType, type: 'KeyValue', items: promoCodeTypes});
    options.push({display: 'Double Verif.', key: 'secondVerificationType', value: secondVerificationType, type: 'KeyValue', items: secondVerifTypesItems});
    options.push({display: 'Product', key: 'productId', value: productId, type: 'KeyValue', items: productsItems});
    options.push({display: 'Bonus Data', key: 'bonusData', value: "" + bonusData, type: 'KeyValue', items: bonusDataItems});
    options.push({display: 'Bonus Type', key: 'bonusDataType', value: bonusDataType, type: 'KeyValue', items: bonusDataTypesItems});
    options.push({display: 'Bonus Duration', key: 'monthsCount', value: "" + monthsCount, type: 'KeyValue', items: monthsItems});
    options.push({display: 'Max Limit', key: 'max', value: maxLimit, type: 'Integer', mandatory: true});
    options.push({display: 'Waiver', key: 'waiverCents', value: waiverCents, type: 'Integer'});
    options.push({display: 'Waiver Count', key: 'waiverMonths', value: waiverMonths + "", type: 'KeyValue', items: waiverCount});
    options.push({display: 'Reg. Discount', key: 'regDiscountCents', value: regDiscountCents, type: 'Integer'});
    options.push({display: 'Device Discount', key: 'deviceDiscountCents', value: deviceDiscountCents, type: 'Integer'});
    options.push({display: 'Sku Name', key: 'skuName1', value: skuName1, type: 'String'});
    options.push({display: 'Sku Key', key: 'skuKey1', value: skuKey1, type: 'String'});
    options.push({display: 'Sku Quantity', key: 'skuQuantity1', value: skuQuantity1, type: 'Integer'});
    options.push({display: 'Free Addon', key: 'freeAddon1', value: freeAddon1, type: 'KeyValue', items: generalFreeItems});
    options.push({display: 'F. Addon Duration', key: 'freeAddonMonths1', value: "" + freeAddonMonths1, type: 'KeyValue', items: monthsItems});
    options.push({display: 'Extra Month After', key: 'extraBillDay', value: extraBillDay, type: 'Integer'});
    if (id) options.push({display: 'Start', key: 'start', value: startDate, type: 'DateTime'});
    if (id) options.push({display: 'End', key: 'end', value: endDate, type: 'DateTime'});
    options.push({display: 'Enabled', key: 'enabled', value: enabled, type: 'Boolean', mandatory: true});

    var params = {
        title: 'Promo Code',
        key: 'promoForm',
        type: id ? 'Update' : 'Create',
        options: options
    };

    createFormDialog(params, function (values, close) {
        if (!values && !close) {
            return;
        }

        if (id) {
            values.id = id;
            values.category = category;
        }
        ajax("PUT", "/api/1/web/promo/add/code",
            values, true, function (data) {
                close();
                showToast("success", "Promo code was " + (id ? "updated" : "created"));
                reloadCallback();
                if (callback) {
                    callback();
                }
            }, function () {
                // do nothing
            }, showToast);
    });
}

function downloadCodesReport(arr) {
    $.ajax({
        "type": "GET",
        "dataType": "json",
        "url": "/api/1/web/promo/get/codes/" + selectedCategoryId,
        "success": function (response) {
            var dataString = "Code ProductId BonusData BonusType ProductDuration " +
                "RegistrationDiscount DeviceDiscount Waiver WaiverDuration UsageLimit UsageCount \n";
            response.data.forEach(function (item) {
                dataString += item.code + " " + item.productId + " " + item.bonusData + " " + item.bonusDataType + " " + item.monthsCount + " "
                + item.regDiscountCents + " " + item.deviceDiscountCents + " " + item.waiverCents + " " + item.waiverMonths + " "
                + item.max + " " + item.usedCount + " " + "\n";
            });
            var category = categoriesMap[selectedCategoryId];
            downloadCSV(dataString, "promo_codes_" + "_" + category.id + "_" + category.name.replace(/ /g, "_")
            + "_" + (new Date().toISOString()) + ".csv", "text/csv");
        },
        "error": function (xhr, status, error) {
            showToast("danger", "Loading error");
        }
    });

}

function downloadCodeLogsReport(arr) {
    $.ajax({
        "type": "GET",
        "dataType": "json",
        "url": "/api/1/web/promo/get/logs/" + selectedCategoryId + "/"
        + (!selectedCode ? "all" : selectedCode.replace("#", "%23")),
        "success": function (response) {

            var dataString = "used_time code status product_id bonus_data bonus_type product_duration waiver waiver_duration sin orn \n";
            response.data.forEach(function (item) {
                var status = item.status == 1 ? "REDEEMED" : (2 ? "PENDING" : "FAILED_TO_REDEEM");

                dataString += item.usedDate.replace(' ', '_') + " " + item.code + " " + status + " "
                + item.productId + " " + item.bonusData + " " + item.bonusDataType + " " + item.monthsCount + " "
                + item.waiverCents + " " + item.waiverMonths + " "
                + (item.serviceInstanceNumber ? item.serviceInstanceNumber : "N/A") + " "
                + (item.orderReferenceNumber ? item.orderReferenceNumber : "N/A") + "\n";
            });
            var category = categoriesMap[selectedCategoryId];
            downloadCSV(dataString, "promo_logs_" + category.name.replace(/ /g, "_")
            + "_" + (new Date().toISOString()) + ".csv", "text/csv");
        },
        "error": function (xhr, status, error) {
            showToast("danger", "Loading error");
        }
    });

}