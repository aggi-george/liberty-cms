var dataSet;
var MAX = 4000000000;

function init() {
    $(".username")[0].innerHTML = getCookie("cmsuser");
}

function tableifyList() {
    dataSet = tableify($("#addText"), $("#addTable"), [ ]);
    if (!dataSet || dataSet.length == 0) return;
    $("#tableify").addClass("disabled");
    $("#addList").removeClass("disabled");
    var factor = dataSet.length > 1000 ? dataSet.length * 10000: dataSet.length * 10000;
    if (factor * dataSet.length > MAX) factor = parseInt(MAX / dataSet.length);
    $("#distFactor").val(factor);
    $("#addList").click(function () {
        $("#addList").unbind("click");
        $("#addList").addClass("disabled");
        $("#progress").css("width", "0%");
        runRaffle(function() {
        });
    });
}

function clearTable() {
    dataSet = undefined;
    $("#addText").val("");
    $("#addText").show();
    $("#addTable").empty();
    $("#tableify").removeClass("disabled");
    $("#addList").addClass("disabled");
    $("#addList").unbind("click");
    $("#showResult").empty();
    $("#progress").css("width","0%");
}

function runRaffle(callback) {
    var parameters = new Object;
    parameters.len = dataSet.length;
    parameters.distribute = parseInt($("#distFactor").val());
    parameters.count = $("#numWinners").val() && (parseInt($("#numWinners").val()) < dataSet.length) ?
            parseInt($("#numWinners").val()) :
            dataSet.length > 20 ? 20 : dataSet.length;
    $("#numWinners").val(parameters.count);
    ajax("POST", "/api/1/web/raffle", parameters, true, function (data) {
        callback();
        if (data && (data.code == 0)) {
            var checker = setInterval(function () {
                checkProgress(function (err) {
                    if (!err) clearInterval(checker); 
                });
            }, 1000);
        } else {
            showToast("warning", "Oops! Start failed");
        }
    });
}

function checkProgress(callback) {
    var parameters = new Object;
    ajax("GET", "/api/1/web/raffle", parameters, true, function (data) {
        if (data && (data.code == 0)) {
            if ( typeof(data.progress) != "undefined" ) {
                $("#progress").css("width", data.progress + "%");
                callback(true);
            } else {
                $("#showResult").empty();
                var headersHTML = "";
                var headers = new Array;
                Object.keys(dataSet[0]).forEach(function(key, idx) {
                    headersHTML += "<TH>" + key + "</TH>";
                    headers.push(key);
                });
                headersHTML += "<TH>COUNT</TH>";
                $("#showResult").append("<THEAD>" + headersHTML + "</THEAD>");
                data.result.forEach(function (item) {
                    var row = "";
                    headers.forEach(function (key) {
                        row += "<TD>" + dataSet[item.id][key] + "</TD>";
                    });
                    row += "<TD>" + item.count + "</TD>";
                    $("#showResult").append("<TR>" + row + "</TR>");
                });
                showToast("success", "Done");
                $("#progress").css("width", "100%");
                callback(false);
            }
        } else {
            showToast("warning", "Oops! Progress failed");
            callback(false);
        }
    });
}
