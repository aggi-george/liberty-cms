function init() {
	$(".username")[0].innerHTML = getCookie("cmsuser");
	$.ajax({
		url: "/api/1/web/sitecontrol/get/sitecontrol",
		type: "GET",
		dataType: "json",
		data: {},
		success: function(data){
			if ( data.code == 0 ) {
				data.list.forEach(function (site) {
					$("#showAll > tbody:last-child").append("<TR>" +
						"<TD>" + site.name + "</TD>" +
						"<TD>" + site.email + "</TD>" +
						"<TD>" + site.phone + "</TD>" +
						"<TD>" + site.end_date + "</TD>" +
						"<TD>" + site.message + "</TD>" +
					"<TR>");
				});
			} else alert("Fail");
		},
		statusCode: {
			403: function(error){
				location.reload();
			}
		}
	});
}
