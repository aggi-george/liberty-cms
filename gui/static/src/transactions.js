var selectedTransactionId;
var selectedTransactionRow;

var downloadPageLenth = 10000;
var transactionTableRelations;

var transactionMap = {};

function init() {
    $(".username")[0].innerHTML = getCookie("cmsuser");
    loadTransactions();
}

function reloadCallback() {
    if ($("#transactionContainer").is(":visible")) {
        transactionTableRelations.fnDraw(false);
    }
}

function reloadTransactions() {
    if ($("#transactionContainer").is(":visible")) {
        transactionTableRelations.fnDraw(false);
    }
}

function loadTransactions() {
    DEFAULT_RELOAD_CALLBACK = reloadCallback;
    $(".username")[0].innerHTML = getCookie("cmsuser");

    transactionTableRelations = $("#showTransactions").DataTable({
        "iDisplayLength": 10,
        "sAjaxSource": "/api/1/web/transactions/get",
        "bFilter": true,
        "bAutoWidth": false,
        "bProcessing": true,
        "bServerSide": true,
        "bLengthChange": true,
        "aoColumns": [
            {"sTitle": "Id", "sWidth": "6%"},
            {"sTitle": "Date", "sWidth": "8%"},
            {"sTitle": "Billing No.", "sWidth": "9%"},
            {"sTitle": "Amount", "sWidth": "6%"},
            {"sTitle": "Type / State", "sWidth": "13%"},
            {"sTitle": "Status", "sWidth": "13%"},
            {"sTitle": "Ext. ID", "sWidth": "13%"},
            {"sTitle": "Int. ID", "sWidth": "11%"},
            {"sTitle": "UUID"},
            {"sTitle": "Action", "sWidth": "8%"},
        ],
        "aoColumnDefs": [{"bSortable": false, "aTargets": [0, 1, 2, 3, 4, 5, 6, 7, 8]}],
        "bDestroy": true,
        "aaSorting": [[0, 'desc']],
        "fnServerData": function (sSource, aaData, fnCallback) {

            var type = $("#transactionType").val();
            var completion = $("#transactionCompletion").val();
            var startDate = new Date($("#startDate").val()).getTime();
            var endDate = new Date($("#endDate").val()).getTime();
            aaData.push({name: "type", value: type});
            aaData.push({name: "completion", value: completion});
            aaData.push({name: "startDate", value: startDate});
            aaData.push({name: "endDate", value: endDate});

            $.ajax({
                "type": "GET",
                "dataType": "json",
                "url": sSource,
                "data": aaData,
                "success": function (response) {
                    var tableViewData = {
                        "iTotalRecords": response.iTotalRecords,
                        "iTotalDisplayRecords": response.iTotalDisplayRecords,
                        "aaData": []
                    };

                    if (response.data) {
                        response.data.forEach(function (item) {
                            transactionMap[item.id] = item;
                            var params = {
                                id: item.id,
                                type: item.type,
                                billingAccountNumber: item.billingAccountNumber
                            }

                            var old = (new Date() - new Date(item.createdAt)) > 60 * 1000
                            var oldActive = item.uniqueKey && old;

                            tableViewData.aaData.push([
                                getText(item.id),
                                getDateText(item.createdAt, true),
                                (item.billingAccountNumber ? getCustomerAccountLink(getText(item.billingAccountNumber)) : "N/A") + "<br>",
                                getText(item.amount > 0 ? (item.amount / 100) : 0),
                                getStatusLabel("NEUTRAL", item.type) + (item.state ? getStatusLabel("NEUTRAL", item.state) : ""),
                                (item.uniqueKey ? getStatusLabel(old ? "ERROR" : "WARNING", old ? "UNFINISHED TR" : "ACTIVE TR") : "") +
                                getStatusLabel(item.status),
                                item.externalTransactionId ? "<a href='javascript:loadTransactionStatus(\"" + item.billingAccountNumber
                                + "\", \"" + item.externalTransactionId + "\", \"\");'>" + item.externalTransactionId + "</a>" : "",
                                getText(item.internalId),
                                item.uuid ? "<a href='javascript:loadTransactionStatus(\"" + item.billingAccountNumber + "\", \"\", \""
                                + item.uuid + "\");'>" + item.uuid + "</a>" : "",
                                (oldActive ? getTableActionButton("RECOVER", "REFRESH", "RECOVER_TRANSACTION", params) : "") +
                                getTableActionButton("VIEW", "VIEW", undefined, params, "viewTransactionLog")
                            ]);
                        });
                    }
                    fnCallback(tableViewData);
                    $('#startDate').datetimepicker();
                    $('#endDate').datetimepicker();
                },
                "error": function (xhr, status, error) {
                    showToast("danger", "Loading error");
                }
            });
        },
        "fnInitComplete": function (oSettings, json) {
            $("#spinContainer").hide();
            $("#transactionContainer").show();
        },
        "fnCreatedRow": function (nRow, aData, iDataIndex) {
        },
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            $(nRow).find('td').each(function (column, td) {
                if (column < 10) {
                    $(td).attr('data-id', aData[0]);
                }
            });

            // Row click
            $(nRow).on('click', function () {
            });

            // Cell click
            $('td', nRow).on('click', function () {
                selectedTransactionRow = nRow;
                if ($(this).attr("data-id")) {
                    var id = $(this).attr("data-id");
                }
            });
        }
    });
}

$("#startDate").change(()=>{
    loadTransactions();
})

$("#endDate").change(()=>{
    loadTransactions();
})

function getDateText(date, showTime) {
    return date && date !== "0000-00-00 00:00:00" ? (showTime ? getDateTime(date).replace(" ", "<br>")
        : getDateTime(date).split(" ")[0]) : "NONE";
}

function loadTransactionStatus(billingAccountNumber, orderRef, uuid) {
    var title = orderRef ? ("Order reference no. " + orderRef) : ("UUID " + uuid);
    var showProgressTimeout = setTimeout(function () {
        showToast("success", "Still loading transaction status for " + title + "...");
    }, 800)

    ajax("GET", "/api/1/web/transactions/status", {
        billingAccountNumber: billingAccountNumber,
        orderRef: orderRef,
        uuid: uuid
    }, true, function (data) {
        clearTimeout(showProgressTimeout);

        BootstrapDialog.show({
            size: BootstrapDialog.SIZE_WIDE,
            title: "Transaction Payment Gateway Status. " + title + ".",
            message: JSON.stringify(data, null, 4).replace(new RegExp(' ', 'g'), "&nbsp&nbsp"),
            buttons: []
        });
    }, function () {

    }, showToast);
}

function viewTransactionLog(dialogId, paramsText) {
    var params = parseButtonParams(paramsText);
    if (params && params.id && transactionMap && transactionMap[params.id]) {
        var copy = JSON.parse(JSON.stringify(transactionMap[params.id]))

        if (copy.metadata) {
            copy.metadata = JSON.parse(copy.metadata);
        }

        BootstrapDialog.show({
            size: BootstrapDialog.SIZE_WIDE,
            title: "Transaction Preview",
            message: JSON.stringify(copy, null, 4).replace(new RegExp(' ', 'g'), "&nbsp&nbsp"),
            buttons: []
        });
    } else {
        showToast("danger", "Transaction is not found");
    }
}

function downloadReport(offset){
    var aaData = [];
    currentOffset = offset ? offset : 0;
    if(!offset){
        showLoadingLabel();
    }
    var startDate = new Date($("#startDate").val()).getTime();
    var endDate = new Date($("#endDate").val()).getTime();
    aaData.push({name: "type", value: $("#transactionType").val()});
    aaData.push({name: "completion", value: $("#transactionCompletion").val()});
    aaData.push({name: "startDate", value: startDate});
    aaData.push({name: "endDate", value: endDate});
    aaData.push({name: "iDisplayLength", value: downloadPageLenth});
    aaData.push({name: "iDisplayStart", value: currentOffset});
    aaData.push({name: "sSearch", value: $("#showTransactions_filter input[type=text]").val()});

    $.ajax({
        "type": "GET",
        "dataType": "json",
        "url": "/api/1/web/transactions/get",
        "data": aaData,
        "success": function (response) {
            var bigList = generateReport(response.data);
            var fileName = $("#transactionType").val() + "_" + $("#transactionCompletion").val() + ($("#showTransactions_filter input[type=text]").val().length > 0 ? ("_" +  $("#showTransactions_filter input[type=text]").val()) : "");
            downloadCSV(bigList, fileName, "text/csv");
            if(response.data.length == downloadPageLenth){
                downloadReport(currentOffset + downloadPageLenth);
            }else{
                hideLoadingLabel();
            }
        },
        "error": function (xhr, status, error) {
            showToast("danger", "Failed to fetch report data");
        }
    });
}

function generateReport(records){
    var csv = "id,date,BAN,amount,type,state,status,externalId,internalId,uuid\n";
    records.forEach(function(item){
        var entry = [];
        var old = (new Date() - new Date(item.createdAt)) > 60 * 1000
        entry.push([
            getText(item.id),
            getDateText(item.createdAt, false),
            item.billingAccountNumber ? getText(item.billingAccountNumber) : "N/A",
            getText(item.amount > 0 ? (item.amount / 100) : 0),
            item.type ? item.type : "N/A",
            item.state ? item.state : "N/A",
            (item.uniqueKey ? (old ? "UNFINISHED TR" : "ACTIVE TR") : "N/A"),
            item.externalTransactionId ? item.externalTransactionId : "N/A",
            getText(item.internalId),
            item.uuid ? item.uuid : "N/A",
            "\n"
        ]);
        csv += entry.join(",");
    });
    return csv;
}