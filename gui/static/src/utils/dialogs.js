var paceDialogOption = {
    display: 'Pace', key: 'delay', type: 'KeyValue', items: [
        {key: '200', value: "1000 / 30 sec"},
        {key: '400', value: "1000 / 1 min"},
        {key: '2000', value: "1000 / 5 min"},
        {key: '4000', value: "1000 / 10 min"},
        {key: '8000', value: "1000 / 20 min"},
        {key: '16000', value: "1000 / 40 min"}
    ], mandatory: true
}

var confirmDialogContent = {
    'BONUS_UNSUBSCRIBE': {
        'TITLE_KEY': 'REMOVE_BONUS_OR_ADDON_TITLE',
        'MESSAGE_KEY': 'REMOVE_BONUS_OR_ADDON_MESSAGE',
        'API_ACTION': 'DELETE',
        'API_URL': '/api/1/web/customers/update/addon/selfcare/',
        'VALIDATE_LIST': ['number', 'prefix', 'historyId', 'product'],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': true,
        'FORM': {
            'titleKey': 'REMOVE_BONUS_OR_ADDON_TITLE',
            'idKey': 'bonusUnsubscOptions',
            'options': [{
                display: 'Effective', key: 'overrideEffect', type: 'KeyValue', mandatory: true,
                items: [
                    {key: '0', value: "Immediate"},
                    {key: '1', value: "Next Day"},
                    {key: '2', value: "Next Bill Cycle"}
                ]
            }]
        }
    },
    'ALIGN_BONUS_HISTORY': {
        'TITLE_KEY': 'BONUS_CONFIRM_TITLE',
        'MESSAGE_KEY': 'ALIGN_BONUS_CONFIRM_MESSAGE',
        'API_ACTION': 'PUT',
        'API_URL': '/api/1/web/customers/update/addon/resubscribe/',
        'VALIDATE_LIST': ['number', 'prefix', 'serviceInstanceNumber', 'account', 'current'],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': true
    },
    'ADDON_UPDATE': {
        'TITLE_KEY': 'ADDON_UPDATE_CONFIRM_TITLE',
        'MESSAGE_KEY': 'ADDON_UPDATE_CONFIRM_MESSAGE',
        'API_ACTION': 'PUT',
        'API_URL': '/api/1/web/customers/update/addon/selfcare/',
        'VALIDATE_LIST': ['number', 'prefix', 'serviceInstanceNumber', 'account', 'product', 'productName'],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': true,
        'FORM': {
            'titleKey': 'ADDON_UPDATE_CONFIRM_TITLE',
            'idKey': 'addonOptions',
            'options': [{
                display: 'Effective', key: 'overrideEffect', type: 'KeyValue', mandatory: true,
                items: [
                    {key: '0', value: "Immediate"},
                    {key: '1', value: "Next Day"},
                    {key: '2', value: "Next Bill Cycle"}
                ]
            }, {display: 'Payments Disabled', key: 'paymentsDisabled', type: 'Boolean'}, {
                display: 'Recurrent', key: 'overrideRecurrent', type: 'KeyValue', mandatory: true,
                items: [
                    {key: 'true', value: "Yes"},
                    {key: 'false', value: "No"}
                ]
            }]
        }
    },
    'ADDON_UNSUBSCRIBE': {
        'TITLE_KEY': 'ADDON_UNSUBSCRIBE_CONFIRM_TITLE',
        'MESSAGE_KEY': 'ADDON_UNSUBSCRIBE_CONFIRM_MESSAGE',
        'API_ACTION': 'DELETE',
        'API_URL': '/api/1/web/customers/update/addon/selfcare/',
        'VALIDATE_LIST': ['number', 'prefix', 'serviceInstanceNumber', 'account', 'product', 'productName'],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': true,
        'FORM': {
            'titleKey': 'ADDON_UNSUBSCRIBE_CONFIRM_TITLE',
            'idKey': 'addonUnsubscOptions',
            'options': [{
                display: 'Effective', key: 'overrideEffect', type: 'KeyValue', mandatory: true,
                items: [
                    {key: '0', value: "Immediate"},
                    {key: '1', value: "Next Day"},
                    {key: '2', value: "Next Bill Cycle"}
                ]
            }]
        }
    },
    'BLOCK_NOTIFICATIONS': {
        'TITLE_KEY': 'BLOCK_NOTIFICATIONS_CONFIRM_TITLE',
        'MESSAGE_KEY1': 'BLOCK_NOTIFICATIONS_CONFIRM_MESSAGE',
        'MESSAGE_KEY2': 'UNBLOCK_NOTIFICATIONS_CONFIRM_MESSAGE',
        'API_ACTION': 'PUT',
        'API_URL': '/api/1/web/customers/notifications/block/',
        'VALIDATE_LIST': ['prefix', 'number', 'serviceInstanceNumber', 'account', 'block'],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': true
    },
    'RESEND_FROM_ECOMM': {
        'TITLE_KEY': 'RESEND_FROM_ECOMM_TITLE',
        'MESSAGE_KEY': 'RESEND_FROM_ECOMM_MESSAGE',
        'API_ACTION': 'PUT',
        'API_URL': '/api/1/web/customers/ecomm/notification/',
        'VALIDATE_LIST': ['prefix', 'number', 'serviceInstanceNumber', 'type'],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': true
    },
    'ADD_CREDIT_NOTES': {
        'TITLE_KEY': 'ADD_CREDIT_NOTES_TITLE',
        'MESSAGE_KEY': 'ADD_CREDIT_NOTES_MESSAGE',
        'API_ACTION': 'PUT',
        'API_URL': '/api/1/web/customers/bill/add/credit/notes/',
        'VALIDATE_LIST': ['prefix', 'number', 'amount', 'reason'],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': true,
        'FORM': {
            'titleKey': 'ADD_CREDIT_NOTES_TITLE',
            'idKey': 'addCreditNotes',
            'options': [
                {display: 'Amount($)', key: 'amount', type: 'Integer', mandatory: true},
                {display: 'Reason', key: 'reason', type: 'String', mandatory: true},
            ]
        }
    },
    'PHONE_NUMBER_CHANGE': {
        'TITLE_KEY': 'CHANGE_PHONE_NUMBER_TITLE',
        'MESSAGE_KEY': 'CHANGE_PHONE_NUMBER_MESSAGE',
        'API_ACTION': 'PUT',
        'API_URL': '/api/1/web/customers/repair/',
        'VALIDATE_LIST': [],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': true,
        'FORM': {
            'titleKey': 'CHANGE_PHONE_NUMBER_TITLE',
            'idKey': 'changePhoneNumber',
            'options': [
                {display: 'Current Number', key: 'number', customKey: 'numberList', type: 'KeyValue', mandatory: true},
                {display: 'New Number', key: 'newNumber', type: 'String', mandatory: true}
            ]
        }
    },
    'SIM_CHANGE': {
        'TITLE_KEY': 'CHANGE_SIM_TITLE',
        'MESSAGE_KEY': 'CHANGE_SIM_MESSAGE',
        'API_ACTION': 'PUT',
        'API_URL': '/api/1/web/customers/repair/',
        'VALIDATE_LIST': [],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': true,
        'FORM': {
            'titleKey': 'CHANGE_SIM_TITLE',
            'idKey': 'changePhoneNumber',
            'options': [
                {display: 'Current Number', key: 'number', customKey: 'numberList', type: 'KeyValue', mandatory: true},
                {display: 'New ICCID', key: 'newIccid', type: 'String', mandatory: true},
                {
                    display: 'Charge', key: 'charge', type: 'KeyValue', defaultKey: 'Please Select', mandatory: true,
                    items: [
                        {key: 'true', value: "Yes"},
                        {key: 'false', value: "No"}
                    ]
                },
                {
                    display: 'Send Delivery Link', key: 'delivery', type: 'KeyValue', defaultKey: 'Please Select', mandatory: true,
                    items: [
                        {key: 'true', value: "Yes"},
                        {key: 'false', value: "No"}
                    ]
                }
            ]
        }
    },
    'CONFIRM_SIM_DAMAGE': {
        'TITLE_KEY': 'CHANGE_SIM_TITLE',
        'MESSAGE_KEY': 'CONFIRM_SIM_DAMAGE_MESSAGE',
        'API_ACTION': 'PUT',
        'API_URL': '/api/1/web/customers/simDamageConfirm/',
        'VALIDATE_LIST': ['serviceInstanceNumber']
    },
    'USE_BONUS': {
        'TITLE_KEY': 'BONUS_CONFIRM_TITLE',
        'MESSAGE_KEY': 'USE_BONUS_MESSAGE',
        'API_ACTION': 'PUT',
        'API_URL': '/api/1/web/customers/update/addon/use/bonus/',
        'VALIDATE_LIST': ['number', 'prefix', 'serviceInstanceNumber', 'account', 'id'],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': true
    },
    'PAY_NOW': {
        'TITLE_KEY': 'PAYMENT_CONFIRM_TITLE',
        'MESSAGE_KEY': 'PAYMENT_CONFIRM_MESSAGE',
        'API_ACTION': 'PUT',
        'API_URL': '/api/1/web/customers/bill/payment/',
        'VALIDATE_LIST': ['number', 'prefix', 'serviceInstanceNumber', 'account', 'outstanding', 'invoiceId', 'amount'],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': true
    },
    'BIRTHDAY_BONUS': {
        'TITLE_KEY': 'BONUS_CONFIRM_TITLE',
        'MESSAGE_KEY': 'BIRTHDAY_BONUS_CONFIRM_MESSAGE',
        'API_ACTION': 'PUT',
        'API_URL': '/api/1/web/customers/update/addon/add/bonus/birthday/',
        'VALIDATE_LIST': ['number', 'prefix', 'serviceInstanceNumber', 'account'],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': true
    },
    'WINBACK_BONUS': {
        'TITLE_KEY': 'BONUS_CONFIRM_TITLE',
        'MESSAGE_KEY': 'WINBACK_BONUS_CONFIRM_MESSAGE',
        'API_ACTION': 'PUT',
        'API_URL': '/api/1/web/customers/update/addon/add/bonus/winback/',
        'VALIDATE_LIST': ['number', 'prefix', 'serviceInstanceNumber', 'account'],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': true,
        'FORM': {
            'titleKey': 'BONUS_CONFIRM_TITLE',
            'idKey': 'id',
            'options': [{
                display: 'Amount', key: 'productId', type: 'KeyValue', mandatory: true,
                items: [
                    {key: 'PRD00553', value: "Bonus 100MB Surprise"},
                    {key: 'PRD00557', value: "Bonus 1GB Surprise"},
                    {key: 'PRD00554', value: "Bonus 250MB Surprise"},
                    {key: 'PRD00555', value: "Bonus 500MB Surprise"},
                    {key: 'PRD00556', value: "Bonus 750MB Surprise"}
                ]
            }]
        }
    },
    'UPDATE_ROAMING_CAP': {
        'TITLE_KEY': 'ROAMING_CAP_CONFIRM_TITLE',
        'MESSAGE_KEY': 'ROAMING_CAP_CONFIRM_MESSAGE',
        'API_ACTION': 'PUT',
        'API_URL': '/api/1/web/customers/update/cap/roaming/',
        'VALIDATE_LIST': ['number', 'prefix', 'serviceInstanceNumber', 'account', 'limit'],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': true
    },
    'UPDATE_ROAMING_SETTINGS': {
        'TITLE_KEY': 'ROAMING_SETTINGS_CONFIRM_TITLE',
        'MESSAGE_KEY': 'ROAMING_SETTINGS_CONFIRM_MESSAGE',
        'API_ACTION': 'PUT',
        'API_URL': '/api/1/web/customers/update/roaming/',
        'VALIDATE_LIST': ['number', 'prefix', 'serviceInstanceNumber', 'account', 'type'],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': true
    },
    'ADD_LOYALITY_BONUS': {
        'TITLE_KEY': 'BONUS_CONFIRM_TITLE',
        'MESSAGE_KEY': 'LOYALITY_BONUS_CONFIRM_MESSAGE',
        'API_ACTION': 'PUT',
        'API_URL': '/api/1/web/customers/update/addon/add/bonus/loyalty/',
        'VALIDATE_LIST': ['number', 'prefix', 'serviceInstanceNumber', 'account'],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': true
    },
    'ADD_CONTRACT_BUSTER_BONUS': {
        'TITLE_KEY': 'BONUS_CONFIRM_TITLE',
        'MESSAGE_KEY': 'CONTRACT_BREAKUP_CONFIRM_MESSAGE',
        'API_ACTION': 'PUT',
        'API_URL': '/api/1/web/customers/update/addon/add/bonus/contractbuster/',
        'VALIDATE_LIST': ['number', 'prefix', 'serviceInstanceNumber', 'account'],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': true,
        'FORM': {
            'titleKey': 'BONUS_CONFIRM_TITLE',
            'idKey': 'id',
            'options': [{
                display: 'Months', key: 'months', type: 'KeyValue', mandatory: true,
                items: [
                    {key: '2', value: "2 months"},
                    {key: '3', value: "3 months"},
                    {key: '4', value: "4 months"},
                    {key: '5', value: "5 months"},
                    {key: '6', value: "6+ months"}
                ]
            }]
        }
    },
    'CARE_INSTALL_BONUS': {
        'TITLE_KEY': 'BONUS_CONFIRM_TITLE',
        'MESSAGE_KEY': 'ADD_CARE_INSTALLATON_CONFIRM_MESSAGE',
        'API_ACTION': 'PUT',
        'API_URL': '/api/1/web/customers/update/addon/add/bonus/careinstallation/',
        'VALIDATE_LIST': ['number', 'prefix', 'serviceInstanceNumber', 'account', 'onegb'],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': true
    },
    'ADD_LAZADA_BONUS': {
        'TITLE_KEY': 'BONUS_CONFIRM_TITLE',
        'MESSAGE_KEY': 'ADD_LAZADA_BONUS_CONFIRM_MESSAGE',
        'API_ACTION': 'PUT',
        'API_URL': '/api/1/web/customers/update/addon/add/bonus/lazada/',
        'VALIDATE_LIST': ['number', 'prefix', 'serviceInstanceNumber', 'account'],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': true
    },
    'ADD_PENDING_BONUS': {
        'TITLE_KEY': 'BONUS_CONFIRM_TITLE',
        'MESSAGE_KEY': 'ADD_PENDING_BONUS_CONFIRM_MESSAGE',
        'API_ACTION': 'PUT',
        'API_URL': '/api/1/web/customers/update/addon/add/bonus/pending/',
        'VALIDATE_LIST': ['number', 'prefix', 'serviceInstanceNumber', 'account'],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': true
    },
    'RE_SUBSC_ADDONS': {
        'TITLE_KEY': 'ADDONS_CONFIRM_TITLE',
        'MESSAGE_KEY': 'RESUBSC_ADDON_CONFIRM_MESSAGE',
        'API_ACTION': 'PUT',
        'API_URL': '/api/1/web/customers/update/addon/resubscribe/',
        'VALIDATE_LIST': ['number', 'prefix', 'serviceInstanceNumber', 'account'],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': true,
        'FORM': {
            'titleKey': 'ADDONS_CONFIRM_TITLE',
            'idKey': 'resubAddon',
            'options': [{
                display: 'Type', key: 'schedule', type: 'KeyValue', mandatory: true,
                items: [
                    {key: 'false', value: "Execute"},
                    {key: 'true', value: "Schedule"}
                ]
            }]
        }
    },
    'DEACTIVATE_REFERRER_BONUS': {
        'TITLE_KEY': 'REFERRER_BONUS_TITLE',
        'MESSAGE_KEY': 'DEACTIVATE_REFERRER_BONUS_TITLE',
        'API_ACTION': 'PUT',
        'API_URL': '/api/1/web/customers/referrer/bonus/deactivate',
        'VALIDATE_LIST': ['number', 'prefix'],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': false
    },
    'ADD_AUTO_BOOST': {
        'TITLE_KEY': 'AUTOBOOST_CONFIRM_TITLE',
        'MESSAGE_KEY': 'AUTOBOOST_ENABLE_CONFIRM_MESSAGE',
        'MESSAGE_KEY2': 'AUTOBOOST_ADD_CONFIRM_MESSAGE',
        'MESSAGE_KEY3': 'AUTOBOOST_EXTRA_MESSAGE',
        'API_ACTION': 'PUT',
        'API_URL': '/api/1/web/customers/update/addon/add/autoboost/',
        'VALIDATE_LIST': ['number', 'prefix', 'serviceInstanceNumber', 'account', 'checkData', 'enable'],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': true
    },
    'CREATE_UPDATE_CC_LINK': {
        'TITLE_KEY': 'CC_UPDATE_TITLE',
        'MESSAGE_KEY': 'CC_UPDATE_LINK_CONFIRM_MESSAGE',
        'API_ACTION': 'GET',
        'API_URL': '/api/1/web/customers/link/update/cc/',
        'VALIDATE_LIST': ['number', 'prefix', 'account'],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': true
    },
    'CONSUME_DATA': {
        'TITLE_KEY': 'CONSUME_DATA_TITLE',
        'MESSAGE_KEY': 'CONSUME_DATA_CONFIRM_MESSAGE',
        'API_ACTION': 'PUT',
        'API_URL': '/api/1/web/customers/consume/data/',
        'VALIDATE_LIST': ['number', 'prefix', 'serviceInstanceNumber', 'account', 'amountMb', 'rgId'],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': true,
        'FORM': {
            'titleKey': 'CONSUME_DATA_TITLE',
            'okButtonText': 'Save',
            'options': [
                {
                    display: 'Type', key: 'type', type: 'KeyValue', items: [
                            {key: 'single', value: 'One Single Chunck'},
                            {key: 'multi', value: 'Many Small Chuncks'}
                    ], mandatory: true
                },
                {
                    display: 'Amount', key: 'amountMb', type: 'KeyValue', items: [
                            {key: '10', value: '10MB'},
                            {key: '100', value: '100MB'},
                            {key: '1024', value: '1GB'}
                    ], mandatory: true
                },
                {
                    display: 'Type', key: 'rgId', type: 'KeyValue', items: [
                            {key: '300', value: 'Local Data'},
                            {key: '301', value: 'Whatsapp'},
                            {key: '302', value: 'FB Messanger'},
                            {key: '303', value: 'FB General'},
                            {key: '319', value: 'MMS'}
                    ], mandatory: true
                }
            ]
        }
    },
    'ADD_PORTIN_BONUS': {
        'TITLE_KEY': 'BONUS_CONFIRM_TITLE',
        'MESSAGE_KEY': 'PORT_IN_BONUS_CONFIRM_MESSAGE',
        'API_ACTION': 'PUT',
        'API_URL': '/api/1/web/customers/update/addon/add/bonus/portin/',
        'VALIDATE_LIST': ['number', 'prefix', 'serviceInstanceNumber', 'account', 'extra'],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': true
    },
    'USE_REFERRAL_CODE': {
        'TITLE_KEY': 'REFERRER_CODE_TITLE',
        'MESSAGE_KEY': 'USER_REFERRAL_CODE_USE_CONFIRM_MESSAGE',
        'API_ACTION': 'PUT',
        'API_URL': '/api/1/web/customers/referral/use/',
        'VALIDATE_LIST': ['number', 'prefix', 'code'],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': false,
        'FORM': {
            'titleKey': 'REFERRER_CODE_TITLE',
            'idKey': 'referralCode',
            'options': [
                {
                    display: 'Code', key: 'code', type: 'String', mandatory: true
                }]
        }
    },
    'DELETE_REFERRAL': {
        'TITLE_KEY': 'BONUS_CONFIRM_TITLE',
        'MESSAGE_KEY': 'DELETE_REFERRAL_CONFIRM_MESSAGE',
        'API_ACTION': 'DELETE',
        'API_URL': '/api/1/web/customers/referral',
        'VALIDATE_LIST': ['number', 'prefix', 'referrer', 'id'],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': false
    },
    'REMOVE_APP_REGISTRATION': {
        'TITLE_KEY': 'REMOVE_APP_REGISTRATION_TITLE',
        'MESSAGE_KEY': 'REMOVE_APP_REGISTRATION_MESSAGE',
        'API_ACTION': 'DELETE',
        'API_URL': '/api/1/web/customers/delete/apps/',
        'VALIDATE_LIST': ['number', 'prefix', 'app'],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': true
    },
    'REMOVE_APP_REGISTRATION_VIA_APP_MANAGER': {
        'TITLE_KEY': 'REMOVE_APP_REGISTRATION_VIA_APP_MANAGER_TITLE',
        'MESSAGE_KEY': 'REMOVE_APP_REGISTRATION_VIA_APP_MANAGER_MESSAGE',
        'API_ACTION': 'DELETE',
        'API_URL': '/api/1/web/customers/delete/app/',
        'VALIDATE_LIST': ['userKey'],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': false
    },
    'MODIFY_CIRCLES_TALK': {
        'TITLE_KEY': 'CIRCLES_TALK',
        'API_ACTION': 'PUT',
        'API_URL': '/api/1/web/customers/apps/gentwo/manage',
        'VALIDATE_LIST': ['number', 'prefix'],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': false
    },
    'CIRCLES_TALK_ADD_CUSTOM_CREDIT': {
        'TITLE_KEY': 'CIRCLES_TALK_ADD_CREDIT_TITLE',
        'MESSAGE_KEY': 'CIRCLES_TALK_ADD_CREDIT_MESSAGE',
        'API_ACTION': 'PUT',
        'API_URL': '/api/1/web/customers/apps/gentwo/manage',
        'VALIDATE_LIST': ['number', 'prefix'],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': false,
        'FORM': {
            'titleKey': 'CIRCLES_TALK_ADD_CREDIT_TITLE',
            'idKey': 'addCustomCredit',
            'options': [
                {
                    display: 'Amount', key: 'amount', type: 'String'
                }]
        }
    },
    'DELETE_BONUS_HISTORY': {
        'TITLE_KEY': 'BONUS_CONFIRM_TITLE',
        'MESSAGE_KEY': 'DELETE_BONUS_HISTORY_CONFIRM_MESSAGE',
        'API_ACTION': 'DELETE',
        'API_URL': '/api/1/web/customers/bonus/history',
        'VALIDATE_LIST': ['number', 'prefix', 'id'],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': false
    },
    'DELETE_GOLDEN_TICKET': {
        'TITLE_KEY': 'Remove golden ticket',
        'MESSAGE_KEY': 'Would you like to remove golden ticket {id}?',
        'API_ACTION': 'DELETE',
        'API_URL': '/api/1/web/customers/leaderboard/goldentickets',
        'VALIDATE_LIST': ['id'],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': false
    },
    'ADD_GOLDEN_TICKET': {
        'TITLE_KEY': 'Add golden ticket',
        'MESSAGE_KEY': 'Would you like to add golden ticket for date {date}?',
        'API_ACTION': 'PUT',
        'API_URL': '/api/1/web/customers/leaderboard/goldentickets/',
        'VALIDATE_LIST': ['date'],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': true,
        'FORM': {
            'titleKey': 'Add golden ticket',
            'idKey': 'id',
            'options': [
                {display: 'Date', key: 'date', type: 'Date', mandatory: true},
                {display: 'Add Waiver', key: 'addWaiver', type: 'Boolean'},
                {display: 'Silent', key: 'silent', type: 'Boolean'}
            ]
        }
    },
    'CANCEL_PORTIN': {
        'TITLE_KEY': 'PORT_IN_TITLE',
        'MESSAGE_KEY': 'PORTIN_CANCEL_MESSAGE',
        'API_ACTION': 'PUT',
        'API_URL': '/api/1/web/customers/portin/action/',
        'VALIDATE_LIST': ['type', 'portInPrefix'],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': true,
        'FORM': {
            'titleKey': 'PORT_IN_TITLE',
            'idKey': 'createPortin',
            'options': [
                {display: 'Unpaired Numbers', key: 'portInNumberSelected', customKey: 'unpairedNumbers', type: 'KeyValue'},
                {display: 'New Number', key: 'portInNumberTyped', type: 'Integer'}
            ]
        }
    },
    'UPLOAD_PORTIN_GA_INFO_CSV': {
        'TITLE_KEY': 'Upload GA report',
        'MESSAGE_KEY': 'Would you like to upload GA report?',
        'API_ACTION': 'PUT',
        'API_URL': '/api/1/web/customers/portin/report/upload',
        'VALIDATE_LIST': [],
        'API_TIMEOUT': 3 * 60 * 1000,
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': false,
        'FORM': {
            'titleKey': 'Select GA report',
            'okButtonText': 'Upload',
            'options': [
                {display: 'Report CSV', key: 'file', type: 'Upload', fileType: 'Temp', mandatory: true}
            ]
        }
    },
    'CREATE_PORTIN': {
        'TITLE_KEY': 'PORT_IN_TITLE',
        'MESSAGE_KEY': 'PORTIN_CREATE_MESSAGE',
        'API_ACTION': 'PUT',
        'API_URL': '/api/1/web/customers/portin/action/',
        'VALIDATE_LIST': ['type', 'portInPrefix'],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': true,
        'FORM': {
            'titleKey': 'PORT_IN_TITLE',
            'idKey': 'createPortin',
            'options': [
                {display: 'Unpaired Numbers', key: 'portInNumberSelected', customKey: 'unpairedNumbers', type: 'KeyValue'},
                {display: 'New Number', key: 'portInNumberTyped', type: 'Integer'},
                {
                    display: 'Donor Telco', key: 'donorNetworkCode', type: 'KeyValue',
                    items: [
                        {key: '001', value: "M1"},
                        {key: '002', value: "StarHub"},
                        {key: '003', value: "Singtel"}
                    ]
                },
                {display: 'Start Date', key: 'startDate', customKey: 'startDatesList', type: 'KeyValue'}
            ]
        }
    },
    'CREATE_CORPORATE_PORTIN': {
        'TITLE_KEY': 'PORT_IN_TITLE',
        'MESSAGE_KEY': 'PORTIN_CREATE_MESSAGE',
        'API_ACTION': 'PUT',
        'API_URL': '/api/1/web/customers/portin/corporate/',
        'VALIDATE_LIST': ['portinNumber', 'tempNumber', 'imsiNumber', 'donorTelco', 'customerName', 'customerId', 'serviceInstanceNumber'],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': false,
        'FORM': {
            'titleKey': 'PORT_IN_TITLE',
            'idKey': 'createPortin',
            'options': [
                {display: 'Portin Number', key: 'portinNumber', type: 'Integer', mandatory: true},
                {display: 'Temp Number', key: 'tempNumber', type: 'Integer', mandatory: true},
                {display: 'IMSI Number', key: 'imsiNumber', type: 'Integer', mandatory: true},
                {
                    display: 'Donor Telco', key: 'donorTelco', type: 'KeyValue',
                    items: [
                        {key: '001', value: "M1"},
                        {key: '002', value: "StarHub"},
                        {key: '003', value: "Singtel"}
                    ]
                },
                {display: 'Customer Name', key: 'customerName', type: 'String', mandatory: true},
                {display: 'Customer IC', key: 'customerId', type: 'String', mandatory: true},
                {display: 'Service Instance Number', key: 'serviceInstanceNumber', type: 'String', mandatory: true},
            ]
        }
    },
    'SEND_PORT_IN_NOTIFICATION': {
        'TITLE_KEY': 'PORT_IN_TITLE',
        'MESSAGE_KEY': 'SEND_PORT_IN_NOTIFICATION_CONFIRM_MESSAGE',
        'API_ACTION': 'PUT',
        'API_URL': '/api/1/web/customers/portin/notification/',
        'VALIDATE_LIST': ['type', 'portInPrefix', 'unpairedNumbers'],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': true,
        'FORM': {
            'titleKey': 'PORT_IN_TITLE',
            'idKey': 'portinNotification',
            'options': [{
                display: 'Type', key: 'type', type: 'KeyValue', mandatory: true,
                items: [
                    {key: 'SUCCESS', value: "Success"},
                    {key: 'APPROVE', value: "Approve"},
                    {key: 'FAILURE', value: "Failed"}
                ]
            },
                {
                    display: 'New Number', key: 'portInNumberTyped', type: 'String', mandatory: false
                },
                {
                    display: 'Unpaired', key: 'portInNumberSelected', customKey: 'unpairedNumbers', type: 'KeyValue', mandatory: false
                }
            ]
        }
    },
    'RESEND_PORT_IN_MNP_NOTIFICATION': {
        'TITLE_KEY': 'PORT_IN_TITLE',
        'MESSAGE_KEY': 'RESEND_PORT_IN_MNP_NOTIFICATION_MESSAGE',
        'API_ACTION': 'PUT',
        'API_URL': '/api/1/web/portin/request/recover/mnp/',
        'VALIDATE_LIST': ['portInPrefix', 'portInNumber'],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': false
    },
    'RESEND_PORT_IN_PROVISIONING_NOTIFICATION': {
        'TITLE_KEY': 'PORT_IN_TITLE',
        'MESSAGE_KEY': 'RESEND_PORT_IN_PROVISIONING_NOTIFICATION_MESSAGE',
        'API_ACTION': 'PUT',
        'API_URL': '/api/1/web/portin/request/recover/provision/',
        'VALIDATE_LIST': ['portInPrefix', 'portInNumber'],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': false
    },
    'MOVE_PORT_IN_TO_IN_PROGRESS_STATUS': {
        'TITLE_KEY': 'PORT_IN_TITLE',
        'MESSAGE_KEY': 'MOVE_PORT_IN_TO_IN_PROGRESS_STATUS_MESSAGE',
        'API_ACTION': 'PUT',
        'API_URL': '/api/1/web/portin/request/recover/timeout/',
        'VALIDATE_LIST': ['portInPrefix', 'portInNumber'],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': false
    },
    'ACTIVATE_PORT_IN': {
        'TITLE_KEY': 'Activate Port-In',
        'MESSAGE_KEY': 'Would you like to activate this port-In request?',
        'API_ACTION': 'PUT',
        'API_URL': '/api/1/web/portin/request/activate',
        'VALIDATE_LIST': ['id'],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': false,
        'FORM': {
            'titleKey': 'Activate Port-In',
            'idKey': 'id',
            'options': [
                {display: 'Port-In Date', key: 'startDate', type: 'DateTime', mandatory: true},
            ]
        }
    },
    'UPDATE_PROFILE': {
        'TITLE_KEY': 'UPDATE_DETAILS_TITLE',
        'MESSAGE_KEY': 'UPDATE_DETAILS_CONFIRM_MESSAGE',
        'API_ACTION': 'PUT',
        'API_URL': '/api/1/web/customers/update/details/',
        'VALIDATE_LIST': ['name', 'idType', 'idNumber'],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': true,
        'FORM': {
            'titleKey': 'UPDATE_DETAILS_TITLE',
            'idKey': 'account',
            'options': [
                {display: 'Name', key: 'name', type: 'String', mandatory: true},
                {
                    display: 'ID Type', key: 'idType', type: 'KeyValue', mandatory: true,
                    items: [
                        {key: 'NRIC', value: "NRIC"},
                        {key: 'FIN', value: "FIN"}]
                },
                {display: 'ID Number', key: 'idNumber', type: 'String', mandatory: true},
                {display: 'Birthday', key: 'birthday', type: 'Date', mandatory: true},
                {
                    display: 'Port-In Status', key: 'portedStatus', type: 'KeyValue', mandatory: true,
                    items: [
                        {key: 'MNP', value: "MNP"},
                        {key: 'NORMAL', value: "NORMAL"}]
                },
                {display: "Order No.", key: 'orderRefNo', type: 'String', mandatory: true}
            ]
        }
    },
    'UPDATE_PROFILE_SETTINGS': {
        'TITLE_KEY': 'Update Settings',
        'MESSAGE_KEY': 'Would you like to update settings?',
        'API_ACTION': 'PUT',
        'API_URL': '/api/1/web/customers/update/profiles/settings/',
        'VALIDATE_LIST': ['optionId', 'optionType', 'optionValue'],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': true,
        'FORM': {
            'titleKey': 'Would you like to update settings?',
            'idKey': 'key',
            'options': [
                {display: 'ID', key: 'optionId', type: 'String', mandatory: true},
                {
                    display: 'Type', key: 'optionType', type: 'KeyValue', mandatory: true,
                    items: [
                        {key: 'Integer ', value: "Integer"},
                        {key: 'Boolean', value: "Boolean"},
                        {key: 'String', value: "String"}]
                },
                {display: 'Value', key: 'optionValue', type: 'String', mandatory: true}
            ]
        }
    },
    'VERIFY_PORT_IN_STATUS': {
        'TITLE_KEY': 'Verify Port-In Status',
        'MESSAGE_KEY': 'Would you like to update settings?',
        'API_ACTION': 'PUT',
        'API_URL': '/api/1/web/portin/move/status',
        'VALIDATE_LIST': ['id'],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': false,
        'FORM': {
            'titleKey': 'VERIFY_PORT_IN_STATUS_TITLE',
            'idKey': 'id',
            'okButtonText': 'Update',
            'options': [
                {
                    display: 'Remote status', key: 'bssStatus', type: 'String', disabled: true
                },
                {
                    display: 'Local status', key: 'localStatus', type: 'String', disabled: true
                }
            ]
        }
    },
    'UPDATE_PORT_IN_STATUS': {
        'TITLE_KEY': 'UPDATE_PORT_IN_STATUS_TITLE',
        'MESSAGE_KEY': 'UPDATE_PORT_IN_STATUS_CONFIRM_MESSAGE',
        'API_ACTION': 'PUT',
        'API_URL': '/api/1/web/portin/request/update/status/',
        'API_TIMEOUT': 3 * 60 * 1000,
        'VALIDATE_LIST': ['id', 'status'],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': false,
        'FORM': {
            'titleKey': 'UPDATE_PORT_IN_STATUS_TITLE',
            'idKey': 'id',
            'options': [{
                display: 'New Status', key: 'status', type: 'KeyValue', mandatory: true,
                items: [
                    {key: 'WAITING', value: "WAITING"},
                    {key: 'WAITING_APPROVE', value: "WAITING APPROVE"},
                    {key: 'WAITING_RETRY', value: "WAITING RETRY"},
                    {key: 'IN_PROGRESS', value: "IN PROGRESS"},
                    {key: 'DONOR_APPROVED', value: "DONOR APPROVED"},
                    {key: 'CANCELED', value: "CANCELED"},
                    {key: 'FAILED', value: "FAILED"},
                    {key: 'DONE', value: "DONE"}]
            }, {
                display: 'Failure Reason', key: 'reason', type: 'KeyValue', mandatory: false,
                items: [
                    {key: 'ERROR_DOC_REJECTED', value: "REJECTED AFTER CHECKING"},
                    {key: 'ERROR_DONOR_TELCO', value: "INVALID DONOR TELCO"},
                    {key: 'ERROR_ID_MISTMATCH', value: "ID MISMATCH"},
                    {key: 'ERROR_DOC_MISTMATCH', value: "DOC MISMATCH"},
                    {key: 'ERROR_INACTIVE', value: "INACTIVE"},
                    {key: 'ERROR_PREPAID', value: "PREPAID"},
                    {key: 'ERROR_OTHER', value: "OTHER"}]
            },
                {display: 'Failure Description', key: 'description', type: 'String', mandatory: false},
            ]
        }
    },
    'UPDATE_PORT_IN_TEMP_NAME': {
        'TITLE_KEY': 'UPDATE_PORT_IN_TEMP_NAME_TITLE',
        'MESSAGE_KEY': 'UPDATE_PORT_IN_TEMP_NAME_CONFIRM_MESSAGE',
        'API_ACTION': 'PUT',
        'API_URL': '/api/1/web/portin/request/update/owner/',
        'VALIDATE_LIST': ['id', 'ownerName', 'ownerIdNumber', 'ownerIdType'],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': false,
        'FORM': {
            'titleKey': 'UPDATE_PORT_IN_TEMP_NAME_TITLE',
            'idKey': 'id',
            'options': [{
                display: 'Owner ID Front', key: 'fileIdFront', type: 'File', disabled: true
            }, {
                display: 'Owner ID Back', key: 'fileIdBack', type: 'File', disabled: true
            }, {
                display: 'Transfer Form', key: 'fileAuthForm', type: 'File', disabled: true
            }, {
                display: 'Owner Name', key: 'ownerName', type: 'String', mandatory: true, limit: 48
            }, {
                display: 'Owner ID Type', key: 'ownerIdType', type: 'KeyValue', mandatory: true,
                items: [
                    {key: 'NRIC', value: "NRIC"},
                    {key: 'FIN', value: "FIN"}]
            }, {
                display: 'Owner ID Number', key: 'ownerIdNumber', type: 'String', mandatory: true
            }]
        }
    },
    'VIEW_PORT_IN_DOCS': {
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': false,
        'FORM': {
            'titleKey': 'VIEW_PORT_IN_DOCS',
            'idKey': 'id',
            'previewOnly': true,
            'options': [
                {display: 'Owner ID Front', key: 'fileIdFront', type: 'File', disabled: true},
                {display: 'Owner ID Back', key: 'fileIdBack', type: 'File', disabled: true},
                {display: 'Transfer Form', key: 'fileAuthForm', type: 'File', disabled: true},
                {display: 'Owner Name', key: 'ownerName', type: 'String', disabled: true},
                {display: 'Owner ID Type', key: 'ownerIdType', type: 'String', disabled: true},
                {display: 'Owner ID Number', key: 'ownerIdNumber', type: 'String', disabled: true},
                {display: 'User Name', key: 'customerName', type: 'String', disabled: true},
                {display: 'User ID Type', key: 'customerIdType', type: 'String', disabled: true},
                {display: 'User ID Number', key: 'customerIdNumber', type: 'String', disabled: true},
            ]
        }
    },
    'ADD_FREE_BOOST': {
        'TITLE_KEY': 'FREE_BOOST_CONFIRM_TITLE',
        'MESSAGE_KEY': 'FREE_BOOST_ENABLE_CONFIRM_MESSAGE',
        'API_ACTION': 'PUT',
        'API_URL': '/api/1/web/customers/update/addon/free/boost/',
        'VALIDATE_LIST': ['name', 'id', 'count', 'number', 'prefix', 'account', 'serviceInstanceNumber'],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': true
    },
    'TERMINATE_ACCOUNT': {
        'TITLE_KEY': 'TERMINATE_ACCOUNT_TITLE',
        'MESSAGE_KEY': 'TERMINATE_ACCOUNT_MESSAGE',
        'API_ACTION': 'PUT',
        'API_URL': '/api/1/web/customers/changestatus/',
        'VALIDATE_LIST': ['type', 'effective', 'billing_cycle', 'number', 'volunteer', 'prefix', 'account', 'serviceInstanceNumber'],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': true,
        'FORM': {
            'titleKey': 'VERIFICATION_TERMINATING_NUMBER',
            'idKey': 'verificationForm',
            'options': [
                {
                    display: 'Number to Term.', key: 'verificationNumber', type: 'Integer', mandatory: true
                }]
        }
    },
    'SUSPEND_ACCOUNT': {
        'TITLE_KEY': 'SUSPEND_ACCOUNT_TITLE',
        'MESSAGE_KEY': 'SUSPEND_ACCOUNT_MESSAGE',
        'API_ACTION': 'PUT',
        'API_URL': '/api/1/web/customers/changestatus/',
        'VALIDATE_LIST': ['type', 'earliestTerminationDate', 'billing_cycle', 'number', 'volunteer', 'months', 'prefix', 'account', 'serviceInstanceNumber'],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': true,
        'FORM': {
            'titleKey': 'SUSPEND_ACCOUNT_TITLE',
            'idKey': 'suspendAccount',
            'options': [{
                display: 'Suspend For', key: 'months', type: 'KeyValue', mandatory: true,
                items: [
                    {key: '0', value: "Free"},
                    {key: '-1', value: "Lost SIM"},
                    {key: '-2', value: "Non-payment"},
                    {key: '1', value: "1 Month"},
                    {key: '2', value: "2 Months"},
                    {key: '3', value: "3 Months"},
                    {key: '4', value: "4 Months"},
                    {key: '5', value: "5 Months"},
                    {key: '6', value: "6 Months"},
                    {key: '7', value: "7 Months"},
                    {key: '8', value: "8 Months"},
                    {key: '9', value: "9 Months"},
                    {key: '10', value: "10 Months"},
                    {key: '11', value: "11 Months"},
                    {key: '12', value: "12 Months"}
                ]
            },
                {
                    display: 'Per Month Charge', key: 'perMonthAmount', type: 'KeyValue', mandatory: true,
                    items: [
                        {key: '5', value: '5$'},
                        {key: '10', value: '10$'}
                    ]
                },
                {
                    display: 'Volunteer', key: 'volunteer', type: 'KeyValue', mandatory: true,
                    items: [
                        {key: 'true', value: "Yes"},
                        {key: 'false', value: "No"}
                    ]
                },
                {
                    display: 'Number to Suspend', key: 'verificationNumber', type: 'Integer', mandatory: true
                }]
        }
    },
    'ACTIVATE_ACCOUNT': {
        'TITLE_KEY': 'ACTIVATE_ACCOUNT_TITLE',
        'MESSAGE_KEY': 'ACTIVATE_ACCOUNT_MESSAGE',
        'API_ACTION': 'PUT',
        'API_URL': '/api/1/web/customers/changestatus/',
        'VALIDATE_LIST': ['type', 'earliestTerminationDate', 'billing_cycle', 'number', 'months', 'prefix', 'account', 'serviceInstanceNumber'],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': true
    },
    'VALIDATE_ACCOUNT_STATUS': {
        'TITLE_KEY': 'Account status validation',
        'MESSAGE_KEY': 'Would you like to update account status base on customer invoices?',
        'API_ACTION': 'PUT',
        'API_URL': '/api/1/web/customers/validatestatus/',
        'VALIDATE_LIST': ['prefix', 'number', 'account', 'serviceInstanceNumber'],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': true,
        'FORM': {
            'titleKey': 'Account status validation',
            'idKey': 'suspendAccount',
            'okButtonText': 'Validate',
            'options': [
                {display: 'Delayed', key: 'delayed', type: 'Boolean', mandatory: true},
                {display: 'Immediate Susp.', key: 'immediateSuspension', type: 'Boolean', mandatory: true}
            ]
        }
    },
    'RECACHE': {
        'TITLE_KEY': 'RECACHE_CONFIRM_TITLE',
        'MESSAGE_KEY': 'RECACHE_CONFIRM_MESSAGE',
        'API_ACTION': 'PUT',
        'API_URL': '/api/1/web/customers/update/cache/',
        'VALIDATE_LIST': ['reload', 'prefix', 'number', 'account', 'serviceInstanceNumber'],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': true
    },
    'EDIT_PROMO_CATEGORY': {
        'TITLE_KEY': 'EDIT_PROMO_CATEGORY_TITLE',
        'MESSAGE_KEY': 'EDIT_PROMO_CATEGORY_MESSAGE',
        'API_ACTION': 'PUT',
        'API_URL': '/api/1/web/promo/add/category/',
        'VALIDATE_LIST': [],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': false,
        'FORM': {
            'titleKey': 'EDIT_PROMO_CATEGORY_TITLE',
            'idKey': 'id',
            'options': [
                {display: 'Name', key: 'name', type: 'String', mandatory: true},
                {
                    display: 'Type', key: 'type', type: 'KeyValue', items: [
                    {key: 'internal', value: "APP"},
                    {key: 'partner', value: "E-COMMERCE"},
                    {key: 'internal_ecomm', value: "E-COMMERCE + APP"}
                ], mandatory: true
                },
                {display: 'Tag', key: 'tag', type: 'String', mandatory: false},
                {
                    display: 'Partner', key: 'partner', type: 'KeyValue', customKey:"partnersItems", mandatory: false
                },
                {display: 'Max Limit L1', key: 'max', type: 'Integer', mandatory: true},
                {display: 'Max Limit L2', key: 'maxL2', type: 'Integer', mandatory: false},
                {display: 'Max Limit L3', key: 'maxL3', type: 'Integer', mandatory: false},
                {
                    display: 'Usage Type', key: 'multipleCodesAllowed',
                    type: 'KeyValue', items: [
                    {key: '0', value: "SINGLE"},
                    {key: '1', value: "MULTIPLE"}
                ], mandatory: true
                },
                {display: 'Start', key: 'start', type: 'DateTime', mandatory: true},
                {display: 'End', key: 'end', type: 'DateTime'},
                {display: 'Enabled', key: 'enabled', type: 'Boolean', mandatory: true},
                {display: 'App Title', key: 'title', type: 'String', mandatory: false},
                {display: 'App Message', key: 'message', type: 'String', mandatory: false},
                {display: 'App Image', key: 'image1', type: 'String', mandatory: false},
                {display: 'E-Comm Title', key: 'publicName', type: 'String', mandatory: false},
                {display: 'Code Prefix', key: 'codePrefix', type: 'String', mandatory: false},
                {display: 'Code Valid Days', key: 'codeValidDays', type: 'KeyValue', items: [
                    {key: '-1', value: "NO EXPIRY (CAMPAIGN EXPIRY)"},
                    {key: '1', value: "1 Day"},
                    {key: '3', value: "3 Days"},
                    {key: '5', value: "5 Days"},
                    {key: '7', value: "7 Days"},
                    {key: '15', value: "15 Days"},
                    {key: '31', value: "31 Days"},
                ], mandatory: false},
                {display: 'Code Pattern L1', key: 'codePatternL1', type: 'String', mandatory: false},
                {display: 'Code Pattern L2', key: 'codePatternL2', type: 'String', mandatory: false},
                {display: 'Code Pattern L3', key: 'codePatternL3', type: 'String', mandatory: false},
                {display: 'Merchant Number', key: 'customMNumber', type: 'String', mandatory: false},
                {display: 'Bank Name', key: 'bankName', type: 'String', mandatory: false},
                {display: 'Web Registration', key: 'webRegistration', type: 'Boolean', mandatory: false}
            ]
        }
    },
    'COPY_PROMO_CODE': {
        'TITLE_KEY': 'COPY_PROMO_CODE_TITLE',
        'MESSAGE_KEY': 'COPY_PROMO_CODE_MESSAGE',
        'API_ACTION': 'PUT',
        'API_TIMEOUT': 10 * 60 * 1000,
        'API_URL': '/api/1/web/promo/copy/code/',
        'VALIDATE_LIST': [],
        'FORM': {
            'titleKey': 'COPY_PROMO_CODE_TITLE',
            'idKey': 'id',
            'options': [
                {display: 'Category Id', key: 'categoryId', type: 'String', mandatory: true, disabled: true},
                {display: 'Code L1', key: 'code', type: 'String', mandatory: true},
                {display: 'Code L2', key: 'codeL2', type: 'String', mandatory: false},
                {display: 'Code L3', key: 'codeL3', type: 'String', mandatory: false},
                {display: 'Code Prefix', key: 'prefix', type: 'String', mandatory: true},
                {display: 'Code Valid Days', key: 'codeValidDays', type: 'KeyValue', items: [
                    {key: '-1', value: "NO EXPIRY (CAMPAIGN EXPIRY)"},
                    {key: '1', value: "1 Day"},
                    {key: '3', value: "3 Days"},
                    {key: '5', value: "5 Days"},
                    {key: '7', value: "7 Days"},
                    {key: '15', value: "15 Days"},
                    {key: '31', value: "31 Days"},
                ], mandatory: false},
                {display: 'Random Length', key: 'randomLength', type: 'Integer', mandatory: true},
                {display: 'Count New', key: 'count', type: 'Integer', mandatory: true}
            ]
        }
    },
    'REMOVE_PROMO_CATEGORY': {
        'TITLE_KEY': 'REMOVE_PROMO_CATEGORY_TITLE',
        'MESSAGE_KEY': 'REMOVE_PROMO_CATEGORY_MESSAGE',
        'API_ACTION': 'DELETE',
        'API_URL': '/api/1/web/promo/delete/category',
        'VALIDATE_LIST': ['id'],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': false
    },
    'REMOVE_PROMO_CODE': {
        'TITLE_KEY': 'REMOVE_PROMO_CODE_TITLE',
        'MESSAGE_KEY': 'REMOVE_PROMO_CODE_MESSAGE',
        'API_ACTION': 'DELETE',
        'API_URL': '/api/1/web/promo/delete/code',
        'VALIDATE_LIST': ['id'],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': false
    },
    'REMOVE_PROMO_CODE_LOG': {
        'TITLE_KEY': 'REMOVE_PROMO_LOG_TITLE',
        'MESSAGE_KEY': 'REMOVE_PROMO_LOG_MESSAGE',
        'API_ACTION': 'DELETE',
        'API_URL': '/api/1/web/promo/delete/log',
        'VALIDATE_LIST': ['id'],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': false
    },
    'EDIT_PROMO_WEB_PAGE': {
        'TITLE_KEY': 'EDIT_PROMO_WEB_PAGE_TITLE',
        'MESSAGE_KEY': 'EDIT_PROMO_WEB_PAGE_MESSAGE',
        'API_ACTION': 'PUT',
        'API_URL': '/api/1/web/promo/add/category/page',
        'VALIDATE_LIST': ['id'],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': false,
        'FORM': {
            'titleKey': 'EDIT_PROMO_WEB_PAGE_TITLE',
            'idKey': 'id',
            'options': [
                {display: 'URL Path', key: 'pathUrl', type: 'String', mandatory: true},
                {display: 'T&C URL', key: 'tacUrl', type: 'String', mandatory: false},
                {display: 'FAQ URL', key: 'faqUrl', type: 'String', mandatory: false},
                {display: 'Header Image', key: 'headerImageUrl', type: 'String', mandatory: false},
                {display: 'Footer Image', key: 'footerImageUrl', type: 'String', mandatory: false},
                {display: 'Background Image', key: 'backgroundImageUrl', type: 'String', mandatory: false},
                {display: 'Success Image', key: 'successImageUrl', type: 'String', mandatory: false},
                {display: 'Field Name', key: 'fieldName', type: 'Boolean', mandatory: false},
                {display: 'Field Phone', key: 'fieldPhone', type: 'Boolean', mandatory: false},
                {display: 'Field Email', key: 'fieldEmail', type: 'Boolean', mandatory: false},
                {display: 'Field Password', key: 'fieldPassword', type: 'Boolean', mandatory: false},
                {display: 'Field NRIC', key: 'fieldNric', type: 'Boolean', mandatory: false},
                {display: 'Field Corp. Email', key: 'fieldCorporateEmail', type: 'Boolean', mandatory: false},
                {
                    display: 'Unique Field', key: 'uniqueField', type: 'KeyValue', items: [
                    {key: 'name', value: "NAME"},
                    {key: 'email', value: "EMAIL"},
                    {key: 'nric', value: "NRIC"},
                    {key: 'corporateEmail', value: "CORPORATE EMAIL"},
                    {key: 'schoolEmail', value: "SCHOOL EMAIL"},
                    {key: 'OAUTH_UBER_DRIVER_ID', value: "OAUTH UBER DRIVER ID"},
                    {key: 'OAUTH_POEMS_ID', value: "OAUTH POEMS ID"}
                ], mandatory: true
                },
                {display: 'Button Text', key: 'registrationButtonText', type: 'String', mandatory: false},
                {display: 'Button Text (L-In)', key: 'registrationButtonTextLoggedIn', type: 'String', mandatory: false},
                {
                    display: 'Registration Type', key: 'registrationType', type: 'KeyValue', items: [
                    {key: 'REGISTER_INTERNAL', value: "INTERNAL ONLY"},
                    {key: 'REGISTER_AFTER_LOGIN_AUTH_2_0', value: "VIA AUTH 2.0"}
                ], mandatory: true
                },
                {display: 'OAuth2.0 URL', key: 'registrationOAuth20Url', type: 'String', mandatory: false},
                {display: 'Welcome Activity', key: 'welcomeActivity', type: 'String', mandatory: false},
                {display: 'Corp Email Ext.', key: 'emailExtension', type: 'String', mandatory: false},
                {display: 'Usage Activity', key: 'usageActivity', type: 'String', mandatory: false},
                {display: 'Reminder1 Activity', key: 'reminderActivity1', type: 'String', mandatory: false},
                {display: 'Reminder1 On Day', key: 'reminderOnDay1', type: 'Integer', mandatory: false},
                {display: 'Reminder1 Date', key: 'reminderDate1', type: 'DateTime', mandatory: false},
                {display: 'Reminder2 Activity', key: 'reminderActivity2', type: 'String', mandatory: false},
                {display: 'Reminder2 On Day', key: 'reminderOnDay2', type: 'Integer', mandatory: false},
                {display: 'Reminder2 Date', key: 'reminderDate2', type: 'DateTime', mandatory: false}
            ]
        }
    },
    'RESYNC_CLASSES': {
        'TITLE_KEY': 'RESYNC_CLASSES_TITLE',
        'MESSAGE_KEY': 'RESYNC_CLASSES_MESSAGE',
        'API_ACTION': 'PUT',
        'API_URL': '/api/1/web/analytics/classes/resync',
        'API_TIMEOUT': 10 * 60 * 1000,
        'VALIDATE_LIST': [],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': false
    },
    'SEND_CLASS_NOTIFICATION': {
        'TITLE_KEY': 'SEND_CLASS_NOTIFICATION_TITLE',
        'MESSAGE_KEY': 'SEND_CLASS_NOTIFICATION_MESSAGE',
        'API_ACTION': 'PUT',
        'API_URL': '/api/1/web/analytics/classes/send/notification',
        'API_TIMEOUT': 10 * 60 * 1000,
        'VALIDATE_LIST': ['classId', 'delay', 'activity'],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': false,
        'FORM': {
            'titleKey': 'SEND_CLASS_NOTIFICATION_TITLE',
            'okButtonText': 'Send',
            'options': [
                {display: 'Class ID', key: 'classId', type: 'String', disabled: true},
                {display: 'Class Name', key: 'className', type: 'String', disabled: true},
                {
                    display: 'Notification', key: 'activity', type: 'Autocomplete',
                    url: "/api/1/web/analytics/get/notification/ids/%query", wildcard: "%query", resultKey: "value", mandatory: true
                },
                paceDialogOption,
                {
                    display: 'Ignore If', key: 'timeUnique', type: 'KeyValue', items: [
                    {key: '86400000', value: "Has been sent in the last 1 day"},
                    {key: '432000000', value: "Has been sent in the last 5 days"},
                    {key: '2592000000', value: "Has been sent in the last 30 days"},
                ]},
                {display: 'One per Email', key: 'emailUnique', type: 'Boolean', mandatory: false}
            ]
        }
    },
    'ADD_UPDATE_KPI_DEFINITION': {
        'TITLE_KEY': 'ADD_KPI_DEFINITION_TITLE',
        'MESSAGE_KEY': 'ADD_KPI_DEFINITION_MESSAGE',
        'API_ACTION': 'PUT',
        'API_URL': '/api/1/web/bi/put/kpiDefinition',
        'VALIDATE_LIST': [],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': false,
        'FORM': {
            'titleKey': 'ADD_KPI_DEFINITION_TITLE',
            'okButtonText': 'Save',
            'options': [
                {display: 'Key', key: 'key', type: 'String', mandatory: true, regex: /[A-Z|_]+/, regexMessageKey: "BI_KEY_FORMAT_ERROR"},
                {display: 'Name', key: 'name', type: 'String', mandatory: true},
                {display: 'Description', key: 'description', type: 'String', mandatory: true},
                {
                    display: 'Type', key: 'type', type: 'KeyValue', items: [
                    {key: 'manual', value: 'Manual'},
                    {key: 'query', value: 'Query'},
                    {key: 'derive', value: 'Derive'},
                    {key: 'mariaDB', value: 'Maria DB'},
                    {key: 'mongoDB', value: 'Mongo DB'}
                ], mandatory: true
                },
                {
                    display: 'Data Type', key: 'dataType', type: 'KeyValue', items: [
                    {key: 'currency', value: 'Currency'},
                    {key: 'number', value: 'Number'},
                    {key: 'percentage', value: 'Percentage'},
                    {key: 'megabyte', value: 'Megabyte'},
                    {key: 'gigabyte', value: 'Gigabyte'}
                ], mandatory: true
                },
                {display: 'Query', key: 'query', rows: "4", cols: "5", type: 'Textarea', mandatory: true}
            ]
        }
    },
    'ADD_KPI_VALUE': {
        'TITLE_KEY': 'ADD_KPI_VALUE_TITLE',
        'MESSAGE_KEY': 'ADD_KPI_VALUE_MESSAGE',
        'API_ACTION': 'PUT',
        'API_URL': '/api/1/web/bi/put/kpiValue',
        'VALIDATE_LIST': [],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': false,
        'FORM': {
            'titleKey': 'ADD_KPI_VALUE_TITLE',
            'okButtonText': 'Save',
            'options': [
                {display: 'Value', key: 'value', type: 'Float', mandatory: true},
                {display: 'Start Date', key: 'startDate', type: 'DateTime', mandatory: false},
                {display: 'End Date', key: 'endDate', type: 'DateTime', mandatory: false}
            ]
        }
    },
    'DELETE_KPI_DEFINITION': {
        'TITLE_KEY': 'DELETE_KPI_DEFINITION_TITLE',
        'MESSAGE_KEY': 'DELETE_KPI_DEFINITION_MESSAGE',
        'API_ACTION': 'DELETE',
        'API_URL': '/api/1/web/bi/delete/kpiDefinition',
        'VALIDATE_LIST': [],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': false
    },
    'DELETE_KPI_VIEW': {
        'TITLE_KEY': 'DELETE_KPI_VIEW_TITLE',
        'MESSAGE_KEY': 'DELETE_KPI_VIEW_MESSAGE',
        'API_ACTION': 'DELETE',
        'API_URL': '/api/1/web/bi/delete/kpiView',
        'VALIDATE_LIST': [],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': false
    },
    'REMOVE_KPI_DEF_FROM_VIEW': {
        'TITLE_KEY': 'REMOVE_KPI_DEF_FROM_TITLE',
        'MESSAGE_KEY': 'REMOVE_KPI_DEF_FROM_MESSAGE',
        'API_ACTION': 'DELETE',
        'API_URL': '/api/1/web/bi/delete/kpiGenFromView',
        'VALIDATE_LIST': [],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': false
    },
    'ADD_UPDATE_KPI_GENERATOR': {
        'TITLE_KEY': 'ADD_KPI_GENERATOR_TITLE',
        'MESSAGE_KEY': 'ADD_KPI_GENERATOR_MESSAGE',
        'API_ACTION': 'PUT',
        'API_URL': '/api/1/web/bi/put/kpiGenerator',
        'VALIDATE_LIST': [],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': false,
        'FORM': {
            'titleKey': 'ADD_KPI_GENERATOR_TITLE',
            'okButtonText': 'Save',
            'options': [
                {display: 'Key', key: 'key', type: 'String', mandatory: true, regex: /[A-Z|_]+/, regexMessageKey: "BI_KEY_FORMAT_ERROR"},
                {
                    display: 'Definition Key', key: 'definitionKey', type: 'KeyValue', customKey: 'kpiDefKeys', mandatory: true
                },
                {display: 'Name', key: 'name', type: 'String', mandatory: true},
                {
                    display: 'Period', key: 'period', type: 'KeyValue', items: [
                    {key: 'alltime', value: 'All Time'},
                    {key: 'monthly', value: 'Monthly'},
                    {key: 'weekly', value: 'Weekly'},
                    {key: 'daily', value: 'Daily'}
                ], mandatory: true
                },
                {display: 'Upper Bound', key: 'upperBound', type: 'Float', mandatory: false},
                {display: 'Lower Bound', key: 'lowerBound', type: 'Float', mandatory: false}
            ]
        }
    },
    'ADD_UPDATE_KPI_VIEW': {
        'TITLE_KEY': 'ADD_KPI_VIEW_TITLE',
        'MESSAGE_KEY': 'ADD_KPI_VIEW_MESSAGE',
        'API_ACTION': 'PUT',
        'API_URL': '/api/1/web/bi/put/kpiView',
        'VALIDATE_LIST': [],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': false,
        'FORM': {
            'titleKey': 'ADD_KPI_VIEW_MESSAGE',
            'okButtonText': 'Save',
            'options': [
                {display: 'Key', key: 'key', type: 'String', mandatory: true, regex: /[A-Z|_]+/, regexMessageKey: "BI_KEY_FORMAT_ERROR"},
                {display: 'Name', key: 'name', type: 'String', mandatory: true},
                {display: 'Description', key: 'description', type: 'String', mandatory: true},
                {
                    display: 'Tab', key: 'tab', type: 'KeyValue', customKey: 'tabs', mandatory: true
                },
                {
                    display: 'Type', key: 'viewType', type: 'KeyValue', items: [
                    {key: 'number', value: 'Number'},
                    {key: 'table', value: 'Table'},
                    {key: 'graph', value: 'Graph'},
                    {key: 'target', value: 'Target'},
                    {key: 'list', value: 'List'}
                ], mandatory: true
                },
                {
                    display: 'Timeline', key: 'timeline', type: 'KeyValue', items: [
                    {key: 'alltime', value: 'Alltime'},
                    {key: 'monthly', value: 'Monthly'},
                    {key: 'weekly', value: 'Weekly'},
                    {key: 'daily', value: 'Daily'}
                ], mandatory: true
                }
            ]
        }
    },
    'ADD_KPI_TO_VIEW': {
        'TITLE_KEY': 'ADD_KPI_TO_VIEW_TITLE',
        'MESSAGE_KEY': 'ADD_KPI_TO_VIEW_MESSAGE',
        'API_ACTION': 'PUT',
        'API_URL': '/api/1/web/bi/put/kpiToView',
        'VALIDATE_LIST': [],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': false,
        'FORM': {
            'titleKey': 'ADD_KPI_TO_VIEW_MESSAGE',
            'okButtonText': 'Save',
            'options': [
                {
                    display: 'Generator', key: 'generator', type: 'KeyValue', customKey: 'generatorList', mandatory: true
                },
                {display: 'Rank', key: 'rank', type: 'Float', mandatory: false}
            ]
        }
    },
    'DELETE_KPI_GENERATOR': {
        'TITLE_KEY': 'DELETE_KPI_GENERATOR_TITLE',
        'MESSAGE_KEY': 'DELETE_KPI_GENERATOR_MESSAGE',
        'API_ACTION': 'DELETE',
        'API_URL': '/api/1/web/bi/delete/kpiGenerator',
        'VALIDATE_LIST': [],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': false
    },
    'REGENARATE_VALUES_FOR_KPI_GENERATOR': {
        'TITLE_KEY': 'REGENARATE_VALUES_FOR_KPI_GENERATOR_TITLE',
        'MESSAGE_KEY': 'REGENARATE_VALUES_FOR_KPI_GENERATOR_MESSAGE',
        'API_ACTION': 'GET',
        'API_URL': '/api/1/web/bi/get/regenerate',
        'VALIDATE_LIST': ['key'],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': false
    },
    'SYNC_BILLS': {
        'TITLE_KEY': 'SYNC_BILLS_TITLE',
        'MESSAGE_KEY': 'SYNC_BILLS_MESSAGE',
        'API_ACTION': 'PUT',
        'API_URL': '/api/1/web/invoices/sync',
        'VALIDATE_LIST': [],
        'API_TIMEOUT': 3 * 60 * 1000,
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': false,
        'FORM': {
            'titleKey': 'SYNC_BILLS_TITLE',
            'okButtonText': 'Sync',
            'options': [
                {display: 'Month', key: 'month', type: 'DateMonth', mandatory: true},
                {
                    display: 'Verify Content', key: 'verifyContent', type: 'KeyValue', items: [
                    {key: 'ALL', value: 'ALL'},
                    {key: 'UNKNOWN_ONLY', value: 'CONTENT UNCHECKED'},
                    {key: 'FAILED_ONLY', value: 'CONTENT CHECK FAILED'}
                ], mandatory: false
                },
                {display: 'Override Lock', key: 'overrideLock', type: 'Boolean', mandatory: false}
            ]
        }
    },
    'SYNC_UNPAID_BILLS': {
        'TITLE_KEY': 'Sync Unpaid Invoices',
        'MESSAGE_KEY': 'Would you like to sync all unpaid invoices?',
        'API_ACTION': 'PUT',
        'API_URL': '/api/1/web/invoices/unpaid/sync',
        'VALIDATE_LIST': [],
        'API_TIMEOUT': 3 * 60 * 1000,
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': false
    },
    'MAKE_BATCH_PAYMENTS': {
        'TITLE_KEY': 'Batch Payment',
        'MESSAGE_KEY': 'Would you like to continue with batch payment for {batchSize} customer(s)?',
        'API_ACTION': 'PUT',
        'API_URL': '/api/1/web/invoices/payment/run/batch',
        'VALIDATE_LIST': [],
        'API_TIMEOUT': 3 * 60 * 1000,
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': false,
        'FORM': {
            'titleKey': 'Batch Payment Configuration',
            'okButtonText': 'Run Batch',
            'options': [
                {
                    display: 'Batch Size', key: 'batchSize', type: 'KeyValue', items: [
                    {key: '1', value: "1"},
                    {key: '10', value: "10"},
                    {key: '50', value: "50"},
                    {key: '100', value: "100"},
                    {key: '200', value: "200"},
                    {key: '500', value: "500"},
                    {key: '1000', value: "1,000"},
                    {key: '3000', value: "3,000"},
                    {key: '5000', value: "5,000"},
                    {key: '10000', value: "10,000"},
                    {key: '20000', value: "20,000"},
                    {key: 'ALL', value: "All"},
                ], mandatory: true
                },
                {display: 'Single BAN', key: 'billingAccountNumber', type: 'String', mandatory: false},
                {display: 'Silent', key: 'silent', type: 'Boolean', mandatory: false},
                {display: 'No Prev. Payment', key: 'noPaymentAttempt', type: 'Boolean', mandatory: false},
                {display: 'In Current Cycle', key: 'successSync', type: 'Boolean', mandatory: false},
                {display: 'Months No.', key: 'monthNumber', type: 'KeyValue', items: [
                    {key: '1', value: "1 Month"},
                    {key: '2', value: "2 Months"},
                    {key: '3', value: "3+ Months"}
                ], mandatory: false},
                {display: 'Retry Unfinished', key: 'retryUnfinished', type: 'Boolean', mandatory: false},
                {display: 'Ignore Sync Status', key: 'ignoreInvalidSync', type: 'Boolean', mandatory: false},
                {display: 'Ignore Suspension', key: 'ignoreSuspension', type: 'Boolean', mandatory: false},
                {display: 'Override Lock', key: 'overrideLock', type: 'Boolean', mandatory: false}
            ]
        }
    },
    'UPLOAD_PAYMENTS_CSV': {
        'TITLE_KEY': 'UPLOAD_PAYMENTS_CSV_TITLE',
        'MESSAGE_KEY': 'UPLOAD_PAYMENTS_CSV_MESSAGE',
        'API_ACTION': 'PUT',
        'API_URL': '/api/1/web/invoices/payment/update',
        'VALIDATE_LIST': [],
        'API_TIMEOUT': 3 * 60 * 1000,
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': false,
        'FORM': {
            'titleKey': 'UPLOAD_PAYMENTS_CSV_TITLE',
            'okButtonText': 'Process',
            'options': [
                {display: 'Payment CSV', key: 'file', type: 'Upload', fileType: 'Temp', mandatory: true},
                {display: 'Override Lock', key: 'overrideLock', type: 'Boolean', mandatory: false}
            ]
        }
    },
    'RECOVER_TRANSACTION': {
        'TITLE_KEY': 'Recover transaction',
        'MESSAGE_KEY': 'Would you like to recover transaction {id} ({type}) for customer {billingAccountNumber}?',
        'API_ACTION': 'PUT',
        'API_URL': '/api/1/web/transactions/recover',
        'VALIDATE_LIST': ['id'],
        'API_TIMEOUT': 3 * 60 * 1000,
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': false,
        'FORM': {
            'titleKey': 'Recover transaction config',
            'okButtonText': 'Recover',
            'options': [
                {
                    display: 'If Payment Failed', key: 'paymentFailedStrategy', type: 'KeyValue', items: [
                    {key: 'IGNORE', value: "IGNORE TRANSACTION"},
                    {key: 'RETRY', value: "RETRY TRANSACTION"}], defaultSelected:'IGNORE', mandatory: true
                },
                {
                    display: 'If Payment Succeed', key: 'paymentSucceedStrategy', type: 'KeyValue', items: [
                    {key: 'VOID', value: "VOID PAYMENT"},
                    {key: 'COMPLETE', value: "FINISH TRANSACTION"}], defaultSelected:'COMPLETE', mandatory: true
                },
                {
                    display: 'If Processing', key: 'paymentInProgressStrategy', type: 'KeyValue', items: [
                    {key: 'CONSIDER_AS_PAYMENT_SUCCESS', value: "CONSIDER AS SUCCESSFUL PAYMENT"},
                    {key: 'CONSIDER_AS_PAYMENT_FAILURE', value: "CONSIDER AS FAILED PAYMENT"}]
                },
                {
                    display: 'Skip Actions', key: 'skipActions', type: 'Boolean'
                }
            ]
        }
    },
    'SEND_ALL_BILLS': {
        'TITLE_KEY': 'SEND_BILLS_TITLE',
        'MESSAGE_KEY': 'SEND_BILLS_MESSAGE',
        'API_ACTION': 'PUT',
        'API_URL': '/api/1/web/invoices/send/bill/all',
        'API_TIMEOUT': 3 * 60 * 1000,
        'VALIDATE_LIST': ['monthId'],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': false,
        'FORM': {
            'titleKey': 'SEND_BILLS_TITLE',
            'okButtonText': 'Send',
            'options': [
                {display: 'Month', key: 'monthName', type: 'String', disabled: true},
                paceDialogOption,
                {
                    display: 'Batch Size', key: 'batchSize', type: 'KeyValue', items: [
                    {key: '1', value: "1"},
                    {key: '10', value: "10"},
                    {key: '1000', value: "1,000"},
                    {key: '3000', value: "3,000"},
                    {key: '5000', value: "5,000"},
                    {key: '10000', value: "10,000"},
                    {key: '20000', value: "20,000"},
                    {key: 'ALL', value: "All"},
                ], mandatory: true
                },
                {
                    display: 'Type', key: 'type', type: 'KeyValue', items: [
                    {key: 'ANY', value: "Any"},
                    {key: 'PAID', value: "Paid Only"},
                    {key: 'UNPAID', value: "Unpaid Only"},
                    {key: 'PARTIALLY_PAID_NEW', value: "Partially Paid (New)"},
                    {key: 'PARTIALLY_PAID_OLD', value: "Partially Paid (Old)"}
                ]},
                {
                    display: 'Payment Attempt', key: 'paymentType', type: 'KeyValue', items: [
                    {key: 'ANY', value: "Any"},
                    {key: 'PAYMENT_PROCESSED', value: "HAS Payment Attempt"},
                    {key: 'PAYMENT_NOT_PROCESSED', value: "NO Payment Attempt"}
                ]},
                {display: 'Re-send failed', key: 'resendFailed', type: 'Boolean', mandatory: false},
                {display: 'Override Lock', key: 'overrideLock', type: 'Boolean', mandatory: false}
            ]
        }
    },
    'SEND_SINGLE_BILL': {
        'TITLE_KEY': 'SEND_SINGLE_BILL_TITLE',
        'MESSAGE_KEY': 'SEND_SINGLE_BILL_MESSAGE',
        'API_ACTION': 'PUT',
        'API_URL': '/api/1/web/invoices/send/bill/single',
        'VALIDATE_LIST': ['invoiceId', 'billingAccountNumber'],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': false
    },
    'SEND_BILL_PAYMENT_REMINDER': {
        'TITLE_KEY': 'SEND_BILL_PAYMENT_REMINDER_TITLE',
        'MESSAGE_KEY': 'SEND_BILL_PAYMENT_REMINDER_MESSAGE',
        'API_ACTION': 'PUT',
        'API_URL': '/api/1/web/invoices/send/reminder/nonpayment',
        'API_TIMEOUT': 3 * 60 * 1000,
        'VALIDATE_LIST': [],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': false,
        'FORM': {
            'titleKey': 'SEND_BILL_PAYMENT_REMINDER_TITLE',
            'okButtonText': 'Send',
            'options': [
                {
                    display: 'Type', key: 'type', type: 'KeyValue', items: [
                    {key: 'NON_PAYMENT_REMINDER_1', value: "Send Non-Payment Reminder #1"},
                    {key: 'NON_PAYMENT_REMINDER_2', value: "Send Non-Payment Reminder #2"}
                ], mandatory: true
                },
                paceDialogOption,
                {
                    display: 'Batch Size', key: 'batchSize', type: 'KeyValue', items: [
                    {key: '1', value: "1"},
                    {key: '10', value: "10"},
                    {key: '1000', value: "1,000"},
                    {key: '3000', value: "3,000"},
                    {key: '5000', value: "5,000"},
                    {key: '10000', value: "10,000"},
                    {key: '20000', value: "20,000"},
                    {key: 'ALL', value: "All"},
                ], mandatory: true
                },
                {
                    display: 'Ignore If', key: 'timeUnique', type: 'KeyValue', items: [
                    {key: '86400000', value: "Has been sent in the last 1 day"},
                    {key: '432000000', value: "Has been sent in the last 5 days"},
                ]
                },
                {display: 'Override Lock', key: 'overrideLock', type: 'Boolean', mandatory: false}
            ]
        }
    },
    'MAKE_CREDIT_CAP_PAYMENT': {
        'TITLE_KEY': 'Credit Cap Payment',
        'MESSAGE_KEY': 'Would you like to post Credit Cap Payment?',
        'API_ACTION': 'POST',
        'API_URL': '/api/1/web/customers/pay200/',
        'API_TIMEOUT': 1 * 60 * 1000,
        'VALIDATE_LIST': [],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': false,
        'FORM': {
            'titleKey': 'Credit Cap Payment',
            'okButtonText': 'Post',
            'options': [{
                display: 'Payment Amount', key: 'overridePaymentCents', type: 'KeyValue', items: [
                    {key: '20000', value: "$200"},
                    {key: '1000$', value: "$10 (debug)"},
                    {key: '100$', value: "$1 (debug)"}
                ], mandatory: true
            }, {
                display: 'Override Limit', key: 'overrideLimit', type: 'Boolean', mandatory: false
            }, {
                display: 'Override Usage', key: 'overrideUsage', type: 'Boolean', mandatory: false
            }, {
                display: 'Delayed', key: 'delayed', type: 'Boolean', mandatory: false
            }, {
                display: 'Override Delay', key: 'overrideDelay', type: 'KeyValue', items: [
                    {key: '1800000', value: "30 min"},
                    {key: '900000', value: "15 min (debug)"},
                    {key: '60000', value: "1 min (debug)"},
                    {key: '10000', value: "10 sec (debug)"}
                ], mandatory: false
            }, {
                display: 'Override Lock', key: 'overrideLock', type: 'Boolean', mandatory: false
            }, {
                display: 'Warning Only', key: 'warningOnly', type: 'Boolean', mandatory: false
            }]
        }
    },
    'ADD_ICCIDS': {
        'TITLE_KEY': 'ADD_ICCIDS_TITLE',
        'MESSAGE_KEY': 'ADD_ICCIDS_MESSAGE',
        'API_ACTION': 'PUT',
        'API_URL': '/api/1/web/iccids/put/iccids',
        'VALIDATE_LIST': ['list', 'purpose'],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': false,
        'FORM': {
            'titleKey': 'ADD_ICCIDS_TITLE',
            'okButtonText': 'Save',
            'options': [
                {display: 'ICCIDS (new line seperated)', key: 'list', rows: "4", cols: "5", type: 'Textarea', mandatory: true},
                {
                    display: 'Purpose', key: 'purpose', type: 'KeyValue', items: [
                        {key: 'general', value: 'General'},
                        {key: 'replacement', value: 'Replacement'}
                ], mandatory: true
            }
            ]
        }
    },
    'ADD_NUMBERS': {
        'TITLE_KEY': 'ADD_NUMBERS_TITLE',
        'MESSAGE_KEY': 'ADD_NUMBERS_MESSAGE',
        'API_ACTION': 'PUT',
        'API_URL': '/api/1/web/numbers/put/numbers',
        'VALIDATE_LIST': [],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': false,
        'FORM': {
            'titleKey': 'ADD_NUMBERS_TITLE',
            'okButtonText': 'Save',
            'options': [
                {display: 'Numbers (new line seperated)', key: 'list', rows: "4", cols: "5", type: 'Textarea', mandatory: true},
                {
                    display: 'Type', key: 'type', type: 'KeyValue', items: [
                        {key: 'test', value: 'Test'}
                ], mandatory: true
            }
            ]
        }
    },
    'DELETE_NUMBER_FROM_POOL': {
        'TITLE_KEY': 'DELETE_NUMBER_CONFIRM_TITLE',
        'MESSAGE_KEY': 'DELETE_NUMBER_CONFIRM_MESSAGE',
        'API_ACTION': 'DELETE',
        'API_URL': '/api/1/web/numbers/delete/number',
        'VALIDATE_LIST': ['number'],
        'APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA': false
    },
    'EDIT_GEOLOCATION_PROVIDER': {
        'TITLE_KEY': 'Create / Edit Geolocation Provider',
        'MESSAGE_KEY': 'Would you like to create / save geolocation provider?',
        'API_ACTION': 'PUT',
        'API_URL': '/api/1/web/locations/provider',
        'FORM': {
            'titleKey': 'Create / Edit Geolocation Provider',
            'okButtonText': 'Continue',
            'options': [
                {display: 'Name', key: 'name', type: 'String', mandatory: true},
                {display: 'Enabled', key: 'enabled', type: 'Boolean'},
                {
                    display: 'Notification', key: 'activity', type: 'Autocomplete',
                    url: "/api/1/web/analytics/get/notification/ids/%query", wildcard: "%query", resultKey: "value"
                },
                {display: 'Enabled Redemp.', key: 'generateRedemptionCode', type: 'Boolean'},
                {display: 'Redemp. Title', key: 'redemptionTitle', type: 'String'},
                {display: 'Redemp. Message', key: 'redemptionMessage', type: 'String'},
                {display: 'Redemp. Image', key: 'redemptionImageId', type: 'Upload', fileType: 'Resource'},
                {display: 'Redemp. C. Length', key: 'redemptionCodeLength', type: 'Integer'},
                {display: 'Instruction', key: 'instruction', type: 'String'},
                {display: 'Valid Until', key: 'validUntil', type: 'Date'},
                {display: 'Redeemed Message', key: 'redeemedMessage', type: 'String'},
            ]
        }
    },
    'EDIT_GEOLOCATION_POINT': {
        'TITLE_KEY': 'Create / Edit Geolocation Point',
        'MESSAGE_KEY': 'Would you like to create / save geolocation point?',
        'API_ACTION': 'PUT',
        'API_URL': '/api/1/web/locations/point',
        'FORM': {
            'titleKey': 'Create / Edit Geolocation Point',
            'okButtonText': 'Continue',
            'options': [
                {display: 'Label', key: 'label', type: 'String', mandatory: true},
                {display: 'Latitude', key: 'latitude', type: 'Float', mandatory: false},
                {display: 'Longitude', key: 'longitude', type: 'Float', mandatory: false},
                {display: 'Radius (m)', key: 'radius', type: 'Float', mandatory: false},
                {display: 'Enabled', key: 'enabled', type: 'Boolean'},
                {
                    display: 'Notification', key: 'activity', type: 'Autocomplete',
                    url: "/api/1/web/analytics/get/notification/ids/%query", wildcard: "%query", resultKey: "value"
                },
                {display: 'Enabled Redemp.', key: 'generateRedemptionCode', type: 'Boolean'},
                {display: 'Redemp. Code', key: 'redemptionCode', type: 'String'}
            ]
        }
    },
    'REDEEM_GEOLOCATION_PROMO': {
        'TITLE_KEY': 'Redeem Promo',
        'MESSAGE_KEY': 'Would you like to redeem promo?',
        'API_ACTION': 'PUT',
        'API_URL': '/api/1/web/locations/promo/redeem',
        'FORM': {
            'titleKey': 'Redeem Promo',
            'okButtonText': 'Redeem',
            'options': [
                {display: 'Redmp. Code', key: 'redemptionCode', type: 'String', mandatory: true}
            ]
        }
    },
    'REMOVE_GEOLOCATION_POINT': {
        'TITLE_KEY': 'Remove Geolocation Point',
        'MESSAGE_KEY': 'Would you like remove geolocation point {label}?',
        'API_ACTION': 'DELETE',
        'API_URL': '/api/1/web/locations/point',
        'VALIDATE_LIST': ['id']
    },
    'REMOVE_GEOLOCATION_PROVIDER': {
        'TITLE_KEY': 'Remove Geolocation Provider',
        'MESSAGE_KEY': 'Would you like remove geolocation provider {name}?',
        'API_ACTION': 'DELETE',
        'API_URL': '/api/1/web/locations/provider',
        'VALIDATE_LIST': ['id']
    },
    'REMOVE_GEOLOCATION_PROMO': {
        'TITLE_KEY': 'Remove Geolocation Promo',
        'MESSAGE_KEY': 'Would you like remove geolocation promo?',
        'API_ACTION': 'DELETE',
        'API_URL': '/api/1/web/locations/promo',
        'VALIDATE_LIST': ['id']
    }
};

function generateConfirmationDialog(id, params, reloadCallback, callback, noButtonContent) {
    var tempCustomValidator = params.customValidator;
    delete params.customValidator;
    var paramKeys = Object.keys(params);
    if (params) {
        params = JSON.parse(JSON.stringify(params)); // deep copy
    }

    paramKeys.forEach(function (item) {
        if (!params[item]) {
            params[item] = undefined;
        }
    });

    var form = confirmDialogContent[id].FORM;
    params.customValidator = tempCustomValidator;
    if (!form) {
        generateConfirmationDialogInternal(id, params, reloadCallback, callback, noButtonContent);
    } else {
        generateFormDialogInternal(id, form, params, reloadCallback, callback);
    }
}

function generateConfirmationDialogInternal(id, params, reloadCallback, callback, noButtonContent) {
    if (validateParams(params, confirmDialogContent[id].VALIDATE_LIST)) {
        var title = genUserMessage(confirmDialogContent[id].TITLE_KEY);
        var message = genUserMessage(confirmDialogContent[id].MESSAGE_KEY, params);
        var progressUrl = confirmDialogContent[id].PROGRESS_API;
        var timeout = confirmDialogContent[id].API_TIMEOUT;

        if (confirmDialogContent[id].APPEND_ACCOUNT_AND_SERVICE_INSTANCE_DATA) {
            var url = confirmDialogContent[id].API_URL + params.account
                + "/" + params.serviceInstanceNumber;
        } else {
            var url = confirmDialogContent[id].API_URL;
        }


        switch (id) {
            case 'RE_SUBSC_ADDONS':
                message = genUserMessage(confirmDialogContent[id].MESSAGE_KEY, {
                    number: params.number,
                    schedule: params.schedule ? 'schedule re-subscription' : 're-subscribe'
                });
                break;
            case 'ADD_AUTO_BOOST':
                message = params.enable ? genUserMessage(confirmDialogContent[id].MESSAGE_KEY, params)
                    : genUserMessage(confirmDialogContent[id].MESSAGE_KEY2, params) + (params.checkData ? ""
                    : genUserMessage(confirmDialogContent[id].MESSAGE_KEY3));
                break;
            case 'ADD_PORTIN_BONUS':
                message = genUserMessage(confirmDialogContent[id].MESSAGE_KEY, {
                    number: params.number,
                    extra: params.extra ? " Extra" : ""
                });
                break;
            case 'BLOCK_NOTIFICATIONS':
                message = params.block ? genUserMessage(confirmDialogContent[id].MESSAGE_KEY1) : genUserMessage(confirmDialogContent[id].MESSAGE_KEY2);
                break;
            case 'MODIFY_CIRCLES_TALK':
                switch (params.type) {
                    case 'ADD_CREDIT':
                        message = genUserMessage('ADD_CIRCLES_TALK_CREDIT_MESSAGE', params);
                        break;
                    case 'CHANGE_PLAN':
                        message = genUserMessage('CIRCLES_TALK_CHANGE_PLAN', params);
                        break;
                    case 'REDIRECT_MT':
                        message = genUserMessage('SWITCH_ON_OFF_REDIRECT', {
                            onOrOff: params.mt == "1" ? "ON" : "OFF",
                            mtOrMo: "MT"
                        });
                        break;
                    case 'REDIRECT_MO':
                        message = genUserMessage('SWITCH_ON_OFF_REDIRECT', {
                            onOrOff: params.mo == "1" ? "ON" : "OFF",
                            mtOrMo: "MO"
                        });
                        break;
                }
                break;
        }

        showConfirmationDialog(title, message, confirmDialogContent[id].API_ACTION, params, url,
            reloadCallback, callback, noButtonContent, progressUrl, timeout, id);
    } else {
        if (callback) callback(null, null);
        showToast("warning", genUserMessage('PARAMS_MISSING'));
    }
}

function generateFormDialogInternal(id, form, params, reloadCallback, callback) {
    if (params) {
        form.options.forEach(function (field) {
            if (params[field.key]) {
                field.value = params[field.key];
            } else {
                field.value = undefined;
            }
            if (field.customKey) {
                field.customValue = params[field.customKey];
            } else {
                field.customValue = undefined;
            }
        });
    }
    form.title = genUserMessage(form.titleKey);
    form.key = 'dialogForm';
    form.type = form.okButtonText ? 'CUSTOM' : (params[form.idKey] ? 'Update' : 'Create');
    form.customValidator = params.customValidator ? params.customValidator : undefined;
    form.originalParams = params;

    createFormDialog(form, function (values, close, dialogRef, buttonRef, canceled) {
        if (canceled) {
            if (callback) callback(undefined, undefined, canceled);
            return;
        }

        Object.keys(values).forEach(function (key) {
            params[key] = values[key];
        });

        generateConfirmationDialogInternal(id, params, reloadCallback, function (err, data, canceled) {
            if (canceled) {
                // do nothing
                return;
            }
            if (!err) {
                close();
            }

            if (callback) callback(err, data, canceled);
        });
    });
}

function showConfirmationDialog(title, message, ajaxMethod, ajaxParams, ajaxUrl, reloadCallback,
                                callback, noButtonContent, progressUrl, timeout, dialogId) {
    var btns = [];

    var inProgress;
    var progressInterval;
    var progressTitleId;
    var progressTitleOriginal;

    var startOperation = function () {
        inProgress = true;
        if (progressUrl) {
            if (progressInterval) clearInterval(progressInterval);
            progressTitleOriginal = $("#" + progressTitleId + "_title").html()
            var loadProgress = () => {
                ajax("GET", progressUrl, {}, true, function (data) {
                    if (inProgress) {
                        var progressText = "";
                        if (data.stepsCount >= 0 && data.stepNumber >= 0) {
                            if (progressText) progressText += " - ";
                            progressText += data.stepNumber + "/" + data.stepsCount;
                        }
                        if (data.stepProgress >= 0) {
                            if (progressText) progressText += " - ";
                            progressText += data.stepProgress + "%";
                        }

                        $("#" + progressTitleId + "_title").html(progressTitleOriginal +
                        (progressText ? " (" + progressText + ")" : ""))
                    }
                }, function (code, errorMessage) {
                    if (inProgress) {
                        $("#" + progressTitleId + "_title").html(progressTitleOriginal + " ##progress fetching error##");
                    }
                }, showToast);
            };

            loadProgress();
            progressInterval = setInterval(loadProgress, 500);
        }
    }

    var stopOperation = function () {
        inProgress = false;
        if (progressUrl) {
            if (progressInterval) clearInterval(progressInterval);
            progressInterval = undefined;
            $("#" + progressTitleId + "_title").html(progressTitleOriginal);
        }
    }

    var fileProgressHandler = (key, progress) => {
        if (!key) {
            $("#" + progressTitleId + "_title").html("All files uploaded. Processing main request...");
        } else {
            $("#" + progressTitleId + "_title").html("Uploading '" + key + "'... " + progress + "%");
        }
    }
    
    if(confirmDialogContent[dialogId].FORM && confirmDialogContent[dialogId].FORM.options){
        confirmDialogContent[dialogId].FORM.options.forEach(function(item){
            if(item.customKey){
                delete ajaxParams[item.customKey];
            }
        });
    }

    btns.push({
        label: noButtonContent ? "Yes" : "Confirm",
        action: function (dialog) {
            if (ajaxUrl) {
                var buttonRef = this;
                buttonRef.disable();
                buttonRef.spin();
                dialog.enableButtons(false);
                dialog.setClosable(false);

                uploadDialogFilesIfRequired(ajaxParams, fileProgressHandler, (err) => {

                    if (err) {
                        stopOperation();
                        showToast("danger", "File uploading failed (" + err.message + ")");
                        dialog.close();
                        return;
                    }

                    startOperation();
                    ajax(
                        ajaxMethod,
                        ajaxUrl,
                        ajaxParams, true,
                        function (data) {
                            stopOperation();

                            dialog.enableButtons(true);
                            if (data.message) {
                                showToast("success", data.message);
                            } else {
                                showToast("success", "Success");
                            }
                            if (callback) callback(null, data);
                            buttonRef.enable();
                            buttonRef.stopSpin();
                            dialog.close();
                            if (reloadCallback) {
                                reloadCallback(null, data);
                            }
                        }, function (code, errorMessage) {
                            stopOperation();

                            var error = new Error(errorMessage);
                            error.code = code;
                            dialog.enableButtons(true);
                            buttonRef.enable();
                            buttonRef.stopSpin();
                            dialog.setClosable(true);
                        }, showToast, timeout
                    );
                });
            } else {
                dialog.close();
                if (callback) callback(undefined, undefined);
                if (reloadCallback) {
                    reloadCallback(undefined, undefined);
                }
            }
        }
    });

    if (noButtonContent) {
        btns.push({
            label: "No",
            action: function (dialog) {
                var buttonRef = this;
                buttonRef.disable();
                buttonRef.spin();
                dialog.enableButtons(false);
                dialog.setClosable(false);

                var ajaxParams = noButtonContent.ajaxMethod ? noButtonContent.ajaxMethod : ajaxMethod

                uploadDialogFilesIfRequired(ajaxParams, fileProgressHandler, (err) => {

                    if (err) {
                        stopOperation();
                        showToast("danger", "File uploading failed (" + err.message + ")");
                        dialog.close();
                        return;
                    }

                    startOperation();
                    ajax(
                        ajaxParams,
                        noButtonContent.ajaxUrl ? noButtonContent.ajaxUrl : ajaxUrl,
                        noButtonContent.ajaxParams ? noButtonContent.ajaxParams : ajaxParams, true,
                        function (data) {
                            stopOperation();

                            dialog.enableButtons(true);
                            if (data.message) {
                                showToast("success", data.message);
                            } else {
                                showToast("success", "Success");
                            }
                            if (noButtonContent.callback) {
                                noButtonContent.callback(null, data);
                            } else if (callback) {
                                callback(null, data);
                            }
                            buttonRef.enable();
                            buttonRef.stopSpin();
                            dialog.close();

                            if (noButtonContent.reloadCallback) {
                                noButtonContent.reloadCallback(null, data);
                            } else if (reloadCallback) {
                                reloadCallback(null, data);
                            }
                        }, function (code, errorMessage) {
                            stopOperation();

                            var error = new Error(errorMessage);
                            error.code = code;
                            dialog.enableButtons(true);
                            buttonRef.enable();
                            buttonRef.stopSpin();
                            dialog.setClosable(true);
                        }, showToast, timeout);
                });
            }
        });
    }

    btns.push({
        label: 'Cancel',
        action: function (dialog) {
            dialog.close();
            if (callback) callback(undefined, undefined, true);
        }
    });
    var dialog = BootstrapDialog.show({
        title: title,
        message: message,
        buttons: btns
    });
    progressTitleId = dialog.getId();
}

function uploadDialogFilesIfRequired(params, progressCallback, callback) {
    var filesToUpload = [];
    Object.keys(params).forEach((key) => {
        if (params[key] && params[key].file) {
            filesToUpload.push({
                key: key,
                file: params[key].file,
                type: params[key].type,
                name: params[key].name
            });
        }
    })

    if (filesToUpload.length == 0) {
        if (callback) callback();
    } else {
        uploadDialogFile(0, filesToUpload, params, progressCallback, callback)
    }
}

function uploadDialogFile(position, files, params, progressCallback, callback) {
    if (!files || position >= files.length) {
        if (progressCallback) progressCallback(undefined, 100);
        if (callback) callback();
        return;
    }

    if (!files || !files[position]) {
        uploadDialogFile(position + 1, files, params, progressCallback, callback);
        return;
    }

    var fileToUpload = files[position];
    uploadFile(fileToUpload.file, fileToUpload.type, (progress) => {
        if (progressCallback) progressCallback(fileToUpload.name, progress);
    }, (err, fileId) => {
        params[fileToUpload.key] = fileId ? fileId : undefined;

        if (err) {
            if (progressCallback) progressCallback(fileToUpload.key, 0);

            if (!params.metadata) {
                params.metadata = {};
            }
            if (!params.metadata.errors) {
                params.metadata.errors = [];
            }
            params.metadata.errors.push({
                key: fileToUpload.key,
                error: err.message
            });

            if (callback) callback(err);
            return;
        }

        if (progressCallback) progressCallback(fileToUpload.key, 100);
        uploadDialogFile(position + 1, files, params, progressCallback, callback);
    });
}

function validateParams(params, validateList) {
    var paramKeys = Object.keys(params);
    if (validateList && paramKeys.length < validateList.length) {
        return false;
    } else {
        for (var i = 0; validateList && i < validateList.length; i++) {
            if (paramKeys.indexOf(validateList[i]) == -1) {
                return false;
            }
        }
    }
    return true;
}

function showInfoDialog(title, message, callback) {

    BootstrapDialog.show({
        title: title,
        message: message,
        buttons: [{
            label: 'OK',
            action: function (dialog) {
                dialog.close();
                if(callback){
                    callback();
                }
            }
        }]
    });
}

function showDecisionDialog(title, message, yesCallback, noCallback){
    BootstrapDialog.show({
        title: title,
        message: message,
        buttons: [{
            label: 'YES',
            action: function (dialog) {
                dialog.close();
                if(yesCallback){
                    yesCallback();
                }
            }
        },{
            label: 'NO',
            action: function (dialog) {
                dialog.close();
                if(noCallback){
                    noCallback();
                }
            }
        }]
    });
}

function createFormDialog(params, callback) {
    var buttons = [];
    if (!params.previewOnly) {
        buttons.push({
            label: (params.type === 'CUSTOM'
                ? params.okButtonText : (params.type === 'Update'
                ? 'Update' : params.type === 'Create' ? 'Create ' : 'Confirm')),
            action: function (dialogRef) {
                var buttonRef = this; // 'this' here is a jQuery object that wrapping the <button> DOM element.

                var executeAction = function (result) {
                    if (callback) {
                        callback(result, function () {
                            dialogRef.close();
                        }, dialogRef, buttonRef);
                    } else {
                        dialogRef.close();
                    }
                }

                var showConfirmation = function (result) {
                    BootstrapDialog.show({
                        title: 'Confirmation',
                        message: 'Proceed with the operation?',
                        buttons: [{
                            label: 'Cancel',
                            action: function (dialog) {
                                dialog.close();
                                if (callback) callback(undefined, undefined,
                                    dialog, this, true);
                            }
                        }, {
                            label: 'Confirm',
                            action: function (dialog) {
                                dialog.close();
                                executeAction(result);
                            }
                        }]
                    });
                }

                var parseForm = function () {
                    var body = dialogRef.$modalBody[0];
                    var fields = body.children[0].children[0].children[0].children;
                    var result = parseDivForm(fields, params.customValidator, params.originalParams);
                    if (result) {
                        if (params.confirmation) {
                            showConfirmation(result);
                        } else {
                            executeAction(result);
                        }
                    }
                }

                parseForm();
            }
        });
    }
    buttons.push({
        label: 'Cancel',
        action: function (dialogRef) {
            if (callback) callback(undefined, undefined,
                dialogRef, this, true);
            dialogRef.close();
        }
    });

    BootstrapDialog.show({
        title: params.key === 'dialogForm' ? params.title : (params.type === 'Update'
            ? 'Update ' : params.type === 'Create' ? 'Create ' : '') + params.title,
        message: createDivForm(params),
        buttons: buttons
    });
}

function parseDivForm(fields, customValidator, originalParams) {
    var result = {};
    if (customValidator) {
        var customValidatorResult = customValidator(fields, originalParams);
        if (customValidatorResult !== true) {
            showToast("warning", customValidatorResult);
            return;
        }
    }
    for (var i = 0; i < fields.length; i++) {
        var field = fields[i];
        var item = field.children[1].children[0];
        var name = item.getAttribute('dName');
        var type = item.getAttribute('vType');
        var fileType = item.getAttribute('fType');
        var gt = item.getAttribute('vGt');
        var lt = item.getAttribute('vLt');
        var key = item.getAttribute('name');
        var originalValue = item.getAttribute('value');
        var mandatory = item.getAttribute('mandatory');
        var regex = item.getAttribute('regex');
        var regexMessageKey = item.getAttribute('regexMessageKey');

        var value;
        if (item.tagName == "LABEL") {
            value = originalValue;
        } else if (item.tagName == "DIV" && item.id && item.id.indexOf("autocomplete") >= 0) {
            value = $('#' + item.id + ' input.typeahead.tt-input').val();
        } else if (item.tagName == "FORM" && item.id && item.id.indexOf("upload") >= 0) {
            var filesFieldId = $('#' + item.id)[0].getAttribute("fileinputid");
            var prevFileId = $('#' + item.id)[0].getAttribute("prevFileId");
            var file = $('#' + filesFieldId)[0].files[0];
            if (file) {
                value = {
                    file: file,
                    type: fileType,
                    name: name
                }
            } else if (prevFileId) {
                value = prevFileId;
            } else {
                value = "";
            }
        } else {
            value = item.value ? item.value.trim() : undefined;
        }

        if (!key) {
            continue;
        }

        if (mandatory === "true" && (!value || value.length == 0)) {
            showToast("warning", name + " is mandatory")
            return;
        }

        if (regex) {
            regex = new RegExp(regex);
            var regMatch = regex.exec(value)[0];
            if (regMatch != value) {
                showToast("warning", regexMessageKey ? genUserMessage(regexMessageKey) : "Incorrect field format");
                return;
            }
        }

        if (type === "Integer") {
            value = parseInt(value);
            if ((typeof gt !== 'undefined' && gt !== null) && parseInt(gt) >= value) {
                showToast("warning", name + " should be greater " + gt)
                return;
            }
            if ((typeof lt !== 'undefined' && lt !== null) && parseInt(lt) <= value) {
                showToast("warning", name + " should be less than " + lt)
                return;
            }
        } else if (type === "Float") {
            value = parseFloat(value);
            if ((typeof gt !== 'undefined' && gt !== null) && parseFloat(gt) >= value) {
                showToast("warning", name + " should be greater " + gt)
                return;
            }
        } else if (type === "Boolean") {
            value = parseInt(value) == 1;
        } else if (type === "DateTime") {
            if (value && value !== "") {
                try {
                    value = new Date(new Date(value).getTime()).toISOString();
                } catch (e) {
                    value = undefined;
                }
                if (!value) {
                    showToast("warning", name + " has invalid data format");
                    return;
                }
            }
        }

        result[key] = value;
    }
    return result;
}

function createDivForm(item) {
    var form = "";
    form += "<div>";

    var number = 0;
    item.options.forEach(function (option) {
        var optionDisplayName = option.display;
        var optionKey = option.key;
        var optionType = option.type;
        var optionItems = option.items;
        var optionValue = option.value;
        var optionDisabled = option.disabled;
        var optionDefaultKey = option.defaultKey;
        var optionDefaultSelected = option.defaultSelected;
        var customValue = option.customValue;
        var optionGt = option.gt;
        var optionLt = option.lt;
        var optionUrl = option.url;
        var optionLimit = option.limit;
        var optionResultKey = option.resultKey;
        var optionWildcard = option.wildcard;
        var mandatoryValue = option.mandatory ? option.mandatory : false;
        var optionMetadata = option.metadata;
        var optionRows = option.rows;
        var optionCols = option.cols;
        var optionFileType = option.fileType;
        var regex = option.regex ? option.regex.source : undefined;
        var regexMessageKey = option.regexMessageKey ? option.regexMessageKey : undefined;

        form += "<div class='container'>";
        form += "<div class='cell25'>";
        form += "<label style='padding-top: 5pt'>" + optionDisplayName + "</label>";
        form += "</div>";
        form += (optionType === "File" ? "<div class='cell75-image'>" : "<div class='cell75'>");

        if (optionType === "File") {
            var nameFile = "fileTime" + (number++);
            setTimeout(function () {
                $('#' + nameFile)[0].src = url;
            }, 1000);
            var url = "/api/1/web/customers/doc/" + optionValue;
            form += "<a href='" + url + "' target='_blank'>";
            form += "<img id='" + nameFile + "' src='' style='width:120pt;height:120pt;object-fit:cover;" +
            "background:url(../../res/images/background.png)'>";
            form += "</a>";
        } else if (optionType === "Upload") {
            var nameForm = "uploadId" + (number++);
            var nameFile = nameForm + "File";
            var nameLabel = nameForm + "Label";

            setTimeout(function () {
                var inputs = document.querySelectorAll('.inputfile');
                Array.prototype.forEach.call(inputs, function (input) {
                    var label = input.nextElementSibling;
                    var labelVal = label.innerHTML;

                    input.addEventListener('change', function (e) {
                        var target = e.target;
                        var tagetLabelId = target.getAttribute("labelid");
                        if (tagetLabelId != nameLabel) {
                            return;
                        }

                        var fileNameOriginal = '';
                        var label = $("#" + nameLabel)[0];

                        if (this.files && this.files.length > 1) {
                            fileNameOriginal = this.files.length + " files selected";
                        } else {
                            fileNameOriginal = e.target.value.split('\\').pop();
                        }

                        var fileName;
                        if (fileNameOriginal && fileNameOriginal.length > 26) {
                            fileName = fileNameOriginal.substring(0, 15) + "..." +
                            fileNameOriginal.substring(fileNameOriginal.length - 10, fileNameOriginal.length);
                        } else {
                            fileName = fileNameOriginal;
                        }

                        if (fileName) {
                            label.innerText = fileName;
                        } else {
                            label.innerText = "Choose a file";
                        }
                    });
                });
            }, 1000);

            var fileSelectText = "Choose a file";
            if (optionValue) {
                fileSelectText = "<a href='/api/1/web/file/" + (optionFileType ? optionFileType.toLowerCase() : "undefined") +
                "/" + optionValue + "' target='_blank'>Link</a>";
            }

            form += "<form  id='" + nameForm + "' style='width:60%;' class='inputform' " +
            " name=" + optionKey + " vType='" + optionType + "'" + " fType='" + optionFileType + "'" +
            " dName='" + optionDisplayName + "'" + " mandatory=" + mandatoryValue +
            " onSubmit='event.preventDefault();' fileInputId='" + nameFile + "'" + (optionValue ? "' prevFileId='" + optionValue + "'": "") + ">";
            form += "<input class='inputfile' id='" + nameFile + "' type='file' labelId='" + nameLabel + "'/>";
            form += "<label for='" + nameFile + "'><i class='fa fa-upload'></i></label>";
            form += "<label id='" + nameLabel + "' class='inputlabel'>" + fileSelectText + "</label>";
            form += " </form>";

        } else if (optionDisabled) {
            var originalValue = (optionValue ? optionValue : "");
            var labelValue = originalValue.length > 30 ? originalValue.substring(0, 30) + "..." : originalValue;

            form += "<label style='width:60%; background:#ebebeb'  name=" + optionKey +
            " value='" + originalValue + "'" + " vType='" + optionType + "'" +
            ((typeof optionGt !== 'undefined' && optionGt !== null) ? " vGt='" + optionGt + "' " : " ") +
            " dName='" + optionDisplayName + "'" + " mandatory=" + mandatoryValue +
            " class='form-control' id=selectReadonly>" + labelValue + "</label>";
        } else if (optionType === "Integer") {
            form += "<input type=number style='width:60%' name=" + optionKey +
            " value='" + (optionValue ? optionValue : "0") + "'" +
            " vType='" + optionType + "'" +
            ((typeof optionGt !== 'undefined' && optionGt !== null) ? " vGt='" + optionGt + "' " : " ") +
            ((typeof optionLt !== 'undefined' && optionGt !== null) ? " vLt='" + optionLt + "' " : " ") +
            " dName='" + optionDisplayName + "'" +
            " mandatory=" + mandatoryValue +
            " class=form-control id=selectInteger>";
        } else if (optionType === "Float") {
            form += "<input type=number step='0.000000000000001' style='width:60%' name=" + optionKey +
            " value='" + (optionValue ? optionValue : "0") + "'" +
            " vType='" + optionType + "'" +
            ((typeof optionGt !== 'undefined' && optionGt !== null) ? " vGt='" + optionGt + "' " : " ") +
            " dName='" + optionDisplayName + "'" +
            " mandatory=" + mandatoryValue +
            " class=form-control id=selectInteger>";
        } else if (optionType === "String") {
            form += "<input type=text style='width:60%' name=" + optionKey +
            " value='" + (optionValue ? optionValue : "") + "'" +
            " vType='" + optionType + "'" +
            " dName='" + optionDisplayName + "'" +
            " mandatory=" + mandatoryValue +
            (optionLimit ? " maxlength=" + optionLimit : "") +
            (regex ? " regex=" + regex : "") +
            (regexMessageKey ? " regexMessageKey=" + regexMessageKey : "") +
            " class=form-control id=selectString>";
        } else if (optionType === "Textarea") {
            form += "<textarea name=" + optionKey +
            " rows='" + optionRows + "'" +
            " cols='" + optionCols + "'" +
            " class=form-control id=selectString>" +
            (optionValue ? optionValue : "") +
            "</textarea>";
        } else if (optionType === "Boolean") {
            form += "<select class=form-control style='width:60%' name=" + optionKey +
            " mandatory=" + mandatoryValue +
            " vType='" + optionType + "'" +
            " dName='" + optionDisplayName + "'" +
            " id=selectBoolean>" +
            "<option value='0'>NO</option>" +
            "<option value='1' " + (optionValue ? "selected='selected'" : "") + ">YES</option>" +
            "</select>";
        } else if (optionType === "KeyValue") {
            var optionList = customValue ? getOptions(customValue, (optionValue ? optionValue : optionDefaultSelected), optionDefaultKey)
                : getOptions(optionItems, (optionValue ? optionValue : optionDefaultSelected), optionDefaultKey);
            form += "<select class=form-control style='width:60%' name=" + optionKey +
            " mandatory=" + mandatoryValue +
            " vType='" + optionType + "'" +
            " dName='" + optionDisplayName + "'" +
            " id=selectProductId>" +
            optionList +
            "</select>";
        } else if (optionType === "Autocomplete") {
            var name = "autocomplete" + (number++);
            form += "<div style='width:100%' name=" + optionKey +
            " mandatory=" + mandatoryValue +
            " vType='" + optionType + "'" +
            " dName='" + optionDisplayName + "'" +
            " id='" + name + "' >" +
            "<input class='typeahead form-control' " + (optionValue ? "value='" + optionValue + "' " : "") + " type='text' placeholder=''>" +
            "</div>";

            form += "<script type='text/javascript'>";
            form += "$(function () {";
            form += "   var bestPictures = new Bloodhound({";
            form += "       datumTokenizer: Bloodhound.tokenizers.obj.whitespace('" + optionResultKey + "'),";
            form += "       queryTokenizer: Bloodhound.tokenizers.whitespace,";
            form += "       remote: {";
            form += "           url: '" + optionUrl + "',";
            form += "           wildcard: '" + optionWildcard + "'";
            form += "       }";
            form += "   });";
            form += "   $('#" + name + " .typeahead').typeahead({";
            form += "       highlight: true,";
            form += "       hint: true";
            form += "   }, {";
            form += "       name: 'best-pictures',";
            form += "       display: 'value',";
            form += "       source: bestPictures";
            form += "   });";
            form += "});";
            form += "</script>";
        } else if (optionType === "DateTime") {
            var name = "selectTime" + (number++);
            form += "<input type='text' style='width:60%' class=form-control name=" + optionKey +
            " mandatory=" + mandatoryValue +
            " value='" + (optionValue && optionValue !== "0000-00-00 00:00:00" ? getDateTime(optionValue) : "") + "'" +
            " vType='" + optionType + "'" +
            " dName='" + optionDisplayName + "'" +
            " id='" + name + "'/>";
            form += "<script type='text/javascript'>";
            form += "$(function () {";
            form += "    $('#" + name + "').datetimepicker();";
            form += "});";
            form += "</script>";
        } else if (optionType === "Date" || optionType === "DateMonth") {
            var name = "selectDate" + (number++);
            form += "<input type='text' style='width:60%' class=form-control name=" + optionKey +
            " mandatory=" + mandatoryValue +
            " value='" + (optionValue && optionValue !== "0000-00-00" ? getDate(optionValue) : "") + "'" +
            " vType='" + optionType + "'" +
            " dName='" + optionDisplayName + "'" +
            " id='" + name + "'/>";
            form += "<script type='text/javascript'>";
            form += "$(function () {";
            form += "    $('#" + name + "').datetimepicker({format:"
            + (optionType === "DateMonth" ? "'mm-yyyy'" : "'yyyy-mm-dd'")
            + ", startView:'year', minView:"
            + (optionType === "DateMonth" ? "'year'" : "'month'") + ", autoclose:true});";
            form += "});";
            form += "</script>";
        }
        form += "</div>";
        form += "</div>";
    });

    if (item.button && item.button.update) {
        form += "<div class='container'>";
        form += "<div class='cell'></div>";
        form += "<div class='cell'><input type=submit class=form-control style='width:100%' value=Update></div>";
        form += "</div>";
    }

    form += "</div>";
    form += "";

    return form;
}

function getOptions(optionItems, selectedKey, optionDefaultKey) {
    var options = "<option value=''>" + (optionDefaultKey ? optionDefaultKey : "NONE") + "</option>";
    if (typeof(optionItems[0]) == "object") {
        optionItems.forEach(function (item) {
            options += "<option value='" + item.key + "' "
            + (selectedKey === item.key ? "selected='selected'" : "") + ">"
            + item.value + "</option>";
        });
    } else {
        optionItems.forEach(function (item) {
            options += "<option value='" + item + "' "
            + (selectedKey === item ? "selected='selected'" : "") + ">"
            + item + "</option>";
        });
    }
    return options;
}

function uploadFromUI(uploadFormId) {
    uploadFileFromForm($("#" + uploadFormId)[0]);
}

function uploadFileFromForm(form, callback) {
    if (!form || !form.file || !form.file.files[0]) {
        return callback(new Error("Invalid params, no file to upload"));
    }
    uploadFile(form.file.files[0], "temp", undefined, callback);
}

function uploadFile(file, type, progressCallback, callback) {
    if (!callback) callback = (err, fileId) => {
        showToast(err ? "danger" : "success", err ? err.message : "File has been uploaded, ID " + fileId);
    }

    var xmlhttp = new XMLHttpRequest();
    var operationId = new Date().getTime() + "-" +
        parseInt(Math.random() * 1000000000000);

    if (!file) {
        return callback(new Error("Invalid params, no file to upload"));
    }

    if (!type) {
        type = "temp";
    }

    var formData = new FormData();
    formData.append("operationId", operationId);
    formData.append("content", file);

    xmlhttp.upload.addEventListener("progress", (progressObject) => {
        if (progressObject) {
            var progress = parseInt(progressObject.loaded / progressObject.total * 100);
            if (progressCallback) progressCallback(progress);
        }
    }, false);

    xmlhttp.onreadystatechange = () => {
        if (xmlhttp.readyState != 4) {
            // ignore all statues apart of 4
            return;
        }

        if (xmlhttp.status == 403) {
            // unauthorised
            location.reload();
            return callback(new Error("Unauthorised, can not upload file"));
        }

        if (!xmlhttp.responseText) {
            return callback(new Error("Failed to upload files, empty response"));
        }

        var data = JSON.parse(xmlhttp.responseText);

        if (xmlhttp.status != 200 || (data && data.code != 0)) {
            if (data && data.code != 0) {
                return callback(new Error("Failed to upload file, " +
                (data.error ? "error message: " + data.error : "error code " + data.code)));
            } else {
                return callback(new Error("Failed to upload file, error status " + xmlhttp.status));
            }
        }

        if (!data || !data.fileId) {
            return callback(new Error("Invalid response, no files ID found"))
        }

        return callback(undefined, data.fileId);
    }
    xmlhttp.error = function (e) {
        return callback(e)
    };

    xmlhttp.open("PUT", "/api/1/web/file/" + type.toLowerCase(), true);
    xmlhttp.send(formData);
}
