var mSelectedLogRow;
var mCodesTableRelations;
var mLogContent = {};
var mPageConfig;

function getPage() {
    var href = window.location.href;
    var lastSegment = href.substr(href.lastIndexOf('/') + 1);
    return lastSegment;
}

function getURLConfig(logsType) {
    if (logsType === "logs_gentwo_feedback") {
        return {
            category: "gentwo", type: "feedback", title: "Feedback Logs",
            hasInfoANumber: true, hasInfoBNumber: true, hasFeedbackRating: true, hasLog: false, hasInfoDestination: false,
            hasInfoCallId: true, hasInfoRole: true, hasInfoRate: true, hasInfoDuration: true
        };
    } else if (logsType === "logs_data_aggregation") {
        return {
            category: "data", type: "aggregation", title: "Bonus Aggregation", hasStatus: true,
            hasNumber: true, hasEmail: false, hasInitiator: true, hasLog: false, hasCycle: true
        };
    } else if (logsType === "logs_account_status") {
        return {
            category: "account", type: "status_change", title: "Status Change", hasStatus: true,
            hasNumber: true, hasEmail: false, hasActivity: false, hasLog: false, hasType: true, hasReferrerNumber: true,
            hasReferralBonus: true, hasInitiator: true
        };
    } else if (logsType === "logs_account_creation") {
        return {
            category: "account", type: "creation", title: "Account Creation", hasStatus: true,
            hasNumber: true, hasLog: false, hasType: true, hasPortInStatus: true, hasPortInNumber: true, hasServiceInstanceNo: true
        };
    } else if (logsType === "logs_pay_now") {
        return {
            category: "account",
            type: "pay_now",
            title: "Invoice Payments",
            hasStatus: true,
            hasNumber: true,
            hasBillingAccountNo: true,
            hasInvoiceId: true,
            hasPlatform: true,
            hasECommOrderRef: true,
            hasPaidInvoiceAmounts: true,
            noHtmlExtraFields: {
                hasIdPostfix: true,
                hasStatusVerification: true
            }
        };
    } else if (logsType === "logs_payments_creditcap") {
        return {
            category: "account", type: "creditcap", title: "Credit Cap Payments", hasStatus: true,
            hasNumber: true, hasBillingAccountNo: true, hasECommOrderRef: true, hasPaidAmount: true
        };
    } else if (logsType === "logs_notification_webhook") {
        return {
            hasNotificationId: true, hasServiceInstanceNo: true, category: "notifications",
            type: "webhook", title: "WebHook Notifications", hasStatus: true,
            hasTo: true, hasActivity: true, hasLog: false
        };
    } else if (logsType === "logs_notification_all") {
        return {
            hasNotificationId: true, hasServiceInstanceNo: true, category: "notifications",
            type: "all", title: "All Notifications", hasStatus: true,
            hasTo: true, hasActivity: true, hasLog: false
        };
    } else if (logsType === "logs_notification_sms") {
        return {
            hasNotificationId: true, hasServiceInstanceNo: true, category: "notifications",
            type: "sms", title: "SMS Notifications", hasStatus: true,
            hasTo: true, hasActivity: true, hasLog: false
        };
    } else if (logsType === "logs_notification_push") {
        return {
            hasNotificationId: true, hasServiceInstanceNo: true, category: "notifications",
            type: "push", title: "Push Notifications", hasAppType: true, hasCountDevices: true, hasStatus: true,
            hasTo: true, hasActivity: true, hasLog: false
        };
    } else if (logsType === "logs_notification_email") {
        return {
            hasNotificationId: true, hasServiceInstanceNo: true, category: "notifications",
            type: "email", title: "Email Notifications", hasStatus: true,
            hasTo: true, hasActivity: true, hasEmailContent: true, hasLog: false
        };
    } else if (logsType === "logs_bill_sent") {
        return {
            hasNotificationId: true, hasServiceInstanceNo: true, hasBillingAccountNo: true, category: "bill", type: "sent",
            title: "Sent Bills", hasStatus: true, hasTo: true, hasFile: true, hasLog: false
        };
    } else if (logsType === "logs_paas") {
        return {
            category: "paas", type: "payments", title: "Paas Payments", hasStatus: true,
            hasCustomerAccountNo: true, hasBillingAccountNo: true, hasAmount: true, hasLog: false
        };
    } else if (logsType === "logs_internal_web") {
        return {
            category: "internal", type: "web", title: "Users' Actions", hasUsername: true,
            hasType: true, hasLog: false
        };
    } else if (logsType === "logs_ec_errors") {
        return {
            category: "ec", type: "errors", title: "EliteCore Errors",
            hasStatus: true, hasMethod: true, hasHost: true, hasResponseCode: true, hasLog: false
        };
    } else if (logsType === "logs_portin") {
        return {
            category: "portin", type: "status", title: "Port-In",
            hasStatus: true, hasServiceInstanceNo: true, hasActionType: true, hasPortInStatus: true, hasLog: false
        };
    } else if (logsType === "logs_promo_codes") {
        return {
            category: "promo", type: "codes", title: "Promo Codes",
            hasStatus: true, hasType: true, hasCode: true, hasLog: false
        };
    } else if (logsType === "logs_addon") {
        return {
            category: "addon", type: "addon", title: "Addons", hasServiceInstanceNo: true, hasNumber: true,
            hasStatus: true, hasType: true, hasMethod: true, hasCode: false, hasLog: false
        };
    } else {
        return {
            category: "undefined", type: "undefined", title: "Not Supported Url", hasStatus: true,
            hasNumber: true, hasEmail: true, hasInitiator: false, hasLog: false
        };
    }
}

function reloadLogs() {
    mCodesTableRelations.fnDraw(false);
}

function loadLogs() {
    var pageConfig = getURLConfig(getPage().split(".")[0]);
    mPageConfig = pageConfig;
    loadLogsFromConfig(pageConfig, undefined, "showLogs");
}

function loadLogsDialog(filterConfig, logType){
    var pageConfig = getURLConfig(logType);
    var dialogHtml = `<div class="tableDialogStyle"><table class="table  table-hover general-table" id="logsPopupDialog"></table></div>`;
    BootstrapDialog.show({
        title: "Logs for " + filterConfig.searchString,
        message: dialogHtml,
        size: BootstrapDialog.SIZE_WIDE,
        onshown: function(dialogRef){
            loadLogsFromConfig(pageConfig, filterConfig, "logsPopupDialog");
        },
        buttons: [{
            label: 'Close',
            action: function(dialogItself){
                dialogItself.close();
            }
        }]
    });
}

function loadLogsFromConfig(config, filterConfig, view) {
    if(!filterConfig){
        $(".username")[0].innerHTML = getCookie("cmsuser");
        $("#sectionHeader")[0].innerHTML = config.title + "<span class='tools pull-right'>" +
        "<BUTTON id='downloadBtn' class='btn action-button'onClick='downloadReport();'>DOWNLOAD REPORT</BUTTON></span>";

        $('#filterType').append($('<option>', {value: "NUMBER", text: 'by Number'}));
        $('#filterType').append($('<option>', {value: "EMAIL", text: 'by Email'}));
        $('#filterType').append($('<option>', {value: "ACTIVITY", text: 'by Activity'}));
        $('#filterType').append($('<option>', {value: "SIN", text: 'by Service No.'}));
        $('#filterType').append($('<option>', {value: "CAN", text: 'by Account No.'}));
        $('#filterType').append($('<option>', {value: "BAN", text: 'by Billing No.'}));
        $('#filterType').append($('<option>', {value: "TYPE", text: 'by Type'}));
        $('#filterType').append($('<option>', {value: "STATUS", text: 'by Status'}));
        $('#filterType').append($('<option>', {value: "NOTIFICATION_ID", text: 'by Notification ID'}));
        $('#filterType').append($('<option>', {value: "ORDER_NUMBER", text: 'by Order No.'}));
        $('#filterType').append($('<option>', {value: "INVOICE_ID", text: 'by Invoice ID'}));
        $('#filterType').append($('<option>', {value: "METHOD", text: 'by Method'}));
        $('#filterType').append($('<option>', {value: "USER_NAME", text: 'by User Name'}));
        $('#filterType').append($('<option>', {value: "CODE", text: 'by Code'}));
        $('#filterType').append($('<option>', {value: "REPORT_ID", text: 'by Report Id'}));
        $('#filterType').append($('<option>', {value: "ACCOUNT_STATUS_VALIDATION", text: 'by Validation Status'}));

        $("#logContainer").hide();
        $("#spinContainer").show();
    }

    if(!filterConfig){
        setTimeout(() => {
            $('div.dataTables_filter input').unbind();
            $("div.dataTables_filter input").keyup( function (e) {
                if (e.keyCode == 13) {
                    mCodesTableRelations.fnFilter(this.value);
                }
            });
        }, 1000);
    }

    var columns = buildColumns(config);
    mCodesTableRelations = $("#" + view).DataTable({
        "iDisplayLength": 10,
        "sAjaxSource": "/api/1/web/logs/" + config.category + "/" + config.type + "",
        "bFilter": filterConfig ? false : true,
        "bAutoWidth": false,
        "bProcessing": true,
        "bServerSide": true,
        "aoColumns": columns,
        "bLengthChange": true,
        "bDestroy": true,
        "bSort": false,
        "aaSorting": [[0, 'desc']],
        "fnServerData": function (sSource, aaData, fnCallback) {
            var period = filterConfig ? filterConfig.period : parseInt($("#period").val());
            var filterType = filterConfig ? filterConfig.filterType : $("#filterType").val();
            var matchType = filterConfig ? filterConfig.matchType : $("#matchType").val();
            aaData.push({name: "period", value: period});
            aaData.push({name: "searchType", value: filterType});
            aaData.push({name: "matchType", value: matchType});
            if(filterConfig && filterConfig.searchString){
                aaData.push({name: "sSearch", value: filterConfig.searchString});
            }

            $.ajax({
                "type": "GET",
                "dataType": "json",
                "url": sSource,
                "data": aaData,
                "success": function (response) {
                    if(!response.access.downloadButton){
                        $('#downloadBtn').hide();
                    }
                    var tableViewData = {
                        "iTotalRecords": response.iTotalRecords,
                        "iTotalDisplayRecords": response.iTotalDisplayRecords,
                        "aaData": []
                    };

                    mLogContent = {};
                    if (response.data) {
                        buildOutput(config, response.data, tableViewData.aaData);
                    }
                    fnCallback(tableViewData);
                },
                "error": function (xhr, status, error) {
                    $("#spinContainer").hide();
                    showToast("danger", "Loading error (" + JSON.stringify(error) + ")");
                }
            });
        },
        "fnInitComplete": function (oSettings, json) {
            $("#spinContainer").hide();
            $("#logContainer").show();
        },
        "fnCreatedRow": function (nRow, aData, iDataIndex) {
        },
        "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
            // Cell click
            $('td', nRow).on('click', function () {
                mSelectedLogRow = nRow;
            });
        }
    });
}


function buildColumns(config, noHtml) {

    if (noHtml && config.noHtmlExtraFields) {
        config = JSON.parse(JSON.stringify(config));
        Object.keys(config.noHtmlExtraFields).forEach((key) => {
            config[key] = config.noHtmlExtraFields[key];
        });
    }

    var columns = [];
    if (config.hasId) columns.push({"sTitle": "ID"});
    if (config.hasIdPostfix) columns.push({"sTitle": "ID"});
    columns.push({"sTitle": "Date"});
    if (config.hasPlatform) columns.push({"sTitle": "Platform"});
    if (config.hasECommOrderRef) columns.push({"sTitle": "Order Reference"});
    if (config.hasNotificationId) columns.push({"sTitle": "ID"});
    if (config.hasStatus) columns.push({"sTitle": "Status"});
    if (config.hasCustomerAccountNo) columns.push({"sTitle": "Account No."});
    if (config.hasBillingAccountNo) columns.push({"sTitle": "Billing No."});
    if (config.hasServiceInstanceNo) columns.push({"sTitle": "Service No."});
    if (config.hasUsername) columns.push({"sTitle": "User Name"});
    if (config.hasAmount) columns.push({"sTitle": "Amount"});
    if (config.hasCycle) columns.push({"sTitle": "Cycle"});
    if (config.hasCode) columns.push({"sTitle": "Code"});
    if (config.hasMethod) columns.push({"sTitle": "Method"});
    if (config.hasType) columns.push({"sTitle": "Type"});
    if (config.hasAppType) columns.push({"sTitle": "App Type"});
    if (config.hasActionType) columns.push({"sTitle": "Action Type"});
    if (config.hasCountDevices) columns.push({"sTitle": "Devices"});
    if (config.hasPortInStatus) columns.push({"sTitle": "Port-In Status"});
    if (config.hasActivity) columns.push({"sTitle": "Activity"});
    if (config.hasFile) columns.push({"sTitle": "File"});
    if (config.hasNumber) columns.push({"sTitle": "Number"});
    if (config.hasPortInNumber) columns.push({"sTitle": "Port-In Number"});
    if (config.hasEmail) columns.push({"sTitle": "Email"});
    if (config.hasTo) columns.push({"sTitle": "To"});
    if (config.hasHost) columns.push({"sTitle": "Host"});
    if (config.hasResponseCode) columns.push({"sTitle": "Code"});
    if (config.hasInfoCallId) columns.push({"sTitle": "Call Id"});
    if (config.hasInfoANumber) columns.push({"sTitle": "aNumber"});
    if (config.hasInfoBNumber) columns.push({"sTitle": "bNumber"});
    if (config.hasInfoRole) columns.push({"sTitle": "Role"});
    if (config.hasInfoDuration) columns.push({"sTitle": "Duration"});
    if (config.hasInfoDestination) columns.push({"sTitle": "Destination"});
    if (config.hasInfoRate) columns.push({"sTitle": "Rate"});
    if (config.hasFeedbackRating) columns.push({"sTitle": "Stars"});
    if (config.hasInvoiceId) columns.push({"sTitle": "Invoice Id"});
    if (config.hasPaidAmount) columns.push({"sTitle": "Amount"});
    if (config.hasPaidInvoiceAmounts) columns.push({"sTitle": "Amount"});
    if (config.hasStatusVerification) columns.push({"sTitle": "Activation Suspension"});
    if (config.hasInitiator) columns.push({"sTitle": "Initiator"});
    if (config.hasReferrerNumber) columns.push({"sTitle": "Ref. Number"});
    if (config.hasReferralBonus) columns.push({"sTitle": "Ref. Bonus"});
    if (!config.hasLog) {
        columns.push({"sTitle": ""});
        if (config.hasEmailContent) columns.push({"sTitle": "Content", sWidth: "10%"});
        columns.push({"sTitle": "View", sWidth: "8%"});
    } else {
        if (config.hasLog) columns.push({"sTitle": "Log"});
        if (config.hasEmailContent) columns.push({"sTitle": "Content", sWidth: "10%"});
    }

    var validColumns = config.hasLog ? columns.length - 1 : columns.length - 2;
    var totalSize = config.hasLog ? 40 : 90;
    var columnSize = totalSize / validColumns;
    if (columnSize > 12) {
        columnSize = 12;
    }

    var leftSize = totalSize - columnSize * validColumns;
    for (var i = 0; i < validColumns; i++) {
        if ((columns[i]['sTitle'] === 'ID'
            || columns[i]['sTitle'] === 'Date')) {
            columns[i]["sWidth"] = "10%";
        } else if (columns[i]['sTitle'] === 'Devices'
            || columns[i]['sTitle'] === 'Platform'
            || columns[i]['sTitle'] === 'App Type') {
            columns[i]["sWidth"] = "9%";
            leftSize += columnSize / 2;
        } else if (columns[i]['sTitle'] === 'User Name'
            || columns[i]['sTitle'] === 'Method'
            || columns[i]['sTitle'] === 'Action Type') {
            columns[i]["sWidth"] = "20%";
        } else if ((columns[i]['sTitle'] === 'Email'
            || columns[i]['sTitle'] === 'To'
            || columns[i]['sTitle'] === 'File'
            || columns[i]['sTitle'] === 'Activity'
            || columns[i]['sTitle'] === 'Order Reference'
            || columns[i]['sTitle'] === 'Initiator') && leftSize > 0) {
            columns[i]["sWidth"] = (columnSize + leftSize / 2) + "%";
        } else {
            columns[i]["sWidth"] = columnSize + "%";
        }
    }

    return columns;
}

function buildOutput(config, input, output, noHtml, recordsIds) {

    if (noHtml && config.noHtmlExtraFields) {
        config = JSON.parse(JSON.stringify(config));
        Object.keys(config.noHtmlExtraFields).forEach((key) => {
            config[key] = config.noHtmlExtraFields[key];
        });
    }

    input.forEach(function (item) {
        if (noHtml && recordsIds && item._id) {
            if (recordsIds.indexOf(item._id) >= 0) {
                return;
            } else {
                recordsIds.push(item._id);
            }
        }

        mLogContent[item._id] = item;

        var log = JSON.stringify(item);
        var viewLogs = noHtml ? "" : "<button type='button' class='btn btn-white btn-sm' " +
            "style='border-style:none;'  onClick='viewLog(\"" + item._id + "\");'>" +
            "<i class='fa fa-envelope-open'></i>  View</button>";

        var viewEmail = noHtml ? "" : "<button type='button' class='btn btn-white btn-sm' " +
            "style='border-style:none;'  onClick='viewEmail(\"" + item._id + "\");'>" +
            "<i class='fa fa-email'></i>  Content</button>";

        var values = [];
        if (config.hasId) values.push(getText(item._id));
        if (config.hasIdPostfix) {
            if (item._id) {
                var canCut = item._id.length - 8;
                values.push(canCut > 0 ? "..." + item._id.substring(canCut) : item._id);
            } else {
                values.push(getStatusLabel("NONE", "NONE", noHtml));
            }
        }
        values.push(noHtml ? new Date(item.ts).toISOString() : getDateTime(new Date(item.ts)).replace(" ", "<br>"));
        if (config.hasPlatform) values.push(getText(item.platform));
        if (config.hasECommOrderRef) {
            values.push(getText(item.eCommOrderRef).replace(/,/g, noHtml ? "::" : "<br>"));
        }
        if (config.hasNotificationId) {
            if (item.notificationId) {
                var canCut = item.notificationId.length - 10;
                values.push(canCut > 0 ? "..." + item.notificationId.substring(canCut) : item.notificationId);
            } else {
                values.push(getStatusLabel("NONE", "NONE", noHtml));
            }
        }

        if (config.hasStatus) {
            var statusLabel = "";

            var addLabel = function (status, type) {
                if (status === "NOT_FOUND") status = "NOT FOUND"
                var content = (type ? getText(type) + ": " : "") + getText(status ? status : "UNDEFINED");
                statusLabel += " " + getStatusLabel(status, content, noHtml);
            }

            if (item.status === "OK") {
                if (item.statusSMS) addLabel(item.statusSMS, "SMS");
                if (item.statusEmail) addLabel(item.statusEmail, "EMAIL");
                if (item.statusSelfcare) addLabel(item.statusSelfcare, "CC");
                if (item.statusGentwo) addLabel(item.statusSelfcare, "CT");
                if (item.statusInternal) addLabel(item.statusInternal, "INTERNAL");
            }

            if(item.status){
                if (typeof item.status === 'string' && (item.status.split(",").length > 1)) {
                    item.status.split(",").forEach((status) => {
                        addLabel(status);
                    })
                }else if(typeof item.status === 'object' && item.status.status){
                    addLabel(item.status.status);
                } else {
                    addLabel(item.status);
                }
            }
            

            if (!statusLabel) addLabel(item.status);
            values.push(noHtml ? statusLabel.replace(/  /g, "::") : statusLabel);
        }

        if (config.hasCustomerAccountNo) {
            if (item.customerAccountNumber) {
                values.push(getCustomerAccountLink(item.customerAccountNumber, noHtml));
            } else {
                values.push(getStatusLabel("NONE", "NONE" , noHtml));
            }
        }
        if (config.hasBillingAccountNo) {
            var ban = item.billingAccount ? item.billingAccount : item.billingAccountNumber;
            if (ban) {
                values.push(getCustomerAccountLink(ban, noHtml));
            } else {
                values.push(getStatusLabel("NONE", "NONE" , noHtml));
            }
        }
        if (config.hasServiceInstanceNo) {
            if (item.serviceInstanceNumber) {
                values.push(getCustomerAccountLink(item.serviceInstanceNumber, noHtml));
            } else {
                values.push(getStatusLabel("NONE", "NONE" , noHtml));
            }
        }
        if (config.hasUsername) values.push(getText(item.username));
        if (config.hasAmount) values.push(getText(item.amount));
        if (config.hasCycle) values.push(getText(item.cycle));
        if (config.hasCode) values.push(item.code ? getCustomerCodeLink(item.code, noHtml) : "");
        if (config.hasMethod) values.push(getText(item.method));
        if (config.hasType) values.push(item.type ? getStatusLabel("NEUTRAL", item.type, noHtml) : "");
        if (config.hasAppType) values.push(getStatusLabel("NEUTRAL", item.appType ? item.appType : "NONE", noHtml));
        if (config.hasActionType) values.push(getStatusLabel("NEUTRAL", item.type, noHtml));
        if (config.hasCountDevices) values.push(getText(item.countDevices));
        if (config.hasPortInStatus) {
            if (item.result && item.result.portin) {
                values.push(getStatusLabel(item.result.portin.status, item.result.portin.status, noHtml));
            } else if (item.params) {
                values.push(getStatusLabel(item.params.status, item.params.status, noHtml));
            } else  {
                values.push(getStatusLabel("NONE", "NONE", noHtml));
            }
        }
        if (config.hasActivity) values.push(getText(item.activity));
        if (config.hasFile) {
            if (item.filename) {
                var canCut = item.filename.length - 20;
                values.push(canCut > 0 ? "..." + item.filename.substring(canCut) : item.filename);
            } else {
                values.push(getText(getStatusLabel("NONE", "NONE", noHtml)));
            }
        }
        if (config.hasNumber) {
            if (item.number && item.number != null) {
                values.push(getCustomerNumberLink(item.number, noHtml));
            } else {
                values.push(getStatusLabel("NONE", "NONE", noHtml));
            }
        }
        if (config.hasPortInNumber) {
            if (item.portinNumber && item.portinNumber != null) {
                values.push(getCustomerNumberLink(item.portinNumber, noHtml));
            } else {
                values.push(item.type == "CREATE_ACCOUNT" ? getStatusLabel("NONE", "NONE", noHtml)
                    : getStatusLabel("N/A", "N/A", noHtml));
            }
        }
        if (config.hasEmail) values.push(getText(item.email));
        if (config.hasTo) {
            var numberLink = item.number ? getCustomerNumberLink(item.number, noHtml) : undefined;
            var firstParam = item.serviceInstanceNumber || item.billingAccount ? numberLink : item.email;
            var secondParam = item.serviceInstanceNumber || item.billingAccount ? item.email : numberLink;

            if (item.to) {
                if (/^\d+$/.test(item.to)) {
                    values.push(getCustomerNumberLink(item.to, noHtml));
                } else {
                    values.push(getText(item.to));
                }
            } else if (firstParam) {
                values.push(getText(firstParam));
            } else if (secondParam) {
                values.push(getText(secondParam));
            } else {
                values.push("NONE");
            }
        }
        if (config.hasHost) values.push(getText(item.host));
        if (config.hasResponseCode) {
            if (item.responseCode) {
                values.push(getText(item.responseCode));
            } else {
                values.push(getStatusLabel("NONE", "NONE", noHtml));
            }
        }
        if (config.hasInfoCallId) {
            if (item.callInfo && item.callInfo.callId) {
                var canCut = item.callInfo.callId.length - 10;
                values.push(canCut > 0 ? "..." + item.callInfo.callId.substring(canCut) : item.callInfo.callId);
            } else {
                values.push(getStatusLabel("NONE", "NONE", noHtml));
            }
        }
        if (config.hasInfoANumber) {
            if (item.callInfo && item.callInfo.aNumber) {
                if (item.callInfo.aNumber.length > 8 && !noHtml) {
                    values.push("<A href='" + window.location.origin + "/customers/customers.html?number="
                    + item.callInfo.aNumber.substring(3) + "' target='_blank'>" + item.callInfo.aNumber + "</A>");
                } else {
                    values.push(getText(item.callInfo.aNumber));
                }
            } else {
                values.push(getStatusLabel("NONE", "NONE", noHtml));
            }
        }
        if (config.hasInfoBNumber) {
            if (item.callInfo && item.callInfo.bNnumber) {
                item.callInfo.bNumber = item.callInfo.bNnumber;
            }
            if (item.callInfo && item.callInfo.bNumber) {
                if (item.callInfo.bNumber.length > 8 && !noHtml) {
                    values.push("<A href='" + window.location.origin + "/customers/customers.html?number="
                    + item.callInfo.bNumber.substring(3) + "' target='_blank'>" + item.callInfo.bNumber + "</A>");
                } else {
                    values.push(getText(item.callInfo.bNumber));
                }
            } else {
                values.push(getStatusLabel("NONE", "NONE", noHtml));
            }
        }
        if (config.hasInfoRole) values.push(item.callInfo ? getText(item.callInfo.role) : "NONE");
        if (config.hasInfoDuration) {
            var durationSec = parseInt(item.callInfo.duration);
            var durationMin = parseInt(durationSec / 60);
            durationSec = durationSec - durationMin * 60;
            values.push(item.callInfo ? durationMin + "m " + durationSec + "s" : "NONE");
        }
        if (config.hasInfoDestination) values.push(item.callInfo ? getText(item.callInfo.destination) : "NONE");
        if (config.hasInfoRate) values.push(item.callInfo
            ? getText(item.callInfo.localRate + " SGD / <br>" + item.callInfo.usdRate + " USD") : "NONE");
        if (config.hasFeedbackRating) values.push(item.feedback ? getText(item.feedback.rating) : "NONE");
        if (config.hasInvoiceId) values.push(getText(item.invoiceId).replace(/,/g, noHtml ? "::" : "<br>"));
        if (config.hasPaidAmount) values.push(item.amount ? ("$" + getText(item.amount)) : "$ 0");
        if (config.hasPaidInvoiceAmounts) {
            if (item.invoiceAmounts) {
                values.push((noHtml ? "" : "$ ") + getText(item.invoiceAmounts).replace(/,/g, noHtml ? "::" : "<br>$ "));
            } else {
                values.push((noHtml ? "" : "$ ") + "0");
            }
        }
        if (config.hasStatusVerification) {
            var result;
            if (item.result) result = item.result.statusValidation;
            if (result && result.result) result = result.result;

            var status = result && result.status ? result.status : "NONE";
            values.push(getStatusLabel(status, status, noHtml));
        }
        if (config.hasInitiator) values.push(getText(item.initiator));
        if (config.hasReferrerNumber) values.push(getText(item.referrerNumber));
        if (config.hasReferralBonus) values.push(getText(item.referralBonus));
        if (config.hasLog) {
            if (config.hasEmailContent) values.push(viewEmail);
            values.push(log);
        } else {
            values.push("");
            if (config.hasEmailContent) values.push(viewEmail);
            values.push(viewLogs);
        }

        output.push(values);
    });
}

function downloadReport() {
    createAndDownloadReport({
        period: parseInt($("#period").val()),
        filterType: $("#filterType").val(),
        matchType: $("#matchType").val(),
        searchString: $('div.dataTables_filter input').val()
    }, mPageConfig);
}

function createAndDownloadReport(filterConfig, config) {
    showToast("success", "Report will be created shortly");
    createAndDownloadReportInternal(filterConfig, config)
}

function createAndDownloadReportInternal(filterConfig, config, totalyLoaded, offset, part, recordsIds) {
    var columns = buildColumns(config, true);
    var period = filterConfig.period;
    var filterType = filterConfig.filterType;
    var matchType = filterConfig.matchType;
    var value = filterConfig.searchString;
    var limit = 10000;
    var totalLimit = 200000;

    if (!part) {
        part = 1;
    }
    if (!offset) {
        offset = 0;
    }
    if (!totalyLoaded) {
        totalyLoaded = 0;
    }
    if (!recordsIds) {
        recordsIds = [];
    }

    var projections = {_id: 1, status: 1, type: 1, reportId: 1, platform: 1, billingAccountNumber: 1,
        invoiceId: 1, amount: 1, invoiceAmounts: 1, eCommOrderRef:1, ts: 1, number: 1, "result.statusValidation": 1};

    var aaData = [];
    aaData.push({name: "period", value: period});
    aaData.push({name: "searchType", value: filterType});
    aaData.push({name: "matchType", value: matchType});
    aaData.push({name: "sSearch", value: value});
    aaData.push({name: "iDisplayLength", value: limit});
    aaData.push({name: "iDisplayStart", value: offset});
    aaData.push({name: "noTotalCount", value: true});
    aaData.push({name: "projections", value: JSON.stringify(projections)});

    $.ajax({
        "type": "GET",
        "dataType": "json",
        "url": "/api/1/web/logs/" + config.category + "/" + config.type + "",
        "data": aaData,
        "success": function (response) {
            var output = [];
            var dataRowsCount = 0;
            if (response.data) {
                buildOutput(config, response.data, output, true, recordsIds);
                dataRowsCount = response.data.length;
            }

            console.log("Report: load " + dataRowsCount + " rows, offset is " + offset + ", ", response.data[0]);

            var dataString = "";
            var rows = 0;
            var time = period > 0 ? (period > 24 ? period / 24 + "D" : period + "H") : "ALL";
            var date = new Date().toISOString();
            var skipColumns = [];

            var self = this;
            output.forEach(function (row) {
                if (!self.dataString) {
                    self.dataString = "";
                    self.skipColumns = [];

                    columns.forEach((column, indx) => {
                        var title = column.sTitle.replace(/\./g, ' ').trim().toUpperCase().replace(/ /g, '_');
                        if (!title || title == "VIEW" || title == "CONTENT" || title == "CONTENT") {
                            self.skipColumns.push(indx);
                        } else {
                            self.dataString += title + " ";
                        }
                    });
                    self.dataString += "\n";
                }

                var maxSubRowsCount = 1;
                row.forEach((cell, indx) => {
                    var subRows = cell.split("::");
                    if (subRows.length > maxSubRowsCount) {
                        maxSubRowsCount = subRows.length;
                    }
                    row[indx] = subRows;
                });

                if (maxSubRowsCount > 1) {
                    row.forEach((cell, indx) => {
                        if (cell.length < maxSubRowsCount) {
                            row[indx] = [];
                            for (var i = 0; i < maxSubRowsCount; i++) {
                                row[indx][i] = cell[i] || i == 0 ? cell[i] : row[indx][i - 1];
                            }
                        }
                    });
                }

                for (var i = 0; i < maxSubRowsCount; i++) {
                    row.forEach((cell, indx) => {
                        if (skipColumns.indexOf(indx) == -1) {
                            self.dataString += cell[i].trim().replace(/ /g, '_') + " ";
                        }
                    });
                    self.dataString += "\n";
                    rows++;
                }
            });

            downloadCSV(self.dataString, "Report_"
            + (config.title ? config.title.replace(/ /g, "_") + "_" : "")
            + (value ? filterType.replace(/ /g, "_") + "-" + value.replace(/ /g, "_") + "_" : "")
            + (time ? time.replace(/ /g, "_") + "_" : "")
            + date + (part > 1 ? "_part_" + part : "")
            + "_rows_" + rows + ".csv", "text/csv");

            console.log("Report: report csv files created, rows count is " + rows);

            var shiftCount = dataRowsCount - rows;
            if (shiftCount > 0) {
                showToast("warning", "There is a small shift in the report due to new records, shift count is " + shiftCount);
            }

            if (dataRowsCount < limit) {
                showToast("success", "Report has been successfully created, total rows count is " + (rows + totalyLoaded));
            } else {
                if ((offset + dataRowsCount) >= totalLimit) {
                    showToast("warning", "Report (part #" + part + ") has been successfully created, " +
                    "unfortunately size of report has exceeded possible size, loaded rows count is " + (rows + totalyLoaded), 7000);
                    return;
                }

                showToast("success", "Report (part #" + part + ") has been successfully created, " +
                "current rows count is " + (rows + totalyLoaded) + ", continue...");
                createAndDownloadReportInternal(filterConfig, config, totalyLoaded + rows,
                    offset + dataRowsCount, part + 1, recordsIds);

            }
        },
        "error": function (xhr, status, error) {
            showToast("danger", "Failed to fetch report data");
        }
    });
}

function viewLog(id) {
    if (id && mLogContent && mLogContent[id]) {
        var logObj = JSON.parse(JSON.stringify(mLogContent[id]))
        if (logObj.html) logObj.html = "...Content Hidden...";
        if (logObj.html_internal_1) logObj.html_internal_1 = "...Content Hidden...";

        BootstrapDialog.show({
            size: BootstrapDialog.SIZE_WIDE,
            title: "Log Preview",
            message: JSON.stringify(logObj, null, 4).replace(new RegExp(' ', 'g'), "&nbsp&nbsp"),
            buttons: []
        });
    } else {
        showToast("danger", "Log content is not found");
    }
}

function viewEmail(id) {
    if (id && mLogContent && mLogContent[id]) {
        var logObj = mLogContent[id];
        BootstrapDialog.show({
            size: BootstrapDialog.SIZE_WIDE,
            title: "TO: " + logObj.to + "  FROM: " + logObj.from + "\nSUBJECT: " + logObj.subject,
            message: logObj.html,
            buttons: []
        });
    } else {
        showToast("danger", "Email content is not found");
    }
}
