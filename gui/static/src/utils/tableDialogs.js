var tableDialogContent = {
    'customerNameSearch': {
        'columnDefinitions': [
            {'sTitle': 'Name', 'sClass': 'fivecol-table-column', 'mData': 'accountName', 'mRender': function(data, type, full){
                    return data;
                }
            },
            {'sTitle': 'Account', 'sClass': 'fivecol-table-column', 'mData': 'accountNumber', 'mRender': function(data, type, full){
                    return  (data && data != "None") ? "<a href='/customers/customers.html?account="
                    + data + "'>" + data + "</a>" : "None";
                }
            },
            {'sTitle': 'Service	', 'sClass': 'fivecol-table-column', 'mData': 'serviceInstanceNumber', 'mRender': function(data, type, full){
                    return  (data && data != "None") ? "<a href='/customers/customers.html?account="
                    + data + "'>" + data + "</a>" : "None";
                }
            },
            {'sTitle': 'Number', 'sClass': 'fivecol-table-column', 'mData':'number', 'mRender': function(data, type, full){
                    return  (data && data != "None") ? "<a href='/customers/customers.html?number="
                    + data + "'>" + data + "</a>" : "None";
                }
            },
            {'sTitle': 'Status', 'sClass': 'fivecol-table-column', 'mData':'accountStatus', 'mRender': function(data, type, full){
                    return (data == "ACTIVE") ? getStatusLabel("ACTIVE", "ACTIVE") : getStatusLabel('NOT_ACTIVE', "INACTIVE");
                }
            }
        ],
        'tableId': 'customerNameSearch',
        'title': 'Results',
        'buttons': [{
            label: 'Close',
            action: function(dialogItself){
                dialogItself.close();
            }
        }],
        'pageSize': 5
    },
    'biViewList': {
        'columnDefinitions': [
            {'sTitle': 'Generator', 'sClass': 'fivecol-table-column', 'mData': 'genKey', 'mRender': function(data, type, full){
                    return data;
                }
            },
            {'sTitle': 'Rank', 'sClass': 'fivecol-table-column', 'mData': 'rank', 'mRender': function(data, type, full){
                    return data;
                }
            },
            {'sTitle': 'Actions', 'sClass': 'fivecol-table-column', 'mData':'viewKey', 'mRender': function(data, type, full){
                    if(data){
                        full.rank = full.rank.toString();
                    }
                    return data ? getTableActionButton('Remove', undefined, "REMOVE_KPI_DEF_FROM_VIEW", full) : "";
                }
            }
        ],
        'tableId': 'biViewItems',
        'title': 'BI view Items',
        'buttons': [{
            label: 'Close',
            action: function(dialogItself){
                dialogItself.close();
            }
        }],
        'pageSize': 5
    },
    'apiList': {
        'columnDefinitions': [
            {'sTitle': 'API path', 'sClass': 'fivecol-table-column', 'mData': 'path', 'mRender': function(data, type, full){
                    return data;
                }
            },
            {'sTitle': 'Time(ms)', 'sClass': 'fivecol-table-column', 'mData': 'time', 'mRender': function(data, type, full){
                    return data;
                }
            }
        ],
        'tableId': 'apiListWithLatency',
        'title': 'API calls with Latency',
        'buttons': [{
            label: 'Close',
            action: function(dialogItself){
                dialogItself.close();
            }
        }],
        'pageSize': 10,
        'cssClass': 'api-dialog'
    },
    'customerStatusChangeSchedules': {
        'columnDefinitions': [
            {'sTitle': 'Action', 'sClass': 'three-col-table-column', 'mData': 'action', 'mRender': function(data, type, full){
                    return data;
                }
            },
            {'sTitle': 'Scheduled Date', 'sClass': 'three-col-table-column', 'mData': 'start', 'mRender': function(data, type, full){
                    if(data){
                        return new Date(data).toString().slice(0, 24);
                    }else{
                        return "N/A";
                    }
                }
            },
            {'sTitle': 'Status', 'sClass': 'three-col-table-column', 'mData': 'start', 'mRender': function(data, type, full){
                    if(full.processed){
                        if(full.processed.error){
                            return full.processed.error;
                        }else if(full.processed.statusChanged){
                            return "Success";
                        }else{
                            return "N/A";
                        }
                    }else{
                        return "Pending";
                    }
                }
            }
        ],
        'tableId': 'customerStatusChangeSchedulesTable',
        'title': 'Results',
        'buttons': [{
            label: 'Close',
            action: function(dialogItself){
                dialogItself.close();
            }
        }],
        'pageSize': 5
    },
    anomalyList: {
        columnDefinitions: [
            { sTitle: 'ServiceInstanceNumber', mData: 'serviceInstanceNumber', mRender: (data, type, full) => { return data; } },
            { sTitle: 'EC Bucket', sWidth: '20px', mData: 'bucket', mRender: (data, type, full) => { return data; } },
            { sTitle: 'Kirk History', sWidth: '20px', mData: 'history', mRender: (data, type, full) => { return data; } },
            { sTitle: 'email', mData: 'email', mRender: (data, type, full) => { return data; } },
            { sTitle: 'number', mData: 'number', mRender: (data, type, full) => { return data; } },
        ],
        tableId: 'anomalyList',
        title: 'List of Bonus Anomaly From EC DR',
        buttons: [ {
            label: 'Close',
            action: (dialogItself) => { dialogItself.close(); }
        }],
        pageSize: 10,
    },
};

function generateTableDialog(key, data){
    var dialogHtml = `<div class="tableDialogStyle"><table class="table  table-hover general-table" id="` + tableDialogContent[key].tableId + `"></table></div>`;
    BootstrapDialog.show({
        title: tableDialogContent[key].title,
        message: dialogHtml,
        onshown: function(dialogRef){
            generateTable(tableDialogContent[key].tableId, data, tableDialogContent[key].columnDefinitions, tableDialogContent[key].pageSize);
        },
        buttons: tableDialogContent[key].buttons,
        cssClass: tableDialogContent[key].cssClass ? tableDialogContent[key].cssClass : "",
    });
}
