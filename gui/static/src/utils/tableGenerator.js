function generateKeyValueTable(id, content){
    $('#' + id).DataTable({
        "aaData": sanitizeRows(content),
        "aoColumns": [{"width": "50%"},{"width": "50%"}],
        "bDestroy": true,
        "bFilter": false,
        "bSort": false,
        "bPaginate": false,
        "asStripeClasses":['', ''],
        "bInfo": false
    });
}

function generateTable(id, rows, columnDefs, paginate, filter){
    var dataTableParams = {
        "bDestroy": true,
        "bFilter": filter ? true : false,
        "bSort": false,
        "bPaginate": false,
        "asStripeClasses":['', ''],
        "bInfo": false,
        "bLengthChange": false
    };
    //set empty if no rows
    if((!rows || rows.length == 0) && columnDefs){
        if(columnDefs){
            var tableHeader = "<thead><tr>";
            var nodataRow = "<tr>";
            columnDefs.forEach(function(item, index){
                tableHeader += "<td><B>" + item.sTitle + "</B></td>";
                nodataRow += (index == 0 ? "<td>No Data</td>" : "<td></td>");
            });
            tableHeader += "</tr></thead>";
            nodataRow += "</tr>";
            $("#" + id).html(tableHeader + nodataRow);
        }
    }else{
        rows = sanitizeRows(rows, columnDefs);
        dataTableParams.aaData = rows;
        if(columnDefs){
            dataTableParams.aoColumns = columnDefs;
        }
        if(paginate && rows.length > paginate){
            dataTableParams.bPaginate = true;
            dataTableParams.iDisplayLength = paginate;
        }
        $('#' + id).DataTable(dataTableParams);
    }
}

function addRows(id, rows, columnDefs){
    $('#' + id).dataTable().fnAddData(sanitizeRows(rows, columnDefs));
}

function sanitizeRows(rows, columnDefs){
    if(columnDefs){
        var columnNames = columnDefs.map(function(item){
            return item.mData;
        });
        for(var i = 0; i < rows.length; i++){
            columnNames.forEach(function (colName) {
                if(rows[i][colName] === undefined || rows[i][colName] === null){
                    rows[i][colName] = "None";
                }
            });
        }
    }else{
        for(var i = 0; i < rows.length; i++){
            for(var j = 0; j < rows[i].length; j++){
                if(!rows[i][j]){
                    rows[i][j] = "None";
                }
            }
        }
    }
    return rows;
}