/**
 *  MB   =>  KB    => Discrepancy KB
 * 50MB  => 51200  => 1228.8 KB
 * 100MB => 102400 => 2457.6 KB
 * 150MB => 153600 => 3686.4 KB
 * 200BM => 204800 => 4915.2 KB
 * 250BM => 256000 => 6144 KB
 * 300BM => 307200 => 7372.8 KB
 * 400BM => 409600 => 9830.4 KB
 * 500BM => 512000 => 12288 KB
 * 600BM => 614400 => 14745.6 KB
 * 700BM => 716800 => 17203.2 KB
 * 750BM => 768000 => 18432 KB
 * 800BM => 819200 => 19660.8 KB
 * 900BM => 921600 => 22118.4 KB
 */

if(window.location.pathname != "/login.html" && getCookie("cmsuser").length == 0){
    location.href = "/login.html";
}

var DISCREPANCY = {
    51200: 1228.8,
    102400: 2457.6,
    153600: 3686.4,
    204800: 4915.2,
    256000: 6144,
    307200: 7372.8,
    409600: 9830.4,
    512000: 12288,
    614400: 14745.6,
    716800: 17203.2,
    768000: 18432,
    819200: 19660.8,
    921600: 22118.4
};

var WEB_loadingFromNavigation;
var DEFAULT_RELOAD_CALLBACK;
var CURRENT_PAGE_LOADING = false;

function getTime(t) {
    if (!t) return "Invalid";
    var h = t.getHours();
    var m = t.getMinutes();
    var s = t.getSeconds();

    if ((h + '') === 'NaN') {
        return 'INVALID';
    } else {
        return ('0' + h).slice(-2) + ":" + ('0' + m).slice(-2) + ":" + ('0' + s).slice(-2);
    }
}

function getDate(t) {
    if (!t) return "Invalid";
    if(typeof(t) == "string") t = new Date(t);
    var y = t.getFullYear();
    var m = t.getMonth() + 1;
    var d = t.getDate();

    if ((y + '') === 'NaN') {
        return 'INVALID';
    } else {
        return y + "-" + ('0' + m).slice(-2) + "-" + ('0' + d).slice(-2);
    }
}

function getDateTime(d) {
    if (!d) return "";
    var t = new Date(d);
    return getDate(t) + " " + getTime(t);
}

function s2time(s) {
    if (!s) return "00:00:00";
    var date = new Date(null);
    date.setSeconds(s); // specify value for SECONDS here
    return date.toISOString().substr(11, 8);
}

function time2s(s) {
    if (!s) return 0;
    var h = parseInt(s.split(':')[0]);
    var m = parseInt(s.split(':')[1]);
    var s = parseInt(s.split(':')[2]);
    return (h * 60 * 60) + (m * 60) + s;
}

function getText(t) {
    if (t == "undefined") return "";
    if (!t) return "";
    if (t instanceof Object) return "";
    return t + "";
}

function getFormattedInt(v) {
    if (v > 0) {
        var result = '';
        while (v > 0) {
            while (result.length % 3 > 0) {
                result = '0' + result;
            }

            if (result) result = ',' + result;
            var left = '' + v % 1000;

            result = left + result;
            v = parseInt(v / 1000);
        }
        return result;
    }
    return "0";
}

var customErrorMessage = [
    {
        substring: "FOREIGN KEY (`code_pattern_level_1`) REFERENCES `promoCodes` (`code`))",
        message: "Code pattern L1 does not exists / Can not remove or change name of a code used as a pattern"
    },
    {
        substring: "FOREIGN KEY (`code_pattern_level_2`) REFERENCES `promoCodes` (`code`))",
        message: "Code pattern L2 does not exists / Can not remove or change name of a code used as a pattern"
    },
    {
        substring: "FOREIGN KEY (`code_pattern_level_3`) REFERENCES `promoCodes` (`code`))",
        message: "Code pattern L3 does not exists / Can not remove or change name of a code used as a pattern"
    },
    {
        substring: "FOREIGN KEY (`providerId`) REFERENCES `locationProviders` (`id`)",
        message: "Provider(s) can not be remove (changed) since it has points associated with that point(s)"
    }
]

function ajax(method, path, params, reload403, callback, errorCallback, toastCallback, timeout) {
    $.ajax({
        "dataType": "json",
        "type": method,
        "url": path,
        "data": params,
        "success": function (data) {
            callback(data)
        },
        timeout: timeout ? timeout : 30000,
        "error": function (xhr, status, error) {
            if (xhr.status != 403 && xhr.status != 400 && !WEB_loadingFromNavigation) {
                if (toastCallback) {
                    toastCallback("danger", "Operation failed (" + xhr.status + " " + error + ")");
                } else {
                    alert("Operation failed (" + error + ")");
                }
                if (errorCallback) {
                    errorCallback(xhr.status, xhr.responseText);
                }
            }
        },
        "statusCode": {
            403: function (error) {
                if (reload403) {
                    location.href = "/login.html";
                } else {
                    if (toastCallback) {
                        toastCallback("danger", "Unauthorised");
                    } else {
                        alert("Failed");
                    }
                }
                if (errorCallback) {
                    errorCallback(403, error.responseJSON);
                }
            },
            400: function (error) {
                if (!error.responseJSON || !error.responseJSON.error) {
                    if (toastCallback) {
                        toastCallback("danger", "Unknown error");
                    } else {
                        alert("Operation failed");
                    }
                } else {
                    for (var i = 0; i < customErrorMessage.length; i++) {
                        if (error.responseJSON.error.indexOf(customErrorMessage[i].substring) >= 0) {
                            error.responseJSON.error = customErrorMessage[i].message;
                        }
                    }

                    if (toastCallback) {
                        toastCallback("danger", error.responseJSON.error);
                    } else {
                        alert("Operation failed (code=" + error.responseJSON.code
                            + ", error=" + error.responseJSON.error + ")");
                    }
                }
                if (errorCallback) {
                    errorCallback(400, error.responseJSON);
                }
            }
        }
    });
}

function showAPI(api) {
    $("#showAPI tbody").empty();
    api.forEach(function (item) {
        $("#showAPI > tbody:last-child").append("<TR>" +
            "<TD>" + item.transport + "</TD>" +
            "<TD>" + item.method + "</TD>" +
            "<TD>" + item.api + "</TD>" +
            "<TD>" + item.parameters + "</TD>" +
            "<TR>");
    });
}

function menu() {
    $.ajax({
        url: "/api/1/web/menu/get",
        type: "GET",
        dataType: "json",
        data: {},
        success: function (data) {
            if (data.code == 0) {
                data.list.forEach(function (li) {
                    if (li.sub) {
                        var lisub = "<UL class='sub'>";
                        li.sub.forEach(function (sub) {
                            lisub += "<LI><A href='" + window.location.origin + '/' + sub.href + "'>" + sub.span + "</A></LI>";
                        });
                        lisub += "</UL>";
                        $("#nav-accordion").append("<LI class='sub-menu'>" +
                            "<A href='javascript:;'>" +
                            "<I class='" + li.class + "'></I>" +
                            "<SPAN>" + li.span + "</SPAN>" +
                            "</A>" + lisub +
                            "</LI>");
                    } else {
                        $("#nav-accordion").append("<LI>" +
                            "<A href='" + li.href + "'>" +
                            "<I class='" + li.class + "'></I>" +
                            "<SPAN>" + li.span + "</SPAN>" +
                            "</A>" +
                            "</LI>");
                    }
                });
                $('#nav-accordion').dcAccordion({		// reinitialize accordion
                    eventType: 'click',
                    autoClose: true,
                    saveState: true,
                    disableLink: true,
                    speed: 'slow',
                    showCount: false,
                    autoExpand: true,
                    classExpand: 'dcjq-current-parent'
                });

            } else window.location.href = "/login.html";
        },
        error: function (error) {
            //			alert(error);
        }
    });
    return true;
}

function logout(){
    removeFromStorage("cmsuser");
    window.location.href='/'
}

function addToStorage(key, value, expires){

    if(window.localStorage){

        window.localStorage.cookie = window.localStorage.cookie || '{}';

        let cookie = JSON.parse(window.localStorage.cookie);
        cookie[key] = {value,expires};

        window.localStorage.cookie = JSON.stringify(cookie);
    }
}

function removeFromStorage(key){
    if(window.localStorage){
        let cookie = JSON.parse(window.localStorage.cookie);
        delete cookie[key];
        window.localStorage.cookie = JSON.stringify(cookie);
    };
}

function login(form) {
    if (!$("#login-form").valid()) return false;
    $.ajax({
        url: "/api/1/web/auth/login",
        type: "POST",
        dataType: "json",
        data: {"user": form.user.value, "secret": form.secret.value, "update": form.update.value},
        success: function (data) {
            if (data.code == 0) {
                var d = new Date();
                d.setTime(d.getTime() + (8 * 60 * 60 * 1000));	// 8 hours
                addToStorage("cmsuser", data.user, d.toISOString());
                window.location.href = "/dashboard/" + data.default;
            } else if (data.code == 1) {
                $(".update").show();
                $("#update").focus();
            } else alert("Fail");
        },
        error: function (error) {
            //			alert(error);
        }
    });
}
//This is something like read from storage.
function getCookie(key){
    let result = "";
    if(window.localStorage){
        if(window.localStorage.cookie) {
            let cookie = null;
            try {
                cookie = JSON.parse(window.localStorage.cookie);
            }catch(ex){

            }
            if(cookie){
                let obj = cookie[key];
                if (obj) {
                    if(obj.expires){
                        if(new Date(obj.expires) > (new Date())) result = obj.value;
                    }
                    else result = obj.value;
                }
            }
        }
    }
    return result;
}

function downloadCSV(content, fileName, mimeType) {
    var a = document.createElement('a');
    mimeType = mimeType || 'application/octet-stream';

    if (navigator.msSaveBlob) { // IE10
        return navigator.msSaveBlob(new Blob([content], {type: mimeType}), fileName);
    } else if ('download' in a) { //html5 A[download]
        a.href = 'data:' + mimeType + ',' + encodeURIComponent(content);
        a.setAttribute('download', fileName);
        document.body.appendChild(a);
        setTimeout(function () {
            a.click();
            document.body.removeChild(a);
        }, 66);
        return true;
    } else { //do iframe dataURL download (old ch+FF):
        var f = document.createElement('iframe');
        document.body.appendChild(f);
        f.src = 'data:' + mimeType + ',' + encodeURIComponent(content);

        setTimeout(function () {
            document.body.removeChild(f);
        }, 333);
        return true;
    }
}

function showToast(type, message, delay) {
    var delayDefault = 4000;
    if (delay) {
        delayDefault = delay;
    }

    $.notify({
        // options
        message: message
    }, {
        type: type,
        offset: 20,
        spacing: 10,
        z_index: 1051,
        delay: delayDefault,
        timer: 1000,
        placement: {
            from: "top",
            align: "right"
        }
    });
}

function packageSort(a, b, bySizeOnly) {
    if (typeof a === "object") {
        var typeDefault = 0;
        var typeLoyalty = 1;
        var typeCCI = 2;
        var typeCTI = 3;
        var typeSurprise = 4;
        var typeBirthday = 5;
        var typeReferral = 6;
        var typePortIn = 7;
        var typeRecovery = 8;
        var typeContractBuster = 9;
        var typeCommon = 10;
        var typeGeneral = 11;
        var typeRecurrent = 12;

        var getType = function (name) {
            if (!name || (typeof name) !== "string") {
                return typeDefault;
            }

            if (name.indexOf(" Loyalty") > -1) {
                return typeLoyalty;
            } else if (name.indexOf(" Circles Care Install") > -1) {
                return typeCCI;
            } else if (name.indexOf(" Circles Talk Install") > -1) {
                return typeCTI;
            } else if (name.indexOf(" Surprise") > -1) {
                return typeSurprise;
            } else if (name.indexOf(" Birthday") > -1) {
                return typeBirthday;
            } else if (name.indexOf(" Referral") > -1) {
                return typeReferral;
            } else if (name.indexOf(" Port In") > -1) {
                return typePortIn;
            } else if (name.indexOf(" Recovery") > -1) {
                return typeRecovery;
            } else if (name.indexOf(" Contract Buster") > -1) {
                return typeContractBuster;
            } else if (name.indexOf(" Common") > -1) {
                return typeCommon;
            } else if (name.indexOf(" General") > -1) {
                return typeGeneral;
            } else if (name.indexOf(" R") > -1 && name.indexOf(" R ") == -1) {
                return typeRecurrent;
            } else {
                return typeDefault;
            }
        };

        var aType = getType(a.name);
        var bType = getType(b.name);

        if (aType == bType || bySizeOnly) {
            if (a.recurrent != b.recurrent) {
                return a.recurrent ? 1 : -1;
            }

            if (a.kb && b.kb) {
                return a.kb - b.kb;
            }

            if (a.name < b.name) return -1;
            else if (a.name > b.name) return 1;
            else return 0;
        } else {
            return aType - bType;
        }
    } else {
        if (a < b) return -1;
        else if (a > b) return 1;
        else return 0;
    }
}

function tableify(source, resultView, requiredHeader) {
    var data = source ? source.val() : "";
    var result = [];

    if (data == "") {
        return;
    }

    var columns = [];
    var validate = function (idx, cell) {
        if (columns[idx] == "email") {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return regex.test(cell);
        } else if (columns[idx] == "number") {
            return (parseInt(cell) == cell);
        } else if (columns[idx] == "action") {
            var allowedAction = ["addon", "notification", "suspend", "terminate", "activate"];
            return (allowedAction.indexOf(cell) > -1);
        } else if (cell.trim() == "") {
            return false;
        } else {
            return true;
        }
    }

    var rows = data.split("\n");
    if (rows[rows.length - 1] == "") {
        // remove empty last line
        rows.splice(rows.length - 1, 1);
    }

    var thead = $('<thead />');
    var tbody = $('<tbody />');
    for (var y in rows) {
        if (y == 0) {
            var cells = rows[y].split("\t");
            for (var x in cells) {
                var idxRequired = requiredHeader.indexOf(cells[x].toLowerCase());
                if (idxRequired > -1) {
                    // required header
                    thead.append('<th>' + cells[x] + '</th>');
                    requiredHeader.splice(idxRequired, 1);
                } else if (cells[x] != "") {
                    // not blank
                    thead.append('<th>' + cells[x] + '</th>');
                } else {
                    // unknown header
                    thead.append('<th class=invalid>' + cells[x] + '</th>');
                }
                columns.push(cells[x]);
            }
            if (requiredHeader.length > 0) {
                thead.append('<th class=invalid>Missing Header(s) ' + requiredHeader + '</th>')
            }

        } else {
            var cells = rows[y].split("\t");
            var row = $('<tr />');
            var item = {};
            for (var x in cells) {
                var valid = validate(x, cells[x]);
                if (!valid) {
                    row.append('<td class=invalid>' + cells[x] + '</td>');
                } else {
                    item[columns[x].toLowerCase()] = cells[x];
                    row.append('<td>' + cells[x] + '</td>');
                }
            }
            result.push(item)
            tbody.append(row);
        }
    }

    if (source) {
        source.hide();
    }
    if (resultView) {
        resultView.append(thead);
        resultView.append(tbody);
    }

    if ($(".invalid").length > 0) {
        $(".invalid").css({backgroundColor: 'red'});
        return undefined;
    } else {
        return result;
    }
}

function getCorrectedKB(kb) {
    var key = "" + kb;
    var discrepancyKb = DISCREPANCY[key] ? DISCREPANCY[key] : 0;
    return kb + discrepancyKb;
}

function convertKToGb(kb) {
    return Math.round((kb / 1024 / 1024) * 100) / 100;
}

function getInitiatorLabel(initiator) {
    if (!initiator) {
        return "N/A";
    }

    return getStatusLabel("NONE", initiator.replace("][", ": ").replace(/[\[\]]+/g, ""));
}

function getStatusLabel(status, content, noHtml) {
    if (!status) {
        status = "N/A";
    }
    if (noHtml) {
        return content ? content : status;
    }

    if (!content) {
        content = status;
    }

    if (content.indexOf("ERROR_") == 0
        || content.indexOf(" ERROR_") >= 0
        || content.indexOf(":ERROR_") >= 0
        || content.indexOf(": ERROR_") >= 0) {
        content = content.replace("ERROR_", "");
    }
    if (content.indexOf("RECOVERED_") == 0) {
        status = "RECOVERED";
    }
    if (content.indexOf("PF_") == 0
        || content.indexOf(" PF_") >= 0
        || content.indexOf(":PF_") >= 0
        || content.indexOf(": PF_") >= 0) {
        content = content.replace("PF_", "");
    }

    content = content.replace(/_/g, ' ');
    status = status.replace(/ /g, '');

    if (!status) {
        return "<label class='status status-ignored'>" + content + "</label>"
    } else if (status === "SENDING" || status === "NO" || status === "WARNING" || status === "ERROR_OPERATION_NOT_SUPPORTED"
        || status === "IGN_MULTI" || status === "RESTRICT" || status === "OFF" || status === "RECOVERED" || status=="BLOCKED"
        || status=="UNFINISHED") {
        return "<label class='status status-warning'>" + content + "</label>";
    } else if (status === "SENT" || status === "PAID" || status === "NEGATIVE" || status === "OK" || status === "YES" || status === "ON"
        || status === "UPDATED" || status === "ACTIVE" || status === "ADDED_WAIVER" || status === "STATUS_CHANGE_NOT_REQUIRED") {
        return "<label class='status status-success'>" + content + "</label>";
    } else if (status === "UNKNOWN" || status === "NOT_SENT" || status === "IGNORED" || status === "NEUTRAL" || status === "N/A") {
        return "<label class='status status-ignored'>" + content + "</label>"
    } else if (status === "NOT FOUND" || status === "NOT_FOUND" || status === "NOT_REQUIRED") {
        return "<label class='status status-not-found'>" + content + "</label>"
    } else if (status === "WAITING" || status === "WARNING") {
        return "<label class='status status-not-found'>" + content + "</label>"
    } else if (status === "DELAYED" || status === "IN_PROGRESS" || status === "DONOR_APPROVED"
        || status === "FAILURE_POSTPONED" || status === "NEUTRAL2") {
        return "<label class='status status-in-progress'>" + content + "</label>"
    } else if (status === "UNPAID" || status === "FAILED" || status === "REJECTED"
        || status === "NOT_ACTIVE" || status === "MISSING" || status === "INVALID") {
        return "<label class='status status-error'>" + content + "</label>"
    } else if (status === "CANCELED") {
        return "<label class='status status-canceled'>" + content + "</label>"
    } else if (status === "DONE" || status === "CREATED" || status === "REVERTED") {
        return "<label class='status status-success'>" + content + "</label>"
    } else if (status === "PENDING" || status === "WAITING_APPROVE" || status === "WAITING_RETRY") {
        return "<label class='status status-warning'>" + content + "</label>"
    } else if (status === "NONE" || status === "NO_WAIVER") {
        return "<label class='status status-transparent'>" + content + "</label>"
    } else {
        return "<label class='status status-error'>" + content + "</label>"
    }
}

function getCustomerAccountLink(serviceInstanceNumber, noHtml) {
    if (noHtml) return serviceInstanceNumber;
    return "<A href='" + window.location.origin + "/customers/customers.html?account=" + serviceInstanceNumber
        + "' target='_blank'>" + serviceInstanceNumber + "</A>"
}

function getPdfBillUrl(label, billFullPath, noHtml) {
    if (noHtml) return billFullPath;
    if(!billFullPath){
        return label;
    }
    return "<A href='" + window.location.origin + "/api/1/web/invoices/get/file?path=" + encodeURIComponent(billFullPath)
        + "' target='_blank'>" + label + "</A>"
}

function getCustomerNumberLink(number, noHtml) {
    if (noHtml) return number;
    return "<A href='" + window.location.origin + "/customers/customers.html?number=" + number
        + "' target='_blank'>" + number + "</A>"
}

function getCustomerCodeLink(code, noHtml) {
    if (noHtml) return code;
    return "<A href='" + window.location.origin + "/customers/customers.html?referralCode=" + code
        + "' target='_blank'>" + code + "</A>"
}

function getCustomerReferralCodeLink(referralCode, noHtml) {
    if (noHtml) return referralCode;
    return "<A href='" + window.location.origin + "/customers/customers.html?referralCode=" + referralCode
        + "' target='_blank'>" + referralCode + "</A>"
}

function getActionButton(id, title, style, dialogId, params, reloadCallback, callback) {
    if (!id) {
        id = title + "_" + style + "_" + dialogId + "_" + (new Date().getTime());
    }
    var iconClass;
    if (style === "EDIT") {
        iconClass = "fa fa-pencil";
    }
    var button = "<button type='button' class='btn action-button' " +
        "id='" + id + "'>" + (iconClass ? "<i class='" + iconClass + "'></i>  " : "")
        + (title ? title.toUpperCase() : "") + "</button>";

    $("#" + id).click(function () {
        generateConfirmationDialog(dialogId, params, reloadCallback, callback);
    });

    return button;
}

function getOptionSelect(id, options, style, selectedKey, disableEmptyOption) {
    var selectionClass = style ? 'form-control input-sm ' + style : 'form-control input-sm';
    var optionHtml = "<SELECT id = '"+ id +"' class='" + selectionClass + "'>" + (disableEmptyOption ? "" : "<OPTION></OPTION>");
    if(typeof(options[0]) == "object"){
        options.forEach(function(item){
            optionHtml += "<OPTION value='" + item.key + "' " + (selectedKey == item.key ? "selected" : "")
                + ">" + (item.value ? item.value : item.key) + "</OPTION>";
        });
    }else{
        options.forEach(function(item){
            optionHtml += "<OPTION value='" + item + "' " + (selectedKey == item ? "selected" : "") + ">" + item + "</OPTION>";
        });
    }
    optionHtml += "</SELECT>";
    return optionHtml;
}

function getOptionSelectWithDisabledOptions(id, options, style, selectedKey, disableEmptyOption, disabledKeys) {
    var selectionClass = style ? 'form-control input-sm ' + style : 'form-control input-sm';
    var optionHtml = "<SELECT id = '"+ id +"' class='" + selectionClass + "'>" + (disableEmptyOption ? "" : "<OPTION></OPTION>");
    if(typeof(options[0]) == "object"){
        options.forEach(function(item){
            var disabled = (disabledKeys && disabledKeys.length > 0 && disabledKeys.indexOf(item.key) != -1) ? "disabled" : "";
            optionHtml += "<OPTION value='" + item.key + "' " + (selectedKey == item.key ? "selected" : "")
                + " " + disabled + ">" + (item.value ? item.value : item.key) + "</OPTION>";
        });
    }else{
        options.forEach(function(item){
            var disabled = (disabledKeys && disabledKeys.length > 0 && disabledKeys.indexOf(item) != -1) ? "disabled" : "";
            optionHtml += "<OPTION value='" + item + "' " + (selectedKey == item ? "selected" : "") + " " + disabled + ">" + item + "</OPTION>";
        });
    }
    optionHtml += "</SELECT>";
    return optionHtml;
}

function replaceOptionsOfSelect(id, options, selectedKey) {
    var optionHtml = "<OPTION></OPTION>";
    if(typeof(options[0]) == "object"){
        options.forEach(function(item){
            optionHtml += "<OPTION value='" + item.key + "' " + (selectedKey == item.key ? "selected" : "")
                + ">" + (item.value ? item.value : item.key) + "</OPTION>";
        });
    }else{
        options.forEach(function(item){
            optionHtml += "<OPTION value='" + item + "' " + (selectedKey == item ? "selected" : "") + ">" + item + "</OPTION>";
        });
    }
    optionHtml += "</SELECT>";
    $('#' + id).empty().append(optionHtml);
}

function setOptionSelectValue(id, key){
    $('#' + id + ' option[value=' + key + ']').attr('selected','selected');
}

function getTextInput(id, type, size, maxLength){
    return "<INPUT id=" + id + " name=newPortInNumber class='form-control number-input' type='"
        + type + "' size='" + size + "' maxlength='" + maxLength + "'>"
}

function getTableActionButton(title, style, dialogId, params, method) {
    if (!params) params = {};

    var iconClass;
    if (style === "SENDING") {
        iconClass = "fa fa-send";
    } else if (style === "UPLOAD") {
        iconClass = "fa fa-upload";
    } else if (style === "EDIT") {
        iconClass = "fa fa-pencil";
    } else if (style === "VIEW") {
        iconClass = "fa fa-eye";
    } else if (style === "REMOVE") {
        iconClass = "fa fa-times";
    } else if (style === "REFRESH") {
        iconClass = "fa fa-refresh";
    }

    if (!method) {
        method = "onButtonClick";
    }

    return "<button type='button' class='btn action-button' " +
        "onClick='" + method + "(\"" + dialogId + "\",\"" + JSON.stringify(params).replace(/'/g, "%27").replace(/\"/g, "\\\"") + "\");'>" +
        (iconClass ? "<i class='" + iconClass + "'></i>  " : "") + (title ? title.toUpperCase() : "") + "</button>"
}

function getTableActionButtonWithoutCallback(btnId, title, style) {
    var iconClass;
    if (style === "EDIT") {
        iconClass = "fa fa-pencil";
    } else if (style === "VIEW") {
        iconClass = "fa fa-eye";
    }

    return "<button id='" + btnId + "' type='button' class='btn action-button' "
        +(iconClass ? "<i class='" + iconClass + "'></i>  " : "") + ">" +(title ? title.toUpperCase() : "") + "</button>"
}

function getLabelSwitch(id, checked, label, styleClass){
    return "<INPUT id='" + id + "' type=checkbox " + (checked ? "checked" : "") + " class='switch-mini noNotif " +
        (styleClass ? styleClass : "") + "' data-text-label='" + label + "'>";
}

function onButtonClick(dialogId, paramsText) {
    generateConfirmationDialog(dialogId, parseButtonParams(paramsText), DEFAULT_RELOAD_CALLBACK);
}

function parseButtonParams(paramsText) {
    return paramsText ? JSON.parse(paramsText.replace(/%27/g, "'")) : {};
}

function showLoadingLabel() {
    if (!$("#loading-label").length) {
        $('#main-content').prepend('<div id="loading-label"><img id="loading-label-image" src="../../res/images/loading-label.gif" /></div>');
    }
}

function hideLoadingLabel() {
    if ($("#loading-label").length) {
        $('#loading-label').remove();
    }
}

function getColorForStatus(status) {
    if (status === 'WAITING' || status === 'NOT_SENT'|| status === 'UNKNOWN') {
        return "#2cb4ac";
    } else if (status === 'WAITING_APPROVE' || status === 'PENDING') {
        return "#f2bf38";
    } else if (status === 'WAITING_RETRY') {
        return "#f2bf38";
    } else if (status === 'IN_PROGRESS' || status === 'SENDING' || status === 'ACTIVE') {
        return "#bda4ec";
    } else if (status === 'DONOR_APPROVED') {
        return "#bda4ec";
    } else if (status === 'FAILED' || status.indexOf("ERROR") >= 0 || status.indexOf("PF_") >= 0
        || status === 'UNPAID') {
        return "#f88568";
    } else if (status === 'CANCELED') {
        return "#b59541";
    } else if (status === 'DONE' || status === 'PAID' || status === 'SENT' || status === 'NEGATIVE') {
        return "#96b662";
    } else {
        return "#767676";
    }
}