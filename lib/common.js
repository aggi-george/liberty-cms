var formidable = require('formidable');
var md5 = require('MD5');
var fs = require('fs');
var config = require('../config');

var logsEnabled = config.LOGSENABLED;

var PARSE_ERROR = 15;	// common
exports.shortMonths = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
exports.longMonths = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

/**
 * 400
 */
exports.VERSION_KEY_INVALID = 1; //registration/register, registration/auth
exports.SIM_ID_NOT_FOUND = 2; //registration/register
exports.ATTEMPT_RETRY = 14; //registration/register
exports.EMPTY_SIM_ID_COUNTRY_CODE_PHONE = 18; //registration/register
exports.PIN_CODE_INVALID = 3; //registration/auth
exports.EMPTY_COUNTRY_CODE_PHONE = 19; //registration/auth
var UPLOAD_ABORTED = 6; //image/set
exports.UPLOAD_ABORTED = UPLOAD_ABORTED; //image/set
exports.FILE_TOO_LARGE = 7; //image/set
exports.PARSE_ERROR = PARSE_ERROR;
exports.MONKEY_ERROR = 31;
exports.SUBSCRIPTION_ERROR = 32;
exports.NOT_CIRCLES = 69;
exports.CIRCLES_STILL_PROCESSING = 67;
exports.BILL_FILE_NOT_FOUND_OR_INVALID = 70;
exports.PROMO_CODE_EXPIRED_OR_INVALID = 71;
exports.PROMO_CODE_USED = 72;
exports.FREE_BOOST_USED = 73;
exports.PAYMENT_ERROR = 74;
exports.PAYMENT_PROCESSING_ERROR = 75;
exports.CODE_MAX_LIMIT_REACHED_ERROR = 76;
exports.CODE_ALREADY_TAKEN_ERROR = 77;
exports.CODE_LENGTH_INVALID_ERROR = 78;
exports.CODE_INAPPROPRIATE_CONTENT_ERROR = 79;
exports.CODE_INVALID_CHARS_ERROR = 80;
exports.PAYMENT_ERROR_PF_NOT_HONORED = 81;
exports.PAYMENT_ERROR_PF_LOST = 82;
exports.PAYMENT_ERROR_PF_STOLEN = 83;
exports.PAYMENT_ERROR_PF_INSUFFICIENT_FUND = 84;
exports.PAYMENT_ERROR_PF_INVALID_CARD_INFORMATION = 85;
exports.PAYMENT_ERROR_PF_CARD_EXPIRED = 86;
exports.CUSTOMER_NOT_FOUND = 87;
exports.PORT_IN_CURRENT_NUMBER = 88;
exports.PORT_IN_ALREADY_ACTIVE = 89;
exports.OPERATION_LOCKED = 90;
exports.INVALID_PAYMENT_AMOUNT = 91;
exports.OPEN_INVOICE_NOT_FOUND = 92;
exports.REGISTER_PORT_IN_NUMBER_ACTIVE = 93;
exports.REGISTER_PORT_IN_NUMBER_NOT_ACTIVE = 94;
exports.REGISTER_TEMP_NUMBER_NOT_ACTIVE = 95;
exports.PROMO_ALREADY_HAS_THIRD_PARTY_BONUS = 96;
exports.PAYMENT_ERROR_UNFINISHED_TRANSACTION = 97;
exports.LOCATION_PROMO_FAILED_TO_REDEEM = 98;
exports.LOCATION_PROMO_DIALOG_DISABLED = 100;
exports.LOCATION_PROMO_DIALOG_FAILED = 101;
exports.ERROR_RESTRICTED_ADDON_SUBSCRIPTION = 102;
exports.ERROR_CIRCLES_SWITCH_UPGRADE_REQUIRED = 103;
exports.ERROR_CIRCLES_SWITCH_UPGRADE_NOT_ALLOWED = 104;
exports.ERROR_PROCESSING_ANOTHER_ACTION = 105;

/**
  * 401
 */
exports.SESSION_KEY_INVALID = 4; //all API that has signature

/**
  * 403
  */
exports.EMPTY_PASSWORD = 16; //registration/refresh
exports.USER_INVALID = 5; //registration/refresh

/**
  * 404
  */
exports.USER_NOT_FOUND = 8; //message/buzz/send

/**
  * 500
  */
exports.SCP_AVAILABLE_FOR_CIRCLES_NUMBERS_ONLY = 15;
exports.SERVER_DATABASE_ERROR  = 17; //500, internal error, db error for any API

/**
 * e-commerce errors
 */

exports.PAAS_ERROR = 1001;

exports.MIME_TYPE_WAKEUP = "sys_wakeup";
exports.MIME_TYPE_BUZZ = "msg_buzz";
exports.MIME_TYPE_CONTACT_REGISTRATION = "cn_registration";
exports.MIME_TYPE_CONTACT_DEACTIVATION = "cn_deactivation";
exports.MIME_TYPE_CONTACT_DETAILS_UPDATE = "cn_update";
exports.MIME_TYPE_TEXT = "text";
exports.MIME_TYPE_BONUS = "bonus";
exports.MIME_TYPE_BONUS_DATA = "bonus_data";
exports.MIME_TYPE_DATA_WARNING = "data_warning";
exports.EXTRA_MIME_TYPE = "mime_type";
exports.EXTRA_PHONE_NUMBER = "phone_number";

exports.BONUS_REFERRER = 0.5;
exports.BONUS_REFERREE = 0.5;
exports.BONUS_FB_SHARE = 0.25;
exports.BONUS_FB_LIKE = 0.25;
exports.LIMIT_DAILY = 90*60;	// seconds
exports.LIMIT_MONTHLY = 20/100;	// max percent loss

exports.TRUSTEDPROXY = [ '10.20.1.10', '10.20.1.19', '10.20.1.29', '52.74.220.218', '52.76.179.246' ];

exports.getAppName = function (str) {
	return (str == "selfcare") ? "Circles Care" : "Say Circles";
}

exports.notify = [];

exports.msOffsetSG = function(d) {
	var o = 8 * 60 * 60 * 1000;
	if (!d) return new Date().getTime() + o;
	else return d + o;
}

exports.getLocalTime = function(d) {
	return new Date((d ? (d.getTime ? d.getTime() : d) : new Date().getTime()) + 8 * 60 * 60 * 1000);
}

function getTime(t) {
	var h = t.getHours();
	var m = t.getMinutes();
	var s = t.getSeconds();
	return ('0'+h).slice(-2) + ":" + ('0'+m).slice(-2) + ":" + ('0'+s).slice(-2);
}

function getTimeString(t) {
	var h = t.getHours();
	var m = t.getMinutes();
	var s = t.getSeconds();
	return ('0'+h).slice(-2) + ":" + ('0'+m).slice(-2) + ":" + ('0'+s).slice(-2);
}

exports.getTimeString = getTimeString;

exports.getMonthStartFromTimestamp = function(timestamp){
	var currentDate = new Date(timestamp);
	var monthString = exports.shortMonths[currentDate.getMonth()];
	var yearComponents = currentDate.getFullYear().toString().split('');
	return '01-' + monthString + '-' + yearComponents[2]+yearComponents[3];
}

function getDate(t) {
	var y = t.getFullYear();
	var m = t.getMonth()+1;
	var d = t.getDate();
	return y + "-" + ('0'+m).slice(-2) + "-" + ('0'+d).slice(-2);
}

exports.getDate = getDate;

function getDateDayFirst(t) {
	var y = t.getFullYear();
	var m = t.getMonth()+1;
	var d = t.getDate();
	return ('0'+d).slice(-2) + "-" + ('0'+m).slice(-2) + "-" + y;
}

exports.getDateDayFirst = getDateDayFirst;

function getDateTime(d) {
	if ( !d ) return "";
	var t = new Date(d);
	return getDate(t) + " " + getTime(t);
}

exports.getDateTime = getDateTime;

exports.getContactName = function(fullnum, contacts) {
	if ( typeof(contacts) == "string" ) return "";
	for ( var j=0;j<contacts.length;j++ )
		for ( var k=0;k<contacts[j].nums.length;k++ )
			if ( contacts[j].nums[k] == "+" + fullnum )
				return contacts[j].fn + " " + contacts[j].ln;
	return "";
}

exports.isArray = isArray;

function isArray(q) {
	if ( (q != null) && (typeof(q) == 'object') && q.length ) return true;
	else return false;
}

exports.isObject = isObject;

function isObject(q) {
	if ( (q != null) && (typeof(q) == 'object')) return true;
	else return false;
}

exports.safeParse = function(str) {
	try {
		return JSON.parse(str);
	} catch (ex) {
		return str;
	}
}

exports.safeDate = function(str) {
	var timestamp = Date.parse(str)

	if ( isNaN(timestamp) == false) {
		return new Date(timestamp);
	} else {
		return undefined;
	}
}

exports.deepCopy = function(obj) {
	if (!obj) return obj;
	return JSON.parse(JSON.stringify(obj))
}

exports.sortMd5 = function(fields) {
	var kv = [];
	for (var k in fields) kv.push([k, fields[k]]);
	kv.sort()
	str = "";
	for ( var i=0; i<kv.length; i++ ) {
		str += kv[i][0] + "=" + kv[i][1] + ";";
	}
	return md5(str.slice(0,-1));
}

exports.isNum = function(arg) {
	if ( parseInt(arg) == arg ) return true;
	else return false;
}

function log(f,s) {
	console.log(new Date().toISOString().slice(0,19).replace(/T/g,' '), '-', f, '-', s);
}

function error(f,s) {
	console.error(new Date().toISOString().slice(0,19).replace(/T/g,' '), '-', f, '-', s);
}

exports.log = log;
exports.error = error;

exports.formParse = function(req, res, fieldCount, callback) {
	var form = new formidable.IncomingForm()
	form.uploadDir = '/tmp/';
	form.keepExtensions = true;
	form.multiples = false;
	form.maxFieldsSize = 2 * 1024 * 1024;	// 2mb
	form.maxFields = fieldCount;
	form.parse(req, function(err, fields) {
		if ( !err ) callback(fields);
		else res.status(400).json({"code":PARSE_ERROR,"details":"Parse Error","description":"Parse Error"});
	});
}

exports.formParseFiles = function(req, res, fieldCount, callback) {
	var form = new formidable.IncomingForm()
	form.uploadDir = '/tmp/';
	form.keepExtensions = true;
	form.multiples = false;
	form.maxFieldsSize = 2 * 1024 * 1024;	// 2mb
	form.maxFields = fieldCount;
	var percent_complete = 0;
	var aborted = 0;
	form.on('aborted', function () {
		aborted = 1; 
		if (!res.headersSent) res.status(400).json({"code":UPLOAD_ABORTED,"details":"upload error","description":"Upload Aborted"});
	});
	form.on('error', function () {
		if ( parseInt(res.statusCode) == 200 )
			res.status(400).json({"code":FILE_TOO_LARGE,"details":"file size error","description":"File too large"});
	});
	form.on('progress', function(bytesReceived, bytesExpected) {
		percent_complete = (bytesReceived / bytesExpected) * 100;
	});
	form.parse(req, function(err, fields, files) {
		if ( !err && (files || (files.content && (percent_complete == 100))) ) callback(fields, files);
		else if ( parseInt(res.statusCode) == 200 )
			res.status(400).json({"code":PARSE_ERROR,"details":"Parse Error","description":"Parse Error"});
	});
}

exports.logUrl = function(req) {
	log(req.ip, "PATH = " + req.path);
}

var entityMap = {
	"&": "&amp;",
	"<": "&lt;",
	">": "&gt;",
	'"': '&quot;',
	"'": '&#39;',
	"/": '&#x2F;',
	"\\": '&#92;'
};

exports.escapeHtml = escapeHtml;

function escapeHtml(string) {
	return String(string).replace(/[&<>"'\/\\]/g, function (s) {
		return entityMap[s];
	});
}

exports.escapeRegExp = escapeRegExp;

function escapeRegExp(string) {
	return String(string).replace(/[.?*+^$[\]\\(){}|-]/g, "\\$&");
}

exports.mv = mv;

function mv(src, dst) {
	var source = fs.createReadStream(src);
	var dest = fs.createWriteStream(dst);

	source.pipe(dest);
	source.on('end', function() {
		fs.unlink(src);
	});
	source.on('error', function(err) { /* error */ });
}

exports.prettifyKBUnit = function (kb) {
	if (kb >= 1024 * 1024) {
		return {value: Math.round((kb / 1024 / 1024) * 10) / 10, unit: "GB"};
	} else if (kb >= 1024) {
		return {value: Math.round(kb / 1024), unit: "MB"};
	} else {
		return {value: 0, unit: "MB"};
	}
}

function calculateDistance (lat1,lon1,lat2,lon2) {
	var R = 6371; // km (change this constant to get miles)
	var dLat = (lat2-lat1) * Math.PI / 180;
	var dLon = (lon2-lon1) * Math.PI / 180;
	var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
		Math.cos(lat1 * Math.PI / 180 ) * Math.cos(lat2 * Math.PI / 180 ) *
		Math.sin(dLon/2) * Math.sin(dLon/2);
	var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
	var d = R * c;
	if (d>1) return Math.round(d);

	d = isInt(d) ? d : Number(d.toFixed(3));
	return d;
}

function convertToGb(kb) {
	let gb = kb / (1000 * 1000);

	return Number(gb.toFixed(2));
}

function getMonthDiff(start, end) {
	return (end.getMonth() - start.getMonth() + (12 * (end.getFullYear() - start.getFullYear())));
}

function isInt(num) {
	return num % 1 === 0;
}

function uniqify(list, fields) {
	/*
	* This function takes in an array of objects
	* and will create a unique list based on some of the 
	* properties of the objects in the array.
	*/
    if (!Array.isArray(list)) {
        return [];
    } else {
        try {
            if (fields.length > 0) {
                // create a temporary duplicate list
                // of objects with only said properties.
                let duplicateList = [];
                list.forEach(function(ele) {
                    let obj = {};
                    fields.forEach(function(field) {
                        obj[field] = ele[field];
                    });
                    duplicateList.push(obj);
                });
                // uniqify the temp duplicate list.
                let tempUniqueList = Array.from(new Set(duplicateList.map(JSON.stringify))).map(JSON.parse);
                // return the original object with all the properties
                let uniqueList = [];
                tempUniqueList.forEach(function(elem) {
                    let uniqueAddon = list.find(function(dupElement) {
                        return Object.keys(elem).every(function (key) {
                            return dupElement[key] === elem[key];
                        });
                    });
                    if (uniqueAddon) {
                        uniqueList.push(uniqueAddon);
                    }
                });
                return uniqueList;
            } else {
                return Array.from(new Set(list.map(JSON.stringify))).map(JSON.parse);
            }
        } catch (e) {
            error('Failed to uniqify the list with error', e);
            return [];
        }
    }
}

exports.convertToGb = convertToGb;
exports.getMonthDiff = getMonthDiff;
exports.isInt = isInt;
exports.calculateDistance = calculateDistance;
exports.uniqify = uniqify;
