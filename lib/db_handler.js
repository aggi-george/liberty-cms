var mysql = require('mysql');
var md5 = require('MD5');
var mongodb = require('mongodb');
var Redis = require('ioredis');
var kue = require('kue');
var oracledb = require('oracledb');
var common = require('./common');
var BaseError = require('./../src/core/errors/baseError');
var config = require('../config');
var EventEmitter = require('events').EventEmitter;

var logsEnabled = config.LOGSENABLED;
var apiCount = 0;

var mysqlpool = mysql.createPoolCluster({ "restoreNodeTimeout" : 3 * 60 * 1000 }); // will be kicked out for 3mins after 5 errors (default) 
if (config.MYSQLDB1) mysqlpool.add(config.MYSQLDB1);
if (config.MYSQLDB2) mysqlpool.add(config.MYSQLDB2);
if (config.MYSQLDB3) mysqlpool.add(config.MYSQLDB3);
var redisClient;
var oracleClient;
var oracleListeners = [];
var oracleReinitInProgress;
var statusEvent = new EventEmitter();

if (config.REDIS_SENTINEL) {
    var REDIS_Q_PREFIX = config.REDIS_QUEUE_PREFIX
        ? config.REDIS_QUEUE_PREFIX : "q";

    var conn = {
        "sentinels": config.REDIS_SENTINEL,
        "name": "mymaster",
        "password": config.REDIS_PASSWORD,
        "dropBufferSupport": true
    };

    redisClient = new Redis(conn);
    redisClient.on("ready", function (err, result) {
        statusEvent.emit("ready");
    });
    redisClient.on("error", function (err) {
        common.error("redisClient error", err.message);
    });
    var queue = kue.createQueue({
        "redis": {
            "createClientFactory": function () {
                return new Redis(conn);
            }
        },
        "prefix": REDIS_Q_PREFIX
    });
    queue.setMaxListeners(150);
    queue.watchStuckJobs(60000);
    queue.on('error', function (err) {
        common.error("Queue", err);
    });

    exports.queue = queue;
    exports.queueJobs = kue.Job;
}

//openOracleConnection();

function openOracleConnection(callback) {
    if (oracleReinitInProgress) {
        console.log("DatabaseHandler", "oracle: reconnection is in progress, add to listeners");
        if (callback) oracleListeners.push(callback);
        return;
    }

    oracleReinitInProgress = true;
    var handleResult = (err, connection) => {
        if (err) {
            console.error("DatabaseHandler", "oracle: connection error, error=" + err.message);
            oracleClient = undefined;
        } else {
            console.log("DatabaseHandler", "oracle: successfully connected");
            oracleClient = connection;
        }

        var listeners = oracleListeners;

        oracleListeners = [];
        oracleReinitInProgress = false;

        if (callback) callback(err, connection);
        listeners.forEach((savedCallback) => {
            if (savedCallback) savedCallback(err, connection);
        })
    }

    if (oracleClient) {
        oracleClient.release();
    }

    if (!config.ORACLE) {
        return handleResult(new Error("ORACLE config is empty"));
    }

    oracledb.maxRows = 20000;
    oracledb.poolTimeout = 0;

    console.log("DatabaseHandler", "oracle: creating a pool...");
    oracledb.createPool(config.ORACLE, function (err, pool) {
        if (err) {
            return handleResult(err);
        }

        console.log("DatabaseHandler", "oracle: retrieving a connection...");
        pool.getConnection(function (err, connection) {
            if (err) {
                return handleResult(err);
            }

            return handleResult(err, connection);
        });
    });
}

function oracleConnection(callback) {
    oracledb.getConnection(config.ORACLE, function(err, connection) {
        if (err || !connection) {
            oracledb.getConnection(config.ORACLE, function(err, connection) {    // try again
                callback(err, connection);
            });
        } else {
            callback(err, connection);
        }
    });
}

function oracleQuery1(query, callback) {
    oracleConnection(function(err, connection) {
        if (err || !connection) {
            common.error("oracleQuery", "Cannot get connection");
            return callback(err);
        }
        connection.execute(query, [], {resultSet: false, maxRows:100000}, function (err, result) {
            callback(err, result);
            connection.close(function(err) {
                if (err) console.error(err.message);
            });
        });
    });
}

function oracleQuery(query, options, callback) {
    if ( (typeof(options) != "object") && !callback) {
        callback = options;
        options =  { resultSet: false, maxRows:100000 };
    }
    if (options && options.outFormat && (options.outFormat == "object")) options.outFormat = oracledb.OBJECT;
    var result = new Array;
    var fetchRowsFromRS = function(connection, resultSet, numRows, callback) {
        resultSet.getRows(numRows, function (err, rows) {
            if (err) {
                common.error("oracleQuery", "Cannot get connection");
                return callback(err);
            } else if (rows.length > 0) {
                result = result.concat(rows);
                if (rows.length === numRows) {
                    fetchRowsFromRS(connection, resultSet, numRows, callback);
                } else {
                    callback(undefined, result);
                }
            } else {
                callback(undefined, result);
            }
        });
    }
    oracleConnection(function(err, connection) {
        if (err || !connection) {
            common.error("oracleQuery", "Cannot get connection");
            return callback(err);
        }
        connection.execute(query, [], options, function (err, result) {
            if (options && options.resultSet && result) {
                fetchRowsFromRS(connection, result.resultSet, 1000, function(fetchErr, fetchResult) {
                    result.rows = fetchResult;
                    callback(fetchErr, result);
                    connection.close(function(err) {
                        if (err) console.error(err.message);
                    });
                });
            } else {
                callback(err, result);
                connection.close(function(err) {
                    if (err) console.error(err.message);
                });
            }
        });
    });
}

function oracleQueryOld(query, callback) {
    if (!callback) callback = () => {
    }

    if (!oracleClient) {
        return callback(new Error("Oracle Client is not initialised"));
    }

    oracleClient.execute(query, [], {resultSet: false}, function (err, result) {
        if (err) {
            if (err.message.indexOf("ORA") >= 0) {
                console.error("DatabaseHandler", "oracle: failed to make a query, error="
                    + err.message + ", reconnecting...");
                openOracleConnection((err, oracleClient) => {

                    if (err) {
                        console.error("DatabaseHandler", "oracle: reconnecting failed, error=" + err.message);
                        return callback(err);
                    }

                    if (!oracleClient) {
                        console.error("DatabaseHandler", "oracle: reconnecting failed, no open connection");
                        return callback(new Error("Oracle Client is not initialised after retry"));
                    }

                    oracleClient.execute(query, [], {resultSet: false}, function (err, result) {
                        if (err) {
                            console.error("DatabaseHandler", "oracle: failed to make a query after " +
                                "reconnection, error=" + err.message);
                            return callback(new Error(err.message + " (FAILED AFTER RETRY)"));
                        }

                        console.log("DatabaseHandler", "oracle: query was made after reconnection");
                        callback(undefined, result.rows);
                    });
                });

                return;
            } else {
                console.error("DatabaseHandler", "oracle: failed to make a query, error=" + err.message);
                return callback(err);
            }
        }

        callback(undefined, result.rows);
    });
}

exports.oracle = {
    query: oracleQuery
}

function fetchRowsFromRS(connection, resultSet, numRows)
{
    resultSet.getRows( // get numRows rows
        numRows,
        function (err, rows)
        {
            if (err) {
                console.log("Please ignore for now - Kenan")  // close the result set and release the connection
            } else if (rows.length > 0) { // got some rows
                console.log(rows); // process rows
                if (rows.length === numRows) // might be more rows
                    fetchRowsFromRS(connection, resultSet, numRows);
                else // got fewer rows than requested so must be at end
                    console.log("Please ignore for now - Kenan"); // close the result set and release the connection
            } else { // else no rows
                console.log("Please ignore for now - Kenan"); // close the result set and release the connection
            }
        });
}


//exports

exports.statusEvent = statusEvent;
exports.apiCount = apiCount;
exports.sessionCheckGet = sessionCheckGet;
exports.sessionCheckPost = sessionCheckPost;
exports.sessionCheckFiles = sessionCheckFiles;
exports.sessionCheckJson = sessionCheckJson;
exports.escape = mysqlEscape;
exports.query = mysqlQuery;			// handle error here
exports.query_noerr = mysqlQueryNoErr;
exports.query_err = mysqlQueryErr;		// handle error in callback
exports.transaction = mysqlTransaction;
exports.rollback = mysqlRollback;
exports.commit = mysqlCommit;
exports.objectID = function (id) { return new mongodb.ObjectID(id) };
exports.readPreference = mongodb.ReadPreference;

exports.sql = {
    query: exports.query_err,
    transaction: exports.transaction,
    rollback: exports.rollback,
    commit: exports.commit
}

//functions

function sessionCheckGet(req, res, callback) {
    sessionCheckInternal(req, res, common.sortMd5(req.query), callback);
}

function sessionCheckPost(req, res, fields, callback) {
    sessionCheckInternal(req, res, common.sortMd5(fields), callback);
}

function sessionCheckFiles(req, res, files, callback) {
    sessionCheckInternal(req, res, md5("" + files.content.size), callback);
}

function sessionCheckJson(req, res, fields, callback) {
    sessionCheckInternal(req, res, md5(JSON.stringify(fields)), callback);
}

function sessionCheckInternal(req, res, paramHash, callback) {
    if ( (req.connection.remoteAddress == config.MYSELF) && (req.params.signature == config.MYSIG) ) {
        callback(req, res);
    } else {
        apiCount++;
        sessionCacheGet(req.params.user_key, function (value) {
            if (value) {
                var hash = md5(paramHash + ";" + req.params.user_key + ";" + value);
                if (hash == req.params.signature) {
                    callback(req, res);
                } else {
                    res.status(401).json({
                        "code": common.SESSION_KEY_INVALID,
                        "details": "session_key error",
                        "description": "Session Key is Invalid"
                    });
                }
            } else {
                res.status(401).json({
                    "code": common.SESSION_KEY_INVALID,
                    "details": "session_key error",
                    "description": "Session Key is Invalid"
                });
            }
        });
    }
}

function mysqlEscape(string) {
    return mysql.escape(string);
};

function mysqlQuery(res, q, callback) {
    mysqlpool.getConnection(function (err, connection) {
        if (!err) {
            connection.query(q, function (err, rows) {
                connection.release();
                if (!err) callback(rows);
                else res.status(500).json({
                    "code": common.SERVER_DATABASE_ERROR,
                    "details": "db error",
                    "description": "DB Error"
                });
            });
        } else res.status(500).json({"code": common.SERVER_DATABASE_ERROR, "details": "db error", "description": "DB Error"})
    });
}

function mysqlQueryNoErr(q, callback) {
    mysqlpool.getConnection(function (err, connection) {
        if (!err) {
            connection.query(q, function (err, rows) {
                connection.release();
                if (!err) callback(rows);
                else callback(undefined);
            });
        } else {
            callback(undefined);
        }
    });
}
function mysqlQueryErr(q, callback) {
    mysqlpool.getConnection(function (err, connection) {
        if (!err) {
            connection.query(q, function (err, rows) {
                if(err){
                    if(err.code && err.code == 'ER_LOCK_DEADLOCK' ){
                        connection.query(q, function (err, rows) {
                            connection.release();
                            callback(err, rows);
                        });
                    }else{
                        connection.release();
                        callback(err);
                    }
                }else{
                    connection.release();
                    callback(err, rows);
                }
            });
        } else {
            callback(err);
        }
    });
}

function mysqlCommit(connection, result, callback) {
    connection.commit(function(err) {
        if (err) {
            mysqlRollback(connection, callback);
        } else {
            connection.release();
            callback(err, result);
        }
    });
}

function mysqlTransaction(callback) {
    mysqlpool.getConnection(function (err, connection) {
        if (err) {
            callback(err);
        } else {
            connection.beginTransaction(function (err) {
                callback(err, connection);
            });
        }
    });
}

function mysqlRollback(connection, callback) {
    connection.rollback(function() {
        connection.release();
        callback();
    });
}

//old exports


var profile_pic;
var profile_doc;
var temp_files;
var resource_files;
var cdrs;
var eccdrs;
var m1cdrs;
var ocslogs;
var scpcdrs;
var notifications_webhook;
var notifications_sms;
var notifications_push;
var notifications_logbook;
var notifications_sync;
var notifications_bills;
var notifications_emails;
var notifications_hlr;
var notifications_resubscription;
var notifications_account;
var notifications_status_change;
var notifications_audit_logs;
var notifications_pay_now;
var notifications_credit_cap;
var notifications_invoices_sync;
var notifications_promo_codes;
var notifications_port_in;
var notifications_bi;
var notifications_bikey;
var paas;

var mongoOptions = { "poolSize" : 200 };
if (config.MONGODB) mongodb.MongoClient.connect(config.MONGODB, mongoOptions, function (err, db) {
    if (err) return console.error(err);

    var dbNamePostfix = config.DEV ? "_dev" : "";

    var circlesDB = db.db("circles" + dbNamePostfix);
    profile_pic = circlesDB.collection("profile_pic");
    profile_doc = circlesDB.collection("profile_doc");
    temp_files = circlesDB.collection("temp_files");
    resource_files = circlesDB.collection("resource_files");
    exports.weblog = circlesDB.collection("weblog");
    exports.hlrlog = circlesDB.collection("hlrlog");
    exports.addonlog = circlesDB.collection("addonlog");
    exports.promocodes = circlesDB.collection("promocodes");
    exports.scheduler = circlesDB.collection("scheduler");
    exports.iccidPool = circlesDB.collection("iccidPool");
    exports.numberPool = circlesDB.collection("numberPool");
    exports.templinks = circlesDB.collection("templinks");
    exports.apistats = circlesDB.collection("apistats");
    exports.ecstats = circlesDB.collection("ecstats");
    exports.classifiertats = circlesDB.collection("classifiertats");
    exports.ecerrors = circlesDB.collection("ecerrors");
    exports.circlesTalkStats = circlesDB.collection("circlestalkstats");
    exports.paneltyDefinitions = circlesDB.collection("paneltyDefinitions");
    exports.penaltyHistory = circlesDB.collection("penaltyHistory");
    exports.testCollection = circlesDB.collection("testCollection");
    exports.zendeskTickets = circlesDB.collection("zendeskTickets");
    exports.zendeskTicketsComments = circlesDB.collection("zendeskTicketsComments");
    exports.customerLocations = circlesDB.collection("customerLocations");
    exports.appStates = circlesDB.collection("appStates");

    var notifDB = db.db("notifications" + dbNamePostfix);
    notifications_sms = notifDB.collection("logbooksms");
    notifications_push = notifDB.collection("logbookpush");
    notifications_logbook = notifDB.collection("logbook");
    notifications_sync = notifDB.collection("logbooksync");
    notifications_webhook = notifDB.collection("logbookwebhook");
    notifications_bills = notifDB.collection("logbookbills");
    notifications_emails = notifDB.collection("logbookemails");
    notifications_hlr = notifDB.collection("logbookhlr");
    notifications_resubscription = notifDB.collection("logbookresubscription");
    notifications_status_change = notifDB.collection("logbookstatuschange");
    notifications_pay_now = notifDB.collection("logbookpaynow");
    notifications_credit_cap = notifDB.collection("logbookcreditcap");
    notifications_invoices_sync = notifDB.collection("logbookinvoicessync");
    notifications_promo_codes = notifDB.collection("logbookpromocodes");
    notifications_port_in = notifDB.collection("logbookportin");
    notifications_paas = notifDB.collection("logbookpaas");
    notifications_account = notifDB.collection("logbookaccount");
    notifications_bi = notifDB.collection("logbookbi");
    notifications_bikey = notifDB.collection("logbookbikey");
    notifications_audit_logs = notifDB.collection("logbookauditlogs");

    var biDb = db.db("reports" + dbNamePostfix);
    var kpiDefinitions = biDb.collection("kpidefinitions");
    var kpiGenerators = biDb.collection("kpigenerators");
    var kpiViews = biDb.collection("kpiViews");
    var kpiValues = biDb.collection("kpivalues");
    var biReports = biDb.collection("biReports");
    var biReportValues = biDb.collection("biReportValues");

    exports.kpiDefinitions = kpiDefinitions;
    exports.kpiValues = kpiValues;
    exports.kpiGenerators = kpiGenerators;
    exports.kpiViews = kpiViews;

    exports.notifications_webhook = notifications_webhook;
    exports.notifications_logbook = notifications_logbook;
    exports.notifications_sync = notifications_sync;
    exports.notifications_emails = notifications_emails;
    exports.notifications_hlr = notifications_hlr;
    exports.notifications_sms = notifications_sms;
    exports.notifications_push = notifications_push;
    exports.notifications_bills = notifications_bills;
    exports.notifications_webhook = notifications_webhook;
    exports.notifications_resubscription = notifications_resubscription;
    exports.notifications_status_change = notifications_status_change;
    exports.notifications_pay_now = notifications_pay_now;
    exports.notifications_credit_cap = notifications_credit_cap;
    exports.notifications_invoices_sync = notifications_invoices_sync;
    exports.notifications_promo_codes = notifications_promo_codes;
    exports.notifications_port_in = notifications_port_in;
    exports.notifications_paas = notifications_paas;
    exports.notifications_account = notifications_account;
    exports.notifications_audit_logs = notifications_audit_logs;
    exports.notifications_bi = notifications_bi;
    exports.notifications_bikey = notifications_bikey;
    exports.notifications_account = notifications_account;
    exports.agg = notifDB.collection("agg");

    var paasDB = db.db("paas" + dbNamePostfix);
    paas = paasDB.collection("paas");
    exports.paas = paas;

    var cdrsDB = db.db("cdrs" + dbNamePostfix);
    cdrs = cdrsDB.collection("cdrs");
    m1cdrs = cdrsDB.collection("m1cdrs");
    ocslogs = cdrsDB.collection("ocslogs");
    scpcdrs = cdrsDB.collection("scpcdrs");
    eccdrs = cdrsDB.collection("eccdrs");
    exports.cdrs = cdrs;
    exports.m1cdrs = m1cdrs;
    exports.ocslogs = ocslogs;
    exports.scpcdrs = scpcdrs;
    exports.eccdrs = eccdrs;

    var siptraceDB = db.db("siptrace" + dbNamePostfix);
    exports.siptrace = siptraceDB.collection("sip_trace");

    var contactsDB = db.db("contacts" + dbNamePostfix);
    exports.contacts = contactsDB.collection("contacts");

    const diameterDB = db.db("diameter" + dbNamePostfix);
    exports.DATAReq = diameterDB.collection('DATAReq');
    exports.DATARes = diameterDB.collection('DATARes');
    exports.VOICEReq = diameterDB.collection('VOICEReq');
    exports.VOICERes = diameterDB.collection('VOICERes');
    exports.SMSReq = diameterDB.collection('SMSReq');
    exports.SMSRes = diameterDB.collection('SMSRes');
});

exports.getCDR = getCDR;
exports.updateMOS = updateMOS;
exports.getUsage = getUsage;

function getUsage(number, start, callback) {
    var match = new Object;
    var project = new Object;
    var group = new Object;
    match.orig_ani = number;
    match.start_time = { "$gte" : start };
    project.orig_ani = "$orig_ani";
    project.ingress_cost = "$ingress_cost";
    group._id = "$orig_ani";
    group.total = { "$sum" : "$ingress_cost" };
    var agg_array = [{ "$match" : match }, { "$project" : project }, { "$group" : group }];
    cdrs.aggregate(agg_array, function(err, result) {
        if (result && result[0]) callback(result[0].total);
        else callback(0);
    });
}

function updateMOS(sipcallid, mos) {
    var find = {"call_id":sipcallid,"ingress_billtime":{"$gt":0}};
    var order = [["ingress_bill_time","desc"]];
    var modify = {"$set":{"mos":mos.toFixed(2)}};
    var options = {};
    cdrs.findAndModify(find, order, modify, options, function (err, object) {
        if (err) {
            if (logsEnabled) conmmon.err("updateMOS", err.message);
        }
    });
}

function getCDR(condition, ampm, callback) {
    var reply = new Array;
    var projection = new Object;
    projection.start_time = 1;
    projection.orig_ani = 1;
    projection.term_dst_number = 1;
    projection.term_code_name = 1;
    projection.egress_carrier_name = 1;
    projection.ingress_billtime = 1;
    projection.ingress_cost = 1;
    projection.egress_cost = 1;
    projection.media = 1;
    projection.release_side = 1;
    projection.release_cause = 1;
    projection.call_id = 1;
    projection.mos = 1;
    var cursor = cdrs.find(condition, projection).sort({start_time:-1}).toArray(function(err, doc) {
        for ( var i=0; i<doc.length; i++ ) {
            if ( ((ampm == 0) && (new Date(doc[i].start_time*1000).getHours() < 12)) ||
                ((ampm == 1) && (new Date(doc[i].start_time*1000).getHours() >= 12)) ) {
                var row = new Object;
                row.startdate = new Date(Math.round(doc[i].start_time*1000));
                row.srcnumber = doc[i].orig_ani;
                row.destnumber = doc[i].term_dst_number;
                row.destname = doc[i].term_code_name;
                row.provider = (doc[i].egress_carrier_name) ? doc[i].egress_carrier_name : "";
                row.duration = (doc[i].ingress_billtime) ? doc[i].ingress_billtime : 0;
                row.charge = (doc[i].ingress_cost) ? doc[i].ingress_cost.toFixed(6) : "0.000000";
                row.cost = (doc[i].egress_cost) ? doc[i].egress_cost.toFixed(6) : "0.000000";
                row.media = doc[i].media;
                row.term = doc[i].release_side;
                row.hangup_cause = doc[i].release_cause;
                row.acc_callid = doc[i].call_id;
                row.mos = (doc[i].mos) ? doc[i].mos : "0.00";
                reply.push(row);
            }
        }
        callback(reply);
    });
}

function cache_partition(cache) {
    var dev = (config.DEV) ? 10 : 0;		// max 32, ecomm using 21, 22
    switch(cache) {
        case "cache":
            return dev + 1;
        case "session_cache":
            return dev + 2;
        case "uuid_cache":
            return dev + 3;
        case "ip_cache":
            return dev + 4;
        case "account_cache":
            return dev + 5;
        case "api_stats":
            return dev + 6;
        case "switch":
            return dev + 7;
        case "account_cache_micro":
            return dev + 8;
        case "diameter":
            return dev + 9;
        default:
            return 0;
    }
}



exports.cache_put = cache_put;
exports.cache_hput = cache_hput;
exports.cache_del = cache_del;
exports.cache_hdel = cache_hdel;
exports.paas_put = paas_put;

function cache_put(cache, key, value, ttl) {
    if (!key) {
        common.error("cache_put", "empty cache");
    } else if (redisClient) {
        var final = ( typeof(value) == "object" ) ? (value ? JSON.stringify(value) : value) : value;
        redisClient.multi().select(cache_partition(cache)).set(key, final).expire(key, ttl / 1000).exec();
    } else {
        common.error("cache_put", "redis not connected");
    }
}

function cache_hput(cache, key, value, ttl) {
    if (!key) {
        common.error("cache_hput", "empty cache");
    } else if (redisClient) {
        redisClient.multi().select(cache_partition(cache)).hmset(key, value).expire(key, ttl/1000).exec();
    } else {
        common.error("cache_hput", "redis not connected");
    }
}

function cache_del(cache, key) {
    if (!key) {
        common.error("cache_del", "empty cache");
    } else if (redisClient) {
        redisClient.multi().select(cache_partition(cache)).del(key).exec();
    } else {
        common.error("cache_del", "redis not connected");
    }
}

function cache_hdel(cache, key, hashKey) {
    if (!key) {
        common.error("cache_hdel", "empty cache");
    } else if (redisClient) {
        redisClient.multi().select(cache_partition(cache)).hdel(key, hashKey).exec();
    } else {
        common.error("cache_hdel", "redis not connected");
    }
}

function paas_put(obj, prefix, number) {
    if (obj) {
        obj.ts = new Date().getTime();
        if (number) {
            obj.number = number;
        }
        if (prefix) {
            obj.prefix = prefix;
        }
        paas.insert(obj, function () { });
    }
}

exports.sessionCacheGet = sessionCacheGet;

function sessionCacheGet(key, callback) {
    cache_get("session_cache", key, callback);
}

exports.cache_ttl = cache_ttl;
exports.cache_incr = cache_incr;
exports.cache_expire = cache_expire;
exports.cache_get = cache_get;
exports.cache_hget = cache_hget;
exports.paas_get = paas_get;
exports.monkey_test = monkey_test;

exports.cache = {
    get: exports.cache_get,
    hget: exports.cache_hget,
    expire: exports.cache_expire,
    put: exports.cache_put,
    del: exports.cache_del
};

function cache_ttl(cache, key, callback) {
    if (redisClient) {
        redisClient.multi().select(cache_partition(cache)).ttl(key).exec(function(err, value) {
            if ( value && value[1] && (typeof(value[1]) == "object") ) callback(common.safeParse(value[1][1]));
            else if (value && value[1]) callback(common.safeParse(value[1]));
            else callback(undefined);
        });
    }
}

function cache_incr(cache, key, ttl, callback) {
    if (redisClient) {
        var execute = function(cb) {
            if (ttl) redisClient.multi().select(cache_partition(cache)).incr(key).expire(key, ttl/1000).exec(cb);
            else redisClient.multi().select(cache_partition(cache)).incr(key).exec(cb);
        }
        execute(function(err, value) {
            if ( value && value[1] && (typeof(value[1]) == "object") ) callback(common.safeParse(value[1][1]));
            else if (value && value[1]) callback(common.safeParse(value[1]));
            else callback(undefined);
        });
    }
}

function cache_expire(cache, key, ttl) {
    if (redisClient) {
        redisClient.multi().select(cache_partition(cache)).expire(key, ttl/1000).exec();
    }
}

function cache_get(cache, key, callback) {
    if (redisClient) {
        if ( typeof(key) == "string" ) {
            redisClient.multi().select(cache_partition(cache)).get(key).exec(function(err, value) {
                if ( value && value[1] && (typeof(value[1]) == "object") ) callback(common.safeParse(value[1][1]));
                else if (value && value[1]) callback(common.safeParse(value[1]));
                else callback(undefined);
            });
        } else {
            redisClient.multi().select(cache_partition(cache)).mget(key).exec(function(err, value) {
                if ( value && value[1] && (typeof(value[1]) == "object") ) callback(common.safeParse(value[1][1]));
                else if (value && value[1]) callback(common.safeParse(value[1]));
                else callback(undefined);
            });
        }
    } else {
        common.error("cache_put", "redis not connected");
        callback(undefined);
    }
}

function cache_hget(cache, key, callback) {
    if (redisClient) {
        redisClient.multi().select(cache_partition(cache)).hgetall(key).exec(function(err, value) {
            if ( value && value[1] && (typeof(value[1]) == "object") ) callback(common.safeParse(value[1][1]));
            else if (value && value[1]) callback(common.safeParse(value[1]));
            else callback(undefined);
        });
    } else {
        common.error("cache_put", "redis not connected");
        callback(undefined);
    }
}

function paas_get(obj, proj, sort, callback) {
    paas.find(obj, proj).sort(sort).limit(50).toArray(function (err, item) {
        callback(item);
    });
}


function monkey_test(cache, key, callback) {
    if (redisClient) {
        cache_get(cache, key, function(value) {
            if (value) {
                if ( parseInt(value) >= config.MONKEYLIMIT ) {
                    callback(parseInt(value));
                } else {
                    redisClient.multi().select(cache_partition(cache)).set(key, parseInt(value) + 1).expire(key, 60).exec();
                    callback(undefined);
                }
            } else {
                redisClient.multi().select(cache_partition(cache)).set(key, 1).expire(key, 60).exec();
                callback(undefined);
            }
        });
    } else {
        common.error("cache_put", "redis not connected");
        callback(undefined);
    }
}

exports.cache_keys = function (cache, wildcard, callback) {
    if (redisClient) {
        redisClient.multi().select(cache_partition(cache)).keys(wildcard).exec(function(err, value) {
            if ( value && value[1] && (typeof(value[1]) == "object") ) callback(value[1][1]);
            else if ( value && value[1] ) callback(value[1]);
            else callback(undefined);
        });
    } else {
        common.error("cache_put", "redis not connected");
        callback(undefined);
    }
}

exports.lockOperation = function (key, value, ttl, callback) {
    exports.cache_lock("cache", key, value, ttl, function (lock) {
        if (lock) {
            callback();
        } else {
            callback(new BaseError("Operation on " + key + " is currently not allowed", "ERROR_OPERATION_LOCKED"));
        }
    });
}

exports.unlockOperation = function (key) {
    exports.cache_del("cache", key);
}

exports.cache_lock = function (cache, key, value, ttl, callback) {
    if (redisClient) {
        var tmpkey = "temp_" + key;
        redisClient.multi()
            .select(cache_partition(cache))
            .setex(tmpkey, ttl/1000, (typeof(value) == "object" ? JSON.stringify(value) : value))
            .renamenx(tmpkey, key)
            .exec(function(err, value) {
                if (!err && value) {
                    if ( typeof(value[2]) == "object" ) {
                        if (!value[2][1]) cache_del(cache, tmpkey);
                        callback(value[2][1]);
                    } else {
                        if (!value[2]) cache_del(cache, tmpkey);
                        callback(value[2]);
                    }
                } else callback(undefined);
            });
    } else {
        common.error("cache_lock", "redis not connected");
        callback(undefined);
    }
}

exports.sessionCheck = function (req, res, paramHash, callback) {
    cache_get("session_cache", req.params.user_key, function (value) {
        if (value) {
            var hash = md5(paramHash + ";" + req.params.user_key + ";" + value);
            if (hash == req.params.signature) callback();
            else res.status(401).json({
                "code": common.SESSION_KEY_INVALID,
                "details": "session_key error",
                "description": "Session Key is Invalid"
            });
        } else res.status(401).json({
            "code": common.SESSION_KEY_INVALID,
            "details": "session_key error",
            "description": "Session Key is Invalid"
        });
    });
}

exports.uuidCheck = function (res, key, callback) {
    cache_get("uuid_cache", key, function (value) {
        if ( value && (value.code > 1000) ) {
            res.status(500).json(value);
            return true;
        } else if (value) {
            res.json(value);
            return true;
        }
        callback();
    });
}

exports.uuidCheck2 = function (res, user_key, uuid, callback) {
    cache_hget("uuid_cache", user_key + "_" + uuid, function(cache) {
        if (cache && cache.reply) {				// already sent reply
            res.status(parseInt(cache.status)).json(JSON.parse(cache.reply));
        } else if (cache && cache.status == "0") {		// previous request received
            var count = 0;
            var processing = setInterval(function () {
                cache_hget("uuid_cache", user_key + "_" + uuid, function(cache) {
                    if (cache && cache.reply) {
                        res.status(parseInt(cache.status)).json(JSON.parse(cache.reply));
                        clearInterval(processing);
                    }
                    count++;
                    if (count>5) clearInterval(processing);
                });
            }, 2000);
        } else {						// first request received
            cache_hput("uuid_cache", user_key + "_" + uuid, { "status" : "0" }, 120000);
            callback();
        }
    });
}

exports.profile_pic_set = function (obj, callback) {
    profile_pic.remove({"number": obj.number}, function (err) {
        profile_pic.insert(obj, function () {
            callback();
        });
    });
}

exports.profile_doc_files_set = function (obj, callback) {
    profile_doc.insert(obj, function () {
        callback();
    });
}

exports.tempFilesInsert = function (obj, callback) {
    temp_files.insert(obj, function (err) {
        callback(err);
    });
}

exports.resourceFilesInsert = function (obj, callback) {
    resource_files.insert(obj, function (err) {
        callback(err);
    });
}

exports.profile_pic_get = function (obj, callback) {
    profile_pic.findOne(obj, function (err, item) {
        callback(item);
    });
}

exports.profile_doc_files_get = function (filename, callback) {
    profile_doc.find({filename: filename}, {}).toArray(function (err, items) {
        callback(err, items ? items[0] : undefined);
    });
}

exports.tempFilesGet = function (filename, callback) {
    temp_files.find({filename: filename}, {}).toArray(function (err, items) {
        callback(err, items ? items[0] : undefined);
    });
}

exports.resourceFilesGet = function (filename, callback) {
    resource_files.find({filename: filename}, {}).toArray(function (err, items) {
        callback(err, items ? items[0] : undefined);
    });
}

exports.profile_pic_del = function (obj) {
    profile_pic.remove(obj, function (err) {
    });
}

exports.notifications_logbook_get = function (obj, proj, sort, callback) {
    notifications_logbook.find(obj, proj).sort(sort).limit(100).toArray(function (err, item) {
        callback(item);
    });
}

exports.notifications_logbook_count_get = function (obj, callback) {
    notifications_logbook.count(obj, function (err, count) {
        callback(count);
    });
}

exports.notifications_logbookwebhook_get = function (obj, proj, sort, callback) {
    notifications_webhook.find(obj, proj).sort(sort).limit(100).toArray(function (err, item) {
        callback(item);
    });
}

exports.notifications_logbookwebhook_count_get = function (obj, callback) {
    notifications_webhook.count(obj, function (err, count) {
        callback(count);
    });
}

exports.notifications_logbookbill_get = function (obj, proj, sort, callback) {
    notifications_bills.find(obj, proj).sort(sort).limit(100).toArray(function (err, item) {
        callback(item);
    });
}

exports.notifications_logbookemails_get = function (obj, proj, sort, callback) {
    notifications_emails.find(obj, proj).sort(sort).limit(100).toArray(function (err, item) {
        callback(item);
    });
}

exports.notifications_logbooksms_get = function (obj, proj, sort, callback) {
    notifications_sms.find(obj, proj).sort(sort).limit(100).toArray(function (err, item) {
        callback(item);
    });
}

exports.notifications_logbook_set_read = function (liststr, callback) {
    var list = new Array;
    liststr.forEach(function (item) {
        if (item.length == 24) list.push(new mongodb.ObjectId(item));
    });
    var find = { "_id" : { "$in" : list }};
    var modify = { "$set" : { "read" : true } };
    var options = { "multi" : true };
    notifications_logbook.update(find, modify, options, function (err, object) {
        if (err) {
            common.error("notifications_logbook_set_read", err.message);
        }
        callback();
    });
}

exports.notifications_insert = function (col, obj, callback) {
    if (obj) {
        obj.ts = new Date().getTime();
    }

    if ( col == "sms" ) {
        notifications_sms.insert(obj, function () { if (callback) callback() });
    } else if ( col == "push" ) {
        notifications_push.insert(obj, function () { if (callback) callback() });
    } else if ( col == "log" ) {
        notifications_logbook.insert(obj, function () { if (callback) callback() });
    } else if ( col == "webhook" ) {
        notifications_webhook.insert(obj, function () { if (callback) callback() });
    } else if ( col == "bill" ) {
        notifications_bills.insert(obj, function () { if (callback) callback() });
    } else if ( col == "email" ) {
        notifications_emails.insert(obj, function (err, result) { if (callback) callback(err, result) });
    } else if ( col == "hlr" ) {
        notifications_hlr.insert(obj, function () { if (callback) callback() });
    } else if ( col == "auditlog" ) {
        notifications_audit_logs.insert(obj, function() { if (callback) callback() });
    } else {
        if (callback) callback();
    }
}

exports.arrayToString = function arrayToString(values) {
    var stringValues = "";
    if (values) {
        values.forEach(function (value) {
            if (stringValues.length > 0) {
                stringValues += ",";
            }
            stringValues += mysqlEscape(value);
        });
    }
    return stringValues;
}
