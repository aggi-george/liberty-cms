var async = require('async');
var request = require('request');
var db = require('./db_handler');
var config = require('../config');
var common = require('./common');
const slack = require('../src/notifications/slack');

var elitecoreConnector = require('./manager/ec/elitecoreConnector');
var elitecoreUserService = require('./manager/ec/elitecoreUserService');
var elitecorePackagesService = require('./manager/ec/elitecorePackagesService');
var elitecoreUtils = require('./manager/ec/elitecoreUtils');

var log = config.EC_LOGSENABLED;

exports.PACKAGES = {"base" : [], "boost" : [], "bonus" : [], "general" : [] };
exports.ROAMING_ADDON = "PRD00315";
exports.CLI_ADDON = "PRD00445";
exports.ROAMING_ADDONS = config.DEV ? [ "PRD00589", "PRD00315" ] : [ "PRD00649","PRD00650" ];
exports.SPECIAL_ADDON = [ exports.ROAMING_ADDON, exports.CLI_ADDON ];

elitecoreConnector.init(function(wsdlClients) {
});

elitecorePackagesService.init(function(hierarchy) {
	exports.PACKAGES = hierarchy;
});

elitecoreConnector.addStatsUpdateListener(function(result, allRequestsStats, allErrors, allTimeouts) {
	//if (log) common.log("EC Stats", JSON.stringify(allRequestsStats));
	if (result && result.length > 0) {
		var nowMs = new Date().getTime();
		result.forEach(function (item) {
			item.ts = nowMs;
		});
		//if (log) common.log("EC Stats", "Saved " + result.length + " logs");
		db.ecstats.insertMany(result, function(e,r) { if (e) common.error("elitecore addStatsUpdateListener ecstats", e.message) });
	}
	if (allErrors && allErrors.length > 0) {
		db.ecerrors.insertMany(allErrors, function(e,r) { if (e) common.error("elitecore addStatsUpdateListener allErrors", e.message) });
	}
	if (allTimeouts && allTimeouts.length > 0) {
		db.ecerrors.insertMany(allTimeouts, function(e,r) { if (e) common.error("elitecore addStatsUpdateListener allTimeouts", e.message) });
	}
});

////

exports.doInventoryStatusOperation = doInventoryStatusOperation;
function doInventoryStatusOperation(args, callback) {
	elitecoreConnector.doInventoryStatusOperation(args.inventoryNumber, args.operationAlias, callback);
}

exports.repairInventoryRebundle = repairInventoryRebundle;
function repairInventoryRebundle(args, callback) {
	elitecoreConnector.repairInventoryRebundle(args, callback);
}

exports.listInventory = listInventory;
function listInventory(args, callback) {
	elitecoreConnector.listInventory(args.serviceInstanceNumber, callback);
}

exports.unpairInventory = unpairInventory;
function unpairInventory(args, callback) {
	elitecoreConnector.unpairInventory(args, callback);
}

exports.repairInventory = repairInventory;
function repairInventory(args, callback) {
	elitecoreConnector.repairInventory(args, callback);
}

exports.removeInventory = removeInventory;
function removeInventory(args, callback) {
	elitecoreConnector.removeInventory(args.serviceInstanceNumber, args.inventoryNumber, callback);
}

exports.addInventory = addInventory;
function addInventory(args, callback) {
	elitecoreConnector.addInventory(args.effectDate, args.inventoryNumber, args.serviceInstanceNumber ,callback);
}

exports.getInventoryDetail = getInventoryDetail;
function getInventoryDetail(args, callback) {
	elitecoreConnector.getInventoryDetail(args, callback);
}

exports.createAccount = createAccount;
function createAccount(args, callback) {
	elitecoreConnector.createAccount(args, callback);
}

exports.changeServicePlan = changeServicePlan;
function changeServicePlan(args, callback) {
	elitecoreConnector.changeServicePlan(args.billingAccountNumber, args.effectiveDate,
		args.packageName, args.remarks, args.serviceInstanceNumber, callback);
}

exports.searchAccount = searchAccount;
function searchAccount(args, callback) {
	elitecoreConnector.searchAccount(args.mobileOrMSISDNNumber, args.accountNumber, args.accountName, args.serviceUserName, callback);
}

exports.searchCreditNote = searchCreditNote;
function searchCreditNote(args, callback) {
	elitecoreConnector.searchCreditNote(args, callback);
}

exports.getBillingAccountDetail = getBillingAccountDetail;
function getBillingAccountDetail(args, callback) {
	elitecoreConnector.getBillingAccountDetail(args.accountNumber, callback);
}

exports.updateCustomerAccountData = updateCustomerAccountData;
function updateCustomerAccountData(args, callback) {
	elitecoreConnector.updateCustomerAccountData(args, callback);
}

exports.getCustomerHierarchyDetails = getCustomerHierarchyDetails;
function getCustomerHierarchyDetails(args, callback) {
	elitecoreConnector.getCustomerHierarchyDetails(args.accountNumber, callback);
}

exports.updateBillingAccount = updateBillingAccount;
function updateBillingAccount(args, callback) {
	elitecoreConnector.updateBillingAccount(args, callback);
}

exports.updateServiceAccount = updateServiceAccount;
function updateServiceAccount(args, callback) {
	elitecoreConnector.updateServiceAccount(args, callback);
}

exports.getProductOfferDetails = getProductOfferDetails;
function getProductOfferDetails(args, callback) {
	elitecoreConnector.getProductOfferDetails(callback);
}

exports.getSpecificProdOfferDetails = getSpecificProdOfferDetails;
function getSpecificProdOfferDetails(args, callback) {
	elitecoreConnector.getSpecificProdOfferDetails(args, callback);
}

exports.getBusinessHierarchyDetail = getBusinessHierarchyDetail;
function getBusinessHierarchyDetail(callback) {
	elitecoreConnector.getBusinessHierarchyDetail(callback);
}

exports.searchInventory = searchInventory;
function searchInventory(args, callback) {
	elitecoreConnector.searchInventory(args.inventoryNumber, callback);
}

exports.subscribeAddOnPackage = subscribeAddOnPackage;
function subscribeAddOnPackage(args, callback){
	logAddon("subscribeAddOnPackage", args, undefined, undefined, function(_id) {
		elitecoreConnector.subscribeAddOnPackage(args, function(err ,result) {
			args._id = _id;
			logAddon("subscribeAddOnPackage", args, err, result);
			callback(err, result);
		});
	});
}

exports.unSubscribeAddOnPackage = unSubscribeAddOnPackage;
function unSubscribeAddOnPackage(args, callback) {
	logAddon("unSubscribeAddOnPackage", args, undefined, undefined, function(_id) {
		elitecoreConnector.unSubscribeAddOnPackage(args, function(err ,result) {
			args._id = _id;
			logAddon("unSubscribeAddOnPackage", args, err, result);
			callback(err, result);
		});
	});
}

exports.getFnFMembersDetails = getFnFMembersDetails;
function getFnFMembersDetails(args, callback) {
	elitecoreConnector.getFnFMembersDetails(args, callback);
}

exports.addFnFMembers = addFnFMembers;
function addFnFMembers(args, callback) {
	elitecoreConnector.addFnFMembers(args, callback);
}

exports.removeFnFMembers = removeFnFMembers;
function removeFnFMembers(args, callback) {
	elitecoreConnector.removeFnFMembers(args, callback);
}

exports.searchPayment = searchPayment;
function searchPayment(args, callback) {
	elitecoreConnector.searchPayment(args, callback);
}

exports.createCreditNote = createCreditNote;
function createCreditNote(args, callback) {
	elitecoreConnector.createCreditNote(args, callback);
}

exports.modifyPlanAttributes = modifyPlanAttributes;
function modifyPlanAttributes(args, callback) {
	elitecoreConnector.modifyPlanAttributes(args, callback);
}

exports.changeServiceInstanceStatus = changeServiceInstanceStatus;
function changeServiceInstanceStatus(args, callback) {
	elitecoreConnector.changeServiceInstanceStatus(args, callback);
}

exports.viewCustomer = viewCustomer;
function viewCustomer(args, callback) {
	elitecoreConnector.viewCustomer(args.subscriberIdentifier, args.fromDate, callback);
}

exports.viewCustomerUsageInformation = viewCustomerUsageInformation;
function viewCustomerUsageInformation(args, callback) {
	elitecoreConnector.viewCustomerUsageInformation(args, callback);
}

exports.nextBill = nextBill;
function nextBill(billing_cycle, ref, refUTC) {
	return elitecoreUtils.calculateBillCycle(billing_cycle, ref, refUTC);
}

exports.getCustomerDetailsNumber = getCustomerDetailsNumber;
function getCustomerDetailsNumber(prefix, number, fresh, callback, basic) {
	elitecoreUserService.loadUserInfoByPhoneNumber(number, callback, fresh, basic);
}

exports.getCustomerDetailsBilling = getCustomerDetailsBilling;
function getCustomerDetailsBilling(billingAccount, callback, basic) {
	elitecoreUserService.loadUserInfoByBillingAccountNumber(billingAccount, callback, false, basic);
}

exports.getCustomerDetailsAccount = getCustomerDetailsAccount;
function getCustomerDetailsAccount(accountNumber, fresh, callback, basic) {
	elitecoreUserService.loadUserInfoByCustomerAccountNumber(accountNumber, callback, fresh, basic);
}

exports.getCustomerDetailsServiceInstance = getCustomerDetailsServiceInstance;
function getCustomerDetailsServiceInstance(serviceInstanceNumber, fresh, callback, basic) {
	elitecoreUserService.loadUserInfoByServiceInstanceNumber(serviceInstanceNumber, callback, fresh, basic);
}

exports.getCustomerDetailsService = getCustomerDetailsService;
function getCustomerDetailsService(serviceInstanceNumber, fresh, callback, basic) {
	elitecoreUserService.loadUserInfoByServiceInstanceNumber(serviceInstanceNumber, callback, fresh, basic);
}

exports.getCache = getCache;
function getCache(userKey, fresh, callback) {
	elitecoreUserService.loadUserInfoByUserKey(userKey, callback, fresh);
}

exports.setCache = setCache;
function setCache(number, prefix, user_key, account, callback) {
	if (user_key) {
		elitecoreUserService.loadUserInfoByUserKey(user_key, callback, true);
	} else if (number) {
		elitecoreUserService.loadUserInfoByPhoneNumber(number, callback, true);
	} else {
		elitecoreUserService.loadUserInfoByCustomerAccountNumber(account, callback, true);
	}
}

//*****************************

exports.packages = packages;
function packages() {
	return exports.PACKAGES;
}

exports.effective = effective;
function effective(id) {
	var options = [ "immediate", "next_day", "next_bill_cycle" ];
	if ( typeof(id) == "undefined" ) {
		return options;
	} else {
		return (typeof(id) == "number") ? options[id] : options.indexOf(id);
	}
}

function findInArray(arr, id, type) {
	var found = false;
	arr.forEach(function (item) {
		if (item[type] == id) {
			found = item;
			return false;
		} else {
			return true;
		}
	});
	return found;
}

function findInObject(obj, id, type) {
	var found = false;
	Object.keys(obj).every(function (key) {
		if ( common.isArray(obj[key]) ) {
			found = findInArray(obj[key], id, type);
			if (found) {
				return false;
			} else {
				return true;
			}
		} else {
			return true;
		}
	});
	return found;
}

exports.findPackage = findPackage;
function findPackage(obj, id) {
	return JSON.parse(JSON.stringify(findPackageOriginal(obj, id)));
}

exports.findPackageOriginal = findPackageOriginal;
function findPackageOriginal(obj, id) {
	var found = new Object;
	Object.keys(obj).every(function (o1) {
		if (common.isArray(obj[o1])) {
			found = findInArray(obj[o1], id, "id");
			if (found && Object.keys(found).length) {
				return false;
			}
			obj[o1].every(function (o2) {
				found = findInObject(o2, id, "id");
				if (found && Object.keys(found).length) {
					return false;
				}
				return true;
			});
			if (found && Object.keys(found).length) {
				return false;
			}
		}
		return true;
	});
	return found;
}

exports.findPackageWithFreeId = findPackageWithFreeId;
function findPackageWithFreeId(obj, id) {
	var found = new Object;
	Object.keys(obj).every(function (o1) {
		if (common.isArray(obj[o1])) {
			found = findInArray(obj[o1], id, "free_package");
			if (found && Object.keys(found).length) {
				return false;
			}
			obj[o1].every(function (o2) {
				found = findInObject(o2, id, "free_package");
				if (found && Object.keys(found).length) {
					return false;
				}
				return true;
			});
			if (found && Object.keys(found).length) {
				return false;
			}
		}
		return true;
	});
	return found;
}

exports.findPackageName = findPackageName;
function findPackageName(obj, name) {
	var found = new Object;
	Object.keys(obj).every(function (o1) {
		if (common.isArray(obj[o1])) {
			found = findInArray(obj[o1], name, "name");
			if (found && Object.keys(found).length) {
				return false;
			}
			obj[o1].every(function (o2) {
				found = findInObject(o2, name, "name");
				if (found && Object.keys(found).length) {
					return false;
				}
				return true;
			});
			if (found && Object.keys(found).length) {
				return false;
			}
		}
		return true;
	});
	return common.deepCopy(found);
}

//TODO
exports.is_circles = is_circles;
function is_circles(number, prefix, callback) {
	 if ( parseInt(prefix) == 65 ) {
		searchAccount({ "mobileOrMSISDNNumber" : number }, function(error, response) {
			if ( error || !response || !response.return ||
				!response.return.searchAccountResponseVO ||
				!response.return.searchAccountResponseVO[0] ) {
				callback(0);
			} else {
				if ( (response.return.searchAccountResponseVO[0].accountStatus == "ACTIVE") ||
						(response.return.searchAccountResponseVO[0].accountStatus == "INACTIVE") ) {
					var now = new Date().getTime();
					viewCustomer({	"subscriberIdentifier" : number,
							"fromDate" : now
							}, function(err, result) {
						var base_plan = "";
						if (result && result.return && result.return.responseObject && result.return.responseObject.packageData) {
							result.return.responseObject.packageData.every(function (item) {
								item.from = parseInt(item.validFromDate);
								item.to = parseInt(item.validToDate);
								if ( (item.from <= now) && (item.to > now) ) {
									exports.PACKAGES.base.every(function (base) {
										if ( base.options.map(function(o) { return o.name }).indexOf(item.packageName) > -1 ) {
											base_plan = item.packageName;
											return false;
										} else return true;
									});
								}
								if ( base_plan != "" ) return false;
								else return true;
							});
						}
						if (base_plan == "") common.error("Error Base Plan", number);
						callback(1, base_plan);
					});
				} else {
					callback(0);
				}
			}
		});
	} else {
		callback(0);
	}
}

function modifyAttribute(serviceInstance, packageHistoryId, ts, category, value, callback) {
	if (!ts) ts = new Date(common.msOffsetSG()).toISOString().split(".")[0];
	var args = new Object;
	args.serviceInstanceNumber = serviceInstance;
	args.packageHistoryId = (typeof(packageHistoryId) == "object") ? packageHistoryId[0] : packageHistoryId;
	args.effectiveDate = ts;
	args.internalDemographicRequestVOs = {
		"category" : category,
		"strValue" : value.toString(),			// set string
		"value" : parseInt(value)		// set int
	};
	if (log) common.log("modifyAttribute", JSON.stringify(args));
	modifyPlanAttributes(args, function (err, result) {
		callback(err, result);
	});
}

exports.addonSubscribe = addonSubscribe;
function addonSubscribe(number, id, addon_list, bill_cycle, callback) {
	var cycle = nextBill(bill_cycle);
	var list = new Array;
	var modify_effect;
        if (addon_list.length == 0) {
                return callback(new Error("addonSubscribe - Empty list"));
        }
	addon_list.forEach(function(addon) {
		var effect;
		var obj = new Object;
		obj.packageId = addon.product_id;
		var now = new Date().getTime();
		var tomorrow = common.getDate(new Date(now + 24 * 60 * 60 * 1000)) + "T00:00:00";
		if ( effective(addon.effect) == "immediate" ) {
			effect = undefined;
		} else if ( effective(addon.effect) == "next_day" ) {
			effect = tomorrow;
		} else {
			effect = cycle.end + "T00:00:00";		// next bill cycle
		}
		obj.fromDate = addon.fromDate ? addon.fromDate : effect;
		obj.billStartDate = (effect < obj.fromDate) ? obj.fromDate : effect;
		if ( !addon.recurrent ) {
			//TODO add support of effect 1 "Next Day"
			if (addon.effect == 0 || addon.effect == 1) {
				// send the last day of billing cycle without time
				obj.toDate = common.getDate(new Date(new Date(cycle.end).getTime() - (24 * 60 * 60 * 1000)));
			} else {
				var nextCycleEnd = new Date(cycle.end);
				var toDate = new Date(nextCycleEnd.setMonth(nextCycleEnd.getMonth() + 1));
				// send the last day of billing cycle without time
				obj.toDate = common.getDate(new Date(toDate.getTime() - (24 * 60 * 60 * 1000)));
			}
		} else if (addon.toDate) {
			var date = addon.toDate.toISOString().split(".")[0];
			obj.toDate = date;
		}

		if ( exports.SPECIAL_ADDON.indexOf(addon.product_id) > -1 ) modify_effect = effect;
		if ( exports.ROAMING_ADDONS.indexOf(addon.product_id) == -1 ) list.push(obj);
	});
	var start = new Date();
	if (log) common.log("addonSubscribe " + id, JSON.stringify(list));
	if ( list && list.length > 0 ) {
		subscribeAddOnPackage({ "serviceInstanceNumber" : id, "addOnPlans" : list }, function(err, result) {
			var api_time = new Date() - start;
			if (log) common.log("addonSubscribe", JSON.stringify(result));
			if ( err ) {
				callback(err, result);
			} else if ( !result || !result.return || (result.return.responseCode != "0") ) {
				callback(new Error("Failed to subscribe, empty response or invalid response code"), result);
			} else {
				if ( list && list[0] && (exports.SPECIAL_ADDON.indexOf(list[0].packageId) > -1) ) {
					var tag = "";
					if ( list[0].packageId == exports.ROAMING_ADDON ) {	// catch roaming, call modify plan API
						tag = "AUTO_ROAMING";
					} else {					// CLI, we only have 2
						tag = "CALLER_ID";
					}
					var packageHistoryId = result.return.successAddOnPlans[0].packageHistoryId;
					modifyAttribute(id, packageHistoryId, modify_effect, tag, "1", function (error, response) {
						if (error && !result) common.error("addonSubscribe modify", JSON.stringify(error));
						else {
							if (log) common.log("addonSubscribe modify", JSON.stringify(response));
						}
						callback(undefined, result.return.successAddOnPlans);
					});
				} else {
					callback(undefined, result.return.successAddOnPlans);
				}
			}
		});
	} else {
		callback(undefined, null);		// nothing to do
	}
}

exports.addonUnsubscribe = addonUnsubscribe;
function addonUnsubscribe(number, id, addon_list, bill_cycle, callback) {
	var now = common.msOffsetSG();
	var cycle = nextBill(bill_cycle);
	var tasks = new Array;
	if (addon_list.length == 0) {
		return callback(new Error("addonUnsubscribe - Empty list"));
	}
	addon_list.forEach(function (item) {
		var effectiveDate;
		if ( effective(item.effect) == "immediate" ) {
			effectiveDate = new Date(now).toISOString().split(".")[0];	// now
		} else if ( effective(item.effect) == "next_day" ) {	// tomorrow
			if ( item.current ) {		// tomorrow, current
				effectiveDate = common.getDate(new Date(now)) + "T23:59:59";
			} else {				// tomorrow, future
				effectiveDate = common.getDate(new Date(now + 24 * 60 * 60 * 1000)) + "T00:00:01";
			}
		} else if ( item.current ) {		// next bill, current
			effectiveDate = new Date(new Date(cycle.end).getTime() - 1000).toISOString().split(".")[0];
		} else {					// next bill, future
			effectiveDate = new Date(new Date(cycle.end).getTime() + 1000).toISOString().split(".")[0];
		}
		var unsub = { "serviceInstanceNumber" : id,
				"packageHistoryIdList" : item.packageHistoryId,
				"effectiveDate" : effectiveDate }; 
		var tag;
		if ( item.product_id == exports.ROAMING_ADDON ) {		// catch roaming, call modify plan API
			tag = "AUTO_ROAMING";
		} else if ( item.product_id == exports.CLI_ADDON ) {
			tag = "CALLER_ID"; 
		}
		if (tag) {
			tasks.push(function (cb) {
				modifyAttribute(id, item.packageHistoryId, effectiveDate, tag, "0", function (error, response) {
					if (error && !response) {
						common.error("addonUnsubscribe modify", JSON.stringify(error));
						return cb(true, error);
					} else {
						if (log) common.log("addonUnsubscribe modify", JSON.stringify(response));
					}

					if (log) common.log("addonUnsubscribe", JSON.stringify(unsub));
					unSubscribeAddOnPackage(unsub, function (err, result) {
						if (log) common.log("addonUnsubscribe", JSON.stringify(result));
						// code 0 shows that addon has been unsubscribed
						// code -1003028003 shows that addon was unsubscribed before

						if (result && result.return && result.return.responseCode === '-1003028003') {
							return cb(undefined, {
								responseCode: '0',
								responseMessage: 'Add was unsubscribe before.',
								ecCode: "-1003028003"
							});
						} else if (err) {
							return cb(err, result);
						} if (err || !result || !result.return ||
							(result.return.responseCode != "0")) {
							return cb(new Error("Failed to unsubscribe, empty response or invalid response code"), result);
						} else {
							return cb(undefined, result.return);
						}
					});
				});
			});
		} else {
			tasks.push(function (cb) {
				if (log) common.log("addonUnsubscribe", JSON.stringify(unsub));
				unSubscribeAddOnPackage(unsub, function(err, result) {
					if (log) common.log("addonUnsubscribe", JSON.stringify(result));

					// code 0 shows that addon has been unsubscribed
					// code -1003028003 shows that addon was unsubscribed before

					if (result && result.return && result.return.responseCode === '-1003028003') {
						cb(undefined, {
							responseCode: '0',
							responseMessage: 'Add was unsubscribe before.',
							ecCode: "-1003028003"
						});
					} else if (err) {
						cb(err, result);
					} else if (!result || !result.return || result.return.responseCode != "0") {
						cb(new Error("Failed to unsubscribe, empty response or invalid response code"), result);
					} else {
						cb(undefined, result.return);
					}
				});
			});
		}
	});
	async.parallelLimit(tasks, 1, function (err, result) {
		callback(err, result);
	});
}

// TODO move to another file
exports.addCreditNotes = function(billinAccountName, billingAccountNumber, amountMinor, reason, callback) {
	var date = new Date().toISOString().split("T")[0];
	var params = {
		"accountCurrencyAlias": "SGD",
		"accountList": [{
			"accountName": billinAccountName,
			"accountNumber": billingAccountNumber,
			"applyTax": "Y",
			"exReceivedAmount": amountMinor,
			"receivedAmount": amountMinor
		}],
		"eventAlias": "CREATE_CREDIT_EVENT",
		"paymentCurrencyAlias": "SGD",
		"paymentDate": date,
		"reasonAlias": reason,
		"staffId": "S000103",
		"staffName": "LWUser"
	};

	exports.createCreditNote(params, function (err, result) {
		callback(err, result);
	});
}


// TODO remove after removing from actions.js + portInManager.js
exports.getInventory = getInventory;
function getInventory(serviceInstanceNumber, callback) {
	listInventory({ "serviceInstanceNumber" : serviceInstanceNumber }, function(err, result) {
		if ( !err && result && result.return && result.return.inventories ) {
			var list = new Array;
			result.return.inventories.forEach(function (item) {
				var obj = {	"number" : item.inventoryNumber,
						"status" : item.status };
				if (item.bundleInventoryRes) {
					obj.iccid = item.bundleInventoryRes.inventoryNumber;
					if (item.bundleInventoryRes.bundleInventoryRes) {
						obj.imsi = item.bundleInventoryRes.bundleInventoryRes.inventoryNumber;
					}
				}
				list.push(obj);
			});
			callback(undefined, list);
		} else {
			var error = JSON.stringify({ "error" : err, "result" : result });
			callback(new Error("EC error, err=" + JSON.stringify(error)));
		}
	});
}

// TODO remove after removing from execute_action.js
exports.getCustomerNumberByServiceInstance = getCustomerNumberByServiceInstance;
function getCustomerNumberByServiceInstance(serviceInstanceNumber, callback) {
	getInventory(serviceInstanceNumber, function (err, inventory) {
		if (err) {
			return callback(err);
		}
		if (!inventory) {
			return callback(new Error("Inventory is not found"));
		}

		//"CST01" active number
		var number;
		inventory.forEach(function (item) {
			if (item.status == "CST01") number = item.number;
		});

		callback(undefined, number);
	});
}

// TODO remove after removing from  autorepair.js + daily.js
exports.getAccountNumber = getAccountNumber;
function getAccountNumber(prefix, number, callback) {
	searchAccount({ "mobileOrMSISDNNumber" : number }, function(err, sAccount) {
		if (!err) {
			if (sAccount && sAccount.return && sAccount.return.searchAccountResponseVO &&
					sAccount.return.searchAccountResponseVO[0] &&
					sAccount.return.searchAccountResponseVO[0].accountNumber) {
				var accountNumber = sAccount.return.searchAccountResponseVO[0].accountNumber;
				callback(undefined, accountNumber);
			} else callback(new Error("Account Number Not Found " + number));
		} else callback(new Error(JSON.stringify(err)));
	});
}

// TODO move to a separate file
exports.isLockdown = isLockdown;
function isLockdown(billing_cycle, dref) {
	var dateRef = (dref) ? new Date(dref) : new Date();
	var end = nextBill(billing_cycle, dateRef).endDate.getTime() - (2 * 60 * 60 * 1000); // SGT 6:00am of billing cycle
	var start = end - (6 * 60 * 60 * 1000) - (10 * 60 * 1000);	// SGT 11:50pm before billing cycle
	var lock = {"enabled": 0};
	lock.title = "Maintenance Activity";
	lock.message = "Account changes are locked as of the moment because of bill processing, " +
	"please try again after 6AM";
	if ((dateRef.getTime() > start) && (dateRef.getTime() < end)) {
		lock.enabled = 1;
	}
	return lock;
}

function logAddon(method, args, error, result, callback) {
    if (args && !args._id && !error && !result) {
        var serviceInstanceNumber = (args.serviceInstanceNumber) ? args.serviceInstanceNumber : undefined;
        db.addonlog.insert({
                "ts" : new Date().getTime(),
                "myfqdn" : config.MYFQDN,
                "method" : method,
                "type" : (args && args.addOnPlans && args.addOnPlans[0] && args.addOnPlans[0].packageId ? args.addOnPlans[0].packageId : ""),
                "serviceInstanceNumber" : serviceInstanceNumber,
                "args" : args
            }, function(e,r) {
            if (r && r.insertedIds && r.insertedIds[0]) callback(r.insertedIds[0]);
            else callback();
        });
    } else if (args && args._id) {
        var addonStatus = ( result && result.return && (result.return.responseCode == "0") ) ? "OK" : "ERROR";
        db.addonlog.update({ "_id" : args._id }, { "$set" : {
            "ts_end" : new Date().getTime(),
            "status" : addonStatus,
            "result" : result,
            "error" : error
        } }, callback);
        if (addonStatus === "ERROR") slack.send({
            channel: (config.DEV ? '#rikerelitecore-errors' : '#elitecore-errors'),
            username: `BSS Addon Bot (method) ${config.MYFQDN}`,
            text: `Addon Failed on BSS ${args.serviceInstanceNumber} - ${JSON.stringify(args)}\nError: ${JSON.stringify(error)}\nResult: ${JSON.stringify(result)}`
        });
    }
}
