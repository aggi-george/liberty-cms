var common = require('./common');
var db = require('./db_handler');
var request = require('request');

exports.get = get;

function get(ip, callback) {
	db.cache_get("ip_cache", ip, function(s) {
		if ( s ) callback(undefined, s);
		else {
			request({	"uri" : "http://ipinfo.io/" + ip + "/json",
					"method" : "GET",
					"timeout" : 20000
					}, function(err, response, body) {
				if ( !err && (parseInt(response.statusCode) != 429) ) {
					if ( body != "Please provide a valid IP address" ) {
						s = common.safeParse(body);
						callback(undefined, s);
						db.cache_put("ip_cache", ip, s, 24 * 60 * 60 * 1000);
					} else {
						callback(new Error("IP not valid"));
					}
				} else {
					callback(new Error("Error IP Search"));
				}
			});
		}
	});
}
