var request = require('request');
var common = require('../../common');
var db = require('../../db_handler');
var config = require('../../../config');

var elitecoreConnector = require('./elitecoreConnector');
var elitecoreUserService = require('./elitecoreUserService');
var elitecoreUtils = require('./elitecoreUtils');

var BaseError = require('../../../src/core/errors/baseError');

var log = config.EC_LOGSENABLED;
var networks = {
    M1: "001",
    STARHUB: "002",
    SINGTEL: "003",
    "001": "001",
    "002": "002",
    "003": "003"
}
var testReasons = {
    "11111101": "ERROR_PREPAID",
    "11111102": "ERROR_ID_MISTMATCH",
    "11111103": "ERROR_PENDING_ORDER",
    "11111104": "ERROR_SUSPENDED",
    "11111105": "ERROR_INACTIVE",
    "11111106": "ERROR_INCORRECT_TRANSFER_TIME",
    "11111107": "ERROR_DONOR_TELCO",
    "11111108": "ERROR_MSISDN_NOT_EXISTS"
}

let statusMap = {
    PortIn_Approval_Failed: 'FAILED',
    PortIn_Success: 'DONE',
    PortIn_Submitted: 'IN_PROGRESS',
};

const bluebird = require("bluebird");


module.exports = {

    /**
     *
     */
    portInNumberImmediate: function (serviceInstanceNumber, tempNumber, portInNumber,
        donor, externalRefId, callback, portInRecovery) {
        module.exports.portInNumber(serviceInstanceNumber, tempNumber, portInNumber,
            donor, new Date(), externalRefId, callback, portInRecovery);
    },

    /**
     *
     */
    portInNumber: function (serviceInstanceNumber, tempNumber, portInNumber, donor, startTime, externalRefId,
        callback, portInRecovery) {
        if (!callback) callback = function () {
        };

        var donorCode = networks[donor];
        if (!tempNumber) {
            common.error("ElitecorePortInManager", "portInNumber: temp number is missing");
            return callback(new BaseError("Temp number is missing params missing", "ERROR_EMPTY_TEMP_NUMBER"));
        }

        if (!serviceInstanceNumber || !portInNumber || !startTime) {
            common.error("ElitecorePortInManager", "portInNumber: mandatory params are missing");
            return callback(new BaseError("Mandatory params missing (serviceInstanceNumber, portInNumber, startTime)",
                "ERROR_INVALID_PARAMS"));
        }

        if (tempNumber == portInNumber) {
            common.error("ElitecorePortInManager", "portInNumber: temp number is equal to port in number");
            return callback(new BaseError("Temp number is equal to port in number", "ERROR_EQUAL_PORTIN_AND_TEMP_NUMBERS"));
        }


        if (!donorCode) {
            common.error("ElitecorePortInManager", "portInNumber: donor (name / code) " + donor + " is not supported");
            return callback(new BaseError("Invalid donor", "ERROR_INVALID_DONOR"));
        }

        // need to check if temp number is paired

        checkInventory(serviceInstanceNumber, portInNumber, (err, checkInventoryResult) => {
            if (err) {
                return callback(err, checkInventoryResult);
            }

            var connector = elitecoreConnector;
            var numberAlreadyAttached = checkInventoryResult.numberAlreadyAttached;
            var result = {flow: checkInventoryResult.flow};

            Promise.resolve(checkInventoryResult.foundInventoryResult)
                .then((foundInventoryResult) => {
                    result.flow.push({description: "check if attached number has valid status", result: foundInventoryResult});

                    if (!foundInventoryResult || !foundInventoryResult.obj) {
                        if (log) common.log("ElitecorePortInManager", "inventory " + portInNumber +
                        " does not eixist no need to check status");
                        return;
                    } else if (foundInventoryResult.obj.status == "CST01") {
                        if (log) common.log("ElitecorePortInManager", "inventory " + portInNumber +
                        " is attached and has valid status CST01");
                        return;
                    }
                    numberAlreadyAttached = false;

                    if (log) common.log("ElitecorePortInManager", "removing inventory with status invalid " +
                    foundInventoryResult.obj.status + "...");
                    return connector.getPromise(connector.removeInventory, {
                        serviceInstanceNumber: serviceInstanceNumber,
                        inventoryNumber: portInNumber,
                        validResponses: [{code: 0}]
                    });
                })
                .then((removeInvalidInventoryResult) => {
                    result.flow.push({description: "remove attached number if status is invalid ", result: removeInvalidInventoryResult});

                    if (log) common.log("ElitecorePortInManager", "searching existing inventory...");
                    return connector.getPromise(connector.searchInventory, {
                        inventoryNumber: portInNumber,
                        validResponses: [{code: 0, keys: ["inventoryDetails", "$array0"]}, {code: -1}]
                    });
                })
                .then((searchInventoryResult) => {
                    result.flow.push({description: "check if need to create new inventory", result: searchInventoryResult});

                    if (searchInventoryResult.code == 0) {
                        if (log) common.log("ElitecorePortInManager", "inventory " + portInNumber +
                        " has been found, NO need to create new");
                        // skip creating and move to the next operation
                        return;
                    } else if (searchInventoryResult.code == -1) {
                        if (log) common.log("ElitecorePortInManager", "inventory " + portInNumber +
                        " has NOT been found");
                    }

                    if (log) common.log("ElitecorePortInManager", "creating new...");
                    return connector.getPromise(connector.createInventory, {
                        msisdn: portInNumber, donorCode: donorCode, date: startTime,
                        validResponses: [{code: 0}]
                    });
                })
                .then((createInventoryResult) => {
                    result.flow.push({description: "create new inventory", result: createInventoryResult});

                    if (!createInventoryResult) {
                        if (log) common.log("ElitecorePortInManager", "inventory " + portInNumber + " creation has been skipped");
                    } else if (createInventoryResult.code == 0) {
                        if (log) common.log("ElitecorePortInManager", "inventory " + portInNumber + " has been created");
                    }

                    if (log) common.log("ElitecorePortInManager", "updating attributes...");
                    return connector.getPromise(connector.updateInventoryAttributes, {
                        msisdn: portInNumber, donorCode: donorCode, date: startTime,
                        validResponses: [{code: 0}]
                    });
                })
                .then((updateAttributesResult) => {
                    result.flow.push({description: "update inventory attributes", result: updateAttributesResult});

                    if (updateAttributesResult.code == 0) {
                        if (log) common.log("ElitecorePortInManager", "invenory " + portInNumber +
                        " attributes have been updated");
                    }

                    if (log) common.log("ElitecorePortInManager", "loading inventory status...");
                    return connector.getPromise(connector.searchInventory, {
                        inventoryNumber: portInNumber,
                        validResponses: [{code: 0, keys: ["inventoryDetails", "$array0"]}, {code: -1}]
                    });
                })
                .then((searchInventoryResult) => {
                    result.flow.push({description: "load inventory status", result: searchInventoryResult});

                    if (searchInventoryResult.code == 0) {
                        var status = searchInventoryResult.obj.inventoryStatus;
                        if (status == "Available") {
                            if (log) common.log("ElitecorePortInManager", "inventory " + portInNumber +
                            " is already 'Available', NO need to change status");
                            // skip update of status and move to the next operation
                            return;
                        } else if (status == "In-Use" && numberAlreadyAttached) {
                            if (log) common.log("ElitecorePortInManager", "inventory " + portInNumber + " is already attached " +
                            "has In-Use status already available, NO need to change status or reattach");
                            // skip update of status and move to the next operation
                            return;
                        } else {
                            if (log) common.log("ElitecorePortInManager", "inventory " + portInNumber +
                            " has " + status + " status, it is required to update status");
                        }
                    }

                    if (log) common.log("ElitecorePortInManager", "updating inventory status to 'Available'(FREE)...");
                    return connector.getPromise(connector.doInventoryStatusOperation, {
                        inventoryNumber: portInNumber, operationAlias: "FREE",
                        validResponses: [{code: 1}]
                    });
                })
                .then((updateStatusResult) => {
                    result.flow.push({description: "set inventory status to 'Available'", result: updateStatusResult});

                    if (!updateStatusResult) {
                        if (log) common.log("ElitecorePortInManager", "inventory " + portInNumber +
                        " status update has been skipped, already 'Available' or 'In-Use' for the same customer");
                    } else if (updateStatusResult.code == 0) {
                        if (log) common.log("ElitecorePortInManager", "inventory " + portInNumber + " status has been changed");
                    }

                    if (numberAlreadyAttached) {
                        if (log) common.log("ElitecorePortInManager", "inventory " + portInNumber +
                        " is already attahced, NO need to add it agian");
                        return;
                    }

                    if (log) common.log("ElitecorePortInManager", "adding inventory to a customer...");
                    return connector.getPromise(connector.addInventory, {
                        inventoryNumber: portInNumber,
                        serviceInstanceNumber: serviceInstanceNumber,
                        validResponses: [{code: 0}]
                    });
                })
                .then((addInventoryResult) => {
                    result.flow.push({description: "add inventory to a customer", result: addInventoryResult});

                    if (!addInventoryResult) {
                        if (log) common.log("ElitecorePortInManager", "inventory " + portInNumber +
                        " adding has been skipped, already attached");
                    } else if (addInventoryResult.code == 0) {
                        if (log) common.log("ElitecorePortInManager", "inventory " + portInNumber +
                        " has been added to customer " + serviceInstanceNumber + "(SIN)");
                    }

                    if (portInRecovery) {
                        return connector.getPromise(connector.mnpNotification, {
                            MSISDN: portInNumber,
                            validResponses: [{code: 0}]
                        });
                    } else {
                        if (log) common.log("ElitecorePortInManager", "send provisioning notification...");
                        return connector.getPromise(connector.serviceProvisioningNotification, {
                            inventoryNumber: tempNumber,
                            externalRefId: externalRefId,
                            validResponses: [{code: 0}]
                        });
                    }
                })
                .then((provNotificationResult) => {
                    if (portInRecovery) {
                        result.flow.push({
                            description: "send MNP notification",
                            result: provNotificationResult
                        });

                        if (!provNotificationResult) {
                            // do nothing
                        } else if (provNotificationResult.code == 0) {
                            if (log) common.log("ElitecorePortInManager", "NMP notification for port-in number " + portInNumber +
                            " has been send, temp number " + tempNumber + ", should be removed");
                        }
                    } else {
                        result.flow.push({
                            description: "send provisioning notification",
                            result: provNotificationResult,
                            tempNumber: tempNumber
                        });

                        if (!provNotificationResult) {
                            // do nothing
                        } else if (provNotificationResult.code == 0) {
                            if (log) common.log("ElitecorePortInManager", "provisioning notification for port-in number " + portInNumber +
                            " has been send, temp number " + tempNumber);
                        }
                    }

                    elitecoreUserService.loadUserInfoByServiceInstanceNumber(serviceInstanceNumber, () => {
                    }, true)

                    callback(undefined, result);
                })
                .catch(function (err) {
                    if (err) {
                        if (!result) result = {};
                        result.error = {
                            status: err.status,
                            message: err.message,
                            func: err.func,
                            requestParams: err.params,
                            result: err.raw
                        }
                    }

                    if (err.message && err.message.indexOf("ETIMEDOUT") >= 0) {
                        err.status = "BSS_API_TIMEOUT";
                        elitecoreUserService.loadUserInfoByServiceInstanceNumber(serviceInstanceNumber, () => {
                        }, true)

                        if (!result) result = {};
                        result.damageInventoryResult = {ignored: true, reason: "ETIMEDOUT"};
                        callback(err, result);
                    } else {
                        module.exports.deactivatePortInNumber(serviceInstanceNumber, portInNumber, (deactivationErr, deactivationResult) => {
                            if (err) {
                                common.error("ElitecorePortInManager", "failed to process port-in request, status=" + err.status +
                                ", message=" + err.message + ", func=" + err.func + ", func=" + JSON.stringify(err.params) +
                                ", raw=" + JSON.stringify(err.raw));
                            } else {
                                common.error("ElitecorePortInService", "failed to process port-in request, no error provided");
                            }

                            elitecoreUserService.loadUserInfoByServiceInstanceNumber(serviceInstanceNumber, () => {
                            }, true)

                            if (!result) result = {};
                            result.damageInventoryResult = deactivationResult;
                            callback(err, result);
                        });
                    }
                });
        });
    },

    /**
     * Function will mark a number as damaged (and remove if attached) if the number
     * is not atteched to any service instance or attached to designated
     *
     * @param serviceInstanceNumber
     * @param portInNumber
     * @param callback
     * @returns {*}
     */
    deactivatePortInNumber: function (serviceInstanceNumber, portInNumber, callback) {
        if (!callback) callback = function () {
        };

        if (!serviceInstanceNumber || !portInNumber) {
            if (log) common.log("ElitecorePortInService", "portInNumber: mandatory params are missing");
            return callback(new BaseError("Mandatory params missing (serviceInstanceNumber, portInNumber)", "ERROR_INVALID_PARAMS"));
        }

        checkInventory(serviceInstanceNumber, portInNumber, (err, checkInventoryResult) => {
            if (err) {
                return callback(err, checkInventoryResult);
            }

            var connector = elitecoreConnector;
            var numberAlreadyAttached = checkInventoryResult.numberAlreadyAttached;
            var numberAlreadyDamaged = false;
            var result = {flow: checkInventoryResult.flow};

            Promise.resolve()
                .then(() => {
                    if (!numberAlreadyAttached) {
                        if (log) common.log("ElitecorePortInService", "inventory " + portInNumber +
                        " is not attached to any customer");
                        return;
                    }

                    if (log) common.log("ElitecorePortInService", "marking inventory as 'Damaged'...");
                    return connector.getPromise(connector.markInventoryAsDamaged, {
                        inventoryNumber: portInNumber,
                        serviceInstanceNumber: serviceInstanceNumber,
                        validResponses: [{code: 0}]
                    });
                })
                .then((markDamagedResult) => {
                    result.flow.push({description: "mark attached to customer inventory as damaged", result: markDamagedResult});

                    if (!markDamagedResult) {
                        if (log) common.log("ElitecorePortInService", "inventory " + portInNumber +
                        " is NOT attached to any customer, marking as damaged skipped");
                        return;
                    } else if (markDamagedResult.code == 0) {
                        if (log) common.log("ElitecorePortInService", "inventory " + portInNumber +
                        " has been marked as damaged");
                        numberAlreadyDamaged = true;
                    }

                    if (log) common.log("ElitecorePortInService", "removing inventory...");
                    return connector.getPromise(connector.removeInventory, {
                        serviceInstanceNumber: serviceInstanceNumber,
                        inventoryNumber: portInNumber,
                        validResponses: [{code: 0}]
                    });
                })
                .then((removeInventoryResult) => {
                    result.flow.push({description: "remove attached inventory", result: removeInventoryResult});

                    if (!removeInventoryResult) {
                        if (log) common.log("ElitecorePortInService", "inventory " + portInNumber +
                        " is NOT attached to any customer, removing skipped");
                    } else if (removeInventoryResult.code == 0) {
                        if (log) common.log("ElitecorePortInService", "inventory " + portInNumber +
                        " has been removed");
                    }

                    if (numberAlreadyDamaged) {
                        return;
                    }

                    if (log) common.log("ElitecorePortInService", "updating inventory status to 'Sold'(SOLD)...");
                    return connector.getPromise(connector.doInventoryStatusOperation, {
                        inventoryNumber: portInNumber, operationAlias: "SOLD",
                        validResponses: [{code: 1}, {code: -24}]
                    });
                })
                .then((markSoldResult) => {
                    result.flow.push({description: "mark inventory as sold", result: markSoldResult});

                    if (!markSoldResult) {
                        if (log) common.log("ElitecorePortInService", "inventory " + portInNumber +
                        " is already removed and damaged, marking as SOLD is skipped");
                    } else if (markSoldResult.code == 1) {
                        if (log) common.log("ElitecorePortInService", "inventory " + portInNumber +
                        " has been marked as sold");
                    } else if (markSoldResult.code == -24) {
                        if (log) common.log("ElitecorePortInService", "inventory " + portInNumber +
                        " has NOT been marked as sold, ignoring that error");
                    }

                    if (numberAlreadyDamaged) {
                        return;
                    }

                    if (log) common.log("ElitecorePortInService", "updating inventory status to 'Damaged'(DAMAGED)...");
                    return connector.getPromise(connector.doInventoryStatusOperation, {
                        inventoryNumber: portInNumber, operationAlias: "DAMAGED",
                        validResponses: [{code: 1}, {code: -24}]
                    });
                })
                .then((markDamagedResult) => {
                    result.flow.push({description: "mark inventory as damaged", result: markDamagedResult});

                    if (!markDamagedResult) {
                        if (log) common.log("ElitecorePortInService", "inventory " + portInNumber +
                        " is already removed and damaged, marking as DAMAGED is skipped");
                    } else if (markDamagedResult.code == 1) {
                        if (log) common.log("ElitecorePortInService", "inventory " + portInNumber +
                        " has been marked as damaged");
                    } else if (markDamagedResult.code == -24) {
                        if (log) common.log("ElitecorePortInService", "inventory " + portInNumber +
                        " has NOT been marked as damaged");
                    }

                    callback(undefined, result);
                })
                .catch(function (err) {
                    if (err) {
                        common.error("ElitecorePortInService", "failed to deactivate port in number, status=" + err.status
                        + ", message=" + err.message + ", func=" + err.func + ", func=" + JSON.stringify(err.params) +
                        ", raw=" + JSON.stringify(err.raw));

                        if (!result) result = {};
                        result.error = {
                            status: err.status,
                            message: err.message,
                            func: err.func,
                            requestParams: err.params,
                            reponse: err.raw
                        }
                    } else {
                        common.error("ElitecorePortInService", "failed to deactivate port in number, no error provided");
                    }

                    callback(err, result);
                });
        });
    },

    /**
     * Reasons:
     *
     * ERROR_DONOR_TELCO
     * ERROR_ID_MISTMATCH
     * ERROR_DOC_MISTMATCH
     * ERROR_INACTIVE
     * ERROR_SUSPENDED
     * ERROR_OTHER
     * ERROR_PREPAID
     * ERROR_PENDING_ORDER
     * ERROR_INCORRECT_TRANSFER_TIME
     * ERROR_MSISDN_NOT_EXISTS
     *
     */
    loadPortInFailureReason: function (portInNumber, start_ts, callback) {
        if (!callback) callback = () => {
        }

        var query = "SELECT portinnumber, TO_DATE (failure_date) failure_date, reason" +
            " FROM (SELECT systemauditid SID, lookupfield1 portinnumber," +
            " TO_CHAR (auditdate, 'DD-MON-YYYY') failure_date," +
            " CASE" +
            " WHEN LOWER (remark) LIKE '%different_port_domain%' THEN 'ERROR_PREPAID'" +
            " WHEN LOWER (remark) LIKE '%id_mismatch%' THEN 'ERROR_ID_MISTMATCH'" +
            " WHEN LOWER (remark) LIKE '%pending porting order%' THEN 'ERROR_PENDING_ORDER'" +
            " WHEN remark LIKE '%SUSPENDED%' THEN 'ERROR_SUSPENDED'" +
            " WHEN remark LIKE '%ACTIVE%' THEN 'ERROR_INACTIVE'" +
            " WHEN LOWER (remark) LIKE '%transfer%' THEN 'ERROR_INCORRECT_TRANSFER_TIME'" +
            " WHEN LOWER (remark) LIKE '%wrong%' THEN 'ERROR_DONOR_TELCO'" +
            " WHEN LOWER (remark) LIKE '%does not exist%' THEN 'ERROR_MSISDN_NOT_EXISTS'" +
            " ELSE 'ERROR_OTHER'" +
            " END AS reason" +
            " FROM (SELECT * FROM crestelsystemlwp.tbltsystemaudit audit1 WHERE remark LIKE" +
            " 'MNPNOTIFICATION_REQUEST request sent with MNPNotificationRequestVO [MSISDN=%, notificationType=PortIn_%Failed%'" +
            " AND auditdate > TO_TIMESTAMP(" + db.escape(common.getDateTime(start_ts)) + ",'YYYY-MM-DD HH24:MI:SS')) " +
            " WHERE lookupfield1=" + db.escape(portInNumber) + ")";

        var startTime = new Date().getTime();

        // for test purposes replace failure error for specific port_in_numbers
        if (testReasons[portInNumber]) {
            return callback(undefined, {reason: testReasons[portInNumber], notes: "PortIn failure status is taken from test list"});
        }

        db.oracle.query(query, function (err, results) {
            var time = new Date().getTime() - startTime;

            if (err) {
                common.error("ElitecoreUserService", "loadBaseUserInfo: failed to load port in reason from DB, portInNumber="
                + portInNumber + ", time=" + time + ", error=" + err.message);

                // no need to return any error
                // just return geenral error status
                callback(undefined, {
                    reason: "ERROR_OTHER",
                    errorMessage: err.message,
                    time: time
                });
                return;
            }

            var result;
            if (results && results.rows && results.rows[0] && results.rows[0][2]) {
                result = {
                    reason: results.rows[0][2],
                    time: time
                };
            } else {
                result = {
                    reason: "ERROR_OTHER",
                    errorMessage: "Response is empty or invalid (" + JSON.stringify(results) + ")",
                    time: time
                };
            }

            var reason = results.rows && results.rows[0] && results.rows[0][2] ? results.rows[0][2] : "ERROR_OTHER";
            if (log) common.log("ElitecoreUserService", "loadBaseUserInfo: loaded failure reason from db for portInNumber="
            + portInNumber + ", time=" + result.time + ", reason=" + result.reason);

            callback(undefined, result);
        });
    },

    /**
     * Status:
     *
     * DONE
     * FAILED
     * NOT_FOUND
     *
     */
    loadPortInCompleationStatus: function (portInNumber, callback) {
        if (!callback) callback = () => {
        }

        var failureQuery = "select lookupfield1, min(auditdate) auditdate, remark from " +
            " crestelsystemlwp.tbltsystemaudit where lookupfield1 = " + db.escape(portInNumber) +
            " and remark like '%=PortIn_Approval_Failed%' " +
            " group by lookupfield1, remark";

        var successQuery = "select lookupfield1, min(auditdate) auditdate, remark from " +
            " crestelsystemlwp.tbltsystemaudit where lookupfield1 = " + db.escape(portInNumber) +
            " and remark like '%=PortIn_Success%' " +
            " group by lookupfield1, remark";

        var startTime = new Date().getTime();
        db.oracle.query(successQuery, function (err, results) {
            var time = new Date().getTime() - startTime;

            if (err) {
                common.error("ElitecoreUserService", "loadBaseUserInfo: failed to load port in completion status, portInNumber="
                + portInNumber + ", time=" + time + ", error=" + err.message);
                return callback(err);
            }

            if (results && results.rows.length >= 1) {
                return callback(undefined, {status: "DONE"});
            }

            db.oracle.query(failureQuery, function (err, results) {
                var time = new Date().getTime() - startTime;

                if (err) {
                    common.error("ElitecoreUserService", "loadBaseUserInfo: failed to load port in completion status, portInNumber="
                    + portInNumber + ", time=" + time + ", error=" + err.message);
                    return callback(err);
                }

                if (results && results.rows.length >= 1) {
                    return callback(undefined, {status: "FAILED"});
                }

                callback(undefined, {status: "NOT_FOUND"});
            });
        });
    },

    /**
     *
     */
    getDonorCodeByNetworkName: (donor)=> {
        if (!donor) {
            return "";
        }

        var donorCode = networks[donor.toUpperCase()];
        return donorCode;
    },

    getPortInStatus: (portInNumber, status) => {
        const query = `SELECT lookupfield1,
        substr(remark, REGEXP_INSTR (remark, 'notificationType=') + 17, 
        REGEXP_INSTR (remark, 'statusCode')- REGEXP_INSTR (remark, 'notificationType=') -19 ) 
        NotificationType,
        SUBSTR (remark,
        REGEXP_INSTR (remark, 'statusMessage=') + 14,
        REGEXP_INSTR (remark, 'staffName') - REGEXP_INSTR (remark, 'statusMessage=') -16 )
        StatusMessage
        FROM crestelsystemlwp.tbltsystemaudit
        WHERE lookupfield1 = ${db.escape(portInNumber)}
        AND remark LIKE 'MNPNOTIFICATION_REQUEST %'
        AND auditdate =
        (SELECT MAX (auditdate)
        FROM crestelsystemlwp.tbltsystemaudit
        WHERE lookupfield1 = ${db.escape(portInNumber)}
        AND remark LIKE 'MNPNOTIFICATION_REQUEST %')`;

        let oracleQuery = bluebird.promisify(db.oracle.query);

        return oracleQuery(query)
            .then((result) => {                            
                let resultObj = {
                    bssStatus: 'STATUS_NOT_FOUND',
                    localStatus: status
                };
                if (result && result.rows && result.rows.length) {
                    const firstRow = result.rows[0];
                    const bssStatus = firstRow.length && firstRow[1] ? statusMap[firstRow[1]]: 'NO_STATUS';

                    resultObj.bssStatus = bssStatus;
                }           
                return resultObj;
            })
            .catch(err => {
                common.log("Err ECPortInService =>> ", err);
                throw err;
            })
    }
};

/**
 * Function will return error is number exists and attached not to designated service instance
 *
 * @param serviceInstanceNumber
 * @param portInNumber
 * @param callback
 * @returns {*}
 */
function checkInventory(serviceInstanceNumber, portInNumber, callback) {
    if (!callback) callback = function () {
    };

    if (!serviceInstanceNumber || !portInNumber) {
        if (log) common.log("ElitecorePortInService", "portInNumber: mandatory params are missing");
        return callback(new BaseError("Mandatory params missing (serviceInstanceNumber, portInNumber)", "ERROR_INVALID_PARAMS"));
    }

    var connector = elitecoreConnector;
    var numberAlreadyTaken = false;
    var numberAlreadyAttached = false;
    var result = {flow: []};

    Promise.resolve()
        .then(() => {
            if (log) common.log("ElitecorePortInService", "checking if is in in-use ...");
            return connector.getPromise(connector.searchAccount, {
                number: portInNumber,
                validResponses: [
                    {code: -2002205},
                    {code: 0, keys: ["searchAccountResponseVO", "$array0"]}
                ]
            });
        })
        .then((searchAccountResult) => {
            result.flow.push({description: "check if already attached", result: searchAccountResult});

            if (searchAccountResult.code == -2002205) {
                if (log) common.log("ElitecorePortInService", "inventory " + portInNumber +
                " is NOT attached to any customer");
                return;
            } else if (searchAccountResult.code == 0) {
                if (log) common.log("ElitecorePortInService", "inventory " + portInNumber +
                " is attached to some customer");
            }

            if (log) common.log("ElitecorePortInService", "check if the customer already have inventory attached...");
            return connector.getPromise(connector.listInventory, {
                serviceInstanceNumber: serviceInstanceNumber,
                validResponses: [{code: 0, keys: ["inventories"]}]
            });
        })
        .then((listInventoryResult) => {
            result.flow.push({description: "check if attached to designated customer", result: listInventoryResult});

            if (!listInventoryResult) {
                if (log) common.log("ElitecorePortInService", "inventory " + portInNumber +
                " is NOT attached to any customer, inventories loading skipped");
                return;
            } else if (listInventoryResult.code == 0) {
                if (log) common.log("ElitecorePortInService", "loaded list inventories for " +
                serviceInstanceNumber + "(SIN)");
            }

            if (log) common.log("ElitecorePortInService", "checking if number is attached to designated SIN...");
            return new Promise((resolve, reject) => {
                var foundInventory;
                for (var i = 0; i < listInventoryResult.obj.length; i++) {
                    var inventory = listInventoryResult.obj[i];
                    if (inventory.inventoryNumber == portInNumber) {
                        numberAlreadyAttached = true;
                        foundInventory = inventory;
                    }
                }

                if (numberAlreadyAttached) {
                    resolve({code: '0', obj: foundInventory});
                } else {
                    numberAlreadyTaken = true;
                    var err = new Error("Number " + portInNumber + " is taken and attached to another " +
                    "customer (not found under SIN " + serviceInstanceNumber + ")");

                    err.status = "ERROR_ALREADY_IN_USE";
                    reject(err);
                }
            });
        })
        .then((foundInventoryResult) => {
            result.flow.push({description: "load attached inventory", result: foundInventoryResult});
            result.numberAlreadyTaken = numberAlreadyTaken;
            result.numberAlreadyAttached = numberAlreadyAttached;
            result.foundInventoryResult = foundInventoryResult;

            callback(undefined, result)
        })
        .catch(function (err) {
            if (err) {
                common.error("ElitecorePortInService", "failed to load available inventory, status=" + err.status
                + ", message=" + err.message + ", func=" + err.func + ", params=" + JSON.stringify(err.params) +
                ", raw=" + JSON.stringify(err.raw));

                if (!result) result = {};
                result.error = {
                    status: err.status,
                    message: err.message,
                    func: err.func,
                    requestParams: err.params,
                    reponse: err.raw
                }
            } else {
                common.error("ElitecorePortInService", "failed to load available inventory, no error provided");
            }

            callback(err, result);
        });
}
