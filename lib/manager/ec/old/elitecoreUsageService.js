var async = require('async');
var request = require('request');
var common = require('../../common');
var db = require('../../db_handler');
var config = require('../../../config');

var elitecoreConnector = require('./elitecoreConnector');
var elitecoreUserService = require('./elitecoreUserService');

var log = config.EC_LOGSENABLED;
var loadFromDB = !config.EC_STAGING;

var ocsSchema = (config.EC_STAGING) ? "crestelocstestlwp" : "crestelocslwp";
var pmSchema = (config.EC_STAGING) ? "crestelpmtestlwp" : "crestelpmlwp";

module.exports = {

    /**
     *
     */
    loadCurrentCustomerUsage: function (serviceInstanceNumber, callback, fresh, basic) {

        // fresh is not taken into account, potentially we can add
        // small delay for customer data to decrease load on OCS

        elitecoreConnector.loadBssMode(function (err, result) {
            if ((result && result.offline) || (!config.DEV && basic && loadFromDB)) {
                loadBackupUsageInfo(serviceInstanceNumber, callback);
            } else {
                loadBssUsageInfo(serviceInstanceNumber, callback);
            }
        });
    },

    /**
     * TODO change number to serviceInstanceNumber
     */
    loadRemainingDataForPackage: function (number, packageId, callback) {
        loadBackupRemainingDataForPackage(number, packageId, callback);
    }
}

function loadBackupRemainingDataForPackage(number, packageId, callback) {
    if (!callback) callback = () => {
    }

    var packageSuffix = packageId.substr(packageId.length - 3);
    var query = "select UNUSEDVALUE/1024/1024/1024 from " + config.ORACLE.user + ".tblcustomerbalance where customerid in " +
        "(select distinct CUSTOMERID from " + config.ORACLE.user + ".tblcustomer where subscriberidentifier = '" + number + "' and customerstatusid = 1) " +
        " and serviceid =3  and packageid = '" + packageSuffix + "' and trunc(validtodate) > '"
        + common.getMonthStartFromTimestamp(new Date().getTime()) + "'";

    db.oracle.query(query, function (err, result) {
        if (err) {
            callback(err);
        } else if (result.rows && result.rows[0] && result.rows[0][0]) {
            callback(null, result.rows[0][0]);
        } else if (result.rows && result.rows.length == 0) {
            callback(null, 0);
        } else {
            callback({code: -1, message: "no values found in database"});
        }
    });
}

function loadBackupUsageInfo(serviceInstanceNumber, callback) {
    if (!callback) callback = () => {
    }
    if (!serviceInstanceNumber) {
        return callback(new Error("Invalid params"));
    }

    var query = "select e.name PackageName,decode(serviceid, 3, 'DATA', 107, 'SMS_SERVICE_VAS') ServiceAlias, unusedvalue, usedvalue, " +
        "validfromdate, validtodate, b.customerid " +
        "from " + ocsSchema + ".tblcustomerbalance a, " + ocsSchema + ".tblcustomer b, " +
        ocsSchema + ".ab_hierarchy d, " + pmSchema + ".TBLMPRODUCT e " +
        "where a.CUSTOMERID = b.CUSTOMERID and  b.accountid=d.SI_ID and substr(B.SUBSCRIBERIDENTIFIER,1,2)='LW' " +
        "and a.BALANCETYPEID in (2,3) and to_char(a.packageid) = substr(e.PRODUCTID,6,3) " +
        "and d.SI_NUMBER=" + db.escape(serviceInstanceNumber);

    db.oracle.query(query, function (err, result) {
        if (err) {
            return callback(err);
        }

        var balanceData = []
        if (result && result.rows) result.rows.forEach((item) => {
            balanceData.push({
                packageName: item[0],
                serviceAlias: item[1],
                unusedValue: item[2],
                usedValue: item[3],
                validFromDate: item[4] ? new Date(item[4]).getTime() : 0,
                validToDate: item[5] ? new Date(item[5]).getTime() : 0
            });
        });

        var query = "select b.countername, b.externalid, c.name, usedvalue, a.validfromdate, a.validtodate " +
            "from " + ocsSchema + ".tblcustomercounterbalance a, " + pmSchema + ".TBLMCOUNTER b, " + pmSchema + ".tblmproduct c, " +
            ocsSchema + ".ab_hierarchy d, " + ocsSchema + ".tblcustomer e " +
            "where a.customerid = e.CUSTOMERID and substr(productid,6,3) = to_char(a.PACKAGEID) and a.COUNTERID = b.COUNTERID " +
            "and d.SI_ID=e.ACCOUNTID and substr(e.SUBSCRIBERIDENTIFIER,1,2) = 'LW' and d.SI_NUMBER=" + db.escape(serviceInstanceNumber);

        db.oracle.query(query, function (err, result) {
            if (err) {
                return callback(err);
            }

            var counterBalanceData = []
            if (result && result.rows) result.rows.forEach((item) => {
                counterBalanceData.push({
                    counterName: item[0],
                    externalId: item[1],
                    usedValue: item[3],
                    validFromDate: item[4] ? new Date(item[4]).getTime() : 0,
                    validToDate: item[5] ? new Date(item[5]).getTime() : 0
                });
            });

            callback(undefined, {
                balanceData: balanceData,
                counterBalanceData: counterBalanceData
            });
        });
    });
}

function loadBssUsageInfo(serviceInstanceNumber, callback) {
    if (!callback) callback = () => {
    }

    if (!serviceInstanceNumber) {
        return callback(new Error("Invalid params"));
    }

    var now = new Date();
    elitecoreConnector.viewCustomer(serviceInstanceNumber, now.getTime(), (err, result) => {
        if (err) {
            return callback(err);
        }
        if (!result || !result.return || !result.return.responseObject) {
            return callback(new Error("Invalid 'viewCustomer' BSS response for SIN " + serviceInstanceNumber));
        }

        var viewResponse = result.return.responseObject;
        var balanceData = viewResponse.balanceData ? viewResponse.balanceData : {};
        var counterBalanceData = viewResponse.counterBalanceData ? viewResponse.counterBalanceData : {};

        callback(undefined, {
            balanceData: balanceData,
            counterBalanceData: counterBalanceData
        })
    });
}
