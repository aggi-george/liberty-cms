var async = require('async');
var request = require('request');
var common = require('../../common');
var db = require('../../db_handler');
var config = require('../../../config');

var elitecoreConnector = require('./elitecoreConnector');
var elitecoreUtils = require('./elitecoreUtils');
var elitecorePackagesService = require('./elitecorePackagesService');

var log = config.EC_LOGSENABLED;
var loadFromDB = !config.EC_STAGING;
var cacheTime = 30 * 60 * 1000;

var prefixCustomerAccountNumber = "can_";
var prefixBillingAccountNumber = "ban_";
var prefixServiceAccountNumber = "san_";
var prefixServiceInstanceNumber = "sin_";
var prefixPhoneNumber = "pn_";
var prefixUserKey = "uk_";

module.exports = {

    /**
     *
     */
    clearUserInfo: function (accountNumber, callback) {
        db.cache_del("account_cache_micro", accountNumber);
        db.cache_del("account_cache", accountNumber);
        if (callback) callback();
    },

    /**
     *
     */
    updateUserInfo: function (accountNumber, callback, userKey) {
        loadCustomerDetails(accountNumber, function (err, customerInfo) {
            if (err) {
                if (callback) callback(err);
                return;
            }

            if (!customerInfo) {
                if (callback) callback(new Error("Failed to init customer info"));
                return;
            }

            var accountNumber = customerInfo.customerAccountNumber;
            db.cache_put("account_cache", accountNumber, customerInfo, cacheTime);
            db.cache_put("account_cache", prefixCustomerAccountNumber + customerInfo.customerAccountNumber, accountNumber, cacheTime);
            db.cache_put("account_cache", prefixBillingAccountNumber + customerInfo.billingAccountNumber, accountNumber, cacheTime);
            db.cache_put("account_cache", prefixServiceAccountNumber + customerInfo.serviceAccountNumber, accountNumber, cacheTime);
            db.cache_put("account_cache", prefixServiceInstanceNumber + customerInfo.serviceInstanceNumber, accountNumber, cacheTime);
            customerInfo.serviceInstanceInventories.forEach(function (item) {
                db.cache_put("account_cache", prefixPhoneNumber + item.number, accountNumber, cacheTime);
            });

            if (userKey) db.cache_put("account_cache", prefixUserKey + userKey, accountNumber, cacheTime);
            if (callback) callback(undefined, customerInfo);
        });
    },

    /**
     *
     */
    loadUserInfo: function (keyType, key, callback, fresh) {
        if (!key) {
            if (callback) callback(new Error("Key is empty, keyType=" + keyType));
            return;
        }

        var reloadCache = function (err, accountNumber) {
            if (err) {
                if (callback) callback(err);
                return;
            }

            module.exports.updateUserInfo(accountNumber, function (err, customerInfo) {
                if (callback) callback(err, customerInfo);
                return;
            }, keyType === prefixUserKey ? key : undefined);
        }

        var loadFresh = function () {
            if (keyType === prefixCustomerAccountNumber) {
                reloadCache(undefined, key);
            } else if (keyType === prefixBillingAccountNumber) {
                loadAccountNumberByBillingAccountNumber(key, reloadCache);
            } else if (keyType === prefixServiceAccountNumber) {
                if (callback) callback(new Error("Caching by service instance number is not supported"));
            } else if (keyType === prefixServiceInstanceNumber) {
                loadAccountNumberByServiceInstanceNumber(key, reloadCache);
            } else if (keyType === prefixPhoneNumber) {
                loadAccountNumberByPhoneNumber(key, reloadCache);
            } else if (keyType === prefixUserKey) {
                loadAccountNumberByUserKey(key, (err, number) => {
                    if (err) return reloadCache(err);
                    loadAccountNumberByPhoneNumber(number, reloadCache);
                });
            } else {
                if (callback) callback(new Error("Cache key type is not supported"));
                return;
            }
        };

        if (fresh) return loadFresh();
        db.cache_get("account_cache", keyType + key, function (accountNumber) {
            if (!accountNumber) return loadFresh();
            db.cache_get("account_cache", accountNumber, function (value) {
                if (!value) return loadFresh();
                callback(undefined, value);
            });
        });
    },

    /**
     *
     */
    loadUserInfoByInternalId: function (id, callback, fresh, basic) {
        if (!callback) callback = function () {
        };

        elitecoreConnector.loadBssMode(function (err, result) {
            if ((result && result.offline) || (!config.DEV && basic && loadFromDB)) {
                loadBaseUserInfo(prefixServiceInstanceNumber, id, function (err, cache) {
                    if ((err && err.status != "NOT_FOUND") || cache) return callback(err, cache);
                    loadBaseUserInfo(prefixBillingAccountNumber, id, function (err, cache) {
                        if ((err && err.status != "NOT_FOUND") || cache) return callback(err, cache);
                        loadBaseUserInfo(prefixCustomerAccountNumber, id, function (err, cache) {
                            callback(err, cache);
                        }, fresh);
                    }, fresh);
                }, fresh);
            } else {
                module.exports.loadUserInfo(prefixServiceInstanceNumber, id, function (err, cache) {
                    if ((err && err.status != "NOT_FOUND") || cache) return callback(err, cache);
                    module.exports.loadUserInfo(prefixBillingAccountNumber, id, function (err, cache) {
                        if ((err && err.status != "NOT_FOUND") || cache) return callback(err, cache);
                        module.exports.loadUserInfo(prefixCustomerAccountNumber, id, function (err, cache) {
                            callback(err, cache);
                        }, fresh);
                    }, fresh);
                }, fresh);
            }
        });
    },

    /**
     *
     */
    loadUserInfoByCustomerAccountNumber: function (can, callback, fresh, basic) {
        elitecoreConnector.loadBssMode(function (err, result) {
            if ((result && result.offline) || (!config.DEV && basic && loadFromDB)) {
                loadBaseUserInfo(prefixCustomerAccountNumber, can, callback, fresh);
            } else {
                module.exports.loadUserInfo(prefixCustomerAccountNumber, can, callback, fresh);
            }
        });
    },

    /**
     *
     */
    loadUserInfoByBillingAccountNumber: function (ban, callback, fresh, basic) {
        elitecoreConnector.loadBssMode(function (err, result) {
            if ((result && result.offline) || (!config.DEV && basic && loadFromDB)) {
                loadBaseUserInfo(prefixBillingAccountNumber, ban, callback, fresh);
            } else {
                module.exports.loadUserInfo(prefixBillingAccountNumber, ban, callback, fresh);
            }
        });
    },

    /**
     *
     */
    loadUserInfoByServiceAccountNumber: function (san, callback, fresh, basic) {
        if (callback) callback(new Error("Caching by service account number is not supported"));
    },

    /**
     *
     */
    loadUserInfoByServiceInstanceNumber: function (sin, callback, fresh, basic) {
        elitecoreConnector.loadBssMode(function (err, result) {
            if ((result && result.offline) || (!config.DEV && basic && loadFromDB)) {
                loadBaseUserInfo(prefixServiceInstanceNumber, sin, callback, fresh);
            } else {
                module.exports.loadUserInfo(prefixServiceInstanceNumber, sin, callback, fresh);
            }
        });
    },

    /**
     *
     */
    loadUserInfoByPhoneNumber: function (number, callback, fresh, basic) {
        elitecoreConnector.loadBssMode(function (err, result) {
            if ((result && result.offline) || (!config.DEV && basic && loadFromDB)) {
                loadBaseUserInfo(prefixPhoneNumber, number, callback, fresh);
            } else {
                module.exports.loadUserInfo(prefixPhoneNumber, number, callback, fresh);
            }
        });
    },

    /**
     *
     */
    loadUserInfoByUserKey: function (userKey, callback, fresh, basic) {
        elitecoreConnector.loadBssMode(function (err, result) {
            if ((result && result.offline) || (!config.DEV && basic && loadFromDB)) {
                loadAccountNumberByUserKey(userKey, (err, number) => {
                    if (err) return callback(err);
                    loadBaseUserInfo(prefixPhoneNumber, number, callback, fresh);
                });
            } else {
                module.exports.loadUserInfo(prefixUserKey, userKey, callback, fresh);
            }
        });
    },

    /**
     *
     */
    loadAllUsersBaseInfo: function (options, callback) {
        loadBaseAllWithoutCaching(options, callback);
    },

    /**
     *
     */
    loadActivePackages: function (serviceInstance, callback) {
        loadActivePackages(serviceInstance, callback);
    },

    /**
     *
     */
    loadCustomerWithPackage: function (serviceInstance, callback) {
        loadCustomerWithPackage(serviceInstance, callback);
    },

    /**
     *
     */
    updateCustomerID: function (accountNumber, type, id, callback) {
        var code = getIDCodeByType(type);
        if (!code) {
            if (callback) callback(new Error("Invalid id type"))
            return;
        }

        elitecoreConnector.updateCustomerAccountData({
                accountNumber: accountNumber,
                accountProfiles: {strcustom1: code, strcustom2: id}
            },
            function (err, result) {
                module.exports.clearUserInfo(accountNumber);
                if (callback) callback(err, result);
            });
    },

    /**
     *
     */
    updateOrderReferenceNumber: function (accountNumber, orn, callback) {
        var code = getIDCodeByType(type);
        if (!code) {
            if (callback) callback(new Error("Invalid id type"))
            return;
        }

        elitecoreConnector.updateServiceAccountData({
                accountProfiles: {strcustom7: orn}
            },
            function (err, result) {
                module.exports.clearUserInfo(accountNumber);
                if (callback) callback(err, result);
            });
    },

    /**
     *
     */
    updateActivationDate: function (accountNumber, datestring, callback) {
        if (!accountNumber || !datestring || !Date.parse(datestring)) {
            if (callback) callback(new Error("Incomplete parameters"))
            return;
        }

        elitecoreConnector.updateServiceAccount({
                accountNumber: accountNumber,
                accountProfiles: {datecustom5: datestring}
            },
            function (err, result) {
                module.exports.clearUserInfo(accountNumber);
                if (callback) callback(err, result);
            });
    },

    /**
     *
     */
    updateCustomerDOB: function (accountNumber, birthday, callback) {

        elitecoreConnector.updateCustomerAccountData({
                accountNumber: accountNumber,
                accountProfiles: {"datecustom1": birthday + "T12:00:00.000Z"}
            },
            function (err, result) {
                module.exports.clearUserInfo(accountNumber);
                if (callback) callback(err, result);
            });
    },

    /**
     *
     */
    updateAllCustomerAccountsDetails: function (accountNumber, params, callback, skipAccount, skipBilling, skipService) {
        if (!params) {
            if (callback) callback(new Error("Invalid name"))
            return;
        }

        if (params.name) {
            params.name = params.name.toUpperCase();
        }
        if (params.email) {
            params.email = params.email.toLowerCase();
        }

        var result = {};

        var updateService = (cache) => {
            if (!skipService) {
                var ecParams = {accountNumber: cache.serviceAccountNumber};
                if (params.name) ecParams.firstName = params.name;
                if (params.email) ecParams.contactDetails = {emailId: params.email};

                elitecoreConnector.updateServiceAccount(ecParams, function (err, resultServiceAccountUpdate) {
                    result.resultServiceAccountError = err ? err.message : undefined;
                    result.resultServiceAccountUpdate = resultServiceAccountUpdate;
                    if (err) {
                        if (callback) callback(err, result);
                        return;
                    }

                    module.exports.clearUserInfo(accountNumber);
                    if (callback) callback(err, result);
                });
            } else {
                if (callback) callback(undefined, result);
            }
        }

        var updateBilling = (cache) => {
            if (!skipBilling) {
                var ecParams = {accountNumber: cache.billingAccountNumber};
                if (params.name) ecParams.firstName = params.name
                if (params.email) ecParams.contactDetails = {emailId: params.email};

                elitecoreConnector.updateBillingAccountData(ecParams, function (err, resultBillingAccountUpdate) {
                    result.resultBillingAccountError = err ? err.message : undefined;
                    result.resultBillingAccountUpdate = resultBillingAccountUpdate;
                    if (err) {
                        //    if (callback) callback(err, result);
                        //    return;
                    }

                    module.exports.clearUserInfo(accountNumber);
                    updateService(cache);
                });
            } else {
                updateService(cache);
            }
        }

        var updateCustomer = (cache) => {
            if (!skipAccount) {
                var ecParams = {accountNumber: accountNumber};
                if (params.name) ecParams.firstName = params.name
                if (params.email) ecParams.contactDetails = {emailId: params.email};

                elitecoreConnector.updateCustomerAccountData(ecParams, function (err, resultAccountUpdate) {
                    result.resultAccountError = err ? err.message : undefined;
                    result.resultAccountUpdate = resultAccountUpdate;
                    if (err) {
                        if (callback) callback(err, result);
                        return;
                    }

                    module.exports.clearUserInfo(accountNumber);
                    updateBilling(cache);
                });
            } else {
                updateBilling(cache);
            }
        }

        module.exports.loadUserInfoByCustomerAccountNumber(accountNumber, (err, cache) => {
            if (err) {
                if (callback) callback(err);
                return;
            }

            if (!cache) {
                if (callback) callback(new Error("Customer is not found"));
                return;
            }

            updateCustomer(cache);
        }, false, false)
    },

    /**
     *
     */
    updateCustomerOrderReferenceNumber: function (accountNumber, orderReferenceNumber, callback) {
        module.exports.loadUserInfoByCustomerAccountNumber(accountNumber, (err, cache) => {
            if (err) {
                if (callback) callback(err);
                return;
            }

            if (!cache) {
                if (callback) callback(new Error("Customer is not found"));
                return;
            }

            elitecoreConnector.updateServiceAccount({
                    accountNumber: cache.serviceAccountNumber,
                    accountProfiles: {
                        strcustom7: orderReferenceNumber
                    }
                },
                function (err, result) {
                    module.exports.clearUserInfo(accountNumber);
                    if (callback) callback(err, result);
                });
        }, false, false);
    },

    /**
     *
     */
    getIDTypeByCode: function (code) {
        return getIDTypeByCode(code);
    }
}

function loadBaseUserInfo(keyType, key, callback, fresh) {
    if (!key) {
        if (callback) callback(new Error("Key is empty, keyType=" + keyType));
        return;
    }

    var loadFresh = function () {
        updateBaseUserInfoCache(keyType, key, function (err, value) {
            if (callback) callback(err, value);
        });
    };

    if (fresh) {
        return loadFresh();
    }

    db.cache_get("account_cache_micro", keyType + key, function (accountNumber) {
        if (!accountNumber) return loadFresh();
        db.cache_get("account_cache_micro", accountNumber, function (value) {
            if (!value) return loadFresh();
            if (callback) callback(undefined, value);
        });
    });
}

function joinBaseUserInfoResult(existingCustomer, addCustomerInfo) {
    if (!existingCustomer) return addCustomerInfo;
    if (!addCustomerInfo) return existingCustomer;

    var inventory1 = existingCustomer.serviceInstanceInventoryPrimary;
    var inventory2 = addCustomerInfo.serviceInstanceInventoryPrimary;

    // existing or CirclesLife number gets higher priority

    if (inventory1 && !inventory2) {
        existingCustomer.serviceInstanceInventoryPrimary = inventory1;
    } else if (!inventory1 && inventory2) {
        existingCustomer.serviceInstanceInventoryPrimary = inventory2;
    } else if (inventory1 && inventory1.number
        && (inventory1.number.indexOf("874") >= 0 || inventory1.number.indexOf("808") >= 0)) {
        existingCustomer.serviceInstanceInventoryPrimary = inventory1;
    } else if (inventory2 && inventory2.number
        && (inventory2.number.indexOf("874") >= 0 || inventory2.number.indexOf("808") >= 0)) {
        existingCustomer.serviceInstanceInventoryPrimary = inventory2;
    }

    if (existingCustomer.serviceInstanceInventoryPrimary) {
        existingCustomer.number = existingCustomer.serviceInstanceInventoryPrimary.number;
    }

    var allNumbers = [];
    existingCustomer.serviceInstanceInventories.forEach((inventory) => {
        allNumbers.push(inventory.number);
    });

    addCustomerInfo.serviceInstanceInventories.forEach((inventory) => {
        if (allNumbers.indexOf(inventory.number) == -1) {
            existingCustomer.serviceInstanceInventories.push(inventory);
        }
    });

    return existingCustomer;
}

function parseBaseUserInfoResult(result) {
    var orderReferenceNumber = result[17];
    var siCreationDate = common.safeDate(result[34]);
    var siPackChangeDate = common.safeDate(result[18]);

    // datecustom5 -> custom date that is saved on account activation first time
    var activationDate = common.safeDate(result[19]);

    // datecustom6 -> custom check out date that is passed from eComm and saved on account creation
    var checkoutDate = common.safeDate(result[35]);

    var siBasePlanName = result[36];

    var billingAddress = {
        addressOne: result[20],
        addressTwo: result[21],
        city: result[22],
        state: result[23],
        country: result[24],
        postcode: result[25]
    };

    var billingCreditCard = {
        token: result[26],
        tokenShort: result[27],
        bank: result[28],
        type: result[29],
        last4Digits: result[30]
    }

    var billingCycleName = result[33];
    var billingCycleDay = billingCycleName ? billingCycleName.split('_')[1] : 1;
    var billingCycleStartDay = billingCycleDay ? parseInt(billingCycleDay) : 1;

    var currentDate = new Date();
    var siEarTermDate;

    var planActivationCycle = elitecoreUtils.calculateBillCycle(billingCycleStartDay, undefined, siPackChangeDate);
    if (planActivationCycle.endDateUTC.getTime() > currentDate.getTime()) {
        var nextCycle = elitecoreUtils.calculateBillCycle(billingCycleStartDay, undefined, planActivationCycle.endDateUTC);
        siEarTermDate = nextCycle.endDateUTC;
    } else {
        var currentCycle = elitecoreUtils.calculateBillCycle(billingCycleStartDay, undefined, new Date());
        siEarTermDate = currentCycle.endDateUTC;
    }

    var customerIdentity = {
        type: result[31],
        number: result[32],
        typeFormatted: getIDTypeByCode(result[31])
    };

    var validLength = customerIdentity.number && customerIdentity.number.length == 9;
    var digitsString = validLength ? customerIdentity.number.substring(1, 8) : null;
    customerIdentity.validForVerification = validLength && /^\d+$/.test(digitsString);
    customerIdentity.digits = customerIdentity.validForVerification ? digitsString : null;

    var customerBirthday = common.safeDate(result[7]);
    var attachedNumber = result[16] == 'Active';
    var customerPortedStatus = result[9];
    var customerPorted = customerPortedStatus == 'MNP';
    var siCustomerStatus = result[10] == 'Active' ? 'Active' : "Suspended";
    var iccid = result[13];
    var imsi = result[14];
    var referralCode = result[15];

    // CST20 internal CMS status

    var inventory = {
        number: result[1],
        status: attachedNumber ? "CST01" : "CST20",
        active: attachedNumber,
        statusNumber: attachedNumber ? 1 : 20,
        unpaired: !iccid || !imsi,
        iccid: iccid,
        imsi: imsi
    }

    var inventories = [];
    if (result[1]) {
        inventories.push(inventory);
    }

    return {
        prefix: "65",
        number: attachedNumber ? result[1] : undefined,
        iccid: iccid,
        imsi: imsi,
        first_name: result[0],
        email: result[6],
        account: result[3],
        ported: customerPorted,
        birthday: customerBirthday ? common.getDate(new Date(common.msOffsetSG(customerBirthday.getTime()))) : undefined,
        billing_cycle: billingCycleStartDay,
        status: siCustomerStatus,
        base: {},
        extra_current: [],
        extra: [],
        general: [],
        general_future: [],
        boost: [],
        bonus: [],
        bonus_future: [],
        invalid: {addons: {current: [], future: []}},
        roaming: {},
        recurring: 0,

        customerAccountFullName: result[0],
        customerAccountNumber: result[3],
        customerAccountEmail: result[6],
        customerBirthday: customerBirthday,
        customerPorted: customerPorted,
        customerPortedStatus: customerPortedStatus,
        customerAccountIdCode: customerIdentity.type,
        customerAccountIdType: getIDTypeByCode(customerIdentity.type),
        customerAccountIdNumber: customerIdentity.number,
        billingAccountNumber: result[4],
        billingAccountEmail: result[6],
        billingFullName: result[0],
        billingCycleStartDay: billingCycleStartDay,
        billingCycleName: billingCycleName,
        billingAddress: billingAddress,
        billingCreditCard: billingCreditCard,
        serviceAccountNumber: result[5],
        serviceAccountEmail: result[6],
        serviceInstanceNumber: result[2],
        serviceInstanceCreationDate: siCreationDate,
        serviceInstancePackageChangeDate: siPackChangeDate,
        serviceInstanceEarliestTerminationDate: siEarTermDate,
        serviceInstanceCustomerStatus: siCustomerStatus,
        serviceInstanceInventoryPrimary: attachedNumber ? inventory : undefined,
        serviceInstanceBasePlanName: siBasePlanName,
        serviceInstanceInventories: inventories,
        customerIdentity: customerIdentity,
        referralCode: referralCode,
        orderReferenceNumber: orderReferenceNumber,
        checkoutDate: checkoutDate,
        activationDate: activationDate,
        backupData: true
    }
}

function getCustomerReplicaDBView() {
    return "CRESTELOCSLWP.CUSTOMER_DETAIL_MV";
    //return "CRESTELOCSLWP.customer_dtl_test_vw";
}

function getCustomerReplicaDBFields() {
    return "customername, subscriberidentifier, serviceinstanceaccount, customeraccount, billingaccount, " +
        "serviceaccount, emailid, birthday, numberstatus, portinstatus, accountstatus, available, usedvalue, sim, imsi, referralcode, " +
        "currentstatus, ORDERREFNO, ACTIVATIONDATE, JOINDATE, ADDRESS1, ADDRESS2, CITY, STATE, COUNTRY, POSTALCODE, CCTOKEN, " +
        "CCTOKENSHORT, CCBANK, CCTYPE, CCLAST4DIGIT, IDTYPE, IDNUMBER, BILLCYCLENAME, CREATIONDATE, CHECKOUTDATE, PACKAGENAME";
}

function updateBaseUserInfoCache(keyType, key, callback) {
    if (!callback) callback = () => {
    }

    if (!key) {
        return callback(new Error("Key is empty, keyType=" + keyType));
    }

    var saveCache = function (customerInfo) {
        var accountNumber = customerInfo.customerAccountNumber;
        db.cache_put("account_cache_micro", accountNumber, customerInfo, cacheTime);
        db.cache_put("account_cache_micro", prefixCustomerAccountNumber + customerInfo.customerAccountNumber, accountNumber, cacheTime);
        db.cache_put("account_cache_micro", prefixBillingAccountNumber + customerInfo.billingAccountNumber, accountNumber, cacheTime);
        db.cache_put("account_cache_micro", prefixServiceAccountNumber + customerInfo.serviceAccountNumber, accountNumber, cacheTime);
        db.cache_put("account_cache_micro", prefixServiceInstanceNumber + customerInfo.serviceInstanceNumber, accountNumber, cacheTime);
        customerInfo.serviceInstanceInventories.forEach(function (item) {
            db.cache_put("account_cache_micro", prefixPhoneNumber + item.number, accountNumber, cacheTime);
        });
    }

    var query = "SELECT " + getCustomerReplicaDBFields() + " FROM " + getCustomerReplicaDBView();

    if (keyType === prefixPhoneNumber) {
        query += " WHERE subscriberidentifier=" + db.escape(key);
    } else if (keyType === prefixCustomerAccountNumber) {
        query += " WHERE customeraccount=" + db.escape(key);
    } else if (keyType === prefixBillingAccountNumber) {
        query += " WHERE billingaccount=" + db.escape(key);
    } else if (keyType === prefixServiceAccountNumber) {
        query += " WHERE serviceaccount=" + db.escape(key);
    } else if (keyType === prefixServiceInstanceNumber) {
        query += " WHERE serviceinstanceaccount=" + db.escape(key);
    }

    var startTime = new Date().getTime();
    db.oracle.query(query, function (err, results) {
        var time = new Date().getTime() - startTime;

        if (err) {
            common.error("ElitecoreUserService", "loadBaseUserInfo: failed to load account data from DB, key="
            + key + ", time=" + time + ", error=" + err.message);
            return callback(err);
        }

        var exists = results && results.rows && results.rows[0] ? true : false;
        var count = results && results.rows ? results.rows.length : -1;
        if (log) common.log("ElitecoreUserService", "loadBaseUserInfo: loaded data from db for key="
        + key + ", time=" + time + ", hasData=" + exists + ", count found=" + count);

        if (!exists) {
            return callback();
        }

        var candidate = results.rows[0];
        var candidateID = 0;
        var candidateHasNumber = false;

        results.rows.forEach((item) => {
            var accountNumber = item[3];
            var currentHasNumber = item[16] == 'Active';
            var currentID = accountNumber ? parseInt(accountNumber.replace('LW', '')) : 0;

            if (currentID > candidateID
                || (currentID == candidateID && currentHasNumber && !candidateHasNumber)) {

                candidate = item;
                candidateID = currentID;
                candidateHasNumber = currentHasNumber;
            }
        });

        var customerInfo = parseBaseUserInfoResult(candidate);

        // in some cases we can find terminated customer by their old, removed number
        // so in case of such a search, by phone number, we have to check for number presence
        // and return nothing if it does not exists

        if (keyType === prefixPhoneNumber && !customerInfo.number) {
            customerInfo = undefined;
        }
        if (!customerInfo) {
            return callback();
        }
        if (!customerInfo.serviceInstanceNumber) {
            saveCache(customerInfo);
            return callback(undefined, customerInfo);
        }

        var tasks = [];
        tasks.push(function (callback) {
            var sin = customerInfo.serviceInstanceNumber;
            loadActivePackages(customerInfo.serviceInstanceNumber, (err, result) => {
                if (err) return callback(err);
                callback(undefined, {addOnPlan: result});
            });
        });
        tasks.push(function (callback) {
            var sin = customerInfo.serviceInstanceNumber;
            callback(undefined, {balanceData: {}});
        });
        tasks.push(function (callback) {
            elitecorePackagesService.loadHierarchy(function (err, hierarchy) {
                if (err) return callback(err);
                callback(undefined, {hierarchy: hierarchy});
            });
        });

        async.parallel(tasks, function (err, resultsList) {
            if (err) {
                common.error("ElitecoreUserService", "updateBaseUserInfoCache: error=" + err.message);
                return callback(err);
            }

            var result = {};
            resultsList.forEach(function (item) {
                if (item.addOnPlan) result.addOnPlan = item.addOnPlan;
                if (item.hierarchy) result.hierarchy = item.hierarchy;
                if (item.balanceData) result.balanceData = item.balanceData;
            });

            // need to check if isBasePlan has base_id field

            var siBasePlanName = customerInfo.serviceInstanceBasePlanName;
            var siBasePlan;
            if (result.hierarchy.base) result.hierarchy.base.forEach((item) => {
                if (item.options) item.options.forEach((item) => {
                    if (item.name == customerInfo.serviceInstanceBasePlanName) siBasePlan = item;
                })
            });

            if (!siBasePlan) {
                common.error("ElitecoreUserService", "updateBaseUserInfoCache: Base plan " + siBasePlanName +
                " is not found, sin=" + customerInfo.serviceInstanceNumber);
                return callback(new Error("Base plan " + siBasePlanName + " is not found"));
            }

            customerInfo.base = siBasePlan;
            customerInfo.serviceInstanceBasePlan = siBasePlan;

            var nowMs = new Date().getTime();
            var billCycle = elitecoreUtils.calculateBillCycle(customerInfo.billingCycleStartDay);

            var siAddonsCurrent = {
                extra: {},
                boost: [],
                bonus: [],
                general: [],
                invalid: []
            }
            var siAddonsFuture = {
                extra: {},
                boost: [],
                bonus: [],
                general: [],
                invalid: []
            }
            var siAddonsExpired = [];
            var siRoamingInfo = {
                limit: 100,
                addon: undefined
            };
            var siUnlimitedData = {
                enabled: false,
                addon: undefined
            };
            var addonBSSSubscribed = {};
            if (result.addOnPlan) {
                result.addOnPlan.forEach(function (item) {
                    addPackage(item, true, result.hierarchy, result.balanceData, siBasePlanName, billCycle,
                        siAddonsCurrent, siAddonsFuture, siAddonsExpired, addonBSSSubscribed, siRoamingInfo, siUnlimitedData, nowMs);
                });
            }

            customerInfo.extra_current = siAddonsCurrent.extra;
            customerInfo.extra = siAddonsFuture.extra;
            customerInfo.general = siAddonsCurrent.general;
            customerInfo.general_future = siAddonsFuture.general;
            customerInfo.boost = siAddonsCurrent.boost;
            customerInfo.bonus = siAddonsCurrent.bonus;
            customerInfo.bonus_future = siAddonsFuture.bonus;
            customerInfo.invalid = {addons: {current: siAddonsCurrent.invalid, future: siAddonsFuture.invalid}};
            customerInfo.roaming = siRoamingInfo;

            customerInfo.serviceInstanceCurrentAddons = siAddonsCurrent;
            customerInfo.serviceInstanceFutureAddons = siAddonsFuture;
            customerInfo.serviceInstanceRoaming = siRoamingInfo;
            customerInfo.serviceInstanceUnlimitedData = siUnlimitedData;

            var nextMonthPlanPrice = siBasePlan.price;
            nextMonthPlanPrice += getComponentsPrice(siAddonsFuture, "data");
            nextMonthPlanPrice += getComponentsPrice(siAddonsFuture, "voice");
            nextMonthPlanPrice += getComponentsPrice(siAddonsFuture, "sms");
            siAddonsFuture.general.forEach(function (item) {
                nextMonthPlanPrice += computeGeneralAddonPrice(item);
            });

            customerInfo.recurring = nextMonthPlanPrice;
            customerInfo.nextMonthPlanPrice = nextMonthPlanPrice;

            saveCache(customerInfo);
            return callback(undefined, customerInfo);
        });
    });
}

function computeGeneralAddonPrice (addon) {
    var nextCycle = elitecoreUtils.calculateBillCycle(undefined, undefined, elitecoreUtils.calculateBillCycle().endDateUTC);
    var nextCycleEndDate = nextCycle.endDateUTC.getTime();
    var nextMonthPlanPrice = 0;
    // this is to make sure that addons which are subscribed
    // for 2 months down the lane are not taken into account
    // while computing the nextCycle plan price
    var addonSubscriptionStartMonth = new Date(addon.billStartDate).getTime();
    if (addonSubscriptionStartMonth >= nextCycleEndDate) {
        return nextMonthPlanPrice;
    } else {
        return nextMonthPlanPrice = addon.recurrent ? addon.price : 0;
    }
}

function loadBaseAllWithoutCaching(options, callback) {
    var query = "SELECT " + getCustomerReplicaDBFields() + " FROM " + getCustomerReplicaDBView();

    if (options.serviceInstances) {
        var serviceInstanceIds = "";
        options.serviceInstances.forEach((item) => {
            if (serviceInstanceIds) serviceInstanceIds += ",";
            serviceInstanceIds += db.escape(item);
        });
        query += " WHERE serviceinstanceaccount IN (" + serviceInstanceIds + ")";
    }

    var startTime = new Date().getTime();
    db.oracle.query(query, function (err, results) {
        var time = new Date().getTime() - startTime;

        if (err) {
            common.error("ElitecoreUserService", "loadBaseAllWithoutCaching: failed to load account data " +
            "from DB, time=" + time + ", error=" + err.message);
            if (callback) callback(err);
            return;
        }

        var rows;
        if (results.rows) {
            rows = results.rows;
        } else {
            rows = results;
        }

        var customers = [];
        var customersMap = {};

        if (rows) {
            if (rows.forEach) {
                if (rows.length > 5000) common.log("ElitecoreUserService", "loadBaseAllWithoutCaching: loaded data " +
                "from DB, time=" + time + ", count=" + rows.length);

                rows.forEach((item) => {

                    var customer = parseBaseUserInfoResult(item);
                    var existingCustomer = customersMap[customer.customerAccountNumber];

                    if (existingCustomer) {
                        joinBaseUserInfoResult(existingCustomer, customer);
                    } else {
                        customersMap[customer.customerAccountNumber] = customer;
                        customers.push(customer);
                    }
                });
            } else {
                common.error("ElitecoreUserService", "loadBaseAllWithoutCaching: result is not an array="
                + JSON.stringify(rows));
            }
        }

        if (callback) {
            callback(undefined, customers);
        }
    });
}

function loadActivePackages(serviceInstance, callback) {
    var query = "SELECT c.name, si_number SERVICEINSTANCEACCOUNT, " +
    " a.fromdate, a.todate, a.packageid PackageID, A.CUSTOMERPACKAGEHISTORYID " +
    " FROM crestelcaamlwp.tbltcustomerpackagehistory a, crestelpmlwp.tblmproduct c, crestelocslwp.ab_hierarchy d" +
    " WHERE a.PACKAGEID = c.PRODUCTID  AND TRUNC (a.todate) >= LAST_DAY (TRUNC (SYSDATE, 'MM'))" +
    " AND d.SI_ID = a.ACCOUNTID AND si_number = " + db.escape(serviceInstance) + " ORDER BY si_number";

    var startTime = new Date().getTime();
    db.oracle.query(query, function (err, results) {
        var time = new Date().getTime() - startTime;

        if (err) {
            common.error("ElitecoreUserService", "loadActivePackages: failed to load active addons " +
            "from DB, time=" + time + ", error=" + err.message);
            if (callback) callback(err);
            return;
        }

        var rows;
        if (results.rows) {
            rows = results.rows;
        } else {
            rows = results;
        }

        var searchInstancePackages = [];
        if (rows) {
            if (rows.forEach) {
                if (rows.length > 5000) common.log("ElitecoreUserService", "loadActivePackages: loaded active addons " +
                "from DB, time=" + time + ", count=" + rows.length);

                var lastCustomerInit = false;
                var lastCustomer = {
                    serviceInstanceNumber: "",
                    packages: []
                };

                rows.forEach((item) => {
                    var packageItem = {
                        id: item[4],
                        packageHistoryId: item[5],
                        packageName: item[0],
                        name: item[0],
                        serviceInstanceNumber: item[1],
                        startDate: item[2] ? new Date(item[2]) : undefined,
                        endDate: item[3] ? new Date(item[3]) : undefined,
                        packageActivationDate: item[2] ? new Date(item[2]) : undefined,
                        expiryDate: item[3] ? new Date(item[3]) : undefined
                    }
                    searchInstancePackages.push(packageItem);
                });
            } else {
                common.error("ElitecoreUserService", "loadActivePackages: result is not an array="
                + JSON.stringify(rows));
            }
        }

        if (callback) {
            callback(undefined, searchInstancePackages);
        }
    });
}

function loadCustomerWithPackage(packageName, callback) {
    var query = "select c.name, si_number SERVICEINSTANCEACCOUNT , a.fromdate, a.todate " +
        "from crestelcaamlwp.tbltcustomerpackagehistory a, crestelocslwp.tblcustomer b, crestelpmlwp.tblmproduct c, " +
        "crestelocslwp.ab_hierarchy d " +
        "where a.ACCOUNTID=b.ACCOUNTID and a.PACKAGEID=c.PRODUCTID and trunc(a.todate)>=last_day(trunc(sysdate,'MM')) " +
        "and substr(b.SUBSCRIBERIDENTIFIER,1,2) != 'LW' and d.SI_ID=a.ACCOUNTID " +
        " AND c.name=" + db.escape(packageName);

    var startTime = new Date().getTime();
    db.oracle.query(query, function (err, results) {
        var time = new Date().getTime() - startTime;

        if (err) {
            common.error("ElitecoreUserService", "loadCustomerWithPackage: failed to load active addons " +
            "from DB, time=" + time + ", error=" + err.message);
            if (callback) callback(err);
            return;
        }

        var rows;
        if (results.rows) {
            rows = results.rows;
        } else {
            rows = results;
        }

        var instances = [];
        if (rows) {
            if (rows.forEach) {
                common.log("ElitecoreUserService", "loadCustomerWithPackage: loaded active addons " +
                "from DB, time=" + time + ", count=" + rows.length);

                rows.forEach((item) => {
                    var packageItem = {
                        name: item[0],
                        serviceInstanceNumber: item[1]
                    }

                    if (instances.indexOf(packageItem.serviceInstanceNumber) == -1) {
                        instances.push(packageItem.serviceInstanceNumber);
                    }
                });
            } else {
                common.error("ElitecoreUserService", "loadCustomerWithPackage: result is not an array="
                + JSON.stringify(rows));
            }
        }

        if (callback) {
            callback(undefined, instances);
        }
    });
}

function loadAccountNumberByPhoneNumber(number, callback) {
    elitecoreConnector.searchAccount(number, undefined, undefined, undefined, function (err, result) {
        if (err) {
            common.error("ElitecoreUserService", "loadAccountNumberByPhoneNumber: failed to load account number, number="
            + number + ", error=" + err.message);
            if (callback) callback(err);
            return;
        }

        if (!result || !result.return) {
            common.error("ElitecoreUserService", "loadAccountNumberByPhoneNumber: response does not contain any data, number=" + number);
            if (callback) callback(new Error("Empty response"));
            return;
        }

        var accountDetails = result.return.searchAccountResponseVO;
        if (!accountDetails || !accountDetails[0] || !accountDetails[0].accountNumber) {
            common.error("ElitecoreUserService", "loadAccountNumberByPhoneNumber: account details are empty, number=" + number);
            var error = new Error("Empty service account details");
            error.status = "NOT_FOUND";
            if (callback) callback(error);
            return;
        }

        var accountNumber = accountDetails[0].accountNumber;
        if (callback) callback(undefined, accountNumber);
    });
}

function loadAccountNumberByServiceInstanceNumber(sin, callback) {
    elitecoreConnector.searchAccount(undefined, undefined, undefined, sin, function (err, result) {
        if (err) {
            common.error("ElitecoreUserService", "loadAccountNumberByServiceInstanceNumber: failed to load account number, sin=" +
            sin + ", error=" + err.message);
            if (callback) callback(err);
            return;
        }

        if (!result || !result.return) {
            common.error("ElitecoreUserService", "loadAccountNumberByServiceInstanceNumber: " +
            "response does not contain any data, sin=" + sin);
            if (callback) callback(new Error("Empty response"));
            return;
        }

        var accountDetails = result.return.searchAccountResponseVO;
        if (!accountDetails || !accountDetails[0] || !accountDetails[0].accountNumber) {
            common.error("ElitecoreUserService", "loadAccountNumberByServiceInstanceNumber: " +
            "service instance details are empty, sin=" + sin);
            var error = new Error("Empty service instance details");
            error.status = "NOT_FOUND";
            if (callback) callback(error);
            return;
        }

        accountDetails = accountDetails.sort((ad1, ad2) => {
            if (ad1.accountNumber < ad2.accountNumber) return -1;
            if (ad1.accountNumber > ad2.accountNumber) return 1;
            return 0;
        });

        var accountNumber = accountDetails[0].accountNumber;
        if (callback) callback(undefined, accountNumber);
    });
}

function getIDTypeByCode(code) {
    if (code == 1) {
        return 'NRIC';
    } else if (code == 4) {
        return 'FIN';
    } else {
        return 'OTHER';
    }
}

function getIDCodeByType(type) {
    if (type === 'NRIC') {
        return 1;
    } else if (type === 'FIN') {
        return 4;
    } else {
        return -1;
    }
}

function loadAccountNumberByBillingAccountNumber(ban, callback) {
    elitecoreConnector.getBillingAccountDetail(ban, function (err, result) {
        if (err) {
            common.error("ElitecoreUserService", "loadAccountNumberByBillingAccountNumber: failed to load account number, sin="
            + ban + ", error=" + err.message);
            if (callback) callback(err);
            return;
        }

        if (!result || !result.return) {
            common.error("ElitecoreUserService", "loadAccountNumberByBillingAccountNumber: "
            + "response does not contain any data, ban=" + ban);
            if (callback) callback(new Error("Empty response"));
            return;
        }

        if (!result.return.billingAccountVOs || !result.return.billingAccountVOs.customerAccountNumber) {
            common.error("ElitecoreUserService", "loadAccountNumberByBillingAccountNumber: "
            + "response does not contain any data, ban=" + ban);
            var error = new Error("Billing account details are empty");
            error.status = "NOT_FOUND";
            return callback(error);
        }

        var accountNumber = result.return.billingAccountVOs.customerAccountNumber;
        if (callback) callback(undefined, accountNumber);
    });
}

function loadAccountNumberByUserKey(userKey, callback) {
    var query = "SELECT a.number, m.prefix FROM msisdn m, app a, device d WHERE d.app_id=a.id AND a.number=m.number " +
        "AND d.status='A' AND a.status='A' AND m.status='A' AND d.user_key=" + db.escape(userKey);

    db.query_noerr(query, function (rows) {
        if (!rows || !rows[0]) {
            if (callback) callback(new Error("UserKey is not found"));
            return;
        }

        var prefix = rows[0].prefix.toString();
        var number = rows[0].number.substring(prefix.length);

        callback(undefined, number);
    });
}

function loadCustomerDetails(accountNumber, callback) {
    elitecoreConnector.getCustomerHierarchyDetails(accountNumber, function (err, result) {
        if (err) {
            common.error("ElitecoreUserService", "loadCustomerDetails: failed to load info, accountNumber="
            + accountNumber + ", error=" + err.message);
            if (callback) callback(err);
            return;
        }

        if (!result || !result.return) {
            common.error("ElitecoreUserService", "loadCustomerDetails: response does not contain any data, accountNumber=" + accountNumber);
            if (callback) callback(new Error("Empty response"));
            return;
        }

        var serviceAccountDetails = result.return.serviceAccountDetailResponseobj;
        var serviceInstanceDetail = result.return.serviceInstanceDetailResponseobj;
        var billingAccountDetails = result.return.billingAccountDetailResponseobj;
        var customerAccountDetailsObj = result.return.customerAccountResponseobj;

        if (!serviceAccountDetails || !serviceAccountDetails[0]) {
            common.error("ElitecoreUserService", "loadCustomerDetails: service account details are empty, accountNumber=" + accountNumber);
            if (callback) callback(new Error("Empty service account details"));
            return;
        } else if (!serviceInstanceDetail || !serviceInstanceDetail[0]) {
            common.error("ElitecoreUserService", "loadCustomerDetails: service instance details are empty, accountNumber=" + accountNumber);
            if (callback) callback(new Error("Empty service account details"));
            return;
        } else if (!billingAccountDetails || !billingAccountDetails[0]) {
            common.error("ElitecoreUserService", "loadCustomerDetails: billing instance details are empty, accountNumber=" + accountNumber);
            if (callback) callback(new Error("Empty billing instance details"));
            return;
        } else if (!customerAccountDetailsObj) {
            common.error("ElitecoreUserService", "loadCustomerDetails: customer account details are empty, accountNumber=" + accountNumber);
            if (callback) callback(new Error("Empty billing instance details"));
            return;
        }

        var serviceAccountDetailsObj = serviceAccountDetails[0];
        var serviceInstanceDetailObj = serviceInstanceDetail[0];
        var billingAccountDetailsObj = billingAccountDetails[0];

        // init billing account related fields

        var billingCycleName = billingAccountDetailsObj.billingAccountVOs.billCycleName;
        var billingCycleDay = billingCycleName ? billingCycleName.split('_')[1] : 1;
        var billingCycleStartDay = billingCycleDay ? parseInt(billingCycleDay) : 1;

        var billingAddress = {
            addressOne: billingAccountDetailsObj.billingAccountVOs.addressDetailsVo.addressOne,
            addressTwo: billingAccountDetailsObj.billingAccountVOs.addressDetailsVo.addressTwo,
            postcode: billingAccountDetailsObj.billingAccountVOs.addressDetailsVo.zipPostalCode,
            city: billingAccountDetailsObj.billingAccountVOs.addressDetailsVo.city,
            state: billingAccountDetailsObj.billingAccountVOs.addressDetailsVo.state,
            country: billingAccountDetailsObj.billingAccountVOs.addressDetailsVo.country
        };
        var billingCreditCard = {
            token: billingAccountDetailsObj.billingAccountVOs.accountProfile.strcustom1,
            tokenShort: billingAccountDetailsObj.billingAccountVOs.accountProfile.strcustom2,
            bank: billingAccountDetailsObj.billingAccountVOs.accountProfile.strcustom3,
            type: billingAccountDetailsObj.billingAccountVOs.accountProfile.strcustom4,
            last4Digits: billingAccountDetailsObj.billingAccountVOs.accountProfile.strcustom5,
            customerBankName: billingAccountDetailsObj.billingAccountVOs.accountProfile.strcustom11
        }

        // init customer account related fields

        var checkoutDate = common.safeDate(customerAccountDetailsObj.accountProfile.datecustom6);
        var activationDate = common.safeDate(serviceAccountDetailsObj.accountProfile.datecustom5);
        var customerBirthday = common.safeDate(customerAccountDetailsObj.accountProfile.datecustom1);
        var customerPortedStatus = customerAccountDetailsObj.accountProfile.strcustom3;
        var customerPorted = customerPortedStatus === "MNP" ? true : false;
        var customerIdentity = {
            type: customerAccountDetailsObj.accountProfile.strcustom1,
            number: customerAccountDetailsObj.accountProfile.strcustom2,
            typeFormatted: getIDTypeByCode(customerAccountDetailsObj.accountProfile.strcustom1)
        };

        //check if valid for verification of identity

        var validLength = customerIdentity.number && customerIdentity.number.length == 9;
        var digitsString = validLength ? customerIdentity.number.substring(1, 8) : null;

        customerIdentity.validForVerification = validLength && /^\d+$/.test(digitsString);
        customerIdentity.digits = customerIdentity.validForVerification ? digitsString : null;

        // init service account related fields

        var siCreationDate = common.safeDate(serviceInstanceDetailObj.createdDate);
        var siPackChangeDate = common.safeDate(serviceInstanceDetailObj.basicPlan1.packageActivationDate);
        var siBasePlanName = serviceInstanceDetailObj.basicPlan1.packageName;

        var currentDate = new Date();
        var siEarTermDate;

        var planActivationCycle = elitecoreUtils.calculateBillCycle(billingCycleStartDay, undefined, siPackChangeDate);
        if (planActivationCycle.endDateUTC.getTime() > currentDate.getTime()) {
            var nextCycle = elitecoreUtils.calculateBillCycle(billingCycleStartDay, undefined, planActivationCycle.endDateUTC);
            siEarTermDate = nextCycle.endDateUTC;
        } else {
            var currentCycle = elitecoreUtils.calculateBillCycle(billingCycleStartDay, undefined, new Date());
            siEarTermDate = currentCycle.endDateUTC;
        }

        var tasks = [];
        tasks.push(function (callback) {
            var nowMs = new Date().getTime();
            var sin = serviceInstanceDetailObj.serviceInstanceNumber;
            elitecoreConnector.viewCustomer(sin, nowMs, function (err, result) {
                if (err) return callback(err);
                if (!result || !result.return || !result.return.responseObject) {
                    common.error("ElitecoreUserService", "loadCustomerDetails: customer details (viewCustomer) are empty, " +
                    "accountNumber=" + accountNumber + ", sin=" + sin);
                    return callback(new Error("Customers details are empty"));
                }
                callback(undefined, {customerView: result.return.responseObject});
            });
        });
        tasks.push(function (callback) {
            var sin = serviceInstanceDetailObj.serviceInstanceNumber;
            elitecoreConnector.listInventory(sin, function (err, result) {
                if (err) return callback(err);
                if (!result || !result.return) {
                    common.error("ElitecoreUserService", "loadCustomerDetails: customers inventory (listInventory) is empty, " +
                    "accountNumber=" + accountNumber + ", sin=" + sin);
                    return callback(new Error("Customers inventory is empty"));
                }
                var inventories = result.return.inventories ? result.return.inventories : [];
                callback(undefined, {inventories: inventories});
            });
        });
        tasks.push(function (callback) {
            elitecorePackagesService.loadHierarchy(function (err, hierarchy) {
                if (err) return callback(err);
                callback(undefined, {hierarchy: hierarchy});
            });
        });

        async.parallel(tasks, function (err, resultsList) {
            if (err) {
                common.error("ElitecoreUserService", "updateUserInfo: error=" + err.message);
                if (callback) callback(err);
                return;
            }

            var result = {};
            resultsList.forEach(function (item) {
                if (item.customerView) result.customerView = item.customerView;
                if (item.inventories) result.inventories = item.inventories;
                if (item.hierarchy) result.hierarchy = item.hierarchy;
            });

            // need to check if isBasePlan has base_id field

            var siBasePlan;
            if (result.hierarchy.base) result.hierarchy.base.forEach(function (item) {
                if (item.options) item.options.forEach(function (item) {
                    if (item.name == siBasePlanName) siBasePlan = item;
                })
            })

            if (!siBasePlan) {
                common.error("ElitecoreUserService", "updateUserInfo: Base plan " + siBasePlanName +
                " is not found, accountNumber=" + accountNumber);
                if (callback) callback(new Error("Base plan " + siBasePlanName + " is not found"));
                return;
            }

            // check for available inventory

            var siInventoryPrimary = {number: "", statusNumber: -1};
            var siInventories = [];
            result.inventories.forEach(function (item) {
                var obj = {
                    number: item.inventoryNumber,
                    status: item.status,
                    active: item.status === 'CST01',
                    statusNumber: parseInt(item.status.substring(3))
                };

                obj.unpaired = !item.bundleInventoryRes || !item.bundleInventoryRes.inventoryNumber;
                if (item.bundleInventoryRes) {
                    obj.iccid = item.bundleInventoryRes.inventoryNumber;
                    if (item.bundleInventoryRes.bundleInventoryRes) {
                        obj.imsi = item.bundleInventoryRes.bundleInventoryRes.inventoryNumber;
                    }
                }

                siInventories.push(obj);
                if (obj.number) {
                    if (siInventoryPrimary.statusNumber == -1) {
                        siInventoryPrimary = obj;
                    } else if (siInventoryPrimary.unpaired && !obj.unpaired) {
                        siInventoryPrimary = obj;
                    } else if (siInventoryPrimary.statusNumber > obj.statusNumber && obj.unpaired == siInventoryPrimary.unpaired) {
                        siInventoryPrimary = obj;
                    }
                }
            });

            // check status

            // 4 is actually "Registered" by EC messed up
            // and used that status for terminated customers
            // so in the array we use "Terminated" instead of "Registered"

            var statuses = {
                '1': 'Active',
                '2': 'Inactive',
                '3': 'Suspended',
                '4': 'Terminated',
                '5': 'Terminated'
            }

            var siCustomerStatus = statuses[result.customerView.customerStatusId];
            if (!siCustomerStatus) {
                siCustomerStatus = "Unknown " + result.customerView.customerStatusId;
            }

            var siAddonsCurrent = {
                extra: {},
                boost: [],
                bonus: [],
                general: [],
                invalid: []
            }
            var siAddonsFuture = {
                extra: {},
                boost: [],
                bonus: [],
                general: [],
                invalid: []
            }
            var siAddonsExpired = [];

            var siRoamingInfo = {
                limit: 100,
                addon: undefined
            };
            var siUnlimitedData = {
                enabled: false,
                addon: undefined
            };
            var addonBSSSubscribed = {};

            var nowMs = new Date().getTime();
            var billCycle = elitecoreUtils.calculateBillCycle(billingCycleStartDay);

            if (serviceInstanceDetailObj.addOnPlan) {
                serviceInstanceDetailObj.addOnPlan.forEach(function (item) {
                    addPackage(item, true, result.hierarchy, result.customerView.balanceData, siBasePlanName, billCycle,
                        siAddonsCurrent, siAddonsFuture, siAddonsExpired, addonBSSSubscribed, siRoamingInfo, siUnlimitedData, nowMs);
                });
            }
            if (result.customerView.packageData) {
                result.customerView.packageData.forEach(function (item) {
                    addPackage(item, false, result.hierarchy, result.customerView.balanceData, siBasePlanName, billCycle,
                        siAddonsCurrent, siAddonsFuture, siAddonsExpired, addonBSSSubscribed, siRoamingInfo, siUnlimitedData, nowMs);
                });
            }

            var nextMonthPlanPrice = siBasePlan.price;
            nextMonthPlanPrice += getComponentsPrice(siAddonsFuture, "data");
            nextMonthPlanPrice += getComponentsPrice(siAddonsFuture, "voice");
            nextMonthPlanPrice += getComponentsPrice(siAddonsFuture, "sms");
            siAddonsFuture.general.forEach(function (item) {
                nextMonthPlanPrice += computeGeneralAddonPrice(item);
            });

            var obj = {
                prefix: "65",
                number: siInventoryPrimary.number,
                iccid: siInventoryPrimary.iccid ? siInventoryPrimary.iccid : "Unpaired",
                imsi: siInventoryPrimary.imsi ? siInventoryPrimary.imsi : "Unpaired",
                status: siCustomerStatus,
                first_name: billingAccountDetailsObj.billingAccountVOs.firstName,
                email: customerAccountDetailsObj.contactDetailsVo.emailId,
                account: customerAccountDetailsObj.accountNumber,
                billing_cycle: billingCycleStartDay,
                ported: customerPorted,
                birthday: customerBirthday ? common.getDate(new Date(common.msOffsetSG(customerBirthday.getTime()))) : undefined,
                base: siBasePlan,
                extra_current: siAddonsCurrent.extra,
                extra: siAddonsFuture.extra,
                general: siAddonsCurrent.general,
                general_future: siAddonsFuture.general,
                boost: siAddonsCurrent.boost,
                bonus: siAddonsCurrent.bonus,
                bonus_future: siAddonsFuture.bonus,
                invalid: {addons: {current: siAddonsCurrent.invalid, future: siAddonsFuture.invalid}},
                roaming: siRoamingInfo,
                recurring: nextMonthPlanPrice,

                billingAccountNumber: billingAccountDetailsObj.billingAccountVOs.accountNumber,
                billingAccountEmail: billingAccountDetailsObj.billingAccountVOs.contactDetailsVo.emailId,
                billingFullName: billingAccountDetailsObj.billingAccountVOs.firstName,
                billingCycleStartDay: billingCycleStartDay,
                billingCycleName: billingCycleName,
                billingAddress: billingAddress,
                billingCreditCard: billingCreditCard,
                serviceAccountNumber: serviceAccountDetailsObj.accountNumber,
                serviceAccountEmail: serviceAccountDetailsObj.contactDetailsVo.emailId,
                serviceInstanceNumber: serviceInstanceDetailObj.serviceInstanceNumber,
                serviceInstanceInventoryPrimary: siInventoryPrimary,
                serviceInstanceInventories: siInventories,
                serviceInstanceCustomerStatus: siCustomerStatus,
                serviceInstanceBasePlan: siBasePlan,
                serviceInstanceBasePlanName: siBasePlanName,
                serviceInstanceCurrentAddons: siAddonsCurrent,
                serviceInstanceFutureAddons: siAddonsFuture,
                serviceInstanceRoaming: siRoamingInfo,
                serviceInstanceUnlimitedData: siUnlimitedData,
                serviceInstanceCreationDate: siCreationDate,
                serviceInstancePackageChangeDate: siPackChangeDate,
                serviceInstanceEarliestTerminationDate: siEarTermDate,
                customerAccountFullName: customerAccountDetailsObj.firstName,
                customerAccountNumber: customerAccountDetailsObj.accountNumber,
                customerAccountEmail: customerAccountDetailsObj.contactDetailsVo.emailId,
                customerAccountIdCode: customerAccountDetailsObj.accountProfile.strcustom1,
                customerAccountIdType: getIDTypeByCode(customerAccountDetailsObj.accountProfile.strcustom1),
                customerAccountIdNumber: customerAccountDetailsObj.accountProfile.strcustom2,
                customerBirthday: customerBirthday,
                checkoutDate: checkoutDate,
                activationDate: activationDate,
                customerPorted: customerPorted,
                customerPortedStatus: customerPortedStatus,
                customerIdentity: customerIdentity,
                referralCode: serviceAccountDetailsObj.accountProfile.strcustom1,
                orderReferenceNumber: serviceAccountDetailsObj.accountProfile.strcustom7,
                nextMonthPlanPrice: nextMonthPlanPrice
            }

            callback(undefined, obj);
        });
    });
}

var addPackage = function (pack, currentlySubscribed, businessHierarchy, balanceData, siBasePlanName, billCycle,
                           siAddonsCurrent, siAddonsFuture, siAddonsExpired, addonBSSSubscribed,
                           siRoamingInfo, siUnlimitedData, nowMs) {
    if (!pack || !balanceData || !businessHierarchy) {
        return;
    }

    var packageName = pack.packageName;
    if (packageName == siBasePlanName) {
        return;
    }

    var hierarchyProduct = elitecoreUtils.findPackageByName(businessHierarchy, packageName);
    var addonProduct = hierarchyProduct ? common.deepCopy(hierarchyProduct) : {name: packageName};
    addonProduct.packageHistoryId = pack.packageHistoryId;
    addonProduct.subscribed = currentlySubscribed;

    addonProduct.billStartDate = pack.packageActivationDate
        ? common.safeDate(pack.packageActivationDate) : new Date(parseInt(pack.validFromDate));
    addonProduct.expiryDate = pack.expiryDate
        ? new Date(pack.expiryDate) : new Date(parseInt(pack.validToDate));

    var current = addonProduct.billStartDate.getTime() <= nowMs && addonProduct.expiryDate.getTime() > nowMs;
    var future = addonProduct.expiryDate.getTime() > billCycle.endDateUTC.getTime();

    if (current && addonProduct.name.startsWith("Cap_Data_Roaming_")) {
        var limit = parseInt(addonProduct.name.replace("Cap_Data_Roaming_", ""));
        if (limit > siRoamingInfo.limit) {
            siRoamingInfo.limit = limit;
            siRoamingInfo.addon = addonProduct;
        }
    } else if (current && addonProduct.name === "Data FreeFlow") {
        siUnlimitedData.enabled = true;
        siUnlimitedData.addon = addonProduct;
    }

    var addAddon = function (type, addon) {
        if (!future && !current) {
            siAddonsExpired.push(addonProduct);
            return;
        }

        if (type === "base") {
            if (current) {
                if (!siAddonsCurrent.extra[addon.component]) {
                    siAddonsCurrent.extra[addon.component] = (addon);
                } else {
                    siAddonsCurrent.invalid.push(addon);
                }
            }
            if (future) {
                if (!siAddonsFuture.extra[addon.component]) {
                    siAddonsFuture.extra[addon.component] = (addon);
                } else {
                    siAddonsFuture.invalid.push(addon);
                }
            }
        } else if (type === "boost" || type === "bonus" || type === "general") {
            if (current) siAddonsCurrent[type].push(addon);

            // bonus Bonus 0MB R is reset to 0MB every month so there is no need to put
            // it into consideration when we compute future bonus data

            if (future && addon.name != "Bonus 0MB R") siAddonsFuture[type].push(addon);
        } else {
            if (current) siAddonsCurrent.invalid.push(addonProduct);
            if (future) siAddonsFuture.invalid.push(addonProduct);
        }
    }

    var type = addonProduct.type;
    if (type === "bonus") {
        if (addonProduct.name == "Bonus 0MB R") {
            if (currentlySubscribed) {
                var balanceData = elitecoreUtils.findPackageByFiled(balanceData,
                    addonProduct.name, "packageName");
                if (balanceData) {
                    var leftKb = parseFloat(balanceData.unusedValue) / 1024;
                    var usedKb = parseFloat(balanceData.usedValue) / 1024;
                    addonProduct.kb = leftKb + usedKb;
                }
                addAddon(type, addonProduct);
            } else {
                // bonus Bonus 0MB R is reset to 0MB every month so there is no need to put
                // it into consideration when we compute future bonus data
            }
        } else if (currentlySubscribed) {
            if (current) {
                var currentKb = addonBSSSubscribed[addonProduct.name];
                addonBSSSubscribed[addonProduct.name] = currentKb ? (currentKb + addonProduct.kb) : addonProduct.kb;
            }
            addAddon(type, addonProduct);
        } else {
            var balanceData = elitecoreUtils.findPackageByFiled(balanceData,
                addonProduct.name, "packageName");

            var totalBalanceActive = 0;
            if (balanceData) {
                var leftKb = parseFloat(balanceData.unusedValue) / 1024;
                var usedKb = parseFloat(balanceData.usedValue) / 1024;
                totalBalanceActive = leftKb + usedKb;
                totalBalanceActive += 1024; // reservation discrepancy
            }

            var totalBalanceSubscribed = 0;
            if (addonBSSSubscribed[addonProduct.name]) {
                totalBalanceSubscribed = addonBSSSubscribed[addonProduct.name];
            }

            var totalActiveNotSubscribed = totalBalanceActive - totalBalanceSubscribed;
            var countActiveNotSubscribed = parseInt(totalActiveNotSubscribed / addonProduct.kb);

            if (countActiveNotSubscribed > 200) {
                console.error("ElitecoreUserService[CRITICAL]", "updateUserInfo: data bucket is too large" +
                ", countActiveNotSubscribed=" + countActiveNotSubscribed +
                ", totalActiveNotSubscribed=" + totalActiveNotSubscribed +
                ", addonProduct kb=" + addonProduct.kb +
                ", addonProduct name=" + addonProduct.name);
                countActiveNotSubscribed = 0;
            }

            for (var i = 0; i < countActiveNotSubscribed; i++) {
                var copy = common.deepCopy(addonProduct);
                copy.billStartDate = new Date(copy.billStartDate);
                copy.expiryDate = new Date(copy.expiryDate);
                addAddon(type, addonProduct);
            }
        }
    } else {
        var endPostfix = addonProduct.expiryDate.getTime() > new Date("2100-01-01T00:00:00.000Z").getTime()
            ? "none" : addonProduct.expiryDate.getTime();
        var startPostfix = addonProduct.billStartDate.getTime();

        var addonKey = addonProduct.name + "_" + startPostfix + "_" + endPostfix;
        var generalKey = addonProduct.type === "general" ? "general_" + addonProduct.id : "";

        if (currentlySubscribed) {
            addonBSSSubscribed[addonKey] = 1;
            if (generalKey && current) {
                addonBSSSubscribed[generalKey] = 1;
            }

            addAddon(addonProduct.type, addonProduct);
        } else if (!addonBSSSubscribed[addonKey] && !addonBSSSubscribed[generalKey]) {
            addAddon(addonProduct.type, addonProduct);
        }
    }
}

var getComponentsPrice = function (siAddonsFuture, field) {
    if (siAddonsFuture.extra[field]) {
        return siAddonsFuture.extra[field].price;
    } else {
        return 0;
    }
}
