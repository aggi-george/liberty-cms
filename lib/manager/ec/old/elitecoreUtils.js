var async = require('async');
var request = require('request');
var common = require('../../common');
var config = require('../../../config');

var log = config.EC_LOGSENABLED;

module.exports = {

    /**
     *
     */
    calculateBillCycle: function (billingCycleStartDay, referenceDateSGT, referenceDateUTC) {
        if (!billingCycleStartDay) {
            billingCycleStartDay = 1;
        }
        billingCycleStartDay = parseInt(billingCycleStartDay);

        var index;
        if (referenceDateSGT) {
            index = new Date(referenceDateSGT);
        } else if (referenceDateUTC) {
            index = new Date(common.msOffsetSG(new Date(referenceDateUTC).getTime()));
        } else {
            index = new Date(common.msOffsetSG());
        }

        var yearStart = index.getFullYear();
        var yearEnd = yearStart;
        var monthStart = index.getMonth() + 1;
        var monthEnd = monthStart;
        var day = index.getDate();

        if (day >= billingCycleStartDay) {
            if (monthStart == 12) {
                monthEnd = 1;
                yearEnd++;
            } else {
                monthEnd++;
            }
        } else {
            if (monthStart == 1) {
                monthStart = 12;
                yearStart = yearStart - 1;
            } else {
                monthStart = monthStart - 1;
            }
        }

        var startString = yearStart + "-" + ('0' + monthStart).slice(-2) + "-" + ('0' + billingCycleStartDay).slice(-2);
        var endString = yearEnd + "-" + ('0' + monthEnd).slice(-2) + "-" + ('0' + billingCycleStartDay).slice(-2);

        var cycle = {
            start: startString,
            end: endString,
            startDate: new Date(startString),
            endDate: new Date(endString),

            startDateSGT: new Date(startString),
            endDateSGT: new Date(endString),
            startDateUTC: new Date(new Date(startString).getTime() - 8 * 60 * 60 * 1000),
            endDateUTC: new Date(new Date(endString).getTime() - 8 * 60 * 60 * 1000)
        }

        return cycle;
    },

    /**
     *
     */
    findPackageByName: function (packages, name) {
        return findPackageByField(packages, name, "name");
    },

    /**
     *
     */
    findPackageById: function (packages, id) {
        return findPackageByField(packages, id, "id");
    },

    /**
     *
     */
    findPackageByFiled: function (packages, name, field) {
        return findPackageByField(packages, name, field);
    },

    /**
     *
     */
    replace$Keys: function (obj) {
        return replace$Keys(obj);
    }
}

function findPackageByField(obj, value, field) {
    if (common.isArray(obj)) {
        for (var i = 0; i < obj.length; i++) {
            var found = findPackageByField(obj[i], value, field);
            if (found) return found;
        }
    } else {
        var keys = Object.keys(obj);
        for (var i = 0; i < keys.length; i++) {
            var key = keys[i];
            if (common.isArray(obj[key])) {
                var found = findPackageByField(obj[key], value, field);
                if (found) return found;
            } else if (key === field && value === obj[key]) {
                return obj;
            }
        }
    }
}

function replace$Keys(obj) {
    if (!obj) {
        return;
    }

    if (common.isArray(obj)) {
        for (var i = 0; i < obj.length; i++) {
            replace$Keys(obj[i]);
        }
    } else {
        var keys = Object.keys(obj);
        for (var i = 0; i < keys.length; i++) {
            var key = keys[i];
            var value = obj[key];

            if (key.indexOf("$") == 0) {
                obj['\\' + key] = value;
                delete obj[key];
            }

            if (common.isArray(value) || common.isObject(value)) {
                replace$Keys(value);
            }
        }
    }
}
