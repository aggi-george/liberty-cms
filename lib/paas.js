var request = require('request');
var common = require('./common');
var ec = require('./elitecore');
var config = require('../config');

var logsEnabled = config.LOGSENABLED;

var host = (config.DEV) ? "10.20.1.46:6190" : "10.20.1.45:6190";
var host2 = (config.DEV) ? "http://10.20.1.46:6190" : "http://10.20.1.45:6190"; // api server
var host3 = (config.DEV) ? "http://10.20.1.34:6180" : "http://10.20.1.36:6190"; // admin server

var portalURL = "http://" + host + "/api/mobile/v1/email_address.json";
var chargeURL = "http://" + host + "/api/payment/v1/charge_with_recurrent_token.json";
var statusURL = "http://" + host + "/api/payment/v1/charge_with_recurrent_token_query_status.json";
var deviceURL = "http://" + host + "/api/mobile/v1/customer_order_info.json";

// exports

exports.getDevices = getDevices;
exports.getWebPortalEmail = getWebPortalEmail;
exports.connect = connect;
exports.host = host;

// functions

function connect(method, path, params, callback) {
	var parameters = {	"form" : params,
				"rejectUnauthorized" : true,
				"headers" : { 
					"Accept" : "*/*",
					"timeout" : 15000,
					"Authorization" : "aca2286153a959de1d341abc0d4a5b15"
				} 
			};
	var url = "";
	if ( path == "/api/kirk/v1/update_port_in_status" ) {
		url = host2 + "" + path;
		parameters = params;
	} else if ( ((method == "get") && (path == "/api/mobile/v1/latest_creditcard.json")) ||
			((method == "post") && (path == "/api/mobile/v1/billing_address")) ) {
		url = "http://" + host + "" + path;
	} else {
		url = host3 + "" + path;
		parameters = params;
	}
	var start = new Date();
	request[method](url, parameters, function(err, response, body) {
		if (!err && body) {
			var parsed = common.safeParse(body);
			if ( typeof(parsed) == "object" ) {
				parsed.query_time = new Date() - start;
				callback(undefined, parsed);
			} else {
				if (logsEnabled) common.error("paas connect", url + " " + JSON.stringify(parameters.form));
				callback(true, parsed);
			}
		} else {
			if (logsEnabled) common.error("paas connect", url + " " + JSON.stringify(parameters.form));	
			callback(true, err);
		}
	});
}

function getWebPortalEmail(account, callback) {
	var chargeParams = {
		"form": {
			"account_number": account,
			"channel": "Mobile-App"
		},
		"timeout": 10000,
		"headers": {
			"Authorization": "aca2286153a959de1d341abc0d4a5b15"
		}
	};

	if (logsEnabled) common.log("getWebPortalEmail", "start load transaction, params=" + JSON.stringify(chargeParams));
	request.post(portalURL, chargeParams, function (err, response, body) {
		if (err) {
			if (logsEnabled) common.error("getWebPortalEmail", "start transaction error, err=" + err.message);
			return callback(err);
		}

		if (logsEnabled) common.log("getWebPortalEmail", "start transaction result, body=" + body);
		var parsed = body ? common.safeParse(body) : undefined;
		if (parsed && parsed.data && parsed.data.length > 0) {
			callback(null, parsed.data[0]);
		} else {
			callback(new Error("Web Portal login email is not found in eComm response, " + JSON.stringify(body)));
		}
	});
}

function getDevices(account, callback) {
	var start = new Date();
	var total_time = 0;
	URL = deviceURL + "?account_number=" + account;
	if (logsEnabled) common.log("getDevice", deviceURL);
	var params = {	"timeout" : 15000,
			"headers" : {
				"Authorization" : "aca2286153a959de1d341abc0d4a5b15"
			}
		};
	request.get(URL, params, function(err, response, body) {
		if (!err && body) {
			var parsed = common.safeParse(body);
			if (logsEnabled && !parsed) common.log("getDevice", body);
			if ( parsed && parsed.data && (parsed.code == 0) ) {
				parsed.query_time = new Date() - start;
				total_time += parsed.query_time;
				var devices = new Array;
				var found = false;
				parsed.data.forEach(function (data) {
					if ( data.order.device_info.type == "Device selected" ) {
						found = true;
						devices.push(data.order.device_info.device_related_info)
					}
				});
				if (!found) callback({ "code" : -1, "notfound" : true });
				else callback(undefined, devices);
			} else {
				if (logsEnabled) common.log("getDevice error", JSON.stringify(parsed));
				if (logsEnabled) common.log("getDevice total_time", total_time);
				callback(parsed);
			}
		} else if ( !body && (err == "Error: ETIMEDOUT") ) {
			if (logsEnabled) common.log("getDevice error", err);
			callback({ "code" : -1, "timeout" : true });
		} else {
			if (logsEnabled) common.log("getDevice error", JSON.stringify(err));
			callback(err);
		}
	});
}
