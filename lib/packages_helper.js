var db = require('./db_handler');
var common = require('./common');
var config = require('../config');

exports.loadPackageInfo = function(id, callback) {
    var query = "SELECT s.id, s.name, SUM(s.available) available FROM (SELECT DISTINCT t.id, t.name, " +
	"IF(p.product_id=" +  db.escape(id) + ",1,0) available FROM teams t LEFT JOIN  packagesTeams pt " +
	"ON t.id=pt.teamID LEFT JOIN packages p ON pt.packageID=p.id) s GROUP BY s.id";
    db.query_noerr(query, function(rowsPlatform) {
	var query = "SELECT * FROM packages where product_id = " + db.escape(id);
	db.query_noerr(query, function(rowsInfo) {
	    if (callback) callback(rowsPlatform, (rowsInfo && rowsInfo.length > 0 ? rowsInfo[0] : undefined));
	});
    });
}

exports.isShownOnMobile = function(aviability) {
    if (aviability) {
	for (var i = 0; i < aviability.length; i++) {
	    if (aviability[i].id == 1) {
		return aviability[i].available;
	    }
	}
    }
    return 0;
}

exports.loadPackages = function (teamID, callback) {
	var q = ( teamID && (teamID > 0) ) ?
		"SELECT p.product_id, p.order_id, p.title, p.app, p.subtitle, p.description_short, p.description_full, p.disclaimer, p.sub_effect, p.unsub_effect, p.action, p.beta, p.beta_group, p.advanced_payment, p.free_package, SUM(IF(pt.teamID=" + db.escape(teamID) + ", 1, 0)) enabled FROM packages p LEFT JOIN packagesTeams pt ON pt.packageID=p.id WHERE p.enabled=1 GROUP BY p.product_id" :
		"SELECT p.product_id, p.order_id, p.title, p.app, p.subtitle, p.description_short, p.description_full, p.disclaimer, p.sub_effect, p.unsub_effect, p.action, p.beta, p.beta_group, p.advanced_payment, p.free_package, 1 enabled FROM packages p WHERE p.enabled=1" ;
	db.query_noerr(q, function (rows) {
	    if (callback) callback(rows);
	});
}

exports.updateProductHierarchy = function (product, item) {
	if ( product && item && (product.id == item.product_id) ) {
		product.order_id = item.order_id;
		product.title = item.title;
		product.app = item.app;
		product.subtitle = item.subtitle;
		product.description_short = item.description_short;
		product.description_full = item.description_full;
		product.disclaimer = item.disclaimer;
		product.sub_effect = item.sub_effect;
		product.unsub_effect = item.unsub_effect;
		product.action = item.action;
		product.beta = item.beta;
		product.beta_group = item.beta_group;
		product.advanced_payment = item.advanced_payment;
		product.free_package = item.free_package ? item.free_package : "";
		product.assets = item.assets;
		product.enabled = item.enabled;
	} else {
		if (config.EC_LOGSENABLED) common.log("updateProductHierarchy", item.product_id);
	}
}

