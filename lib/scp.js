var request = require('request');
var common = require('../lib/common');
var config = require('../config');

var logsEnabled = true;

var host = (config.DEV) ? "10.20.2.38" : "10.20.2.38";		// for now only 1 floating IP, dev & prod

// exports

exports.connect = connect;

// functions

function connect(command, msisdn, call_type, callback) {
	var url = "http://" + host + "/provisioning/sip_provisioning.php";
	var sip_allowed = (command == "set") ? 1 : (command == "update") ? 0 : undefined;
	var params = {  
			"command" : command,
			"msisdn" : msisdn,
			"call_type" : call_type,
			"sip_allowed" : sip_allowed,
		 };
	var start = new Date();
	request.get({ "url" : url, "qs" : params, "timeout" : 5000 }, function(err, response, body) {
		if (!err && body) {
			var parsed = common.safeParse(body);
			if ( typeof(parsed) == "object" ) {
				if ( (command == "set") && (parsed.response_code == "-102") ) {	// case where the msisdn exists
					params.command = "update";
					request.get({ "url" : url, "qs" : params }, function(err, response, body) {
						if (!err && body) {
							var parsed = common.safeParse(body);
							if ( typeof(parsed) == "object" ) {
								parsed.query_time = new Date() - start;
								callback(undefined, parsed);
							} else {
								callback(true);
							}
						} else {
							callback(true);
						}
					});
				} else {
					parsed.query_time = new Date() - start;
					callback(undefined, parsed);
				}
			} else {
				callback(true);
			}
		} else {
			callback(true);
		}
	});
}

