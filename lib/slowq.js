var config = require('../config');
var common = require('./common');
var db = require('./db_handler');

var LOG = config.LOGSENABLED;

// exports

exports.send = send;

// functions

function send(push, callback) {
    var job = db.queue.create('slow', push)
        .events(false)
        .removeOnComplete(true)
        .delay(0)
        .attempts(2)
        .backoff({"delay": 10 * 1000, "type": "fixed"})
        .save(function (err) {
            if (LOG) common.log("slow job", job.id);
            if (callback) callback(err);
        });
}
