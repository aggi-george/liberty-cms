var rpc = require('node-json-rpc');
var async = require('async');
var common = require('./common');
var config = require('../config');

exports.connect = connect;

var client1 = config.VOICECORERPC_ADDR1 && config.VOICECORERPC_PORT1 ?
    new rpc.Client({
        "host"  : config.VOICECORERPC_ADDR1,
        "port" : config.VOICECORERPC_PORT1,
        "path" : "/RPC",
        "strict" : true }) : undefined;

var client2 = config.VOICECORERPC_ADDR2 && config.VOICECORERPC_PORT2 ?
    new rpc.Client({
        "host"  : config.VOICECORERPC_ADDR2,
        "port" : config.VOICECORERPC_PORT2,
        "path" : "/RPC",
        "strict" : true }) : undefined;

var client3 = config.VOICECORERPC_ADDR3 && config.VOICECORERPC_PORT3 ?
    new rpc.Client({
        "host"  : config.VOICECORERPC_ADDR3,
        "port" : config.VOICECORERPC_PORT3,
        "path" : "/RPC",
        "strict" : true }) : undefined;

function connect(method, params, id, cb) {
    var tasks = new Array;
    var clients = [ client1, client2, client3 ];
    clients.forEach(function (client, idx) {
        if (client) tasks.push(function (callback) {
            call(idx+1, client, method, params, id, function (err, result) {
                callback(undefined, { "client" : idx+1, "error" : err, "result" : (result && result.result ? result.result : result) });
            });
        });
    })
    async.parallel(tasks, function (err, result) {
        cb(err, result);
    });
}

function call(cid, client, method, params, id, callback) {
    new Promise(function (resolve, reject) {
        var timeout = setTimeout(function () {
            common.error("RPC[" + cid + "]", "timeout");
            reject(new Error("Timeout"));
        }, 2 * 1000);
        client.call({
                "jsonrpc" : "2.0",
                "method" : method,
                "params" : params,
                "id" : id }, function(err, result) {
            clearTimeout(timeout);
            if (err) reject(err);
            else resolve(result);
        });
    }).then(function (result) {
        callback(undefined, result);
    }).catch(function (err) {
        callback(err);
    });
}


