var bonuses = {
    selfcare_install: {
        activity: 'bonus_selfcare_install_added',
        imageId: 'ic-bonus-selfcare-install',
        titleId: 'bonus_selfcare_install_name',
        configTitle: 'CirclesCare Bonus',
        thirdParty: false
    },
    gentwo_install: {
        activity: 'bonus_gentwo_install_added',
        titleId: 'bonus_gentwo_install_name',
        configTitle: 'CirclesTalk Bonus',
        thirdParty: false
    },
    birthday: {
        activity: 'bonus_birthday_added',
        imageId: 'ic-bonus-birthday',
        titleId: 'bonus_birthday_name',
        configTitle: 'Birthday Bonus',
        thirdParty: false
    },
    loyalty: {
        activity: 'bonus_loyalty_added',
        imageId: 'ic-bonus-loyalty',
        titleId: 'bonus_loyalty_name',
        configTitle: 'Loyalty Bonus',
        thirdParty: false
    },
    retention: {
        activity: '500mb_third_month_added',
        imageId: 'ic-bonus-retention',
        titleId: 'bonus_retention_name',
        configTitle: 'Retention Bonus',
        thirdParty: false
    },
    surprise: {
        activity: 'bonus_added',
        imageId: 'ic-bonus-surprise',
        titleId: 'bonus_surprise_name',
        configTitle: 'Surprise Bonus',
        thirdParty: false
    },
    portin: {
        activity: 'bonus_portin_added',
        imageId: 'ic-bonus-port-in',
        titleId: 'bonus_portin_name',
        configTitle: 'Port-In Bonus',
        thirdParty: false
    },
    promo_extra_portin: {
        activity: 'bonus_extra_portin_added',
        imageId: 'ic-bonus-port-in',
        titleId: 'bonus_extra_portin_name',
        configTitle: 'Port-In Extra Bonus',
        thirdParty: false
    },
    promo_gumtree: {
        activity: 'bonus_gumtree_added',
        imageId: 'ic-bonus-gumtree',
        titleId: 'bonus_gumtree_name',
        configTitle: 'Gumtree Bonus',
        thirdParty: true
    },
    promo_carousell: {
        activity: 'bonus_carousell_added',
        overrideBssNotification: true,
        ignoreBssNotification: true,
        imageId: 'ic-bonus-carousell',
        titleId: 'bonus_carousell_name',
        configTitle: 'Carousell Bonus',
        thirdParty: true
    },
    promo_paktor: {
        activity: 'bonus_paktor_added',
        overrideBssNotification: true,
        ignoreBssNotification: true,
        imageId: 'ic-bonus-paktor',
        titleId: 'bonus_paktor_name',
        configTitle: 'Paktor Bonus',
        thirdParty: true
    },
    promo_fave: {
        activity: 'bonus_fave_added',
        overrideBssNotification: true,
        ignoreBssNotification: true,
        imageId: 'ic-bonus-promo',
        titleId: 'bonus_fave_name',
        configTitle: 'Fave Bonus',
        thirdParty: true
    },
    promo_amex: {
        activity: 'bonus_amex_added',
        overrideBssNotification: true,
        ignoreBssNotification: true,
        imageId: 'ic-bonus-promo',
        titleId: 'bonus_amex_name',
        configTitle: 'Amex Bonus',
        thirdParty: true
    },
    promo_nrod: {
        activity: 'bonus_nrod_added',
        imageId: 'ic-bonus-nrod',
        titleId: 'bonus_nrod_name',
        configTitle: '#NROD Bonus',
        thirdParty: false
    },
    promo_lazada: {
        activity: 'bonus_lazada_added',
        imageId: 'ic-bonus-lazada-bonus-data',
        titleId: 'bonus_lazada_name',
        configTitle: 'Lazada Bonus',
        thirdParty: true
    },
    promo_contract_buster: {
        activity: 'bonus_breakup_added',
        imageId: 'ic-bonus-contract-buster',
        titleId: 'bonus_breakup_name',
        configTitle: 'Contract Break-Up Bonus',
        thirdParty: false
    },
    promo_national_day: {
        activity: 'bonus_national_day_added',
        imageId: 'ic-bonus-national-day',
        titleId: 'bonus_national_day_name',
        configTitle: 'National Day Bonus',
        thirdParty: false
    },
    promo_zalora: {
        activity: 'bonus_zalora_added',
        imageId: 'ic-bonus-zalora',
        titleId: 'bonus_zalora_name',
        configTitle: 'Zalora Bonus',
        thirdParty: true
    },
    promo_grab_rewards: {
        activity: 'bonus_grab_added',
        imageId: 'ic-bonus-grab-rewards',
        titleId: 'bonus_grab_rewards_name',
        configTitle: 'Grab Rewards Bonus',
        thirdParty: true
    },
    promo_grab: {
        activity: 'bonus_grab_added',
        imageId: 'ic-bonus-grab',
        titleId: 'bonus_grab_name',
        configTitle: 'Grab Driver Bonus',
        thirdParty: true
    },
    promo_uber_driver: {
        activity: 'bonus_uber_added',
        imageId: 'ic-bonus-uber',
        titleId: 'bonus_uber_drivers_name',
        configTitle: 'Uber Driver Bonus',
        thirdParty: true
    },
    promo_dbs: {
        activity: 'bonus_dbs_added',
        imageId: 'ic-bonus-dbs',
        titleId: 'bonus_dbs_name',
        configTitle: 'DBS Promo',
        thirdParty: true
    },
    promo_shopee: {
        activity: 'bonus_shopee_added',
        imageId: 'ic-bonus-promo',
        titleId: 'bonus_shopee_name',
        configTitle: 'Shopee Promo',
        thirdParty: true
    },
    promo_poems: {
        activity: 'bonus_poems_added',
        overrideBssNotification: true,
        ignoreBssNotification: true,
        imageId: 'ic-bonus-poems',
        titleId: 'bonus_poems_name',
        configTitle: 'POEMS Bonus',
        thirdParty: true
    },
    promo_standard_chartered: {
        activity: 'bonus_standard_chartered_added',
        overrideBssNotification: true,
        ignoreBssNotification: true,
        imageId: 'ic-bonus-standard-chartered',
        titleId: 'bonus_standard_chartered_name',
        configTitle: 'STANDCHART Bonus',
        thirdParty: true
    },
    promo_cis_default: {
        activity: 'bonus_cis_default_added',
        overrideBssNotification: true,
        ignoreBssNotification: true,
        imageId: 'ic-bonus-promo',
        titleId: 'bonus_cis_default_name',
        configTitle: 'CIS Default Bonus',
        thirdParty: true
    },
    promo_device_extra_data: {
        activity: 'bonus_purchased_device_added',
        overrideBssNotification: true,
        ignoreBssNotification: true,
        imageId: 'ic-bonus-device-extra-data',
        titleId: 'bonus_device_extra_data_name',
        configTitle: 'Purchased Device Bonus',
        thirdParty: false
    },
    promo_new_year: {
        activity: 'bonus_new_year_added',
        imageId: 'ic-bonus-new-year',
        titleId: 'bonus_new_year_name',
        configTitle: 'New Year Bonus',
        thirdParty: false
    },
    referral: {
        activity: 'bonus_referral_added',
        overrideBssNotification: false,
        ignoreBssNotification: true,
        imageId: 'ic-bonus-referral',
        titleId: 'bonus_referral_name',
        configTitle: 'Referral Bonus',
        thirdParty: false
    },
    promo_2017_july_giveaway_bonus: {
        activity: 'bonus_circles_life_birthday_added',
        overrideBssNotification: true,
        ignoreBssNotification: true,
        imageId: 'ic-bonus-birthday',
        titleId: 'bonus_circles_life_birthday_name',
        configTitle: 'Circles.Life Birthday Bonus',
        thirdParty: false
    },
    promo: {
        activity: 'bonus_promo_added',
        imageId: 'ic-bonus-promo',
        titleId: 'bonus_promo_name',
        configTitle: 'Promo Code Bonus',
        thirdParty: false
    }
}

var partners = {
    cis_general: {configTitle: 'CIS'},
    uber_drivers: {configTitle: 'Uber Drivers'},
    grab_driver: {configTitle: 'Grab Drivers'},
    grab_rewards: {configTitle: 'Grab Rewards'},
    perx: {configTitle: 'PERX'},
    gumtree: {configTitle: 'Gumtree'},
    poems: {configTitle: 'POEMS'},
    carousell: {configTitle: 'Carousell'},
    amex: {configTitle: 'Amex'},
    paktor: {configTitle: 'Paktor'},
    fave: {configTitle: 'Fave'},
    dbs: {configTitle: 'DBS Promo'},
    shopee: {configTitle: 'Shopee Promo'},
}

module.exports = {

    getCirclesInternalTypes: function () {
        var internalTypes = [];
        Object.keys(bonuses).forEach((key) => {
            if (!bonuses[key].thirdParty) {
                internalTypes.push(key);
            }
        });
        return internalTypes;
    },

    getPartnersTypes: function () {
        return partners;
    },

    getBonusTypes: function () {
        return bonuses;
    },

    getBonusInfo: function (type) {
        var info = bonuses[type];
        if (info) {
            info.type = type;
        } else {
            info = {
                type: 'default',
                activity: 'bonus_default_added',
                ignoreBssNotification: true,
                imageId: 'ic-bonus-promo',
                titleId: 'bonus_default_name'
            }
        }

        return info;
    }
}

   
