module.exports.words = [
    "anus",
    "ass",
    "asses",
    "assface",
    "assfuck",
    "assfucker",
    "asshat",
    "asshole",
    "bastard",
    "bitch",
    "bitchass",
    "bitches",
    "bitchtits",
    "bitchy",
    "blowjob",
    "bollocks",
    "bollox",
    "boner",
    "bullshit",
    "bumblefuck",
    "butt",
    "buttfucker",
    "cameltoe",
    "homosexual",
    "chode",
    "clit",
    "clusterfuck",
    "cock",
    "coochie",
    "cooter",
    "cum",
    "cunt",
    "damn",
    "dickhead",
    "dicks",
    "dike",
    "dildo",
    "dipshit",
    "doochbag",
    "douche",
    "douchebag",
    "dumbass",
    "dyke",
    "erection",
    "fag",
    "faggot",
    "fatass",
    "fuck",
    "fucker",
    "fucked",
    "fucking",
    "fucks",
    "gay",
    "goddamn",
    "hell",
    "hoe",
    "homo",
    "humping",
    "jackass",
    "jagoff",
    "jizz",
    "kooch",
    "lardass",
    "lesbo",
    "lesbian",
    "mothafucka",
    "motherfucker",
    "motherfucking",
    "nigger",
    "nutsack",
    "pecker",
    "peckerhead",
    "penis",
    "piss",
    "pissed",
    "prick",
    "pussies",
    "pussy",
    "queef",
    "queer",
    "shit",
    "shithead",
    "shithole",
    "shitter",
    "shitty",
    "skank",
    "slut",
    "slutbag",
    "tard",
    "testicle",
    "tits",
    "twat",
    "vagina",
    "whore"
]

module.exports.isAllowed = function(word) {
    var forbiddenDictionary = module.exports.words;
    if (!word || !forbiddenDictionary) {
        return true;
    }

    var lowCapsCode = word.toLowerCase();
    for (var i = 0; i < forbiddenDictionary.length; i++) {
        var word = forbiddenDictionary[i];
        if (lowCapsCode.indexOf(word) >= 0) {
            return false;
        }
    }
    return true;
}

module.exports.hasSpecialCharacters = function(word) {
    return !(/^[a-zA-Z0-9-]*$/.test(word));
}

module.exports.hasSpace = function(word) {
    return word.indexOf(" ") >= 0;
}


