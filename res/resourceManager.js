var stringsEn = require('./values/strings-en');
var common = require('../lib/common');

var updateCreditCardActionButton = {
    type: "BUTTON",
    text: "Update Now",
    link: "https://circlescare.app.link/settings-updatecreditcard"
}

var contactCSActionButton = {
    type: "BUTTON",
    text: "Contact Now",
    link: "https://circlescare.app.link/help"
}

var upgradeCirclesSwitchCSActionButton = {
    type: "BUTTON",
    text: "Sounds good",
    link: "https://circlescare.app.link/circlesswitch-upgrade"
}


module.exports = {

    /**
     *
     *
     */
    getString: function (id, lng) {
        if (!lng) lng = "en";

        if (stringsEn && stringsEn.text) {
            if (!stringsEn.text[id]) {
                common.error("ResourceManager", "Language file is not found, id=" + id);
                return undefined;
            }
            return stringsEn.text[id];
        } else {
            common.error("ResourceManager", "Language file is not found, id=" + id);
            return undefined;
        }
    },

    getErrorValues: function (err) {
        if (!err) return {code: 0};

        var code;
        var errorMessage;
        var errorTitle;
        var actions = [];

        if (err.status === "PAYMENT_PROCESSING"
            || err.status === "ERROR_ALL_PAYMENTS_DISABLED"
            || err.status === "ERROR_INVOICE_NOT_FOUND"
            || err.status === "ERROR_ALL_INVOICES_PAYMENTS_DISABLED"
            || err.status === "ERROR_PAYMENTS_BLOCKED_NOT_ALL_DELIVERED"
            || err.status === "ERROR_BAN_PAYMENTS_BLOCKED"
            || err.status === "ERROR_INVOICE_SYNC_FAILED") {
            errorTitle = module.exports.getString("dialog_title_warning");
            errorMessage = module.exports.getString("error_code_payment_processing");
            code = common.PAYMENT_PROCESSING_ERROR;
        } else if (err.status === "ERROR_UNFINISHED_TRANSACTION" || err.status === 'ERROR_UNFINISHED_TR') {
            errorTitle = module.exports.getString("dialog_title_payment_rejected");
            errorMessage = module.exports.getString("error_code_payment_last_transaction_failed");
            code = common.PAYMENT_ERROR_UNFINISHED_TRANSACTION;
            actions.push(contactCSActionButton);
        } else if (err.status === "ERROR_PAAS_TRANSACTION_FAILED") {
            errorTitle = module.exports.getString("dialog_title_error");
            errorMessage = module.exports.getString("error_code_payment_paas_transaction_failed");
            code = common.PAYMENT_ERROR;
            actions.push(updateCreditCardActionButton);
        }  else if (err.status === "PF_NOT_HONORED") {
            errorTitle = module.exports.getString("dialog_title_payment_rejected");
            errorMessage = module.exports.getString("error_code_payment_pf_not_honored");
            code = common.PAYMENT_ERROR_PF_NOT_HONORED;
            actions.push(updateCreditCardActionButton);
        } else if (err.status === "PF_LOST") {
            errorTitle = module.exports.getString("dialog_title_payment_rejected");
            errorMessage = module.exports.getString("error_code_payment_pf_lost");
            code = common.PAYMENT_ERROR_PF_LOST;
            actions.push(updateCreditCardActionButton);
        } else if (err.status === "PF_STOLEN") {
            errorTitle = module.exports.getString("dialog_title_payment_rejected");
            errorMessage = module.exports.getString("error_code_payment_pf_stolen");
            code = common.PAYMENT_ERROR_PF_STOLEN;
            actions.push(updateCreditCardActionButton);
        } else if (err.status === "PF_INSUFFICIENT_FUND") {
            errorTitle = module.exports.getString("dialog_title_payment_rejected");
            errorMessage = module.exports.getString("error_code_payment_pf_insufficient_fund");
            code = common.PAYMENT_ERROR_PF_INSUFFICIENT_FUND;
            actions.push(updateCreditCardActionButton);
        } else if (err.status === "PF_INVALID_CARD_INFORMATION") {
            errorTitle = module.exports.getString("dialog_title_payment_rejected");
            errorMessage = module.exports.getString("error_code_payment_pf_invalid_card_information");
            code = common.PAYMENT_ERROR_PF_INVALID_CARD_INFORMATION;
            actions.push(updateCreditCardActionButton);
        } else if (err.status === "PF_CARD_EXPIRED") {
            errorTitle = module.exports.getString("dialog_title_payment_rejected");
            errorMessage = module.exports.getString("error_code_payment_pf_card_expired");
            code = common.PAYMENT_ERROR_PF_CARD_EXPIRED;
            actions.push(updateCreditCardActionButton);
        } else if (err.status === "CUSTOMER_NOT_FOUND") {
            errorTitle = module.exports.getString("dialog_title_error");
            errorMessage = module.exports.getString("error_code_customer_not_found");
            code = common.CUSTOMER_NOT_FOUND;
        } else if (err.status === "ERROR_OPERATION_LOCKED") {
            errorTitle = module.exports.getString("dialog_title_warning");
            errorMessage = module.exports.getString("error_code_operation_is_locked");
            code = common.OPERATION_LOCKED;
        } else if (err.status === "ERROR_INVALID_PAYMENT_AMOUNT") {
            errorTitle = module.exports.getString("dialog_title_error");
            errorMessage = module.exports.getString("error_code_invalid_payment_amount");
            code = common.INVALID_PAYMENT_AMOUNT;
        } else if (err.status === "ERROR_OPEN_INVOICE_NOT_FOUND") {
            errorTitle = module.exports.getString("dialog_title_error");
            errorMessage = module.exports.getString("error_code_open_invoice_not_found");
            code = common.OPEN_INVOICE_NOT_FOUND;
        } else if (err.status === "ERROR_CREDIT_CAP_PAYMENT_NOT_ALLOWED"
            || err.status === "ERROR_CREDIT_CAP_PAYMENT_NOT_REQUIRED") {
            errorTitle = module.exports.getString("dialog_title_error");
            errorMessage = module.exports.getString("error_code_credit_cap_payment_not_allowed");
            code = common.OPEN_INVOICE_NOT_FOUND;
            actions.push(contactCSActionButton);
        } else if (err.status === "ERROR_RESTRICTED_ADDON_SUBSCRIPTION") {
            errorTitle = module.exports.getString("dialog_title_error");
            errorMessage = err.message;
            code = common.ERROR_RESTRICTED_ADDON_SUBSCRIPTION;
        } else if (err.status === "ERROR_CIRCLES_SWITCH_UPGRADE_REQUIRED") {
            errorTitle = module.exports.getString("dialog_title_want_more");
            errorMessage = module.exports.getString("error_code_circles_switch_upgrade_required");
            code = common.ERROR_CIRCLES_SWITCH_UPGRADE_REQUIRED;
            actions.push(upgradeCirclesSwitchCSActionButton);
        } else if (err.status === "ERROR_CIRCLES_SWITCH_UPGRADE_NOT_ALLOWED") {
            errorTitle = module.exports.getString("dialog_title_thanks_for_trying_circles_switch");
            errorMessage = module.exports.getString("error_code_circles_switch_upgrade_not_allowed");
            code = common.ERROR_CIRCLES_SWITCH_UPGRADE_NOT_ALLOWED;
        }  else if (err.status === "ERROR_JOB_CREATION_LOCKED" || err.status === "ERROR_OTHER_JOB_IS_ACTIVE") {
            errorTitle = module.exports.getString("dialog_title_warning");
            errorMessage = module.exports.getString("error_code_action_processing");
            code = common.ERROR_PROCESSING_ANOTHER_ACTION;
        } else {
            errorTitle = module.exports.getString("dialog_title_error");
            errorMessage = module.exports.getString("error_code_common");
            code = -1;
        }

        return {
            code: code,
            description: errorMessage,
            title: errorTitle,
            details: err.message,
            actions: actions
        };
    },

    getErrorValuesForCS: function (err) {
        if (!err) return {code: 0};

        var code;
        var errorMessage;
        var errorTitle;

        if (err.status === "PAYMENT_PROCESSING") {
            errorTitle = module.exports.getString("dialog_title_warning");
            errorMessage = module.exports.getString("error_code_payment_processing");
            code = common.PAYMENT_PROCESSING_ERROR;
        } else if (err.status === "ERROR_PAAS_TRANSACTION_FAILED") {
            errorTitle = module.exports.getString("dialog_title_error");
            errorMessage = module.exports.getString("error_code_payment_paas_transaction_failed");
            code = common.PAYMENT_ERROR;
        }  else if (err.status === "PF_NOT_HONORED") {
            errorTitle = module.exports.getString("dialog_title_payment_rejected");
            errorMessage = module.exports.getString("error_code_payment_pf_not_honored_cs");
            code = common.PAYMENT_ERROR_PF_NOT_HONORED;
        } else if (err.status === "PF_LOST") {
            errorTitle = module.exports.getString("dialog_title_payment_rejected");
            errorMessage = module.exports.getString("error_code_payment_pf_lost_cs");
            code = common.PAYMENT_ERROR_PF_LOST;
        } else if (err.status === "PF_STOLEN") {
            errorTitle = module.exports.getString("dialog_title_payment_rejected");
            errorMessage = module.exports.getString("error_code_payment_pf_stolen_cs");
            code = common.PAYMENT_ERROR_PF_STOLEN;
        } else if (err.status === "PF_INSUFFICIENT_FUND") {
            errorTitle = module.exports.getString("dialog_title_payment_rejected");
            errorMessage = module.exports.getString("error_code_payment_pf_insufficient_fund_cs");
            code = common.PAYMENT_ERROR_PF_INSUFFICIENT_FUND;
        } else if (err.status === "PF_INVALID_CARD_INFORMATION") {
            errorTitle = module.exports.getString("dialog_title_payment_rejected");
            errorMessage = module.exports.getString("error_code_payment_pf_invalid_card_information_cs");
            code = common.PAYMENT_ERROR_PF_INVALID_CARD_INFORMATION;
        } else if (err.status === "PF_CARD_EXPIRED") {
            errorTitle = module.exports.getString("dialog_title_payment_rejected");
            errorMessage = module.exports.getString("error_code_payment_pf_card_expired_cs");
            code = common.PAYMENT_ERROR_PF_CARD_EXPIRED;
        } else if (err.status === "CUSTOMER_NOT_FOUND") {
            errorTitle = module.exports.getString("dialog_title_error");
            errorMessage = module.exports.getString("error_code_customer_not_found");
            code = common.CUSTOMER_NOT_FOUND;
        } else if (err.status === "ERROR_UNFINISHED_TRANSACTION" || err.status === 'ERROR_UNFINISHED_TR') {
            errorTitle = module.exports.getString("dialog_title_payment_rejected");
            errorMessage = module.exports.getString("error_code_payment_last_transaction_failed");
            code = common.PAYMENT_ERROR_UNFINISHED_TRANSACTION;
        } else {
            errorTitle = module.exports.getString("dialog_title_error");
            errorMessage = module.exports.getString("error_code_common");
            code = -1;
        }

        return {
            code: code,
            description: errorMessage,
            title: errorTitle,
            details: err.message
        };
    }
}
