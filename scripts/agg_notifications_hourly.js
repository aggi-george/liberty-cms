#!/usr/bin/env nodejs

var db = require('../lib/db_handler');

args = []
process.argv.forEach(function (val, index, array)
{
    args.push(val);
});
if (args[2]) {
        common.log("Syntax", "./agg_notifications_hourly.js");
        exit(1);
}

var project = { "$project" : { 	"stamp" : { "$dateToString" : { "format" : "%Y-%m-%dT%H:00:00.000Z", "date" : { "$add" : [ new Date(0), "$ts" ] } } },
				"activity" : 1, 
				"teamName" : 1, 
				"sms" : { "$setIsSubset" : [ "$transport", [ "sms" ] ] },
				"email" : { "$setIsSubset" : [ "$transport", [ "email" ] ] },
				"selfcare" : { "$setIsSubset" : [ "$transport", [ "selfcare" ] ] },
				"gentwo" : { "$setIsSubset" : [ "$transport", [ "gentwo" ] ] },
				"ussd" : { "$setIsSubset" : [ "$transport", [ "ussd" ] ] }
		} };
var group = { "$group" : { "_id" : {	"stamp" : "$stamp",
					"activity" : "$activity",
					"teamName" : "$teamName",
					"sms" : "$sms",
					"email" : "$email",
					"selfcare" : "$selfcare",
					"gentwo" : "$gentwo",
					"ussd" : "$ussd" },
			"count" : { "$sum" : 1 }
		} };

var combine = { "$group" : { "_id" : "$_id.stamp", "groups" : { "$addToSet" : { "activity" : "$_id.activity", 
										"teamName" : "$_id.teamName", 
										"sms" : "$_id.sms",
										"email" : "$_id.email",
										"selfcare" : "$_id.selfcare",
										"gentwo" : "$_id.gentwo",
										"ussd" : "$_id.ussd",
										"count" : "$count"
		} } } };
	
var count = 0;

function agg(match, callback) {
	db.notifications_logbook.aggregate([ match, project, group, combine ]).toArray(function (err, result) {
		if ( !err && result && (result.length > 0) ) {
			result[0].ts = new Date(result[0]._id).getTime();
			delete result[0]._id;
			db.agg.updateOne({ "ts" : result[0].id }, result[0], { "upsert" : true }, function (error, res) {
				callback();
			});
		} else {
			callback();
		}
	}); 
}

setTimeout(function () {
//	var start = new Date("2016-01-20T00:00:00.000Z").getTime();
//	var end = new Date("2016-02-18T00:00:00.000Z").getTime();
	var hour = new Date().toISOString().split(":")[0] + ":00:00.000Z";
	var end =  new Date(hour).getTime();
	var start = end - (60 * 60 * 1000);
	var match = { "$match" : { "transport" : { "$ne" : null } } };
	for (var i=start; i<end; i=i+(60 * 60 * 1000)) {	// 1 hour
		count++;
		match["$match"].ts = { "$gte" : i, "$lt" : i + (60 * 60 * 1000) };
		agg(match, function () {
			count--;
			if (count==0) process.exit(0);
		});
	}
}, 1000);
