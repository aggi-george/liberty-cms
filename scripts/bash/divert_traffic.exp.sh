#!/usr/bin/expect -f
log_user 0
set stty_init "rows 1000 cols 200"
set adminuser {bypass}
set prompt {$ }
set host [lindex $argv 0]
set traffic [lindex $argv 1]
set command "sudo \$HOME/bypass_traffic.sh $traffic"
set timeout 10

spawn ssh -q -o StrictHostKeyChecking=no $adminuser@$host

sleep 2
expect $prompt
send "$command\r"
expect $prompt
send "exit\r"
close
