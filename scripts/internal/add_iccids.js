#!/usr/bin/env nodejs

var async = require('async');
var fs = require('fs');

var config = require('../../config');
//config.DEV = false;
config.EC_LOGSENABLED = false;
config.LOGSENABLED = false;

var db = require('../../lib/db_handler');

function readFile(callback) {
    try {
        var input = fs.createReadStream("iccids.txt");
    } catch (e) {
        console.error(e.message);
        process.exit(-1);
    }

    var remaining = '';
    var array = [];

    input.on('data', function (data) {

        remaining += data;
        var index = remaining.indexOf('\n');
        while (index > -1) {
            var line = remaining.substring(0, index);
            remaining = remaining.substring(index + 1);
            array.push({iccid: line});
            index = remaining.indexOf('\n');
        }
    });

    input.on('end', function () {

        if (remaining.length > 0) {
            array.push({iccid: remaining.trim()});
        }

        if (callback) {
            callback(undefined, array);
        }
    });
}

function addIccids(iccids, callback) {
    var addTask = function (tasks, iccid) {
        tasks.push(function (callback) {
            db.iccidPool.insert({_id: iccid}, function () {
                callback()
            });
        });
    }

    var tasks = [];
    iccids.forEach(function (item) {
        addTask(tasks, item.iccid);
    });

    console.log("adding new ICCIDs... count=" + tasks.length);
    async.parallelLimit(tasks, 20, function (err, result) {
        console.log("added ICCIDs");
        if (callback) callback();
    });
}

console.log("init...");
setTimeout(function () {

    console.log("loading ICCIDs...");
    readFile(function(err, list) {
        if (err) {
            console.error(err.message);
            process.exit(-1);
        }

        addIccids(list, function() {
            process.exit(0);
        });
    });
}, 2500);

