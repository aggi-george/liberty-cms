#!/usr/bin/env nodejs
require('../../bin/global');
console.log("Initializing...");
var async = require('async');
var fs = require('fs');
var config = require('../../config');

config.EC_LOGSENABLED = false;
config.LOGSENABLED = false;

var ec = require('../../lib/elitecore');
var db = require('../../lib/db_handler');
var common = require('../../lib/common');

var classManager = require('../../src/core/manager/analytics/classManager')
var bonusManager = require('../../src/core/manager/bonus/bonusManager')

var recurrent = true;
var future = false;
var ignoreDuplicates = true;
var delay = 3000;
var progressOn = 50;
var parallelLimit = 5;
var timeoutDelay = 30 * 1000;
var toDate = undefined;
var addonType = "general";
var addonName = "IMESSAGE_ADDON";
var classType = "ACCOUNT_ACTIVE";
var key = config.NOTIFICATION_KEY;

var countProcessed = 0;
var addBonusProduct = (sin, addon, cl) => {
    new Promise(
        (resolve, reject) => {
            var logProgress = (type) => {
                countProcessed++;
                if (countProcessed % progressOn == 0) {
                    common.log("[ADD_ADDON_SCRIPT]", "Processed (" + type + ") " +
                    countProcessed + " customers, sin=" + sin);
                }
            }

            var timeout = setTimeout(function () {
                logProgress("timeout");
                common.error("[ADD_ADDON_SCRIPT]", "Failed to process customer, timeout, sin=" + sin);
                reject(new Error("Timeout, " + sin));
            }, timeoutDelay);

            ec.getCustomerDetailsService(sin, true, (err, cache) => {
                if (err) {
                    clearTimeout(timeout);
                    logProgress("error");
                    common.error("[ADD_ADDON_SCRIPT]", "Failed load load history, sin=" + sin + ", error=" + err.message);
                    return reject(err);
                }

                if (!cache) {
                    clearTimeout(timeout);
                    logProgress("error");
                    common.error("[ADD_ADDON_SCRIPT]", "Failed load customer data");
                    return reject(new Error("Failed to load customer data"));
                }


                var found;
                if (future) {
                    found = cache.serviceInstanceFutureAddons[addonType].some((item) => {
                        if (item.name == addonName) {
                            return true;
                        }
                    });
                } else {
                    found = cache.serviceInstanceCurrentAddons[addonType].some((item) => {
                        if (item.name == addonName) {
                            return true;
                        }
                    });
                }

                if (found && ignoreDuplicates) {
                    clearTimeout(timeout);
                    logProgress("skipped");
                    return resolve({added: false, skipped: true});
                }

                //ignore time and specify the last sec of the day in SGT
                toDate = toDate ? new Date(toDate.setHours(15, 59, 59, 0)) : toDate;

                var sub = {
                    product_id: addon.id,
                    recurrent: recurrent,
                    effect: future ? 2 : 0,
                    toDate: toDate
                };

                ec.addonSubscribe(cache.number, sin, [sub], cache.billing_cycle, (err, result) => {
                    if (err) {
                        clearTimeout(timeout);
                        logProgress("error");
                        common.error("[ADD_ADDON_SCRIPT]", "Failed subscribe addon, sin=" + sin + ", error=" + err.message);
                        return reject(err);
                    }

                    clearTimeout(timeout);
                    ec.setCache(cache.number, cache.prefix, undefined, cache.customerAccountNumber, () => {
                        setTimeout(() => {
                            countProcessed++;
                            if (countProcessed % progressOn == 0) {
                                common.log("[ADD_ADDON_SCRIPT]", "Processed (finished) " + countProcessed + " customers, sin=" + sin);
                            }

                            resolve({added: true});
                        }, delay);
                    });
                });
            });
        }).then(function (result) {
            cl(undefined, result);
        }).catch(function (err) {
            cl(undefined, {added: false, error: err.message});
        });
}

setTimeout(function () {

    var addon = ec.findPackageName(ec.packages(), addonName);
    if (!addon || !addon.id) {
        common.error("[ADD_ADDON_SCRIPT]", "Failed load package " + addonName);
        process.exit(0);
    }

    classManager.loadClasses(undefined, (err, result) => {
        if (err) {
            common.error("[ADD_ADDON_SCRIPT]", "Failed load customers, error=" + err.message);
            process.exit(0);
        }

        var found;
        result.data.some((item) => {
            if (item.type == classType) {
                found = item;
                return true;
            }
        });

        if (!found || !found.id) {
            common.error("[ADD_ADDON_SCRIPT]", "Failed find class, error='No class found with type " + classType + "'");
            process.exit(0);
        }

        classManager.loadCustomers(undefined, found.id, (err, result) => {
            if (err) {
                common.error("[ADD_ADDON_SCRIPT]", "Failed load customers, error=" + err.message);
                process.exit(0);
            }
            common.log("[ADD_ADDON_SCRIPT]", "Loaded customer, count=" + result.data.length);

            var asyncResults = [];
            var taskWorker = (job, callback) => {
                addBonusProduct(job.sin, addon, (err, result) => {
                    asyncResults.push(result);
                    callback(err, result);
                });
            }

            var taskResultHandler = (err) => {
                if (q.isKilled) return;
                if (err) {
                    q.isKilled = true;
                    q.kill();
                    taskDrainHandler(err);
                }
            }

            var taskDrainHandler = (err) => {
                if (err) {
                    common.error("[ADD_ADDON_SCRIPT]", "Failed to process customers, error=" + err.message);
                    process.exit(0);
                }

                var countInvalidJoinDate = 0;
                var countSkipped = 0;
                var countAdded = 0;
                var countErrors = 0;
                var countTotal = 0;

                if (asyncResults) asyncResults.forEach((item) => {
                    countTotal++;
                    if (item.added) {
                        countAdded++;
                    }
                    if (item.skipped) {
                        countSkipped++;
                    }
                    if (item.invalidJoinDate) {
                        countInvalidJoinDate++;
                    }
                    if (item.error) {
                        countErrors++;
                        common.error("[ADD_ADDON_SCRIPT]", "Processed customer error=" + item.error);
                    }
                })

                common.log("[ADD_ADDON_SCRIPT]", "Processed customer, countAdded=" + countAdded + ", countTotal="
                + countTotal + ", countSkipped=" + countSkipped + ", countErrors=" + countErrors +
                ", countInvalidJoinDate=" + countInvalidJoinDate);

                process.exit(0);
            }

            var q = async.queue(taskWorker, parallelLimit);
            q.drain = taskDrainHandler;

            result.data.forEach((item, idx) => {
                if (idx < 52000) {
                    return;
                }

                var sin = item.serviceInstanceNumber;
                q.push({sin: sin}, taskResultHandler);
            });
        });
    });

}, 10000);
