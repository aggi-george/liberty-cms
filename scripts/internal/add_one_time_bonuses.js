#!/usr/bin/env nodejs
require('../../bin/global');
console.log("Initializing...");
var async = require('async');
var fs = require('fs');
var config = require('../../config');

config.EC_LOGSENABLED = false;
config.LOGSENABLED = false;

var ec = require('../../lib/elitecore');
var db = require('../../lib/db_handler');
var common = require('../../lib/common');

var classManager = require('../../src/core/manager/analytics/classManager')
var bonusManager = require('../../src/core/manager/bonus/bonusManager')

var recurrent = true;
var ignoreDuplicates = true;
var delay = 1000;
var progressOn = 10;
var parallelLimit = 10;
var timeoutDelay = 30 * 1000;
var toDate = new Date("2017-12-31T15:59:59");
var joinedBefore = new Date("2017-06-30T15:59:59");
var bonusType = "promo_2017_july_giveaway_bonus";
var bonusName = "Bonus 1GB Giveaway 07.2017";
var bonusActivity = "bonus_circles_life_birthday_added";
var classType = "ACCOUNT_ACTIVE"; // "ACCOUNT_BETA";
var key = config.NOTIFICATION_KEY;

var countProcessed = 0;
var addBonusProduct = (sin, bonus, cl) => {
    new Promise(
        (resolve, reject) => {
            var logProgress = (type) => {
                countProcessed++;
                if (countProcessed % progressOn == 0) {
                    common.log("[ADD_BONUS_SCRIPT]", "Processed (" + type + ") " +
                    countProcessed + " customers");
                }
            }

            var timeout = setTimeout(function () {
                logProgress("timeout");
                common.error("[ADD_BONUS_SCRIPT]", "Failed to process customer, timeout, sin=" + sin);
                reject(new Error("Timeout, " + sin));
            }, timeoutDelay);

            bonusManager.loadHistoryBonusListBySI(sin, true, (err, result) => {
                if (err) {
                    clearTimeout(timeout);
                    logProgress("error");
                    common.error("[ADD_BONUS_SCRIPT]", "Failed load load history, error=" + err.message);
                    return reject(err);
                }

                var found = result.some((item) => {
                    if (item.product_type == bonusType) {
                        return true;
                    }
                });

                var addBonusHandler = () => {
                    if (found && ignoreDuplicates) {
                        clearTimeout(timeout);
                        logProgress("skipped");
                        return resolve({added: false, skipped: true});
                    }

                    var addHistoryItemHandler = (err, result) => {
                        if (err) {
                            clearTimeout(timeout);
                            logProgress("error");
                            common.error("[ADD_BONUS_SCRIPT]", "Failed to subscribe bonus, error=" + err.message);
                            return reject(err);
                        }

                        clearTimeout(timeout);
                        setTimeout(() => {
                            countProcessed++;
                            if (countProcessed % progressOn == 0) {
                                common.log("[ADD_BONUS_SCRIPT]", "Processed (finished) " +
                                countProcessed + " customers");
                            }

                            resolve({added: true});
                        }, delay);
                    }

                    if (recurrent) {
                        bonusManager.addSingleRecurrentBonus(sin, bonus.id, toDate, bonusType,
                            bonusActivity, key, {}, addHistoryItemHandler);
                    } else {
                        bonusManager.addSingleNonRecurrentBonus(sin, bonus.id, bonusType,
                            bonusActivity, key, {}, addHistoryItemHandler);
                    }

                }

                if (!joinedBefore) {
                    return addBonusHandler();
                }

                db.query_err("SELECT orderrefno FROM ecAccounts WHERE serviceinstanceaccount=" + db.escape(sin),
                    (err, result) => {
                        if (err) {
                            clearTimeout(timeout);
                            logProgress("error");
                            common.error("[ADD_BONUS_SCRIPT]", "Failed to load customer ORN, error=" + err.message);
                            return reject(err);
                        }
                        if (!result || !result[0]) {
                            clearTimeout(timeout);
                            logProgress("error");
                            common.error("[ADD_BONUS_SCRIPT]", "Failed to load customer details from db");
                            return reject(new Error("Failed to load customer details from db"));
                        }

                        if (!result[0].orderrefno) {
                            // old customer
                            return addBonusHandler();
                        }

                        var orn = result[0].orderrefno;

                        db.notifications_logbook_get({
                            order_reference_number: orn,
                            activity: "payment_successful"
                        }, {}, {"ts": -1}, function (logs) {
                            var joinDate = (logs && logs.length > 0) ? new Date(logs[0].ts) : undefined;
                            if (!joinDate) {
                                // old customer
                                return addBonusHandler();
                            }

                            if (joinedBefore.getTime() < joinDate.getTime()) {
                                clearTimeout(timeout);
                                logProgress("invalid_join_date");
                                return resolve({added: false, invalidJoinDate: true});
                            }

                            return addBonusHandler();
                        });
                    });
            });
        }).then(function (result) {
            cl(undefined, result);
        }).catch(function (err) {
            cl(undefined, {added: false, error: err.message});
        });
}

setTimeout(function () {

    var bonus = ec.findPackageName(ec.packages(), bonusName);
    if (!bonus || !bonus.id) {
        common.error("[ADD_BONUS_SCRIPT]", "Failed load bonus package");
        process.exit(0);
    }

    classManager.loadClasses(undefined, (err, result) => {
        if (err) {
            common.error("[ADD_BONUS_SCRIPT]", "Failed load customers, error=" + err.message);
            process.exit(0);
        }

        var found;
        result.data.some((item) => {
            if (item.type == classType) {
                found = item;
                return true;
            }
        });

        if (!found || !found.id) {
            common.error("[ADD_BONUS_SCRIPT]", "Failed find class, error='No class found with type " + classType + "'");
            process.exit(0);
        }

        classManager.loadCustomers(undefined, found.id, (err, result) => {
            if (err) {
                common.error("[ADD_BONUS_SCRIPT]", "Failed load customers, error=" + err.message);
                process.exit(0);
            }
            common.log("[ADD_BONUS_SCRIPT]", "Loaded customer, count=" + result.data.length);

            var asyncResults = [];
            var taskWorker = (job, callback) => {
                addBonusProduct(job.sin, bonus, (err, result) => {
                    asyncResults.push(result);
                    callback(err, result);
                });
            }

            var taskResultHandler = (err) => {
                if (q.isKilled) return;
                if (err) {
                    q.isKilled = true;
                    q.kill();
                    taskDrainHandler(err);
                }
            }

            var taskDrainHandler = (err) => {
                if (err) {
                    common.error("[ADD_BONUS_SCRIPT]", "Failed to process customers, error=" + err.message);
                    process.exit(0);
                }

                var countInvalidJoinDate = 0;
                var countSkipped = 0;
                var countAdded = 0;
                var countErrors = 0;
                var countTotal = 0;

                if (asyncResults) asyncResults.forEach((item) => {
                    countTotal++;
                    if (item.added) {
                        countAdded++;
                    }
                    if (item.skipped) {
                        countSkipped++;
                    }
                    if (item.invalidJoinDate) {
                        countInvalidJoinDate++;
                    }
                    if (item.error) {
                        countErrors++;
                        common.error("[ADD_BONUS_SCRIPT]", "Processed customer error=" + item.error);
                    }
                })

                common.log("[ADD_BONUS_SCRIPT]", "Processed customer, countAdded=" + countAdded + ", countTotal="
                + countTotal + ", countSkipped=" + countSkipped + ", countErrors=" + countErrors +
                ", countInvalidJoinDate=" + countInvalidJoinDate);

                process.exit(0);
            }

            var q = async.queue(taskWorker, parallelLimit);
            q.drain = taskDrainHandler;

            result.data.forEach((item, idx) => {
                //if (idx > 0) {
                //    return;
                //}

                var sin = item.serviceInstanceNumber;
                q.push({sin: sin}, taskResultHandler);
            });
        });
    });

}, 2000);
