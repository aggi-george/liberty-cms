#!/usr/bin/env nodejs
// this script fixes current undersub and fixes future oversub and undersub
require('../../bin/global');
const async = require('async');
const db = require(`${__lib}/db_handler`);
const config = require(`${__base}/config`);
const subscriptionManager = require(`${__core}/manager/bonus/subscriptionManager`);
const testers = require(`${__base}/tests/testers`).testers;

const DEBUG = false;
const PROGRESS = true;
const ocsSchema = (config.EC_STAGING) ? 'crestelocstestlwp' : 'crestelocslwp';
const pmSchema = (config.EC_STAGING) ? 'crestelpmtestlwp' : 'crestelpmlwp';

const SG_PREFIX = '65';
let oversub = new Array;
let undersub = new Array;
let zero = new Array;
let unattached = new Array;
let queueLength = 0;

let args = new Array;
process.argv.forEach(function (val, index, array) {
    args.push(val);
});

const fix = parseInt(args[2]);
const future = parseInt(args[3]);

let bonusQ = async.queue(function(task, callback) {
    const action = function(cb) {
        if (fix && !future) subscriptionManager.updateCurrentBillBonuses(SG_PREFIX, task.number, '[CMS][script]', undefined, cb);
        else if (fix && future) subscriptionManager.updateNextBillBonuses(SG_PREFIX, task.number, '[CMS][script]', undefined, cb);
        else cb();
    }
    action(function(bErr, bResult) {
        const item = {
            serviceInstanceNumber: task.serviceInstanceNumber,
            bucket: task.bucket,
            history: task.history,
            number: task.number,
            error: bErr,
            result: bResult,
        }
        const qOut = fix && future ? `oversubItem|${JSON.stringify(item)}` : `undersubItem|${JSON.stringify(item)}`;
        const qPush = fix && future ? oversub : undersub;
        console.log(qOut);
        qPush.push(item);
        if (PROGRESS) console.log(`progress|${parseInt(qPush.length/queueLength*100)}`);
        setImmediate(() => { callback(); });
    });
}, 5);

const d = new Date();
const dateTime = new Date(d.getFullYear(), d.getMonth() + 1, 1).toISOString().slice(0, 19).replace('T', ' ');
const historyCondition = future ?
    `h.added_ts <= ${db.escape(dateTime)} AND h.deactivated = 0 AND (h.display_until_ts > ${db.escape(dateTime)} OR h.display_until_ts = 0)` :
    'h.added_ts < NOW() AND h.deactivated = 0 AND (h.display_until_ts > NOW() OR h.display_until_ts = 0)';
const oracleCondition = future ?
    `TO_CHAR(b.validfromdate, 'YYYY-MM-DD HH24:MI:SS') <= ${db.escape(dateTime)} AND TO_CHAR(b.validtodate, 'YYYY-MM-DD HH24:MI:SS') > ${db.escape(dateTime.slice(0,-1)+'1')}` :
    'b.validfromdate < SYSDATE AND b.validtodate > SYSDATE';

const queryHistory = `SELECT h.service_instance_number, a.emailid, a.joindate,
    (SELECT subscriberidentifier FROM ecNetworkServiceInstance WHERE
    serviceinstanceaccount=a.serviceinstanceaccount AND numberstatus='Active'
    ORDER BY lastmodifieddate DESC LIMIT 1) subscriberidentifier,
    SUM(h.bonus_kb_pretty)/1000000 pretty FROM
    historyBonuses h LEFT JOIN ecAccounts a ON a.serviceinstanceaccount=h.service_instance_number WHERE
    ${historyCondition}
    GROUP BY h.service_instance_number`;

const queryOracle = `SELECT s.subscriberidentifier, SUM(s.bucket) bucket FROM (SELECT b.customerbalanceid, b.customerid,
    b.totalusagevalue/1024/1024/1024 bucket, p.productid, c.subscriberidentifier, h.name FROM
    tblcustomerbalance b LEFT JOIN ${ocsSchema}.tblcustomer c ON c.customerid=b.customerid AND substr(c.subscriberidentifier,0,2) ='LW',
    ${pmSchema}.tblmproduct p LEFT JOIN ${pmSchema}.tblmproducthierarchyrel hr ON p.productid=hr.productid LEFT JOIN
    ${pmSchema}.tblmproducthierarchy h ON hr.hierarchyid=h.prodhierarchyid WHERE
    h.name='Bonus' AND c.customerstatusid=1 AND CONCAT('PRD00',b.packageid)=p.productid AND
    ${oracleCondition} ) s
    GROUP BY s.subscriberidentifier ORDER BY s.subscriberidentifier`;

if (DEBUG) {
    console.log(queryOracle);
    console.log(queryHistory);
}

const start = new Date();
async.parallel({
            bucket: function(cb) {
                if (PROGRESS) console.log('progress|1');
                db.oracle.query(queryOracle, { resultSet: true }, cb);
            },
            history: function(cb) {
                if (PROGRESS) console.log('progress|2');
                db.query_err(queryHistory, cb);
            }
        }, function(e,r) {
    if (PROGRESS) console.log('progress|3');
    const getHistory = function(serviceInstanceNumber) {
        return r.history.filter(function(o) {
            if (o.service_instance_number == serviceInstanceNumber) return o;
            else return undefined;
        })[0];
    }
    if (e) {
        console.error(e);
        process.exit(1);
    }
    if (!r || !r.bucket || !r.bucket.rows) {
        console.error('ec buckets empty list');
        process.exit(1);
    }
    console.log(`bucket|${r.bucket.rows.length}`);
    console.log(`history|${r.history.length}`);
    r.bucket.rows.forEach(function(b) {
        let history = getHistory(b[0]);
        if (!history) {
            if (b[1] != 0) {
                const item = {
                    serviceInstanceNumber: b[0],
                    bucket: b[1],
                    history: 0,
                }
                console.log(`oversubItem|${JSON.stringify(item)}`);
                let qPush = oversub;
                if (fix && future) {
                    qPush = bonusQ;
                    queueLength++;
                }
                qPush.push(item);
            } else {
                zero.push({
                    serviceInstanceNumber: b[0],
                    bucket: b[1],
                    history: 0,
                });
            }
        } else if ( (testers.indexOf(history.emailid) == -1) ||
                (history.joindate == null) ) {
            const bucket = Math.ceil(b[1] * 10) / 10;
            if (history.subscriberidentifier == null) {
                unattached.push({
                    serviceInstanceNumber: b[0],
                    bucket: bucket,
                    history: history.pretty,
                });
            } else if (bucket > history.pretty) {
                const item = {
                    serviceInstanceNumber: b[0],
                    bucket: bucket,
                    history: history.pretty,
                    email: history.emailid,
                    number: history.subscriberidentifier,
                }
                console.log(`oversubItem|${JSON.stringify(item)}`);
                let qPush = oversub;
                if (fix && future) {
                    qPush = bonusQ;
                    queueLength++;
                }
                qPush.push(item);
            } else if (bucket < history.pretty) {
                let qPush = bonusQ;
                if (fix && future) {
                    qPush = undersub;
                } else {
                    queueLength++;
                }
                qPush.push({
                    number: history.subscriberidentifier,
                    serviceInstanceNumber: b[0],
                    bucket: bucket,
                    history: history.pretty,
                });
            }
        }
    })
    console.log(`zero|${zero.length}`);
    console.log(`unattached|${unattached.length}`);
    let qOut = `undersub|${undersub.length}`;
    if (fix && future) {
        qOut = `oversub|${oversub.length}`;
        console.log(`undersub|${undersub.length}`);
    } else {
        console.log(`oversub|${oversub.length}`);
    }
    if (bonusQ.length() === 0) {
        console.log(qOut);
        if (PROGRESS) console.log(`progress|100`);
        console.log(`time|${(new Date()-start)}`);
        process.exit(0);
    } else bonusQ.drain = function() {
        if (fix && future) {
            console.log(`oversub|${oversub.length}`);
        } else {
            console.log(`undersub|${undersub.length}`);
        }
        console.log(`time|${(new Date()-start)}`);
        process.exit(0);
    }
});
