#!/usr/bin/env nodejs

var async = require('async');
var fs = require('fs');

var config = require('../../config');
//config.DEV = false;
config.EC_LOGSENABLED = false;
config.LOGSENABLED = false;

var db = require('../../lib/db_handler');
var ec = require('../../lib/elitecore');
var elitecoreConnector = require('../../lib/manager/ec/elitecoreConnector');
var elitecoreUserService = require('../../lib/manager/ec/elitecoreUserService');

var lastExecutionTime = -1;

elitecoreConnector.addStatsUpdateListener(function(result, allRequestsStats) {
    if (result && result.length > 0) {
        var nowMs = new Date().getTime();
        result.forEach(function (item) {
            item.ts = nowMs;
        });

        if (allRequestsStats && allRequestsStats.executionTime) {
            lastExecutionTime = allRequestsStats.executionTime;
        }
    }
});

function loadSelfcareNumbers(callback) {
    var query = "select customername, subscriberidentifier, serviceinstanceaccount, customeraccount, billingaccount, " +
        "serviceaccount, emailid, birthday from CRESTELOCSLWP.CUSTOMER_DETAIL_MV";

    db.oracle.query(query, function (err, results) {

        var list = [];
        if (results) {
            results.rows.forEach(function (item) {
                var prefix = "65";
                var number = item[1];
                var sin = item[2];
                list.push({prefix: prefix, number: number, sin: sin});
            });
        }

        callback(undefined, list);
    });
}

console.log("init...");
setTimeout(function () {
    var index = 2000;
    var requestNum = 0;
    var handledCount = 0;
    var accountsCount = 0;
    var perMinNum = 300;
    var incrementNum = 200;

    var singleShot = false;
    var singleShotCount = 300;

    var start = new Date().getTime();
    setInterval(function () {
        console.log("firing requests... requestsCount=" + requestNum + ", accountsCount=" + accountsCount +
        ", responsesCount=" + handledCount + ", requestsPerMinCount=" + perMinNum +
        ", timeFromStart=" + parseInt(((new Date().getTime() - start) / 1000)) +
        ", ecRequestCount=" + elitecoreConnector.getCountRequests() +
        ", ecTimeoutsCount=" + elitecoreConnector.getCountTimeouts() +
        ", ecExecutionTime=" + lastExecutionTime);
    }, 2000);

    loadSelfcareNumbers(function (err, list) {
        if (!list) {
            process.exit(-1);
        }

        console.log("caching... length=" + list.length + ", index=" + index);
        var startTime = new Date().getTime();

        if (singleShot) {
            console.log("firing requests... singleShot=" + singleShot + ", singleShotCount=" + singleShotCount);
            for (var i = 0; i < singleShotCount; i++) {
                while (index >= list.length || !list[index] || !list[index].number) {
                    console.log("re-init index... length=" + list.length);
                    index = parseInt(list.length * 0.1);
                }

                var number = list[index].number;
                var sin = list[index].sin;
                index++;

                elitecoreUserService.loadUserInfoByServiceInstanceNumber(sin, function (err, cache) {
                    handledCount++;

                    if (cache) {
                        accountsCount++;
                    }

                    if (handledCount == singleShotCount) {
                        setTimeout(function() {
                            console.log("caching finished...");
                            process.exit(0);
                        }, 2100);
                    }
                }, true)
            }
            console.log("firing requests finished...");
        } else {

            var apiCaller = function () {
                while (index >= list.length || !list[index] || !list[index].number) {
                    console.log("re-init index... length=" + list.length);
                    index = parseInt(list.length * 0.1);
                }

                var number = list[index].number;
                var sin = list[index].sin;
                //console.log("fire a request...  num=" + requestNum + ", number=" + number);

                index++;
                requestNum++;

                elitecoreUserService.loadUserInfoByServiceInstanceNumber(sin, function (err, cache) {
                    handledCount++

                    if (cache) {
                        accountsCount++;
                    }
                }, true)

                //setTimeout(function () {
                //    handledCount++;
                //}, 2000);

                var currentTime = new Date().getTime();
                if (currentTime - startTime > 30 * 1000) {
                    perMinNum += incrementNum;
                    startTime = currentTime;

                    console.log("increasing speed... requests per min=" + perMinNum);

                    clearInterval(interval);
                    interval = setInterval(apiCaller, (60 * 1000) / perMinNum);
                }
            }

            var interval = setInterval(apiCaller, (60 * 1000) / perMinNum);
        }
    });
}, 3500);

