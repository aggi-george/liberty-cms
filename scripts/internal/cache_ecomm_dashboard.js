#!/usr/bin/env nodejs

var async = require('async');
var request = require('request');
var config = require('../../config');
var db = require('../../lib/db_handler');
var paas = require('../../lib/paas');

var LOG = config.LOGSENABLED;

function getEcommAll(cb) {
	var tasks = new Array;
	tasks.push(function (callback) {
		var path = "/counts.json";
		var params = { "qs" : JSON.parse(JSON.stringify(config.PAAS_CREDS)) };
		paas.connect("get", path, params, function (err, result) {
			if (!err && result) {
//				if (result.code == 0) {
				if (result.stats) {
					callback(undefined, { "stats" : result.stats, "queryTime" : result.query_time });
				} else {
					callback(undefined, { "stats" : { }, "error" : result });
				}
			} else {
				console.error("ecomm stats", err)
				callback(undefined, { "stats" : { }, "error" : err });
			} 
		});
	});
	tasks.push(function (callback) {
		var path = "/orders.json";
		var params = { "qs" : JSON.parse(JSON.stringify(config.PAAS_CREDS)) };
		params.qs.status = "delivered";
		paas.connect("get", path, params, function (err, result) {
			if (!err && result) {
//				if (result.code == 0) {
				if (result.orders) {
					callback(undefined, { "delivered" : result.orders, "queryTime" : result.query_time });
				} else {
					callback(undefined, { "delivered" : [], "error" : result });
				}
			} else {
				console.error("ecomm delivered", err)
				callback(undefined, { "delivered" : [], "error" : err });
			}
		});
	});
	tasks.push(function (callback) {
		var path = "/orders.json";
		var params = { "qs" : JSON.parse(JSON.stringify(config.PAAS_CREDS)) };
		params.qs.is_port_in = "true";
		params.qs.porting_status = "success";
		params.qs.approval = "approved";
		paas.connect("get", path, params, function (err, result) {
			if (!err && result) {
//				if (result.code == 0) {
				if (result.orders) {
					callback(undefined, { "initiated" : result.orders, "queryTime" : result.query_time });
				} else {
					callback(undefined, { "initiated" : [], "error" : result });
				}
			} else {
				console.error("ecomm initiated", err)
				callback(undefined, { "initiated" : [], "error" : err });
			}
		});
	});
	tasks.push(function (callback) {
		var match = {	"$or" : [ { "activity" : "port_in_success" },
					{ "activity" : "port_in_approved" } ],
				"$and": [ { "number" : { "$exists" : 1 } },
					{ "number" : { "$ne" : 'NA' } },
					{ "number" : { "$ne" : '' } }
		] };
		var project = { "_id" : 0, "number" : 1, "activity" : 1 };
		var group = { "_id" : { "number" : "$number", "activity" : "$activity" } };
		var agg_array = [ { "$match" : match }, { "$project" : project }, { "$group" : group } ];
		var start = new Date();
		db.notifications_logbook.aggregate(agg_array, function(err, result) {
			if (!result) return callback(undefined, { "port_in_success" : [], "port_in_approved" : [], "queryTime" : (new Date() - start) });
			var success = result.map(function (o) {
				if (o._id.activity == "port_in_success") return o._id.number; });
			var approved = result.map(function (o) {
				if (o._id.activity == "port_in_approved") return o._id.number; });
			callback(undefined, { "port_in_success" : success.filter(function (o) { return o }),
					"port_in_approved" : approved.filter(function (o) { return o }),
					"queryTime" : (new Date() - start) });
		});
	});
	tasks.push(function (callback) {
		var start = new Date();
		var q = "SELECT DISTINCT service_instance_number FROM historyBonuses WHERE bonus_type='selfcare_install'";
		db.query_noerr(q, function(rows) {
			var numbers = rows.map(function (o) { return o.service_instance_number; });
			callback(undefined, { "bonus" : numbers,
					"queryTime" : (new Date() - start) });
		});
	});
	tasks.push(function (callback) {
		var startDate = new Date("2016-06-28T00:00:00.000Z").getTime();
		var activity = 'payment_successful';
		var start = new Date();
		db.notifications_logbook
			.aggregate([
				{$match: {activity: activity, ts: {$gt: startDate}, referral_code: {"$exists": true, "$ne": ""}}},
				{$project: {_id: 0, referral_code: 1, number: 1, customer_name: 1}},
				{$group: {_id: "$referral_code", count: {$sum: 1}, numbers: {$push: "$$ROOT"}}},
				{$sort: {count: -1}},
				{$limit: 100}])
			.toArray(function (err, result) {
				callback(undefined, { "referral_codes_top_100": result,
						"queryTime" : (new Date() - start) });
			});
	});
	tasks.push(function (callback) {
		var startDate = new Date("2016-06-28T00:00:00.000Z").getTime();
		var activity = 'payment_successful';
		var start = new Date();
		db.notifications_logbook
			.aggregate([
				{$match: {activity: activity, ts: {$gt: startDate}, referral_code: {"$exists": true, "$ne": ""}}},
				{$project: {_id: 0, referral_code: 1, number: 1, customer_name: 1, ts: 1}},
				{$sort: {count: -1}}])
			.toArray(function (err, result) {
				callback(undefined, { "referral_codes_on_payment": result,
						"queryTime" : (new Date() - start) });
			});
	});
	tasks.push(function (callback) {
		var startDate = new Date("2016-06-28T00:00:00.000Z").getTime();
		var activity = 'validate_base_plan';
		var start = new Date();
		db.notifications_logbook
			.aggregate([
				{$match: {activity: activity, ts: {$gt: startDate}, referral_code: {"$exists": true, "$ne": ""}}},
				{$project: {_id: 0, referral_code: 1, number: 1, name: 1, ts: 1}},
				{$sort: {count: -1}}])
			.toArray(function (err, result) {
				callback(undefined, { "referral_codes": result,
						"queryTime" : (new Date() - start) });
			});
	});
	tasks.push(function (callback) {
		var startDate = new Date("2016-07-01T00:00:00.000Z").getTime();
		var activity = 'bonus_referral_added';
		var start = new Date();
		db.notifications_logbook
			.aggregate([
				{$match: {activity: activity, ts: {$gt: startDate}, referral_code: {"$exists": true, "$ne": ""}}},
				{$project: {_id: 0, referral_code: 1, joined_user_number: 1, joined_user_name: 1, number: 1, name: 1}},
				{$sort: {count: -1}}])
			.toArray(function (err, result) {
				var referral_bonuses = (!result) ? [] : result.map(function(o) {
					return o.referral_code + "_" + o.joined_user_number;
				});
				callback(undefined, { "referral_bonuses": referral_bonuses,
						"queryTime" : (new Date() - start) });
			});
	});
	tasks.push(function (callback) {
		var start = new Date();
		var startDate = new Date("2016-06-28T16:00:00.000Z").getTime();
		var grabStartDate = new Date("2016-08-30T16:00:00.000Z").getTime();
		var lazadaStartDate = new Date("2016-10-13T16:00:00.000Z").getTime();
		var past10Days = new Date().getTime() - 10 * 24 * 60 * 60 * 1000;

		if (past10Days > startDate) {
			startDate = past10Days;
		}
		if (past10Days > lazadaStartDate) {
			lazadaStartDate = past10Days;
		}
		if (past10Days > grabStartDate) {
			grabStartDate = past10Days;
		}

		var activity = 'payment_successful';
		var dateFormatted = {$dateToString: {format: "%Y-%m-%d", date: {$add: [new Date(0), "$ts"]}}};

		var registrations = [
			{$match: {activity: activity, ts: {$gt: past10Days}}},
			{$project: {_id: 0, referral_code: 1, number: 1, customer_name: 1, ts:1, date: dateFormatted}},
			{$group: {_id: "$date", count: {$sum: 1}, numbers: {$push: "$$ROOT"}}},
			{$sort: {_id: 1}}
		];

		var registrationsWithReferrals = JSON.parse(JSON.stringify(registrations));
		registrationsWithReferrals[0].$match['$and'] = [
			{referral_code: {"$regex": "^(?!GRAB).*"}},
			{referral_code: {"$ne": "ZCIRCLES"}},
			{referral_code: {"$ne": "LAZCIRCLES"}},
			{referral_code: {"$exists": true, "$ne": ""}}
		];
		registrationsWithReferrals[0].$match.ts['$gt'] = startDate;
		registrationsWithReferrals[1].$project['date'] = dateFormatted;

		var registrationsWithGrabCode = JSON.parse(JSON.stringify(registrations));
		registrationsWithGrabCode[0].$match['referral_code'] = {"$regex": "GRAB.*"};
		registrationsWithGrabCode[0].$match.ts['$gt'] = grabStartDate;
		registrationsWithGrabCode[1].$project['date'] = dateFormatted;

		var registrationsWithLazadaCode = JSON.parse(JSON.stringify(registrations));
		registrationsWithLazadaCode[0].$match['referral_code'] = "LAZCIRCLES";
		registrationsWithLazadaCode[0].$match.ts['$gt'] = lazadaStartDate;
		registrationsWithLazadaCode[1].$project['date'] = dateFormatted;

		var addMissingDays = function(result) {
			if (!result) {
				return;
			}

			for (var i = 0; i < 11; i++) {
				var date = new Date(new Date().getTime() - i * 24 * 60 * 60 * 1000);
				var key = date.toISOString().split("T")[0];

				var item = result.filter(function(item) {
					return item._id === key;
				});
				if (!item || item.length == 0) {
					result.push({_id: key, count: 0, numbers: []});
				}
			}
			result.sort(function(a, b) {
				return a._id < b._id ? -1 : 1;
			});
		};

		db.notifications_logbook.aggregate(registrations).toArray(function (err, resultAll) {
			addMissingDays(resultAll);
			db.notifications_logbook.aggregate(registrationsWithGrabCode).toArray(function (err, resultGrab) {
				addMissingDays(resultGrab);
				db.notifications_logbook.aggregate(registrationsWithLazadaCode).toArray(function (err, resultLazada) {
					addMissingDays(resultLazada);
					db.notifications_logbook.aggregate(registrationsWithReferrals).toArray(function (err, resultReferrals) {
						addMissingDays(resultReferrals);
						callback(undefined, {
							"success_payments": resultAll,
							"success_payments_with_referrals": resultReferrals,
							"success_payments_with_grab_code": resultGrab,
							"success_payments_with_lazada": resultLazada,
							"queryTime": (new Date() - start)
						});
					});
				});
			});
		});
	});
	tasks.push(function (callback) {
                var start = new Date();
		var q = "SELECT number FROM app WHERE app_type='selfcare'";
		db.query_noerr(q, function (rows) {
			// assuming 65XXXXXXXX, fix this
			var numbers = rows ? rows.map(function (o) {
				return o.number.substring(2) }) : [];
			callback(undefined, { "installs" : numbers,
				"queryTime" : (new Date() - start) });
		});
	});
	async.parallel(tasks, function (err, result) {
		var reply = new Object;
		var delivered;
		var installs;
		var bonus;
		var referral_codes_top_100;
		var success_payments;
		var success_payments_with_referrals;
		var success_payments_with_grab_code;
		var success_payments_with_lazada;
		var referral_bonuses;
		var referral_codes_on_payment;
		var referral_codes;
		var initiated;
		var ported;
		result.forEach(function (item) {
			if (item) {
				if (LOG) console.log("Dashboard " + Object.keys(item)[0], item.queryTime);
				if (item.registrations) {
					reply.registrations = item.registrations;
				}
				if (item.stats) {
					reply.stats = item.stats;
				}
				if (item.initiated) {
					initiated = item.initiated;
				}
				if (item.port_in_success) {
					ported = item.port_in_success;
					approved = item.port_in_approved;
				}
				if (item.delivered) {
					delivered = item.delivered;
				}
				if (item.installs) {
					installs = item.installs;
				}
				if (item.bonus) {
					bonus = item.bonus;
				}
				if (item.referral_codes_top_100) {
					referral_codes_top_100 = item.referral_codes_top_100;
				}
				if (item.success_payments) {
					success_payments = item.success_payments;
				}
				if (item.success_payments_with_referrals) {
					success_payments_with_referrals = item.success_payments_with_referrals;
				}
				if (item.success_payments_with_grab_code) {
					success_payments_with_grab_code = item.success_payments_with_grab_code;
				}
				if (item.success_payments_with_lazada) {
					success_payments_with_lazada = item.success_payments_with_lazada;
				}
				if (item.referral_bonuses) {
					referral_bonuses = item.referral_bonuses;
				}
				if (item.referral_codes) {
					referral_codes = item.referral_codes;
				}
				if (item.referral_codes_on_payment) {
					referral_codes_on_payment = item.referral_codes_on_payment;
				}
			}
		});
		if (initiated) reply.stats.porting_approved = { "list" : initiated.filter(function (o) {
			if ( (approved.indexOf(o.number) > -1) || (ported.indexOf(o.number) > -1) ) return o;
			else return undefined;
		}) };
		if (reply.stats.porting_approved)
			reply.stats.porting_approved.count = reply.stats.porting_approved.list.length;
		if (initiated) reply.stats.porting_not_approved = { "list" : initiated.filter(function (o) {
			if ( (approved.indexOf(o.number) == -1) && (ported.indexOf(o.number) == -1) ) return o;
			else return undefined;
		}) };
		if (reply.stats.porting_not_approved)
			reply.stats.porting_not_approved.count = reply.stats.porting_not_approved.list.length;
		if (initiated) reply.stats.porting_fail = { "list" : initiated.filter(function (o) {
			if ( (approved.indexOf(o.number) > -1) && (ported.indexOf(o.number) == -1) ) return o;
			else return undefined;
		}) };
		if (reply.stats.porting_fail)
			reply.stats.porting_fail.count = reply.stats.porting_fail.list.length;
		if (initiated) reply.stats.porting_success = { "list" : initiated.filter(function (o) {
			if ( ported.indexOf(o.number) > -1 ) return o;
			else return undefined;
		}) };
		if (reply.stats.porting_success)
			reply.stats.porting_success.count = reply.stats.porting_success.list.length;
		var failport = (reply.stats.porting_fail) ?
				reply.stats.porting_fail.list.map(function (o) { return o.number; }) :
				[ ];

		reply.stats.delivered = { "list" : delivered, "count" : delivered.length };
		reply.stats.delivered.count = reply.stats.delivered.list.length;
		reply.stats.no_install = { "list" : delivered.filter(function (o) {
			// not used temp number or ported number to activate app
			if ( (parseInt(o.temporary_number) != o.temporary_number) &&
					(parseInt(o.number) != o.number) ) return undefined;	// phone only orders
			else if ( (installs.indexOf(o.number) == -1) && (installs.indexOf(o.temporary_number) == -1) ) return o;
			else return undefined;
		}) };
		reply.stats.no_install.count = reply.stats.no_install.list.length;
		reply.stats.installs = { "list" : delivered.filter(function (o) {
			if ( (parseInt(o.temporary_number) != o.temporary_number) &&
					(parseInt(o.number) != o.number) ) {
				return undefined;	// phone only orders
			} else if ( installs.indexOf(o.number) > -1 ) {		// used ported number to activate app
				return o;
			} else if ( installs.indexOf(o.temporary_number) > -1 ) {	// used temp number to activate app
				o.ported_number = o.number;
				o.number = o.temporary_number;
				return o;
			} else {
				return undefined;
			}
		}) };
		reply.stats.installs.count = reply.stats.installs.list.length;
		reply.stats.no_bonus = { "list" : reply.stats.installs.list.filter(function (o) {
			if (bonus.indexOf(o.service_instance_number) == -1) return o;
			else return undefined;
		}) };
		reply.stats.no_bonus.count = reply.stats.no_bonus.list.length;
		reply.stats.bonus = { "list" : reply.stats.installs.list.filter(function (o) {
			if (bonus.indexOf(o.service_instance_number) > -1) return o;
			else return undefined;
		}) };
		reply.stats.bonus.count = reply.stats.bonus.list.length;

		if (referral_codes && referral_bonuses) {
			reply.stats.no_referral_bonus = { "list" : referral_codes.filter(function (o) {
				if (referral_bonuses.indexOf(o.referral_code + "_" + o.number) == -1) return o;
				else return undefined;
			}) };
			reply.stats.referral_bonus = { "list" : referral_codes.filter(function (o) {
				if (referral_bonuses.indexOf(o.referral_code + "_" + o.number) > -1) return o;
				else return undefined;
			}) };
		} else {
			reply.stats.no_referral_bonus = { "list" : [] };
			reply.stats.referral_bonus = { "list" : [] };
		}
		reply.stats.no_referral_bonus.count = reply.stats.no_referral_bonus.list.length;
		reply.stats.referral_bonus.count = reply.stats.referral_bonus.list.length;

		reply.stats.referrals = {
			listTop100: referral_codes_top_100
		}

		reply.stats.payments = {
			listTotal: success_payments,
			listReferrals: success_payments_with_referrals,
			listGrabCodes: success_payments_with_grab_code,
			listLazadaCodes: success_payments_with_lazada
		}
		cb(reply);
	});
}

setTimeout(function() {
    getEcommAll(function(reply) {
        db.cache_put("api_stats", "ecomm_dashboard", reply, 24 * 60 * 60 * 1000);
        setTimeout(function() {
            process.exit(0);
        }, 1000);
    });
}, 10000);
