#!/usr/bin/env nodejs

var resolve = require('path').resolve;
global.__base = resolve('../../');
global.__core = resolve('../../') + '/src/core';
global.__manager = resolve('../../') + '/src/core/manager';
global.__api = resolve('../../') + '/src/api';
global.__lib = resolve('../../') + '/lib';
global.__res = resolve('../../') + '/res';
global.__scripts = resolve('../../') + '/scripts';
var dateformat = require('dateformat');
var common = require('../../lib/common');
var db = require('../../lib/db_handler');
var ec = require('../../lib/elitecore');
var config = require('../../config');
var async = require('async');
var PortInManager = require('../../src/core/manager/porting/portInManager');
var notificationSend = require('../../src/core/manager/notifications/send');
var NETWORKS = [
    {code: "001", name: "M1"},
    {code: "002", name: "StarHub"},
    {code: "003", name: "Singtel"}
];
var util = require('util');
var bag = {
    // Starting all the portIns on Jan 2 so set the date as Jan 2 SGT
    portInScheduleStartDate: 1514853000000,
    milliSecondsInADay: 24 * 60 * 60 * 100,
    portInSchedule: {
        0: 0, // No portIns on Sunday
        1: 100, // 100 portins on Monday and so on
        2: 200,
        3: 200,
        4: 200,
        5: 100,
        6: 0 // No PortIns on Saturday
    }
};

setTimeout(function () {
    async.series([
        getAllActiveSwitchCustomers.bind(null, bag),
        sendNotificationsAndSchedulePortIns.bind(null, bag),
    ], function (err) {
        if (err) {
            common.error("Could not process the request because of error, ", err);
        } else {
            common.log("Successfully sent notification and scheduled the portin for switch customers");
        }
    });
}, 15000);

function getAllActiveSwitchCustomers(bag, next) {
    common.log("Inside getAllActiveSwitchCustomers");

    var query = "select customername, service_instance_number, number, emailid, baseplan, start_ts, " +
    "temp_number, status, p.id as requestId, donor_network_code from ecAccounts a left join customerProfile r on " +
    "a.serviceinstanceaccount=r.service_instance_number left join " + 
    "customerPortIn p on r.id=p.profile_id where a.baseplan='CirclesSwitch' and p.status='WAITING' " + 
    "and a.accountstatus='Active' and p.start_ts > '2018-02-01'";

    common.log("Query to get Circles Switch User", query);
    db.query_err(query, function (err, result) {
        if (err) {
            common.error("circlesSwitchSchedule", "loadLastRequest: failed to load last port-in requests, err=" + err);
            return next(err);
        }

        if (!result || !result.length) {
            return next("No Circles Switch customers found in the database");
        }

        common.log("Loaded " + result.length + " circles switch customers from the database");
        common.log(util.inspect(result), {depth: null});
        bag.circlesSwitchCustomers = result;
        return next();
    });
};

function sendNotificationsAndSchedulePortIns(bag, next) {
    common.log("Inside sendNotificationsAndSchedulePortIns");

    var portInScheduleStartDate = bag.portInScheduleStartDate;
    var totalNumberOfDays = 0;
    var currentBatchPortInDate = new Date(portInScheduleStartDate);
    var currentDay = currentBatchPortInDate.getDay();
    var totalPortIns = bag.circlesSwitchCustomers.length;
    var portInsScheduled = 0;
    var currentDayPortInCount = totalNumberOfDays ? bag.portInSchedule[currentDay] : 100;
    var prefix = 65;

    function executeBatchScheduling (err) {
        if (err) {
            return next(err);
        }
        if (totalPortIns > 0) {
            common.log("Current Batch Port In Date", currentBatchPortInDate);
            common.log("Current Batch Total Number of Days", totalNumberOfDays);
            common.log("Current Batch Port In Day", currentDay);
            common.log("Total number of portIns left ", totalPortIns);
            common.log("Total number of portIns Done ", portInsScheduled);
            common.log("Total number of portIns Scheduled today ", currentDayPortInCount);
            var currentDayPortIns = bag.circlesSwitchCustomers.slice(portInsScheduled, portInsScheduled + currentDayPortInCount);
            console.error('These are the current day portIns', util.inspect(currentDayPortIns, {depth: null}));
            async.eachOfLimit(currentDayPortIns, 10, function (portIn, index, nextPortIn) {
                var temp_number = portIn.temp_number
                var portInTime;
                if (index % 2 !== 0) {
                    portInTime = common.getDateTime(new Date(currentBatchPortInDate.getTime() + 4 * 60 * 60 * 1000));
                } else {
                    portInTime = common.getDateTime(currentBatchPortInDate);
                }
                ec.getCustomerDetailsNumber(prefix, temp_number, true, function (err, cache) {
                    if (err) {
                        common.error("circlesSwitchSchedule", "activateRequest: failed to load customer cache " +
                        "number= " + temp_number + " err=", err);
                        return nextPortIn(err);
                    }
                    if (!cache) {
                        common.error("circlesSwitchSchedule", "activateRequest: no customer found for number =" +
                        temp_number);
                        return nextPortIn();
                    }
                    if (cache.status !== 'Active') {
                        common.error("Could not schedule portin for the following request "+
                        "because the number is not active ", temp_number);
                        return nextPortIn();
                    }
                    
                    common.log("Scheduling PortIn for number " + cache.number + " with requestId " + portIn.requestId);
                    const updateQuery = `UPDATE customerPortIn SET start_ts=${db.escape(portInTime)} WHERE id= ${ db.escape(portIn.requestId) }`;
                    console.log("Update query", updateQuery);
                    db.query_err(updateQuery, (err, rows) => {
                        if (err) {
                            common.error("circlesSwitchSchedule", "Failed to update request status, err=" + err.message);
                            return nextPortIn(err);
                        }
                        PortInManager.schedulePortIn(portIn.service_instance_number, cache.prefix,
                            cache.number, portIn.requestId, portIn.number,
                            portIn.donor_network_code, portInTime, '[CMS][INTERNAL]', (err, result) => {
                                if (err) {
                                    common.error("circlesSwitchSchedule", "failed to schedule portIn " +
                                    "requestId= " + portIn.requestId + " err=", err);
                                    return nextPortIn(err);
                                }
                                var networkName;
                                NETWORKS.forEach(function (item) {
                                    if (item.code === portIn.donor_network_code) {
                                        networkName = item.name;
                                    }
                                });
                                console.log(util.inspect(result, {depth: null}));
                                sendNotificationInternal('circles_switch_port_in_activated', cache.prefix, cache.number,
                                    portIn.number, networkName, portInTime, 'PORT_IN_ACTIVATION',
                                    undefined, config.NOTIFICATION_KEY, function (err, result) {
                                        if (err) {
                                            common.error("circlesSwitchSchedule", "Failed to send notification, for portInRequest " +
                                            portIn.requestId + "err= " + err.message);
                                            return nextPortIn(err);
                                        }

                                        return nextPortIn(undefined, result);
                                    });
                            }
                        );
                    });

                });
            }, function(err) {
                currentBatchPortInDate = new Date(currentBatchPortInDate.getTime() + 24 * 60 * 60 * 1000);
                totalNumberOfDays = totalNumberOfDays + 1;
                currentDay = currentBatchPortInDate.getDay();
                totalPortIns = totalPortIns - currentDayPortInCount ;
                portInsScheduled = portInsScheduled + currentDayPortInCount;
                currentDayPortInCount = totalNumberOfDays ? bag.portInSchedule[currentDay] : 100;
                common.log("Next Batch Port In Date", currentBatchPortInDate);
                common.log("Next Batch Total Number of Days", totalNumberOfDays);
                common.log("Next Batch Port In Day", currentDay);
                common.log("Total number of portIns left after this batch", totalPortIns);
                common.log("Total number of portIns Done after this batch", portInsScheduled);
                common.log("Total number of portIns to be scheduled in next batch", currentDayPortInCount);
                return executeBatchScheduling(err);
            });
        } else {
            return next();
        }
    }

    return executeBatchScheduling();
}

function sendNotificationInternal(activity, prefix, number, portingNumber, donorNetwork,
                                  startDate, reason, description, notificationKey, callback) {
    var startDateSG = !startDate ? undefined
        : new Date(new Date(startDate).getTime() + 8 * 60 * 60 * 1000);
    common.log("Sending notification for number " + number + " with portingNumber " + portingNumber);
    // TODO: remove notificationKey - move logic to ROLE
    const notification = {
        teamID: 5,
        prefix,
        number,
        activity,
        notificationKey,
        reason: reason,
        porting_number: portingNumber,
        donor_network: donorNetwork,
        description: description ? description : 'None',
        start_date: startDateSG && startDateSG.getTime() ? dateformat(startDateSG, 'dS mmmm') : undefined,
        number_selected: portingNumber
    }
    notificationSend.deliver(notification, undefined, function (err, result) {
        if (err) {
            if (callback) callback(err, { activity, variables: notification });
            return;
        } else {
            if (callback) callback(undefined, { activity, variables: notification, result });
            return;
        }
    });
}