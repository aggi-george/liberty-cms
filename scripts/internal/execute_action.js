#!/usr/bin/env nodejs

require('../../bin/global');

var async = require('async');
var fs = require('fs');

var config = require('../../config');
config.EC_LOGSENABLED = false;
config.LOGSENABLED = false;

var notificationSend = require('../../src/core/manager/notifications/send');
var ec = require('../../lib/elitecore');
var db = require('../../lib/db_handler');
var common = require('../../lib/common');
var subscriptionManager = require('../../src/core/manager/bonus/subscriptionManager');
var waiverManager = require('../../src/core/manager/bonus/waiverManager');
var bonusManager = require('../../src/core/manager/bonus/bonusManager');
var promotionsManager = require('../../src/core/manager/promotions/promotionsManager');

var SUPPORTED_MODES = [
    "ADD_WAIVER",
    "AGGREGATE_BONUSES",
    "ADD_BONUS",
    "SEND_NOTIFICATION",
    "PRINT_EMAIL",
    "PRINT_NUMBER_EMAIL",
    "PRINT_NUMBER_EMAIL_CODE",
    "PRINT_SIN"
];

var SUPPORTED_BONUS_TYPES = [
];

var SUPPORTED_INPUT = [
    "TERMINAL",
    "FILE",
    "SELFCARE",
    "DB"
];

var SUPPORTED_CRITERION = [
    "ALL",
    "NOT_REFERRED",
    "LEADERBOARD_TOP",
    "LEADERBOARD_REACHED_GB",
    "EXTERNAL_GRAB",
    "REFERRED_AT_LEAST_ONCE",
    "REFERRED_AT_LEAST_3_TIMES",
    "REFERRED_LESS_3_TIMES",
    "GRAB_SIGN_UP",
    "HAS_BONUS",
    "NOT_PORTED_IN",
    "PORTED_IN"
];

var tag = "SCRIPT[ACTION]";

args = [];
var mode, criterion, input, activity, fileName, number, topCount,
    delay, greaterGb, maxGb, waiverAmountCents, waiverReason, allowDuplicates, external, bonusType;

process.argv.forEach(function (val, index, array) {
    args.push(val);
});

function printHelp(errorMessage) {

    if (errorMessage) {
        console.log(tag, "ERROR: " + errorMessage);
        console.log(tag, "-------------------------------------");
    }

    console.log(tag, "Syntax");
    console.log(tag, "-m   <mandatory:MODE>      " + JSON.stringify(SUPPORTED_MODES));
    console.log(tag, "-c   <mandatory:CRITERION> " + JSON.stringify(SUPPORTED_CRITERION));
    console.log(tag, "-i   <mandatory:INPUT>     " + JSON.stringify(SUPPORTED_INPUT));
    console.log(tag, "-a   <optional:activity> ");
    console.log(tag, "-ad  <optional:allowDuplicates>    [DEFAULT: 0]");
    console.log(tag, "-f   <optional:file> ");
    console.log(tag, "-n   <optional:number>");
    console.log(tag, "-t   <optional:topCount>");
    console.log(tag, "-gg  <optional:greaterGb>");
    console.log(tag, "-mg  <optional:maxGb>");
    console.log(tag, "-wa  <optional:waiverAmountCents>");
    console.log(tag, "-wr  <optional:waiverReason>");
    console.log(tag, "-bt  <optional:bonusType>   " + JSON.stringify(SUPPORTED_BONUS_TYPES));
    console.log(tag, "-ext <optional:external>          [DEFAULT: 0");
    console.log(tag, "-tg  <optional:tag>               [DEFAULT: 'SCRIPT[ACTION]'");
    console.log(tag, "-d   <optional:delay>             [DEFAULT: 2000ms]");
    process.exit(1);
}

function initParam(key, value) {
    if (!value) {
        if (key.trim() == "-tg") {
            tag = "";
        }
        return;
    }

    if (key.trim() == "-m") {
        mode = value;
    } else if (key.trim() == "-c") {
        criterion = value;
    } else if (key.trim() == "-i") {
        input = value;
    } else if (key.trim() == "-a") {
        activity = value;
    } else if (key.trim() == "-f") {
        fileName = value;
    } else if (key.trim() == "-n") {
        number = value;
    } else if (key.trim() == "-k") {
        tag = value;
    } else if (key.trim() == "-bt") {
        bonusType = value;
    } else if (key.trim() == "-ext") {
        external = parseInt(value);
    } else if (key.trim() == "-tg") {
        topCount = parseInt(value);
    } else if (key.trim() == "-d") {
        delay = parseInt(value);
    } else if (key.trim() == "-ad") {
        allowDuplicates = parseInt(value);
    } else if (key.trim() == "-gg") {
        greaterGb = parseFloat(value);
    } else if (key.trim() == "-mg") {
        maxGb = parseFloat(value);
    } else if (key.trim() == "-wa") {
        waiverAmountCents = parseInt(value);
    } else if (key.trim() == "-wr") {
        waiverReason = value;
    }
}

for (var i = 2; i < args.length; i += 2) {
    initParam(args[i], args[i + 1]);
}

if (!mode || !criterion || !input) {
    printHelp("some mandatory params are missing");
    process.exit(0);
} else if (SUPPORTED_MODES.indexOf(mode) == -1) {
    printHelp("invalid mode [" + mode + "]");
    process.exit(0);
} else if (SUPPORTED_CRITERION.indexOf(criterion) == -1) {
    printHelp("invalid criterion [" + criterion + "]");
    process.exit(0);
} else if (SUPPORTED_INPUT.indexOf(input) == -1) {
    printHelp("invalid input [" + input + "]");
    process.exit(0);
} else if (!activity && mode === "SEND_NOTIFICATION") {
    printHelp("mode SEND_NOTIFICATION requires -a <activity>");
    process.exit(0);
} else if (!fileName && input === "FILE") {
    printHelp("input FILE requires -f <file>");
    process.exit(0);
} else if (!number && input === "TERMINAL") {
    printHelp("input TERMINAL requires -n <number>");
    process.exit(0);
} else if (!topCount && criterion === "LEADERBOARD_TOP") {
    printHelp("input TERMINAL requires -n <number>");
    process.exit(0);
} else if (!waiverAmountCents && mode === "ADD_WAIVER") {
    printHelp("mode ADD_WAIVER requires -wa <waiverAmountCents>");
    process.exit(0);
} else if (!waiverReason && mode === "ADD_WAIVER") {
    printHelp("mode ADD_WAIVER requires -wr <waiverReason>");
    process.exit(0);
} else if (input !== "DB" && criterion === "LEADERBOARD_TOP") {
    printHelp("criterion LEADERBOARD_TOP requires input -i DB");
    process.exit(0);
} else if (input !== "DB" && criterion === "GRAB_SIGN_UP") {
    printHelp("criterion GRAB_SIGN_UP requires input -i DB");
    process.exit(0);
} else if (input !== "DB" && criterion === "EXTERNAL_GRAB") {
    printHelp("criterion EXTERNAL_GRAB requires input -i DB");
    process.exit(0);
} else if (input !== "DB" && criterion === "HAS_BONUS") {
    printHelp("criterion HAS_BONUS requires input -i DB");
    process.exit(0);
} else if (input !== "DB" && criterion === "LEADERBOARD_REACHED_GB") {
    printHelp("criterion LEADERBOARD_REACHED_GB requires input -i DB");
    process.exit(0);
} else if (!greaterGb && !maxGb && criterion === "LEADERBOARD_REACHED_GB") {
    printHelp("criterion LEADERBOARD_REACHED_GB requires -gg or -mg param");
    process.exit(0);
} else if (input !== "SELFCARE" && criterion === "REFERRED_LESS_3_TIMES") {
    printHelp("criterion REFERRED_LESS_3_TIMES requires input -i SELFCARE");
    process.exit(0);
} else if (mode === "ADD_WAIVER" && input !== "FILE" && input !== "TERMINAL") {
    printHelp("mode ADD_WAIVER requires input -i FILE|TERMINAL");
    process.exit(0);
} else if (mode === "ADD_BONUS" && (!bonusType || SUPPORTED_BONUS_TYPES.indexOf(bonusType) == -1)) {
    printHelp("mode ADD_BONUS requires correct bonus type [" + bonusType + "]");
    process.exit(0);
}

var errorNumbers = [];
var errorReason = {};
var skippedNumbers = [];
var successNumbers = [];
var successInfo = [];

function readFile(callback) {
    try {
        var input = fs.createReadStream(fileName);
    } catch (e) {
        console.error(e);
        process.exit(-1);
    }

    var remaining = '';
    var array = [];

    input.on('data', function (data) {

        remaining += data;
        var index = remaining.indexOf('\n');
        while (index > -1) {
            var line = remaining.substring(0, index);
            remaining = remaining.substring(index + 1);
            array.push({prefix: "65", number: line});
            index = remaining.indexOf('\n');
        }
    });

    input.on('end', function () {
        if (remaining.length > 0) {
            array.push({prefix: "65", number: remaining.trim()});
        }

        if (callback) {
            callback(undefined, array);
        }
    });
}

function loadGrabSingUp(callback) {
    var query = "SELECT prefix, number, name, email FROM externalCustomers WHERE source=" + db.escape('grab');

    db.query_err(query, function (err, rows) {
        if (err) {
            return callback(err);
        }

        var numbers = [];
        rows.forEach(function (item) {
            if (item.number) {
                numbers.push({prefix: item.prefix, number: item.number, email: item.email, name: item.name});
            }
        });

        return callback(undefined, numbers);
    });
}

function loadExternalCustomersWithCode(source, callback) {
    var query = "SELECT number, name, email, code FROM externalCustomers LEFT JOIN promoCodes " +
        "ON (promoCodes.id = promo_code_id) WHERE source=" + db.escape(source) + " GROUP BY externalCustomers.id";

    db.query_err(query, function (err, rows) {
        if (err) {
            return callback(err);
        }

        var numbers = [];
        var countLoaded = 0;
        var addTask = function (tasks, item) {
            tasks.push(function (callback) {

                if (!item.email && !item.number) {
                    console.log(tag, countLoaded + " - " + item.number + " " + item.code + ", INVALID ITEM");
                    return callback();
                }

                promotionsManager.loadPromoCode(item.code, function (err, code) {
                    countLoaded++;
                    if (err) {
                        console.log(tag, countLoaded + " - " + item.number + " " + item.code + " INVALID CODE, ERROR=" + err.message);
                        return callback();
                    } else {
                        console.log(tag, countLoaded + " - " + item.number + " " + item.code + " ADDED CODE");
                        numbers.push({
                            number: item.number, prefix: "65", email: item.email,
                            name: item.name, referral_code: item.code
                        });
                        return callback();
                    }
                }, true);
            });
        }

        var tasks = [];
        rows.forEach(function (item) {
            addTask(tasks, item);
        });

        async.parallelLimit(tasks, 20, function (errGroup, resultGroup) {
            callback(undefined, numbers);
        });
    });
}

function loadAllWithBonus(callback) {
    var query = "SELECT DISTINCT service_instance_number as sin FROM historyBonuses";

    db.query_err(query, function (err, rows) {
        processDBResponse(err, rows, callback);
    });
}

function loadReferredOnceNumbers(callback) {
    var query = "SELECT service_instance_number as sin FROM historyBonuses WHERE bonus_type=" + db.escape('referral');

    db.query_err(query, function (err, rows) {
        processDBResponse(err, rows, callback);
    });
}

function loadReferred3TimesNumbers(callback) {
    var query = "SELECT sin FROM (SELECT service_instance_number as sin, count(*) as count FROM historyBonuses WHERE bonus_type="
        + db.escape('referral') + " GROUP BY sin) as data WHERE count >= 3";

    db.query_err(query, function (err, rows) {
        processDBResponse(err, rows, callback);
    });
}

function leaderBoardTop(callback) {
    var query = "SELECT sum(bonus_kb_pretty) as bonusDataGb, service_instance_number as sin FROM historyBonuses " +
        "WHERE display_until_ts=0 AND deactivated=0 GROUP BY service_instance_number ORDER by bonusDataGb DESC LIMIT " + topCount;

    db.query_err(query, function (err, rows) {
        processDBResponse(err, rows, callback);
    });
}

function leaderBoardGB(callback) {
    var query = "SELECT * FROM (SELECT service_instance_number as sin, SUM(bonus_kb_pretty) / 1000 / 1000 as bonus_gb " +
        "FROM historyBonuses WHERE deactivated=0 AND (display_until_ts < 1000 || display_until_ts = '') " +
        "GROUP BY service_instance_number) as bonuses WHERE 1=1 AND ";

    if (greaterGb && maxGb) {
        query += "bonus_gb >= " + db.escape(greaterGb) + " AND bonus_gb < " + db.escape(maxGb);
    } else if (greaterGb) {
        query += "bonus_gb >= " + db.escape(greaterGb);
    } else if (maxGb) {
        query += "bonus_gb < " + db.escape(maxGb);
    }

    db.query_err(query, function (err, rows) {
        processDBResponse(err, rows, callback);
    });
}

function processDBResponse(err, rows, callback) {
    if (err) {
        return callback(err);
    }

    var serviceInstanceNumbers = [];
    rows.forEach(function (item) {
        if (serviceInstanceNumbers.indexOf(item.sin) == -1) {
            serviceInstanceNumbers.push(item.sin);
        }
    });

    var numbers = [];
    if (serviceInstanceNumbers.length == 0) {
        return callback(undefined, numbers);
    }

    var countLoaded = 0;
    var addTask = function (tasks, item) {
        tasks.push(function (callback) {
            ec.getCustomerDetailsService(item, false, function (err, cache) {
                countLoaded++;

                if (countLoaded % 300 == 0) {
                    console.log(tag, "PROGRESS: " + countLoaded + " / " + serviceInstanceNumbers.length);
                }

                if (err) {
                    console.log(tag, countLoaded + " - " + item + " ERROR=" + err.message);
                    callback(undefined, {sin: item});
                } else if (!cache) {
                    console.log(tag, countLoaded + " - " + item + " NO NUMBER IS FOUND");
                    callback(undefined, {sin: item});
                } else {
                    numbers.push({sin: item, prefix: "65", number: cache.number});
                    callback(undefined, {sin: item, prefix: "65", number: cache.number});
                }
            }, true);
        });
    }

    var tasks = [];
    serviceInstanceNumbers.forEach(function (item) {
        addTask(tasks, item);
    });

    async.parallelLimit(tasks, 20, function (errGroup, resultGroup) {
        callback(undefined, numbers);
    });
}

function loadSelfcareNumbers(callback) {
    var q = "SELECT number FROM app WHERE app_type='selfcare'";
    db.query_noerr(q, function (rows) {
        var list = [];
        if (rows) {
            rows.forEach(function (item) {
                var prefix = "65";
                var number = item.number.substring(2);
                list.push({prefix: prefix, number: number});
            });
        }

        callback(undefined, list);
    });
}

function addSendTask(activity, tasks, i, item) {
    var prefix = item.prefix;
    var number = item.number;
    var name = item.name;
    var email = item.email;
    var referral_code = item.referral_code;
    var callbackCalled = false;

    tasks.push(function (callback) {
        var printLog = function (number, wait, info) {
            var status = "UNKNOWN";
            if (successNumbers.indexOf(number) >= 0) {
                status = info ? "INFO   " : "SUCCESS";
            } else if (skippedNumbers.indexOf(number) >= 0) {
                status = "SKIPPED";
            } else if (errorNumbers.indexOf(number) >= 0) {
                status = "ERROR  ";
            }

            console.log(tag, status + " - i=" + i + ", number=" + number
            + ", success=" + successNumbers.length + ", skipped=" + skippedNumbers.length
            + ", errors=" + errorNumbers.length + (errorNumbers.indexOf(number) > 0 ? ", reason=" + errorReason[number] : ""));


            if (wait && delay != -1) {
                var waitDelay = delay > 0 ? delay : 2000;
                if ((mode === "AGGREGATE_BONUSES" || mode === "ADD_BONUS") && (!waitDelay || waitDelay < 5000)) {
                    waitDelay = 5000;
                }
                setTimeout(function () {
                    if (!callbackCalled) {
                        callbackCalled = true;
                        callback();
                    }
                }, waitDelay);
            } else {
                if (!callbackCalled) {
                    callbackCalled = true;
                    callback();
                }
            }
        }

        var executeModeAction = function (cache) {
            if (mode === "ADD_BONUS") {
                errorNumbers.push(number);
                errorReason[number] = "Bonus Type is Not Supported";
                return printLog(number, false);
            } else if (mode === "ADD_WAIVER") {
                waiverManager.addCreditNotes(prefix, number, waiverReason, waiverAmountCents, function (err, result) {
                    if (err) {
                        errorNumbers.push(number);
                        errorReason[number] = err.message;
                        return printLog(number, true);
                    }
                    successNumbers.push(number);
                    return printLog(number, true);
                });
            } else if (mode === "SEND_NOTIFICATION") {
                notificationSend.internal({
                    activity: activity,
                    number: number,
                    name: name,
                    email: email,
                    prefix: prefix,
                    referral_code: referral_code
                }, undefined, function (err, result) {
                    if (err) {
                        errorNumbers.push(number);
                        errorReason[number] = err.message;
                        return printLog(number, true);
                    }
                    successNumbers.push(number);
                    return printLog(number, true);
                });
            } else if (mode === "PRINT_EMAIL" || mode === "PRINT_SIN" || mode === "PRINT_NUMBER_EMAIL" || mode === "PRINT_NUMBER_EMAIL_CODE") {
                successNumbers.push(number);
                if (mode === "PRINT_NUMBER_EMAIL_CODE") {
                    successInfo.push(cache.number + " " + cache.email + " " + cache.referral_code);
                } else if (mode === "PRINT_NUMBER_EMAIL") {
                    successInfo.push(cache.number + " " + cache.email);
                } else if (mode === "PRINT_EMAIL") {
                    successInfo.push(cache.email);
                } else if (mode === "PRINT_SIN") {
                    successInfo.push(cache.serviceInstanceNumber);
                }
                return printLog(number, true, true);
            } else if (mode === "AGGREGATE_BONUSES") {
                bonusManager.scheduleAggregation(prefix, number,
                    "[CMS][UpdateScript]", config.NOTIFICATION_KEY, (err) => {
                        if (err) {
                            errorNumbers.push(number);
                            errorReason[number] = err.message;
                            return printLog(number, false);
                        }

                        successNumbers.push(number);
                        return printLog(number, false);
                    });

                //subscriptionManager.updateNextBillBonuses(prefix, number, "[CMS][Script]", config.OPERATIONS_KEY,
                //    function (err, result) {
                //        if (err) {
                //            errorNumbers.push(number);
                //            errorReason[number] = err.message;
                //            return printLog(number, true);
                //        }
                //
                //        if (result.ignored) {
                //            skippedNumbers.push(number);
                //            return printLog(number, true);
                //        } else {
                //            successNumbers.push(number);
                //            return printLog(number, true);
                //        }
                //    })
            } else {
                skippedNumbers.push(number);
                return printLog(number, true);
            }
        }

        var checkReferrals = function (cache) {
            var query = "SELECT count(*) as count FROM historyBonuses WHERE service_instance_number="
                + db.escape(cache.serviceInstanceNumber) + " AND bonus_type=" + db.escape('referral');

            if (criterion === "ALL") {
                return executeModeAction(cache);
            }

            db.query_err(query, function (err, rows) {
                if (err || !rows) {
                    errorNumbers.push(number);
                    errorReason[number] = "DB ERROR " + err.message;
                    return printLog(number, false);
                }

                if (rows.length > 0 && rows[0].count > 0 && criterion === "NOT_REFERRED") {
                    skippedNumbers.push(number);
                    return printLog(number, true);
                } else if (rows.length > 0 && rows[0].count == 0 && criterion === "REFERRED_AT_LEAST_ONCE") {
                    skippedNumbers.push(number);
                    return printLog(number, true);
                } else if (rows.length > 0 && rows[0].count < 3 && criterion === "REFERRED_AT_LEAST_3_TIMES") {
                    skippedNumbers.push(number);
                    return printLog(number, true);
                } else if (rows.length > 0 && rows[0].count >= 3 && criterion === "REFERRED_LESS_3_TIMES") {
                    skippedNumbers.push(number);
                    return printLog(number, true);
                }

                executeModeAction(cache);
            });
        }

        var checkPortedIn = function (cache) {
            if (criterion === "ALL") {
                return checkReferrals(cache);
            }

            if (criterion === "NOT_PORTED_IN" || criterion === "PORTED_IN") {
                ec.getCustomerDetailsNumber(prefix, number, false, function (err, cache) {
                    if (!cache) {
                        errorNumbers.push(number);
                        errorReason[number] = "EC NOT FOUND ON STEP 2";
                        return printLog(number, true);
                    }

                    if (cache.ported && criterion === "NOT_PORTED_IN") {
                        skippedNumbers.push(number);
                        return printLog(number, true);
                    } else if (!cache.ported && criterion === "PORTED_IN") {
                        skippedNumbers.push(number);
                        return printLog(number, true);
                    }

                    checkReferrals(cache);
                });
            } else {
                return checkReferrals(cache);
            }
        }

        var loadCache = function () {
            if (external || (input === "DB" && (criterion === "GRAB_SIGN_UP" || criterion === "EXTERNAL_GRAB"))) {
                executeModeAction({
                    prefix: prefix,
                    number: number,
                    email: email,
                    name: name,
                    referral_code: referral_code
                });
            } else {
                ec.getCustomerDetailsNumber(prefix, number, false, function (err, cache) {
                    if (err) {
                        errorNumbers.push(number);
                        errorReason[number] = "EC ERROR";
                        return printLog(number, true);
                    }

                    if (!cache) {
                        errorNumbers.push(number);
                        errorReason[number] = "CUSTOMER NO FOUND";

                        // no need to wait since customer info is loaded from db and not
                        return printLog(number, false);
                    }

                    checkPortedIn(cache);
                }, true);
            }
        }

        var checkDuplicates = function () {
            if (activity && !allowDuplicates) {
                var startData = new Date().getTime() - 5 * 24 * 60 * 60 * 1000;
                db.notifications_logbook_get({"number": number, "activity": activity, "ts": {"$gt": startData}},
                    {}, {"ts": -1}, function (logs) {
                        var exists = logs && logs.length > 0;
                        if (exists) {
                            skippedNumbers.push(number);
                            return printLog(number, false);
                        }

                        loadCache();
                    });
            } else {
                loadCache();
            }
        }

        checkDuplicates();
    });
}

setTimeout(function () {
    var tasks = [];
    console.log(tag, "Load customers numbers");

    var processNumbers = function (err, list) {
        console.log(tag, "LOADED[" + (list ? list.length : "0" + (err ? ", err=" + err : "")) + "]");
        if (list) {
            var max = list.length;

            for (var i = 0; i < max; i++) {
                addSendTask(activity, tasks, i, list[i])
            }

            var parallelOperations = 10;
            if (mode === "AGGREGATE_BONUSES" || mode === "ADD_BONUS") {
                parallelOperations = 5;
            }

            console.log(tag, "-------------------------------------");
            console.log(tag, "COUNT[" + tasks.length + "]");

            async.parallelLimit(tasks, parallelOperations, function (errGroup, resultGroup) {
                if (successNumbers.length + errorNumbers.length + skippedNumbers.length == max) {

                    console.log(tag, "-------------------------------------");
                    console.log(tag, "FAILED[" + errorNumbers.length + "]");
                    errorNumbers.forEach(function (item) {
                        console.log(tag, item + " - " + errorReason[item]);
                    });

                    console.log(tag, "-------------------------------------");
                    console.log(tag, "SKIPPED[" + skippedNumbers.length + "]");
                    skippedNumbers.forEach(function (item) {
                        console.log(tag, item);
                    });

                    if (mode === "PRINT_EMAIL" || mode === "PRINT_SIN" || mode === "PRINT_NUMBER_EMAIL"
                        || mode === "PRINT_NUMBER_EMAIL_CODE") {
                        console.log(tag, "-------------------------------------");
                        console.log(tag, "INFO[" + successInfo.length + "]");
                        successInfo.forEach(function (item) {
                            console.log(tag, item);
                        });
                    }

                    console.log(tag, "-------------------------------------");
                    console.log(tag, "SUCCESS[" + successNumbers.length + "]");

                    if (mode === "AGGREGATE_BONUSES") {
                        successNumbers.forEach(function (item) {
                            console.log(tag, item);
                        });
                    }
                    process.exit(0);
                }
            });
        } else {
            process.exit(-1);
        }
    }

    if (number && input === "TERMINAL") {
        processNumbers(undefined, [{prefix: "65", number: number}]);
    } else if (fileName && input === "FILE") {
        readFile(processNumbers);
    } else {
        if (input === "DB" && criterion === "REFERRED_AT_LEAST_ONCE") {
            loadReferredOnceNumbers(processNumbers);
        } else if (input === "DB" && criterion === "REFERRED_AT_LEAST_3_TIMES") {
            loadReferred3TimesNumbers(processNumbers);
        } else if (input === "DB" && criterion === "LEADERBOARD_TOP") {
            leaderBoardTop(processNumbers);
        } else if (input === "DB" && criterion === "GRAB_SIGN_UP") {
            loadGrabSingUp(processNumbers);
        } else if (input === "DB" && criterion === "EXTERNAL_GRAB") {
            loadExternalCustomersWithCode("grab", processNumbers);
        } else if (input === "DB" && criterion === "HAS_BONUS") {
            loadAllWithBonus(processNumbers);
        } else if (input === "DB" && criterion === "LEADERBOARD_REACHED_GB") {
            leaderBoardGB(processNumbers);
        } else {
            loadSelfcareNumbers(processNumbers);
        }
    }

}, 2500);
