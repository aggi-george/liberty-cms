#!/usr/bin/env nodejs

var async = require('async');
var fs = require('fs');

var config = require('../../config');
//config.DEV = false;
config.EC_LOGSENABLED = false;
config.LOGSENABLED = false;

var ec = require('../../lib/elitecore');
var elitecoreConnector = require('../../lib/manager/ec/elitecoreConnector');
var db = require('../../lib/db_handler');
var common = require('../../lib/common');

var mode = "UPDATE_ID";
var testCount = -1;
var timeoutDelay = 60 * 1000;
var updateDelay = 5 * 1000;
var parallel = 20;

function finish() {
    process.exit(0);
}

var ignoredActivity = ["payment_successful", "payment_failed_on_ecom", "welcome_email", "customer_request_voicemail",
    "payment_unsuccessful", "delivery_info", "document_approval", "order_trackable_reminder", "rescheduled_delivery",
    "failed_doc_disapproved", "cacti_send_sms", "delivery_failed_notification_experts", "cart_abandoned_delivery_info",
    "daily_delivery_reminder_report", "delivery_cancelled_order", "delivery_failed", "delivery_failed_notification_experts",
    "delivery_failed_port_in_cancellation", "delivery_failed_reminder_final", "delivery_failed_survey",
    "delivery_failed_survey_temporary", "grab_sms", "delivery_info", "delivery_info_new_approved", "delivery_slot_full",
    "delivery_success", "predelivery_changes", "rescheduled_delivery_approved", "schedule_redelivery_failure",
    "cancel_doc_disapproved", "doc_cancelled_order", "document_approval", "document_reminder",
    "failed_doc_disapproved", "portin_failure_id_mismatch_doc", "generic_report", "daily_delivery_reminder_report",
    "daily_phone_inventory_report", "daily_port_in_report", "daily_scheduler_report", "daily_tracking_reminder_report",
    "cart_abandoned_base_plan", "cart_abandoned_delivery_info", "cart_abandoned_new_number", "cart_abandoned_phone",
    "cart_abandoned_ported", "forgot_password_email", "incomplete_shipment", "invite_email", "no_transaction_one_week",
    "no_transaction_one_month", "no_transaction_after_login", "partner_grab_code_usage", "partner_grab_share_code",
    "partner_grab_share_code_reminder", "partner_grab_welcome", "password_change", "payment_failure_survey",
    "payment_failure_survey_reminder", "payment_successful", "payment_successful_no_installment", "payment_successful_phone",
    "payment_unsuccessful", "payment_unsuccessful_reason", "reg_not_approved", "release_warning", "rescheduled_delivery_approved"];

function removeFromAllTables(match, callbak, status) {
    var removed = 0;
    db.notifications_bills.remove(match, function (err, result) {
        if (status) status("BILLS_REMOVED");
        removed += result.result.n;
        db.notifications_webhook.remove(match, function (err, result) {
            removed += result.result.n;
            if (status) status("EXTERNAL_REMOVED");
            db.notifications_emails.remove(match, function (err, result) {
                removed += result.result.n;
                if (status) status("EMAIL_REMOVED");
                db.notifications_sms.remove(match, function (err, result) {
                    removed += result.result.n;
                    if (status) status("SMS_REMOVED");
                    db.notifications_logbook.remove(match, function (err, result) {
                        removed += result.result.n;
                        if (status) status("ALL_REMOVED");
                        callbak(undefined, removed);
                    });
                });
            });
        });
    });
}

function updateForAllTables(match, params, callback, status) {
    var updated = 0;
    db.notifications_bills.update(match, {"$set": params}, {multi: true}, function (err, result) {
        updated += result.result.n;
        if (status) status("BILLS_UPDATED");
        db.notifications_webhook.update(match, {"$set": params}, {multi: true}, function (err, result) {
            updated += result.result.n;
            if (status) status("EXTERNAL_UPDATED");
            db.notifications_emails.update(match, {"$set": params}, {multi: true}, function (err, result) {
                updated += result.result.n;
                if (status) status("EMAIL_UPDATED");
                db.notifications_sms.update(match, {"$set": params}, {multi: true}, function (err, result) {
                    updated += result.result.n;
                    if (status) status("SMS_UPDATED");
                    db.notifications_logbook.update(match, {"$set": params}, {multi: true}, function (err, result) {
                        updated += result.result.n;
                        if (status) status("ALL_UPDATED");
                        if (callback) callback(undefined, updated);
                    });
                });
            });
        });
    });
}

function clearOld() {
    console.log("loading plan switch notifications...");

    db.notifications_logbook.find({
        activity: "validate_base_plan"
    }).toArray(function (err, rows) {
        if (err) {
            console.error("error " + err.message);
            process.exit(-1);
        }

        var map = {};
        var numbers = [];
        var duplicates = [];

        console.log("loaded plan switch notifications, count=" + rows.length);

        rows.forEach(function (item) {
            var ts = parseInt(item.ts);
            if (numbers.indexOf(item.number) == -1) {
                numbers.push(item.number);
                map[item.number] = {ts: ts};
            } else if (map[item.number].ts < ts) {
                map[item.number].ts = ts;
                duplicates.push(item.number);
            } else {
                duplicates.push(item.number);
            }
        });

        var interval = setInterval(function () {
            console.log("remove old notifications... removed " + handled + " / " + numbers.length);
        }, updateDelay);

        var removed = 0;
        var handled = 0;
        var errorsRemove = [];
        var addTask = function (tasks, number, ts) {
            tasks.push(function (callbak) {

                new Promise(
                    function (resolve, reject) {
                        var timeout = setTimeout(function () {
                            errorsRemove.push(number);
                            handled++;
                            console.error("timeout on remove old, number=" + number);
                            reject(new Error("Timeout"));
                        }, timeoutDelay);

                        if (testCount > 0) console.log("remove old notifications, number=" + number);
                        removeFromAllTables({
                            number: number, ts: {"$lt": (ts - 1000)}, activity: {"$nin": ignoredActivity}
                        }, function (err, count) {
                            handled++;
                            removed += count;

                            if (err) {
                                errorsRemove.push(number);
                                clearTimeout(timeout);
                                reject(err);
                            } else {
                                clearTimeout(timeout);
                                resolve();
                            }
                        });
                    }).then(function (result) {
                        callbak();
                    }).catch(function (err) {
                        callbak();
                    });
            });
        }

        var tasks = [];
        numbers.forEach(function (item) {
            if (!testCount || testCount == -1 || tasks.length < testCount) {
                addTask(tasks, item, map[item].ts);
            }
        });

        console.log("removing old notifications... count=" + tasks.length + ", duplicates="
        + duplicates.length + ", " + JSON.stringify(duplicates));

        async.parallelLimit(tasks, parallel, function (err, result) {
            console.log("remove old notifications... removed " + handled + " / " + numbers.length);
            console.log("failed to remove, numbers=", JSON.stringify(errorsRemove));
            console.log("removed old notifications, removed=" + removed);
            clearInterval(interval);
            finish();
        });
    });
}

function updateNumbersInfo(numbers) {
    console.log("updating account info...");

    var map = {};
    var found = [];
    var notFound = [];
    var errors = [];
    var errorsRemove = [];
    var updated = 0;
    var removed = 0;
    var notificationsUpdated = 0;
    var notificationsRemoved = 0;

    var interval = setInterval(function () {
        console.log("update account info... updated " + updated + " / " + numbers.length
        + ", removed " + removed + " / " + notFound.length);
    }, updateDelay);

    var addTaskToUpdate = function (tasks, number) {
        tasks.push(function (callbak) {

            new Promise(
                function (resolve, reject) {
                    var currentStatus = "NONE";

                    var timeout = setTimeout(function () {
                        updated++;
                        errors.push(number);
                        console.error("timeout on update, number=" + number +
                        ", sin=" + (map[number] ? map[number].serviceInstanceNumber : undefined) +
                        ", currentStatus=" + currentStatus);
                        reject(new Error("Timeout"));
                    }, timeoutDelay);

                    ec.getCustomerDetailsNumber("65", number, false, function (err, cache) {
                        if (err) {
                            updated++;
                            errors.push(number);
                            clearTimeout(timeout);
                            reject(err);
                        } else if (!cache) {
                            if (testCount > 0) console.log("double-checking not found number=" + number);

                            setTimeout(function () {
                                elitecoreConnector.searchAccount(number, undefined, undefined, undefined, function (err, result) {
                                    clearTimeout(timeout);
                                    updated++;

                                    if (err || !result || !result.return) {
                                        errors.push(number);
                                        reject(new Error("Error"));
                                        return;
                                    }

                                    var accountDetails = result.return.searchAccountResponseVO;
                                    if (!accountDetails || !accountDetails[0] || !accountDetails[0].accountNumber) {
                                        if (testCount > 0) console.log("not found number=" + number);
                                        notFound.push(number);

                                        removeFromAllTables({
                                            number: number, activity: {"$nin": ignoredActivity},
                                            serviceInstanceNumber: {"$exists": false}
                                        }, function (err, removedCount) {
                                            removed++;
                                            notificationsRemoved += removedCount;
                                            if (err) {
                                                errorsRemove.push(number);
                                                clearTimeout(timeout);
                                                reject(err);
                                            } else {
                                                clearTimeout(timeout);
                                                resolve();
                                            }
                                        }, function (status) {
                                            currentStatus = status;
                                        });
                                        return;
                                    }

                                    console.error("ERROR: not found in DB number " + number + " was found via API, "
                                    + (accountDetails[0] && accountDetails[0].accountNumber
                                        ? "accountNumber=" + accountDetails[0].accountNumber +
                                    ", accountStatus=" + accountDetails[0].accountStatus
                                        : "accountDetails=" + JSON.stringify(accountDetails[0])));

                                    errors.push(number);
                                    reject(new Error("Error"));
                                });
                            }, 1000);

                        } else {
                            found.push(number);
                            map[number] = {
                                customerAccountNumber: cache.customerAccountNumber,
                                billingAccountNumber: cache.billingAccountNumber,
                                serviceInstanceNumber: cache.serviceInstanceNumber
                            }

                            if (testCount > 0) console.log("update account info, number=" + number +
                            ", sin=" + map[number].serviceInstanceNumber);

                            updateForAllTables({
                                "$or": [{number: number}, {to: number}],
                                serviceInstanceNumber: {"$exists": false}
                            }, map[number], function (err, notificationsCount) {
                                updated++;
                                notificationsUpdated += notificationsCount;
                                clearTimeout(timeout);
                                resolve();
                            }, function (status) {
                                currentStatus = status;
                            });
                        }
                    }, true);

                }).then(function (result) {
                    callbak();
                }).catch(function (err) {
                    callbak();
                });
        });
    }

    var tasks = [];
    numbers.forEach(function (item) {
        if (!testCount || testCount == -1 || tasks.length < testCount) {
            addTaskToUpdate(tasks, item);
        }
    });

    async.parallelLimit(tasks, parallel, function (err, result) {
        clearInterval(interval);
        console.log("update account info... updated " + updated + " / " + found.length
        + ", removed " + removed + " / " + notFound.length);

        console.log("failed to update, numbers=", JSON.stringify(errors));
        console.log("failed to remove, numbers=", JSON.stringify(errorsRemove));

        console.log("loaded account info, found=" + found.length +
        ", updated notifications=" + notificationsUpdated + ", not found=" + notFound.length +
        ", removed notifications=" + notificationsRemoved + ", errors=" + errors.length);

        finish();
    });
}

function loadDistinctNumbers() {
    console.log("loading numbers...");
    db.notifications_logbook.distinct("number", {activity: {"$nin": ignoredActivity}}, function (err, rows) {
        if (err) {
            console.error("error " + err.message);
            process.exit(-1);
        }

        console.log("loaded all notifications, count=" + rows.length);

        var numbers = [];

        rows.forEach(function (item) {
            if (item && item.length == 8) {
                if (numbers.indexOf(item) == -1) {
                    numbers.push(item);
                }
            }
        });

        console.log("loaded unique numbers, count=" + numbers.length);
        updateNumbersInfo(numbers);
    });
}

function loadNotUpdatedNumber() {
    console.log("loading not updated numbers...");
    db.notifications_logbook.find({
        activity: "validate_base_plan",
        serviceInstanceNumber: {"$exists": false}
    }).toArray(function (err, rows) {
        if (err) {
            console.error("error " + err.message);
            process.exit(-1);
        }

        console.log("loaded all notifications, count=" + rows.length);

        var numbers = [];

        rows.forEach(function (item) {
            if (item.number && item.number.length == 8) {
                if (numbers.indexOf(item.number) == -1) {
                    numbers.push(item.number);
                }
            }
        });

        console.log("loaded unique numbers, count=" + numbers.length);
        updateNumbersInfo(numbers);
    });
}

console.log("init...");
setTimeout(function () {
    if (mode == "CLEAR_OLD") {
        clearOld();
    } else if (mode == "UPDATE_ID") {
        loadNotUpdatedNumber();
    }
}, 2000);
