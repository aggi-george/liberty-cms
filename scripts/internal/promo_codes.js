#!/usr/bin/env nodejs

var async = require('async');
var fs = require('fs');

var config = require('../../config');
config.EC_LOGSENABLED = false;

var ec = require('../../lib/elitecore');
var db = require('../../lib/db_handler');
var common = require('../../lib/common');

var promotionsManager = require('../../src/core/manager/promotions/promotionsManager');

var tag = "SCRIPT[PROMO]";
var delay = 50;

args = [];
var prefix,
    categoryId,
    length = 0,
    startTime,
    endTime,
    maxLimit = 0,
    regDiscount = 0,
    waiverCents = 0,
    waiverMonths = 0,
    productId,
    productDuration = 0,
    generateCount = 0,
    enabled = 0;

process.argv.forEach(function (val, index, array) {
    args.push(val);
});

function printHelp(errorMessage) {
    if (errorMessage) {
        console.log(tag, "ERROR: " + errorMessage);
        console.log(tag, "-------------------------------------");
    }

    console.log(tag, "Syntax");
    console.log(tag, "-gc  <mandatory:GENERATE_COUNT>");
    console.log(tag, "-p   <mandatory:PREFIX>");
    console.log(tag, "-c   <mandatory:CATEGORY_ID>");
    console.log(tag, "-l   <mandatory:LENGTH>");
    console.log(tag, "-s   <optional:START>             [DEFAULT: none]");
    console.log(tag, "-e   <optional:END>               [DEFAULT: none]");
    console.log(tag, "-ml  <optional:MAX_LIMIT>         [DEFAULT: 0][SUPPORTED: 1, 2, ..., N; -1 -> unlimited]");
    console.log(tag, "-rd  <optional:REG_DISCOUNT>      [DEFAULT: 0]");
    console.log(tag, "-wa  <optional:WAIVER_CENTS>      [DEFAULT: 0]");
    console.log(tag, "-wc  <optional:WAIVER_COUNT>      [DEFAULT: 0][SUPPORTED: 0 -> 1 immediate; 1, 2, ..., N -> N count in the end of a bill cycle]");
    console.log(tag, "-pr  <optional:PRODUCT_ID>        [DEFAULT: none]");
    console.log(tag, "-pd  <optional:PRODUCT_DURATION>  [DEFAULT: 1][SUPPORTED: 1 -> 1 month; 2 -> 2 month; -1 -> recurrent]");
    console.log(tag, "-ed  <optional:ENABLED>           [DEFAULT: 0][SUPPORTED: 1 -> enabled; 0 -> disabled]");
    console.log(tag, "-tg  <optional:TAG>               [DEFAULT: 'SCRIPT[PROMO]]'");
    console.log(tag, "-d   <optional:DELAY_MS>          [DEFAULT: 50]");
    process.exit(1);
}

function initParam(key, value) {
    if (!value) {
        if (key.trim() == "-tg") {
            tag = "";
        }
        return;
    }

    if (key.trim() == "-p") {
        prefix = value;
    } else if (key.trim() == "-l") {
        length = parseInt(value);
    } else if (key.trim() == "-c") {
        categoryId = parseInt(value);
    } else if (key.trim() == "-s") {
        startTime = new Date(value);
    } else if (key.trim() == "-e") {
        endTime = new Date(value);
    } else if (key.trim() == "-ml") {
        maxLimit = parseInt(value);
    } else if (key.trim() == "-rd") {
        regDiscount = parseInt(value);
    } else if (key.trim() == "-wa") {
        waiverCents = parseInt(value);
    } else if (key.trim() == "-wc") {
        waiverMonths = parseInt(value);
    } else if (key.trim() == "-pr") {
        productId = value;
    } else if (key.trim() == "-pd") {
        productDuration = parseInt(value);
    } else if (key.trim() == "-ed") {
        enabled = parseInt(value);
    } else if (key.trim() == "-gc") {
        generateCount = parseInt(value);
    } else if (key.trim() == "-tg") {
        tag = value;
    } else if (key.trim() == "-d") {
        delay = parseInt(value);
    }
}

for (var i = 2; i < args.length; i += 2) {
    initParam(args[i], args[i + 1]);
}

//console.log(tag, "prefix=" + prefix + ", categoryId=" + categoryId + ", length=" + length
//+ ", startTime=" + startTime + ", endTime=" + endTime + ", maxLimit=" + maxLimit + ", regDiscount=" + regDiscount
//+ ", waiverCents=" + waiverCents + ", waiverMonths=" + waiverMonths + ", productId=" + productId
//+ ", productDuration=" + productDuration + ", generateCount=" + generateCount + ", enabled=" + enabled);

if (!prefix || !categoryId || !length || !generateCount) {
    printHelp("some mandatory params are missing");
    process.exit(1);
}

var codes = [];
var errors = [];

function addGenerateTask(tasks, i) {
    var callbackCalled = false;
    tasks.push(function (callback) {

        var printLog = function (status, code, extra, wait) {
            console.log(tag, status + " - i=" + i + ", code=" + code + ", success=" + codes.length
            + ", errors=" + errors.length + (extra ? ", extra=" + extra : ""));

            if (wait) {
                setTimeout(function () {
                    if (!callbackCalled) {
                        callbackCalled = true;
                        callback();
                    }
                }, delay);
            } else {
                if (!callbackCalled) {
                    callbackCalled = true;
                    callback();
                }
            }
        }

        promotionsManager.generateAPromoCode(prefix, length, {
            categoryId: categoryId,
            productId: productId,
            max: maxLimit,
            waiverCents: waiverCents,
            waiverMonths: waiverMonths,
            regDiscountCents: regDiscount,
            deviceDiscountCents: 0,
            monthsCount: productDuration,
            skuName1: undefined,
            skuKey1: undefined,
            skuQuantity1: 0,
            freeAddon1: undefined,
            freeAddonMonths1: undefined,
            secondVerificationType: undefined,
            enabled: 0,
            start: startTime,
            end: endTime,
            bonus_data: 0,
            bonus_data_type: undefined
        }, function (err, result) {
            if (err) {
                errors.push(err.message);
                return printLog("ERROR", "NONE", err.message);
            }

            codes.push(result.code);
            return printLog("SUCCESS", result.code);
        });
    });
}

setTimeout(function () {
    var query = "SELECT id, name, type, partner FROM promoCodesCategories WHERE id=" + db.escape(categoryId);
    db.query_err(query, function (err, rows) {
        if (err) {
            console.error(tag, "loading category, error=" + err.message);
            process.exit(-1);
        }

        var category = rows ? rows[0] : undefined;
        if (!category) {
            console.error(tag, "category not found, categoryId=" + categoryId);
            process.exit(-1);
        }
        console.log(tag, "generate " + generateCount + " code(s) for category '" + category.name + "'");

        var tasks = [];
        for (var i = 0; i < generateCount; i++) {
            addGenerateTask(tasks, i)
        }

        console.log(tag, "-------------------------------------");
        console.log(tag, "COUNT[" + tasks.length + "]");

        var parallelOperations = 1;
        async.parallelLimit(tasks, parallelOperations, function (errGroup, resultGroup) {
            console.log(tag, "-------------------------------------");
            console.log(tag, "FAILED[" + errors.length + "]");
            errors.forEach(function (item) {
                console.log(tag, "ERROR: " + item);
            });

            console.log(tag, "-------------------------------------");
            console.log(tag, "CODES[" + codes.length + "]");
            codes.forEach(function (item) {
                console.log(tag, item);
            });
            process.exit(0);
        });
    });
}, 1500);
