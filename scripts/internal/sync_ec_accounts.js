#!/usr/bin/env nodejs

require('../../bin/global');
const async = require('async');
const db = require(`${__lib}/db_handler`);
const common = require(`${__lib}/common`);
const config = require(`${__base}/config`);
const ecPackagesService = require(`${__lib}/manager/ec/elitecorePackagesService`);

const caamSchema = (config.DEV) ? 'crestelcaamtestlwp' : 'crestelcaamlwp';
const ocsSchema = (config.DEV) ? 'crestelocstestlwp' : 'crestelocslwp';
let processed = 0;

const DEBUG = false;

function syncAccounts(bases, callback) {
    let decodeString = '';
    let whereString = '';
    bases.objs.forEach((o) => {
        decodeString += `${db.escape(o.id)},${db.escape(o.name)},`;
        whereString += `${db.escape(o.id)},`;
    });
    decodeString += db.escape('Unknown');
    const queryAccounts = `SELECT * FROM (SELECT hier.ca_number customeraccount, hier.ba_number billingaccount, hier.sa_number serviceaccount,
        hier.si_number serviceinstanceaccount, hier.emailid, TRIM(hier.name) customername, hier.activationdate, macct.createdate, macct.accountsubstatusid,
        DECODE (macct.accountsubstatusid,
            'ACS01', 'Active',
            'ACS06', 'Suspended',
            'ACS07', 'Terminated') accountstatus,
        macctprofile_sa.strcustom1 referralcode, macctprofile_sa.strcustom7 orderrefno, macctprofile_sa.datecustom5 joindate,
        macctprofile_ca.datecustom1 birthday, macctprofile_ca.strcustom3 portinstatus, macctprofile_ca.strcustom2 nric,
        DECODE (phistory.packageid, ${decodeString}) baseplan, ca_id, ba_id, sa_id, si_id,
        RANK() OVER (PARTITION BY hier.si_number ORDER BY phistory.fromdate desc) position
        FROM ${ocsSchema}.ab_hierarchy hier LEFT JOIN ${caamSchema}.tblmaccount macct ON macct.accountid=hier.si_id
        LEFT JOIN ${caamSchema}.tblmaccountprofile macctprofile_sa ON macctprofile_sa.accountid=hier.sa_id
        LEFT JOIN ${caamSchema}.tblmaccountprofile macctprofile_ca ON macctprofile_ca.accountid=hier.ca_id
        LEFT JOIN ${caamSchema}.tbltcustomerpackagehistory phistory ON hier.si_id=phistory.accountid
        WHERE phistory.packageid in (${whereString.slice(0,-1)})) s
        WHERE s.position=1`;
    if (DEBUG) console.log(queryAccounts);
    db.oracle.query(queryAccounts, { resultSet: true }, (e,r)=>{
        if (e) {
            console.error(e);
            callback();
        } else if ( r && r.rows && (r.rows.length > 0) ) {
            console.log('accounts total,' + r.rows.length);
            let insertQ = async.queue(function(task, cb) {
                if (!task) return cb();
                const insertAccounts = `INSERT INTO ecAccounts (customeraccount, billingaccount, serviceaccount, serviceinstanceaccount, emailid, customername,
                    activationdate, createdate, accountsubstatusid, accountstatus, referralcode, orderrefno, joindate, birthday, portinstatus, nric, baseplan) VALUES
                    ${task.chunk.slice(0, -1)}
                    ON DUPLICATE KEY UPDATE
                    emailid=VALUES(emailid),
                    customername=VALUES(customername),
                    createdate=VALUES(createdate),
                    accountsubstatusid=VALUES(accountsubstatusid),
                    accountstatus=VALUES(accountstatus),
                    referralcode=VALUES(referralcode),
                    orderrefno=VALUES(orderrefno),
                    joindate=VALUES(joindate),
                    activationdate=VALUES(activationdate),
                    birthday=VALUES(birthday),
                    portinstatus=VALUES(portinstatus),
                    nric=VALUES(nric),
                    baseplan=VALUES(baseplan)`;
                if (DEBUG) console.log(insertAccounts);
                db.query_err(insertAccounts, function(e, r) {
                    if (e) console.error(e);
                    processed += (r && r.message) ? parseInt(r.message.substring(10).split('  ')[0]) : 0;
                    cb();
                });
            }, 3);
            let chunk = '';
            r.rows.forEach((item, idx)=>{
                chunk += `(
                ${db.escape(item[0])},
                ${db.escape(item[1])},
                ${db.escape(item[2])},
                ${db.escape(item[3])},
                ${db.escape(item[4])},
                ${db.escape(item[5])},
                TIMESTAMP(${db.escape(item[6])}),
                TIMESTAMP(${db.escape(item[7])}),
                ${db.escape(item[8])},
                ${db.escape(item[9])},
                ${db.escape(item[10])},
                ${db.escape(item[11])},
                DATE(${db.escape(item[12])}),
                ${db.escape(item[13])},
                ${db.escape(item[14])},
                ${db.escape(item[15])},
                ${db.escape(item[16])}),`;
                if (idx && (idx % 1000 == 0)) {
                    insertQ.push({ chunk: chunk });
                    chunk = '';
                }
            });
            if (chunk.length > 0) {
                insertQ.push({ chunk: chunk });
                chunk = '';
            }
            insertQ.drain = () => {
                console.log(`accounts done,${processed}`);
                callback();
            }
        } else {
            callback();
        }
    });
}

ecPackagesService.loadHierarchy((err, hierarchy) => {
    if (!hierarchy) {
        console.error('Cannot load hierarchy');
        return process.exit(1);
    }
    const basesArray = hierarchy.base.map((o) => {
            return o.options.map((b) => {
                return { id: b.id, name: b.name };
            });
    }).filter((o) => { return o });
    let bases = { objs: [] };
    basesArray.forEach((o) => {
        o.forEach((i) => {
            bases.objs.push({ id: i.id, name: i.name });
            bases[i.id] = i.name;
        });
    });
    syncAccounts(bases, ()=>{
        process.exit();
    });
});
