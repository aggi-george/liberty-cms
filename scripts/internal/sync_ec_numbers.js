#!/usr/bin/env nodejs

require('../../bin/global');
const async = require('async');
const db = require(`${__lib}/db_handler`);
const config = require(`${__base}/config`);

const DEBUG = false;
let processed = 0;

const caamSchema = (config.DEV) ? 'crestelcaamtestlwp' : 'crestelcaamlwp';
const ocsSchema = (config.DEV) ? 'crestelocstestlwp' : 'crestelocslwp';

let testQ = async.queue((task, callback)=>{
    processed++;
    const selectQ = `SELECT * FROM ecNetworkServiceInstance WHERE
        subscriberidentifier=${db.escape(task.subscriberidentifier)} AND
        serviceinstanceaccount=${db.escape(task.serviceinstanceaccount)}`;
    db.query_err(selectQ, (sErr, sRes)=>{
        if (sErr) {
            console.log(sErr);
            return callback();
        }
        if (sRes && sRes.length==0) {
            if (task.numberstatus=='Active') {
                // this is not expected to hit duplicate entry but if it did, overwrite
                let insertQ = `INSERT INTO ecNetworkServiceInstance (serviceinstanceaccount, subscriberidentifier,
                    commonstatusid, lastmodifieddate, numberstatus) VALUES (
                    ${db.escape(task.serviceinstanceaccount)},
                    ${db.escape(task.subscriberidentifier)},
                    ${db.escape(task.commonstatusid)},
                    ${db.escape(task.lastmodifieddate)},
                    ${db.escape(task.numberstatus)}) ON DUPLICATE KEY UPDATE
                    commonstatusid=VALUES(commonstatusid),
                    lastmodifieddate=VALUES(lastmodifieddate),
                    numberstatus=VALUES(numberstatus)`;
                if (DEBUG) console.log(insertQ);
                db.query_err(insertQ,(e,r)=>{
                    if (e) console.log(e);
                    callback();
                });
            } else {
                // if not active, ignore
                callback();
            }
        } else if (sRes && sRes.length==1) {
            if (sRes[0].numberstatus != task.numberstatus) {
                let updateQ = `UPDATE ecNetworkServiceInstance SET
                    commonstatusid=${db.escape(task.commonstatusid)},
                    lastmodifieddate=${task.lastmodifieddate ? db.escape(task.lastmodifieddate) : 'NULL'},
                    numberstatus=${db.escape(task.numberstatus)}
                    WHERE serviceinstanceaccount=${db.escape(task.serviceinstanceaccount)} AND
                    subscriberidentifier=${db.escape(task.subscriberidentifier)}`;
                if (DEBUG) console.log(updateQ);
                db.query_err(updateQ,(e,r)=>{
                    if (e) console.log(e);
                    callback();
                });
            } else {
                // ignore since same status
                callback();
            }
        } else {
            // this will never happen since unique key, but still, go
            callback();
        }
    });
}, 3);

const queryNetworkServiceInstance = `SELECT * FROM (SELECT h.si_number, n.subscriberidentifier, n.commonstatusid, n.lastmodifieddate,
    DECODE (n.commonstatusid,
        'CST01', 'Active',
        'CST03', 'Damaged',
        'CST04', 'Disconnected',
        'Other') numberstatus, RANK() OVER (PARTITION BY h.si_number, n.subscriberidentifier ORDER BY n.lastmodifieddate desc) position
    FROM ${caamSchema}.tbltnetworkserviceinstance n, ${ocsSchema}.ab_hierarchy h WHERE h.si_id=n.serviceinstanceid) s WHERE s.position=1`;
if (DEBUG) console.log(queryNetworkServiceInstance);
db.oracle.query(queryNetworkServiceInstance, { resultSet: true }, (e,r)=>{
    const total = r && r.rows ? r.rows.length : 0;
    console.log(`numbers total,${total}`);
    if (r && r.rows) r.rows.forEach((o)=>{
        testQ.push({
            serviceinstanceaccount: o[0],
            subscriberidentifier: o[1],
            commonstatusid: o[2],
            lastmodifieddate: o[3],
            numberstatus: o[4]
        });
    });
    testQ.drain = ()=>{
        console.log(`numbers done,${processed}`);
        process.exit();
    };
});
