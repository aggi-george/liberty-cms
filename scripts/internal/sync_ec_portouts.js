#!/usr/bin/env nodejs

require('../../bin/global');
var async = require('async');
var db = require(__lib + '/db_handler');
var common = require(__lib + '/common');
var config = require(__base + '/config');
var portoutManager = require(__manager + '/porting/portOutManager');

var systemSchema = (config.DEV) ? "crestelsystemtestlwp" : "crestelsystemlwp";

var LOGSQL = false;

var s;

var sTasks = {
    "request" : function(cb) { portoutRequest(cb) },
    "approved" : function(cb) { portoutApproved(cb) },
    "cancelled" : function(cb) { portoutCancelled(cb) },
    "success" : function(cb) { portoutSuccess(cb) },
    "failed" : function(cb) { portoutFailed(cb) },
};
async.series(sTasks, function(err, result) {
    if (err) console.log(err.message);
    s = true;
    if (s) process.exit(0);
});

function parseRemark(ts, remarkstr) {
    var remark = remarkstr.split('[')[1].split(']')[0].split(',');
    var keyval = { "ts" : ts };
    remark.forEach(function(o) {
        var key = o.split("=")[0];
        var val = o.split("=")[1];
        if (key) {
            key = key.trim();
            if (!val) val = "";
            else val = val.trim();
            keyval[key] = val;
            if (key == "NPORefID") {
                var donor = val.split('-')[0];
                keyval.donor_network_code = donor;
                keyval.donor_network = (donor == "001") ? "M1" :
                    (donor == "002") ? "StarHub" :
                    (donor == "003") ? "Singtel" : "UNKNOWN";
            }
        }
    });
    if (!keyval.MSISDN) {
        console.error("Request Parse Error " + ts + " " + remark); // test
        return undefined;
    } else {
        return keyval;
    }
}

function getRequest(number, ts, callback) {
    var select = "SELECT p.id, s.serviceinstanceaccount FROM customerPortOut p, " +
        " (SELECT n.serviceinstanceaccount, n.subscriberidentifier, c.id FROM ecNetworkServiceInstance n LEFT JOIN " +
        " customerProfile c ON n.serviceinstanceaccount=c.service_instance_number WHERE " +
        " n.subscriberidentifier=" + db.escape(number) + ") s WHERE " +
        " p.profile_id=s.id AND p.number=s.subscriberidentifier AND p.added_ts < " + db.escape(ts) + " ORDER BY added_ts DESC LIMIT 1";
    if (LOGSQL) console.log(select);
    db.query_err(select, function(err, rows) {
        if (err || !rows || !rows[0] || !rows[0].id) {
            console.error("Approved cannot find profile " + number);
            callback(new Error("Port out request not found"));
        } else callback(undefined, rows[0].id, rows[0].serviceinstanceaccount);
    });
}

function getAudit(maxcond, statuscond, type, callback) {
    var mCond = "";
    var oCond = "";
    if (statuscond && type) {
        mCond = " WHERE status=" + db.escape(statuscond);
        oCond = " remark LIKE 'MNPNOTIFICATION_REQUEST%' AND remark LIKE " + db.escape("%notificationType=" + type + "%");
    } else {
        oCond = " remark LIKE 'PORTOUTCUSTOMER_REQUEST%'";
    }
    var select = "SELECT MAX(" + maxcond + ") newest FROM customerPortOut " + mCond;
    if (LOGSQL) console.log(select);
    db.query_err(select, function(err, rows) {
        var max = rows && rows[0] && rows[0].newest && (rows[0].newest != "0000-00-00 00:00:00") ? common.getDateTime(rows[0].newest) : undefined;
        var query = "SELECT auditdate, remark FROM " + systemSchema + ".tbltsystemaudit WHERE " + oCond +
            (max ? " AND auditdate > TO_TIMESTAMP(" + db.escape(max) + ",'YYYY-MM-DD HH24:MI:SS')" : " AND TRUNC(auditdate) > TRUNC(SYSDATE - 7)") +
            " ORDER BY auditdate DESC";
        if (LOGSQL) console.log(query);
        db.oracle.query(query, { resultSet: true }, callback);
    });
}

function portoutRequest(callback) {
    var requestQ = async.queue(function(task, cb) {
        if (!task) return cb();
        var requestInsert = "INSERT INTO customerPortOut (profile_id, status, number, " +
            " donor_network_code, donor_network, added_ts, customer_name, customer_id_type, customer_id_number) " +
            " SELECT c.id, 'WAITING', " +
            db.escape(task.MSISDN) + ", " +
            db.escape(task.donor_network_code) + ", " +
            db.escape(task.donor_network) + ", " +
            db.escape(common.getDateTime(task.ts)) + ", " +
            db.escape(task.CustomerName) + ", " +
            db.escape(task.idType) + ", " + db.escape(task.customerId) + " FROM " +
            " customerProfile c LEFT JOIN ecNetworkServiceInstance n ON c.service_instance_number=n.serviceinstanceaccount " +
            " WHERE n.subscriberidentifier=" + db.escape(task.MSISDN) + " ORDER BY n.lastmodifieddate DESC LIMIT 1";
        if (LOGSQL) console.log(requestInsert);
        db.query_err(requestInsert, function() { cb() });
    }, 1);
    getAudit("added_ts", undefined, undefined, function(err, result) {
        if (err) return callback(err);
        else if (result && result.rows && result.rows.length === 0) return callback();
        console.log("portoutrequests total," + result.rows.length);
        result.rows.forEach(function(item) {
            requestQ.push(parseRemark(item[0], item[1]));
        });
        requestQ.drain = function() { callback() };
    });
}

function portoutApproved(callback) {
    var approvedQ = async.queue(function(task, cb) {
        if (!task) return cb();
        getRequest(task.MSISDN, task.ts, function(err, id, serviceInstanceNumber) {
            if (err) return cb();
            var approvedUpdate = "UPDATE customerPortOut SET " +
                " start_ts=" + db.escape(common.getDateTime(task.ts)) + ", " +
                " status='DONOR_APPROVED', " +
                " status_update_ts=" + db.escape(common.getDateTime(task.ts)) + " WHERE " +
                " status!='DONOR_APPROVED' AND end_ts='0000-00-00 00:00:00' AND id=" + id;
            if (LOGSQL) console.log(approvedUpdate);
            db.query_err(approvedUpdate, function(errUpdate, resultUpdate) {
                if (resultUpdate.changedRows === 0) return cb();
                else portoutManager.reattachInventory(task.MSISDN, serviceInstanceNumber, function(err, result) {
                    db.query_err("UPDATE customerPortOut SET warning_type=" + (result ? "NULL" : db.escape("REATTACH_ERROR")) +
                        " WHERE id=" + id, function() { cb() });
                });
            });
        });
    }, 1);
    getAudit("start_ts", "DONOR_APPROVED", "PortOut_Approved", function(err, result) {
        if (err) return callback(err);
        else if (result && result.rows && result.rows.length === 0) return callback();
        console.log("portoutapproved total," + result.rows.length);
        result.rows.forEach(function(item) {
            approvedQ.push(parseRemark(item[0], item[1]));
        });
        approvedQ.drain = function() { callback() };
    });
}

function portoutCancelled(callback) {
    var cancelledQ = async.queue(function(task, cb) {
        if (!task) return cb();
        getRequest(task.MSISDN, task.ts, function(err, id) {
            if (err) return cb();
            var cancelledUpdate = "UPDATE customerPortOut SET " +
               " end_ts=" + db.escape(common.getDateTime(task.ts)) + ", " +
               " status='CANCELED', " +
               " status_update_ts=" + db.escape(common.getDateTime(task.ts)) + " WHERE " +
               " id=" + id;
            if (LOGSQL) console.log(cancelledUpdate);
            db.query_err(cancelledUpdate, function() { cb() });
        });
    }, 1);
    getAudit("end_ts", "CANCELED", "PortOut_Cancelled", function(err, result) {
        if (err) return callback(err);
        else if (result && result.rows && result.rows.length === 0) return callback();
        console.log("portoutcancelled total," + result.rows.length);
        result.rows.forEach(function(item) {
            cancelledQ.push(parseRemark(item[0], item[1]));
        });
        cancelledQ.drain = function() { callback() };
    });
}

function portoutSuccess(callback) {
    var successQ = async.queue(function(task, cb) {
        if (!task) return cb();
        getRequest(task.MSISDN, task.ts, function(err, id, serviceInstanceNumber) {
            if (err) return cb();
            var successUpdate = "UPDATE customerPortOut SET " +
                " end_ts=" + db.escape(common.getDateTime(task.ts)) + ", " +
                " status='DONE', " +
                " status_update_ts=" + db.escape(common.getDateTime(task.ts)) + " WHERE " +
                " status!='DONE' AND id=" + id;
            if (LOGSQL) console.log(successUpdate);
            db.query_err(successUpdate, function(errUpdate, resultUpdate) {
                if (resultUpdate.changedRows === 0) return cb();
                else portoutManager.scheduleTermination(serviceInstanceNumber, function(err, result) {
                    db.query_err("UPDATE customerPortOut SET warning_type=" + (result ? "NULL" : db.escape("SCHEDULE_TERMINATION_ERROR")) +
                        " WHERE id=" + id, function() { cb() });
                });
            });
        });
    }, 1);
    getAudit("end_ts", "DONE", "PortOut_Success", function(err, result) {
        if (err) return callback(err);
        else if (result && result.rows && result.rows.length === 0) return callback();
        console.log("portoutsuccess total," + result.rows.length);
        result.rows.forEach(function(item) {
            successQ.push(parseRemark(item[0], item[1]));
        });
        successQ.drain = function() { callback() };
    });
}

function portoutFailed(callback) {
    var failedQ = async.queue(function(task, cb) {
        if (!task) return cb();
        getRequest(task.MSISDN, task.ts, function(err, id) {
            if (err) return cb();
            var error_status = task.statusMessage.split('|')[2];
            var failedUpdate = "UPDATE customerPortOut SET " +
               " end_ts=" + db.escape(common.getDateTime(task.ts)) + ", " +
               " status='FAILED', " +
               " error_status=" + db.escape(error_status) + ", " +
               " status_update_ts=" + db.escape(common.getDateTime(task.ts)) + " WHERE " +
               " id=" + id;
            if (LOGSQL) console.log(failedUpdate);
            db.query_err(failedUpdate, function() { cb() });
        });
    }, 1);
    getAudit("end_ts", "FAILED", "PortOut_Approval_Failed", function(err, result) {
        if (err) return callback(err);
        else if (result && result.rows && result.rows.length === 0) return callback();
        console.log("portoutfailed total," + result.rows.length);
        result.rows.forEach(function(item) {
            failedQ.push(parseRemark(item[0], item[1]));
        });
        failedQ.drain = function() { callback() };
    });
}
