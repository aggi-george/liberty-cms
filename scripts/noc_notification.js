#!/usr/bin/env nodejs
var async = require('async');
var request = require('request');
var db = require('../lib/db_handler');
var common = require('../lib/common');
var config = require('../config');
var errorDefinitions = require('./error_definitions');

var logsEnabled = config.LOGSENABLED;

args = []
process.argv.forEach(function (val, index, array)
{
    args.push(val);
});
if (args[2]) {
        common.log("Syntax", "./noc_notificaction.js");
        exit(1);
}

//setTimeout(function() {
var go = function(){
	var ref = new Date();
	var y = ref.getFullYear();
	var m = ref.getMonth();
	var d = ref.getDate();
	var h = ref.getHours();
	var min = ref.getMinutes();
	var now = new Date(y, m, d, h, min).getTime(); // start of current hour, script can run in mid of hour
	var last_min = now - (60 * 5 * 1000)
	var nextHour = now + (60 * 60 * 1000);
	var tasks = new Array();
        errorDefinitions.errorList.forEach(function (item) { 
		tasks.push(function (callback) {
			db.ocslogs.aggregate([ {"$match": {ERR : item[0], ETS: {"$gte" : last_min, "$lt": now}}},{ $group :{   _id : {msisdn : "$MSISDN", errors : "$ERR", errdesc : "$ERRDESC"}, total: {$sum  : 1}}} ]).toArray(function (err, result) {
				var sumOfTotal = 0;	
        	                var list = [];
        	                result.forEach(function (entry) { 
					sumOfTotal += entry.total
					list = list.concat([[entry._id.msisdn, entry._id.errors, entry._id.errdesc, entry.total]]);
				});
				var head_data = [ "MSISDN", "Error Code", "Error Description", "Count" ];
				var body_data = list;
				if (item[3] >= sumOfTotal) {
					var subject_data = "[NOC] INFO ALERT: OCS Error Found: " + item[0] + " - " + item[1];
				} else if (item[4] >= sumOfTotal) {
					 var subject_data = "[NOC] MINOR ALERT: OCS Error Found: " + item[0] + " - " + item[1];
				} else if (item[5] >= sumOfTotal){
					var subject_data = "[NOC] WARNING ALERT: OCS Error Found: " + item[0] + " - " + item[1];
				} else if (item[6] >= sumOfTotal){
					var subject_data = "[NOC] MAJOR ALERT: OCS Error Found: " + item[0] + " - " + item[1];
				} else {
					var subject_data = "[NOC] CRITICAL ALERT: OCS Error Found: " + item[0] + " - " + item[1];
				}
				var email_data = {subject: subject_data, head: head_data, body: body_data};
				if (body_data.length > 0) {
					if (logsEnabled) common.log("ocserrors", list);
				}
        	                callback(undefined, { "ocserrors" : email_data });
        	        });
		});
	});

	async.parallel(tasks, function (err, result) {
		var emailTasks = new Array;
		result.forEach(function (item) {
			if (item) {
				if (item.ocserrors && item.ocserrors != "" && item.ocserrors.body.length > 0) {
					emailTasks.push(function (callback) {
						sendNotification("ocserror_warning", item.ocserrors, function () {
							callback(undefined, 1);
						});
					});
				}
			}
		});
		async.parallel(emailTasks, function (err, result) {	
			process.exit(0);
		});
	});
  	clearInterval(job);
}

var job = setTimeout(go, 900000);

function sendNotification(activity, list, callback) {
	var q = "SELECT apiKey FROM teams WHERE id=5";
	db.query_noerr(q, function(result) {
		var key = (result) ? result[0].apiKey : undefined;
		//var url = "https://" + config.MYSELF + ":" + config.WEBSPORT + "/api/1/notifications/send/" + activity + "/" + key;
		var url = "https://" + config.MYSELF + ":" + config.WEBSPORT + "/api/1/notifications/send/" + activity + "/" + key;
		//var url = "https://10.20.1.27:7443/api/1/notifications/send/" + activity + "/" + key;

		var params = { "json" : { }, "rejectUnauthorized" : false, "dynamic_subject" : list.subject };
		params.json.dynamic_subject = list.subject;
		params.json.email = "elitecore.monitor@circles.asia";
		params.json.list = {
			"style" : { "table" : "background-color:#FFFFFF;border-collapse:collapse;border:2px solid #000000;color:#000000;width:100%", "th" : "background-color:#FFFFFF;border-collapse:collapse;border:1px solid #000000;color:#000000;", "td" : "background-color:#FFFFFF;border-collapse:collapse;border:1px solid #000000;color:#000000;font-size:11px" },
			"head" : list.head,
			"body": list.body
			
		};
		request.post(url, params, function(err, response, body) { 
			callback();
		});
	});
}

