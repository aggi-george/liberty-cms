#!/usr/bin/env nodejs
var start = new Date();
args = [];
process.argv.forEach(function (val, index, array) {
    args.push(val);
});

if (args.length < 5) {
    console.error("check syntax - ./random_distribute.js len distribute count");
    process.exit(-1);
}

var len = args[2];
var distribute = args[3];
var count = args[4];

var arr = new Array;
for (var i=0; i<len; i++) {
    arr.push({ "id" : i, "count" : 0 });
}

var progress = 0;

for (var i=0; i<distribute; i++) { 
    var idx = parseInt(Math.random() * len);
    arr[idx].count++;
    progress += 100 / distribute;
    var percent = { "progress" : progress };
    console.log(JSON.stringify(percent));
}

var sorted = arr.sort(function (a,b) { return b.count - a.count });
sorted.length = count;

var result = { "result" : sorted };
console.log(JSON.stringify(result));
