#!/usr/bin/env nodejs

console.log("Initializing...");

var async = require('async');
var fs = require('fs');
var util = require('util');
var kue = require('kue');

var config = require('../../config');
var db = require('../../lib/db_handler');
var noop = function() {};

var aggressive = false;
if (aggressive) {
    db.queue.CLEANUP_MAX_FAILED_TIME = 1 * 60 * 60 * 1000;
    db.queue.CLEANUP_MAX_INACTIVE_TIME = 1 * 60 * 60 * 1000;
    db.queue.CLEANUP_MAX_DELAYED_TIME = 1 * 60 * 60 * 1000;
    db.queue.CLEANUP_MAX_ACTIVE_TIME = 1 * 60 * 60 * 1000;
    db.queue.CLEANUP_MAX_COMPLETE_TIME = 1 * 60 * 60 * 1000;
} else {
    db.queue.CLEANUP_MAX_FAILED_TIME = 30 * 24 * 60 * 60 * 1000;
    db.queue.CLEANUP_MAX_INACTIVE_TIME = 30 * 24 * 60 * 60 * 1000;
    db.queue.CLEANUP_MAX_DELAYED_TIME = 1 * 24 * 60 * 60 * 1000;
    db.queue.CLEANUP_MAX_ACTIVE_TIME = 1 * 24 * 60 * 60 * 1000;
    db.queue.CLEANUP_MAX_COMPLETE_TIME = 5 * 24 * 60 * 60 * 1000;
}

db.queue.CLEANUP_INTERVAL = 5 * 60 * 1000; // 5 minutes

// this is a simple log action
function QueueActionLog(message) {
    this.message = message || 'QueueActionLog :: got an action for job id(%s)';

    this.apply = function(job) {
        console.log(util.format(this.message, job.id));
        return true;
    };
}

// remove item action
function QueueActionRemove(age) {
    this.age = age;

    this.apply = function(job) {
        job.remove(noop);
        return true;
    };
}

// filter by age
function QueueFilterAge(age) {
    this.now = new Date();
    this.age = age;

    this.test = function(job) {
        var created = new Date(parseInt(job.created_at));
        var age = parseInt(this.now - created);

        return age > this.age;

    };
}

// the queue iterator
var queueIterator = function(ids, queueFilterChain, queueActionChain) {
    ids.forEach(function(id, index) {
        // get the kue job
        db.queueJobs.get(id, function(err, job) {
            if (err || !job) return;
            var filterIterator = function(filter) { return filter.test(job) };
            var actionIterator = function(filter) { return filter.apply(job) };

            // apply filter chain
            if(queueFilterChain.every(filterIterator)) {

                // apply action chain
                queueActionChain.every(actionIterator);
            }
        });
    });
};

function performCleanup() {

    db.queue.failed(function(err, ids) {
        if (!ids) return;
        queueIterator(
            ids,
            [new QueueFilterAge(db.queue.CLEANUP_MAX_FAILED_TIME)],
            [new QueueActionLog('Going to remove job id(%s) for being failed too long'),
                new QueueActionRemove()]
        );
    });

    db.queue.inactive(function(err, ids) {
        if (!ids) return;
        queueIterator(
            ids,
            [new QueueFilterAge(db.queue.CLEANUP_MAX_INACTIVE_TIME)],
            [new QueueActionLog('Going to remove job id(%s) for being inactive too long'),
                new QueueActionRemove()]
        );
    });

    db.queue.active(function(err, ids) {
        if (!ids) return;
        queueIterator(
            ids,
            [new QueueFilterAge(db.queue.CLEANUP_MAX_ACTIVE_TIME)],
            [new QueueActionLog('Going to remove job id(%s) for being active too long'),
                new QueueActionRemove()]
        );
    });

    db.queue.delayed(function(err, ids) {
        if (!ids) return;
        queueIterator(
            ids,
            [new QueueFilterAge(db.queue.CLEANUP_MAX_DELAYED_TIME)],
            [new QueueActionLog('Going to remove job id(%s) for being active too long'),
                new QueueActionRemove()]
        );
    });

    db.queue.complete(function(err, ids) {
        if (!ids) return;
        queueIterator(
            ids,
            [new QueueFilterAge(db.queue.CLEANUP_MAX_COMPLETE_TIME)],
            [new QueueActionLog('Going to remove job id(%s) for being complete too long'),
                new QueueActionRemove()]
        );
    });
}

setTimeout(() => {
    console.log("Cleaning...");
    performCleanup();
}, 2000)

