#!/usr/bin/env nodejs

require('../bin/global');

console.log("Initializing...");
var db = require('../lib/db_handler');
var async = require('async');
var fs = require('fs');
var config = require('../config');
var ec = require('../lib/elitecore');
var request = require('request');
var util = require('util');

config.EC_LOGSENABLED = false;

var common = require('../lib/common');

var elitecoreConnector = require('../lib/manager/ec/elitecoreConnector');
var elitecoreBillService = require('../lib/manager/ec/elitecoreBillService');
var elitecoreUsageService = require('../lib/manager/ec/elitecoreUsageService');
var elitecoreUserService = require('../lib/manager/ec/elitecoreUserService');
//var elitecorePortInService = require('../lib/manager/ec/elitecorePortInService');
//var elitecoreUtils = require('../lib/manager/ec/elitecoreUtils');
//var elitecorePackagesService = require('../lib/manager/ec/elitecorePackagesService');
//var elitecoreBillService = require('../lib/manager/ec/elitecoreBillService');
//var elitecoreUtils = require('../lib/manager/ec/elitecoreUtils');
//var portInManager = require('../src/core/manager/porting/portInManager');
//var partnerManager = require("../src/core/manager/promotions/partnerManager");
//var promotionsManager = require("../src/core/manager/promotions/promotionsManager");
//var oauthManager = require("../src/core/manager/promotions/oauthManager");
//var classManager = require("../src/core/manager/analytics/classManager");
//var leaderboardManager = require("../src/core/manager/bonus/leaderboardManager");
//var configManager = require("../src/core/manager/config/configManager");
//var paymentManager = require("../src/core/manager/account/paymentManager");
//var transactionManager = require("../src/core/manager/account/transactionManager");
//var ecommManager = require("../lib/manager/ecomm/ecommManager");
//var billManager = require("../src/core/manager/account/billManager");
//var invoiceManager = require("../src/core/manager/account/invoiceManager");
//var invoiceNotificationManager = require("../src/core/manager/account/invoiceNotificationManager");
//var syncQueueService = require("../src/core/queue/syncQueueService");
//var baseAddonsClass = require("../src/core/manager/analytics/classCategories/base/baseAddonsClass");
//var analyticsManager = require("../src/core/manager/analytics/analyticsManager");
//var fileManager = require("../src/core/manager/file/fileManager");
//var BaseError = require("../lib/base/baseError");

setTimeout(function () {

    //oauthManager.verifyOAuth("OAUTH_UBER_DRIVER_ID",
    //    {code: "qAQx340kACkuVjyWQrYNhls4NMthCY"},
    //    "https://10.20.1.10:16443/link/debug/uber/token", (err, result) => {
    //
    //        if (err) {
    //            console.error("something went wrong: " + err.message + ", status: " + err.status);
    //        } else {
    //            console.log(JSON.stringify(result))
    //        }
    //        process.exit(0);
    //    })


    //var past7Days = new Date(new Date().getTime() - 30 * 24 * 60 * 60 * 1000).getTime();
    //db.notifications_bills.find({
    //    billingAccountNumber: {"$in": ["port_in_request_canceled_short"]},
    //    ts: {$gt: past7Days}
    //}, {sort:{ts:-1}}).toArray(function (err, resultCanceled) {
    //    if (err) {
    //        common.error("Script", "something went wrong: " + err.message);
    //    } else {
    //        var numbers = [];
    //        resultCanceled.forEach((item) => {
    //            numbers.push(item.number);
    //        });
    //
    //        //common.log("Script", "validateBasePlan: canceled number " + JSON.stringify(numbers));
    //
    //        db.notifications_logbook.find({
    //            activity: {"$in": ["port_in_request_success_short"]},
    //            ts: {$gt: past7Days}
    //        }, {sort:{ts:-1}}).toArray(function (err, resultSuccess) {
    //            if (err) {
    //                common.error("Script", "something went wrong: " + err.message);
    //            } else {
    //                var numbersS = [];
    //                resultSuccess.forEach((item) => {
    //                    if (numbers.indexOf(item.number) >= 0) {
    //                        numbersS.push(item.number);
    //
    //                        console.log(new Date(item.ts).toISOString(), item.number, item.serviceInstanceNumber);
    //                    }
    //                });
    //
    //                common.log("Script", "validateBasePlan: success number " + numbersS.length);
    //            }
    //            process.exit(0);
    //        });
    //    }
    //
    //});

    //var selectQuery = "SELECT count(*) as count " +
    //    " FROM customerPortIn LEFT OUTER JOIN customerProfile " +
    //    " ON (customerProfile.id = customerPortIn.profile_id) " +
    //    " WHERE customerProfile.service_instance_number =" + db.escape("LW2507291");
    //
    //db.query_err(selectQuery, function (err, rows) {
    //    if (err) {
    //        common.error("PortinManager", "Failed to load current request status, err=" + err.message);
    //        return callback(new BaseError("Failed to load current request status", "ERROR_DB"));
    //    }
    //
    //    if (!rows || rows.length < 1) {
    //        common.error("PortinManager", "No requests found");
    //        return callback(new BaseError("No port-in requests found", "ERROR_NOT_FOUND"));
    //    }
    //
    //    common.log("PortinManager", "count " + (rows[0].count > 0));
    //    process.exit(0);
    //});

    //classManager.classifyCustomers((err) => {
    //    if (err) {
    //        console.error("Something went wrong: " + err.message);
    //    } else {
    //        console.log("Done");
    //    }
    //    process.exit(0);
    //});
    //
    //baseAddonsClass.loadPackages({serviceInstanceNumber: "LW665344"},  function (err, obj) {
    //        if (err) {
    //            console.error("Something went wrong: " + err.message);
    //        } else {
    //            //console.log(obj);
    //        }
    //        process.exit(0);
    //    });

    //classManager.saveCurrentState((err) => {
    //    if (err) {
    //        console.error("Something went wrong: " + err.message);
    //    } else {
    //        console.log("Done");
    //    }
    //    process.exit(0);
    //});

    //analyticsManager.loadClassesStats(new Date("2017-02-19"), new Date("2017-02-28"), 9, undefined, (err, result) => {

    //var points = (new Date("2017-02-28").getTime() - new Date("2017-02-19").getTime()) / (24 * 60 * 60 * 1000) + 1;
    //
    //analyticsManager.loadClassesStats(new Date("2017-02-19").getTime() - 8 * 60 * 60 * 1000, new Date("2017-02-28").getTime() - 8 * 60 * 60 * 1000, points, "REFERRALS_ZERO_TO_TWO", (err, result) => {
    //    if (err) {
    //        console.error("Something went wrong: " + err.message);
    //    } else {
    //        result.forEach((item) => {
    //            console.log(item);
    //            console.log("--------------------------");
    //        })
    //    }
    //    process.exit(0);
    //})


    //elitecorePortInService.loadPortInFailureReason("87213358", common.getDateTime(new Date()), (err, result) => {
    //    if (err) {
    //        console.error("Something went wrong: " + err.message);
    //    } else {
    //        console.log(result);
    //    }
    //    process.exit(0);
    //});

    //elitecorePackagesService.loadHierarchy(function (err, hierarchy) {
    //    var registrationProduct = elitecoreUtils.findPackageByName(hierarchy, "Registration 18");
    //    console.log(registrationProduct);
    //});

    //elitecoreUserService.loadUserInfoByServiceInstanceNumber("LW1577019", function (err, cache) {
    //    if (err) {
    //        console.error(err.message);
    //        process.exit(0);
    //    }
    //
    //    console.log("--------------");
    //    console.log(cache);
    //    process.exit(0);
    //}, true, true);

    //elitecoreUsageService.loadRemainingDataForPackage("87428517", ec.findPackageName(ec.packages(), "Plus 2020").id, function (err, result) {
    //    if (err) {
    //        console.error(err.message);
    //        process.exit(0);
    //    }
    //
    //    console.log("Plus 2020", result);
    //    elitecoreUsageService.loadRemainingDataForPackage("87428517", ec.findPackageName(ec.packages(), "CirclesOne").id, function (err, result) {
    //        if (err) {
    //            console.error(err.message);
    //            process.exit(0);
    //        }
    //
    //        console.log("CirclesOne", result);
    //        process.exit(0);
    //    }, true, true);
    //}, true, true);
    //invoiceManager.loadBillsCount(['LW945282', 'LW910826', 'LW995566', 'LW995594', 'LW995598'], {}, (err, result) => {
    //     if (err) {
    //         console.error("Something went wrong: " + err.message);
    //     } else {
    //         console.log(JSON.stringify(result));
    //     }
    //     process.exit(0);
    //});
    //invoiceManager.syncBills(new Date("2017-03-01T00:00:00.000Z"), {verifyContent: true, overrideLock: true}, (err, result) => {
    //    if (err) {
    //        console.error("Something went wrong: " + err.message);
    //    } else {
    //        //console.log(JSON.stringify(result))
    //    }
    //    process.exit(0);
    //});
    //
    //syncQueueService.run("SYNC_INVOICES", {
    //    monthDate: "2017-03-01",
    //    options: {
    //        verifyContent: false,
    //        overrideLock: true
    //    }
    //}, {
    //    errorIfRunning: true,
    //    initiator: "[SCRIPT][alexey@circles.asia]"
    //}, (err, result) => {
    //    if (err) {
    //        console.error("Something went wrong: " + err.message);
    //    } else {
    //        //console.log(JSON.stringify(result))
    //    }
    //
    //    setInterval(() => {
    //
    //        console.log("-----------------------------");
    //
    //        //syncQueueService.isRunning("SYNC_INVOICES", (err, result) => {
    //        //    console.log("queue job: ", result);
    //        //});
    //
    //        invoiceManager.loadBillsSyncProgress((err, status) => {
    //            console.log("progress: ", status);
    //        })
    //    }, 2000);
    //
    //    //process.exit(0);
    //})

    // e570a72a01545e9abaf71896027d7f8e
    // b6093d37e742b6afd1187ba8d21999a2 - pdf

    //var callback = (err) => {
    //    if (err) {
    //        console.error("Error => " + err.message);
    //    } else {
    //        console.log("Finished");
    //    }
    //    process.exit(0);
    //}
    //
    //invoiceManager.syncPaymentsFromFile("e570a72a01545e9abaf71896027d7f8e", "temp", {}, callback);

    //var numbers = ["88081685", "87484042"]
    //
    //var bonusManager = require("../src/core/manager/bonus/bonusManager");
    //numbers.forEach((number) => {
    //    elitecoreUserService.loadUserInfoByPhoneNumber(number, (err, cache) => {
    //        if (err) {
    //            console.error("Error => " + err.message + " " + number);
    //            return;
    //        }
    //        if (!cache) {
    //            console.error("Error => " + "no cache " + number);
    //            return;
    //
    //        }
    //
    //        db.query_err("UPDATE historyBonuses SET bonus_kb=2097152, bonus_kb_pretty=2000000 WHERE " +
    //        "service_instance_number=" + db.escape(cache.serviceInstanceNumber) + " AND bonus_type='selfcare_install' AND added_ts <'2016-08-05 00:00:00'", (err, result) => {
    //                if (err) {
    //                    console.error("Error => " + err.message);
    //                } else {
    //                    console.log("Finished " + number);
    //                }
    //            })
    //        db.query_err("UPDATE historyBonuses SET bonus_kb=524288, bonus_kb_pretty=500000 WHERE " +
    //        "service_instance_number=" + db.escape(cache.serviceInstanceNumber) + " AND bonus_type='loyalty'", (err, result) => {
    //            if (err) {
    //                console.error("Error => " + err.message);
    //            } else {
    //                console.log("Finished " + number);
    //            }
    //        })
    //        db.query_err("UPDATE historyBonuses SET bonus_kb=1048576, bonus_kb_pretty=1000000 WHERE " +
    //        "service_instance_number=" + db.escape(cache.serviceInstanceNumber) + " AND bonus_type='portin'", (err, result) => {
    //            if (err) {
    //                console.error("Error => " + err.message);
    //            } else {
    //                console.log("Finished " + number);
    //            }
    //        })
    //        db.query_err("UPDATE historyBonuses SET bonus_kb=1048576, bonus_kb_pretty=1000000 WHERE " +
    //        "service_instance_number=" + db.escape(cache.serviceInstanceNumber) + " AND bonus_type='promo_extra_portin'", (err, result) => {
    //            if (err) {
    //                console.error("Error => " + err.message);
    //            } else {
    //                console.log("Finished " + number);
    //            }
    //        })
    //        db.query_err("UPDATE historyBonuses SET bonus_kb=2097152, bonus_kb_pretty=2000000 WHERE " +
    //        "service_instance_number=" + db.escape(cache.serviceInstanceNumber) + " AND bonus_type='promo_national_day'", (err, result) => {
    //            if (err) {
    //                console.error("Error => " + err.message);
    //            } else {
    //                console.log("Finished " + number);
    //            }
    //        })
    //        db.query_err("UPDATE historyBonuses SET bonus_kb=204800, bonus_kb_pretty=200000 WHERE " +
    //        "service_instance_number=" + db.escape(cache.serviceInstanceNumber) + " AND bonus_type='referral'", (err, result) => {
    //            if (err) {
    //                console.error("Error => " + err.message);
    //            } else {
    //                console.log("Finished " + number);
    //            }
    //        })
    //        db.query_err("UPDATE historyBonuses SET bonus_kb=524288, bonus_kb_pretty=500000 WHERE " +
    //        "service_instance_number=" + db.escape(cache.serviceInstanceNumber) + " AND bonus_type='birthday'", (err, result) => {
    //            if (err) {
    //                console.error("Error => " + err.message);
    //            } else {
    //                console.log("Finished " + number);
    //            }
    //        })
    //        db.query_err("UPDATE historyBonuses SET bonus_kb=524288, bonus_kb_pretty=500000 WHERE " +
    //        "service_instance_number=" + db.escape(cache.serviceInstanceNumber) + " AND bonus_type='promo_zalora'", (err, result) => {
    //            if (err) {
    //                console.error("Error => " + err.message);
    //            } else {
    //                console.log("Finished " + number);
    //            }
    //        })
    //    }, false, true);
    //});

    //
    //var obj = {
    //    name: "Glicerio jr. espina galac"
    //}
    //
    //function toTitleCase(str) {
    //    return str.replace(/\w[\S\.]*/g, function (txt) {
    //        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    //    });
    //}
    //
    //obj.name = toTitleCase(obj.name);
    //
    //console.log(obj.name);

    //elitecoreUserService.loadUserInfoByServiceInstanceNumber("LW725288", (err, result) => {
    //    if (err) {
    //        console.error("Something went wrong: " + err.message);
    //    } else {
    //        console.log("-------------------------------");
    //        if (!result) {
    //            console.log("no cache 1");
    //        } else {
    //            console.log(result.billingFullName, result.number, result.account, result.email);
    //        }
    //    }
    //
    //    elitecoreUserService.loadUserInfoByServiceInstanceNumber("LW725288", (err, result) => {
    //        if (err) {
    //            console.error("Something went wrong: " + err.message);
    //        } else {
    //            console.log("-------------------------------");
    //            if (!result) {
    //                console.log("no cache 2");
    //            } else {
    //                console.log(result.billingFullName, result.number, result.account, result.email);
    //            }
    //        }
    //        process.exit(0);
    //    }, false, false);
    //
    //}, false, true);

    //elitecoreUserService.loadUserInfoByBillingAccountNumber("LW870298", (err, result) => {
    //    if (err) {
    //        console.error("Something went wrong: " + err.message);
    //    } else {
    //        console.log("-------------------------------");
    //        if (!result) {
    //            console.log("no cache 2");
    //        } else {
    //            console.log(result.billingFullName, result.number, result.account, result.email);
    //        }
    //    }
    //    process.exit(0);
    //}, true);

    //console.log(portInManager.getAvailablePortInDates("65"));
    //process.exit(0);

    //db.queueJobs.rangeByType("slow", 'inactive', 0, 10000, 'asc', function (err, jobs) {
    //    if (err) {
    //        console.error("Something went wrong: " + err.message);
    //        process.exit(0);
    //    } else {
    //        console.log("Jobs count=" + jobs.length);
    //        jobs.forEach(function (job) {
    //            job.remove();
    //        });
    //
    //        setTimeout(() => {
    //            process.exit(0);
    //        }, 3000)
    //    }
    //});

    //analyticsManager.loadEcOverallStats(new Date(new Date().getTime() - 24 * 60 * 60 * 1000), new Date(), (err, result) => {
    //        if (err) {
    //            console.error("Something went wrong: " + err.message);
    //            process.exit(0);
    //        } else {
    //            console.log("Result: ", result);
    //            process.exit(0);
    //        }
    //    });

    //elitecoreUserService.loadActivePackages("LW665344", function (err, obj) {
    //    if (err) {
    //        console.error("Something went wrong: " + err.message);
    //    } else {
    //        console.log(obj);
    //    }
    //    process.exit(0);
    //}, false, true);

    //
    // elitecoreUserService.loadCustomerWithPackage("Plus 2020", function (err, obj) {
    //    if (err) {
    //        console.error("Something went wrong: " + err.message);
    //    } else {
    //        console.log(obj);
    //    }
    //    process.exit(0);
    //});

    //portInManager.loadRequest(undefined, {status: "WAITING"}, function (err, result) {
    //    if (err) {
    //        console.error("Something went wrong: " + err.message);
    //    } else {
    //        console.log(result.data.length);
    //    }
    //
    //    var asyncTasks = [];
    //    result.data.forEach((item) => {
    //        (function (item) {
    //            asyncTasks.push((callback) => {
    //                console.error("Starting PORT-IN request " + item.id + ", number=" + item.number);
    //                portInManager.updateRequestStatus(item.id, "IN_PROGRESS", false, true, function (err, result) {
    //                    if (err) {
    //                        console.error("Failed to start PORT-IN request " + item.id + ", number=" + item.number + ", error=" + err.message);
    //                    } else {
    //                        console.log("Started PORT-IN request " + item.id + ", number=" + item.number);
    //                    }
    //                    callback();
    //                }, undefined, undefined, "[SCRIPT][INTERNAL]", undefined);
    //            });
    //        }(item));
    //    });
    //
    //    async.parallelLimit(asyncTasks, 1, (err, result) => {
    //        if (err) {
    //            console.error("Something went wrong: " + err.message);
    //        } else {
    //            console.log("DONE");
    //        }
    //        setTimeout(() => {
    //            process.exit(0);
    //        }, 5000);
    //    });
    //
    //}, undefined, undefined, undefined, undefined, undefined, false,
    //    new Date("2017-06-28"), new Date("2017-06-29"));

    //console.log("START");
    //invoiceManager.loadInvoicesForMonth(new Date('2017-06-01'), 36000, (err, map, counthandled) => {
    //    if (err) {
    //        console.error("Something went wrong: " + err.message);
    //    } else {
    //        console.log("COUNT TOTAL " + counthandled + ", " + Object.keys(map).length);
    //    }
    //    process.exit(0);
    //}, (progress) => {
    //    console.log("PROGRESS " + progress);
    //})

    //elitecoreUserService.loadAllUsersBaseInfo((err, result) => {
    //    if (err) {
    //        console.error("Something went wrong: " + err.message);
    //    } else {
    //        console.log("Done");
    //    }
    //
    //    process.exit(0);
    //});

    //leaderboardManager.loadRating((err, result) => {
    //    if (err) {
    //        console.error("something went wrong: " + err.message);
    //    } else {
    //        console.log(JSON.stringify(result))
    //    }
    //    process.exit(0);
    //});

    //var query = "SELECT ca_number, ba_number, si_number, emailid, name, debitdocumentnumber, billdate, " +
    //    "status, totalamount, leftamount, billtype, dateofbirth FROM crestelocslwp.LWP_Billview WHERE billdate=" +
    //    "TO_TIMESTAMP(" + db.escape(new Date("2017-05-01")) + ", 'YYYY-MM-DD HH24:MI:SS.xff') AND status='PAID'";
    //
    //var startTime = new Date().getTime();
    //db.oracle.query(query, function (err, results) {
    //    var time = new Date().getTime() - startTime;
    //    if (err) {
    //        console.error("failed to load bills data from DB, time=" + time + ", error=" + err.message);
    //        process.exit(0);
    //    }
    //
    //    console.log("count: " + results.rows.length);
    //    var ids = [];
    //    results.rows.forEach((item) => {
    //        ids.push(item[1]);
    //    });
    //
    //    var startTime = new Date().getTime();
    //    invoiceManager.loadBills(38, {type: 'CLOSED'}, (err, result) => {
    //        var time = new Date().getTime() - startTime;
    //        if (err) {
    //            console.error("failed to load invoices data from DB, time=" + time + ", error=" + err.message);
    //            process.exit(0);
    //        }
    //
    //        console.log("count: ", result.data.length);
    //        result.data.forEach((item) => {
    //            if (ids.indexOf(item.billingAccountNumber) == -1) {
    //                console.log("not found: ", item.billingAccountNumber);
    //            }
    //        });
    //
    //        process.exit(0);
    //    });
    //});

    //var query = "SELECT ca_number, ba_number, si_number, emailid, name, debitdocumentnumber, billdate, " +
    //    "status, totalamount, leftamount, billtype, dateofbirth FROM crestelocslwp.LWP_Billview WHERE billdate=" +
    //    "TO_TIMESTAMP(" + db.escape(new Date("2017-05-01")) + ", 'YYYY-MM-DD HH24:MI:SS.xff') AND status='PAID'";
    //
    //var startTime = new Date().getTime();
    //db.oracle.query(query, function (err, results) {
    //    var time = new Date().getTime() - startTime;
    //    if (err) {
    //        console.error("failed to load bills data from DB, time=" + time + ", error=" + err.message);
    //        process.exit(0);
    //    }
    //
    //    console.log("item: ", results.rows.length);
    //    process.exit(0);
    //});

    //elitecorePortInService.loadPortInCompleationStatus('87445937', (err, result) => {
    //    if (err) {
    //        console.error("failed to load bills data from DB, time=" + time + ", error=" + err.message);
    //        process.exit(0);
    //    }
    //
    //    console.log("item: ", result);
    //    process.exit(0);
    //});

    //var query = "SELECT ca_number, ba_number, si_number, emailid, name, debitdocumentnumber, billdate, " +
    //    "status, totalamount, leftamount, billtype, dateofbirth FROM crestelocslwp.LWP_Billview WHERE ba_number='LW085078'";
    //
    //console.log(query)
    //db.oracle.query(query, function (err, results) {
    //    if (err) {
    //        console.error("failed to load bills data from DB, error=" + err.message);
    //        process.exit(0);
    //    }
    //
    //    console.log(results.rows)
    //    console.log("-------------")
    //
    //    var query = "select customername, subscriberidentifier, serviceinstanceaccount, customeraccount, billingaccount, " +
    //        "serviceaccount, emailid, birthday, numberstatus, portinstatus, accountstatus, available, usedvalue, sim, imsi, referralcode, " +
    //        "currentstatus from CRESTELOCSLWP.CUSTOMER_DETAIL_MV WHERE billingaccount='LW130898'";
    //
    //    console.log(query)
    //    db.oracle.query(query, function (err, results) {
    //        if (err) {
    //            console.error("failed to load bills data from DB, error=" + err.message);
    //            process.exit(0);
    //        }
    //
    //        console.log(results.rows[0])
    //        process.exit(0);
    //    });
    //});

    //var nowMs = new Date().getTime();
    //var monthCount = 1;
    //var startingNext = false;
    //
    //var billCycle;
    //for (var i = startingNext ? - 1 : 0; i < monthCount; i++) {
    //    billCycle = elitecoreUtils.calculateBillCycle(1, undefined, billCycle ? billCycle.endDateUTC : new Date());
    //}
    //
    //console.log("monthCount=" + monthCount + ", startingNext=" + startingNext + ", toDate=" + billCycle.endDateUTC.toISOString());
    //process.exit(0);
    //
    //promotionsManager.loadPendingProducts(["PRD00234", "PRD00440"], "000001490864361721", (err, result) => {
    //    if (err) {
    //        console.error("error=" + err.message);
    //        process.exit(0);
    //    }
    //    console.log(result)
    //    process.exit(0);
    //})

    // promotionsManager.useCode("ECOMM", "CK_TER_TEST", "87421072", "CK_TER_TESTK6FVH",
    // "000001504518621207", 'key', (err, result) => {
    //    if (err) {
    //        console.error("error=" + err.message);
    //        process.exit(0);
    //    }
    //    var util = require('util');
    //    console.error(util.inspect(result, {depth: null}));
    //    process.exit(0);
    // })

    // promotionsManager.removeCodeUsageLog(1234567)
    // .then((result) => {
    //     console.log("This is the result", util.inspect(result));
    // })
    // .catch((err) => {
    //     console.error("This is the error", util.inspect(err));
    // });

    // promotionsManager.applyPendingPromo(65, 87421072, "000001504518621207", "PORT_IN",
    // config.OPERATIONS_KEY, function (err, result) {
    //     if (err) {
    //         return console.error("This is promotions error", util.inspect(err, {depth: null}));
    //     }
    //     return console.log("This is the result", util.inspect(result, {depth: null}));

    // });


    //elitecoreUserService.loadCustomerUsage("LW1559804"/*"LW985340"*/, (err, result) => {
    //    if (err) {
    //        console.error("failed to load bills data from DB, error=" + err.message);
    //        process.exit(0);
    //
    //    }
    //    console.log(result)
    //
    //    billManager.loadAdvancedPayments("LW1559804"/*"LW985338"*/, {}, function (err, result) {
    //        if (err) {
    //            console.error("failed to load payments data from DB, error=" + err.message);
    //            process.exit(0);
    //
    //        }
    //
    //        console.log(result)
    //        process.exit(0);
    //    });
    //})

    //var notification = {
    //    teamID: 5,
    //    teamName: "Operations",
    //    activity: "bill_payment_success_after_failure",
    //    delay: -1,
    //    prefix: "65",
    //    number: "86164389",
    //    name: "Alex",
    //    email: "alexey@circles.asia",
    //    billingAccountNumber: "LW665342",
    //    invoicenumber: "LW665342",
    //    month: "May",
    //    pdfpassword: "PWD",
    //    totalAmount: 0,
    //    leftAmount: 0
    //};
    //
    //var notificationSend = require('../src/core/manager/notifications/send');
    //notificationSend.deliver(notification, null, (err, result) => {
    //    if (err) {
    //        console.error(err.message);
    //        process.exit(0);
    //    }
    //
    //    console.log(JSON.stringify(result.activity));
    //    process.exit(0);
    //});

    //leaderboardManager.updateRating(config.ELITECORE_KEY, function () {
    //    process.exit(0);
    //});

    //leaderboardManager.loadLeaderboard(function (err, result) {
    //    if (err) {
    //        console.error(err.message);
    //        process.exit(0);
    //    }
    //
    //    console.log(JSON.stringify(result))
    //    leaderboardManager.loadGoldenCircleBoard(function (err, result) {
    //        if (err) {
    //            console.error(err.message);
    //            process.exit(0);
    //        }
    //
    //        console.log(JSON.stringify(result))
    //        process.exit(0);
    //    });
    //});

    //leaderboardManager.addGoldenTicket("LW085080", {
    //    retrospectiveDate: new Date("2017-01-15"),
    //    waiver: true
    //}, function (err, result) {
    //    if (err) {
    //        console.error(err.message);
    //    }
    //
    //    leaderboardManager.loadGoldenCircleBoard(function (err, result) {
    //        if (err) {
    //            console.error(err.message);
    //            process.exit(0);
    //        }
    //
    //        console.log(JSON.stringify(result));
    //        process.exit(0);
    //    });
    //});

    //db.notifications_logbook.aggregate([{$unwind: "$transport"}, {$match: {transport: "selfcare"}}, {
    //    $group: {
    //        _id: {
    //            number: "$number",
    //            sin: "$serviceInstanceNumber"
    //        }
    //    }
    //}, {
    //    $group: {_id: {number: "$_id.number"}, sins: {$push: "$_id.sin"}}
    //}, {
    //    $match: {'sins.1': {$exists: true}}
    //}]).toArray(function (err, result) {
    //    if (err) {
    //        console.error("something went wrong: " + err.message);
    //        process.exit(0);
    //    }
    //
    //    result.forEach(function (item) {
    //        console.log(item._id.number + " - " + JSON.stringify(item.sins))
    //    })
    //});

    //invoiceManager.syncUnpaidBills({overrideLock: true}, (err, result) => {
    //    if (err) {
    //        console.error("something went wrong: " + err.message);
    //        process.exit(0);
    //    }
    //
    //    process.exit(0);
    //
    //    module.exports.scheduleAggregation(userCache.prefix, userCache.number,
    //        "[CMS][BonusManager]", executionKey);
    //});

    //elitecoreUserService.loadAllUsersBaseInfo({serviceInstances: ['LW145888', 'LW520370']}, (err, results) => {
    //    if (err) {
    //        console.error("something went wrong: " + err.message);
    //        process.exit(0);
    //    }
    //
    //    console.error("log: ", results);
    //    process.exit(0);
    //});

    //invoiceManager.syncBills(new Date("2017-03-01T00:00:00.000Z"), {verifyContent: false, overrideLock: true}, (err, result) => {
    //    if (err) {
    //        console.error("something went wrong: " + err.message);
    //        process.exit(0);
    //    }
    //
    //    process.exit(0);
    //});

    //invoiceNotificationManager.sendNonPaymentReminder("NON_PAYMENT_REMINDER_1", {}, (err, result) => {
    //    if (err) {
    //        console.error("something went wrong: " + err.message);
    //    } else {
    //        console.log(JSON.stringify(result))
    //    }
    //    process.exit(0);
    //});

    //var selectQuery = " SELECT serviceinstanceaccount, orderrefno, customerProfile.id as profile_id FROM " +
    //    "ecAccounts LEFT JOIN customerProfile ON customerProfile.service_instance_number = serviceinstanceaccount " +
    //    "WHERE serviceinstanceaccount IN (SELECT service_instance_number FROM customerPortIn LEFT JOIN customerProfile " +
    //    "ON customerPortIn.profile_id = customerProfile.id)";
    //db.query_err(selectQuery, function (err, rows) {
    //    if (err) {
    //        console.error("something went wrong: " + err.message);
    //    }
    //
    //    var asyncTasks = []
    //    rows.forEach((item) => {
    //        var sin = item.serviceinstanceaccount;
    //        var orn = item.orderrefno;
    //        var profile_id = item.profile_id;
    //
    //        if (orn && sin && profile_id) {
    //            //if (asyncTasks.length > 100) {
    //            //    return;
    //            //}
    //
    //            (function (sin, orn, profile_id) {
    //                asyncTasks.push((callback) => {
    //                    console.log(sin, orn, profile_id);
    //
    //                    var selectQuery = "UPDATE customerPortIn SET order_reference_number=" + db.escape(orn) +
    //                        " WHERE profile_id=" + db.escape(profile_id);
    //                    db.query_err(selectQuery, function (err, rows) {
    //                        if (err) {
    //                            return callback(err);
    //                        }
    //
    //                        callback();
    //                    });
    //                });
    //            }(sin, orn, profile_id));
    //        }
    //    });
    //
    //    async.parallelLimit(asyncTasks, 50, (err, result) => {
    //        if (err) {
    //            console.error("Something went wrong: " + err.message);
    //            process.exit(0);
    //        }
    //
    //        console.log(asyncTasks.length);
    //        setTimeout(() => {
    //            process.exit(0);
    //        }, 2000);
    //    });
    //});

    //var items = [];
    //for(var i = 0; i < 10; i++){
    //    items.push(i);
    //}
    //
    //var codeNotFounds = [];
    //var codeSuccess = [];
    //async.each(items, (item, callback) => {
    //    request({
    //        uri: "http://" + config.CMSHOST + ":" + config.CMSPORT + "/api/1/referral/code/generate/Am8zaj8wzk",
    //        method: "PUT",
    //        qs: "",
    //        json: {"tag": "DD_2020_August"},
    //        timeout: 35000
    //    }, function (err, response, body) {
    //        if (err) {
    //            console.log("CREATION", err);
    //            callback();
    //        } else {
    //            console.log("CREATION", body.promoCode);
    //
    //            var promoCode = body.promoCode;
    //            request({
    //                uri: "http://" + config.CMSHOST + ":" + config.CMSPORT + "/api/1/promotions/use/code/" + promoCode + "/Am8zaj8wzk",
    //                method: "PUT",
    //                qs: "",
    //                json: {"order_reference_number": "TEST" + Math.floor(Math.random() * 10000000)},
    //                timeout: 35000
    //            }, function (err, response, body) {
    //                if (err) {
    //                    console.log("USAGE", err);
    //                    callback();
    //                } else {
    //                    if (body.code == -1 && body.status == 'CODE_NOT_FOUND') console.log("USAGE", promoCode, body);
    //                    callback();
    //                }
    //            });
    //        }
    //    });
    //}, function (err, result) {
    //});

    //var start = new Date().getTime();
    //configManager.refreshBssSetting("CMS");
    //
    //paymentManager.makeBillPaymentByBA("LW1589193",
    //    [{invoiceId: "REG0000000625062", amount: 2.87}],
    //    {platform: "CMS", batch: true, retryUnfinished: true, overrideLock: true},
    //    (err, result) => {
    //        if (err) {
    //            console.error("ERROR", "makeBillPaymentByBA: message=" + err.message);
    //            process.exit(0);
    //        }
    //
    //        var end = new Date().getTime();
    //        console.log("SUCCESS", "makeBillPaymentByBA, time=" + (end - start));
    //        process.exit(0);
    //    });

    //elitecoreBillService.clearCustomerPaymentsCache("LW1589193");


    //transactionManager.performPayment("LW1589193", {
    //    amount: 1.03,
    //    type: "INVOICE_PAYMENT",
    //    internalId: "REG0000000625062",
    //    metadata: transactionManager.createCloseInvoiceAction("REG0000000625062")
    //}, () => {
    //
    //}, (err, result) => {
    //    if (err) {
    //        console.error("ERROR", "processInvoicePaymentAction: message=" + err.message);
    //        process.exit(0);
    //    }
    //
    //    var end = new Date().getTime();
    //    console.log("SUCCESS", "processInvoicePaymentAction, result=", result);
    //    process.exit(0);
    //})

    //transactionManager.recoverTransaction(2602, 'IGNORE', 'VOID', {}, (err, result) => {
    //    if (err) {
    //        console.error("ERROR", "processInvoicePaymentAction: message=" + err.message);
    //        process.exit(0);
    //    }
    //
    //    var end = new Date().getTime();
    //    console.log("SUCCESS", "processInvoicePaymentAction, result");
    //    process.exit(0);
    //})

    //var info = {};
    //ecommManager.checkStatus(
    //    "LW1125354",
    //    "28542b1e7eac1053fd8ac85e2393dbf5",
    //    "P-000001503657859334",
    //    (err, result) => {
    //        if (err) {
    //            console.error("ERROR", "processInvoicePaymentAction: error=" + err.message);
    //            process.exit(0);
    //        }
    //        if (!result || !result.result || !result.result.response) {
    //            console.error("ERROR", "processInvoicePaymentAction: invalid result");
    //            process.exit(0);
    //        }
    //
    //        var response = result.result.response;
    //        if (response.code == 1) {
    //            var orderRef = info.transaction.externalTransactionId;
    //            var errorMessage = "Payment is still processing..." +
    //                " | External ID - " + info.transaction.uuid +
    //                " | Order Ref - " + (orderRef ? orderRef : "EMPTY");
    //
    //        } else if (response.code == -1) {
    //            info.paymentStatus = 'FAILED';
    //            if (response.payment_status) {
    //                info.paymentFailureReason = response.payment_status;
    //            }
    //            console.error("SUCCESS", "processInvoicePaymentAction: failure", response);
    //            process.exit(0);
    //        } else if (response.code == 0) {
    //            info.paymentStatus = 'SUCCESS';
    //            console.error("SUCCESS", "processInvoicePaymentAction: success", response);
    //            process.exit(0);
    //        }
    //    });

    //db.notifications_port_in.find({type: "UPDATE_REQUEST_STATUS", ts: {'$gt': 1503579397375}}, {})
    //    .sort({"_id": -1})
    //    .toArray(function (err, items) {
    //        if (err) {
    //            console.error("ERROR", "notifications_port_in: error=" + err.message);
    //            process.exit(0);
    //        }
    //
    //        var lastStatusUpdateMap = {};
    //        items.forEach((item) => {
    //            if (!lastStatusUpdateMap[item.serviceInstanceNumber] || lastStatusUpdateMap[item.serviceInstanceNumber].ts < item.ts) {
    //                lastStatusUpdateMap[item.serviceInstanceNumber] = item;
    //            }
    //        });
    //
    //        var lastStatusUpdate = []
    //        Object.keys(lastStatusUpdateMap).forEach((sin) => {
    //            lastStatusUpdate.push({
    //                serviceInstanceNumber: lastStatusUpdateMap[sin].serviceInstanceNumber,
    //                requestId: lastStatusUpdateMap[sin].params.id,
    //                status: lastStatusUpdateMap[sin].params.status,
    //                reason: lastStatusUpdateMap[sin].params.reason
    //            });
    //        })
    //
    //        console.log("SUCCESS", "notifications_port_in: items, original=", items[0], items.length);
    //        console.log("SUCCESS", "notifications_port_in: items, filtered=", lastStatusUpdate[0], lastStatusUpdate.length);
    //
    //        lastStatusUpdate.forEach((item) => {
    //            if (item.requestId > 0) {
    //                db.query_err("UPDATE customerPortIn SET status=" + db.escape(item.status) +
    //                ", error_status=" + db.escape(item.reason) + " WHERE id=" + item.requestId, (err) => {
    //                    if (err) {
    //                        console.error("ERROR", "notifications_port_in: update query error=" + err.message);
    //                        process.exit(0);
    //                    }
    //                    console.log("SUCCESS", "notifications_port_in: updated items, item=", item);
    //
    //                });
    //            }
    //        });
    //    });

    //
    //elitecoreConnector.getPromise(elitecoreConnector.makeDebitPaymentDataRequest, {
    //    accountNumber: "LW690282",
    //    transactionAmount: 1,
    //    documentNumber: 'REG0000000583866',
    //    paymentDate: new Date().toISOString().split('T')[0],
    //    channelAlias: "BillManager",
    //    validResponses: [
    //        {keys: ["receivableAccountData", "$array0"]}
    //    ]
    //}).then((result) => {
    //    console.log("SUCCESS", result)
    //    elitecoreBillService.clearCustomerPaymentsCache("LW690282");
    //    process.exit(0);
    //}).catch((err) => {
    //    console.log("ERROR", err.message)
    //    //process.exit(0);
    //});

    //configManager.loadConfigMapForGroup("elitecore", function (err, items) {
    //    if (err) {
    //        console.log("ERROR", err.message);
    //        process.exit(0);
    //    }
    //
    //    console.log("SUCCESS", item);
    //    process.exit(0);
    //});

}, 6000);
