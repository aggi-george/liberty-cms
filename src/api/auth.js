const db = require(`${global.__lib}/db_handler`);
const common = require(`${global.__lib}/common`);
const config = require(`${global.__base}/config`);

const LOG = config.LOGSENABLED;

class Auth {

    static checkApiKeyForTeams(key, options, callback) {
        if (!key) return callback(new Error('Unknown/Empty Key'));
        if (!options.ids && !options.names) return callback(new Error('Unknown/Empty Match'));
        let cond = '';
        if (options.ids) cond = `id IN (${options.ids.join(',')})`;
        else if (options.names) cond = `name IN (${options.names.map((o)=>{ return db.escape(o); }).join(',')})`;
        const q = `SELECT id FROM teams WHERE apiKey=${db.escape(key)} AND ${cond}`;
        if (LOG) common.log('checkApiKeyForTeams', q);
        db.query_err(q, (err, result) => {
            if (result && result.length > 0) {
                callback(null, true);
            } else {
                callback(null, false);
            }
        });
    }

}

module.exports = Auth;
