var db = require('../../../lib/db_handler');
var common = require('../../../lib/common');
var config = require('../../../config');

var STUB_ONLY_DATA = false;
var CHECK_SESSION = true;
var CHECK_TEAM_KEY = true;
var QUERY_FIELDS_MAX = 50;

var LOG = config.LOGSENABLED;

module.exports = {

    /**
     *
     *
     */
    executeMethod: function (req, res, apiReal, apiStub, methodName) {
        if (STUB_ONLY_DATA) {
            apiStub[methodName](req, res);
        } else {
            var query = 'SELECT a.number FROM app a, device d '
                + ' WHERE d.app_id=a.id AND d.user_key='
                + db.escape(req.params.user_key);

            db.query_noerr(query, function (rows) {
                var numbers = ['6587654321'];
                if (!rows || !rows[0]) {
                    res.status(401).json({
                        code: common.SESSION_KEY_INVALID,
                        details: 'session_key error',
                        description: 'Session Key is Invalid'
                    });
                } else if (numbers.indexOf(rows[0].number) == -1) {
                    if (apiReal) {
                        if (apiReal[methodName]) {
                            apiReal[methodName](req, res);
                        } else {
                            if (LOG) common.error('SessionManager',
                                apiReal.toString().split(' ')[1] + ' ' + methodName);
                        }
                    } else {
                        res.status(400).json({
                            code: -1,
                            details: 'API is not supported',
                            description: 'API is not supported'
                        });
                    }
                } else {
                    req.fakenumber = true;
                    if (apiStub && apiStub[methodName]) {
                        apiStub[methodName](req, res);
                    } else {
                        res.status(400).json({
                            code: -1,
                            details: 'API is not supported',
                            description: 'API is not supported'
                        });
                    }
                }
            });
        }
    },

    /**
     *
     *
     */
    parsePost: function (req, res, next) {
        common.formParse(req, res, QUERY_FIELDS_MAX, function (fields) {
            req.fields = fields;
            next();
        });
    },

    /**
     *
     *
     */
    parsePut: function (req, res, next) {
        common.formParse(req, res, QUERY_FIELDS_MAX, function (fields) {
            req.fields = fields;
            next();
        });
    },

    /**
     *
     *
     */
    parsePostFiles: function (req, res, next) {
        common.formParseFiles(req, res, QUERY_FIELDS_MAX, function (fields, files) {
            Object.keys(fields).forEach(function (key) {
                fields[key] = common.escapeHtml(fields[key]);
            });
            req.fields = fields;
            req.files = files;
            next();
        });
    },

    /**
     *
     *
     */
    parsePutFiles: function (req, res, next) {
        common.formParseFiles(req, res, QUERY_FIELDS_MAX, function (fields, files) {
            Object.keys(fields).forEach(function (key) {
                fields[key] = common.escapeHtml(fields[key]);
            });
            req.fields = fields;
            req.files = files;
            next();
        });
    },

    /**
     *
     *
     */
    parseGet: function (req, res, next) {
        req.fields = req.query;
        next();
    },

    /**
     * Generic way to parse a form with protection from injection
     * will work for forms with files or HTTP GET
     */
    parseForm: function (req, res, next) {
        common.formParseFiles(req, res, QUERY_FIELDS_MAX, function (fields, files) {
            Object.keys(fields).forEach(function (key) {
                fields[key] = common.escapeHtml(fields[key]);
            });
            req.fields = fields;
            req.files = files;
            next();
        });
    },

    /**
     *
     *
     */

    sessionCheckGet: function (req, res, next) {
        if (CHECK_SESSION) {
            db.sessionCheckGet(req, res, function (req, res) {
                next();
            });
        } else {
            next();
        }
    },

    /**
     *
     *
     */
    sessionCheckPost: function (req, res, next) {
        if (CHECK_SESSION) {
            db.sessionCheckPost(req, res, req.fields, function (req, res) {
                next();
            });
        } else {
            next();
        }
    },

    /**
     *
     *
     */
    sessionCheckJson: function (req, res, next) {
        if (CHECK_SESSION) {
            db.sessionCheckJson(req, res, req.fields, function (req, res) {
                next();
            });
        } else {
            next();
        }
    },

    /**
     *
     *
     */
    sessionCheckFiles: function (req, res, next) {
        if (CHECK_SESSION) {
            db.sessionCheckFiles(req, res, req.files, function (req, res) {
                next();
            });
        } else {
            next();
        }
    },

    /**
     *
     *
     */
    uuidCheck: function (res, userKey, uuid, callback) {
        db.cache_hget("uuid_cache", userKey + "_" + uuid, function (cache) {
            if (cache && cache.reply) {
                if (LOG) common.log("MobileSessionManager", "uuidCheck: cached response returned, " +
                "user key " + userKey + ", uuid " + uuid);

                res.status(parseInt(cache.status)).json(JSON.parse(cache.reply));
            } else if (cache && cache.status == "0") {
                if (LOG) common.log("MobileSessionManager", "uuidCheck: similar request is being processed, " +
                "'subscribe' for the response, user key " + userKey + ", uuid " + uuid);

                var count = 0;
                var processing = setInterval(function () {
                    db.cache_hget("uuid_cache", userKey + "_" + uuid, function (cache) {
                        if (cache && cache.reply) {
                            if (LOG) common.log("MobileSessionManager", "uuidCheck: cached response returned after delay, " +
                            "user key " + userKey + ", uuid " + uuid);

                            res.status(parseInt(cache.status)).json(JSON.parse(cache.reply));
                            return clearInterval(processing);
                        }

                        count++;
                        if (count > 5) {
                            common.error("MobileSessionManager", "uuidCheck: failed to find 'subscribed' " +
                            "response after a delay, user key " + userKey + ", uuid " + uuid);

                            return clearInterval(processing);
                        }
                    });
                }, 2000);
            } else {
                db.cache_hput("uuid_cache", userKey + "_" + uuid, {"status": "0"}, 120000);
                callback();
            }
        });
    },

    /**
     *
     *
     */
    cacheAndReply: function (res, userKey, uuid, status, result, callback) {
        var cacheKey = userKey + "_" + uuid;
        db.cache_hput("uuid_cache", cacheKey, {
            status: status,
            reply: JSON.stringify(result)
        }, 120000);
        res.status(status).json(result);
        if (callback) callback();
    },

    /**
     *
     *
     */
    teamUrlKeyCheck: function (req, res, next) {
        if (CHECK_TEAM_KEY) {
            var query = "SELECT id, name FROM teams WHERE apiKey=" + db.escape(req.query.teamKey);
            db.sql.query(query, function (err, rows) {
                if (!rows || rows.length == 0) {
                    return res.status(403).json({"code": -1, status: "ERROR", message: "Unauthorized"});
                }
                next();
            });
        } else {
            next();
        }
    },

    /**
     *
     *
     */
    getTeamAllowed: function (params, callback) {
        let getQuery = (key, activity) => {
            if (!key) return undefined;
            return (!activity) ?
                "SELECT t.id, t.name FROM teams t WHERE t.apiKey=" + db.escape(key) :
                "SELECT t.id, t.name FROM teams t LEFT JOIN teamsNotifications tn ON t.id=tn.teamID " +
                " LEFT JOIN notifications n ON tn.notificationID=n.id " +
                " WHERE t.apiKey=" + db.escape(key) + " AND n.activity=" + db.escape(activity);
        }
        const query = getQuery(params.key, params.activity);
        db.sql.query(query, function (err, rows) {
            if (!rows || rows.length == 0) {
                return callback(new Error('Unknown team'));
            }
            callback(undefined, rows[0]);
        });
    }

}
