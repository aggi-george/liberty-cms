var async = require('async');
var config = require('../../../config');
var common = require('../../../lib/common');
var db = require('../../../lib/db_handler');
var ec = require('../../../lib/elitecore');
var elitecoreUserService = require('../../../lib/manager/ec/elitecoreUserService');
var accountManager = require('./../../core/manager/account/accountManager');
const accountActions = require(`${global.__manager}/account/actions`);
var sessionManger = require('../base/sessionManager');
var billManager = require('./../../core/manager/account/billManager');
var detailsManager = require('./../../core/manager/account/detailsManager');
var repairManager = require('./../../core/manager/account/repairManager');
var portInManager = require('./../../core/manager/porting/portInManager');

var log = config.LOGBSSWS;

module.exports = {

    init: function (path, web) {

        // parse params
        web.post(path + '/*', sessionManger.parsePost);
        web.get(path + '/*', sessionManger.parseGet);

        // check team
        web.post(path + '/*', sessionManger.teamUrlKeyCheck);
        web.get(path + '/*', sessionManger.teamUrlKeyCheck);

        // api
        web.post(path + '/customer/delivery/rescheduled', rescheduledDelivery);
        web.post(path + '/customer/create', createCustomer);
        web.post(path + '/customer/activate/plan', activateAccountPlan);
        web.post(path + '/customer/sim/pair', postSIMPair);
        web.post(path + '/customer/deposit', postCustomerDeposit);
        web.post(path + '/customer/details', postCustomerDetails);
        web.get(path + '/customer/details', getCustomerDetails);
        web.post(path + '/customer/status/change', postCustomerStatusChange);
        web.get(path + '/customer/bills', customerBills);
        web.get(path + '/inventory', inventory);
    }
}

function createCustomer(req, res) {
    var location = req.fields.location;
    var planName = req.fields.planName;
    var accountInformation = req.fields.accountInformation;
    var orderInformation = req.fields.orderInformation;
    var paymentGateway = req.fields.paymentGateway;
    var personalInformation = req.fields.personalInformation;
    var advancedPayments = req.fields.advancedPayments;

    if (log) common.log("CustomerApi", "location=" + location + ", planName=" + planName
    + ", accountInformation=" + JSON.stringify(accountInformation)
    + ", orderInformation=" + JSON.stringify(orderInformation)
    + ", paymentGateway=" + JSON.stringify(paymentGateway)
    + ", personalInformation=" + JSON.stringify(personalInformation)
    + ", advancedPayments=" + JSON.stringify(advancedPayments));

    accountManager.createAccount(location, planName, accountInformation, orderInformation,
        paymentGateway, personalInformation, advancedPayments, function (err, result) {
            if (err) {
                return res.status(400).json({code: -1, status: err.status, error: err.message});
            } else {
                return res.json({code: 0, result: result});
            }
        });
}

function rescheduledDelivery(req, res) {
    var serviceInstanceId = req.fields.serviceInstanceId;
    var newDeliveryDate = req.fields.newDeliveryDate ? new Date(req.fields.newDeliveryDate) : undefined;

    if (!newDeliveryDate || !newDeliveryDate.getTime()) {
        return res.status(400).json({
            code: -1, status: "ERROR_INVALID_DATA_FORMAT",
            error: "Failed to parse new delivery date " + req.fields.newDeliveryDate
        });
    }

    var newPortInDate = new Date(newDeliveryDate.getTime() + 24 * 60 * 60 * 1000);
    if (log) common.log("CustomerApi", "serviceInstanceId=" + serviceInstanceId
    + ", newDeliveryDate=" + newDeliveryDate + ", newPortInDate=" + newPortInDate);

    elitecoreUserService.loadUserInfoByServiceInstanceNumber(serviceInstanceId, (err, cache) => {
        if (err) {
            return res.status(400).json({code: -1, status: err.status, error: err.message});
        }

        if (!cache || !cache.number) {
            return res.status(400).json({
                code: -1, status: "ERROR_CUSTOMER_NOT_FOUND",
                error: "Failed to find a customer by serviceInstanceId " + serviceInstanceId
            });
        }

        portInManager.reScheduleActiveRequest(cache.prefix, cache.number, newPortInDate,
            function (err, result) {
                if (err) {
                    return res.status(400).json({code: -1, status: err.status, error: err.message});
                }

                return res.json({code: 0, result: result});
            }, undefined, "[ECOMM][" + serviceInstanceId + "]", "Customer requested to reschedule delivery to " + newDeliveryDate);
    });
}

function activateAccountPlan(req, res) {
    var location = req.fields.location;
    var planName = req.fields.planName;
    var registrationPayment = req.fields.registrationPayment;
    var serviceInstanceId = req.fields.serviceInstanceId;
    var serviceInstanceInformation = req.fields.serviceInstanceInformation;

    if (log) common.log("CustomerApi", "original request, request=" + JSON.stringify(req.fields));
    if (log) common.log("CustomerApi", "location=" + location + ", planName=" + planName
    + ", registrationPayment=" + JSON.stringify(registrationPayment)
    + ", serviceInstanceId=" + JSON.stringify(serviceInstanceId)
    + ", serviceInstanceInformation=" + JSON.stringify(serviceInstanceInformation));

    accountManager.activateAccountPlan(location, planName, serviceInstanceId,
        serviceInstanceInformation, registrationPayment, function (err, result) {
            if (err) {
                return res.status(400).json({code: -1, status: err.status, error: err.message});
            } else {
                return res.json({code: 0, result: result});
            }
        });
}

function postSIMPair(req, res) {
    var serviceInstanceId = req.fields.serviceInstanceId;
    var number = req.fields.number;
    var iccid = req.fields.iccid;
    if (log) common.log("CustomerApi", "postSIMPair: serviceInstanceId=" + serviceInstanceId +
    ", number=" + number + ", iccid=" + iccid);

    repairManager.simPair(serviceInstanceId, number, iccid, (err, result) => {
        if (err) {
            return res.status(400).json({code: -1, status: err.status, error: err.message});
        } else {
            return res.json({code: 0, result: result});
        }
    });
}

function postCustomerDeposit(req, res) {
    var customerAccountId = req.fields.customerAccountId;
    var amount = parseFloat(req.fields.amount);
    if (log) common.log("CustomerApi", "customerDeposit: customerAccountId=" + customerAccountId + ", amount=" + amount);

    billManager.addDepositPayment(customerAccountId, amount, (err, result) => {
        if (err) {
            return res.status(400).json({code: -1, status: err.status, error: err.message});
        } else {
            return res.json({code: 0, result: result});
        }
    });
}

function postCustomerDetails(req, res) {
    var customerAccountId = req.fields.customerAccountId;
    var details = req.fields.details;

    if (log) common.log("CustomerApi", "postCustomerDetails: customerAccountId=" +
    customerAccountId + ", details=" + JSON.stringify(details));

    detailsManager.updateAccountDetails(customerAccountId, details, (err, result) => {
        if (err) {
            return res.status(400).json({code: -1, status: err.status, error: err.message});
        } else {
            return res.json({code: 0, result: result});
        }
    });
}

function getCustomerDetails(req, res) {
    var serviceInstanceId = req.fields.serviceInstanceId;
    var number = req.fields.number;
    if (log) common.log("CustomerApi", "getCustomerDetails: serviceInstanceId=" + serviceInstanceId + ", number=" + number);

    if (!serviceInstanceId && !number) {
        return res.status(400).json({
            code: -1, status: "ERROR_INVALID_PARAMS",
            error: "Invalid params, serviceInstanceId & number are empty"
        });
    }

    var loadFunction = serviceInstanceId
        ? elitecoreUserService.loadUserInfoByServiceInstanceNumber
        : elitecoreUserService.loadUserInfoByPhoneNumber;
    var paramId = serviceInstanceId
        ? serviceInstanceId
        : number;

    loadFunction(paramId, (err, cache) => {
        if (err) {
            return res.status(400).json({code: -1, status: err.status, error: err.message});
        }

        if (!cache) {
            return res.status(400).json({
                code: -1, status: "ERROR_CUSTOMER_NOT_FOUND",
                error: "Failed to find a customer by serviceInstanceId " + serviceInstanceId
            });
        }

        return res.json({code: 0, result: {
            customerAccountId:cache.customerAccountNumber,
            billingAccountId:cache.billingAccountNumber,
            serviceAccountId:cache.serviceAccountNumber,
            serviceInstanceId:cache.serviceInstanceNumber,
            prefix: cache.prefix,
            number: cache.number,
            status: cache.status,
            name: cache.billingFullName
        }});
    });
}

function postCustomerStatusChange(req, res) {
    var serviceInstanceId = req.fields.serviceInstanceId;
    var status = req.fields.status;
    var reason = req.fields.reason;

    var initiator = "[ECOMM][" + serviceInstanceId + "]";
    var executionKey = config.NOTIFICATION_KEY;

    if (log) common.log("CustomerApi", "postCustomerStatusChange: serviceInstanceId=" +
    serviceInstanceId + ", status=" + status + ", reason=" + reason);

    accountActions.changeStatus(serviceInstanceId, status, "Middleware",
        reason, initiator, executionKey, (err, result) => {
            if (err) {
                return res.status(400).json({code: -1, status: err.status, error: err.message});
            } else {
                return res.json({code: 0, result: result});
            }
        });
}

function customerBills(req, res) {
    var customerAccountIds = req.fields.customerAccountIds;
    var customerAccountId = req.fields.customerAccountId;
    if (log) common.log("CustomerApi", "customerBills: customerAccountIds=" + JSON.stringify(customerAccountIds)
    + ", customerAccountId=" + customerAccountId);

    var addTask = (tasks, accountId) => {
        tasks.push((callback) => {
            billManager.loadBillHistoryByAccount(accountId, (err, result) => {
                var unpaidBills = [];
                var paidBills = [];
                var totalOutstanding = 0;

                if (!err && result && result.bills) {
                    result.bills.forEach((bill) => {
                        var outstanding = bill.price.value - bill.paid.value;
                        if (outstanding > 0) {
                            totalOutstanding += outstanding;
                            unpaidBills.push({
                                invoiceId: bill.id,
                                amount: bill.price.value,
                                outstanding: outstanding
                            });
                        } else {
                            paidBills.push({
                                invoiceId: bill.id,
                                amount: bill.price.value
                            });
                        }
                    });
                }

                callback(err, {
                    accountId: accountId,
                    totalOutstanding: totalOutstanding,
                    unpaidBills: unpaidBills,
                    paidBills: paidBills
                });
            });
        });
    }

    var tasks = [];
    if (customerAccountIds) {
        customerAccountIds.forEach((accountId) => {
            addTask(tasks, accountId);
        });
    }

    if (customerAccountId) {
        addTask(tasks, customerAccountId);
    }

    async.parallelLimit(tasks, 5, function (err, resultList) {
        if (err) {
            return res.status(400).json({code: -1, status: err.status, error: err.message});
        } else {
            return res.json({code: 0, result: resultList});
        }
    });
}

function inventory(req, res) {
    var status = req.fields.status;
    var type = req.fields.type;
    var limit = req.fields.limit ? parseInt(req.fields.limit) : 0;
    var offset = req.fields.offset ? parseInt(req.fields.offset) : 0;
    var inventoryNumberLength = req.fields.inventoryNumberLength ? parseInt(req.fields.inventoryNumberLength) : 0;
    var inventoryCategory = req.fields.inventoryCategory;

    if (log) common.log("CustomerApi", "inventory: status=" + status + ", type=" + type +
    ", inventoryCategory=" + inventoryCategory + ", limit=" + limit + ", offset=" + offset +
    ", inventoryNumberLength=" + inventoryNumberLength);

    accountManager.loadInventory(status, type, {
        limit,
        offset,
        inventoryCategory,
        inventoryNumberLength
    }, (err, resultList) => {
        if (err) {
            return res.status(400).json({code: -1, status: err.status, error: err.message});
        } else {
            return res.json({code: 0, result: resultList});
        }
    });
}
