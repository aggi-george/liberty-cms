const config = require('../../../config');
const common = require('../../../lib/common');
const db = require('../../../lib/db_handler');
const ec = require('../../../lib/elitecore');
const usageManager = require('../../core/manager/account/usageManager');
const paymentManager = require('./../../core/manager/account/paymentManager');
const billManager = require('./../../core/manager/account/billManager');
const anomalyManager = require('./../../core/manager/account/anomalyManager');
const accountManager = require('./../../core/manager/account/accountManager');

const bss = require(`${global.__bss}/index`);

const FIELDS_LIMIT = 20;
const LOG = config.LOGSENABLED;


function getProvisioningReport(req, res) {
  const { numbers, report } = req.fields;

  if (numbers && numbers instanceof Array && numbers.length) {
    return anomalyManager.getCustomerProvisioningReport(req.fields, (err, response) => {
      if (!err && response) {
        const { notificationId, result } = response;
        if (report) {
          return res.json({
            code: 0,
            result,
            notificationId: notificationId || null,
          });
        }
        return res.json({
          code: 0,
          result: 'OK',
          notificationId: notificationId || null,
        });
      }
      return res.json({
        code: -1,
        result: 'Failed',
      });
    });
  }
  return res.json({
    code: -1,
    result: 'Failed: Numbers are not provided or Body data doesnt include numbers field',
  });
}

module.exports = {

  init(baseApi, category, web) {
    let path = baseApi + category;

    // Parse params
    web.put(`${path}/*`, parsePut);
    web.post(`${path}/*`, parsePost);

    // packages - hierarchy all
    path = `${path}/`;
    web.get(`${path}usage/data/:prefix/:number/:key`, loadDataUsage);
    web.post(`${path}provisioning/anomaly/:key`, getProvisioningReport);
    web.get(`${path}inventory/status/:number/:key`, getNumberStatus);

    web.post(`${path}customer/update/:key`, updateCustomerBillingAccount);
    web.post(`${path}customer/addons/subscribe/:key`, subscribeAddOnPackage);
  },
};

function parsePut(req, res, next) {
  common.formParse(req, res, FIELDS_LIMIT, (fields) => {
        req.fields = fields;
        next();
    });
}

function parsePost(req, res, next) {
  common.formParse(req, res, FIELDS_LIMIT, (fields) => {
        req.fields = fields;
        next();
    });
}

/**
 *
 * @param req - sample body below
 {
   "addOnPlans": [{"packageId":""}],
   "serviceInstanceNumber": "LW100276"
 }
 * @param res
 */
function subscribeAddOnPackage(req,res){
    let addOnPlans = req.fields.addOnPlans;
    let serviceInstanceNumber = req.fields.serviceInstanceNumber;


    if (LOG) common.log(`CustomerApi - subscribeAddOnPackage`, `serviceInstanceNumber=${serviceInstanceNumber}, , addOnPlans=${JSON.stringify(addOnPlans)}`);

    bss.subscribeAddOnPackage({addOnPlans,serviceInstanceNumber}, function (err, result) {
        if (err) {
            return res.status(400).json({code: -1, status: err.status, error: err.message});
        } else {
            return res.json({code: 0, result: result});
        }
    });
}

/**
 *
 * @param req sample body below
 {
   "accountNumber": "LW100274",
   "addressDetails":{
          "addressOne": "2113, Henderson road, #6-10",
          "addressTwo": "Henderson road;6;10;2113",
          "zipPostalCode": "510124"
         },
    "accountProfiles": {
               "strcustom1": "20140916001-03112017-1509679237631",
               "strcustom2": "20140916001",
               "strcustom3": "uob",
               "strcustom4": "visa",
               "strcustom5": "1111",
               "strcustom11": "uob"
           }

 }
 * @param res
 */
function updateCustomerBillingAccount(req,res){
    let accountNumber = req.fields.accountNumber;
    let addressDetails = req.fields.addressDetails;
    let accountProfiles= req.fields.accountProfiles;


    if (LOG) common.log(`CustomerApi - updateCustomerBillingAccount`, `accountNumber=${accountNumber}, , addressDetails=${JSON.stringify(addressDetails)}, accountProfiles=${JSON.stringify(accountProfiles)}`);

    bss.updateBillingAccount({accountNumber,addressDetails,accountProfiles}, function (err, result) {
        if (err) {
            return res.status(400).json({code: -1, status: err.status, error: err.message});
        } else {
            return res.json({code: 0, result: result});
        }
    });
}

function loadDataUsage(req, res) {
  const code = req.params.code;
  const prefix = req.params.prefix;
  const number = req.params.number;

  usageManager.computeUsage(prefix, number, (err, usageResult) => {
        if (err) {
            return res.status(400).json({
                code: 0,
                error: err.message,
                status: "ERROR"
            });
        }

        let leftKb = 0;
        let usedKb = 0;
        let prorated = false;

        const addData = function(chunk) {
            if (chunk) {
                leftKb += chunk.left;
                usedKb += (chunk.used - (chunk.prorated ? (chunk.plan_kb - chunk.prorated_kb) : 0));

                if (chunk.prorated) {
                    prorated = true;
                }
            }
        };

        if (usageResult && usageResult.data) {
            addData(usageResult.data.basic);
            addData(usageResult.data.bonus);
            addData(usageResult.data.boost);
            addData(usageResult.data.extra);
        }

        const status = usedKb > 0 ? "ACTIVE" : "INACTIVE";
        return res.json({
            code: 0,
            status: status,
            leftKb: parseInt(leftKb),
            usedKb: parseInt(usedKb),
            prorated: prorated
        });
    });
}

function getNumberStatus(req, res) {
  const {number} = req.params;

  return accountManager.getInventoryStatusFromEC(number)
      .then(({type, status}) => {
        return res.json({
          "code": 0,
          "number": number,
          type,
          status
        });
      })
      .catch(err => {
        common.error(`AccountAPI.getNumberStatus() Error: ${err} for number: ${number}`);
        return res.json({
          code: -1,
          number,
          status: 'Failed'
        });
      });
}
