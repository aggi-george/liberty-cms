var config = require('../../../config');
var common = require('../../../lib/common');
var db = require('../../../lib/db_handler');
var async = require('async');

var LOG = config.LOGSENABLED;

module.exports = {

    init: function (baseApi, category, web) {
        var path = baseApi + category;
        var path = path + '/';
        web.get(path + 'queue/:key', queue);
    },

    getQueueStats : getQueueStats,

    getAPI: function () {
        return api;
    }
}

function queue(req, res) {
    if (req.ip != "10.20.1.20") return res.status(403).json({ "code" : -1 });
    db.query_noerr("SELECT id FROM teams WHERE apiKey=" + db.escape(req.params.key), function (rows) { 
        if ( !rows || !rows.length || (rows[0].id != 5) ) return res.status(403).json({ "code" : -1 });
        getQueueStats(function (err, result) {
            if (!err) {
                res.json({ "code" : 0, "result" : result });
            } else {
                res.json({ "code" : -1 });
            }
        });
    });
}

function getQueueStats(callback) {
    var type = [ "email", "hlr", "aggregation", "sms", "slow", "push", "invoicePayment", "creditCapPayment", "statusValidation", "ingress"];
    var tasks = new Array;
    var pushTask = function (i, id) {
            tasks.push(function (cb) {
                    db.queue[id + "Count"](type[i], function (err, total) {
                            cb(undefined, { "type" : type[i], "id" : id, "count" : total });
                    });
            });
    }
    for (var i in type) {
            pushTask(i, "active");
            pushTask(i, "inactive");
            pushTask(i, "failed");
            pushTask(i, "complete");
            pushTask(i, "delayed");
    }
    async.parallel(tasks, function (err, result) {
            var reply = new Object;
            result.forEach(function (item) {
                    if (item) {
                            if (!reply[item.type]) reply[item.type] = new Object;
                            reply[item.type][item.id] = item.count;
                    }
            });
        callback(err, reply);
    });
}
