var config = require('../../../config');
var common = require('../../../lib/common');
var db = require('../../../lib/db_handler');
var ec = require('../../../lib/elitecore');
var bonusManger = require('./../../core/manager/bonus/bonusManager');
var leaderboardManager = require('./../../core/manager/bonus/leaderboardManager');

var API_DOC = [];
var FIELDS_LIMIT = 20;
var LOG = config.LOGSENABLED;

module.exports = {

    init: function (baseApi, category, web) {
        var path = baseApi + category;

        //Parse params
        web.put(path + '/*', parsePut);
        web.post(path + '/*', parsePost);

        // packages - hierarchy all
        var path = path + '/';
        web.get(path + 'data/total/get/:key', dataTotal);
        web.get(path + 'leaderboard/get/:key', leaderboard);
        web.get(path + 'history/get/:prefix/:number/:key', historyBonus);
        web.get(path + 'available/get/:prefix/:number/:key', loadBonusesList);
        web.put(path + 'available/use/:id/:key', useBonus);
        
    },

    getAPI: function () {
        return api;
    }
}

function parsePut(req, res, next) {
    common.formParse(req, res, FIELDS_LIMIT, function (fields) {
        req.fields = fields;
        next();
    });
}

function parsePost(req, res, next) {
    common.formParse(req, res, FIELDS_LIMIT, function (fields) {
        req.fields = fields;
        next();
    });
}

function loadBonusesList(req, res) {
    var code = req.params.code;
    var prefix = req.params.prefix;
    var number = req.params.number;

    bonusManger.loadAvailableBonusList(prefix, number, true, function (error, items) {
        if (error) {
            return res.status(400).json({
                code: -1,
                status: error.id,
                error: error.message
            });
        } else {
            return res.json({
                code: 0,
                items: items
            });
        }
    });
}

function useBonus(req, res) {
    var code = req.params.code;
    var prefix = req.fields.prefix;
    var number = req.fields.number;
    var id = req.params.id;

    bonusManger.useAvailableBonus(prefix, number, id, function (error, result) {
        if (error) {
            res.status(400).json({
                code: -1,
                status: error.id,
                error: error.message
            });
        } else {
            res.json({
                code: 0,
                name: result.name,
                product_id: result.product_id,
                kb: result.kb
            });
        }
    });
}

function historyBonus(req, res) {
    var code = req.params.code;
    var prefix = req.params.prefix;
    var number = req.params.number;


    ec.getCustomerDetailsNumber(prefix, number, false, function (error, cache) {
        if (error) {
            return res.status(400).json({
                code: -1,
                status: error.id,
                error: error.message
            });
        }

        bonusManger.loadHistoryBonusListBySI(cache.serviceInstanceNumber, true, function (error, items, rating) {
            if (error) {
                return res.status(400).json({
                    code: -1,
                    status: error.id,
                    error: error.message
                });
            }

            leaderboardManager.loadGoldenCircleRatingBySI(cache.serviceInstanceNumber, (error, goldenTicketsResult) => {
                if (error) {
                    return res.status(400).json({
                        code: -1,
                        status: error.id,
                        error: error.message
                    });
                }

                if (!rating) {
                    rating = {};
                }
                if (goldenTicketsResult && goldenTicketsResult.position >= 0) {
                    rating.rating = {
                        goldenTickets: true,
                        goldenTicketsPosition: goldenTicketsResult.position,
                        goldenTicketsCount: goldenTicketsResult.ticketsCount
                    };
                }

                return res.json({
                    code: 0,
                    items: items,
                    rating: rating
                });
            });
        });
    });
}

function leaderboard(req, res) {
    var code = req.params.code;

    leaderboardManager.loadLeaderboard(function (error, dataLeaderboard) {
        if (error) {
            return res.status(400).json({
                code: -1,
                status: error.id,
                error: error.message
            });
        }

        leaderboardManager.loadGoldenCircleBoard(function (error, goldenCircleLeaderboard) {
            if (error) {
                return res.status(400).json({
                    code: -1,
                    status: error.id,
                    error: error.message
                });
            }

            return res.json({
                code: 0,
                items: dataLeaderboard,
                goldenCircle: goldenCircleLeaderboard
            });
        });
    });
}

function dataTotal(req, res) {
    bonusManger.loadTotalGivenBonus(function (error, result) {
        if (error) {
            return res.status(400).json({
                code: -1,
                status: error.id,
                error: error.message
            });
        } else {
            return res.json({
                code: 0,
                result: result
            });
        }
    });
}