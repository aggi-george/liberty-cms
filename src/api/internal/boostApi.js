var config = require('../../../config');
var common = require('../../../lib/common');
var db = require('../../../lib/db_handler');
var ec = require('../../../lib/elitecore');
var boostManager = require('./../../core/manager/boost/boostManager');

var API_DOC = [];
var FIELDS_LIMIT = 20;
var LOG = config.LOGSENABLED;

module.exports = {

    init: function (baseApi, category, web) {
        var path = baseApi + category;

        //Parse params
        web.put(path + '/*', parsePut);
        web.post(path + '/*', parsePost);

        // packages - hierarchy all
        var path = path + '/';
        web.get(path + 'free/available/:prefix/:number/:key', loadFreeBoostList);
        web.put(path + 'free/use/:id/:key', useFreeBoost);
    },

    getAPI: function () {
        return API_DOC;
    }
}

function parsePut(req, res, next) {
    common.formParse(req, res, FIELDS_LIMIT, function (fields) {
        req.fields = fields;
        next();
    });
}

function parsePost(req, res, next) {
    common.formParse(req, res, FIELDS_LIMIT, function (fields) {
        req.fields = fields;
        next();
    });
}

function loadFreeBoostList(req, res) {
    var code = req.params.code;
    var prefix = req.params.prefix;
    var number = req.params.number;

    boostManager.loadFreeBoostList(prefix, number, function (error, items) {
        if (error) {
            return res.status(400).json({
                code: -1,
                status: error.id,
                error: error.message
            });
        } else {
            return res.json({
                code: 0,
                items: items
            });
        }
    });
}

function useFreeBoost(req, res) {
    var code = req.params.code;
    var prefix = req.fields.prefix;
    var number = req.fields.number;
    var id = req.params.id;

    boostManager.useFreeBoost(prefix, number, id, function (error, result) {
        if (error) {
            res.status(400).json({
                code: -1,
                status: error.id,
                error: error.message
            });
        } else {
            res.json({
                code: 0,
                name: result.name,
                product_id: result.product_id,
                kb: result.kb
            });
        }
    });
}
