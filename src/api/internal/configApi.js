var config = require('../../../config');
var common = require('../../../lib/common');
var db = require('../../../lib/db_handler');
var ec = require('../../../lib/elitecore');
var configManager = require('./../../core/manager/config/configManager');

var API_DOC = [];
var FIELDS_LIMIT = 20;
var LOG = config.LOGSENABLED;

module.exports = {

    init: function (baseApi, category, web) {
        var path = baseApi + category;

        //Parse params
        web.put(path + '/*', parsePut);
        web.post(path + '/*', parsePost);

        // packages - hierarchy all
        var path = path + '/';
        web.get(path + 'map/get/:prefix/:number/:type/:key', loadConfigMap);
    },

    getAPI: function () {
        return api;
    }
}

function parsePut(req, res, next) {
    common.formParse(req, res, FIELDS_LIMIT, function (fields) {
        req.fields = fields;
        next();
    });
}

function parsePost(req, res, next) {
    common.formParse(req, res, FIELDS_LIMIT, function (fields) {
        req.fields = fields;
        next();
    });
}

function loadConfigMap(req, res) {
    var code = req.params.code;
    var type = req.params.type;
    var prefix = req.params.prefix;
    var number = req.params.number;

    configManager.loadConfigMapForGroup(type, function (error, config) {
        if (error) {
            return res.status(400).json({
                code: -1,
                status: error.id,
                error: error.message
            });
        } else {

            // apply test configuration if required

            if (config && config.length > 0) {
                config.forEach(function(item) {
                    if (item.key === "settings") {
                        var testNumber = item.options.settings_show_all_test_number;
                        if (testNumber === number) {
                            item.options.help_show_whatsapp = true;
                            item.options.settings_show_autoboost_limit = true;
                            item.options.settings_show_port_in_later = true;
                        }
                    }
                });
            }

            return res.json({
                code: 0,
                config: config
            });
        }
    });
}
