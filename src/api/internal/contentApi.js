var fs = require('fs');
var config = require('../../../config');
var common = require('../../../lib/common');
var db = require('../../../lib/db_handler');
var ec = require('../../../lib/elitecore');
var contentsRates = require(__core + '/manager/contents/rates');

var postFieldsLimit = 20;
//var LOG = config.LOGSENABLED;
var logsEnabled = false;

var api = new Array;

//exports

exports.init = init;
exports.getAPI = getAPI;
exports.getContents = getContents;

//functions

function init(baseApi, category, web) {
    var path = baseApi + category;

    // packages - hierarchy all
    var usagePath = path + '/get';
    web.get(usagePath + '/:cat/:type/:key', get);
    api.push({    "transport" : "HTTPS",
            "method" : "GET",
            "api" : usagePath + '/:cat/:type/:key',
            "parameters" : "cat : terms (type : general, mobile, payment, website, dataprotection, copyright, circlescare, circlestalk) " +
                    "<BR> cat : rates (type : circlestalk, idd002, idd021, ppurroaming) " +
                    "<BR> cat : faq (type : general, myplan, technical) <BR> cat : blog ()" });

}

function parsePost(req, res, next) {
    common.formParse(req, res, postFieldsLimit, function(fields) {
        req.fields = fields;
        next();
    });
}

function getAPI() {
    return api;
}

function get(req, res) {
    var q = "SELECT id FROM teams WHERE apiKey=" + db.escape(req.params.key);
    db.query_noerr(q, function(result) {
        if ( result && result[0] &&
            ((result[0].id == 2) ||        // ECommerce
            (result[0].id == 1)) ) {    // Mobile
            var cat = req.params.cat;
            var type = req.params.type;
            var prefix = (req.query) ? req.query.prefix : undefined;
            var iso = (req.query) ? req.query.iso : undefined;
            getContents(cat, type, prefix, iso, function (err, result) {
                if (err && !result) {
                    res.status(400).send({ "code" : -1 });
                } else {
                    if ( cat == "faq" && result.content ) {
                        var reply = new Array;
                        result.content.forEach(function (item) {
                            delete item.id;
                            if ( item.orderID > 0 ) reply.push(item);
                        });
                        res.json({ "code" : 0, "result" : {
                                        "content" : reply,
                                        "cat" : cat,
                                        "type" : type } });
                    } else {
                        res.json({ "code" : 0, "result" : result });
                    }
                }
            });
        } else {
            if ( logsEnabled) common.log("hierarchy bonus 403", req.ip + " " + req.params.key);
            res.status(403).send("403");
        }
    });
}

function getContents(cat, type, prefixQuery, iso, callback) {
    var result = new Object;
    result.cat = cat;
    result.type = type;
    var q2 = "";
    if ( cat == "rates" ) {
        contentsRates.get(type, prefixQuery, iso, callback);
    } else {
        if ( cat == "faq" ) {
            q2 = "SELECT id, orderID, question, answer FROM faq WHERE category=" + db.escape(type);
        } else {
            q2 = "SELECT content FROM contents WHERE contentName=" + db.escape(cat + "_" + type);
        }
        db.query_noerr(q2, function (rows) {
            if (rows && rows[0]) {
                if ( cat == "faq" ) {
                    result.content = rows;
                } else {
                    result.content = rows[0].content;
                }
                callback(undefined, result);
            } else callback(true);
        });
    }
}
