var config = require('../../../config');
var common = require('../../../lib/common');
var db = require('../../../lib/db_handler');
var healthManager = require('../../../src/core/manager/system/healthManager');
var async = require('async');

var LOG = config.LOGSENABLED;

module.exports = {

    init: function (baseApi, category, web) {
        var path = baseApi + category;
        var path = path + '/';
        web.get(path + 'ping', ping);
        web.get(path + 'test', test);
        web.get(path + 'pm2raw', getPm2Raw);
    },

    getAPI: function () {
        return api;
    }
}

function ping(req, res) {
    async.parallel([function(callback){
		callback(null, {});
	}], function(err, result){
		res.json({"ok": 1});
	});
}

function test(req, res){
    healthManager.executeBasicHealth(function(result){
        res.json(result);
    });
}

function getPm2Raw(req, res){
    healthManager.extractImportantParamsFromPm2(function(err, result){
        if(err){
            res.json({code: -1, error: err});
        }else{
            res.json({code: -1, error: result});
        }
    });
}
