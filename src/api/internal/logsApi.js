var config = require('../../../config');
var common = require('../../../lib/common');
var db = require('../../../lib/db_handler');

var LOG = config.LOGSENABLED;

var correctKey = ['394kbi8X', 'C2IurIIF', '95CHx1rh' ];

module.exports = {

    init: function (baseApi, category, web) {
        var path = baseApi + category;
        var path = path + '/';
        web.post(path + 'commands/:key', commands);
    },

    getAPI: function () {
        return api;
    }
}

function commands(req, res) {
    var fullBody = '';
    var insert_me = '';
    if (correctKey.indexOf(req.params.key) == -1) {
        common.log("Audit Log: Invalid Key"); 
        return res.status(400).json({
            code: -1, status: "invalid key"
        })	
    }

    req.on('data', function(chunk) {
        fullBody += chunk.toString();
        insert_me = common.safeParse(fullBody);
        if (typeof(insert_me) == "object") db.notifications_insert("auditlog", insert_me, function () {
            //common.log("Audit Log: " + JSON.stringify(fullBody))
        });
    });				

    return res.status(200).json({
        code: 0, status: "OK"
    })
}
