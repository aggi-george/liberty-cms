var config = require('../../../config');
var common = require('../../../lib/common');
var db = require('../../../lib/db_handler');
var sessionManager = require('../base/sessionManager');

var log = config.LOGSENABLED;

var header;
var footer;

module.exports = {

    /**
     *
     */
    init: function (baseApi, category, web) {
        var path = baseApi + category;

        // Parse params

        web.post(path + '/*', sessionManager.parsePost);
        web.put(path + '/*', sessionManager.parsePutFiles);

        // notifications - get logs API
        var usagePath = path + '/get';
        web.get(usagePath + '/:serviceInstanceNumber/:key', getUserNotifications);

        // notifications - set read API
        var usagePath = path + '/read/set';
        web.post(usagePath + '/:serviceInstanceNumber/:key', setReadUserNotifications);

        var settingsPath = path + '/settings';
        web.put(settingsPath + '/unsubscribe/:key', unsubscribeNotifications);
        web.get(settingsPath + '/options/:profileId/:key', getSubscribedNotifications);
    },

    /**
     *
     */
    getAPI: function () {
        return [];
    }

}

// API fuctions

function getUserNotifications(req, res) {
    var q = "SELECT id FROM teams WHERE apiKey=" + db.escape(req.params.key);
    db.query_noerr(q, function (result) {
        if (result && result[0] && (result[0].id == 1)) {
            var query = new Object;
            query.serviceInstanceNumber = req.params.serviceInstanceNumber;
            query.transport = "selfcare";
            query.app = {"$exists": true};
            if (req.query.anchor) query.ts = {"$gt": parseInt(req.query.anchor)};
            var projection = new Object;
            projection.ts = 1;
            projection.read = 1;
            projection.app = 1;
            projection.content = 1;
            db.notifications_logbook_get(query, projection, {"_id": -1}, function (group) {
                var list = new Array;
                if (group) group.forEach(function (item) {
                    var notif = new Object;
                    if (item.app) {
                        notif.id = item._id.toString();
                        notif.date = new Date(item.ts).toISOString();
                        notif.anchor = item.ts.toString();
                        notif.read = item.read;
                        notif.mime_type = item.app.mime_type;
                        notif.title = item.app.title;
                        notif.subtitle = item.app.subtitle;
                        notif.text = item.app.text;
                        notif.link = item.app.link;
                        notif.image_preview = item.app.image_preview;
                        notif.image = item.app.image;
                        notif.content = item.content;
                        list.push(notif);
                    }
                });
                res.json({"code": 0, "list": list});
            });
        } else {
            if (log) common.log("NotificationManager", "setReadUserNotifications: 403 " + req.ip + " " + req.params.key);
            res.status(403).send("403");
        }
    });
}

function setReadUserNotifications(req, res) {
    var query = "SELECT id FROM teams WHERE apiKey=" + db.escape(req.params.key);
    db.query_noerr(query, function (result) {
        if (result && result[0] && (result[0].id == 1) && req.fields.ids) {
            var list = req.fields.ids.split(",");
            db.notifications_logbook_set_read(list, function (group) {
                res.json({"code": 0});
            });
        } else {
            if (log) common.log("NotificationManager", "setReadUserNotifications: 403 " + req.ip + " " + req.params.key);
            res.status(403).send("403");
        }
    });
}

function unsubscribeNotifications(req, res) {
    var unsubscribedIds = req.fields.unsubscribedIds;
    var profileId = req.fields.profileId;

    if (!unsubscribedIds) {
        unsubscribedIds = [];
    }
    if (typeof unsubscribedIds == 'string') {
        unsubscribedIds = unsubscribedIds.split(',');
        unsubscribedIds.forEach((id, indx) => {
            unsubscribedIds[indx] = parseInt(id);
        })
    }

    var query = "SELECT service_instance_number FROM customerProfile WHERE id=" + db.escape(profileId);
    db.query_err(query, function (err, result) {
        if (err) {
            return res.status(400).json({
                code: -1,
                status: "ERROR_SQL",
                error: err.message
            });
        }

        if (!result || !result[0]) {
            return res.status(400).json({
                code: -1,
                status: "ERROR_PROFILE_NOT_FOUND",
                error: "Unfortunately, no customer profile found, please try make sure you use valid profile Id"
            });
        }

        db.transaction(function (err, connection) {
            if (err) {
                return res.status(400).json({
                    code: -1,
                    status: "ERROR_TRANSACTION",
                    error: err.message
                });
            }

            var rollbackCallback = function (err) {
                connection.rollback(function () {
                    connection.release();
                    res.status(400).json({
                        code: -1,
                        status: "ERROR_TRANSACTION_ROLLBACK",
                        error: err.message
                    });
                });
            };

            var commitCallback = function () {
                connection.commit(function (err) {
                    if (err) {
                        return rollbackCallback(err);
                    }

                    connection.release();
                    res.json({code: 0});
                });
            };

            var queries = [];
            queries.push("DELETE FROM notificationsUnsubscribtions WHERE profileId=" + db.escape(profileId));
            unsubscribedIds.forEach(function (groupId) {
                queries.push("INSERT INTO notificationsUnsubscribtions (profileId, groupId) " +
                "VALUES (" + db.escape(profileId) + ", " + db.escape(groupId) + ")");
            });
            var countProcessed = 0;
            var failed = false;

            queries.forEach(function (query) {
                connection.query(query, function (err, result) {
                    countProcessed++;

                    // ignore if already failed
                    if (failed) {
                        return;
                    }
                    if (err) {
                        failed = true;
                        return rollbackCallback(err);
                    }
                    if (countProcessed == queries.length) {
                        return commitCallback();
                    }
                });
            });
        });
    });
}

function getSubscribedNotifications(req, res) {
    var profileId = req.params.profileId;

    var query = "SELECT service_instance_number FROM customerProfile WHERE id=" + db.escape(profileId);
    db.query_err(query, function (err, result) {
        if (err) {
            return res.status(400).json({
                code: -1,
                status: "ERROR_SQL",
                error: err.message
            });
        }

        if (!result || !result[0]) {
            return res.status(400).json({
                code: -1,
                status: "ERROR_PROFILE_NOT_FOUND",
                error: "Unfortunately, no customer profile found, please try make sure you use valid profile Id"
            });
        }

        var query = "SELECT ng.*, IF((SELECT count(*) FROM notificationsUnsubscribtions nu " +
            "WHERE nu.profileId=" + db.escape(profileId) + " AND nu.groupId=ng.id)=0, 1, 0) " +
            "as subscribed FROM notificationsGroup ng";

        db.query_err(query, function (err, result) {
            if (err) {
                return res.status(400).json({
                    code: -1,
                    status: "ERROR_SQL",
                    error: err.message
                });
            }

            if (!result || !result[0]) {
                return res.status(400).json({
                    code: -1,
                    status: "ERROR_NO_NOTIFICATION_GROUPS_FOUND",
                    error: "Unfortunately, no group have been defined, please create at least one notification group"
                });
            }
            res.json({"code": 0, groups: result});
        });
    });
}
