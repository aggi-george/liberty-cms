var aws = require('aws-sdk');
var async = require('async');
var config = require('../../../config');
var common = require('../../../lib/common');
var db = require('../../../lib/db_handler');
var ec = require('../../../lib/elitecore');
var packagesHelper = require('../../../lib/packages_helper');
var packageManager = require(__core + '/manager/packages/packageManager');
var hlrManager = require('../../core/manager/hlr/hlrManager');

var postFieldsLimit = 20;
var LOG = config.LOGSENABLED;

aws.config.update({ region : "ap-southeast-1", accessKeyId : config.AWS_KEY, secretAccessKey : config.AWS_SECRET });
var s3 = new aws.S3();

var api = new Array;

//exports

exports.init = init;
exports.getAPI = getAPI;

//functions

function init(baseApi, category, web) {
	var path = baseApi + category;

	//Parse params
	web.put(path + '/*', parseFields);
	web.delete(path + '/*', parseFields);

	// packages - hierarchy all
	var usagePath = path + '/hierarchy';
	web.get(usagePath + '/get/all/:key', all);
	api.push({	"transport" : "HTTPS", 
			"method" : "GET",
			"api" : usagePath + '/get/all/:key',
			"parameters" : "" }); 

	// packages - hierarchy bonus addons
	var usagePath = path + '/hierarchy';
	web.get(usagePath + '/get/bonus/:key', bonus);
	api.push({	"transport" : "HTTPS", 
			"method" : "GET",
			"api" : usagePath + '/get/bonus/:key',
			"parameters" : "" }); 

	// packages - hierarchy general addons
	var usagePath = path + '/hierarchy';
	web.get(usagePath + '/get/general/:key', general);
	api.push({	"transport" : "HTTPS", 
			"method" : "GET",
			"api" : usagePath + '/get/general/:key',
			"parameters" : "" }); 

	// packages - add addon
	var usagePath = path + '/addon';
	web.put(usagePath + '/update/:prefix/:number/:product/:key', addonUpdateAPI);
	api.push({	"transport" : "HTTP", 
			"method" : "PUT",
			"api" : usagePath + '/update/:prefix/:number/:product/:key',
			"parameters" : "" }); 

	// packages - remove addon
	var usagePath = path + '/addon';
	web.delete(usagePath + '/update/:prefix/:number/:product/:key', addonUpdateAPI);
	api.push({	"transport" : "HTTP", 
			"method" : "DELETE",
			"api" : usagePath + '/update/:prefix/:number/:product/:key',
			"parameters" : "" }); 

}

function parseFields(req, res, next) {
	common.formParse(req, res, postFieldsLimit, function(fields) {
		req.fields = fields;
		next();
	});
}

function getAPI() {
	return api;
}

function general(req, res) {
	var q = "SELECT id FROM teams WHERE apiKey=" + db.escape(req.params.key);
	db.query_noerr(q, function(result) {
		if ( result && result[0] && ((result[0].id == 1) ||		// Mobile App
						(result[0].id == 2)) ) {	// Ecommerce
			hierarchy(result[0].id, function(converged) {
				res.json({ "code" : 0, "result" : converged.general });
			});
		} else {
			if (LOG) common.log("hierarchy general 403", req.ip + " " + req.params.key);
			res.status(403).send("403");
		}
	});
}

function bonus(req, res) {
	var q = "SELECT id FROM teams WHERE apiKey=" + db.escape(req.params.key);
	db.query_noerr(q, function(result) {
		if ( result && result[0] && ((result[0].id == 1) ||		// Mobile App
						(result[0].id == 2)) ) {	// Ecommerce
			hierarchy(result[0].id, function(converged) {
				res.json({ "code" : 0, "result" : converged.bonus });
			});
		} else {
			if (LOG) common.log("hierarchy bonus 403", req.ip + " " + req.params.key);
			res.status(403).send("403");
		}
	});
}

function all(req, res) {
	var q = "SELECT id FROM teams WHERE apiKey=" + db.escape(req.params.key);
	db.query_noerr(q, function(result) {
		if ( result && result[0] && ((result[0].id == 1) ||		// Mobile App
						(result[0].id == 2)) ) {	// Ecommerce
			hierarchy(result[0].id, function(converged) {
				res.json({ "code" : 0, "result" : converged });
			});
		} else {
			if (LOG) common.log("hierarchy all 403", req.ip + " " + req.params.key);
			res.status(403).send("403");
		}
	});
}

function hierarchy(teamID, callback) {
	packagesHelper.loadPackages(teamID, function (rows) {
		var converged = JSON.parse(JSON.stringify(ec.packages()));		// deep copy
		var count = 0;
		var override = [
		// super dirty hack : if ecomm and package is:
			"PRD00316", // Plus 4G Speed Max
			"PRD00445", // Plus Caller Number Display
			"PRD00317", // Plus Plan Change
			"PRD00315", // Plus Roaming
			"PRD00440" // Whatsapp_Unlimited
		// make action = 0
		];
		var params = { "Bucket" : config.AWS_BUCKET,
				"Prefix" : "assets/" };
		s3.listObjects(params, function(err, list) {
			rows.forEach(function (row) {
				if ( list && list.Contents && (list.Contents.length > 0) ) {
					if (!row.assets) row.assets = new Array;
					list.Contents.forEach(function (item) {
						if ( item && item.Key && 
							(item.Key.split('/')[1] == row.product_id) &&
							(item.Key.split('/')[2]) ) {
							row.assets.push(config.IMAGEHOST + "/" + item.Key);
						}
					});
				}
				if ( (teamID == 2) && (override.indexOf(row.product_id) > -1) ) row.action = 0; 
				packagesHelper.updateProductHierarchy(ec.findPackageOriginal(converged, row.product_id), row);
			});
			callback(converged);
		});
	});
}

function addonUpdateAPI(req, res) {
	var prefix = req.params.prefix;
	var number = req.params.number;
	var productId = req.params.product;
	var historyList = (req.fields && req.fields.history_list) ? req.fields.history_list.split(",") : undefined;
	var suppressNotify = false;
	var cmd = req.method;
	var tasks = new Array;
	if ( productId == hlrManager.roamingOff ) {
		tasks.push(function (callback) {
			hlrManager.set(hlrManager.roamingOff, prefix, number, suppressNotify, callback);
		});
	} else if ( productId == hlrManager.datavoicesms() ) {
		tasks.push(function (callback) {
			hlrManager.set(hlrManager.datavoicesms(), prefix, number, suppressNotify, callback);
		});
	} else if ( productId == hlrManager.voicesms() ) {
		tasks.push(function (callback) {
			hlrManager.set(hlrManager.voicesms(), prefix, number, suppressNotify, callback);
		});
	} else {
		tasks.push(function (callback) {
			packageManager.addonUpdate(prefix, number, cmd, productId, historyList, suppressNotify, function (err, result) {
				callback(err, result);
			});
		});
	}
	async.parallel(tasks, function (err, result) {
		if (err) {
			res.json({code: -1, status: err.status, error: err.message});
		} else if (!result) {
			res.json({code: -1, error: "Empty result"});
		} else {
			res.json({"code": 0, "result": result});
		}
	});
}
