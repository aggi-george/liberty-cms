var dateformat = require('dateformat');
var config = require('../../../config');
var common = require('../../../lib/common');
var db = require('../../../lib/db_handler');
var ec = require('../../../lib/elitecore');
var partnerManager = require('./../../core/manager/promotions/partnerManager');
var promotionsManager = require('./../../core/manager/promotions/promotionsManager');
var oauthManager = require('./../../core/manager/promotions/oauthManager');
var configManager = require('../../core/manager/config/configManager');
var sessionManager = require('../base/sessionManager');
var dbsManager = require('../../core/manager/partners/dbs');

var BaseError = require('../../core/errors/baseError');

var log = config.LOGSENABLED;
var api = [];

//exports

exports.init = init;
exports.getAPI = getAPI;

//functions

function init(baseApi, category, web) {
    var path = baseApi + category;

    // parse params
    web.post(path + '/*', sessionManager.parsePost);
    web.put(path + '/*', sessionManager.parsePut);
    // end-points
    web.get(path + '/pages/:key', loadPartnersPages);
    web.get(path + '/pages/cis/:key', loadCISPages);
    web.get(path + '/customer/:key', loadDetails);
    web.put(path + '/customer/code/:key', createCodeAndExternalCustomer);
    web.post(path + '/topup/subscribe/:key', addonSubscribeWithoutCharging)
}


function getAPI() {
    return api;
}

function loadDetails(req, res) {
    var uniqueId = req.query.uniqueId;
    var partner = req.query.partner;
    var code = req.query.code;

    if (!uniqueId && !code) {
        res.status(400).json({
            code: -1,
            status: "INVALID_PARAMS",
            error: "Please specify at least on identification param (supported: nric, code)"
        });
    }

    var handleResult = function (error, result) {
        if (error) {
            res.status(400).json({
                code: -1,
                status: result && result.status ? result.status : "ERROR",
                error: error.message
            });
        } else if (result) {
            return res.json({
                code: 0,
                valid: true,
                nric: result.nric,
                code: result.code,
                prefix: result.prefix,
                number: result.number,
                name: result.name,
                email: result.email,
                partner: result.source,
                usageLimit: result.usageLimit,
                usageCount: result.usageCount
            });
        } else {
            return res.json({
                code: 0,
                valid: false,
                status: "NOT_FOUND"
            });
        }
    };

    if (uniqueId) {
        partnerManager.loadCustomerByUniqueID(uniqueId, partner, handleResult);
    } else {
        partnerManager.loadCustomerInfoByCode(code, handleResult);
    }
}

function createCodeAndExternalCustomer(req, res) {
    var campaignId = req.fields.campaignId;
    var partner = req.fields.partner;
    var registration = req.fields.registration;
    var redirectUrl = req.fields.redirectUrl;
    if (log) common.log("PartnerApi", "customer create request, fields=" + JSON.stringify(req.fields) + ", " + (typeof registration));

    if ((typeof registration) == "string") {
        registration = common.safeParse(registration);
    }

    var sendError = function (err) {
        var publicMessage = undefined;
        if (err.status == "ERROR_UBER_NO_DRIVER_PROFILE") {
            publicMessage = "Uber Driver verification failed - please log in with your Uber Driver account.";
        } else if (err.status == "ERROR_DEBUG") {
            publicMessage = "Entered email is banned, please enter another email and try again.";
        } else if (err.status == "ERROR_CORPORATE_EMAIL_EMPTY_OR_INVALID") {
            publicMessage = "Please check the format of your email address and try again.";
        } else if (err.status == "ERROR_CORPORATE_UNSUPPORTED_EMAIL_EXTENSION") {
            publicMessage = "It seems like your company does not have a CIS program with us yet. " +
            "Please have your HR Manager contact us at cis@circles.life to get your team on board.";
        }

        common.error("PartnerApi", "failed to create a customer, error=" + err.message);
        return res.status(400).json({
            code: -1,
            status: err.status,
            error: err.message,
            message: publicMessage
        });
    }

    var sendSuccess = function (uniqueId, category, referralCode, customerId, exists) {
        if (log) common.log("PartnerApi", "send success response, uniqueId="
        + uniqueId + ", welcomeActivity=" + category.welcomeActivity + ", referralCode=" + referralCode);

        return res.json({
            code: 0,
            referralCode: referralCode,
            welcomeActivity: category.welcomeActivity,
            partner: category.partner,
            uniqueId: uniqueId,
            customerId: customerId,
            exists: exists
        });
    }

    if (!campaignId || !partner) {
        return sendError(new BaseError("Please specify campaignId and at least one identification param "
        + "(mandatory: campaignId, partner; optional: prefix, number, name, email, nric)", "ERROR_INVALID_PARAMS"));
    }

    // for debug purposes
    if (req.fields && req.fields.email == "debug-error@circles.asia") {
        return sendError(new BaseError("Debug error has been caused by " + req.fields.email, "ERROR_DEBUG"));
    }

    var createReminder = function (activity, date, onDay, categoryEndDate, uniqueId, customer, code, codeEndDate, callback) {
        if (!customer || !code) {
            return callback(new BaseError("Can not schedule reminder, invalid params", "ERROR_SCHEDULED_INVALID_PARAMS"));
        }

        if (!activity) {
            return callback();
        }

        var startDate;
        if (onDay > 0) {
            startDate = new Date(new Date().getTime() + onDay * 24 * 60 * 60 * 1000);
        } else if (date) {
            startDate = new Date(date);
        } else {
            return callback();
        }

        if (categoryEndDate && startDate.getTime() > categoryEndDate.getTime()) {
            return callback();
        }
        if (codeEndDate && startDate.getTime() > codeEndDate.getTime()) {
            return callback();
        }

        var codeExpireDate;
        if (categoryEndDate && codeEndDate) {
            codeExpireDate = (categoryEndDate.getTime() < codeEndDate.getTime()) ? categoryEndDate : codeEndDate;
        } else if (categoryEndDate) {
            codeExpireDate = categoryEndDate;
        } else if (codeEndDate) {
            codeExpireDate = codeEndDate;
        }
        if(startDate.getTime() < new Date().getTime() + 3600000){
            return callback(new BaseError("Can not schedule reminder, remind date is already passed", "ERROR_SCHEDULED_REMIND_DATE_PASSED"));
        }
        var codeExpireDateText = codeExpireDate ? dateformat(new Date(common.msOffsetSG(codeExpireDate)), "dS mmmm") : "'Never'";
        var event = {
            start: startDate.getTime(),
            prefix: customer.prefix,
            number: customer.number,
            email: customer.email,
            name: customer.name,
            partner: customer.partner,
            referral_code: code,
            referralCode: code,
            code_expiry_date: codeExpireDateText,
            codeExpiryDate: codeExpireDateText,
            uniqueId: uniqueId,
            action: "notification",
            activity: activity,
            allDay: false,
            trigger: "[CMS][" + partner + "]",
            title: "Reminder for code " + code + " " + partner
        };

        db.scheduler.insert(event);
        callback(undefined, event);
    }

    var generateExternalCustomer = (uniqueId, category) => {
        partnerManager.addCustomers([{
            uniqueId: uniqueId,
            fields: req.fields,
            category: category
        }], partner, function (err, customersInfo) {
            if (err) {
                return sendError(err);
            }

            if (!customersInfo || !customersInfo[0]) {
                return sendError(new BaseError("Customer has not been created",
                    "ERROR_FAILED_CREATE_CUSTOMER"))
            }

            var customerInfo = customersInfo[0];
            if (log) common.log("PartnerApi", "create customer, customerInfo=" + JSON.stringify(customerInfo));

            createReminder(category.reminderActivity1, category.reminderDate1, category.reminderOnDay1, category.endDate,
                uniqueId, customerInfo.customer, customerInfo.code.code,  customerInfo.code.endDate, (err, event) => {
                    if (err) {
                        // ignore error, activity and date may not be specified
                    }

                    if (event) {
                        if (log) common.log("PartnerApi", "reminder #1, activity="
                        + event.activity + ", date=" + event.start);
                    }

                    createReminder(category.reminderActivity2, category.reminderDate2, category.reminderOnDay2, category.endDate,
                        uniqueId, customerInfo.customer, customerInfo.code.code, customerInfo.code.endDate, (err, event) => {
                            if (err) {
                                // ignore error, activity and date may not be specified
                            }

                            if (event) {
                                if (log) common.log("PartnerApi", "reminder #2, activity="
                                + event.activity + ", date=" + event.start);
                            }

                            sendSuccess(uniqueId, category,
                                customerInfo.code.code,
                                customerInfo.customer.id);
                        });
                });
        });
    }

    var loadExistingCustomer = (uniqueId, category) => {
        partnerManager.loadCustomerByUniqueID(uniqueId, category.partner, function (err, customer) {
            if (err) {
                return sendError(err);
            }
            if (customer) {
                if (log) common.log("PartnerApi", "found existing customer, customer=" + JSON.stringify(customer));
                return sendSuccess(uniqueId, category, customer.code, customer.id, true);
            }

            if (log) common.log("PartnerApi", "no customer found, need to create new, uniqueId=" + uniqueId)
            generateExternalCustomer(uniqueId, category);
        });
    };

    var loadCampaign = () => {
        promotionsManager.loadCategories(campaignId, (err, category) => {
            if (err) {
                return sendError(err);
            }
            if (!category) {
                return sendError(new BaseError("No category found",
                    "ERROR_CATEGORY_NOT_FOUND"));
            }
            if (partner !== category.partner) {
                return sendError(new BaseError("Partner in request and partner in category " +
                "is different", "ERROR_INCONSISTENT_PARAMS"))
            }

            if (log) common.log("PartnerApi", "loaded category, id=" + campaignId
            + ", uniqueField=" + category.uniqueField)

            if (category.emailExtension && category.emailExtension.replace(/ /g, "")) {
                if (!req.fields.corporateEmail || !req.fields.corporateEmail.split('@')[1]) {
                    return sendError(new BaseError("Corporate email is empty or invalid - " + req.fields.corporateEmail,
                        "ERROR_CORPORATE_EMAIL_EMPTY_OR_INVALID", {invalidEmail: req.fields.corporateEmail}))
                }

                var emailExtension = req.fields.corporateEmail.split('@')[1].replace(/ /g, "").replace(/@/g, "");
                var supportedExtensions = category.emailExtension.replace(/ /g, "").replace(/@/g, "").split(',');

                var foundSupported;
                if (supportedExtensions) supportedExtensions.forEach((supportedExtension) => {
                    if (supportedExtension == emailExtension) {
                        foundSupported = true;
                    }
                })

                if (!foundSupported) {
                    return sendError(new BaseError("Corporate email extension of " + req.fields.corporateEmail
                    + " is not supported (supported: " + category.emailExtension + ")",
                        "ERROR_CORPORATE_UNSUPPORTED_EMAIL_EXTENSION", {invalidEmail: req.fields.corporateEmail,
                            invalidExtension: emailExtension}));
                }
            }

            if (oauthManager.getSupported().indexOf(category.uniqueField) >= 0) {
                if (log) common.log("PartnerApi", "check customer using OAuth, redirectUrl=" + redirectUrl
                + ", registration=" + JSON.stringify(registration))

                oauthManager.verifyOAuth(category.uniqueField, registration, redirectUrl, (err, result) => {
                    if (err) {
                        return sendError(err);
                    }

                    return loadExistingCustomer(result.uniqueId, category);
                });
            } else {
                var uniqueId = req.fields[category.uniqueField];
                if (!uniqueId) {
                    return sendError(new BaseError("Unique " + category.uniqueField
                    + " filed is empty", "ERROR_UNIQUE_FIELD_EMPTY"))
                }

                return loadExistingCustomer(uniqueId, category);
            }
        });
    }

    loadCampaign();
}

function loadPartnersPages(req, res) {
    promotionsManager.loadWebPages((err, result) => {
        if (err) {
            res.status(400).json({
                code: -1,
                status: err.status,
                error: err.message
            });
        }

        res.json(result);
    });
}

function loadCISPages(req, res) {
    var email = req.query.email;
    if (!email || !email.split('@')[1]) {
        return res.status(400).json({
            code: -1,
            status: "ERROR_EMAIL_EMPTY_OR_INVALID",
            error: "Email is empty or invalid - " + email,
            message: "Entered email is invalid, please enter another email and try again."
        });
    }

    var emailExtension = email.split('@')[1].replace(/ /g, "").replace(/@/g, "");
    promotionsManager.loadWebPages((err, result) => {
        if (err) {
            res.status(400).json({
                code: -1,
                status: err.status,
                error: err.message
            });
        }

        var validCampaign = [];
        if (result) result.forEach((promo) => {
            if (promo.metadata && promo.metadata.supportedEmailExtensions) {
                promo.metadata.supportedEmailExtensions.some((extension) => {
                    if (extension == emailExtension) {
                        validCampaign.push(promo);
                        return true;
                    }
                });
            }
        });

        res.json(validCampaign);
    });
}

function addonSubscribeWithoutCharging(req, res) {
    const { addonId, serviceInstanceNumber } = req.fields;

    return dbsManager.addonSubscribeWithoutCharging(addonId, serviceInstanceNumber)
        .then(result => {
            return res.json({
                code: 0,
                status: 'Success'
            });
        })
        .catch(err => {
            common.log('PartnerApi.addonSubscribeWithoutCharging Error: ', err);
            return res.status(400).json({
                code: -1,
                status: err.status ? err.status : 'Failed',
                error: err.message
            });
        })
}
