var config = require('../../../config');
var common = require('../../../lib/common');
var db = require('../../../lib/db_handler');
var ec = require('../../../lib/elitecore');

var portInManager = require('./../../core/manager/porting/portInManager');
var elitecoreUserService = require('../../../lib/manager/ec/elitecoreUserService')

var API_DOC = [];
var FIELDS_LIMIT = 20;
var LOG = config.LOGSENABLED;

module.exports = {

    init: function (baseApi, category, web) {
        var path = baseApi + category;

        // parse params
        web.put(path + '/*', parsePut);
        web.post(path + '/*', parsePost);

        // packages - hierarchy all
        var path = path + '/';
        web.put(path + 'request/cancel/:key', cancelRequest);
        web.get(path + 'request/recent/:prefix/:number/:key', loadRecentRequest);
        web.get(path + 'request/active/:prefix/:number/:key', loadActive);
        web.get(path + 'request/status/:serviceInstanceNumber/:key', loadRequestStatus);
    },

    getAPI: function () {
        return API_DOC;
    }
}

function parsePut(req, res, next) {
    common.formParse(req, res, FIELDS_LIMIT, function (fields) {
        req.fields = fields;
        next();
    });
}

function parsePost(req, res, next) {
    common.formParse(req, res, FIELDS_LIMIT, function (fields) {
        req.fields = fields;
        next();
    });
}

function loadActive(req, res) {
    var key = req.params.key;
    var prefix = req.params.prefix;
    var number = req.params.number;

    portInManager.loadActiveRequest(prefix, number, key, function (error, request) {
        if (error) {
            return res.status(400).json({
                code: -1,
                status: error.id,
                error: error.message
            });
        } else {
            return res.json({
                code: 0,
                result: request
            });
        }
    });
}

function loadRecentRequest(req, res) {
    var key = req.params.key;
    var prefix = req.params.prefix;
    var number = req.params.number;

    portInManager.loadRecentRequest(prefix, number, function (error, result) {
        if (error) {
            return res.status(400).json({
                code: -1,
                status: error.id,
                error: error.message
            });
        } else {
            return res.json({
                code: 0,
                result: result
            });
        }
    });
}

function loadRequestStatus(req, res) {
    var key = req.params.key;
    var serviceInstanceNumber = req.params.serviceInstanceNumber;

    if (!serviceInstanceNumber) {
        return res.status(400).json({
            code: -1,
            status: "ERROR_INVALID_PARAMS",
            error: "Invalid service instance number"
        });
    }

    elitecoreUserService.loadUserInfoByServiceInstanceNumber(serviceInstanceNumber, (err, cache) => {
        if (err) {
            return res.status(400).json({
                code: -1,
                status: "ERROR_BSS",
                error: err.message
            });
        }

        if (!cache) {
            return res.status(400).json({
                code: -1,
                status: "ERROR_CUSTOMER_NOT_FOUND",
                error: "Customer not found"
            });
        }

        portInManager.loadRequest(serviceInstanceNumber, {status: "ALL"}, (err, result) => {
            if (err) {
                return res.status(400).json({
                    code: -1,
                    status: err.id,
                    error: err.message
                });
            }

            res.json({
                code: 0,
                result: (result && result.data && result.data[0]) ? result.data[0] : undefined
            });
        });
    });
}

function cancelRequest(req, res) {
    var key = req.params.key;
    var prefix = req.fields.prefix;
    var number = req.fields.number;
    var portingNumber = req.fields.porting_number;
    var serviceInstanceNumber = req.fields.serviceInstanceNumber;
    var reason = req.fields.reason;
    var platform = req.fields.platform;

    var handleResult = (err, result) => {
        if (err) {
            return res.status(400).json({
                code: -1,
                status: err.status,
                error: err.message
            });
        }

        res.json({
            code: 0,
            result: result
        });
    };

    if (prefix && number && portingNumber) {
        if (!reason) {
            reason = "Canceled by customer via CirclesCare";
        }

        var initiator = "[MOBILE][" + number + "]";
        portInManager.cancelRequest(prefix, number, prefix, portingNumber,
            handleResult, key, reason, initiator, true);
    } else if (serviceInstanceNumber) {
        if (!reason) {
            reason = "Unknown cancellation reason";
        }
        if (!platform) {
            platform = "UNKNOWN";
        }

        var initiator = "[" + platform + "][" + serviceInstanceNumber + "]";
        portInManager.cancelRequestByServiceInstanceNumber(serviceInstanceNumber,
            {key: key, reason: reason, initiator: initiator, onlyWaiting: true}, handleResult);
    } else {
        handleResult(new Error("Invalid Params"));
    }
}
