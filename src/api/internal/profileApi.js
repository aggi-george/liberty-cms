var config = require('../../../config');
var common = require('../../../lib/common');
var db = require('../../../lib/db_handler');
var ec = require('../../../lib/elitecore');
var boostManager = require('./../../core/manager/boost/boostManager');
var profileManager = require('./../../core/manager/profile/profileManager');

var API_DOC = [];
var FIELDS_LIMIT = 20;
var LOG = config.LOGSENABLED;

module.exports = {

    init: function (baseApi, category, web) {
        var path = baseApi + category;

        //Parse params
        web.put(path + '/*', parsePut);
        web.post(path + '/*', parsePost);

        // packages - hierarchy all
        var path = path + '/';

        web.get(path + 'settings/get/:appType/:prefix/:number/:key', getSettings);
        web.put(path + 'settings/set/:key', setSettings);

        web.get(path + 'get/:prefix/:number/:key', getProfile);
        web.put(path + 'set/:key', setProfile);
    },

    getAPI: function () {
        return api;
    }
}

function parsePut(req, res, next) {
    common.formParse(req, res, FIELDS_LIMIT, function (fields) {
        req.fields = fields;
        next();
    });
}

function parsePost(req, res, next) {
    common.formParse(req, res, FIELDS_LIMIT, function (fields) {
        req.fields = fields;
        next();
    });
}

function getProfile(req, res) {
    var key = req.params.key;
    var prefix = req.params.prefix;
    var number = req.params.number;

    profileManager.loadProfileDetails(prefix, number, key,
        function (error, details) {
            if (error) {
                return res.status(400).json({
                    code: -1,
                    status: error.id,
                    error: error.message
                });
            } else {
                return res.json({
                    code: 0,
                    details: details
                });
            }
        });
}

function setProfile(req, res) {
    var key = req.params.key;
    var prefix = req.fields.prefix;
    var number = req.fields.number;
    var profileFirstName = req.fields.firstname;
    var profileLastName = req.fields.lastname;
    var profileNickname = req.fields.nickname;
    var profilePicture = req.fields.app_picture;

    profileManager.updateProfileDetails(prefix, number, profileFirstName, profileLastName,
        profileNickname, profilePicture, key, function (error, result) {
            if (error) {
                return res.status(400).json({
                    code: -1,
                    status: error.id,
                    error: error.message
                });
            } else {
                return res.json({
                    code: 0,
                    result: result
                });
            }
        });
}

function getSettings(req, res) {
    var key = req.params.key;
    var appType = req.params.appType;
    var prefix = req.params.prefix;
    var number = req.params.number;

    profileManager.loadSetting(prefix, number, appType,
        key, function (error, result) {
            if (error) {
                return res.status(400).json({
                    code: -1,
                    status: error.id,
                    error: error.message
                });
            } else {
                return res.json({
                    code: 0,
                    settings: result.settings
                });
            }
        });
}

function setSettings(req, res) {
    var key = req.params.key;
    var prefix = req.fields.prefix;
    var number = req.fields.number;
    var appType = req.fields.app_type;
    var optionId = req.fields.option_id;
    var value = req.fields.value;
    var type = req.fields.type;

    profileManager.saveSetting(prefix, number, appType, optionId, type, value,
        key, function (error, result) {
            if (error) {
                return res.status(400).json({
                    code: -1,
                    status: error.id,
                    error: error.message
                });
            } else {
                if (result.addAutoBoost) {
                    boostManager.addAutoBoost(prefix, number, true, key,
                            function (err, result) {
                        // do nothing
                    });
                }
                return res.json({
                    code: 0,
                    result: result
                });
            }
        });
}
