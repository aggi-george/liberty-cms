var request = require('request');

var config = require('../../../config');
var common = require('../../../lib/common');
var db = require('../../../lib/db_handler');
var ec = require('../../../lib/elitecore');
var verificationManager = require('./../../core/manager/promotions/verificationManager');
var promotionsManager = require('./../../core/manager/promotions/promotionsManager');
var partnerManager = require('./../../core/manager/promotions/partnerManager');
var referralManager = require('./../../core/manager/promotions/referralManager');
const terminationManager = require('../../core/manager/account/terminationManager');
const constants = require('./../../core/constants');

var postFieldsLimit = 20;

var BaseError = require('../../core/errors/baseError');

var LOG = config.LOGSENABLED;

var api = new Array;

//exports

exports.init = init;
exports.getAPI = getAPI;

//functions

function init(baseApi, category, web) {
    var path = baseApi + category;

    //Parse params
    web.put(path + '/*', parsePut);

    // packages - hierarchy all
    var usagePath = path + '/use';
    web.put(usagePath + '/promocode/:code/:key', useCode);
    web.put(usagePath + '/code/:code/:key', useCode);
    web.get(`${path}/eligibility/:serviceInstanceNumber/:key`, getWinbackForCustomer);
}

function parsePut(req, res, next) {
    common.formParse(req, res, postFieldsLimit, function (fields) {
        req.fields = fields;
        next();
    });
}

function getAPI() {
    return api;
}

function useCode(req, res) {
    var platform = req.fields.platform;
    var prefix = req.fields.prefix;
    var number = req.fields.number;
    var name = req.fields.name;
    var orderReferenceNumber = req.fields.order_reference_number;
    var verificationValue = req.fields.verificationValue;
    var code = req.params.code;
    var key = req.params.key;

    if (!platform) {
        platform = "ECOMM";
    }

    var handleResponse = (err, result, type) => {

        var obj = {
            status: err ? (err.status ? err.status : "ERROR") : "OK",
            error: err ? err.message : undefined,
            type: type,
            code: code,
            params: {
                code: code,
                verificationValue: verificationValue,
                orderReferenceNumber: orderReferenceNumber,
                prefix: prefix,
                number: number,
                name: name
            },
            result: {},
            ts: new Date().getTime()
        }

        db.notifications_promo_codes.insert(obj, function (logErr) {
            if (logErr) {
                common.error("PromotionApi", "useCode: failed to save logs, error=" + logErr.message);
                // do nothing
            }

            if (err) {
                return res.status(400).json({
                    code: -1,
                    status: err.status ? err.status : "ERROR",
                    error: err.message
                });
            }

            res.json({
                code: 0,
                type: type,
                result: result
            })
        });
    }
     verificationManager.verifyCode(code, verificationValue, false, function (err, result) {
         if (err) {
             if (err.status != "CODE_NOT_FOUND") {
                 return handleResponse(err);
             }

             var promoCodeVerificationError = err.message;
             return referralManager.loadUserAccount(code, function (err, result) {
                 if (err) {
                     return handleResponse(err);
                 }
                 if (!result || !result.valid) {
                     return handleResponse(new BaseError("Neither personal referral nor " +
                     "internal sign-up code has been found, " + promoCodeVerificationError, "CODE_NOT_FOUND"));
                 }
                 handleResponse(undefined, result, "REFERRAL_CODE_USAGE");

                 // send usage notification if any supported

                 ec.getCustomerDetailsAccount(result.accountNumber, false, function (err, cache) {
                     if (err) {
                         return;
                     }
                     if (!cache) {
                         return;
                     }

                     sendNotificationUsageNotification("bonus_referral_used", cache.prefix, cache.number,
                         cache.first_name, cache.email, code, name, number, -1, -1, key, () => {
                             // do nothing
                         });
                 });
             });
         }
         promotionsManager.useCode(platform, prefix, number, code, orderReferenceNumber, key, function (err, result) {
             if (err) {
                 return handleResponse(err);
             }
             handleResponse(undefined, result, "INTERNAL_CODE_USAGE");

             // send usage notification if any supported

             partnerManager.loadCustomerInfoByCode(code, function (err, codeOwner) {
                 if (err || !codeOwner) {
                     return;
                 }

                 promotionsManager.loadCategories(codeOwner.codeCategoryId, function (err, category) {
                     if (err || !category) {
                         return;
                     }
                     if (!category.usageActivity) {
                         return;
                     }

                     sendNotificationUsageNotification(category.usageActivity, codeOwner.prefix, codeOwner.number,
                         codeOwner.name, codeOwner.email, code, name, number, codeOwner.usageLimit, codeOwner.usageCount, key, () => {
                             // do nothing
                         });
                 });
             });

         });
     });
}

function sendNotificationUsageNotification(activity, prefix, number, name, email, referralCode,
                                           joinedUserName, joinedUserNumber, usageLimit,
                                           usageCount, notificationKey, callback) {

    var url = "http://" + config.MYSELF + ":" + config.WEBPORT +
        "/api/1/webhook/notifications/internal/" + notificationKey;

    request({
        uri: url,
        method: 'POST',
        timeout: 20000,
        json: {
            activity: activity,
            prefix: prefix,
            number: number,
            name: name,
            email: email,
            variables: {
                referral_code: referralCode,
                joined_user_name: joinedUserName,
                joined_user_number: joinedUserNumber,
                usage_limit: usageLimit,
                usage_count: usageCount,
                usage_left: (usageLimit - usageCount)
            }
        }
    }, function (err, response, body) {
        if (err) {
            return callback(err);
        }
        if (callback) callback();
    });
}

function getWinbackForCustomer(req, res) {
  const {serviceInstanceNumber} = req.params;
  const numberOfmonths = 2;

  return terminationManager.getTerminationDate(serviceInstanceNumber, numberOfmonths)
    .then(result => {
        const apiResponse = getStaticResponse();

        if (result.termationDates) {
            const { eligibility, termationDates } = result;
            apiResponse.eligibility = eligibility;
            apiResponse.termination_date = termationDates;
            return res.json({
                code: 0,
                result: apiResponse
            })
        }
        apiResponse.eligibility = result;
        return res.json({
            code: 0,
            result: apiResponse
        })
    })
    .catch(error => {
        return res.json({
            code: -1,
            result: 'failed: Internal Api Error'
        });
    })
}

function getStaticResponse() {
    return {
        winback: [
            {
                id: 1,
                enable: true,
                type: 'FREE_2020',
                value: constants.TERMINATION_WINBACK_PROMO_TAG,
                description: '20GB free for 1 month',
                disclaimer: null,
            },
        ],
        reasons: [
            {
                id: 1,
                winback_id: 1,
                title: 'I encountered issues with the network',
                sub_title: 'xyz',
                enable: true,
            },
            {
                id: 2,
                winback_id: 1,
                title: 'I received bad customer service',
                sub_title: 'xyz',
                enable: true,
            },
            {
                id: 3,
                winback_id: 1,
                title: 'I had billing issues',
                sub_title: 'xyz',
                enable: true,
            },
            {
                id: 4,
                winback_id: 1,
                title: 'My port-in failed',
                sub_title: 'xyz',
                enable: true,
            },
            {
                id: 5,
                winback_id: 1,
                title: 'I was offered a better deal with another telco',
                sub_title: 'xyz',
                enable: true,
            },
            {
                id: 6,
                winback_id: 1,
                title: "I'm moving overseas",
                sub_title: 'xyz',
                enable: true,
            },
            {
                id: 7,
                winback_id: 1,
                title: "I have another primary number and don't need this one",
                sub_title: 'xyz',
                enable: true,
            },
            {
                id: 8,
                winback_id: 1,
                title: 'Other',
                sub_title: 'xyz',
                enable: true,
            },
        ],
    };
}

