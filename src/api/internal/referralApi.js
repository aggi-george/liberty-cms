var config = require('../../../config');
var common = require('../../../lib/common');
var db = require('../../../lib/db_handler');
var ec = require('../../../lib/elitecore');
var bonusManager = require('./../../core/manager/bonus/bonusManager');
var referralManager = require('./../../core/manager/promotions/referralManager');
var promotionsManager = require('./../../core/manager/promotions/promotionsManager');
var verificationManager = require('./../../core/manager/promotions/verificationManager');
var configManager = require('../../core/manager/config/configManager');

var BaseError = require('../../core/errors/baseError');

var API_DOC = [];
var FIELDS_LIMIT = 20;
var LOG = config.LOGSENABLED;

const monthNames = ["January", "February", "March", "April", "May", "June",
    "July", "August", "September", "October", "November", "December"];

module.exports = {

    init: function (baseApi, category, web) {
        var path = baseApi + category;

        //Parse params
        web.put(path + '/*', parsePut);
        web.post(path + '/*', parsePost);

        // packages - hierarchy all
        var path = path + '/';
        web.put(path + 'code/add/:key', addCode);
        web.get(path + 'code/valid/:code/:key', validCode);
        web.put(path + 'code/valid/dialog/:key', validDialogCode);
        web.get(path + 'code/get/:prefix/:number/:key', loadCode);
        web.put(path + 'code/generate/:key', generateCode);

        web.get(path + 'tag/valid/:tag/:key', validTag);
    },

    getAPI: function () {
        return api;
    }
}

function parsePut(req, res, next) {
    common.formParse(req, res, FIELDS_LIMIT, function (fields) {
        req.fields = fields;
        next();
    });
}

function parsePost(req, res, next) {
    common.formParse(req, res, FIELDS_LIMIT, function (fields) {
        req.fields = fields;
        next();
    });
}

function validTag(req, res) {
    var key = req.params.key;
    var tag = req.params.tag;

    promotionsManager.loadPromoCampaignOffer(tag, (err, result) => {
        if (err) {
            return res.status(400).json({
                code: -1,
                status: err.status ? err.status : "ERROR",
                error: err.message
            });
        }
        if (!result) {
            return res.status(400).json({
                code: -1,
                status: "ERROR_CATEGORY_CODE_NOT_FOUND",
                error: "Category with tag " + tag + " has not code"
            });
        }

        var formattedResponse = createCodeOutput(result);
        formattedResponse.code = 0;
        res.json(formattedResponse);
    });
}

function validCode(req, res) {
    var key = req.params.key;
    var code = req.params.code;

    // check for personal codes

    var checkPersonalCode = function () {
        referralManager.loadUserAccount(code, function (err, result) {
            if (err) {
                return res.status(400).json({
                    code: -1,
                    status: err.status,
                    error: err.message
                });
            } else {
                if (result.valid) {
                    var dateUsageSince = new Date(new Date().getTime() - 7 * 24 * 60 * 60 * 1000);
                    db.notifications_promo_codes.count({
                        status: "OK",
                        type: "REFERRAL_CODE_USAGE",
                        code: code,
                        ts: {$gt: dateUsageSince.getTime()}
                    }, (err, count) => {
                        if (err) {
                            return res.status(400).json({
                                code: -1,
                                status: err.status,
                                error: err.message
                            });
                        }

                        var secondVerification = {required: false};
                        if (code == "OKOKOK") {
                            secondVerification.required = true;
                            secondVerification.dialog = verificationManager.getVerificationDialogTypes("REFERRER_NUMBER");
                            if (!secondVerification.dialog) {
                                secondVerification.dialog = {title: "", message: "", field: {type: "text", limit: 0}}
                            }
                        }

                        return res.json({
                            code: 0,
                            valid: result.valid,
                            reason: result.reason,
                            publicName: "Referral Code",
                            accountNumber: result.accountNumber,
                            waiver: undefined,
                            discount: result.discountCents,
                            usedSince: dateUsageSince.toISOString(),
                            usedCount: count,
                            verificationDialog: secondVerification
                        });
                    });
                } else {
                    return res.json({
                        code: 0,
                        valid: result.valid,
                        reason: result.reason
                    });
                }
            }
        });
    }


    // check for a generic code

    var checkGenericPartnerBonuses = function () {
        promotionsManager.loadPromoCode(code, function (error, item) {
            if (error && error.status === 'CODE_NOT_FOUND') {
                return checkPersonalCode();
            }

            if (error) {
                return res.json({
                    code: 0,
                    valid: false,
                    reason: error.status,
                    expiration: item ? item.expiration : undefined
                });
            } else if (!item) {
                return res.json({
                    code: 0,
                    valid: false,
                    reason: "NOT_FOUND"
                });
            } else {
                var formattedResponse = createCodeOutput(item);
                formattedResponse.code = 0;
                formattedResponse.valid = true;
                res.json(formattedResponse);
            }
        }, true);
    }

    // start verifying referral codes

    checkGenericPartnerBonuses();
}

function createCodeOutput(item) {
    var partnerBonus = bonusManager.getCodeBonusProduct(item.bonus_data, item.bonus_data_type, item.months_count, item.product_id);

    var freeAddonId = item.freeAddon1;
    var freeAddonProduct = freeAddonId ? common.deepCopy(ec.findPackage(ec.packages(), freeAddonId)) : undefined;
    if (freeAddonProduct) {
        var freePlusTwentyProductId = ec.findPackageName(ec.packages(), "Free Plus 2020").id;
        //var monthFullName = common.longMonths[new Date().getMonth()];
        //var prodExtraText = (freePlusTwentyProductId == freeAddonId) ? ' (Free for ' + monthFullName + ')' : ' (Free for a limited time)';
        freeAddonProduct.monthsDuration = item.freeAddonMonths1;
        if (item.freeAddonMonths1 >= 0) {
            var extraMonths = item.extraBillDay > 0 && item.extraBillDay < common.getLocalTime().getDate() ? 1 : 0;
            var firstMonth = (common.getLocalTime().getMonth() + extraMonths) % 12;
            var lastMonth = (firstMonth + item.freeAddonMonths1 - 1) % 12;

            if (item.freeAddonMonths1 == 1) {
                freeAddonProduct.monthsDurationText = monthNames[firstMonth];
            } else {
                freeAddonProduct.monthsDurationText = monthNames[firstMonth] + "-" + monthNames[lastMonth];
            }
            freeAddonProduct.title = freeAddonProduct.title + " (Free for a limited time)" ;
        } else if (item.freeAddonMonths1 == -1) {
            freeAddonProduct.title = freeAddonProduct.title + ' (Free forever)';
            freeAddonProduct.monthsDurationText = "Forever";
        }
    }

    var paidAddonProduct = freeAddonId ? ec.findPackageWithFreeId(ec.packages(), freeAddonId) : undefined;
    var skuItem = item.skuKey1 ? {sku: item.skuKey1, quantity: item.skuQuantity1} : undefined;
    if (partnerBonus && partnerBonus.publicTitle) {
        partnerBonus.title = partnerBonus.publicTitle;
    }

    var secondVerification = {required: false};
    if (item.secondVerificationType) {
        secondVerification.required = true;
        secondVerification.dialog = verificationManager.getVerificationDialogTypes(item.secondVerificationType);
        if (!secondVerification.dialog) {
            secondVerification.dialog = {title: "", message: "", field: {type: "text", limit: 0}}
        }
    }

    return {
        waiver: item.waiverCents ? item.waiverCents : undefined,
        publicName: item.publicName ? item.publicName : undefined,
        discount: item.regDiscountCents ? item.regDiscountCents : undefined,
        deviceDiscount: item.deviceDiscountCents ? item.deviceDiscountCents : undefined,
        partner: item.partner ? item.partner : undefined,
        verificationDialog: secondVerification,
        extraDeliveryItems: skuItem ? [skuItem] : undefined,
        bonusAddon: partnerBonus ? [partnerBonus] : undefined,
        customMNumber: item.customMNumber ? item.customMNumber : undefined,
        bankName: item.bankName ? item.bankName : undefined,
        freeAddon: freeAddonProduct && paidAddonProduct ? [{
            free: freeAddonProduct,
            paid: paidAddonProduct
        }] : undefined,
        usedCount: (item.count_code + item.count_code_level_2 + item.count_code_level_3),
        level: item.level
    }
}

function loadCode(req, res) {
    var key = req.params.key;
    var prefix = req.params.prefix;
    var number = req.params.number;

    referralManager.loadUserReferralCode(prefix, number, function (err, result) {
        if (err) {
            return res.status(400).json({
                code: -1,
                status: err.status,
                error: err.message
            });
        } else {
            return res.json({
                code: 0,
                data: result
            });
        }
    });
}

function addCode(req, res) {
    var key = req.params.key;
    var prefix = req.fields.prefix;
    var number = req.fields.number;
    var code = req.fields.code;

    referralManager.createUserReferralCode(prefix, number, code, function (err, result) {
        if (err) {
            return res.status(400).json({
                code: -1,
                status: err.status,
                error: err.message
            });
        } else {
            return res.json({
                code: 0,
                data: result
            });
        }
    });
}

function validDialogCode(req, res) {
    var key = req.params.key;
    var code = req.fields.code;
    var verificationValue = req.fields.verificationValue;

    verificationManager.verifyCode(code, verificationValue, true, function (err, result) {
        var obj = {
            status: err ? (err.status ? err.status : "ERROR") : "OK",
            error: err ? err.message : undefined,
            type: "CODE_VERIFICATION",
            code: code,
            params: {
                code: code,
                verificationValue: verificationValue
            },
            result: {},
            ts: new Date().getTime()
        }

        db.notifications_promo_codes.insert(obj, function (logErr) {
            if (logErr) {
                common.error("PromotionApi", "useCode: failed to save logs, error=" + logErr.message);
                // do nothing
            }

            if (err) {
                return res.status(400).json({
                    code: -1,
                    status: err.status,
                    error: err.message
                });
            } else {
                return res.json({
                    code: 0,
                    valid: result ? result.valid : false,
                    message: result ? result.message : undefined
                });
            }
        });
    });
}

function generateCode(req, res) {
    var tag = req.fields.tag;

    var handleResponse = (err, result) => {

        var obj = {
            status: err ? (err.status ? err.status : "ERROR") : "OK",
            error: err ? err.message : undefined,
            type: "GENERATE_CODE",
            code: result ? result.code : undefined,
            params: {
                tag: tag
            },
            result: result,
            ts: new Date().getTime()
        }

        db.notifications_promo_codes.insert(obj, function (logErr) {
            if (logErr) {
                common.error("PromotionApi", "generateCode: failed to save logs, error=" + logErr.message);
                // do nothing
            }

            if (err) {
                return res.status(400).json({
                    code: -1,
                    status: err.status ? err.status : "ERROR",
                    error: err.message
                });
            }
            res.json({
                code: 0,
                promoCode: result ? result.code : undefined
            })
        });
    }

    if (!tag) {
        return handleResponse(new BaseError("Category tag is empty", "ERROR_CATEGORY_NOT_FOUND"));
    }

    promotionsManager.loadCategories(tag, (err, category) => {
        if (err) {
            return handleResponse(err);
        }
        if (!category) {
            return handleResponse(new BaseError("Category with tag " + tag + " is not found", "ERROR_CATEGORY_NOT_FOUND"));
        }
        if (!category.codePrefix || !category.codePatternL1) {
            return handleResponse(new BaseError("Category with tag " + tag + " is not configured properly, " +
            "code prefix or pattern is missing", "ERROR_CATEGORY_INVALID_CONFIG"));
        }

        var patterns = [];
        if (category.codePatternL1) patterns.push(category.codePatternL1);
        if (category.codePatternL2) patterns.push(category.codePatternL2);
        if (category.codePatternL3) patterns.push(category.codePatternL3);

        promotionsManager.generatePromoFromPattern(category.id, category.codeValidDays, category.codePrefix, patterns, 5,
            (err, codeResult) => {
                if (err) {
                    return handleResponse(err);
                }
                if (!category || !codeResult.data || !codeResult.data[0]) {
                    return handleResponse(new BaseError("Failed to generate code, empty response", "ERROR_INVALID_RESPONSE"));
                }

                handleResponse(undefined, codeResult.data[0]);
            }, 0, true);
    });
}

