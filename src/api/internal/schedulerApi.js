var config = require('../../../config');
var common = require('../../../lib/common');
var schedulerManager = require('../../core/manager/scheduler/schedulerManager');
var webUtils = require('../web/web');

var BaseError = require('../../core/errors/baseError.js');



module.exports = {

    init: function (baseApi, category, web) {
        var path = baseApi + category;

        //Parse params
        web.put(path + '/*/:key', parsePut);

        //sim lost endpoint
        var path = path + '/';
        web.put(path + 'addSchedule/:start/:action/:activity/:serviceInstanceNumber/:key', addEventToScheduler);
        web.put(path + 'updateSchedule/:id/:serviceInstanceNumber/:newStart/:key', updateEventInScheduler);
        web.get(path + 'getSchedules/:id/:key', getEventsFromScheduler);
        web.delete(path + 'deleteSchedule/:id/:key', deleteEventFromScheduler);
    },

    getAPI: function () {
        return api;
    }
}

function addEventToScheduler(req, res){
    schedulerManager.createSchedule(req.params, function(err, response){
        if(err || (!response)){
            const message = err && err.message ? err.message : 'Insert Failed';
            common.error("schedulerAPI", message);
            res.json({code: -1, message: message, _id: null});
        }else {
            res.json({ code: 0, result: "Insert OK", _id: response});
        }
    });
}

function updateEventInScheduler(req, res){
    schedulerManager.updateSchedule(req.params, function(err, result){
        if(err){
            common.log("schedulerAPI", err.status);
            res.json({code: -1, message: err.status});
        }else{
            res.json({ code: 0, result: "OK"});
        }
    })
}

function deleteEventFromScheduler(req, res){
    schedulerManager.deleteSchedule(req.params, function(err, result){
        if(err){
            common.log("schedulerAPI", err.status);
            res.json({code: -1, message: err.status});
        }else{
            res.json({ code: 0, result: "OK"});
        }
    });
}

function getEventsFromScheduler(req, res){
    schedulerManager.getSchedules(req.params, function(err, result){
        if(err){
            return res.json({code: -1, message: err.status});
        }else{
            res.json({ code: 0, result: result});
        }
    })
}

function parsePut(req, res, next){
    webUtils.checkApiKeyForTeams(req.params.key, [2], function(err, allowed){
        if(err || !allowed){
            res.status(401).json({
                "code": -1,
                "error": "API key verification failed"
            });
        }else{
            next();
        }
    })
}

