var config = require('../../../config');
var common = require('../../../lib/common');
var db = require('../../../lib/db_handler');
var util = require('util')
var exec = require('child_process').exec;

var LOG = config.LOGSENABLED;

var correctKey = [ 'abcdef', 'bcdefg' ];
var stdoutput = '';

module.exports = {

    init: function (baseApi, category, web) {
        var path = baseApi + category;
        var path = path + '/';
        web.post(path + 'cms_status/initial_auth/:key', initial_auth);
        web.post(path + 'cms_status/:key/:status/:server', cms_status);
        web.post(path + 'bypass/:key/:status/:server', bypass);
        web.post(path + 'bypass_test/:key/:status/:server', bypass_test);
        web.post(path + 'bss_traffic/:key/:status/:server', bss_traffic);
        web.post(path + 'billing/:action', billing_action);
    },

    getAPI: function () {
        return api;
    }
}

function initial_auth(req, res){
    if (correctKey.indexOf(req.params.key) == -1) {
        return res.status(200).json({
            code: -1, status: "invalid key"
        })
    }

    return res.status(200).json({
        code: 0, status: "OK"
    })
}

function cms_status(req, res) {
    var server = "";
    if (req.params.server == "kirk") {
        server = config.KIRK;
    } else if (req.params.server == "picard") {
        server = config.PICARD;
    } else if (req.params.server == "riker") {
        server = config.RIKER;
    } else if (req.params.server == "chekov") {
        server = config.CHEKOV;
    } else {
        return res.status(400).json({
            code: -1, status: "invalid server"
        })
    }

    if (correctKey.indexOf(req.params.key) == -1) {
        common.log("CMS Restart - Invalid Key"); 
        return res.status(400).json({
            code: -1, status: "invalid key"
        })	
    }

    if (req.params.status == "restart") {
        exec("$HOME/scripts/bash/restart_cms.exp.sh " + server, puts);
        common.log("CMS Restart - " + server +  " Successful")
    } else if (req.params.status == "start"){
        exec("$HOME/scripts/bash/start_cms.exp.sh " + server, puts);
        common.log("CMS Start - " + server +  " Successful")
    } else if (req.params.status == "stop"){
        exec("$HOME/scripts/bash/stop_cms.exp.sh " + server, puts);
        common.log("CMS Stop - " + server +  " Successful")
    } else {
        return res.status(400).json({
            code: -1, status: "invalid status"
        })	
    }

    return res.status(200).json({
        code: 0, status: "OK"
    })
}

function bypass(req, res) {
    var server = "";
    var traffic = "";
    if (req.params.server == "epc01") {
        server = config.LWEPC01;
        traffic = config.TRAFFIC_EPC01;
    } else if (req.params.server == "epc02") {
        server = config.LWEPC02;
        traffic = config.TRAFFIC_EPC02;
    } else if (req.params.server == "bypass_server") {
        server = config.BYPASS;
        traffic = config.TRAFFIC_BYPASS;
    } else {
        return res.status(400).json({
            code: -1, status: "invalid server"
        })
    }

    if (correctKey.indexOf(req.params.key) == -1) {
        common.log("Bypass Control - Invalid Key"); 
        return res.status(400).json({
            code: -1, status: "invalid key"
        })	
    }

    if (req.params.status == "divert") {
        exec("$HOME/scripts/bash/divert_traffic.exp.sh " + server + " " + traffic, puts);
        common.log("Bypass Control: Divert traffic " + server +  " Successful")
    } else if (req.params.status == "flush"){
        exec("$HOME/scripts/bash/flush_iptables.exp.sh " + server, puts);
        common.log("Bypass Control: Flush IPTables " + server +  " Successful")
    } else if (req.params.status == "tcpkill"){
        exec("$HOME/scripts/bash/kill_tcpconnections.exp.sh " + server + " " + traffic, puts);
        common.log("Bypass Control: Kill TCP Connection " + server +  " Successful")
    } else if (req.params.status == "restart") {
        exec("$HOME/scripts/bash/restart_bypass.exp.sh " + server, puts);
        common.log("Bypass Service - " + server +  " Successful")
    } else if (req.params.status == "start"){
        exec("$HOME/scripts/bash/start_bypass.exp.sh " + server, puts);
        common.log("Bypass Service - " + server +  " Successful")
    } else if (req.params.status == "stop"){
        exec("$HOME/scripts/bash/stop_bypass.exp.sh " + server, puts);
        common.log("Bypass Service - " + server +  " Successful")
    } else {
        return res.status(400).json({
            code: -1, status: "invalid status"
        })	
    }

    return res.status(200).json({
        code: 0, status: "OK"
    })
}

function bypass_test(req, res) {
    var server = "";
    var traffic = "";
    if (req.params.server == "test_server") {
        server = config.LWTES01;
        traffic = config.TRAFFIC_TES01;
    } else if (req.params.server == "bypass_server") {
        server = config.BYPASS;
        traffic = config.TRAFFIC_BYPASS;
    } else {
        return res.status(400).json({
            code: -1, status: "invalid server"
        })
    }

    if (correctKey.indexOf(req.params.key) == -1) {
        common.log("Bypass Control - Invalid Key"); 
        return res.status(400).json({
            code: -1, status: "invalid key"
        })	
    }

    if (req.params.status == "divert") {
        exec("$HOME/scripts/bash/divert_traffic.exp.sh " + server + " " + traffic, puts);
        common.log("Bypass Control (Test): Divert traffic " + server +  " Successful");
    } else if (req.params.status == "flush"){
        exec("$HOME/scripts/bash/flush_iptables.exp.sh " + server, puts);
        common.log("Bypass Control (Test): Flush IPTables " + server +  " Successful")
    } else if (req.params.status == "tcpkill"){
        exec("$HOME/scripts/bash/kill_tcpconnections.exp.sh " + server + " " + traffic, puts);
        common.log("Bypass Control (Test): Kill TCP Connection " + server +  " Successful")
    } else if (req.params.status == "restart") {
        exec("$HOME/scripts/bash/restart_bypass_test.exp.sh " + server, puts);
        common.log("Bypass Service (Test) - " + server +  " Successful")
    } else if (req.params.status == "start"){
        exec("$HOME/scripts/bash/start_bypass_test.exp.sh " + server, puts);
        common.log("Bypass Service (Test) - " + server +  " Successful")
    } else if (req.params.status == "stop"){
        exec("$HOME/scripts/bash/stop_bypass_test.exp.sh " + server, puts);
        common.log("Bypass Service (Test) - " + server +  " Successful")
    } else {
        return res.status(400).json({
            code: -1, status: "invalid status"
        })	
    }

    return res.status(200).json({
        code: 0, status: "OK"
    })
}
function bss_traffic(req, res) {
    var server = "";
    if (req.params.server == "bss01") {
        server = config.LWBSS01;
    } else if (req.params.server == "bss02") {
        server = config.LWBSS02;
    } else if (req.params.server == "bss03") {
        server = config.LWBSS03;
    } else if (req.params.server == "bss04") {
        server = config.LWBSS04;
    } else {
        return res.status(400).json({
            code: -1, status: "invalid server"
        })
    }

    if (correctKey.indexOf(req.params.key) == -1) {
        common.log("BSS Traffic - Invalid Key"); 
        return res.status(400).json({
            code: -1, status: "invalid key"
        })	
    }

    if (req.params.status == "divert") {
        exec("$HOME/scripts/bash/divert_bsstraffic.exp.sh " + server, puts);
        common.log("BSS Traffic - Divert traffic to Peer " + server +  " Successful");
    } else if (req.params.status == "redivert"){
        exec("$HOME/scripts/bash/allow_bsstraffic.exp.sh " + server, puts);
        common.log("BSS Traffic - Re-divert to normal mode " + server +  " Successful")
    } else {
        return res.status(400).json({
            code: -1, status: "invalid status"
        })	
    }

    return res.status(200).json({
        code: 0, status: "OK"
    })
}

function billing_action(req, res) {
    server = config.LWBSS02;
    var fullBody = "";
    var body = "";
    var filename = "";
    var date = "";
    var status = "";

    req.on('data', function(chunk) {
        fullBody += chunk.toString();
        insert_me = common.safeParse(fullBody);
        date = insert_me.date;
        status = insert_me.status;
        if (!date) {
            return res.status(400).json({
                code: -1, status: "invalid date"
            })	
        } else {
            if (req.params.action == "sync" && status == "success") {
                exec("rsync -avz nodeuser@" + server + ":/opt/crestelsetup/exportbills/live/pdf/" + date + "/" + status + "/*.pdf /mnt/bills/live/" + date , puts);
                common.log("Bills Success is synced!");
            } else if (req.params.action == "sync" && status == "failed") {
                exec("rsync -avz nodeuser@" + server + ":/opt/crestelsetup/exportbills/live/pdf/" + date + "/" + status + "/*.pdf /mnt/bills/live_failed/" + date , puts);
                common.log("Bills Failed is synced!");
            } else if (req.params.action == "sync" && status == "partial") {
                exec("rsync -avz nodeuser@" + server + ":/opt/crestelsetup/exportbills/live/pdf/" + date + "/" + status + "/*.pdf /mnt/bills/live_partially/" + date , puts);
                common.log("Bills Partial is synced!");
            } else if (req.params.action == "sync" && status == "negative") {
                exec("rsync -avz nodeuser@" + server + ":/opt/crestelsetup/exportbills/live/pdf/" + date + "/" + status + "/*.pdf /mnt/bills/live_negative/" + date , puts);
                common.log("Bills Partial is synced!");
            } else if (req.params.action == "delete" && status == "success" && (insert_me.filename)) {
                exec("rm -f /mnt/bills/live/" + date + "/" + insert_me.filename, puts);
                common.log("Bill " + insert_me.filename + " is successfully removed in success directory");
            } else if (req.params.action == "delete" && status == "failed" && (insert_me.filename)) {
                exec("rm -f /mnt/bills/live_failed/" + date + "/" + insert_me.filename, puts);
                common.log("Bill " + insert_me.filename + " is successfully removed in failed directory");
            } else if (req.params.action == "delete" && status == "negative" && (insert_me.filename)) {
                exec("rm -f /mnt/bills/live_negative/" + date + "/" + insert_me.filename, puts);
                common.log("Bill " + insert_me.filename + " is successfully removed in failed directory");
            } else if (req.params.action == "delete" && status == "partial" && (insert_me.filename)) {
                exec("rm -f /mnt/bills/live_partial/" + date + "/" + insert_me.filename, puts);
                common.log("Bill " + insert_me.filename + " is successfully removed in partial directory");
            } else {
                return res.status(400).json({
                    code: -1, status: "invalid status"
                })	
            }
        }
        return res.status(200).json({
            code: 0, status: "OK"
        });
    });


}

function puts(error, stdout, stderr) { }
