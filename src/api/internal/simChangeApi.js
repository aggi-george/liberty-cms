var config = require('../../../config');
var common = require('../../../lib/common');
var ec = require('../../../lib/elitecore');
var bonusManager = require('./../../core/manager/bonus/bonusManager');
var referralManager = require('./../../core/manager/promotions/referralManager');
var promotionsManager = require('./../../core/manager/promotions/promotionsManager');
var configManager = require('../../core/manager/config/configManager');
var repairManager = require('../../core/manager/account/repairManager');
var webUtils = require('../web/web');
var async = require('async');

var BaseError = require('../../core/errors/baseError.js');

var API_DOC = [];
var FIELDS_LIMIT = 20;
var LOG = config.LOGSENABLED;

module.exports = {

    init: function (baseApi, category, web) {
        var path = baseApi + category;

        //Parse params
        web.put(path + '/*/:key', parsePut);

        //sim lost endpoint
        var path = path + '/';
        web.put(path + 'initChangeSim/:key', initChangeSim);
    },

    getAPI: function () {
        return api;
    }
}

function initChangeSim(req, res){
    var serviceInstanceNumber = req.query.serviceInstanceNumber;
    if(!serviceInstanceNumber || serviceInstanceNumber.length == 0){
        return res.status(400).json({
            code: -1,
            status: "INVALID_PARAMS",
            error: "invalid params"
        });
    }else{  
        repairManager.simChangeAndNotify(serviceInstanceNumber, function(err, results){
            if(err){
                return res.status(400).json({
                    code: -1,
                    status: err.status,
                    error: err.message ? err.message : ""
                });
            }else{
                res.json({code: 0});
            }
        });
    }
}

function parsePut(req, res, next){
    webUtils.checkApiKey(req.params.key, function(err, allowed){
        if(err || !allowed){
            res.status(401).json({
                "code": -1,
                "error": "API key verification failed"
            });
        }else{
            next();
        }
    })
}

