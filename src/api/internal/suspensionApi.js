var config = require('../../../config');
var common = require('../../../lib/common');
var webUtils = require('../web/web');
var async = require('async');
var BaseError = require('../../core/errors/baseError.js');
var suspensionManager = require('../../core/manager/account/suspensionManager');
var elitecoreUserService = require('../../../lib/manager/ec/elitecoreUserService');
const accountManager = require('../../core/manager/account/accountManager');
const dateTimeLib = require('../../utils/dateTime');
const Bluebird = require('bluebird');
const SUSPENSION_CHARGE_PER_BILL_CYCLE = 10;
const postFieldsLimit = 20;
const thresholdTime = {
    hour: 3,
    minute: 59,
    second: 59,
    millisecond: 59,
}

module.exports = {

    init: function (baseApi, category, web) {
        var path = baseApi + category;
        //Parse params
        web.put(path + '/*/:key', parseForm);

        path = path + '/';
        web.get(path + 'eligibility/:serviceInstanceNumber/:key', eligibilityHandler);
        web.put(path + 'schedule/:serviceInstanceNumber/:key', scheduleSuspensionHandler);
        web.put(path + 'cancel/:key', cancelSuspensionHandler);
    },
}

function eligibilityHandler(req, res){
    const { serviceInstanceNumber } = req.params;

    return getSuspensionEligibilityBySIN(serviceInstanceNumber)
        .then(({ eligibility }) => {
            if (!eligibility.value) {
                return res.json({
                    code: 0,
                    result: {
                        eligibility
                    },
                });
            }
            const suspension_date = suspensionManager.getSuspensionStartDates();
            return res.json({
                code: 0,
                result: {
                    suspension_date,
                    eligibility,
                    price_per_month: SUSPENSION_CHARGE_PER_BILL_CYCLE,
                }
            })
        })
        .catch(err => {
            common.log('SuspensionApi', 'Error Occurred: '+ err.message);
            if (err.status === 'ALREADY_SCHEDULED') {
                return res.json({
                    code: 0,
                    result: {
                        eligibility: alreadyTerminatedResponse()
                    }
                })
            }
            return res.json({
                code: -1,
                status: err.status ? err.status : 'Failed',
                message: err.message ? err.message : 'N/A'
            })
        });
}
function getCustomerCacheBySIN (sin) {
    const loadCustomerCachePromise = Bluebird.promisify(elitecoreUserService.loadUserInfoByServiceInstanceNumber);
   return loadCustomerCachePromise(sin);
}

function getScheduledSuspensionBySIN ({serviceInstanceNumber, billingAccountNumber}) {
    return suspensionManager.getScheduledSuspensionBySIN(serviceInstanceNumber)
        .then(result => {
            return billingAccountNumber;
        })
        .catch(err => {
            if (err.alreadySuspended) {
                throw new BaseError(
                    `Your number is already scheduled for suspension: ${serviceInstanceNumber}`, 
                    "ALREADY_SCHEDULED");
            }
            throw new BaseError(err);
        });
}

function parseForm(req, res, next){
    webUtils.checkApiKeyForTeams(req.params.key, [2], function(err, allowed){
        if(err || !allowed){
            res.status(401).json({
                "code": -1,
                "error": "API key verification failed"
            });
        }else{
            common.formParse(req, res, postFieldsLimit, function (fields) {
                req.fields = fields;
                next();
            });
        }
    })
}

function scheduleSuspensionHandler(req, res) {
    const { start, numberOfMonths, perMonthAmount } = req.fields;
    const { serviceInstanceNumber } = req.params;
    const executionKey = config.OPERATIONS_KEY;
    const suspensionStart = isNaN(start) ? new Date(start).getTime() : new Date(parseInt(start)).getTime();

    return getSuspensionEligibilityBySIN(serviceInstanceNumber)
        .then(({eligibility}) => {
            if (eligibility && !eligibility.value) {
                return eligibility;
            }

            if (invalidDate(suspensionStart)) {
                return Bluebird.reject(new BaseError('Date is Invalid', 'INVALID_DATE'));
            }

            const suspension_date = suspensionManager.getSuspensionStartDates();
            const isoSuspensionDate = new Date(suspensionStart).toISOString();

            if (suspension_date.indexOf(isoSuspensionDate) > -1) {
                const scheduleSuspensionPromise = Bluebird.promisify(accountManager.scheduleTemporarySuspension);
                return scheduleSuspensionPromise(
                    serviceInstanceNumber, 
                    suspensionStart, numberOfMonths, 
                    perMonthAmount, req.user,
                    executionKey
                );
            }
            return Bluebird.reject(new BaseError('Invalid Date for suspension', 'INVALID_DATE'));
        })
        .then(result => {
            if (result && result.value === false) {
                common.log('SuspensionApi', 'Not eligible for suspension sin= '+ serviceInstanceNumber);
                return res
                        .status(400)
                        .json({
                            code: -1,
                            message: result.description ? result.description : 'Failed',
                        })
            }
            common.log('SuspensionApi', 'Eligible for suspension sin= '+ serviceInstanceNumber);
            const suspensionEffectiveId = result.ops.length ? result.ops[0]._id ? result.ops[0]._id : 'N/A' : 'N/A';
            const suspensionOverId = result.ops.length ? result.ops[1]._id ? result.ops[1]._id : 'N/A' : 'N/A';
            return res
                    .json({
                        "code": 0, 
                        "status": "Success",
                        serviceInstanceNumber,
                        suspensionEffectiveId,
                        suspensionOverId
                    });
        })
        .catch(err => {
            common.log('SuspensionApi', 'Not able to schedule suspension = '+ err.details ? err.details : err.message);
            return res.json({
                code: -1,
                status: err.status ? err.status : 'Failed',
                message : err.description ? err.description: err.message ? err.message : 'N/A'
            })
        });
}

function getSuspensionEligibilityBySIN(sin) {
    return getCustomerCacheBySIN(sin)
        .then(getScheduledSuspensionBySIN)
        .then(suspensionManager.checkInvoiceEligibility);
}

function cancelSuspensionHandler(req, res) {
    const { suspensionEffectiveId, suspensionOverId } = req.fields;

    return suspensionManager.cancelSuspension(suspensionEffectiveId, suspensionOverId)
        .then(() => {
            return res.json({
                code: 0,
                "status": "Success"
            })
        })
        .catch(err => {
            return res.json({
                code: -1,
                status: err.status ? err.status : 'Failed',
                message: err.message ? err.message : 'N/A'
            })
        })
}

function alreadyTerminatedResponse() {
    return {
        value: false,
        reason: 'ALREADY_SCHEDULED',
        description: 'Not eligible for suspension due to:Suspension is already scheduled for this number',
        disclaimer: 'Please contact our Happiness Experts at happiness@circles.life if you have any more questions.',
    };
}

function invalidDate(start) {
    return new Date().getTime() > start || start != parseInt(start);
}

