

const config = require(`${__base}/config`);
const common = require(`${__lib}/common`);
const base = require(`${__api}/base/sessionManager`);
const validationManager = require(`${__manager}/notifications/validationManager`);

const LOG = config.LOGSENABLED;

class ValidationApi {
    static init(apiBase, category, web) {
        const path = `${apiBase}${category}/`;
        web.post(`${path}*/:key`, base.parseForm);
        web.post(`${path}send/:type/:activity/:prefix/:number/:key`, sendHandler);
        web.post(`${path}compare/:notificationId/:pin/:key`, compareHandler);
    }
}

function replyHandler(res, err, result) {
    if (err) {
        result = {
            code: -1,
            error: err.message
        };
    } else result.code = 0;
    res.json(result);
}

function sendHandler(req, res) {
    base.getTeamAllowed(req.params, (errTeam, team) => {
        if (!team) return replyHandler(res, new Error('Invalid Team'));
        if (req.params.type == 'sms') {
            pinSMS(team, req.params, (err, result) => {
                if (err) return replyHandler(res, err);
                else if (result) replyHandler(res, undefined, {
                    notificationId: result.notificationId
                });
            });
        } else replyHandler(res, new Error('Unknown type'));
    });
}

function compareHandler(req, res) {
    base.getTeamAllowed(req.params, (errTeam, team) => {
        if (!team) return replyHandler(res, new Error('Invalid Team'));
        pinCompare(req.params, (err, result) => {
            replyHandler(res, err, result);
        });
    });
}

function pinSMS(team, params, callback) {
    const activity = params.activity == 'portin_otp' ? params.activity : undefined;
    if (!activity) return new Error('Unknown activity');
    else if (!params.activity || !params.prefix || !params.number) return new Error('Invalid parameters');
    params.teamID = team.id;
    params.teamName = team.name;
    validationManager.pinSMS(params, callback);
}

function pinCompare(params, callback) {
    if (!params.notificationId || !params.pin) return new Error('Invalid parameters');
    validationManager.pinCompare(params, callback);
}

module.exports = ValidationApi;
