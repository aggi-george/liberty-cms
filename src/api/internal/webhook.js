var crypto = require("crypto");
var common = require('../../../lib/common');
var config = require('../../../config');
var db = require('../../../lib/db_handler');
var ec = require('../../../lib/elitecore');
var notificationManager = require('../../core/manager/notifications/notificationManager');
var notificationSend = require('../../core/manager/notifications/send');
var sessionManager = require('../base/sessionManager');
var hostManager = require('../../core/manager/system/hostManager');

var SG_PREFIX = "65";
var FIELDS_LIMIT = 20;
var LOG = config.LOGSENABLED;

var SUPPORTED_EC_NOTIFICATIONS = [
    'port_in_failure',
    'port_in_success',
    'port_in_approved',
    'port_in_request',
    'port_out_success',
    'port_out_failure',
    'sim_provisioned',
    'add_on_subscription', //checked
    'add_on_unsubscription',
    'payment_received',
    'bill_payment_success',
    'bill_payment_failure',
    'pay200', //checked
    'pay200_warning',
    'usage_consumption_data', //checked
    'cap_roaming_data', //checked
    'change_base_plan',
    'status_changed',
    'vas_activation',
    'vas_deactivation',
    'whatsapp_consumption_data_800mb',
    'whatsapp_consumption_data_1gb',
    'whatsapp_consumption_data_2gb',
    'whatsapp_consumption_data_3gb',
    'policy_fair_usage_30gb',
    'policy_fair_usage_50gb',
    'policy_fair_usage_55gb',
    'policy_fair_usage_65gb',
    'policy_fair_usage_70gb',
    'whatsapp_consumption_data_common'
];

//exports

exports.init = init;
exports.webHookInternal = webHookInternal;

//functions

function init(baseApi, category, web) {
    var path = baseApi + category;

    //Parse params

    web.post(path + '/*', parsePost);

    // webhook API

    var usagePath = path + '';
    web.post(usagePath + '/notifications/:key', webHook);

    var usagePath = path + '';
    web.post(usagePath + '/notifications/internal/:key', webHookInternal);


//////// LEGACY START

        var path = baseApi + "/notifications";
        var usagePath = path + '/send';

        // Parse params

        web.post(usagePath + '/*', sessionManager.parsePost);
        web.put(usagePath + '/*', sessionManager.parsePutFiles);

        // Notifications - send API

        web.post(usagePath + '/:activity/:key', legacySend);
        web.put(usagePath + '/:activity/:key', legacySend);

//////// LEGACY END
}

function parsePost(req, res, next) {
    common.formParse(req, res, FIELDS_LIMIT, function (fields) {
        req.fields = fields;
        next();
    });
}

//////// LEGACY START

function legacySend(req, res) {
    req.fields.variables = JSON.parse(JSON.stringify(req.fields));
    req.fields.activity = req.params.activity;

    delete req.fields.variables.number;
    delete req.fields.variables.email;
    delete req.fields.variables.name;
    delete req.fields.variables.activity;

    Object.keys(req.fields).forEach(function (key) {
        if (key !== 'number' && key !== 'email' && key !== 'name'
            && key !== 'activity' && key !== 'variables') {
            delete req.fields[key];
        }
    });

    common.log("[DEPRECATED] legacySend: ip=" + req.ip, "fields=" + JSON.stringify(req.fields));
    webHookInternal(req, res);
}

//////// LEGACY END

function webHookInternal(req, res) {
    if (LOG) common.log("webHookInternal", JSON.stringify(req.fields));
    if ((req.ip !== "10.20.1.9")     // worf-VIP
        && (req.ip !== "10.20.1.10")    // guinan-VIP
        && (req.ip !== "10.20.1.19")    // worf
        && (req.ip !== "10.20.1.20")
        && (req.ip !== "10.20.1.21")
        && (req.ip !== "10.20.1.22")
        && (req.ip !== "10.20.1.23")
        && (req.ip !== "10.20.1.27")    // kirk-VIP
        && (req.ip !== "10.20.1.28")    // riker-VIP
        && (req.ip !== "10.20.1.29")    // wesley
        && (req.ip !== "10.20.1.37")    // kirk
        && (req.ip !== "10.20.1.38")    // riker
        && (req.ip !== "10.20.1.39")    // guinan
        && (req.ip !== "10.20.1.40")    // archer
        && (req.ip !== "10.20.1.41")    // hawk
        && (req.ip !== "10.20.1.42")    // raven
        && (req.ip !== "10.20.1.47")    // picard
        && (req.ip !== "10.20.1.48")    // chekov
        && (req.ip !== "10.20.1.57")    // janeway
        && (req.ip !== "10.20.1.1")
        && (req.ip !== "10.20.1.2")
        && (req.ip !== "10.20.1.3")
        && (req.ip !== "10.20.1.4")
        && (req.ip !== "10.20.1.5")
        && (req.ip !== "10.20.1.6")
        && (req.ip !== "10.20.1.7")
        && (req.ip !== "10.20.1.8")
        && (req.ip !== "52.77.162.51")
        && (req.ip !== "52.76.169.92")
        && (req.ip !== "52.76.12.112")
        && (req.ip !== "52.76.169.100")
        && (req.ip !== "52.77.120.196")) {
        if (LOG) common.error("webHook", "authorization error: IP is " + req.ip + " " + req.params.key);
        //res.status(403).json({"code": -1, status: "ERROR", message: "Unauthorized"});
        //return;
    }
    if (!req.fields.activity) {
        if (LOG) common.error("webHookInternal", "activity is missing");
        return res.status(400).json({"code": -1, status: "ERROR", message: "Notification error (Activity is missing)"});
    }

    var vartemp = JSON.parse(JSON.stringify(req.fields));        // support non-json requests
    delete vartemp.team;
    delete vartemp.prefix;
    delete vartemp.number;
    delete vartemp.serviceInstanceNumber;
    delete vartemp.name;
    delete vartemp.email;
    delete vartemp.activity;
    var obj = (req.fields.variables) ? req.fields.variables : vartemp;
    var notificationId = crypto.randomBytes(16).toString("hex");

    if (!obj) obj = {};
    obj.notification = JSON.stringify(req.fields);

    obj.notificationId = notificationId;
    obj.teamName = req.fields.team;
    obj.number = req.fields.number;
    obj.prefix = req.fields.prefix;
    obj.serviceInstanceNumber = req.fields.serviceInstanceNumber;
    obj.email = req.fields.email;
    obj.name = req.fields.name;
    obj.activity = req.fields.activity;
    obj.ip = req.ip;

    obj.server_dev = config.DEV;
    obj.teamKey = req.params.key;
    obj.teamID = -1;
    obj.teamName = "Unknown";

    var saveLog = function (err, obj) {
        var log = {
            ip: req.ip,
            status: "UNKNOWN",
            prefix: obj.prefix,
            number: obj.number,
            serviceInstanceNumber: obj.serviceInstanceNumber,
            customerAccountNumber: obj.customerAccountNumber,
            billingAccountNumber: obj.billingAccountNumber,
            email: obj.email,
            activity: obj.activity,
            notificationId: obj.notificationId,
            notification: obj
        };

        if (err) {
            if (obj.isActivityInvalid) {
                log.status = "ERROR_INVALID_ACTIVITY";
            } else if (obj.isProductInvalid) {
                log.status = "ERROR_PRODUCT_NOT_FOUND";
            } else {
                log.status = "ERROR";
            }
            log.errorMessage = err.message;
        } else if (obj.isIgnored) {
            log.status = "IGNORED";
        } else if (obj.isNotFound) {
            log.status = "NOT_FOUND";
        } else {
            log.status = "OK";
        }

        var getCache = function (prefix, number, serviceInstanceNumber, callback) {
            if (serviceInstanceNumber) {
                ec.getCustomerDetailsServiceInstance(log.serviceInstanceNumber, false, callback, true);
            } else {
                ec.getCustomerDetailsNumber(log.prefix, log.number, false, callback, true);
            }
        };

        if (log.serviceInstanceNumber && log.billingAccountNumber && log.customerAccountNumber) {
            db.notifications_insert("webhook", log);
        } else {
            getCache(log.prefix, log.number, log.serviceInstanceNumber, function(err, cache) {
                if (cache) {
                    log.billingAccountNumber = cache.billingAccountNumber;
                    log.customerAccountNumber = cache.customerAccountNumber;
                    log.serviceInstanceNumber = cache.serviceInstanceNumber;
                }
                db.notifications_insert("webhook", log);
            });
        }
    }

    var query = "SELECT id, name FROM teams WHERE apiKey=" + db.escape(req.params.key);
    if (LOG) common.log("webHookInternal", "query team, query=" + query);
    db.query_noerr(query, function (rows) {
        if (!rows || rows.length == 0) {
            if (LOG) common.error("webHookInternal", "authorization failed, key " + req.params.key + " is invalid");
            res.status(403).json({"code": -1, status: "ERROR", message: "Unauthorized"});
            return;
        }
        obj.teamID = rows[0].id;
        obj.teamName = rows[0].name;

        if (obj.number) obj.numberSource = "notification";
        if (obj.serviceInstanceNumber) obj.serviceSource = "notification";
        if (obj.email) obj.emailSource = "notification";
        if (obj.name) obj.nameSource = "notification";
        if (obj.cardlast4digits) obj.cardlast4digitSource = "notification";

        if (!obj.name) {
            if (obj.customer_name) {
                obj.name = obj.customer_name;
            } else if (obj.firstname) {
                obj.name = obj.firstname;
            }
        }

        var sendNotification = function (obj, product) {
            if (notificationManager.suppressNotificationsList().indexOf(obj.activity) >= 0) {
                notificationManager.suppressNotificationsStatus(obj.number, obj.serviceInstanceNumber, function(cache) {
                    if (cache) {
                        obj.isIgnored = true;
                        saveLog(null, obj);
                    } else {
                        saveLog(null, obj);
                        notificationSend.deliver(obj, product);
                    }
                });
            } else {
                saveLog(null, obj);
                notificationSend.deliver(obj, product);
            }
        };
        var triageStart = new Date();
        notificationManager.triage(obj, function (error, resObj, product) {
            resObj.processingTime = new Date() - triageStart;
            if (error) {
                var errStatus = (error.status) ? error.status : "ERROR";
                res.status(400).json({
                    code: -1,
                    notificationId: notificationId,
                    status: errStatus,
                    error: error.message,
                    message: "Notification failed"
                });
                return saveLog(error, resObj);
            } else {
                if (req.files && req.files.content) {
                    resObj.attachment = {
                        "path": req.files.content.path,
                        "filename": req.files.content.name
                    };
                }

                res.json({
                    code: 0,
                    notificationId: notificationId,
                    status: "OK",
                    message: "Notification accepted"
                });

                sendNotification(resObj, product);
            }
        });
    });
}

function ecActivityMap(activity, value) {
    var activityId = undefined;
    switch(activity.toLowerCase()) {
        case 'portinfailure':
            activityId = 'port_in_failure';
            break;
        case 'portinsuccess':
            activityId = 'port_in_success';
            break;
        case 'portininterimstatus':
            activityId = 'port_in_approved';
            break;
        case 'portout':
            activityId = 'port_out_success';
            break;
        case 'sim_card_success':
            activityId = 'sim_provisioned';
            break;
        case 'make_payment':
            activityId = 'payment_received';
            break;
        case 'pay200warning':
            activityId = 'pay200_warning';
            break;
        case 'usagelimit':
        case 'usageconsumption':
            activityId = 'usage_consumption_data';
            break;
        case 'dataroaming':
            activityId = 'cap_roaming_data';
            break;
        case 'change_service_plan':
            activityId = 'change_base_plan';
            break;
        case 'change_status':
            activityId = 'status_changed';
            break;
        case 'vasactivation':
            activityId = 'vas_activation';
            break;
        case 'vasdeactivation':
            activityId = 'vas_deactivation';
            break;
        case 'whatsappusageconsumption':
            if (value == "800MB") activityId = 'whatsapp_consumption_data_800mb';
            else if (value == "1000MB") activityId = 'whatsapp_consumption_data_1gb';
            else if (value == "2GB") activityId = 'whatsapp_consumption_data_2gb';
            else if (value == "3GB") activityId = 'whatsapp_consumption_data_3gb';
            else activityId = 'whatsapp_consumption_data_common';
            break;
        case 'port_in_failure':
        case 'port_in_success':
        case 'port_in_approved':
        case 'port_in_request':
        case 'port_out_success':
        case 'port_out_failure':
        case 'sim_provisioned':
        case 'add_on_subscription':
        case 'add_on_unsubscription':
        case 'payment_received':
        case 'bill_payment_success':
        case 'bill_payment_failure':
        case 'pay200':
        case 'pay200_warning':
        case 'usage_consumption_data':
        case 'cap_roaming_data':
        case 'change_base_plan':
        case 'status_changed':
        case 'vas_activation':
        case 'vas_deactivation':
        case 'whatsapp_consumption_data_800mb':
        case 'whatsapp_consumption_data_1gb':
        case 'whatsapp_consumption_data_2gb':
        case 'policy_fair_usage_30gb':
        case 'policy_fair_usage_50gb':
        case 'policy_fair_usage_55gb':
        case 'policy_fair_usage_65gb':
        case 'policy_fair_usage_70gb':
            activityId = activity.toLowerCase();
            break;
        default:
    }
    return activityId;
}

function webHook(req, res) {
    if(config.DEV){
        if (LOG) common.log("webHook", "triggered with DEV ENV");
    }else if (!hostManager.allowed(req.ip, ["LWBSS01", "LWBSS02", "LWBSS03", "LWBSS04", "LWTES01", "KIRKFLOAT", "JANEWAY", "KIRK", "PICARD"])) {
        if (LOG) common.error("webHook", "authorization error: IP is " + req.ip + " " + req.params.key);
        res.status(403).send("Notification error (Unauthorized)");
        return;
    }

    // check EliteCore key
    if (req.params.key !== config.ELITECORE_KEY) {
        if (LOG) common.error("webHook: authorization error: key is " + req.params.key);
        res.status(403).send("Unauthorized");
        return;
    }

    if (!req.fields.template) {
        if (!req.query.template) {
            if (LOG) common.error("webHook", "param template is empty");
            res.status(400).send("Notification error (Template is missing)");
            return;
        } else {
            req.fields.template = req.query.template;
        }
    }

    var notificationId = crypto.randomBytes(16).toString("hex");
    var template = (req.fields.template) ? req.fields.template.replace(/&quot;/g, '"') : "";
    var templateObj = common.safeParse(template);

    if (!templateObj || !templateObj.message || !templateObj.number || !templateObj.email || !templateObj.activity) {
        var errorMessage = "Template param is invalid, template=" + template;
        common.error("webHook", errorMessage);
        db.notifications_insert("webhook", {status: "ERROR_INVALID_PARAMS", templateObj: templateObj});
        return res.status(400).json({code: -1, error: errorMessage});
    }

    templateObj.teamKey = req.params.key;
    templateObj.notificationId = notificationId;

    res.json({
        code: 0,
        status: "OK",
        notificationId: notificationId,
        message: "Notification accepted"
    });

    templateObj.ip = req.ip;
    validateWebHookObj(templateObj, function (err, obj, product) {
        if (err) {
            if (LOG) common.error("webHook", "validation failed, error=" + err.message);
        } else if (obj.isIgnored) {
            //some notifications can be ignored, refer to IGNORED_ADD_ON_SUBSCRIPTION_PRODUCTS
            if (LOG) common.log("webHook", "message ignored");
        } else {
            if (obj) obj.notification = template;
            notificationSend.deliver(obj, product);
        }
    });
}

function validateWebHookObj(obj, callback) {
    var originalActivityId = obj.activity;
    obj.activity = ecActivityMap(obj.activity, obj.value);
    obj.originalActivity = originalActivityId;
    obj.isFormattedActivity = originalActivityId !== obj.activity;

    if (originalActivityId === 'add_on_unsubscription'
        && !obj.addonname && obj.addonpack) {
        obj.addonname = obj.addonpack;
    }

    obj.teamID = 3;
    obj.teamKey = config.ELITECORE_KEY;
    obj.teamName = "Elitecore";

    if (SUPPORTED_EC_NOTIFICATIONS.indexOf(obj.activity) < 0) {
        obj.isActivityInvalid = true;
        return callback(notificationManager.saveLogs(new Error("Invalid activity '" + obj.originalActivity + "'"), obj, undefined, undefined, callback));
    }

    var retries = 1;
    var boff = { "type" : "fixed", "delay" : 20 * 1000 };
    if (obj.activity == "usage_consumption_data") {
        retries = 6;
    }
    var job = db.queue.create('ingress', obj)
        .events(false)
        .removeOnComplete(true)
        .attempts(retries)
        .backoff(boff)
        .ttl(60 * 60 * 1000)
        .save(function (err) {
    });
}
