var config = require('../../../config');
var common = require('../../../lib/common');
var schedulerManager = require('../../core/manager/scheduler/schedulerManager');
var webUtils = require('../web/web');

var BaseError = require('../../core/errors/baseError.js');



module.exports = {

    init: function (baseApi, category, web) {
        var path = baseApi + category;

        //Parse params
        web.put(path + '/*/:key', parsePut);

        //sim lost endpoint
        var path = path + '/';
        web.get(path + 'eligibility/:serviceInstanceNumber/:key', getWinbackForCustomer);
    },

    getAPI: function () {
        return api;
    }
}

function getWinbackForCustomer(req, res){
    var sampleResponse = {
        "eligibility": {
          "value": true,
          "availability": "2017-09-01",
          "description": "You are not allowed to terminate. Contact to CS happyness",
          "disclaimer": "*Termination will be available from 2017-09-01"
        },
        "termination_date": [
          {
            "date": "2017-08-31"
          },
          {
            "date": "2017-09-30"
          },
          {
            "date": "2017-10-31"
          },
          {
            "date": "2017-11-30"
          },
          {
            "date": "2017-12-31"
          }
        ],
        "winback": [
          {
            "id": 1,
            "enable": true,
            "type": "FREE_2020",
            "value": "FREE_2020_ADDON_ID",
            "discription": "You will get 20 GB for free for next 3 months",
            "disclaimer": "*You will not be alloed to terminate the order for next 6 month or you need to pay $10 on termination"
          },
          {
            "id": 2,
            "enable": false,
            "type": "FREE_INCOMING",
            "value": "FREE_INCOMING_ADDON_ID",
            "discription": "You will get free incoming call for next 3 months",
            "disclaimer": "*You will not be alloed to terminate the order for next 6 month"
          }
        ],
        "reasons": [
          {
            "id": 1,
            "winback_id": 1,
            "title": "Getting better plan from other telco",
            "sub_title": "xyz",
            "enable": true
          },
          {
            "id": 2,
            "winback_id": 2,
            "title": "Movig to other country",
            "sub_title": "xyz",
            "enable": true
          },
          {
            "id": 3,
            "winback_id": null,
            "title": "Customer expires",
            "sub_title": "xyz",
            "enable": true
          },
          {
            "id": 4,
            "winback_id": null,
            "title": "Lost my SIM card",
            "sub_title": "xyz",
            "enable": true
          }
        ]
      };
    res.json({ code: 0, result: sampleResponse});
}

function parsePut(req, res, next){
    webUtils.checkApiKeyForTeams(req.params.key, [2], function(err, allowed){
        if(err || !allowed){
            res.status(401).json({
                "code": -1,
                "error": "API key verification failed"
            });
        }else{
            next();
        }
    })
}

