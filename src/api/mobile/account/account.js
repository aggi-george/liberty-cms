var db = require(__lib + '/db_handler');
var common = require(__lib + '/common');
var sessionManager = require("../../base/sessionManager");

var usage = require('./stubs/usage_stub_data1');
var user = require('./stubs/user_stub_data1');
var addons = require('./stubs/addons_stub_data1');
var notifications = require('./stubs/notifications_stub_data1');
var promotions = require('./stubs/promotions_stub_data1');

var usage_final = require('./usage');
var user_final = require('./user');
var addons_final = require('./addons');
var notifications_final = require('./notifications');
var promotions_final = require('./promotions');
var referral_final = require('./referrals');
var bonus_final = require('./bonuses');
var resources_final = require('./resources');

var batch = require('./batch');

//exports

exports.init = init;

//functions

function init(baseApi, category, web, webs) {
    var path = baseApi + category;
    var pathRes = "/res" + category;

    var addonPath = path + '/batch';
    webs.post(addonPath + '/:user_key/:signature', sessionManager.parsePost);
    webs.post(addonPath + '/:user_key/:signature', sessionManager.sessionCheckJson);
    webs.post(addonPath + '/:user_key/:signature', function (req, res) {
        sessionManager.executeMethod(req, res, batch, batch, "api");
    });

    var addonPath = path + '/addon';
    webs.post(addonPath + '/extra/update/batch/set/:user_key/:signature', sessionManager.parsePost);
    webs.post(addonPath + '/extra/update/batch/set/:user_key/:signature', sessionManager.sessionCheckJson);
    webs.post(addonPath + '/extra/update/batch/set/:user_key/:signature', function (req, res) {
        sessionManager.executeMethod(req, res, addons_final, addons, "extraUpdateBatchSet");
    });

    var logsPath = path + '/logs';
    webs.post(logsPath + '/app/states/:user_key/:signature', sessionManager.parsePost);
    webs.post(logsPath + '/app/states/:user_key/:signature', sessionManager.sessionCheckJson);
    webs.post(logsPath + '/app/states/:user_key/:signature', function (req, res) {
        sessionManager.executeMethod(req, res, user_final, user, "logsAppStates");
    });

    //Parse params
    webs.post(path + '/*', sessionManager.parsePost);
    webs.get(path + '/*', sessionManager.parseGet);
    web.post(path + '/*', sessionManager.parsePost);
    web.get(path + '/*', sessionManager.parseGet);

    //Check session
    webs.post(path + '/*/:user_key/:signature', sessionManager.sessionCheckPost);
    webs.get(path + '/*/:user_key/:signature', sessionManager.sessionCheckGet);
    web.post(path + '/*/:user_key/:signature', sessionManager.sessionCheckPost);
    //web.get(path + '/*/:user_key/:signature', sessionManager.sessionCheckGet);

    /**
     * Account - Usage - API
     */
    var usagePath = path + '/usage';

    webs.get(usagePath + '/plan/get/:user_key/:signature', function (req, res) {
        sessionManager.executeMethod(req, res, usage_final, usage, "usagePlanGet");
    });
    webs.get(usagePath + '/data/get/:user_key/:signature', function (req, res) {
        sessionManager.executeMethod(req, res, usage_final, usage, "dataGet");
    });
    webs.get(usagePath + '/other/get/:user_key/:signature', function (req, res) {
        sessionManager.executeMethod(req, res, usage_final, usage, "otherGet");
    });
    webs.get(usagePath + '/apps/gentwo/get/:user_key/:signature', function (req, res) {
        sessionManager.executeMethod(req, res, usage_final, usage, "gentwoGet");
    });
    webs.get(usagePath + '/details/get/:user_key/:signature', function (req, res) {
        sessionManager.executeMethod(req, res, usage_final, usage, "callDetailsGet");
    });

    /**
     * Account - History - API
     */
    var historyPath = path + '/history';

    webs.get(historyPath + '/usage/get/:user_key/:signature', function (req, res) {
        sessionManager.executeMethod(req, res, usage_final, usage, "historyUsageGet");
    });
    webs.get(historyPath + '/bills/get/:user_key/:signature', function (req, res) {
        sessionManager.executeMethod(req, res, usage_final, usage, "historyBillsGet");
    });
    webs.post(historyPath + '/bills/resend/:user_key/:signature', function (req, res) {
        sessionManager.executeMethod(req, res, usage_final, usage, "historyBillsResend");
    });
    webs.post(historyPath + '/bills/payment/set/:user_key/:signature', function (req, res) {
        sessionManager.executeMethod(req, res, usage_final, usage, "paymentSet");
    });


    /**
     * Account - Addons - API
     */
    var addonPathRes = pathRes + '/addon';

    webs.get(addonPathRes + '/bonus/history/item/icon/:id', function (req, res) {
        bonus_final.getBonusImage(req, res);
    });
    webs.get(pathRes + '/resource/:id', function (req, res) {
        resources_final.getResource(req, res);
    });

    var addonPath = path + '/addon';

    webs.get(addonPath + '/general/all/get/:user_key/:signature', function (req, res) {
        sessionManager.executeMethod(req, res, addons_final, addons, "generalAllGet");
    });
    webs.get(addonPath + '/topup/all/get/:user_key/:signature', function (req, res) {
        sessionManager.executeMethod(req, res, addons_final, addons, "topupAllGet");
    });
    webs.get(addonPath + '/bonus/history/get/:user_key/:signature', function (req, res) {
        sessionManager.executeMethod(req, res, bonus_final, addons, "bonusHistoryGet");
    });
    webs.get(addonPath + '/bonus/availed/get/:user_key/:signature', function (req, res) {
        sessionManager.executeMethod(req, res, bonus_final, addons, "bonusAvailed");
    });
    webs.get(addonPath + '/bonus/available/get/:user_key/:signature', function (req, res) {
        sessionManager.executeMethod(req, res, bonus_final, addons, "bonusAvailableGet");
    });
    webs.get(addonPath + '/bonus/leaderboard/get/:user_key/:signature', function (req, res) {
        sessionManager.executeMethod(req, res, bonus_final, addons, "leaderboardGet");
    });
    webs.post(addonPath + '/bonus/available/use/:user_key/:signature', function (req, res) {
        sessionManager.executeMethod(req, res, bonus_final, addons, "bonusAvailableUse");
    });
    webs.post(addonPath + '/topup/update/set/:user_key/:signature', function (req, res) {
        sessionManager.executeMethod(req, res, addons_final, addons, "addBoostHandler");
    });
    webs.get(addonPath + '/extra/all/get/:user_key/:signature', function (req, res) {
        sessionManager.executeMethod(req, res, addons_final, addons, "extraAllGet");
    });
    webs.post(addonPath + '/extra/update/set/:user_key/:signature', function (req, res) {
        sessionManager.executeMethod(req, res, addons_final, addons, "extraUpdateSet");
    });
    webs.post(addonPath + '/general/update/set/:user_key/:signature', function (req, res) {
        sessionManager.executeMethod(req, res, addons_final, addons, "generalUpdateSet");
    });

    /**
     * Account - My - Profile/Plan - API
     */
    var myPath = path + '/my';

    webs.get(myPath + '/profile/details/get/:user_key/:signature', function (req, res) {
        sessionManager.executeMethod(req, res, user_final, user, "profileDetailsGet");
    });
    webs.get(myPath + '/profile/boosts/get/:user_key/:signature', function (req, res) {
        sessionManager.executeMethod(req, res, user_final, user, "profileBoostsGet");
    });
    webs.get(myPath + '/profile/creditcard/update/:user_key/:signature', function (req, res) {
        sessionManager.executeMethod(req, res, user_final, user, "creditCardUpdate");
    });
    webs.post(myPath + '/profile/address/billing/set/:user_key/:signature', function (req, res) {
        sessionManager.executeMethod(req, res, user_final, user, "billingAddressUpdateSet");
    });
    webs.post(myPath + '/profile/email/set/:user_key/:signature', function (req, res) {
        sessionManager.executeMethod(req, res, user_final, user, "emailUpdateSet");
    });
    webs.get(myPath + '/profile/dob/verify/get/:user_key/:signature', function (req, res) {
        sessionManager.executeMethod(req, res, user_final, user, "dobVerifyGet");
    });
    webs.get(myPath + '/profile/id/digits/verify/get/:user_key/:signature', function (req, res) {
        sessionManager.executeMethod(req, res, user_final, user, "identityNumberVerifyGet");
    });
    webs.post(myPath + '/profile/plan/circlesswitch/upgrade/:user_key/:signature', function (req, res) {
        sessionManager.executeMethod(req, res, user_final, user, "upgradePlanToCirclesSwitch");
    });
    webs.get(myPath + '/profile/devices/get/:user_key/:signature', function (req, res) {
        sessionManager.executeMethod(req, res, user_final, user, "profileDevicesGet");
    });
    webs.get(myPath + '/plan/details/get/:user_key/:signature', function (req, res) {
        sessionManager.executeMethod(req, res, user_final, user, "planDetailsGet");
    });
    webs.get(myPath + '/guide/screens/get/:user_key/:signature', function (req, res) {
        sessionManager.executeMethod(req, res, user_final, user, "guideScreensGet");
    });
    webs.post(myPath + '/portin/request/add/:user_key/:signature', function (req, res) {
        sessionManager.executeMethod(req, res, user_final, user, "putPortInRequest");
    });
    webs.post(myPath + '/portin/request/cancel/:user_key/:signature', function (req, res) {
        sessionManager.executeMethod(req, res, user_final, user, "cancelPortInRequest");
    });
    webs.get(myPath + '/portin/request/active/:user_key/:signature', function (req, res) {
        sessionManager.executeMethod(req, res, user_final, user, "getPortInStatus");
    });
    webs.post(myPath + '/cap/roaming/set/:user_key/:signature', function (req, res) {
        sessionManager.executeMethod(req, res, user_final, user, "roamingCapUpdateSet");
    });

    /**
     * Account - Notifications - API
     */
    var notificationsPath = path + '/notifications';

    webs.get(notificationsPath + '/history/get/:user_key/:signature', function (req, res) {
        sessionManager.executeMethod(req, res, notifications_final, notifications, "notificationsHistoryGet");
    });
    webs.post(notificationsPath + '/readstatus/update/set/:user_key/:signature', function (req, res) {
        sessionManager.executeMethod(req, res, notifications_final, notifications, "notificationsReadStatusSet");
    });

    /**
     * Account - Promotions - API
     */
    var promotionsPath = path + '/promotions';

    webs.post(promotionsPath + '/code/set/:user_key/:signature', function (req, res) {
        sessionManager.executeMethod(req, res, promotions_final, promotions, "promotionCodeSet");
    });
    webs.post(promotionsPath + '/location/set/:user_key/:signature', function (req, res) {
        sessionManager.executeMethod(req, res, promotions_final, promotions, "locationSet");
    });
    webs.get(promotionsPath + '/location/get/:user_key/:signature', function (req, res) {
        sessionManager.executeMethod(req, res, promotions_final, promotions, "locationsGet");
    });
    webs.get(promotionsPath + '/location/promo/dialog/get/:user_key/:signature', function (req, res) {
        sessionManager.executeMethod(req, res, promotions_final, promotions, "locationPromoDialogGet");
    });
    webs.get(promotionsPath + '/location/nearby_branches/:user_key/:signature', function (req, res) {
        sessionManager.executeMethod(req, res, promotions_final, promotions, "getNearbyBranch");
    });
    webs.post(promotionsPath + '/location/promo/set/:user_key/:signature', function (req, res) {
        sessionManager.executeMethod(req, res, promotions_final, promotions, "locationPromoSet");
    });
    webs.get(promotionsPath + '/payment/url/get/:user_key/:signature', function (req, res) {
        sessionManager.executeMethod(req, res, promotions_final, promotions, "getPaymentUrl");
    });

    /**
     * Account - Referral - API
     */
    var referralPath = path + '/referral';

    webs.get(referralPath + '/code/get/:user_key/:signature', function (req, res) {
        sessionManager.executeMethod(req, res, referral_final, addons, "referralCodeGet");
    });
    webs.post(referralPath + '/code/add/:user_key/:signature', function (req, res) {
        sessionManager.executeMethod(req, res, referral_final, addons, "referralCodeAdd");
    });

    /**
     * Actions - Get status API
     */
    var actionStatus = path + '/actions';

    webs.get(actionStatus + '/status/get/:actionId/:user_key/:signature', function (req, res) {
        sessionManager.executeMethod(req, res, addons_final, addons, "actionProgressGet");
    });

}
