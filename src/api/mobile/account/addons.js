var async = require('async');
var request = require('request');
var config = require(__base + '/config');
var common = require(__lib + '/common');
var db = require(__lib + '/db_handler');
var ec = require('../../../../lib/elitecore');
var betaUsersManager = require(__core + '/manager/beta/betaUsersManager');
var resourceManager = require('../../../../res/resourceManager');
var portInManager = require('../../../core/manager/porting/portInManager');
var user = require('./user');
var packageManager = require(__core + '/manager/packages/packageManager');
var boostManager = require('../../../core/manager/boost/boostManager');
var transactionManager = require('../../../core/manager/account/transactionManager');
var constants = require('../../../core/constants');
var configManager = require('../../../core/manager/config/configManager');
const actionsQueueService = require(`${global.__queue}/mobileActions`);

var BaseError = require('../../../core/errors/baseError');

var LOG = config.LOGSENABLED;

//exports

exports.uuidCheck = uuidCheck;

exports.actionProgressGet = actionProgressGet;

exports.topupAllGet = topupAllGet;
exports.addBoostHandler = addBoostHandler;
exports.addBoostHandlerInternal = addBoostHandlerInternal;

exports.extraAllGet = extraAllGet;
exports.extraUpdateBatchSet = extraUpdateBatchSet;
exports.extraUpdateBatchSetInternal = extraUpdateBatchSetInternal;

exports.generalAllGet = generalAllGet;
exports.generalUpdateSet = generalUpdateSet;
exports.generalUpdateInternal = generalUpdateInternal;


function actionProgressGet(req, res) {
	ec.getCache(req.params.user_key, false, function (err, value) {
		if (err) {
			res.status(400).json(err);
		} else {
			var actionId = req.params.actionId;

            if (!actionId) {
                return res.status(400).json({
                    code: -1,
                    description: "Invalid params, actionId is missing",
                    details: "Invalid params, actionId is missing"
                });
            }

            actionsQueueService.loadJobProgress(actionId, (err, result) => {
                if (err) {
                    return res.status(400).json({
                        code: -1,
                        description: "Failed to load action status",
                        details: err.message
                    });
                }

                return res.json({
                    code: 0,
                    result: result
                });
            });
		}
	});
}

function topupAllGet(req, res) {
	ec.getCache(req.params.user_key, false, function (err, value) {
		if (err) {
			res.status(400).json(err);
		} else {
			var reply = new Array;
			var packages = ec.packages();
			var payment = config.POSTPONED_BOOST_NUMBERS && config.POSTPONED_BOOST_NUMBERS.indexOf(value.number) >= 0
				? "postponed" : "immediate";

			loadPromoBoostFromCMS(value.prefix, value.number, function (error, list) {
				var freeBoostMap = {};
				if (list) {
					list.forEach(function (item) {
						if (freeBoostMap[item.product_kb]) {
							freeBoostMap[item.product_kb].ids.push({id: item.id});
						} else {
							freeBoostMap[item.product_kb] = {
								ids: [{id: item.id}]
							}
						}
					});
				}

				packages.boost.forEach(function (boost) {
					if (boost.app === 'manual') {
						var obj = new Object;
						obj.type = "data";
						obj.unit = "kilobyte";
						obj.recurrent = false;
						obj.payment = payment;
						obj.id = boost.id;
						obj.value = boost.kb;
						obj.price = {
							"prefix": "$",
							"value": boost.price,
							"postfix": "SGD"
						};
						obj.boost_image_url = constants.BOOSTS_IMAGE_URLS[boost.name];

						if (freeBoostMap[boost.kb]) {
							obj.free = {
								list: freeBoostMap[boost.kb].ids
							}
						}

						obj.order_id = parseInt(boost.order_id);
						reply.push(obj);
					}
				});
				res.json(reply);
			});
		}
	});
}

function loadPromoBoostFromCMS(prefix, number, callback) {
	var key = config.NOTIFICATION_KEY;
	var url = "http://" + config.CMSHOST + ":" + config.CMSPORT + "/api/1/boost/free/available/" + prefix + "/" + number + "/" + key;

	request({
		uri: url,
		method: 'GET',
		timeout: 20000
	}, function (error, response, body) {
		if (error) {
			if (LOG) common.log("loadPromoBoostFromCMS", body)
			callback(error);
		} else {
			var reply = common.safeParse(body);
			if (reply.code == 0 && reply.items) {
				callback(undefined,  reply.items);
			} else {
				var error = new Error("Can not load promo boosts");
				error.status = body.status;
				callback(error);
			}
		}
	});
}

function extraAllGet(req, res) {
	ec.getCache(req.params.user_key, false, function(err, value) {
		if (err) {
			res.status(400).json(err);
		} else {
			var reply = new Object;
			reply.data = new Array;
			reply.sms = new Array;
			reply.calls = new Array;
			var packages = ec.packages();
			packages.base.forEach( function (base) {
				if (value.base && value.base.base_id == base.base_id ) {
					if (base.data) base.data.forEach(function (data) {
						if (data.enabled) {
							var obj = new Object;
							obj.type = "data";
							obj.unit = "kilobyte";
							obj.recurrent = true;
							obj.payment = "postponed";
							obj.id = data.id;
							obj.value = data.kb;
							obj.price = { "prefix" : "$", "value" : parseFloat(data.price), "postfix" : "SGD"};
							reply.data.push(obj);
						}
					});
					if (base.sms) base.sms.forEach(function (sms) {
						if (sms.enabled) {
							var obj = new Object;
							obj.type = "sms";
							obj.unit = "sms";
							obj.recurrent = true;
							obj.payment = "postponed";
							obj.id = sms.id;
							obj.value = sms.sms;
							obj.price = { "prefix" : "$", "value" : parseFloat(sms.price), "postfix" : "SGD"};
							reply.sms.push(obj);
						}
					});
					if (base.voice) base.voice.forEach(function (voice) {
						if (voice.enabled) {
							var obj = new Object;
							obj.type = "calls";
							obj.unit = "seconds";
							obj.recurrent = true;
							obj.payment = "postponed";
							obj.id = voice.id;
							obj.value = voice.mins * 60;
							obj.price = { "prefix" : "$", "value" : parseFloat(voice.price), "postfix" : "SGD"};
							reply.calls.push(obj);
						}
					});
				}
			});
			reply.data_visible = true;
			reply.calls_visible = true;
			reply.sms_visible = true;

			res.json(reply);
		}
	});
}

function generalAllGet(req, res) {
	var packages = ec.packages();
	ec.getCache(req.params.user_key, false, function (err, cache) {	// remove this
		if (err || !cache) {
			res.status(400).json(err ? err : {error: "Empty cache"});
			return;
		}

		var processAddon = function (tasks, item) {

			//CMS-343 TODO include in GUI, retrieve from rules
			var unsubscribe_package = '';
			var blocked_by_package = '';
			var blocked_by_package_message = '';

			if (cache.serviceInstanceUnlimitedData.enabled === true &&
				item.id === constants.PAID_2020_ADDON_ID) {
				blocked_by_package = constants.DFF_ADDON_ID;
				blocked_by_package_message = 'Enjoy your unlimited data!';
			}

			if(item.id === constants.DFF_ADDON_ID) {
				unsubscribe_package = constants.PAID_2020_ADDON_ID;
			}
			//CMS-343 end

			tasks.push(function (callback) {
				var obj = {};
				obj.title = item.title;
				obj.subtitle = item.subtitle;
				obj.description_short = item.description_short;
				obj.description_full = item.description_full.replace(/\\n/g, "\n");	// watch out! not ideal
				obj.disclaimer = item.disclaimer;
				obj.id = item.id;
				obj.advanced_payment = item.advanced_payment;
				obj.free_package = item.free_package;
				obj.beta = item.beta;
				obj.betaGroup = item.betaGroup;

				obj.price = {
					"prefix": "$",
					"value": item.price,
					"postfix": "SGD",
					"discount": parseInt(item.discount_price)
				};

				obj.payment = "postponed";
				obj.action_available = (item.action > 0) ? true : false;
				obj.recurrent = item.recurrent;
				obj.sub_effect = ec.effective(item.sub_effect);
				obj.unsub_effect = ec.effective(item.unsub_effect);
				obj.order_id = parseInt(item.order_id);
				
				//CMS-343 TODO include in GUI, retrieve from rules
				obj.blocked_by_package = blocked_by_package;
				obj.blocked_by_package_message = blocked_by_package_message;
				obj.unsubscribe_package = unsubscribe_package;
				//CMS-343 end


				if ( (new Date().getDate() > 25) &&
						(item.price > 5) ){
					obj.remind_user_nonprorated_charge = true;
				}

				if (obj.beta) {
					betaUsersManager.isBetaCustomer(cache.number, obj.betaGroup, function (err, visible) {
						callback(undefined, {addon: visible ? obj : undefined});
					});
				} else {
					callback(undefined, {addon: obj});
				}
			});
		};

		var tasks = [];
		packages.general.forEach(function (item) {
			if ((item.enabled == 1) && (user.GENTWO_APP.indexOf(item.app) == -1)) {
				if (cache.base.id === constants.CIRCLES_SWITCH_PLAN_ID
					&& constants.CIRCLES_SWITCH_ADDON_WHITELIST.indexOf(item.id) == -1) {
						return;
					} else {
						processAddon(tasks, item);
					}
			}
		});

		async.parallel(tasks, function (err, result) {
			var reply = [];
			var roamingGroup = common.deepCopy(user.ROAMING_GROUP);
			roamingGroup.list.push(common.deepCopy(user.ROAMING_OFF));
			reply.push(roamingGroup);

			result.forEach(function(item) {
				if (item.addon) {
					if (ec.ROAMING_ADDONS.indexOf(item.addon.id) > -1) {
						if ( user.TRIALSIM.indexOf(cache.number) == -1 ) {
							if (item.addon.order_id > 0) {
								roamingGroup.order_id = item.addon.order_id;
							}
							roamingGroup.list.push(item.addon);
						}
					} else if (item.addon.id != "PRD00315") {
						reply.push(item.addon);
					}
				}
			});

			res.json(reply);
		});
	});
}

function uuidCheck(res, user_key, uuid, callback) {
	db.cache_hget("uuid_cache", user_key + "_" + uuid, function(cache) {
		if (cache && cache.reply) {				// already sent reply
			res.status(parseInt(cache.status)).json(JSON.parse(cache.reply));
		} else if (cache && cache.status == "0") {		// previous request received
			var count = 0;
			var processing = setInterval(function () {
				db.cache_hget("uuid_cache", user_key + "_" + uuid, function(cache) {
					if (cache && cache.reply && res && !res.headersSent) {
						res.status(parseInt(cache.status)).json(JSON.parse(cache.reply));
						clearInterval(processing);
					}
					count++;
					if (count>5) clearInterval(processing);
				});
			}, 2000);
		} else {						// first request received
			db.cache_hput("uuid_cache", user_key + "_" + uuid, { "status" : "0" }, 120000);
			callback();
		}
	});
}

function addBoostHandler(req, res) {
    var checkMonkey = true;
    var uuid = (req.fields.uuid) ? req.fields.uuid : "addBoostHandler";

    uuidCheck(res, req.params.user_key, uuid, function () {

        if (LOG) common.log("boost", "loading, user_key=" + req.params.user_key);
        ec.getCache(req.params.user_key, false, function (err, value) {
            if (err) {
                return res.status(400).json({
                    code: -1,
                    description: "Oops, failed to add Boost, please try again later...",
                    details: err.message
                });
            }

            if (value.serviceInstanceBasePlanName == 'CirclesSwitch') {
                return configManager.loadConfigMapForKey("selfcare", "circles_switch", function (err, item) {
                    var upgradeEndDate = !err && item.options.upgrade_end_date ? new Date(item.options.upgrade_end_date) : undefined;
                    var canUpgrade = !upgradeEndDate || upgradeEndDate.getTime() > new Date().getTime();
                    portInManager.loadActiveRequestByServiceInstanceNumber(value.serviceInstanceNumber, {}, (err, result) => {
                        var modificationErr = new BaseError('Modifications are not allowed', canUpgrade && (!result || !result.scheduled)
                            ? 'ERROR_CIRCLES_SWITCH_UPGRADE_REQUIRED' : 'ERROR_CIRCLES_SWITCH_UPGRADE_NOT_ALLOWED');
                        var result = resourceManager.getErrorValues(modificationErr);

                        db.cache_hput("uuid_cache", req.params.user_key + "_" + uuid, {
                            status: 400,
                            reply: JSON.stringify(result)
                        }, 120000);

                        res.status(400).json(result);
                    });
                });
            }

            var saveResult = function (err, result, duration) {
                var status = err ? 400 : 200;
                var jsonResult = err ? resourceManager.getErrorValues(err) : {code: 0, result: result};
                res.status(status).json(jsonResult);

                db.cache_hput("uuid_cache", req.params.user_key + "_" + uuid, {
                    status: status,
                    reply: JSON.stringify(jsonResult)
                }, duration > 0 ? duration : 120000);

                db.paas_put({
                    req: {
                        fields: req.fields,
                        user_key: req.params.user_key
                    },
                    result: jsonResult
                }, value.prefix, value.number);
            }

            var serviceInstanceNumber = value.serviceInstanceNumber;
			var background = req.fields.background == 'true' || req.fields.background == '1';
			var delay = req.fields.delay ? parseInt(req.fields.delay) : 0;
			var productId = req.fields.id;
            var payment = req.fields.payment;
            var freeId = req.fields.free_id ? parseInt(req.fields.free_id) : 0;

            var params = {
                serviceInstanceNumber,
                productId,
                payment,
                freeId
            };

            if (background) {
                var jobType = constants.MOBILE_ACTIONS.JOB_TYPE_BOOST;
                var actionTypeType = constants.MOBILE_ACTIONS.ACTION_TYPE_TYPE_BOOST;
				actionsQueueService.createNewUniqueJob(serviceInstanceNumber, jobType,
					actionTypeType, params, {delay: delay}, (err, result) => {
						saveResult(err, result);
					});
            } else {
                if (checkMonkey) {
                    db.monkey_test("cache", "monkey_" + value.account, function (monkey) {
                        if (monkey) {
                            var reply = new Object();
                            reply.code = common.MONKEY_ERROR;
                            reply.title = "Pause!"
                            reply.description = "Sorry, auto pause is on for safety reason. But you can try making changes again, in a short while";
                            reply.details = "Monkey limit";
                            reply.monkey = monkey;
                            res.status(400).json(reply);
                        } else {
                            addBoostHandlerInternal(params, (err) => {
                                saveResult(err);
                            });
                        }
                    });
                } else {
                    addBoostHandlerInternal(params, (err) => {
                        saveResult(err);
                    });
                }
            }
        });
    });
}

function addBoostHandlerInternal(params, callback) {
    if (!callback) callback = () => {
    }
    if (!params) params = {};

    var serviceInstanceNumber = params.serviceInstanceNumber;
    var productId = params.productId;
    var freeId = params.freeId;

    if (!serviceInstanceNumber || (!freeId && !productId)) {
        common.error("MobileAddonsManager", "addBoostHandlerInternal: invalid params");
        return callback(new BaseError("Invalid params", "ERROR_INVALID_PARAMS"));
    }

    var product = ec.findPackage(ec.packages(), productId);
    var name = product.name;
    var amount = product.price;

    if (LOG) common.log("MobileActionManager", "addBoostHandlerInternal: sin=" + serviceInstanceNumber);
    ec.getCustomerDetailsServiceInstance(serviceInstanceNumber, false, function (err, cache) {
        if (freeId > 0) {
            boostManager.useFreeBoost(cache.prefix, cache.number, freeId, (err, status) => {
                if (err) {
                    common.error("MobileAddonsManager", "addBoostHandlerInternal: failed to use free boost, error=" + err.message);
                    return callback(err);
                }

                return callback();
            });
        } else {
            if (params.payment === "postponed") {
                ec.addonSubscribe(cache.number, cache.serviceInstanceNumber, [{
                    product_id: productId,
                    recurrent: false,
                    effect: 0
                }], cache.billing_cycle, function (err, result) {
                    callback(err);
                });
            } else {
                var description = (!name ? "Payment" : "Payment for " + name);
                transactionManager.performPayment(cache.billingAccountNumber, {
                    type: transactionManager.TYPE_BOOST_PAYMENT,
                    amount: amount,
                    internalId: productId,
                    metadata: {
                        actions: [
                            transactionManager.createAdvancedPaymentAction(),
                            transactionManager.createAddonSubAction(description, productId)
                        ]
                    }
                }, (status, transaction, callback) => {
                    if (LOG) common.log("MobileAddonsManager", "addBoostHandlerInternal: transaction status " +
                    "changed, account=" + cache.account + ", status=" + status);
                    callback();
                }, (err, result) => {
                    if (err) {
                        return callback(err);
                    }
                    if (result && result.validPaymentError) {
                        return callback(result.validPaymentError);
                    }

                    callback();
                });
            }
        }
    });
}

function extraBatch(unsub, sub, cache, callback) {
	if ( unsub && (unsub.length > 0) ) {
        let uniqueUnsubAddons = common.uniqify(unsub, ['product_id', 'effect', 'current']);
        if (uniqueUnsubAddons.length < 1) {
            uniqueUnsubAddons = unsub
        }
		ec.addonUnsubscribe(cache.number, cache.serviceInstanceNumber, uniqueUnsubAddons, cache.billing_cycle, function(error, response) {
			if ( !error && response && (sub.length > 0) ) {			// more packages to subscribe
				ec.addonSubscribe(cache.number, cache.serviceInstanceNumber, sub, cache.billing_cycle, function(err, result) {
					if (!err && result) {
						callback(false, result);
					} else {
						common.error("extraBatch", JSON.stringify(result));
						callback(true, result);
					}
				});
			} else if ( !error && response && (sub.length == 0) ) {		// no more packages to subscribe
				callback(false, response);
			} else {
				common.error("extraBatch", JSON.stringify(response));
				callback(true, response);
			}
		});
	} else {
		ec.addonSubscribe(cache.number, cache.serviceInstanceNumber, sub, cache.billing_cycle, function(err, result) {
			if (!err && result) {
				callback(false, result);
			} else {
				callback(true, err);
			}
		});
	}
}

function extraUpdateBatchSet(req, res) {
	if (LOG) common.log("addons", "extraUpdateBatchSet: " + JSON.stringify(req.fields.extra));
	var uuid = (req.fields.uuid) ? req.fields.uuid : "extraUpdateBatchSet";
	uuidCheck(res, req.params.user_key, uuid, function() {
		ec.getCache(req.params.user_key, true, function(err, value) {
			if (err) {
				res.status(400).json(err);
			} else {

                if (value.serviceInstanceBasePlanName == 'CirclesSwitch') {
                    return configManager.loadConfigMapForKey("selfcare", "circles_switch", function (err, item) {
                        var upgradeEndDate = !err && item.options.upgrade_end_date  ? new Date(item.options.upgrade_end_date) : undefined;
                        var canUpgrade = !upgradeEndDate || upgradeEndDate.getTime() > new Date().getTime();
						portInManager.loadActiveRequestByServiceInstanceNumber(value.serviceInstanceNumber, {}, (err, result) => {
							var modificationErr = new BaseError('Modifications are not allowed', canUpgrade && (!result || !result.scheduled)
								? 'ERROR_CIRCLES_SWITCH_UPGRADE_REQUIRED' : 'ERROR_CIRCLES_SWITCH_UPGRADE_NOT_ALLOWED');
							var result = resourceManager.getErrorValues(modificationErr);

							db.cache_hput("uuid_cache", req.params.user_key + "_" + uuid, {
								status: 400,
								reply: JSON.stringify(result)
							}, 120000);
							res.status(400).json(result);
						});
                    });
                }

				var saveResult = (err, result, duration) => {
					var status = err ? 400 : 200;
					var jsonResult = err ? resourceManager.getErrorValues(err) : {code: 0, result: result};
					res.status(status).json(jsonResult);

					db.cache_hput("uuid_cache", req.params.user_key + "_" + uuid, {
						status: status,
						reply: JSON.stringify(jsonResult)
					}, duration > 0 ? duration : 120000);
				}

                var serviceInstanceNumber = value.serviceInstanceNumber;
                var background = req.fields.background == 'true' || req.fields.background == '1';
                var delay = req.fields.delay ? parseInt(req.fields.delay) : 0;

                var params = {
                    serviceInstanceNumber: serviceInstanceNumber,
                    extra: req.fields.extra,
                    userKey: req.params.user_key
                }

                if (background) {
                    var jobType = constants.MOBILE_ACTIONS.JOB_TYPE_PLAN_MODIFICATION;
                    var actionTypeType = constants.MOBILE_ACTIONS.ACTION_TYPE_TYPE_PLAN;
                    actionsQueueService.createNewUniqueJob(serviceInstanceNumber, jobType,
                        actionTypeType, params, {delay: delay}, (err, result) => {
                            saveResult(err, result);
                        });
                } else {
                    db.monkey_test("cache", "monkey_" + value.account, function (monkey) {
                        if (monkey) {
                            var reply = {};
                            reply.code = common.MONKEY_ERROR;
                            reply.title = "Pause!"
                            reply.description = "Sorry, auto pause is on for safety reason. But you can try making changes again, in a short while";
                            reply.details = "Monkey limit";
                            reply.monkey = monkey;
                            return res.status(400).json(reply);
                        }

                        extraUpdateBatchSetInternal(params, (err, result) => {
                            saveResult(err, result);
                        });
                    });
                }
			}
		});
	});
}

function extraUpdateBatchSetInternal(params, callback) {
	if (!callback) callback = () => {
	}
	if (!params) params = {};

	var serviceInstanceNumber = params.serviceInstanceNumber;
	var extra = params.extra;

	if (!serviceInstanceNumber || !extra) {
		common.error("MobileAddonsManager", "extraUpdateBatchSetInternal: invalid params");
		return callback(new BaseError("Invalid params", "ERROR_INVALID_PARAMS"));
	}

	if (LOG) common.log("MobileActionManager", "extraUpdateBatchSetInternal: sin=" + serviceInstanceNumber);
	ec.getCustomerDetailsServiceInstance(serviceInstanceNumber, false, function (err, value) {
		if (err) {
			return callback(err);
		}
		if (!value) {
			return callback(new BaseError("Customer not found", "ERROR_CUSTOMER_NOT_FOUND"));
		}

		var unsub = [];
		var sub = [];

		extra.forEach(function (extra) {
			var current = (extra.type == "data")
				? value.extra_current.data : (extra.type == "sms")
				? value.extra_current.sms : value.extra_current.voice;
			var future = ( extra.type == "data" )
				? value.extra.data : (extra.type == "sms")
				? value.extra.sms : value.extra.voice;
			var unsubCurrent = current && current.packageHistoryId && (!future || (current && future && (current.id == future.id)))
				? true : false;

			if (current && current.packageHistoryId) {
				unsub.push({
					product_id: current.id,
					packageHistoryId: current.packageHistoryId,
					effect: current.unsub_effect,
					current: unsubCurrent
				});
			} else if (future && future.packageHistoryId) {
				unsub.push({
					product_id: future.id,
					packageHistoryId: future.packageHistoryId,
					effect: future.unsub_effect,
					current: unsubCurrent
				});
			}

			if (extra.action == "subscribe") {
				var product;
				ec.packages().base.forEach(function (base) {
					var found = ec.findPackage(base, extra.id);
					if (found) product = found;
				});

				if (product) sub.push({
					product_id: product.id,
					recurrent: product.recurrent,
					effect: product.sub_effect
				});
			}
		});

		extraBatch(unsub, sub, value, function (err, result) {
			ec.setCache(value.number, value.prefix, params.userKey, value.account, function (error, obj) {
				if (err) {
					return callback(err);
				}
				return callback(undefined, result);
			});
		});
	});
}

function generalUpdateSet(req, res) {
    if (LOG) common.log("AddonManager", "generalUpdateSet: id=" + req.fields.id + ", action=" + req.fields.action);

    var uuid = (req.fields.uuid) ? req.fields.uuid : "generalUpdateSet";
    uuidCheck(res, req.params.user_key, uuid, function () {
        ec.getCache(req.params.user_key, true, function (err, cache) {
            if (err) {
                return res.status(400).json(err);
            }

            if (cache.serviceInstanceBasePlanName == 'CirclesSwitch') {
                return configManager.loadConfigMapForKey("selfcare", "circles_switch", function (err, item) {
                    var upgradeEndDate = !err && item.options.upgrade_end_date  ? new Date(item.options.upgrade_end_date) : undefined;
                    var canUpgrade = !upgradeEndDate || upgradeEndDate.getTime() > new Date().getTime();
					portInManager.loadActiveRequestByServiceInstanceNumber(cache.serviceInstanceNumber, {}, (err, result) => {
						var modificationErr = new BaseError('Modifications are not allowed', canUpgrade && (!result || !result.scheduled)
							? 'ERROR_CIRCLES_SWITCH_UPGRADE_REQUIRED' : 'ERROR_CIRCLES_SWITCH_UPGRADE_NOT_ALLOWED');
						var result = resourceManager.getErrorValues(modificationErr);

						db.cache_hput("uuid_cache", req.params.user_key + "_" + uuid, {
							status: 400,
							reply: JSON.stringify(result)
						}, 120000);
						res.status(400).json(result);
					});
                });
            }

            if (LOG) common.log("generalUpdateSet " + cache.serviceInstanceNumber, req.fields.id);

            var saveResult = (err, result, duration) => {
                var status = err ? 400 : 200;
                var jsonResult = err ? resourceManager.getErrorValues(err) : {code: 0, result: result};
                res.status(status).json(jsonResult);

                db.cache_hput("uuid_cache", req.params.user_key + "_" + uuid, {
                    status: status,
                    reply: JSON.stringify(jsonResult)
                }, duration > 0 ? duration : 120000);
            }

            var serviceInstanceNumber = cache.serviceInstanceNumber;
            var background = req.fields.background == 'true' || req.fields.background == '1';
            var delay = req.fields.delay ? parseInt(req.fields.delay) : 0;

            var params = {
                serviceInstanceNumber: serviceInstanceNumber,
                action: req.fields.action,
                productId: req.fields.id
            }

            if (background) {
                var jobType = constants.MOBILE_ACTIONS.JOB_TYPE_PLUS_MODIFICATION;
                var actionTypeType = constants.MOBILE_ACTIONS.ACTION_TYPE_TYPE_PLUS;
                actionsQueueService.createNewUniqueJob(serviceInstanceNumber, jobType,
                    actionTypeType, params, {delay: delay}, (err, result) => {
                        saveResult(err, result);
                    });
            } else {
                db.monkey_test("cache", "monkey_" + cache.account, function (monkey) {
                    if (monkey) {
                        var reply = {};
                        reply.code = common.MONKEY_ERROR;
                        reply.title = "Pause!"
                        reply.description = "Sorry, auto pause is on for safety reason. But you can try making changes again, in a short while";
                        reply.details = "Monkey limit";
                        reply.monkey = monkey;
                        return res.status(400).json(reply);
                    }

                    generalUpdateInternal(params, (err, result) => {
                        saveResult(err, result);
                    });
                });
            }
        });
    });
}

function generalUpdateInternal(params, callback) {
    if (!callback) callback = () => {
    }
    if (!params) params = {};

    var serviceInstanceNumber = params.serviceInstanceNumber;
    var productId = params.productId;
    var action = params.action;

    if (!serviceInstanceNumber || !productId) {
        common.error("MobileAddonsManager", "generalUpdateInternal: invalid params");
        return callback(new BaseError("Invalid params", "ERROR_INVALID_PARAMS"));
    }

    if (LOG) common.log("MobileActionManager", "generalUpdateInternal: sin=" + serviceInstanceNumber);
    ec.getCustomerDetailsServiceInstance(serviceInstanceNumber, false, function (err, cache) {

        var historyIds = packageManager.getCustomerGeneralHistoryIds(productId, cache);
        packageManager.addonUpdate(cache.prefix, cache.number, action, productId, historyIds, false, function (err, result) {
            if (err) {
                common.error("MobileAddonsManager", "generalUpdateInternal: failed to subscribe addon, error=" + err.message);
                return callback(err);
            }
            if (!result) {
                common.error("MobileAddonsManager", "generalUpdateInternal: empty result");
                return callback(new BaseError("Subscription result empty", "ERROR_EMPTY_SUBSCRIPTION_RESULT"));
            }
            return callback(undefined, result)
        });
    });
}

