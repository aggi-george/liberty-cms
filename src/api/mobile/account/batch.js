var async = require('async');
var request = require('request');
var config = require(__base + '/config');
var common = require(__lib + '/common');
var ec = require(__lib + '/elitecore');
var deactivate = require('../registration/deactivate');

var logsEnabled = config.LOGSENABLED;

//exports

exports.api = api;

//functions

function sendRequest(req, user_key, ip, callback) {
	var URL = (req.secure) ? "https://" + config.MYSELF + ":" + config.MOBILESPORT + "/api/2/"
	+ req.url : "http://" + config.MYSELF + ":" + config.MOBILEPORT + "/api/2/" + req.url;
	URL += user_key + "/" + config.MYSIG;
	var apiParams = { "form" : { }, "rejectUnauthorized" : false, "headers" : { "X-Forwarded-For" : ip } };
	apiParams.form = ( req.method == "get" ) ? req.url_params : req.params;
	apiParams.timeout = 20000;
	var start = new Date();
	request[req.method](URL, apiParams, function(err, response, body) {
		//if (logsEnabled) common.log("Batch API", "URL = " + URL + "   err = " + JSON.stringify(err) + "   body = " + JSON.stringify(body));

		if (!err && body) {
			var parsed = common.safeParse(body);
			if ( typeof(parsed) == "object" ) {
				var reply = new Object;
				reply.result = parsed;
				reply.key = req.key;
				reply.query_time = new Date() - start;
				reply.http_status = response.statusCode;
				callback(undefined, reply);
			} else {
				callback({ "code" : -1, "err" : parsed }, undefined);
			}
		} else {
			callback({ "code" : -1, "err" : err }, undefined);
		}
	});
}

function userCache(user_key, fakenumber, callback) {
	if (!fakenumber) {
	        ec.getCache(user_key, false, function(err, cache) {
			if ( err && (err.message == "Empty service account details") ) {
				common.error("userCache", "Deactivating user because EC returns empty " + user_key);
				deactivate.setStatus(user_key, function() {});
			}
			callback(err);
		});
	} else {
		callback(false);
	}
}

function api(req, res) {
	userCache(req.params.user_key, req.fakenumber, function(err) {
		if (err) return res.status(400).json({ "code" : common.PARSE_ERROR, "description" : "Batch Error" });
		var tasks = new Array;
		var user_key = req.params.user_key;
		req.fields.requests.forEach(function (reqitem) {
			tasks.push(function (callback) {
				sendRequest(reqitem, user_key, req.ip, function(err, result) {
					if (err) {
						callback({ "url" : reqitem.url, "error" : err }, result);
					} else {
						callback(err, result);
					}
				});
			});
		});
		async.parallel(tasks, function(err, results){
			if (err) common.error("Batch API", JSON.stringify(err));
			res.json({ "responses" : results });
		});
	});
}
