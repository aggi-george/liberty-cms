var request = require('request');
var fs = require('fs');
let Q = require('q');
var account = require('./account');
var common = require(__lib + '/common');
var db = require(__lib + '/db_handler');
var ec = require(__lib + '/elitecore');
var config = require(__base + '/config');
var bonusManger = require('../../../core/manager/bonus/bonusManager');

var LOG = config.LOGSENABLED;

//exports

exports.leaderboardGet = leaderboardGet;
exports.bonusHistoryGet = bonusHistoryGet;
exports.bonusAvailableGet = bonusAvailableGet;
exports.bonusAvailableUse = bonusAvailableUse;
exports.getBonusImage = getBonusImage;
exports.bonusAvailed = bonusAvailed;
exports.getDataFromManager = getDataFromManager;

//functions

function leaderboardGet(req, res) {
    ec.getCache(req.params.user_key, false, function (err, value) {
        if (err) {
            res.status(400).json(err);
        } else {
            loadLeaderboardFromCMS(function (err, list, goldenCircle) {
                if (err) {
                    return res.status(400).json(err);
                }

                if (list) list.forEach(function (item) {
                    item.users.forEach(function (user) {
                        if (user.serviceInstanceNumber === value.serviceInstanceNumber) {
                            user.self = true;
                        }
                    });
                });

                if (goldenCircle) goldenCircle.forEach(function (item) {
                    item.users.forEach(function (user) {
                        if (user.serviceInstanceNumber === value.serviceInstanceNumber) {
                            user.self = true;
                        }
                    });
                });

                res.json({leaderboard: {list: list, goldenCircle: goldenCircle}});
            });
        }
    });
}

function loadLeaderboardFromCMS(callback) {
    var key = config.NOTIFICATION_KEY;
    var url = "http://" + config.CMSHOST + ":" + config.CMSPORT
        + "/api/1/bonus/leaderboard/get/" + key;

    request({
        uri: url,
        method: 'GET',
        timeout: 20000
    }, function (error, response, body) {
        if (error) {
            if (LOG) common.error("loadBonusFromCMS", body)
            callback(error);
        } else {
            var reply = common.safeParse(body);
            if (reply.code == 0 && reply.items) {
                callback(undefined, reply.items, reply.goldenCircle);
            } else {
                var error = new Error("Can not load leaderboard");
                error.status = body.status;
                callback(error);
            }
        }
    });
}

function bonusHistoryGet(req, res) {
    ec.getCache(req.params.user_key, false, function (err, value) {
        if (err) {
            res.status(400).json(err);
        } else {
            loadHistoryFromCMS(value.prefix, value.number, function (err, list, rating) {
                if (err) {
                    res.status(400).json(err);
                } else {
                    var reply = {};
                    if (list) reply.history = {list: list};
                    if (rating) reply.rating = rating;
                    res.json(reply);
                }
            });
        }
    });
}

function loadHistoryFromCMS(prefix, number, callback) {
    var key = config.NOTIFICATION_KEY;
    var url = "http://" + config.CMSHOST + ":" + config.CMSPORT
        + "/api/1/bonus/history/get/" + prefix + "/" + number + "/" + key;

    request({
        uri: url,
        method: 'GET',
	timeout: 20000
    }, function (error, response, body) {
        if (error) {
            if (LOG) common.error("loadBonusFromCMS", body)
            callback(error);
        } else {
            var reply = common.safeParse(body);
            if (reply.code == 0 && reply.items) {
                callback(undefined, reply.items, reply.rating);
            } else {
                var error = new Error("Can not load bonus history");
                error.status = body.status;
                callback(error);
            }
        }
    });
}

function bonusAvailableGet(req, res) {
    ec.getCache(req.params.user_key, false, function (err, value) {
        if (err) {
            res.status(400).json(err);
        } else {
            loadBonusFromCMS(value.prefix, value.number, function (err, list) {
                if (err) {
                    res.status(400).json(err);
                } else {
                    var reply = {
                        birthday: {
                            list: []
                        },
                        surprise: {
                            list: []
                        }
                    };
                    if (list) {
                        list.forEach(function (item) {
                            if (item.product_name.indexOf("Birthday") >= 0) {
                                reply.birthday.list.push(item);
                            } else if (item.product_name.indexOf("Surprise") >= 0) {
                                reply.surprise.list.push(item);
                            }
                        });
                    }
                    res.json(reply);
                }
            });
        }
    });
}

function loadBonusFromCMS(prefix, number, callback) {
    var key = config.NOTIFICATION_KEY;
    var url = "http://" + config.CMSHOST + ":" + config.CMSPORT
        + "/api/1/bonus/available/get/" + prefix + "/" + number + "/" + key;

    request({
        uri: url,
        method: 'GET',
        timeout: 20000
    }, function (error, response, body) {
        if (error) {
            if (LOG) common.error("loadBonusFromCMS", error.message)
            callback(error);
        } else {
            var reply = common.safeParse(body);
            if (reply.code == 0 && reply.items) {
                callback(undefined, reply.items);
            } else {
                var error = new Error("Can not load boosts");
                error.status = body.status;
                callback(error);
            }
        }
    });
}

function bonusAvailableUse(req, res) {
    ec.getCache(req.params.user_key, false, function (err, value) {
        if (err) {
            res.status(400).json(err);
        } else {
            var bonusId = req.fields.bonus_id ? parseInt(req.fields.bonus_id) : 0;
            useBonusFromCMS(value.prefix, value.number, bonusId, function (err) {
                if (err) {
                    res.status(400).json(err);
                } else {
                    res.json({code: 0});
                }
            });
        }
    });
}

function useBonusFromCMS(prefix, number, id, callback) {
    var key = config.NOTIFICATION_KEY;
    var url = "http://" + config.CMSHOST + ":" + config.CMSPORT
        + "/api/1/bonus/available/use/" + id + "/" + key;

    request({
        uri: url,
        method: 'PUT',
        timeout: 20000,
        json: {
            prefix: prefix,
            number: number
        }
    }, function (error, response, body) {
        if (error) {
            if (LOG) common.error("useBonusFromCMS", body);
            callback(error);
        } else {
            var reply = common.safeParse(body);
            if (reply.code == -1) {
                if (LOG) common.error("useBonusFromCMS", body);
                callback(new Error(reply.error));
            } else {
                callback();
            }
        }
    });
}

function getBonusImage(req, res) {
    if (req.params.id) {
        try {
            var filePath = __res + '/images/' + req.params.id + ".png";
            var stat = fs.statSync(filePath);

            res.writeHead(200, {
                'Content-Type': 'image/png',
                'Content-Length': stat.size
            });

            var readStream = fs.createReadStream(filePath);
            readStream.pipe(res);
        } catch(err) {
            res.status(400).json({
                code: -1,
                error: err.message
            });
        }
    } else {
        res.status(404).send('Not Found');
    }
}

function bonusAvailed(req, res) {
    var userKey = req.params.user_key;
    return Q.nfcall(ec.getCache, userKey, false)
        .then(getDataFromManager)
        .then(result => {
            return res.json(result);
        })
        .catch(err => {
            return res.status(400).json({
                code: -1,
                status: err.id,
                error: err.message
            });
        });
}

function getDataFromManager({serviceInstanceNumber, activationDate}) {
    if (serviceInstanceNumber && activationDate) {
        if (LOG) common.log("getDataFromManager() => Processing bonus for instance", serviceInstanceNumber);
        return bonusManger.bonusEarnerSoFar(serviceInstanceNumber)
            .then(rows => {
                let resultObj = {
                    "bonus_banner_data": {
                        "joined_date": activationDate,
                        "earned_data":{ 
                            "unit": "gigabyte", 
                            "value": 0, 
                            "type": "data"
                        }
                    }
                };

                if (rows && rows.length) {
                    let totalBonusAvailed = 0;

                    rows.forEach(val => {
                        if (val.added_ts) {
                            let bonusinGb = common.convertToGb(val.bonus_kb_pretty),                    
                                end = common.getLocalTime(val.display_until_ts),
                                start = common.getLocalTime(val.added_ts),
                                currDate = common.getLocalTime(new Date());

                            end = !isNaN(end) ? 
                                end >  currDate ? currDate : end : currDate;

                            let monthsDiff = common.getMonthDiff(start, end) + 1,
                                bonusAvailed = monthsDiff * bonusinGb;   

                            totalBonusAvailed += bonusAvailed;    
                        }
                    });
                    totalBonusAvailed = common.isInt(totalBonusAvailed) ? 
                        totalBonusAvailed : Number(totalBonusAvailed.toFixed(2));

                    resultObj.bonus_banner_data.earned_data.value = totalBonusAvailed;
                }
                return resultObj;
            })
            .catch(err => {
                common.log("getDataFromManager() => Error Occurred", err);
                common.log("getDataFromManager() => for instance", serviceInstanceNumber);
                return {"bonus": "Bonus not available"};
            });   
    }
    common.log("getDataFromManager() => Bonus Not Available");
    return Q({"bonus": "Bonus not available"});
}
