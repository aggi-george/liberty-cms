var config = require(__base + '/config');
var common = require(__lib + '/common');
var ec = require(__lib + '/elitecore');
var db = require(__lib + '/db_handler');
var mobileLogs = require(__core + '/manager/notifications/mobileLogs');

//exports

exports.notificationsHistoryGet = notificationsHistoryGet;
exports.notificationsReadStatusSet = notificationsReadStatusSet;

//functions

function notificationsHistoryGet(req, res) {
    ec.getCache(req.params.user_key, false, function(err, cache) {
        if (err || !cache) {
            res.status(400).json(err);
        } else {
            mobileLogs.getUserNotifications(cache.serviceInstanceNumber, req.query.anchor, function(err, result) {
                if (!err && result) {
                    res.json(result);
                } else {
                    res.status(500).json({ "code" : -1 });
                }
            });
        }
    });
}

function notificationsReadStatusSet(req, res) {
    ec.getCache(req.params.user_key, false, function(err, cache) {
        if (err || !cache) {
            res.status(400).json(err);
        } else {
            mobileLogs.setReadUserNotifications(req.fields.ids, function(err, result) {
                if (!err && result) {
                    res.json({ "code" : 0 });
                } else {
                    res.status(500).json({ "code" : -1 });
                }
            });
        }
    });
}
