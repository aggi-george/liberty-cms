var request = require('request');
var account = require('./account');
var common = require(__lib + '/common');
var db = require(__lib + '/db_handler');
var ec = require(__lib + '/elitecore');
var config = require('../../../../config');
var promotionsManager = require('../../../core/manager/promotions/promotionsManager');
var locationManager = require('../../../core/manager/promotions/locationManager');
var dbsManager = require('../../../core/manager/partners/dbs');

var LOG = config.LOGSENABLED;
const limitedDistance = 50;
//exports

exports.promotionCodeSet = promotionCodeSet;
exports.locationSet = locationSet;
exports.locationsGet = locationsGet;
exports.locationPromoDialogGet = locationPromoDialogGet;
exports.locationPromoSet = locationPromoSet;
exports.getNearbyBranch = getNearbyBranch;
exports.getPaymentUrl = getPaymentUrl;

//functions

function loadPromoCodeFromCMS(code, prefix, number, callback) {
    var key = config.NOTIFICATION_KEY;
    var url = "http://" + config.CMSHOST + ":" + config.CMSPORT + "/api/1/promotions/use/promocode/" + code + "/" + key;

    request({
        uri: url,
        method: 'PUT',
        json: {
            "prefix": prefix,
            "number": number,
            "platform": "APP"
        }
    }, function (error, response, body) {
        if (error) {
            if (LOG) common.log("loadPromoCodeFromCMS", body)
            callback(error);
        } else {
            var reply = common.safeParse(body);
            if (reply.code == 0) {
                callback(undefined,
                    true,
                    reply.result ? reply.result.title : "",
                    reply.result ? reply.result.message : "",
                    reply.result ? reply.result.images : "");
            } else {
                var error = new Error("Invalid promo code");
                error.status = body.status;
                callback(error);
            }
        }
    });
}

function promotionCodeSet(req, res) {
    var uuid = (req.fields.uuid) ? req.fields.uuid : "promotionCodeSet";
    var code = req.fields.code;
    if (LOG) common.log("promotionCodeSet", "code=" + code + ", uuid=" + uuid);

    db.uuidCheck2(res, req.params.user_key, uuid, function () {
        if (LOG) common.log("promotionCodeSet", "load user info, user_key=" + req.params.user_key);
        ec.getCache(req.params.user_key, false, function (err, cache) {
            var account = cache.account;
            var serviceInstanceNumber = cache.serviceInstanceNumber;
            if (err) {
                res.status(400).json(err);
            } else {
                if (LOG) common.log("promotionCodeSet", " number=" + serviceInstanceNumber + ", code=" + code);
                db.monkey_test("cache", "monkey_" + account, function (monkey) {
                    if (monkey) {
                        var reply = {
                            code: common.MONKEY_ERROR,
                            title: "Pause!",
                            description: "Sorry, auto pause is on for safety reason. " +
                            "But you can try making changes again, in a short while",
                            details: "Monkey limit",
                            monkey: monkey
                        }
                        res.status(400).json(reply);
                    } else {

                        promotionsManager.useCode("APP", cache.prefix, cache.number, code, cache.orderReferenceNumber,
                            config.NOTIFICATION_KEY, function (err, result) {
                                var reply;
                                var status;
                                if (!err) {
                                    if (LOG) common.log("promotionCodeSet", "success, title=" + result.title +
                                    ", message=" + result.message + ", images=" + JSON.stringify(result.images));

                                    status = 200;
                                    reply = {
                                        code: 0,
                                        codeValid: true,
                                        title: result.title,
                                        subtitle: code,
                                        message: result.message,
                                        images: result.images
                                    };
                                    res.json(reply);

                                } else {
                                    if (LOG) common.error("promotionCodeSet", "error, message=" + err.message +
                                    ", status=" + err.status);

                                    status = 400;
                                    if (err.status === "ERROR_CATEGORY_USED") {
                                        reply = {
                                            code: common.PROMO_CODE_USED,
                                            details: "One code per category",
                                            description: "You've already used a promo code from the same category."
                                        };
                                    } else if (err.status === "ERROR_CODE_USED") {
                                        reply = {
                                            code: common.PROMO_CODE_USED,
                                            details: "Promotion code has been used",
                                            description: "You've already used entered promo code(" + code +
                                            ")."
                                        };
                                    } else if (err.status === "ERROR_ALREADY_HAS_THIRD_PARTY_BONUS") {
                                        reply = {
                                            code: common.PROMO_ALREADY_HAS_THIRD_PARTY_BONUS,
                                            details: err.message,
                                            description: "Unfortunately, you are not entitled for that promotion. " +
                                            "You may already have one 'forever' bonus from any of our other campaigns."
                                        };
                                    } else {
                                        reply = {
                                            code: common.PROMO_CODE_EXPIRED_OR_INVALID,
                                            details: "Promotion code is expired",
                                            description: "The promo code you entered (" + code +
                                            ") is expired or invalid. Please try to enter it again."
                                        };
                                    }
                                    res.status(status).json(reply);
                                }

                                db.cache_hput("uuid_cache", req.params.user_key + "_" + uuid, {
                                    status: status,
                                    reply: JSON.stringify(reply)
                                }, 120000);
                            });
                    }
                });
            }
        });
    });
}

function locationSet(req, res) {
    var uuid = req.fields.uuid ? req.fields.uuid : "locationSet";
    var locationId = req.fields.locationId ? parseInt(req.fields.locationId) : 0;
    var latitude = req.fields.latitude ? parseFloat(req.fields.latitude) : 0;
    var longitude = req.fields.longitude ? parseFloat(req.fields.longitude) : 0;
    var radius = req.fields.radius ? parseFloat(req.fields.radius) : 0;
    if (LOG) common.log("MobilePromotionManager", "locationSet: locationId=" + locationId + ", user_key=" + req.params.user_key);

    db.uuidCheck2(res, req.params.user_key, uuid, function () {
        ec.getCache(req.params.user_key, false, function (err, cache) {
            if (err) {
                return res.status(400).json({
                    code: -1,
                    details: err.message,
                    description: "Failed to load customer customer location"
                });
            }
            if (!cache) {
                return res.status(400).json({
                    code: -1,
                    details: "Customer not found",
                    description: "Customer not found"
                });
            }

            locationManager.saveCustomerLocation(locationId, cache.serviceInstanceNumber, {
                latitude: latitude,
                longitude: longitude,
                radius: radius
            }, (err, result) => {
                var status;
                var reply;

                if (err) {
                    status = 400;
                    reply = {
                        code: -1,
                        details: err.message,
                        description: "Failed to save customer location"
                    }
                } else {
                    status = 200;
                    reply = {code: 0}
                }

                db.cache_hput("uuid_cache", req.params.user_key + "_" + uuid, {
                    status: status,
                    reply: JSON.stringify(reply)
                }, 120000);

                res.status(status).json(reply);
            });
        });
    });
}

function locationsGet(req, res) {
    var uuid = req.fields.uuid ? req.fields.uuid : "locationsGet";
    var latitude = req.fields.latitude ? parseFloat(req.fields.latitude) : 0;
    var longitude = req.fields.longitude ? parseFloat(req.fields.longitude) : 0;
    var radius = req.fields.radius ? parseFloat(req.fields.radius) : 0;
    var count = req.fields.radius ? parseInt(req.fields.count) : 0;
    if (LOG) common.log("MobilePromotionManager", "locationsGet: latitude=" + latitude +
    ", longitude=" + longitude + ", radius=" + radius + ", count=" + count + " user_key=" + req.params.user_key);

    locationManager.loadNearByPoints(latitude, longitude, radius, count, (err, result) => {
        if (err) {
            return res.status(400).json({
                code: -1,
                details: "Customer not found",
                description: "Customer not found"
            });
        }

        res.json({
            code: 0,
            list: result.data
        });
    });
}

function locationPromoDialogGet(req, res) {
    var uuid = req.fields.uuid ? req.fields.uuid : "locationsGet";
    var promoId = req.fields.promoId ? parseInt(req.fields.promoId) : 0;
    if (LOG) common.log("MobilePromotionManager", "locationPromoDialogGet: promoId=" + promoId + ", user_key=" + req.params.user_key);

    locationManager.loadRedemptionDialog(promoId, (err, result) => {
        if (err) {
            if (err.status == 'ERROR_REDEMPTION_DISABLED') {
                return res.status(400).json({
                    code: common.LOCATION_PROMO_DIALOG_DISABLED,
                    details: err.message,
                    description: "Unfortunately, current promo is not available anymore."
                });
            } else {
                return res.status(400).json({
                    code: common.LOCATION_PROMO_DIALOG_FAILED,
                    details: err.message,
                    description: "Oops, failed to load promo details. Please try again."
                });
            }
        }

        result.providerId = result.providerId;
        result.instruction = 'Ask your barista for redemption code.\nThis offer can only be redeemed once';
        result.redeemedMessage = 'Redemption successful.\nEnjoy your coffee!';

        res.json({
            code: 0,
            dialog: result
        });
    });
}

function locationPromoSet(req, res) {
    var uuid = req.fields.uuid ? req.fields.uuid : "locationPromoSet";
    var promoId = req.fields.promoId ? parseInt(req.fields.promoId) : 0;
    var code = req.fields.code ? req.fields.code : 0;
    if (LOG) common.log("MobilePromotionManager", "locationPromoSet: promoId=" + promoId + ", user_key=" + req.params.user_key);

    db.uuidCheck2(res, req.params.user_key, uuid, function () {
        ec.getCache(req.params.user_key, false, function (err, cache) {
            if (err) {
                return res.status(400).json({
                    code: -1,
                    details: err.message,
                    description: "Failed to load customer customer location"
                });
            }
            if (!cache) {
                return res.status(400).json({
                    code: -1,
                    details: "Customer not found",
                    description: "Customer not found"
                });
            }

            locationManager.redeemPromo(promoId, code, (err, result) => {
                var status;
                var reply;

                if (err) {
                    status = 400;
                    reply = {
                        title: "Invalid Code",
                        code: common.LOCATION_PROMO_FAILED_TO_REDEEM,
                        details: err.message,
                        description: "Please enter a valid redemption code"
                    }
                } else {
                    status = 200;
                    reply = {code: 0}
                }

                db.cache_hput("uuid_cache", req.params.user_key + "_" + uuid, {
                    status: status,
                    reply: JSON.stringify(reply)
                }, 120000);

                res.status(status).json(reply);
            });
        });
    });
}

function getNearbyBranch(req, res) {
    const latitude = req.fields.latitude ? parseFloat(req.fields.latitude) : 0;
    const longitude = req.fields.longitude ? parseFloat(req.fields.longitude) : 0;
    const providerId = req.fields.providerId;

    locationManager.loadPoints(
        {
            latitude,
            longitude,
            providerId,
            radius: 0,
            limit: 20
        }, (err, response) => {
        if (err) {
            common.log('LocationManager.locationPromoDialogGet() Error: ', err);
        } else {
            const list = [];
            
            if (response.data && response.data.length) {
                response.data.forEach(val => {
                    if (val.enabled === 1 && val.generateRedemptionCode === 1) {
                        const distance = common.calculateDistance(latitude, longitude, val.latitude, val.longitude) 
                        ? common.calculateDistance(latitude, longitude, val.latitude, val.longitude) : 'N/A';

                        if (distance < limitedDistance) {
                            const nearestBranchDetails = {
                                latitude: val.latitude,
                                longitude: val.longitude,
                                name: val.label,
                                distance,
                            };
                            list.push(nearestBranchDetails)
                        } 
                    }                   
                });
                if (list.length) {
                    list.sort((a, b) => {
                        return a.distance - b.distance;
                    })
                }
            }

            res.json({
                code: 0,
                list,
            });
        }
    });
}

function getPaymentUrl(req, res) {
    const { addonId } = req.fields;
    const { user_key } = req.params;

    ec.getCache(req.params.user_key, false, function (err, cache) {
        if (err) {
            return res.status(400).json({
                code: -1,
                details: err.message,
                description: "Failed to load customer cache"
            });
        }
        if (!cache) {
            return res.status(400).json({
                code: -1,
                details: "Customer not found",
                description: "Customer not found"
            });
        }
        const bankName = cache.billingCreditCard.bank;
        dbsManager.getPaymentUrl({
            serviceInstanceNumber: cache.serviceInstanceNumber, 
            bankName, 
            addonId,
        })
        .then(result => {
            return res.json({
                code:0,
                url: result.redirectUrl ? result.redirectUrl : 'N/A'
            });
        })
        .catch(err => {
            return res.status(400).json({
                code: 400,
                title: 'Get Payment Url Error',
                details: err.status,
                description: "Error occurred while getting payment url"
            }); 
        })
    })
}