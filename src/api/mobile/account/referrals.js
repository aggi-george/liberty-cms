var request = require('request');
var account = require('./account');
var common = require(__lib + '/common');
var db = require(__lib + '/db_handler');
var ec = require(__lib + '/elitecore');
var config = require(__base + '/config');

var LOG = config.LOGSENABLED;

//exports

exports.referralCodeGet = referralCodeGet;
exports.referralCodeAdd = referralCodeAdd;

//functions

function loadReferralCodeFromCMS(prefix, number, callback) {
    var key = config.NOTIFICATION_KEY;
    var url = "http://" + config.CMSHOST + ":" + config.CMSPORT + "/api/1/referral/code/get/" + prefix + "/" + number + "/" + key;

    request({
        uri: url,
        method: 'GET',
        timeout: 20000
    }, function (error, response, body) {
        if (error) {
            common.error("loadReferralCodeFromCMS", JSON.stringify(error) + " " + body)
            callback(error);
        } else {
            var reply = common.safeParse(body);
            if (reply.data) {
                callback(undefined, reply.data);
            } else {
                var error = new Error("Data is empty");
                error.status = body.status;
                callback(error);
            }
        }
    });
}

function updateCodeOnCMS(prefix, number, code, callback) {
    var key = config.NOTIFICATION_KEY;
    var url = "http://" + config.CMSHOST + ":" + config.CMSPORT + "/api/1/referral/code/add/" + key;

    request({
        uri: url,
        method: 'PUT',
        timeout: 20000,
        json: {
            prefix: prefix,
            number: number,
            code: code
        }
    }, function (error, response, body) {
        if (error) {
            common.error("loadReferralCodeFromCMS", JSON.stringify(error) + " " + body)
            callback(error);
        } else {
            var reply = common.safeParse(body);
            if (reply.data) {
                callback(undefined, reply.data);
            } else {
                var error = new Error("Data is empty");
                error.status = body.status;
                callback(error);
            }
        }
    });
}

function referralCodeGet(req, res) {
    if (LOG) common.log("referralCodeGet", "load referral code, user_key=" + req.params.user_key);
    ec.getCache(req.params.user_key, false, function (err, cache) {
        if (!cache) {
            return res.json({"code": -1});
        }
        var prefix = cache.prefix;
        var number = cache.number;

        loadReferralCodeFromCMS(prefix, number, function (err, data) {
            var reply;
            var status;
            if (!err) {
                if (LOG) common.log("referralCodeGet", "success, data=" + JSON.stringify(data));

                status = 200;
                reply = {
                    code: 0,
                    data: data,
                    share: {
                        facebook_title: "Use my referral code " + data.code + ".",
                        facebook_body: "Get $" + data.discountAmount + " off your Circles.Life registration fee when you build your plan!",
                        message_body: "Use my referral code " + data.code + " and get $" + data.discountAmount + " off your Circles.Life " +
                        "registration fee. Build your plan now!"
                    }
                };
                res.json(reply);
            } else {
                if (LOG) common.error("referralCodeGet", "error, message=" + err.message +
                ", status=" + err.status);

                status = 400;
                reply = {
                    code: -1,
                    details: "Can not load referral code",
                    description: "Can not load referral code"
                };
                res.status(status).json(reply);
            }
        });
    });
}

function referralCodeAdd(req, res) {
    if (LOG) common.log("referralCodeAdd", "add referral code, user_key=" + req.params.user_key);
    ec.getCache(req.params.user_key, false, function (err, cache) {
        if (!cache) {
            return res.json({"code": -1});
        }

        var code = req.fields.code;
        var prefix = cache.prefix;
        var number = cache.number;

        updateCodeOnCMS(prefix, number, code, function (err, referralCode) {
            var reply;
            var status;

            if (!err) {
                if (LOG) common.log("referralCodeAdd", "success, referralCode=" + referralCode);
                status = 200;
                reply = {
                    code: 0,
                    referralCode: referralCode
                };
                res.json(reply);

            } else {
                if (LOG) common.error("referralCodeAdd", "error, message=" + err.message +
                ", status=" + err.status);

                status = 400;
                if (err.status === "CODE_MAX_LIMIT_REACHED") {
                    reply = {
                        code: common.CODE_MAX_LIMIT_REACHED_ERROR,
                        details: "CODE_MAX_LIMIT_REACHED_ERROR",
                        description: "Oops you can only change your code once! Please continue using this code."
                    };
                } else if (err.status === "CODE_ALREADY_TAKEN") {
                    reply = {
                        code: common.CODE_ALREADY_TAKEN_ERROR,
                        details: "CODE_ALREADY_TAKEN_ERROR",
                        description: "Oops, this code is already taken. Please try something else."
                    };
                } else if (err.status === "CODE_LENGTH_INVALID") {
                    reply = {
                        code: common.CODE_LENGTH_INVALID_ERROR,
                        details: "CODE_LENGTH_INVALID",
                        description: "Oops, your code needs to be between 5-10 characters. Please try something else."
                    };
                } else if (err.status === "INAPPROPRIATE_CONTENT") {
                    reply = {
                        code: common.CODE_INAPPROPRIATE_CONTENT_ERROR,
                        details: "INAPPROPRIATE_CONTENT_ERROR",
                        description: "Oops, this code is not allowed. Please try something else."
                    };
                } else if (err.status === "INVALID_CHARS") {
                    reply = {
                        code: common.CODE_INVALID_CHARS_ERROR,
                        details: "INVALID_CHARS",
                        description: "Oops, codes can only contains letters or numbers. " +
                        "No special characters are allowed. Please try something else."
                    };
                } else {
                    reply = {
                        code: -1,
                        details: "Oops, can not create new referral code",
                        description: "Oops, can not create new referral code"
                    };
                }

                res.status(status).json(reply);
            }
        });
    });
}
