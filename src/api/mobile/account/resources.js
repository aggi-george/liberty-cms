var async = require('async');
var fs = require('fs');
var md5 = require("MD5");
var mime = require('mime');
var request = require('request');

var common = require(__lib + '/common');
var db = require(__lib + '/db_handler');
var ec = require(__lib + '/elitecore');
var config = require(__base + '/config');
var fileManager = require('../../../core/manager/file/fileManager');

var LOG = config.LOGSENABLED;

//exports

exports.getResource = getResource;

//functions

function getResource(req, res) {
    var fileId = req.params.id;

    if (!fileId) {
        return res.status(400).json({
            code: -1,
            details: "Invalid params",
            description: "Invalid params"
        });
    }

    fileManager.loadFileFromDatabase(fileId, 'resource', (err, result) => {
        if (result) {
            res.set('Content-Type', result.mime);
            res.send(result.file.buffer);
        } else {
            res.status(404).json({
                code: -1,
                details: 'File "' + fileId + '" Not Found',
                description: 'File "' + fileId + '" Not Found'
            });
        }
    });
}
