var account = require('../account');
var common = require(__lib + '/common');

//exports

exports.topupAllGet = topupAllGet;
exports.topupUpdateSet = topupUpdateSet;

exports.extraAllGet = extraAllGet;
exports.extraUpdateSet = extraUpdateSet;
exports.extraUpdateBatchSet = extraUpdateBatchSet;
exports.dataExtraAllGet = dataExtraAllGet;
exports.dataExtraUpdateSet = dataExtraUpdateSet;

exports.generalAllGet = generalAllGet;
exports.generalUpdateSet = generalUpdateSet;

exports.bonusAvailableGet = bonusAvailableGet;
exports.bonusHistoryGet = bonusHistoryGet;
exports.bonusAvailed = bonusAvailed;
exports.referralCodeGet = referralCodeGet;
exports.referralCodeAdd = referralCodeAdd;
exports.leaderboardGet = leaderboardGet;

//functions

function topupAllGet(req, res) {
    res.json([
        {
            "id": "10000001",
            "type": "data",
            "value": 102400,
            "unit": "kilobyte",
            "price": {
                "prefix": "$",
                "value": 1,
                "postfix": "SGD"
            },
            "recurrent": true,
            "payment": "postponed",
            "order_id": 0
        },
        {
            "id": "10000002",
            "type": "data",
            "value": 512000,
            "unit": "kilobyte",
            "price": {
                "prefix": "$",
                "value": 3.5,
                "postfix": "SGD"
            },
            "recurrent": true,
            "payment": "postponed",
            "order_id": 1
        },
        {
            "id": "10000003",
            "type": "data",
            "value": 1048576,
            "unit": "kilobyte",
            "price": {
                "prefix": "$",
                "value": 6,
                "postfix": "SGD"
            },
            "recurrent": true,
            "payment": "postponed",
            "order_id": 2
        }
    ]);
}

function dataExtraAllGet(req, res) {
    res.json([
        {
            "id": "20000001",
            "type": "data",
            "value": 1048576,
            "unit": "kilobyte",
            "price": {
                "prefix": "$",
                "value": 1.1,
                "postfix": "SGD"
            },
            "recurrent": false,
            "payment": "postponed"
        },
        {
            "id": "20000002",
            "type": "data",
            "value": 2097152,
            "unit": "kilobyte",
            "price": {
                "prefix": "$",
                "value": 2.2,
                "postfix": "SGD"
            },
            "recurrent": false,
            "payment": "postponed"
        },
        {
            "id": "20000003",
            "type": "data",
            "value": 3145728,
            "unit": "kilobyte",
            "price": {
                "prefix": "$",
                "value": 3.3,
                "postfix": "SGD"
            },
            "recurrent": false,
            "payment": "postponed"
        },
        {
            "id": "20000004",
            "type": "data",
            "value": 4194304,
            "unit": "kilobyte",
            "price": {
                "prefix": "$",
                "value": 4.4,
                "postfix": "SGD"
            },
            "recurrent": false,
            "payment": "postponed"
        },
        {
            "id": "20000005",
            "type": "data",
            "value": 5242880,
            "unit": "kilobyte",
            "price": {
                "prefix": "$",
                "value": 5.5,
                "postfix": "SGD"
            },
            "recurrent": false,
            "payment": "postponed"
        },
        {
            "id": "20000006",
            "type": "data",
            "value": 6291456,
            "unit": "kilobyte",
            "price": {
                "prefix": "$",
                "value": 6.6,
                "postfix": "SGD"
            },
            "recurrent": false,
            "payment": "postponed"
        },
        {
            "id": "20000007",
            "type": "data",
            "value": 7340032,
            "unit": "kilobyte",
            "price": {
                "prefix": "$",
                "value": 7.7,
                "postfix": "SGD"
            },
            "recurrent": false,
            "payment": "postponed"
        },
        {
            "id": "20000008",
            "type": "data",
            "value": 8388608,
            "unit": "kilobyte",
            "price": {
                "prefix": "$",
                "value": 8.8,
                "postfix": "SGD"
            },
            "recurrent": false,
            "payment": "postponed"
        },
        {
            "id": "20000009",
            "type": "data",
            "value": 9437184,
            "unit": "kilobyte",
            "price": {
                "prefix": "$",
                "value": 9.9,
                "postfix": "SGD"
            },
            "recurrent": false,
            "payment": "postponed"
        },
        {
            "id": "20000010",
            "type": "data",
            "value": 10485760,
            "unit": "kilobyte",
            "price": {
                "prefix": "$",
                "value": 10,
                "postfix": "SGD"
            },
            "recurrent": false,
            "payment": "postponed"
        },
        {
            "id": "20000011",
            "type": "data",
            "value": 11534336,
            "unit": "kilobyte",
            "price": {
                "prefix": "$",
                "value": 11.11,
                "postfix": "SGD"
            },
            "recurrent": false,
            "payment": "postponed"
        },
        {
            "id": "20000012",
            "type": "data",
            "value": 12582912,
            "unit": "kilobyte",
            "price": {
                "prefix": "$",
                "value": 12.12,
                "postfix": "SGD"
            },
            "recurrent": false,
            "payment": "postponed"
        },
        {
            "id": "20000013",
            "type": "data",
            "value": 13631488,
            "unit": "kilobyte",
            "price": {
                "prefix": "$",
                "value": 13.13,
                "postfix": "SGD"
            },
            "recurrent": false,
            "payment": "postponed"
        },
        {
            "id": "20000014",
            "type": "data",
            "value": 14680064,
            "unit": "kilobyte",
            "price": {
                "prefix": "$",
                "value": 14.14,
                "postfix": "SGD"
            },
            "recurrent": false,
            "payment": "postponed"
        },
        {
            "id": "20000015",
            "type": "data",
            "value": 15728640,
            "unit": "kilobyte",
            "price": {
                "prefix": "$",
                "value": 15.15,
                "postfix": "SGD"
            },
            "recurrent": false,
            "payment": "postponed"
        },
        {
            "id": "20000016",
            "type": "data",
            "value": 16777216,
            "unit": "kilobyte",
            "price": {
                "prefix": "$",
                "value": 16,
                "postfix": "SGD"
            },
            "recurrent": false,
            "payment": "postponed"
        },
        {
            "id": "20000017",
            "type": "data",
            "value": 17825792,
            "unit": "kilobyte",
            "price": {
                "prefix": "$",
                "value": 17,
                "postfix": "SGD"
            },
            "recurrent": false,
            "payment": "postponed"
        }
    ]);
}

function extraAllGet(req, res) {
    res.json({
        "data": [
            {
                "type": "data",
                "unit": "kilobyte",
                "recurrent": true,
                "payment": "postponed",
                "id": "PRD00378",
                "value": 13631488,
                "price": {
                    "prefix": "$",
                    "value": 78,
                    "postfix": "SGD"
                }
            },
            {
                "type": "data",
                "unit": "kilobyte",
                "recurrent": true,
                "payment": "postponed",
                "id": "PRD00377",
                "value": 12582912,
                "price": {
                    "prefix": "$",
                    "value": 72,
                    "postfix": "SGD"
                }
            },
            {
                "type": "data",
                "unit": "kilobyte",
                "recurrent": true,
                "payment": "postponed",
                "id": "PRD00376",
                "value": 11534336,
                "price": {
                    "prefix": "$",
                    "value": 66,
                    "postfix": "SGD"
                }
            },
            {
                "type": "data",
                "unit": "kilobyte",
                "recurrent": true,
                "payment": "postponed",
                "id": "PRD00375",
                "value": 10485760,
                "price": {
                    "prefix": "$",
                    "value": 60,
                    "postfix": "SGD"
                }
            },
            {
                "type": "data",
                "unit": "kilobyte",
                "recurrent": true,
                "payment": "postponed",
                "id": "PRD00343",
                "value": 1048576,
                "price": {
                    "prefix": "$",
                    "value": 6,
                    "postfix": "SGD"
                }
            },
            {
                "type": "data",
                "unit": "kilobyte",
                "recurrent": true,
                "payment": "postponed",
                "id": "PRD00360",
                "value": 2097152,
                "price": {
                    "prefix": "$",
                    "value": 12,
                    "postfix": "SGD"
                }
            },
            {
                "type": "data",
                "unit": "kilobyte",
                "recurrent": true,
                "payment": "postponed",
                "id": "PRD00382",
                "value": 17825792,
                "price": {
                    "prefix": "$",
                    "value": 102,
                    "postfix": "SGD"
                }
            },
            {
                "type": "data",
                "unit": "kilobyte",
                "recurrent": true,
                "payment": "postponed",
                "id": "PRD00381",
                "value": 16777216,
                "price": {
                    "prefix": "$",
                    "value": 96,
                    "postfix": "SGD"
                }
            },
            {
                "type": "data",
                "unit": "kilobyte",
                "recurrent": true,
                "payment": "postponed",
                "id": "PRD00380",
                "value": 15728640,
                "price": {
                    "prefix": "$",
                    "value": 90,
                    "postfix": "SGD"
                }
            },
            {
                "type": "data",
                "unit": "kilobyte",
                "recurrent": true,
                "payment": "postponed",
                "id": "PRD00379",
                "value": 14680064,
                "price": {
                    "prefix": "$",
                    "value": 84,
                    "postfix": "SGD"
                }
            },
            {
                "type": "data",
                "unit": "kilobyte",
                "recurrent": true,
                "payment": "postponed",
                "id": "PRD00365",
                "value": 7340032,
                "price": {
                    "prefix": "$",
                    "value": 42,
                    "postfix": "SGD"
                }
            },
            {
                "type": "data",
                "unit": "kilobyte",
                "recurrent": true,
                "payment": "postponed",
                "id": "PRD00364",
                "value": 6291456,
                "price": {
                    "prefix": "$",
                    "value": 36,
                    "postfix": "SGD"
                }
            },
            {
                "type": "data",
                "unit": "kilobyte",
                "recurrent": true,
                "payment": "postponed",
                "id": "PRD00363",
                "value": 5242880,
                "price": {
                    "prefix": "$",
                    "value": 30,
                    "postfix": "SGD"
                }
            },
            {
                "type": "data",
                "unit": "kilobyte",
                "recurrent": true,
                "payment": "postponed",
                "id": "PRD00362",
                "value": 4194304,
                "price": {
                    "prefix": "$",
                    "value": 24,
                    "postfix": "SGD"
                }
            },
            {
                "type": "data",
                "unit": "kilobyte",
                "recurrent": true,
                "payment": "postponed",
                "id": "PRD00361",
                "value": 3145728,
                "price": {
                    "prefix": "$",
                    "value": 18,
                    "postfix": "SGD"
                }
            },
            {
                "type": "data",
                "unit": "kilobyte",
                "recurrent": true,
                "payment": "postponed",
                "id": "PRD00374",
                "value": 9437184,
                "price": {
                    "prefix": "$",
                    "value": 54,
                    "postfix": "SGD"
                }
            },
            {
                "type": "data",
                "unit": "kilobyte",
                "recurrent": true,
                "payment": "postponed",
                "id": "PRD00370",
                "value": 8388608,
                "price": {
                    "prefix": "$",
                    "value": 48,
                    "postfix": "SGD"
                }
            }
        ],
        "sms": [
            {
                "type": "sms",
                "unit": "sms",
                "recurrent": true,
                "payment": "postponed",
                "id": "PRD00396",
                "value": 300,
                "price": {
                    "prefix": "$",
                    "value": 12,
                    "postfix": "SGD"
                }
            },
            {
                "type": "sms",
                "unit": "sms",
                "recurrent": true,
                "payment": "postponed",
                "id": "PRD00529",
                "value": 100,
                "price": {
                    "prefix": "$",
                    "value": 4,
                    "postfix": "SGD"
                }
            },
            {
                "type": "sms",
                "unit": "sms",
                "recurrent": true,
                "payment": "postponed",
                "id": "PRD00530",
                "value": 200,
                "price": {
                    "prefix": "$",
                    "value": 8,
                    "postfix": "SGD"
                }
            },
            {
                "type": "sms",
                "unit": "sms",
                "recurrent": true,
                "payment": "postponed",
                "id": "PRD00533",
                "value": 400,
                "price": {
                    "prefix": "$",
                    "value": 16,
                    "postfix": "SGD"
                }
            }
        ],
        "calls": [
            {
                "type": "calls",
                "unit": "seconds",
                "recurrent": true,
                "payment": "postponed",
                "id": "PRD00383",
                "value": 24000,
                "price": {
                    "prefix": "$",
                    "value": 16,
                    "postfix": "SGD"
                }
            },
            {
                "type": "calls",
                "unit": "seconds",
                "recurrent": true,
                "payment": "postponed",
                "id": "PRD00352",
                "value": 12000,
                "price": {
                    "prefix": "$",
                    "value": 8,
                    "postfix": "SGD"
                }
            },
            {
                "type": "calls",
                "unit": "seconds",
                "recurrent": true,
                "payment": "postponed",
                "id": "PRD00539",
                "value": 6000,
                "price": {
                    "prefix": "$",
                    "value": 4,
                    "postfix": "SGD"
                }
            },
            {
                "type": "calls",
                "unit": "seconds",
                "recurrent": true,
                "payment": "postponed",
                "id": "PRD00541",
                "value": 18000,
                "price": {
                    "prefix": "$",
                    "value": 12,
                    "postfix": "SGD"
                }
            }
        ],
        "data_visible": true,
        "calls_visible": true,
        "sms_visible": true
    });
}

function generalAllGet(req, res) {
    res.json([
        {
            "title": "4G Speed Max",
            "subtitle": "",
            "description_short": "Enjoy uninterrupted island-wide maximum speed on 4G",
            "description_full": "Enjoy uninterrupted island-wide maximum speed on 4G",
            "disclaimer": "",
            "id": "PRD00316",
            "price": {
                "prefix": "$",
                "value": 0,
                "postfix": "SGD",
                "discount": null
            },
            "payment": "postponed",
            "action_available": false,
            "recurrent": true,
            "sub_effect": "immediate",
            "unsub_effect": "next_bill_cycle",
            "order_id": 6
        },
        {
            "title": "Unlimited Plan Change",
            "subtitle": "",
            "description_short": "Customize your plan anytime via the CirclesCare app",
            "description_full": "Customize your plan anytime via the CirclesCare app",
            "disclaimer": "",
            "id": "PRD00317",
            "price": {
                "prefix": "$",
                "value": 0,
                "postfix": "SGD",
                "discount": null
            },
            "payment": "postponed",
            "action_available": false,
            "recurrent": true,
            "sub_effect": "immediate",
            "unsub_effect": "next_bill_cycle",
            "order_id": 5
        },
        {
            "title": "Unlimited WhatsApp",
            "subtitle": "",
            "description_short": "WhatsApp as much as you want without using your data! Does not include video",
            "description_full": "WhatsApp as much as you want without using your data! Does not include video.",
            "disclaimer": "",
            "id": "PRD00440",
            "price": {
                "prefix": "$",
                "value": 0,
                "postfix": "SGD",
                "discount": null
            },
            "payment": "postponed",
            "action_available": false,
            "beta": 0,
            "recurrent": true,
            "sub_effect": "immediate",
            "unsub_effect": "immediate",
            "order_id": 4
        },
        {
            "title": "Caller Number Display",
            "subtitle": "",
            "description_short": "Know who's calling with Caller Identification.",
            "description_full": "Know who's calling with Caller Identification.",
            "disclaimer": "",
            "id": "PRD00445",
            "price": {
                "prefix": "$",
                "value": 0,
                "postfix": "SGD",
                "discount": 0
            },
            "payment": "postponed",
            "action_available": true,
            "recurrent": true,
            "sub_effect": "immediate",
            "unsub_effect": "immediate",
            "order_id": 3
        },
        {
            "title": "Unlimited Incoming Calls",
            "subtitle": "",
            "description_short": "Limitless incoming local calls.",
            "description_full": "Activate limitless incoming local calls that do not take from your Talktime minutes.",
            "disclaimer": "",
            "id": "PRD00463",
            "price": {
                "prefix": "$",
                "value": 2,
                "postfix": "SGD",
                "discount": null
            },
            "payment": "postponed",
            "action_available": true,
            "recurrent": true,
            "sub_effect": "immediate",
            "unsub_effect": "next_bill_cycle",
            "order_id": 2
        },
	{
          "id": "PRD00746",
          "payment": "postponed",
          "description_full": "Get 20 GB of maximum 4G speeds, for $20 on top of your current monthly plan.",
          "title": "20 GB for $20",
          "disclaimer": "",
          "subtitle": "",
          "order_id": 1,
          "recurrent": true,
          "advanced_payment": 1,
          "free_package": "",
          "unsub_effect": "next_bill_cycle",
          "price": {
            "postfix": "SGD",
            "value": 20,
            "prefix": "$",
            "discount": null
          },
          "beta": 0,
          "betaGroup": "",
          "remind_user_nonprorated_charge" : true,
          "description_short": "Get 20 GB of maximum 4G speeds, for $20.",
          "sub_effect": "immediate",
          "action_available": true
        },
	{
          "description_short": "Activate data, calls and SMS when travelling overseas.",
          "list": [
            {
              "title": "Roaming Off",
              "id": "roamingOff",
              "description_short": "Turn Off All Roaming",
              "subtitle": "OFF",
              "description_full": "You are about to send us a request to turn off all roaming features.\n\nPlease note that it will take some time for this to take effect and you cannot change this setting while the request is being processed."
            },
            {
              "id": "PRD00650",
              "payment": "postponed",
              "description_full": "You are about to send us a request to enable Data, Voice & SMS roaming.\n\nPlease note that it will take some time for this to take effect and you cannot change this setting while the request is being processed.",
              "title": "Data, Voice And SMS Roaming On",
              "disclaimer": "",
              "subtitle": "Data, Voice & SMS",
              "order_id": 11,
              "recurrent": true,
              "advanced_payment": 0,
              "free_package": "",
              "unsub_effect": "immediate",
              "price": {
                "postfix": "SGD",
                "value": 0,
                "prefix": "$",
                "discount": null
              },
              "beta": 0,
              "betaGroup": "",
              "description_short": "Activate data, voice and SMS when travelling overseas.",
              "sub_effect": "immediate",
              "action_available": true
            },
            {
              "id": "PRD00649",
              "payment": "postponed",
              "description_full": "You are about to send us a request to enable Voice & SMS roaming.\n\nPlease note that it will take some time for this to take effect and you cannot change this setting while the request is being processed.",
              "title": "Voice And SMS Roaming On",
              "disclaimer": "",
              "subtitle": "Voice & SMS",
              "order_id": 0,
              "recurrent": true,
              "advanced_payment": 0,
              "free_package": "",
              "unsub_effect": "immediate",
              "price": {
                "postfix": "SGD",
                "value": 0,
                "prefix": "$",
                "discount": null
              },
              "beta": 0,
              "betaGroup": "",
              "description_short": "Activate voice and SMS when travelling overseas.",
              "sub_effect": "immediate",
              "action_available": true
            }
          ],
          "id": "roamingGroup",
          "order_id": 11,
          "description_full": "",
          "subtitle": "",
          "title": "Roaming"
        }
    ]);
}
function topupUpdateSet(req, res) {
    common.log("addons", "addBoostHandler: id=" + req.fields.id + ", action=" + req.fields.action);
    setTimeout(function () {
        res.json({code: 0});
    }, 2000);
}

function dataExtraUpdateSet(req, res) {
    common.log("addons", "dataExtraUpdateSet: id=" + req.fields.id + ", action=" + req.fields.action);
    setTimeout(function () {
        res.json({code: 0});
    }, 1000);
}

function extraUpdateSet(req, res) {
    common.log("addons", "extraUpdateSet: id=" + req.fields.id + ", action=" + req.fields.action + ", action=" + req.fields.type);
    setTimeout(function () {
        res.json({code: 0});
    }, 1000);
}

function extraUpdateBatchSet(req, res) {
    common.log("addons", "extraUpdateBatchSet: id=" + req.fields.id + ", action=" + req.fields.action + ", action=" + req.fields.type);
    setTimeout(function () {
        res.json({code: 0});
    }, 1000);
}

function generalUpdateSet(req, res) {
    common.log("addons", "generalUpdateSet: id=" + req.fields.id + ", action=" + req.fields.action);
    setTimeout(function () {
        res.json({code: 0});
    }, 1000);
}

function bonusAvailableGet(req, res) {
    var stubs = [
        { "unit": "gigabyte", "value": 0.2, "type": "data" },
        { "unit": "gigabyte", "value": 3, "type": "data" },
        { "unit": "gigabyte", "value": 4000, "type": "data" },
        { "unit": "gigabyte", "value": 5000000, "type": "data" },
    ]
    var reply = {
        "bonus_banner_data": {
            "joined_date": "2017-08-01T00:01:03Z",
            "earned_data": stubs[0]
        }
    }
    reply.bonus_banner_data.earned_data = stubs[new Date().getTime() % 4];
    res.json(reply);
}

function bonusHistoryGet(req, res) {
    res.json({
        "history": {
            "list": [
                {
                    "id": 438,
                    "product_kb": 204800,
                    "product_type": "referral",
                    "added_date": "2016-08-03T05:43:14.000Z",
                    "leaderboard_included": true,
                    "metadata1": "Andreana jones",
                    "metadata2": "65",
                    "metadata3": "87420551",
                    "metadata4": "referral_referred",
                    "metadata5": "LW085169"
                },
                {
                    "id": 449,
                    "product_kb": 204800,
                    "product_type": "referral",
                    "added_date": "2016-08-03T07:51:05.000Z",
                    "leaderboard_included": true,
                    "metadata1": "kelvin Chua",
                    "metadata2": "65",
                    "metadata3": "87420551",
                    "metadata4": "referral_referred",
                    "metadata5": "LW085169"
                },
                {
                    "id": 460,
                    "product_kb": 3145728,
                    "product_type": "selfcare_install",
                    "added_date": "2016-08-01T11:10:51.000Z",
                    "leaderboard_included": true
                },
                {
                    "id": 516,
                    "product_kb": 2097152,
                    "product_type": "promo_national_day",
                    "added_date": "2016-08-10T03:45:40.000Z",
                    "valid_until": "2016-12-31T15:59:59.000Z",
                    "leaderboard_included": false
                }
            ]
        },
        "rating": {
            "hide": true,
            "dataGb": 3.4,
            "rating": {
                "count": 5,
                "position": 4,
                "needToReachGb": 0
            }
        }
    });
}

function referralCodeGet(req, res) {
    res.json({
        "code": 0,
        "data": {
            "code": "CRLS1",
            "bonusKb": 204800,
            "bonusRecurrent": true,
            "discountAmount": 20,
            "referrals": [
            ],
            "referrer": {
                "id": 430,
                "accountNumber": "LW170245",
                "bonusKb": 0,
                "bonusAdded": 0,
                "addedDate": "2016-08-04T09:56:03.000Z"
            }
        },
        "share": {
            "facebook_title": "Use my referral code G2DHB.",
            "facebook_body": "Get $20 off your Circles.Life registration fee when you build your plan!",
            "message_body": "Use my referral code G2DHB and get $20 off your Circles.Life registration fee. Build your plan now!"
        }
    });
}

function referralCodeAdd(req, res) {
    common.log("addons", "gentwoGeneralUpdateSet: code=" + req.fields.code);
    res.json({code:0});
}

function leaderboardGet(req, res) {
    res.json({
        "leaderboard":{
            "list":[
                {
                    "position":1,
                    "peopleCount":1,
                    "bonusDataGb":8.6,
                    "serviceInstanceNumbers":[
                        "LW080076"
                    ],
                    "users":[
                        {
                            "nickname":"Kelvin913",
                            "picture":"default4",
                            "serviceInstanceNumber":"LW075228"
                        }
                    ]

                },
                {
                    "position":2,
                    "peopleCount":1,
                    "bonusDataGb":6.4,
                    "serviceInstanceNumbers":[
                        "LW075228"
                    ],
                    "users":[
                        {
                            "nickname":"Alex085",
                            "picture":"default5",
                            "serviceInstanceNumber":"LW080076"
                        }
                    ]
                },
                {
                    "position":3,
                    "peopleCount":1,
                    "bonusDataGb":4.2,
                    "serviceInstanceNumbers":[
                        "LW170272"
                    ],
                    "users":[
                        {
                            "nickname":"Shobana923",
                            "picture":"default3",
                            "serviceInstanceNumber":"LW170272"
                        }
                    ]
                },
                {
                    "position":4,
                    "peopleCount":2,
                    "bonusDataGb":2.2,
                    "serviceInstanceNumbers":[
                        "LW100276",
                        "LW085140"
                    ],
                    "users":[
                        {
                            "nickname":"Johny",
                            "picture":"default2",
                            "serviceInstanceNumber":"LW085140",
                            "self":true
                        },
                        {
                            "nickname":"Mona652",
                            "picture":"default1",
                            "serviceInstanceNumber":"LW100276"
                        }
                    ]
                },
                {
                    "position":5,
                    "peopleCount":1,
                    "bonusDataGb":1.6,
                    "serviceInstanceNumbers":[
                        "LW170248"
                    ],
                    "users":[
                        {
                            "nickname":"Mario262",
                            "picture":"default3",
                            "serviceInstanceNumber":"LW170248"
                        }
                    ]
                }
            ]
        }
    });
}

function bonusAvailed(req, res) {
    res.json({
        bonus_banner_data: {
            joined_date: '2017-08-14T06:27:11.000Z',
            earned_data: {
                value: 9,
                type: 'data',
                unit: 'gigabyte'
            }
        }
    });
}
