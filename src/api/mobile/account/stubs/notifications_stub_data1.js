var account = require('../account');
var common = require(__lib + '/common');

//exports

exports.notificationsHistoryGet = notificationsHistoryGet;
exports.notificationsReadStatusSet = notificationsReadStatusSet;

//functions

function notificationsHistoryGet(req, res) {
    if (req.query.anchor === "1471251565373") {
        return res.json([]);
    }

    res.json([
        {
            "id":"57b1846d3b366b150ee1c182",
            "date":"2016-08-15T08:59:25.373Z",
            "anchor":"1471251565373",
            "read":false,
            "mime_type":"bonus_data",
            "title":"Successful Referral!",
            "subtitle":"Earned +200 MB and Bill Credit!",
            "text":"John Lee officially joined the network. In celebration of Singapore National Day, and on top of your 200 MB Bonus Data, we offer you a $5  in bill credit, which means you cumulated $5 by referring your friends this month. Thank you!",
            "content":{

            }
        },
        {
            "id":"57b1846d3b366b150ee1c180",
            "date":"2016-08-15T08:59:25.263Z",
            "anchor":"1471251565263",
            "read":false,
            "mime_type":"bonus_data",
            "title":"Referral Bonus Data added",
            "subtitle":"Thank you!",
            "text":"John Lee officially joined the network. You can now use your 200 MB/mo Referral Bonus Data!",
            "content":{

            }
        },
        {
            "id":"57adaeafcb80bc6a6a375ca0",
            "date":"2016-08-12T11:10:39.358Z",
            "anchor":"1471000239358",
            "read":true,
            "mime_type":"bonus_data",
            "title":"Happy Birthday",
            "subtitle":"Birthday Bonus Data added",
            "text":"Once again, happy birthday! This is a confirmation that your Birthday Bonus Data of $value $unit was successfully added. ",
            "content":{

            }
        },
        {
            "id":"57ac42fc9192544408483abe",
            "date":"2016-08-11T09:18:52.931Z",
            "anchor":"1470907132931",
            "read":true,
            "mime_type":"boost_added",
            "title":"Boost Added",
            "text":"You've just activated a 500 MB Boost to your plan! This is a confirmation that your credit card was charged and that your Boost has been added to your plan",
            "content":{

            }
        },
        {
            "id":"57ac42f09192544408483abc",
            "date":"2016-08-11T09:18:40.892Z",
            "anchor":"1470907120892",
            "read":true,
            "mime_type":"boost_added",
            "title":"Boost Added",
            "text":"You've just activated a 500 MB Boost to your plan! This is a confirmation that your credit card was charged and that your Boost has been added to your plan",
            "content":{

            }
        },
        {
            "id":"57ac0ebd9192544408482abd",
            "date":"2016-08-11T05:35:57.154Z",
            "anchor":"1470893757154",
            "read":true,
            "mime_type":"text",
            "title":"Bonus Data Leaderboard",
            "subtitle":"Are you on the Bonus Data Leaderboard?",
            "text":"Check out the new Leaderboard in the Bonus Menu! Start referring friends to earn even +200MB/mo Data and $5 Bill Credit per referral who signs up before August 18! ",
            "image_preview":"https://s3-ap-southeast-1.amazonaws.com/kirk.circles.asia/assets/marketing_leaderboard/leaderboard-referral-icon.png",
            "image":"https://s3-ap-southeast-1.amazonaws.com/kirk.circles.asia/assets/marketing_leaderboard/leaderboard-referral-full.png",
            "content":{

            }
        },
        {
            "id":"57aaa349063062eb6b643ac6",
            "date":"2016-08-10T03:45:13.656Z",
            "anchor":"1470800713656",
            "read":true,
            "mime_type":"text",
            "title":"Happy SG51!",
            "subtitle":"Surprise 10 GB Bonus Data",
            "text":"It's a special month for Singapore. In celebration we're giving you 10 GB Bonus Data! Over the next 5 months, 2 GB Bonus Data will be added to your monthly usage. Thanks for being on our network!",
            "image_preview":"https://s3-ap-southeast-1.amazonaws.com/kirk.circles.asia/images/notification-image.png",
            "image":"https://s3-ap-southeast-1.amazonaws.com/kirk.circles.asia/images/full-banner.png",
            "content":{

            }
        },
        {
            "id":"57a46928a1cd810a6ce2cf93",
            "date":"2016-08-05T10:23:36.420Z",
            "anchor":"1470392616420",
            "read":true,
            "mime_type":"plus_added",
            "title":"You've added a new Plus",
            "subtitle":"Plus Roaming",
            "text":"You've added another exciting Plus option, Plus Roaming, to your mobile plan. It's already enabled and ready to use! Enjoy!",
            "content":{

            }
        },
        {
            "id":"57a46927a1cd810a6ce2cf91",
            "date":"2016-08-05T10:23:35.669Z",
            "anchor":"1470392615669",
            "read":true,
            "mime_type":"plus_removed",
            "title":"You've removed a Plus",
            "subtitle":"Plus Roaming",
            "text":"You've removed a Plus option, Plus Roaming, from your mobile plan. This has been removed immediately.",
            "content":{

            }
        },
        {
            "id":"57a1d1395907ecac746b78ab",
            "date":"2016-08-03T11:10:49.872Z",
            "anchor":"1470222649872",
            "read":true,
            "mime_type":"bonus_data",
            "title":"App Activation",
            "subtitle":"CirclesCare Bonus Data",
            "text":"Thanks for installing our CirclesCare App. We're giving you 2GB of Bonus Data as a \"thank you\"! ",
            "content":{

            }
        },
        {
            "id":"57a1a2aa2a95e034683f0646",
            "date":"2016-08-03T07:52:10.634Z",
            "anchor":"1470210730634",
            "read":true,
            "mime_type":"text",
            "title":"Referral",
            "subtitle":"Bill Credit Limit!",
            "text":"ABHISHEK GUPTA officially joined the network. Unfortunately, the 5 bill credit has already been granted 2 times for this NRIC, so this referral is not eligible for the 5 waiver. You can still enjoy your Bonus Data. Thank you!",
            "content":{

            }
        },
        {
            "id":"57a1a2aa2a95e034683f0645",
            "date":"2016-08-03T07:52:10.496Z",
            "anchor":"1470210730496",
            "read":true,
            "mime_type":"bonus_data",
            "title":"Referral Bonus Data added",
            "subtitle":"Thank you!",
            "text":"ABHISHEK GUPTA officially joined the network. You can now use your 200 MB/mo Referral Bonus Data!",
            "content":{

            }
        },
        {
            "id":"5799e37280d8e29d41b66bd6",
            "date":"2016-07-28T10:50:26.323Z",
            "anchor":"1469703026323",
            "read":true,
            "mime_type":"data_warning",
            "title":"Watch out, you're low on data!",
            "subtitle":"Do you want to Boost?",
            "text":"You've got about 0 MB to go. Do you want to Boost now? Or we'll Auto-Boost 100MB so you never run out. ",
            "content":{

            }
        },
        {
            "id":"5799bec640579bb205bcfdf0",
            "date":"2016-07-28T08:13:58.720Z",
            "anchor":"1469693638720",
            "read":true,
            "mime_type":"plus_removed",
            "title":"You've removed a Plus",
            "subtitle":"Caller Number Display",
            "text":"You've removed a Plus option, Caller Number Display, from your mobile plan. This has been removed immediately.",
            "content":{

            }
        },
        {
            "id":"5799bde640579bb205bcfde9",
            "date":"2016-07-28T08:10:14.104Z",
            "anchor":"1469693414104",
            "read":true,
            "mime_type":"bonus_data",
            "title":"Surprise!",
            "subtitle":"Bonus Data for you!",
            "text":"Your 100MB additional Bonus Data has been successfully activated. Thanks for being you :)",
            "content":{

            }
        },
        {
            "id":"5795886e320938a1348f2b90",
            "date":"2016-07-25T03:33:02.548Z",
            "anchor":"1469417582548",
            "read":true,
            "mime_type":"plan_sms_changed",
            "title":"Base Plan Changed (SMS)",
            "subtitle":"",
            "text":"Your SMS plan was successfully changed to 200 SMS. Enjoy your new SMS balance starting from the next billing cycle on 1st August.",
            "content":{

            }
        },
        {
            "id":"57958865320938a1348f2b8e",
            "date":"2016-07-25T03:32:53.960Z",
            "anchor":"1469417573960",
            "read":true,
            "mime_type":"plan_sms_changed",
            "title":"Base Plan Changed (SMS)",
            "subtitle":"",
            "text":"Your SMS plan was successfully changed to 0 SMS. Enjoy your new SMS balance starting from the next billing cycle on 1st August.",
            "content":{

            }
        },
        {
            "id":"5795854b320938a1348f2b6b",
            "date":"2016-07-25T03:19:39.047Z",
            "anchor":"1469416779047",
            "read":true,
            "mime_type":"plan_sms_changed",
            "title":"Base Plan Changed (SMS)",
            "subtitle":"",
            "text":"Your SMS plan was successfully changed to 200 SMS. Enjoy your new SMS balance starting from the next billing cycle on 1st August.",
            "content":{

            }
        },
        {
            "id":"5795854184d308bd34c0ddbe",
            "date":"2016-07-25T03:19:29.317Z",
            "anchor":"1469416769317",
            "read":true,
            "mime_type":"plan_sms_changed",
            "title":"Base Plan Changed (SMS)",
            "subtitle":"",
            "text":"Your SMS plan was successfully changed to 0 SMS. Enjoy your new SMS balance starting from the next billing cycle on 1st August.",
            "content":{

            }
        },
        {
            "id":"578de22b6b7df8e047f75362",
            "date":"2016-07-19T08:17:47.328Z",
            "anchor":"1468916267328",
            "read":true,
            "mime_type":"plan_sms_changed",
            "title":"Base Plan Changed (SMS)",
            "subtitle":"",
            "text":"Your SMS plan was successfully changed to 200 SMS. Enjoy your new SMS balance starting from the next billing cycle on 1st August.",
            "content":{

            }
        },
        {
            "id":"57875d7aa12a3c18225de5ea",
            "date":"2016-07-14T09:38:02.809Z",
            "anchor":"1468489082809",
            "read":true,
            "mime_type":"text",
            "title":"Unlimited Incoming Calls",
            "subtitle":"We have a new Plus option",
            "text":"Activate Unlimited Incoming Calls for only $2 per month. Go to \"Customize\"in the menu,  then \"Plus\" to add to your plan.  ",
            "content":{

            }
        },
        {
            "id":"5784cd70a3ca10073da40501",
            "date":"2016-07-12T10:58:56.344Z",
            "anchor":"1468321136344",
            "read":true,
            "mime_type":"text",
            "title":"New Roaming Rates",
            "subtitle":"Roaming Rates have been updated",
            "text":"Check out our new Data Roamer rates in the menu. We bring you pay-as-you-go rates as low as $10 per 100 MB used to your top 10 destination countries.  ",
            "content":{

            }
        },
        {
            "id":"5779d85f7c1e10f07a47057a",
            "date":"2016-07-04T03:30:39.337Z",
            "anchor":"1467603039337",
            "read":true,
            "mime_type":"boost_added",
            "title":"Boost Added",
            "text":"You've just activated a 100 MB Boost to your plan! This is a confirmation that your credit card was charged and that your Boost has been added to your plan",
            "content":{

            }
        },
        {
            "id":"57761368bb557a7571d83e03",
            "date":"2016-07-01T06:53:28.704Z",
            "anchor":"1467356008704",
            "read":true,
            "mime_type":"plan_data_changed",
            "title":"Base Plan Changed (Data)",
            "subtitle":"",
            "text":"Your Data plan was successfully changed to 5 GB. Enjoy your new Data balance starting from the next billing cycle on 1st August.",
            "content":{

            }
        },
        {
            "id":"57760fa64c4a5d91715f7096",
            "date":"2016-07-01T06:37:26.191Z",
            "anchor":"1467355046191",
            "read":true,
            "mime_type":"boost_added",
            "title":"Boost Added",
            "text":"You've just activated a 500 MB Boost to your plan! This is a confirmation that your credit card was charged and that your Boost has been added to your plan",
            "content":{

            }
        },
        {
            "id":"57760f6f4c4a5d91715f7091",
            "date":"2016-07-01T06:36:31.582Z",
            "anchor":"1467354991582",
            "read":true,
            "mime_type":"plus_added",
            "title":"You've added a new Plus",
            "subtitle":"Plus Roaming",
            "text":"You've added another exciting Plus option, Plus Roaming, to your mobile plan. It's already enabled and ready to use! Enjoy!",
            "content":{

            }
        },
        {
            "id":"57760ea2bb557a7571d83df0",
            "date":"2016-07-01T06:33:06.947Z",
            "anchor":"1467354786947",
            "read":true,
            "mime_type":"plus_added",
            "title":"You've added a new Plus",
            "subtitle":"Caller Number Display",
            "text":"You've added another exciting Plus option, Caller Number Display, to your mobile plan. It's already enabled and ready to use! Enjoy!",
            "content":{

            }
        },
        {
            "id":"57760e1a4c4a5d91715f7087",
            "date":"2016-07-01T06:30:50.332Z",
            "anchor":"1467354650332",
            "read":true,
            "mime_type":"plus_removed",
            "title":"You've removed a Plus",
            "subtitle":"Caller Number Display",
            "text":"You've removed a Plus option, Caller Number Display, from your mobile plan. This has been removed immediately.",
            "content":{

            }
        },
        {
            "id":"577606d8bb557a7571d83dc8",
            "date":"2016-07-01T05:59:52.796Z",
            "anchor":"1467352792796",
            "read":true,
            "mime_type":"plus_added",
            "title":"You've added a new Plus",
            "subtitle":"Caller Number Display",
            "text":"You've added another exciting Plus option, Caller Number Display, to your mobile plan. It's already enabled and ready to use! Enjoy!",
            "content":{

            }
        },
        {
            "id":"577604adbb557a7571d83dc4",
            "date":"2016-07-01T05:50:37.956Z",
            "anchor":"1467352237956",
            "read":true,
            "mime_type":"plus_added",
            "title":"You've added a new Plus",
            "subtitle":"Unlimited Incoming Calls",
            "text":"You've added another exciting Plus option, Unlimited Incoming Calls, to your mobile plan. It's already enabled and ready to use! Enjoy!",
            "content":{

            }
        },
        {
            "id":"577602bebb557a7571d83db6",
            "date":"2016-07-01T05:42:22.394Z",
            "anchor":"1467351742394",
            "read":true,
            "mime_type":"plan_data_changed",
            "title":"Base Plan Changed (Calls)",
            "subtitle":"",
            "text":"Your Talktime plan was successfully changed to 100 MINS. Enjoy your new Talktime balance starting from the next billing cycle on 1st August.",
            "content":{

            }
        },
        {
            "id":"5775f0fbbb557a7571d83d82",
            "date":"2016-07-01T04:26:35.163Z",
            "anchor":"1467347195163",
            "read":true,
            "mime_type":"plan_data_changed",
            "title":"Base Plan Changed (Data)",
            "subtitle":"",
            "text":"Your Data plan was successfully changed to 3 GB. Enjoy your new Data balance starting from the next billing cycle on 1st August.",
            "content":{

            }
        },
        {
            "id":"5775f087bb557a7571d83d7a",
            "date":"2016-07-01T04:24:39.975Z",
            "anchor":"1467347079975",
            "read":true,
            "mime_type":"plan_sms_changed",
            "title":"Base Plan Changed (SMS)",
            "subtitle":"",
            "text":"Your SMS plan was successfully changed to 0 SMS. Enjoy your new SMS balance starting from the next billing cycle on 1st August.",
            "content":{

            }
        }
    ]);
}

function notificationsReadStatusSet(req, res) {
    common.log("notifications", "notificationsReadStatusSet: " + req.fields.ids);
    res.json({});
}
