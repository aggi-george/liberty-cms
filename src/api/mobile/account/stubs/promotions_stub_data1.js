var account = require('../account');
var common = require(__lib + '/common');

//exports

exports.promotionCodeSet = promotionCodeSet;

//functions

function promotionCodeSet(req, res) {
    common.log("promotion", "code=" + req.fields.code);
    if (req.fields.code === "0000") {
        res.json({
            codeValid: true,
            title: "Succeed",
            subtitle: "Code - " + req.fields.code,
            message: "Promotion code is valid"
        });
    } else {
        res.json({
            codeValid:false,
            title:"Failed",
            subtitle:"Code - " + req.fields.code,
            message:"Promotion code is expired"
        });
    }
} 
