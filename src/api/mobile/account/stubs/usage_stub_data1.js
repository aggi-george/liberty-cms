var account = require('../account');
var common = require(__lib + '/common');

//exports

exports.usagePlanGet = usagePlanGet;
exports.dataGet = dataGet;
exports.otherGet = otherGet;
exports.historyUsageGet = historyUsageGet;
exports.historyBillsGet = historyBillsGet;
exports.historyBillsDetails = historyBillsDetails;
exports.gentwoGet = gentwoGet;
exports.callDetailsGet = callDetailsGet;
exports.historyBillsResend = historyBillsResend;
exports.paymentSet = paymentSet;

//functions

function usagePlanGet(req, res) {
    res.json({
    "data": {
        "basic": {
            "used": 0,
            "left": 3145727.998046875,
            "unit": "kilobytes",
            "prorated": false,
            "prorated_kb": 0,
            "plan_kb": 3145728
        },
        "extra": {
            "used": 0,
            "left": 1258291.01171875,
            "unit": "kilobytes",
            "prorated": false,
            "prorated_kb": 0,
            "plan_kb": 12582912
        },
        "boost": {
            "used": 0,
            "left": 1024000,
            "unit": "kilobytes",
            "section_min_value": 1048576
        },
        "bonus": {
            "used": 1522858,
            "left": 3124873.1943359375,
            "unit": "kilobytes"
        },
        "promotion_text": {
            "line1": "Bonus",
            "line2": "4.4 GB"
        }
    },
    "other": {
        "basic": {
            "calls": {
                "used": 3000,
                "left": 3000,
                "base_used": 3000,
                "base_left": 3000,
                "extra_used": 0,
                "extra_left": 0,
                "unit_type": "seconds"
            },
            "sms": {
                "used": 0,
                "left": 200,
                "base_used": 0,
                "base_left": 0,
                "extra_used": 0,
                "extra_left": 200,
                "unit_type": "sms"
            }
        },
        "pay_as_you_go": {
            "calls": {
                "total": {
                    "value": 600,
                    "unit_type": "seconds",
                    "price_per_unit": {
                        "prefix": "$",
                        "value": 0.1,
                        "postfix": "SGD",
                        "unit": "minutes"
                    },
                    "price_total": {
                        "prefix": "$",
                        "value": 20.5,
                        "postfix": "SGD"
                    }
                }
            },
            "sms": {
                "total": {
                    "value": 10,
                    "unit_type": "sms",
                    "price_per_unit": {
                        "prefix": "$",
                        "value": 0.1,
                        "postfix": "SGD",
                        "unit": "sms"
                    },
                    "price_total": {
                        "prefix": "$",
                        "value": 10.05,
                        "postfix": "SGD"
                    }
                }
            },
            "calls_idd": {
                "visible": true,
                "total": {
                    "value": 0,
                    "unit_type": "seconds",
                    "price_total": {
                        "prefix": "$",
                        "value": 10,
                        "postfix": "SGD"
                    }
                },
                "incoming": {
                    "value": 0,
                    "unit_type": "seconds",
                    "price_total": {
                        "prefix": "$",
                        "value": 0,
                        "postfix": "SGD"
                    }
                },
                "outgoing": {
                    "value": 0,
                    "unit_type": "seconds",
                    "price_total": {
                        "prefix": "$",
                        "value": 10,
                        "postfix": "SGD"
                    }
                }
            },
            "sms_idd": {
                "visible": true,
                "total": {
                    "value": 3,
                    "unit_type": "sms",
                    "price_total": {
                        "prefix": "$",
                        "value": 5,
                        "postfix": "SGD"
                    }
                }
            },
            "data_roam": {
                "visible": true,
                "total": {
                    "value": 0,
                    "unit_type": "kilobytes",
                    "price_total": {
                        "prefix": "$",
                        "value": 25,
                        "postfix": "SGD"
                    }
                }
            }
        }
    }
    });
}

function dataGet(req, res) {
    res.json({
        "basic": {
            "used": 0,
            "left": 3145727.998046875,
            "unit": "kilobytes",
            "prorated": false,
            "prorated_kb": 0,
            "plan_kb": 3145728
        },
        "extra": {
            "used": 0,
            "left": 1258291.01171875,
            "unit": "kilobytes",
            "prorated": false,
            "prorated_kb": 0,
            "plan_kb": 12582912
        },
        "boost": {
            "used": 0,
            "left": 1024000,
            "unit": "kilobytes",
            "section_min_value": 1048576
        },
        "bonus": {
            "used": 1522858,
            "left": 3124873.1943359375,
            "unit": "kilobytes"
        },
        "promotion_text": {
            "line1": "Bonus",
            "line2": "4.4 GB"
        }
    });
}

function otherGet(req, res) {
    res.json(
        {
            "basic": {
                "calls": {
                    "used": 3000,
                    "left": 3000,
                    "base_used": 3000,
                    "base_left": 3000,
                    "extra_used": 0,
                    "extra_left": 0,
                    "unit_type": "seconds"
                },
                "sms": {
                    "used": 0,
                    "left": 200,
                    "base_used": 0,
                    "base_left": 0,
                    "extra_used": 0,
                    "extra_left": 200,
                    "unit_type": "sms"
                }
            },
            "pay_as_you_go": {
                "calls": {
                    "total": {
                        "value": 600,
                        "unit_type": "seconds",
                        "price_per_unit": {
                            "prefix": "$",
                            "value": 0.1,
                            "postfix": "SGD",
                            "unit": "minutes"
                        },
                        "price_total": {
                            "prefix": "$",
                            "value": 20.5,
                            "postfix": "SGD"
                        }
                    }
                },
                "sms": {
                    "total": {
                        "value": 10,
                        "unit_type": "sms",
                        "price_per_unit": {
                            "prefix": "$",
                            "value": 0.1,
                            "postfix": "SGD",
                            "unit": "sms"
                        },
                        "price_total": {
                            "prefix": "$",
                            "value": 10.05,
                            "postfix": "SGD"
                        }
                    }
                },
                "calls_idd":{
                    "visible":true,
                    "total":{
                        "value":0,
                        "unit_type":"seconds",
                        "price_total":{
                            "prefix":"$",
                            "value":10,
                            "postfix":"SGD"
                        }
                    },
                    "incoming":{
                        "value":0,
                        "unit_type":"seconds",
                        "price_total":{
                            "prefix":"$",
                            "value":0,
                            "postfix":"SGD"
                        }
                    },
                    "outgoing":{
                        "value":0,
                        "unit_type":"seconds",
                        "price_total":{
                            "prefix":"$",
                            "value":10,
                            "postfix":"SGD"
                        }
                    }
                },
                "sms_idd":{
                    "visible":true,
                    "total":{
                        "value":3,
                        "unit_type":"sms",
                        "price_total":{
                            "prefix":"$",
                            "value":5.0,
                            "postfix":"SGD"
                        }
                    }
                },
                "data_roam":{
                    "visible":true,
                    "total":{
                        "value":0,
                        "unit_type":"kilobytes",
                        "price_total":{
                            "prefix":"$",
                            "value":25,
                            "postfix":"SGD"
                        }
                    }
                }
            }
        });
}

function historyUsageGet(req, res) {
    res.json([
        {
            "usage_type": "bills",
            "order_id": 0,
            "containers": [
                {
                    "value":45.0,
                    "prefix":"$",
                    "postfix":"SGD",
                    "unit_type":"price",
                    "date":"2016-07-31T16:00:00.000Z"
                },
                {
                    "value":65.0,
                    "prefix":"$",
                    "postfix":"SGD",
                    "unit_type":"price",
                    "date":"2016-06-30T16:00:00.000Z"
                },
                {
                    "value":60.0,
                    "prefix":"$",
                    "postfix":"SGD",
                    "unit_type":"price",
                    "date":"2016-05-31T16:00:00.000Z"
                }
            ]
/*        },
        {
            "usage_type": "data",
            "order_id": 1,
            "containers": [
                {
                    "value": 4000000,
                    "prefix": "",
                    "postfix": "",
                    "unit_type": "kilobytes",
                    "date": "2016-07-31T16:00:00.000Z"
                },
                {
                    "value": 5000000,
                    "prefix": "",
                    "postfix": "",
                    "unit_type": "kilobytes",
                    "date": "2016-06-30T16:00:00.000Z"
                },
                {
                    "value": 4000000,
                    "prefix": "",
                    "postfix": "",
                    "unit_type": "kilobytes",
                    "date": "2016-05-31T16:00:00.000Z"
                }
            ]
        },
        {
            "usage_type": "calls",
            "order_id": 2,
            "containers": [
                {
                    "value": 1000,
                    "prefix": "",
                    "postfix": "",
                    "unit_type": "seconds",
                    "date": "2016-07-31T16:00:00.000Z"
                },
                {
                    "value": 700,
                    "prefix": "",
                    "postfix": "",
                    "unit_type": "seconds",
                    "date": "2016-06-30T16:00:00.000Z"
                },
                {
                    "value": 800,
                    "prefix": "",
                    "postfix": "",
                    "unit_type": "seconds",
                    "date": "2016-05-31T16:00:00.000Z"
                }
            ]
        },
        {
            "usage_type": "sms",
            "order_id": 3,
            "containers": [
                {
                    "value": 100,
                    "prefix": "",
                    "postfix": "",
                    "unit_type": "sms",
                    "date": "2016-07-31T16:00:00.000Z"
                },
                {
                    "value": 70,
                    "prefix": "",
                    "postfix": "",
                    "unit_type": "sms",
                    "date": "2016-06-30T16:00:00.000Z"
                },
                {
                    "value": 80,
                    "prefix": "",
                    "postfix": "",
                    "unit_type": "sms",
                    "date": "2016-05-31T16:00:00.000Z"
                }
            ]
        },
        {
            "usage_type": "idd",
            "order_id": 4,
            "containers": [
                {
                    "value": 15,
                    "prefix": "$",
                    "postfix": "SGD",
                    "unit_type": "price",
                    "date": "2016-07-31T16:00:00.000Z"
                },
                {
                    "value": 20,
                    "prefix": "$",
                    "postfix": "SGD",
                    "unit_type": "price",
                    "date": "2016-06-30T16:00:00.000Z"
                },
                {
                    "value": 5,
                    "prefix": "$",
                    "postfix": "SGD",
                    "unit_type": "price",
                    "date": "2016-05-31T16:00:00.000Z"
                }
            ]
        },
        {
            "usage_type": "calls_roaming",
            "order_id": 5,
            "containers": [
                {
                    "value": 20,
                    "prefix": "$",
                    "postfix": "SGD",
                    "unit_type": "price",
                    "date": "2016-07-31T16:00:00.000Z"
                },
                {
                    "value": 25,
                    "prefix": "$",
                    "postfix": "SGD",
                    "unit_type": "price",
                    "date": "2016-06-30T16:00:00.000Z"
                },
                {
                    "value": 10,
                    "prefix": "$",
                    "postfix": "SGD",
                    "unit_type": "price",
                    "date": "2016-05-31T16:00:00.000Z"
                }
            ]
        },
        {
            "usage_type": "data_roaming",
            "order_id": 6,
            "containers": [
                {
                    "value": 20,
                    "prefix": "$",
                    "postfix": "SGD",
                    "unit_type": "price",
                    "date": "2016-07-31T16:00:00.000Z"
                },
                {
                    "value": 30,
                    "prefix": "$",
                    "postfix": "SGD",
                    "unit_type": "price",
                    "date": "2016-06-30T16:00:00.000Z"
                },
                {
                    "value": 15,
                    "prefix": "$",
                    "postfix": "SGD",
                    "unit_type": "price",
                    "date": "2016-05-31T16:00:00.000Z"
                }
            ]
*/        }
    ]);
}

function gentwoGet(req, res) {
    res.json({
        "installed": true,
        "credits": {
            "used": {
                "prefix": "$",
                "value": 3,
                "postfix": "SGD"
            },
            "left": {
                "prefix": "$",
                "value": 0.5,
                "postfix": "SGD"
            },
            "start_date": "2015-11-15T12:00:00.000Z",
            "end_date": "2015-12-15T12:00:00.000Z"
        },
        "addons_subscribed": {
            "general": [
                {
                    "id": "8000002",
                    "title": "Free Circles Calls",
                    "subtitle": "Free app-app calls to other Circles users.",
                    "description_short": "Free app-app calls to other Circles users.",
                    "description_full": "Free app-app calls to other Circles users.",
                    "price": {
                        "prefix": "$",
                        "value": 0.0,
                        "postfix": "SGD"
                    },
                    "recurrent": false,
                    "payment": "immediate",
                    "action_available": false,
                    "order_id": 1
                }
            ],
            "history": [
                {
                    "id": "90000001",
                    "type": "credits",
                    "value": 0.99,
                    "app": "gentwo",
                    "unit": "price",
                    "price": {
                        "prefix": "$",
                        "value": 0.99,
                        "postfix": "SGD"
                    },
                    "recurrent": true,
                    "date": "2015-11-20T12:10:00.000Z",
                    "order_id": 0
                },
                {
                    "id": "90000001",
                    "type": "credits",
                    "value": 0.99,
                    "app": "gentwo",
                    "unit": "price",
                    "price": {
                        "prefix": "$",
                        "value": 0.99,
                        "postfix": "SGD"
                    },
                    "recurrent": true,
                    "date": "2015-11-19T12:20:00.000Z",
                    "order_id": 0
                },
                {
                    "id": "90000003",
                    "type": "general",
                    "value": 9.99,
                    "app": "gentwo",
                    "unit": "price",
                    "price": {
                        "prefix": "$",
                        "value": 9.99,
                        "postfix": "SGD"
                    },
                    "recurrent": true,
                    "date": "2015-11-19T12:33:00.000Z",
                    "order_id": 2
                }
            ]
        }
    });
}

function historyBillsGet(req, res) {
    res.json({
        "bills":[
            {
                "id":"REG0000000115111",
                "type":"Regular Invoice",
                "show_price":true,
                "bill_start":"2016-07-01T00:00:00.000Z",
                "bill_end":"2016-07-31T00:00:00.000Z",
                "price":{
                    "prefix":"$",
                    "value":45,
                    "postfix":"SGD"
                },
                "correctedPrice":{
                    "prefix":"$",
                    "value":45,
                    "postfix":"SGD"
                },
                "paid":{
                    "prefix":"$",
                    "value":45,
                    "postfix":"SGD"
                },
                "date":"2016-07-31T16:00:00.000Z",
                "label":"1-31 July 2016",
                "view":false,
                "adjustedFrom":{
                    "prefix":"$",
                    "value":45,
                    "postfix":"SGD"
                },
                "adjustedTo":{
                    "prefix":"$",
                    "value":45,
                    "postfix":"SGD"
                },
                "debitNotes":[

                ],
                "adjustments":[

                ]
            },
            {
                "id":"REG0000000095572",
                "type":"Regular Invoice",
                "show_price":true,
                "bill_start":"2016-06-01T00:00:00.000Z",
                "bill_end":"2016-06-30T00:00:00.000Z",
                "price":{
                    "prefix":"$",
                    "value":65,
                    "postfix":"SGD"
                },
                "correctedPrice":{
                    "prefix":"$",
                    "value":65,
                    "postfix":"SGD"
                },
                "paid":{
                    "prefix":"$",
                    "value":65,
                    "postfix":"SGD"
                },
                "date":"2016-06-30T16:00:00.000Z",
                "label":"1-30 June 2016",
                "view":false,
                "adjustedFrom":{
                    "prefix":"$",
                    "value":65,
                    "postfix":"SGD"
                },
                "adjustedTo":{
                    "prefix":"$",
                    "value":65,
                    "postfix":"SGD"
                },
                "debitNotes":[

                ],
                "adjustments":[

                ]
            },
            {
                "id":"REG0000000085328",
                "type":"Regular Invoice",
                "show_price":true,
                "bill_start":"2016-05-01T00:00:00.000Z",
                "bill_end":"2016-05-31T00:00:00.000Z",
                "price":{
                    "prefix":"$",
                    "value":60,
                    "postfix":"SGD"
                },
                "correctedPrice":{
                    "prefix":"$",
                    "value":60,
                    "postfix":"SGD"
                },
                "paid":{
                    "prefix":"$",
                    "value":60,
                    "postfix":"SGD"
                },
                "date":"2016-05-31T16:00:00.000Z",
                "label":"1-31 May 2016",
                "view":false,
                "adjustedFrom":{
                    "prefix":"$",
                    "value":60,
                    "postfix":"SGD"
                },
                "adjustedTo":{
                    "prefix":"$",
                    "value":60,
                    "postfix":"SGD"
                },
                "debitNotes":[

                ],
                "adjustments":[

                ]
            }
        ],
        "instant_charges":{
            "list":[
                {
                    "id":"CR/DEF000000000000000000016238",
                    "label":"Boost 500MB",
                    "paid":{
                        "prefix":"$",
                        "value":3,
                        "postfix":"SGD"
                    },
                    "date":"2016-08-07T16:00:00.000Z"
                },
                {
                    "id":"CR/DEF000000000000000000016235",
                    "label":"Boost 500MB",
                    "paid":{
                        "prefix":"$",
                        "value":3,
                        "postfix":"SGD"
                    },
                    "date":"2016-08-05T16:00:00.000Z"
                },
                {
                    "id":"CR/DEF000000000000000000016189",
                    "label":"Boost 500MB",
                    "paid":{
                        "prefix":"$",
                        "value":3,
                        "postfix":"SGD"
                    },
                    "date":"2016-08-04T16:00:00.000Z"
                }
            ],
            "label":"1-31 August 2016",
            "paid":{
                "prefix":"$",
                "value":9,
                "postfix":"SGD"
            }
        },
        "refunds":{
            "label":"Included into 1st September bill",
            "paid":{
                "prefix":"$",
                "value":5,
                "postfix":"SGD"
            }
        },
        "creditCap": {
            "paymentRequired": false,
            "paymentAmount": {
                "prefix": "$",
                "value": 200,
                "postfix": ""
            },
            "outstandingAmount": {
                "prefix": "$",
                "value": null,
                "postfix": ""
            }
        }
    });
}

function historyBillsDetails(req, res) {
    res.json(
        {
            "basic": {
                "data": {
                    "value": 1024000,
                    "unit": "kilobytes"
                },
                "calls": {
                    "value": 50000,
                    "unit": "seconds"
                },
                "sms": {
                    "value": 100,
                    "unit": "sms"
                },
                "total_price": {
                    "prefix": "$",
                    "value": 50,
                    "postfix": "SGD"
                }
            },
            "bonus": [
                {
                    "title": "Circles Bonus Data",
                    "value": 102400,
                    "unit": "kilobytes"
                },
                {
                    "title": "Referral Bonus Data",
                    "value": 512000,
                    "unit": "kilobytes"
                }
            ],
            "plus": {
                "general": [],
                "circlestalk_credits": [],
                "circlestalk": [
                    {
                        "title": "CirclesTalk Free SG Calls",
                        "price": {
                            "prefix": "$",
                            "value": 15,
                            "postfix": "SGD"
                        },
                        "date": "2015-11-14T12:00:00.000Z"
                    },
                    {
                        "title": "Unlimited calls on Black Friday",
                        "price": {
                            "prefix": "$",
                            "value": 10,
                            "postfix": "SGD"
                        },
                        "date": "2015-11-14T12:00:00.000Z"
                    }
                ],
                "total_price": {
                    "prefix": "$",
                    "value": 25,
                    "postfix": "SGD"
                }
            },
            "pay_as_use": {
                "local": {
                    "data": {
                        "value": "204800",
                        "unit": "kilobytes",
                        "price": {
                            "prefix": "$",
                            "value": 10,
                            "postfix": "SGD"
                        }
                    },
                    "calls": {
                        "value": "36000",
                        "calls_count": 5,
                        "unit": "seconds",
                        "price": {
                            "prefix": "$",
                            "value": 5,
                            "postfix": "SGD"
                        }
                    },
                    "sms": {
                        "value": "1000",
                        "unit": "sms",
                        "price": {
                            "prefix": "$",
                            "value": 2,
                            "postfix": "SGD"
                        }
                    },
                    "total_price": {
                        "prefix": "$",
                        "value": 17,
                        "postfix": "SGD"
                    }
                },
                "roaming": {
                    "data": {
                        "value": "204800",
                        "unit": "kilobytes",
                        "price": {
                            "prefix": "$",
                            "value": 15,
                            "postfix": "SGD"
                        }
                    },
                    "calls": {
                        "value": "36000",
                        "calls_count": 2,
                        "unit": "seconds",
                        "price": {
                            "prefix": "$",
                            "value": 4,
                            "postfix": "SGD"
                        }
                    },
                    "sms": {
                        "value": "1000",
                        "unit": "sms",
                        "price": {
                            "prefix": "$",
                            "value": 1,
                            "postfix": "SGD"
                        }
                    },
                    "total_price": {
                        "prefix": "$",
                        "value": 20,
                        "postfix": "SGD"
                    }
                },
                "idd": {
                    "calls": {
                        "value": "36000",
                        "calls_count": 2,
                        "unit": "seconds",
                        "price": {
                            "prefix": "$",
                            "value": 5,
                            "postfix": "SGD"
                        }
                    },
                    "sms": {
                        "value": "1000",
                        "unit": "sms",
                        "price": {
                            "prefix": "$",
                            "value": 5,
                            "postfix": "SGD"
                        }
                    },
                    "total_price": {
                        "prefix": "$",
                        "value": 10,
                        "postfix": "SGD"
                    }
                },
                "total_price": {
                    "prefix": "$",
                    "value": 47,
                    "postfix": "SGD"
                }
            },
            "boost": {
                "boost_list": [
                    {
                        "title": "Data Boost 1",
                        "value": "102400",
                        "unit": "kilobytes",
                        "price": {
                            "prefix": "$",
                            "value": 5,
                            "postfix": "SGD"
                        },
                        "date": "2015-11-14T12:00:00.000Z"
                    },
                    {
                        "title": "Data Boost 2",
                        "value": "204800",
                        "unit": "kilobytes",
                        "price": {
                            "prefix": "$",
                            "value": 10,
                            "postfix": "SGD"
                        },
                        "date": "2015-11-14T12:00:00.000Z"
                    }
                ],
                "total_price": {
                    "prefix": "$",
                    "value": 15,
                    "postfix": "SGD"
                }
            }
        }
    );
}

function callDetailsGet(req, res) {
    var reply = [
        {
            "unit": "seconds",
            "duration": 500,
            "recipient_number": "+ 65 87652399",
            "price": {
                "prefix": "$",
                "value": 3,
                "postfix": "SGD"
            },
            "iso_code": "ER",
            "date": "2015-11-14T12:00:00.000Z"
        },
        {
            "unit": "seconds",
            "duration": 330,
            "recipient_number": "+ 65 87654399",
            "price": {
                "prefix": "$",
                "value": 2,
                "postfix": "SGD"
            },
            "iso_code": "GH",
            "date": "2015-11-14T12:00:00.000Z"
        },
        {
            "unit": "seconds",
            "duration": 50,
            "recipient_number": "+ 65 87655399",
            "price": {
                "prefix": "$",
                "value": 3,
                "postfix": "SGD"
            },
            "iso_code": "IN",
            "date": "2015-11-14T12:00:00.000Z"
        }
    ];
    res.json(reply);
}

function historyBillsResend(req, res) {
    res.json({code: 0});
}

function paymentSet(req, res) {
    res.json({code: 0});
}
