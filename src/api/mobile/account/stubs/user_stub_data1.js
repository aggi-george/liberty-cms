var account = require('../account');
var common = require(__lib + '/common');

//exports

exports.profileDetailsGet = profileDetailsGet;
exports.planDetailsGet = planDetailsGet;
exports.profileBoostsGet = profileBoostsGet;
exports.profileDevicesGet = profileDevicesGet;
exports.creditCardUpdate = creditCardUpdate;
exports.billingAddressUpdateSet = billingAddressUpdateSet;
exports.emailUpdateSet = emailUpdateSet;
exports.dobVerifyGet = dobVerifyGet;
exports.identityNumberVerifyGet = identityNumberVerifyGet;
exports.guideScreensGet = guideScreensGet;
exports.getPortInStatus = getPortInStatus;
exports.putPortInRequest = putPortInRequest;
exports.logsAppStates = logsAppStates;

//functions

function profileDetailsGet(req, res) {
    res.json({
        "first_name": "John",
        "last_name": "Woo",
        "middle_name": "H.",
        "email": {
            "can_edit": "true",
            "visible": "true",
            "value": "john@circles.life",
            "status": "verification_done" //verification_done, verification_pending, verification_failed
        },
        "billing_address": {
            "street_building_name": "221A Henderson Rd",
            "unit_no": "2",
            "floor_no": "6",
            "hse_blk_tower": "1",
            "zip_code": "159558",
            "city": "Singapore",
            "country": "Singapore",
            "can_edit": "true",
            "visible": "true"
        },
        "credit_card": {
            "can_edit": "true",
            "visible": "true",
            "last_four_digits": "1234",
            "paytype": "amex"
        },
        "port_in" : {
            "visible" : true
        }
    });
}

function planDetailsGet(req, res) {
    res.json({
        "billing_cycle": {
            "start_date": "2016-08-01T00:00:00.000Z",
            "end_date": "2016-09-01T00:00:00.000Z"
        },
        "bill": {
            "price": {
                "prefix": "$",
                "value": 48,
                "postfix": "SGD"
            },
            "due_by_date": "2016-09-01"
        },
        "devices": [],
        "basic_plan": {
            "price": {
                "prefix": "$",
                "value": 28,
                "postfix": "SGD"
            },
            "data": {
                "value": 3145728,
                "unit": "kilobyte"
            },
            "calls": {
                "value": 6000,
                "unit": "seconds"
            },
            "sms": {
                "value": 0,
                "unit": "sms"
            },
            "components_changed": false
        },
        "addons_subscribed": {
            "extra": {
                "data": {
                    "id": "PRD00360",
                    "type": "data",
                    "value": 2097152,
                    "unit": "kilobyte",
                    "price": {
                        "prefix": "$",
                        "value": 12,
                        "postfix": "SGD"
                    },
                    "payment": "postponed",
                    "date": "2016-07-31T16:00:00.000Z"
                },
                "sms": {
                    "id": "PRD00530",
                    "type": "sms",
                    "value": 200,
                    "unit": "sms",
                    "price": {
                        "prefix": "$",
                        "value": 8,
                        "postfix": "SGD"
                    },
                    "payment": "postponed",
                    "date": "2016-07-31T16:00:00.000Z"
                }
            },
            "bonus": {
                "data": [
                    {
                        "id": "PRD00620",
                        "type": "data",
                        "value": 212457,
                        "unit": "kilobyte",
                        "price": {
                            "prefix": "$",
                            "value": 0,
                            "postfix": "SGD"
                        },
                        "date": "2016-08-12T07:35:43.000Z"
                    },
                    {
                        "id": "PRD00620",
                        "type": "data",
                        "value": 212457,
                        "unit": "kilobyte",
                        "price": {
                            "prefix": "$",
                            "value": 0,
                            "postfix": "SGD"
                        },
                        "date": "2016-08-12T07:35:43.000Z"
                    },
                    {
                        "id": "PRD00624",
                        "type": "data",
                        "value": 1048576,
                        "unit": "kilobyte",
                        "price": {
                            "prefix": "$",
                            "value": 0,
                            "postfix": "SGD"
                        },
                        "date": "2016-08-12T07:35:43.000Z"
                    },
                    {
                        "id": "PRD00624",
                        "type": "data",
                        "value": 1048576,
                        "unit": "kilobyte",
                        "price": {
                            "prefix": "$",
                            "value": 0,
                            "postfix": "SGD"
                        },
                        "date": "2016-08-12T10:22:23.000Z"
                    },
                    {
                        "id": "PRD00624",
                        "type": "data",
                        "value": 1048576,
                        "unit": "kilobyte",
                        "price": {
                            "prefix": "$",
                            "value": 0,
                            "postfix": "SGD"
                        },
                        "date": "2016-08-12T10:26:54.000Z"
                    },
                    {
                        "id": "PRD00624",
                        "type": "data",
                        "value": 1048576,
                        "unit": "kilobyte",
                        "price": {
                            "prefix": "$",
                            "value": 0,
                            "postfix": "SGD"
                        },
                        "date": "2016-08-12T08:18:26.000Z"
                    }
                ]
            },
            "general": [
                {
                    "id": "PRD00440",
                    "title": "Unlimited WhatsApp Plus",
                    "subtitle": "",
                    "description_short": "Unlimited messaging, photos, videos, and voice calling. ",
                    "description_full": "Unlimited messaging, photos, videos, and voice calling on WhatsApp in every base plan - your monthly data limit is untouched.",
                    "disclaimer": "",
                    "sub_effect": "immediate",
                    "unsub_effect": "immediate",
                    "payment": "postponed",
                    "action_available": false,
                    "order_id": 0,
                    "price": {
                        "prefix": "$",
                        "value": 0,
                        "postfix": "SGD",
                        "discount": null
                    },
                    "recurrent": true,
                    "date": "2016-05-25T09:22:58.000Z",
                    "start_date": "2016-05-25T09:22:58.000Z",
                    "expiry_date": "9999-01-01T15:59:59.000Z",
                    "current": true,
                    "future": true
                },
                {
                    "id": "PRD00316",
                    "title": "Plus 4G Speed Max",
                    "subtitle": "",
                    "description_short": "Enjoy uninterrupted maximum speed on 4G.",
                    "description_full": "Enjoy uninterrupted maximum speed on 4G.",
                    "disclaimer": "",
                    "sub_effect": "immediate",
                    "unsub_effect": "next_bill_cycle",
                    "payment": "postponed",
                    "action_available": false,
                    "order_id": 4,
                    "price": {
                        "prefix": "$",
                        "value": 0,
                        "postfix": "SGD",
                        "discount": null
                    },
                    "recurrent": true,
                    "date": "2016-04-04T06:43:19.000Z",
                    "start_date": "2016-04-04T06:43:19.000Z",
                    "expiry_date": "9999-01-01T15:59:59.000Z",
                    "current": true,
                    "future": true
                },
                {
                    "id": "PRD00317",
                    "title": "Plus Plan Change",
                    "subtitle": "",
                    "description_short": "Customize your plan as your mobile needs change.",
                    "description_full": "Customize your plan as your mobile needs change.",
                    "disclaimer": "",
                    "sub_effect": "immediate",
                    "unsub_effect": "next_bill_cycle",
                    "payment": "postponed",
                    "action_available": false,
                    "order_id": 3,
                    "price": {
                        "prefix": "$",
                        "value": 0,
                        "postfix": "SGD",
                        "discount": null
                    },
                    "recurrent": true,
                    "date": "2016-04-04T06:43:19.000Z",
                    "start_date": "2016-04-04T06:43:19.000Z",
                    "expiry_date": "9999-01-01T15:59:59.000Z",
                    "current": true,
                    "future": true
                },
                {
                    "id": "PRD00463",
                    "title": "Unlimited Incoming Calls",
                    "subtitle": "",
                    "description_short": "Limitless incoming local calls.",
                    "description_full": "Activate limitless incoming local calls that do not take from your Talktime minutes.",
                    "disclaimer": "",
                    "sub_effect": "immediate",
                    "unsub_effect": "next_bill_cycle",
                    "payment": "postponed",
                    "action_available": true,
                    "order_id": 1,
                    "price": {
                        "prefix": "$",
                        "value": 2,
                        "postfix": "SGD",
                        "discount": null
                    },
                    "recurrent": true,
                    "date": "2016-07-31T16:00:00.000Z",
                    "start_date": "2016-07-31T16:00:00.000Z",
                    "expiry_date": "9999-01-01T15:59:59.000Z",
                    "current": true,
                    "future": true
		},
		{
              "id": "roamingGroup",
              "title": "Roaming",
              "description_short": "Activate data, calls and SMS when travelling overseas.",
              "subtitle": "",
              "list": [
                {
                  "id": "roamingOff",
                  "title": "Roaming Off",
                  "description_short": "Turn Off All Roaming",
                  "subtitle": "OFF",
                  "description_full": "You are about to send us a request to turn off all roaming features.\n\nPlease note that it will take some time for this to take effect and you cannot change this setting while the request is being processed."
                }
              ],
              "description_full": ""
            }
            ],
            "topup": [
                {
                    "id": "PRD00407",
                    "type": "data",
                    "value": 102400,
                    "unit": "kilobyte",
                    "price": {
                        "prefix": "$",
                        "value": 1,
                        "postfix": "SGD"
                    },
                    "payment": "postponed",
                    "date": "2016-08-11T04:26:11.000Z",
                    "order_id": 0
                },
                {
                    "id": "PRD00407",
                    "type": "data",
                    "value": 102400,
                    "unit": "kilobyte",
                    "price": {
                        "prefix": "$",
                        "value": 1,
                        "postfix": "SGD"
                    },
                    "payment": "postponed",
                    "date": "2016-08-11T05:46:30.000Z",
                    "order_id": 0
                },
                {
                    "id": "PRD00407",
                    "type": "data",
                    "value": 542400,
                    "unit": "kilobyte",
                    "price": {
                        "prefix": "$",
                        "value": 1,
                        "postfix": "SGD"
                    },
                    "payment": "postponed",
                    "date": "2016-08-11T09:18:09.000Z",
                    "order_id": 0
                }
            ]
        },
        "autoboost": {
            "enabled": true,
            "price": {
                "prefix": "$",
                "value": 1.2,
                "postfix": "SGD"
            },
            "data": {
                "value": 102400,
                "unit": "kilobyte"
            }
        }
    });
}

function profileDevicesGet(req, res) {
    res.json({
        "devices": [
            //{
            //    "id": "1",
            //    "platform": "ios",
            //    "name": "iPhone 6S+",
            //    "capacity": "16GB",
            //    "color_hex": "#000000",
            //    "image": {
            //        "large": "http://images.techtimes.com/data/images/full/26095/iphone-6.jpg?w=600"
            //    },
            //    "instalment_plan": {
            //        "duration_months_count": 24,
            //        "upfront_payment": {
            //            "prefix": "$",
            //            "value": 205.5,
            //            "postfix": "SGD"
            //        },
            //        "monthly_payment": {
            //            "prefix": "$",
            //            "value": 89.99,
            //            "postfix": "SGD"
            //        }
            //    },
            //    "date": "2015-10-05T12:00:00.000Z",
            //    "order_id": 0
            //},
            //{
            //    "id": "2",
            //    "name": "Samsung SG6 Edge",
            //    "capacity": "16GB",
            //    "platform": "android",
            //    "color_hex": "#000000",
            //    "image": {
            //        "large": "http://images.techtimes.com/data/images/full/26095/iphone-6.jpg?w=600"
            //    },
            //    "instalment_plan": {
            //        "duration_months_count": 12,
            //        "upfront_payment": {
            //            "prefix": "$",
            //            "value": 300.4,
            //            "postfix": "SGD"
            //        },
            //        "monthly_payment": {
            //            "prefix": "$",
            //            "value": 99.99,
            //            "postfix": "SGD"
            //        }
            //    },
            //    "date": "2015-12-05T12:00:00.000Z",
            //    "order_id": 0
            //}
        ]
    });
}

function profileBoostsGet(req, res) {
    var boosts = [
        {
            "type": "data",
            "unit": "kilobyte",
            "auto": true,
            "recurrent": false,
            "payment": "immediate",
            "id": "PRD00207",
            "value": 102400,
            "price": {
                "prefix": "$",
                "value": 1.2,
                "postfix": "SGD"
            },
            "order_id": 13,
            "date": "2016-08-02T12:00:00.000Z"
        },
        {
            "type": "data",
            "unit": "kilobyte",
            "auto": true,
            "recurrent": false,
            "payment": "immediate",
            "id": "PRD00207",
            "value": 102400,
            "price": {
                "prefix": "$",
                "value": 1.2,
                "postfix": "SGD"
            },
            "order_id": 13,
            "date": "2016-08-02T12:00:00.000Z"
        },
        {
            "type": "data",
            "unit": "kilobyte",
            "auto": false,
            "recurrent": false,
            "payment": "immediate",
            "id": "PRD00206",
            "value": 512000,
            "price": {
                "prefix": "$",
                "value": 3,
                "postfix": "SGD"},
            "order_id": 1,
            "date": "2016-08-05T12:00:00.000Z"
        },
        {
            "type": "data",
            "unit": "kilobyte",
            "auto": false,
            "recurrent": false,
            "payment": "immediate",
            "id": "PRD00206",
            "value": 512000,
            "price": {
                "prefix": "$",
                "value": 3,
                "postfix": "SGD"},
            "order_id": 1,
            "date": "2016-08-06T12:00:00.000Z"
        },
        {
            "type": "data",
            "unit": "kilobyte",
            "auto": false,
            "recurrent": false,
            "payment": "immediate",
            "id": "PRD00206",
            "value": 512000,
            "price": {
                "prefix": "$",
                "value": 3,
                "postfix": "SGD"},
            "order_id": 1,
            "date": "2016-08-08T12:00:00.000Z"
        }
    ];
    res.json(boosts);
}

function billingAddressUpdateSet(req, res) {
    common.log("addons", "billingAddressUpdateSet: hse_blk_tower=" + req.fields.hse_blk_tower + ", street_building_name=" + req.fields.street_building_name
    + ", floor_no=" + req.fields.floor_no + ", unit_no=" + req.fields.unit_no + ", zip_code=" + req.fields.zip_code);
    res.json({});
}

function emailUpdateSet(req, res) {
    common.log("addons", "emailUpdateSet: email=" + req.fields.email);
    res.json({});
}

function dobVerifyGet(req, res) {
    common.log("addons", "dobVerifyGet: day_of_birth=" + req.fields.day_of_birth + ", month_of_birth=" + req.fields.month_of_birth
    + ", year_of_birth=" + req.fields.year_of_birth);
    res.json({
        "dob_valid": true
    });
}

function identityNumberVerifyGet(req, res) {
    common.log("addons", "dobVerifyGet: id_digits=" + req.fields.id_digits);
    res.json({"code": 0, "valid": true});
}

function creditCardUpdate(req, res) {
    res.status(400).json({"description": "Not supported"});
}

function guideScreensGet(req, res) {
    common.log("addons", "guideScreensGet: ignore_ids=" + req.fields.ignore_ids);
    res.json([
        {
            "id": "12345",
            "order_id": 0,
            "content": {
                "title": "Title1",
                "subtitle": "Subtitle1",
                "text": "Text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text",
                "transition_time_ms": 300,
                "display_time_ms": 1500,
                "images": [
                    {"url": "http://www.keenthemes.com/preview/conquer/assets/plugins/jcrop/demos/demo_files/image1.jpg"},
                    {"url": "http://www.keenthemes.com/preview/conquer/assets/plugins/jcrop/demos/demo_files/image2.jpg"},
                    {"url": "http://referentiel.nouvelobs.com/file/13572027.jpg"},
                    {"url": "http://static.guim.co.uk/sys-images/Guardian/Pix/pictures/2014/4/11/1397210130748/Spring-Lamb.-Image-shot-2-011.jpg"}
                ]
            }
        },
        {
            "id": "09876",
            "order_id": 1,
            "content": {
                "title": "Title2",
                "subtitle": "Subtitle2",
                "text": "Text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text, text",
                "transition_time_ms": 1000,
                "display_time_ms": 3500,
                "images": [
                    {"url": "http://www.keenthemes.com/preview/conquer/assets/plugins/jcrop/demos/demo_files/image1.jpg"},
                    {"url": "http://www.keenthemes.com/preview/conquer/assets/plugins/jcrop/demos/demo_files/image2.jpg"},
                    {"url": "http://referentiel.nouvelobs.com/file/13572027.jpg"},
                    {"url": "http://static.guim.co.uk/sys-images/Guardian/Pix/pictures/2014/4/11/1397210130748/Spring-Lamb.-Image-shot-2-011.jpg"}
                ]
            }
        }
    ]);
}

function getPortInStatus(req, res) {
    var dates = new Array;
    var now = new Date();
    for (var i=0; i<15; i++) {
        dates.push({ "date" : new Date(now.getFullYear(), now.getMonth(), now.getDate() + i).toISOString() });
    }
    res.json({ "result" : {
        "country_code" : 65, "status" : "NOT_FOUND", "auth_form_url" : "https:\/\/www.circles.life\/ownershipform.pdf",
        "start_dates": dates, "donor_networks" : [
            { "name" : "M1", "code" : "001" },
            { "name":"StarHub","code":"002" },
            { "name":"Singtel","code":"003" } ]
    }, "code" : 0 });
}

function putPortInRequest(req, res) {
    res.json({ "code" : 0 });
}

function logsAppStates(req, res) {
    res.json({ code: 0, insertedCount: 3 });
}
