var config = require(__base + '/config');
var common = require(__lib + '/common');
var ec = require(__lib + '/elitecore');
var db = require(__lib + '/db_handler');
var usageManager = require('../../../core/manager/account/usageManager');
var sessionManager = require('../../base/sessionManager');
var elitecoreConnector = require(__lib + '/manager/ec/elitecoreConnector');
var resourceManager = require('../../../../res/resourceManager');
var request = require('request');
var addons = require('./addons');

var elitecoreUserService = require('../../../../lib/manager/ec/elitecoreUserService');
var billManager = require('../../../core/manager/account/billManager');
var paymentManager = require('../../../core/manager/account/paymentManager');
var BaseError = require('../../../core/errors/baseError');

var LOG = config.LOGSENABLED;

//exports

exports.usagePlanGet = usagePlanGet;
exports.dataGet = dataGet;
exports.otherGet = otherGet;
exports.historyUsageGet = historyUsageGet;
exports.historyBillsGet = historyBillsGet;
exports.historyBillsResend = historyBillsResend;
exports.gentwoGet = gentwoGet;
exports.callDetailsGet = callDetailsGet;
exports.paymentSet = paymentSet;

//functions

function usagePlanGet(req, res) {		// do not use uuid here or else, top-up won't show right away
    ec.getCache(req.params.user_key, false, function (err, cache) {
        if (err) {
            return res.status(400).json({
                code: -1,
                details: err.message,
                description: "Failed to customer"
            });
        }
        if (!cache) {
            return res.status(400).json({
                code: -1,
                details: "Customer not found",
                description: "Customer not found"
            });
        }
        usageManager.computeUsage(cache.prefix, cache.number, function (err, usageResult) {
            if (err) {
                return res.status(400).json({
                    code: -1,
                    details: err.message,
                    description: "Failed to load data"
                });
            }
            res.json(usageResult);
        });
    });
}

function dataGet(req, res) {		// do not use uuid here or else, top-up won't show right away
    ec.getCache(req.params.user_key, false, function (err, cache) {
        if (err) {
            return res.status(400).json({
                code: -1,
                details: err.message,
                description: "Failed to customer"
            });
        }
        if (!cache) {
            return res.status(400).json({
                code: -1,
                details: "Customer not found",
                description: "Customer not found"
            });
        }
        usageManager.computeUsage(cache.prefix, cache.number, function (err, usageResult) {
            if (err) {
                return res.status(400).json({
                    code: -1,
                    details: err.message,
                    description: "Failed to load data"
                });
            }
            res.json(usageResult ? usageResult.data : {});
        });
    });
}

function otherGet(req, res) {
    ec.getCache(req.params.user_key, false, function (err, cache) {
        if (err) {
            return res.status(400).json({
                code: -1,
                details: err.message,
                description: "Failed to customer"
            });
        }
        if (!cache) {
            return res.status(400).json({
                code: -1,
                details: "Customer not found",
                description: "Customer not found"
            });
        }
        usageManager.computeUsage(cache.prefix, cache.number, function (err, usageResult) {
            if (err) {
                return res.status(400).json({
                    code: -1,
                    details: err.message,
                    description: "Failed to load data"
                });
            }
            res.json(usageResult ? usageResult.other : {});
        });
    });
}

function paymentSet(req, res) {
    var invoiceId = req.fields.invoiceId;
    var amount = req.fields.amount ? parseFloat(req.fields.amount) : -1;
    var creditCap = req.fields.creditCap == 'true' || req.fields.creditCap == '1';

    var userKey = req.params.user_key;
    var uuid = (req.fields.uuid ? req.fields.uuid : "paymentSet") + "_" + invoiceId + "_" + creditCap;

    var handleResponseResult = function (err, result) {
        var status = err ? 400 : 200;
        var result = err ? resourceManager.getErrorValues(err) : {code: 0};
        sessionManager.cacheAndReply(res, userKey, uuid, status, result);
    }

    sessionManager.uuidCheck(res, userKey, uuid, function () {
        if (LOG) common.log("MobileUsageManager", "paymentSet: userKey=" + userKey
        + ", invoiceId=" + invoiceId + ", amount=" + amount + ", creditCap=" + creditCap);

        elitecoreUserService.loadUserInfoByUserKey(userKey, function (err, cache) {
            if (err) {
                return handleResponseResult(err);
            }
            if ((!amount || !invoiceId) && !creditCap) {
                return handleResponseResult(new BaseError("Invalid params", "ERROR_INVALID_PARAMS"));
            }

            if (invoiceId) {
                paymentManager.makeBillPaymentByBA(cache.billingAccountNumber, [{
                    invoiceId: invoiceId,
                    amount: amount
                }], {
                    platform: "SELFCARE",
                    ignoreSuspension: true
                }, function (err, result) {
                    if (err) {
                        return handleResponseResult(err);
                    }
                    if (!result || !result.paymentResults || !result.paymentResults[0] || !result.paymentResults[0].paymentStatus) {
                        return handleResponseResult(new BaseError("Transaction status missing", "ERROR_NO_TRANSACTION_STATUS"));
                    }
                    if (result.paymentResults[0].paymentStatus != 'OK') {
                        var errorMessage = "Payment failed due to " + result.paymentResults[0].paymentStatus;
                        return handleResponseResult(new BaseError(errorMessage, result.paymentResults[0].paymentStatus));
                    }
                    handleResponseResult(undefined, result, cache);
                });
            } else if (creditCap) {
                paymentManager.makeCreditCapPaymentByBA(cache.billingAccountNumber, {
                    platform: "SELFCARE"
                }, (err, result) => {
                    if (err) {
                        return handleResponseResult(err);
                    }
                    if (!result || !result.paymentResult || !result.paymentResult.paymentStatus) {
                        return handleResponseResult(new BaseError("Transaction status missing", "ERROR_NO_TRANSACTION_STATUS"));
                    }
                    if (result.paymentResult.paymentStatus != 'OK') {
                        var errorMessage = "Payment failed due to " + result.paymentResult.paymentStatus;
                        return handleResponseResult(new BaseError(errorMessage, result.paymentResult.paymentStatus));
                    }
                    handleResponseResult(undefined, result, cache);
                });
            }
        });
    });
}

function historyUsageGet(req, res) {
    ec.getCache(req.params.user_key, false, function (err, cache) {
        if (err) {
            return res.status(400).json({
                code: -1,
                details: err.message,
                description: "Failed to load bill history"
            });
        }

        if (LOG) common.log("MobileUsageManager", "historyUsageGet: number=" + cache.number);
        billManager.loadBillHistory(cache.prefix, cache.number, function (err, result) {
            if (err) {
                return res.status(400).json({
                    code: -1,
                    details: err.message,
                    description: "Failed to load bill history"
                });
            }

            var list = [{
                "usage_type": "bills",
                "order_id": 0,
                "containers": []
            }];

            if (result.bills && (result.bills.length > 1)) {
                result.bills.forEach(function (bill) {
                    if (bill.correctedPrice.value > 0) {
                        list[0].containers.push({
                            "value": bill.correctedPrice.value,
                            "prefix": bill.correctedPrice.prefix,
                            "postfix": bill.correctedPrice.postfix,
                            "unit_type": "price",
                            "date": bill.date
                        });
                    }
                });
            }

            res.json(list);
        });
    });
}

function historyBillsGet(req, res) {
    ec.getCache(req.params.user_key, false, function (err, cache) {
        if (err) {
            return res.status(400).json({
                code: -1,
                details: err.message,
                description: "Failed to load bill history"
            });
        }

        if (LOG) common.log("MobileUsageManager", "historyBillsGet: number=" + cache.number);
        billManager.loadBillHistory(cache.prefix, cache.number, function (err, result) {
            if (err) {
                return res.status(400).json({
                    code: -1,
                    details: err.message,
                    description: "Failed to load bill history"
                });
            }

            res.json(result);
        });
    });
}

function historyBillsResend(req, res) {
    if (LOG) common.log("historyBillsResend", "resend request, uuid=" + req.fields.uuid);
    var uuid = (req.fields.uuid) ? req.fields.uuid : "historyBillsResend";

    addons.uuidCheck(res, req.params.user_key, uuid, function () {

        var saveResult = function (status, result) {
            if (LOG) common.log("historyBillsResend", "done, uuid=" + req.fields.uuid);
            if (!res.headerSent) res.status(status).json(result);

            db.cache_hput("uuid_cache", req.params.user_key + "_" + uuid, {
                "status": status,
                "reply": JSON.stringify(result)
            }, 120000);
        }

        ec.getCache(req.params.user_key, false, function (err, cache) {
            if (err || !cache) {
                saveResult(400, {code: -1});
            } else {
                var key = config.NOTIFICATION_KEY;
                var url = "http://" + config.CMSHOST + ":" + config.CMSPORT
                    + "/api/1/webhook/notifications/internal/" + key;

                var invoicenumber = req.fields.id;
                var archive = req.fields.date && req.fields.date.length >= 10
                    ? req.fields.date.substring(0, 10) : undefined;

                if (!invoicenumber || !archive) {
                    common.error("historyBillsResend", "Can not send bill, some params are missing, invoicenumber="
                    + invoicenumber + ", archive=" + archive);

                    return saveResult(400, {
                        code: -1,
                        description: "Oops... We were not able to send your bill. " +
                        "Please try again or contact our customer service experts to get more details.",
                        details: "Some params are missing"
                    });
                }

                if (LOG) common.log("historyBillsResend", "Recent build, number=" + cache.number + ", archive=" + archive
                + ", invoicenumber=" + invoicenumber);

                request({
                    uri: url,
                    method: 'POST',
                    timeout: 20000,
                    json: {
                        "activity": "bill_resend",
                        "number": cache.number,
                        "email": cache.email,
                        "team": "Mobile App",
                        "variables": {
                            "archive": archive,
                            "invoicenumber": invoicenumber
                        }
                    }
                }, function (error, response, body) {
                    if (error || !body || body.code != 0) {
                        if (error) {
                            common.error("historyBillsResend", "Notification is sent to an old email, error=" + error);
                        } else {
                            common.error("historyBillsResend", "Notification is not sent, body=" + JSON.stringify(body));
                        }

                        if (new Date().getTime() - new Date(archive).getTime() < 20 * (1000 * 60 * 60 * 24)) {
                            return saveResult(400, {
                                code: common.BILL_FILE_NOT_FOUND_OR_INVALID,
                                title: "Resend Bill",
                                description: "Your bill is still being processed. It may take a couple more days. Please try again.",
                                details: (error ? error.message : (body ? body.error : undefined))
                            });
                        } else {
                            return saveResult(400, {
                                code: common.BILL_FILE_NOT_FOUND_OR_INVALID,
                                title: "Bill Not Found",
                                description: "Oops... We were not able to send your bill. " +
                                "Please try again or contact our customer service experts to get more details.",
                                details: (error ? error.message : (body ? body.error : undefined))
                            });
                        }
                    }

                    return saveResult(200, {code: 0});
                });
            }
        });
    });


}

function gentwoCredits(number, callback) {
    var q = "SELECT IF(m.credit<0,0,m.credit) credit, IF(m.credit*cu.vsUSD<0,0,m.credit*cu.vsUSD) local_credit, cu.currencySymbol, cu.currencyCode, cu.vsUSD ex, a.status FROM msisdn m LEFT JOIN countries co ON m.iso=co.alpha2code LEFT JOIN currencies cu ON co.currency=cu.currencyCode, app a WHERE a.number=m.number AND a.app_type='gentwo' AND a.number=" + db.escape(number);
    db.query_noerr(q, function (rows) {	// watch out! could have race condition, assume this is faster than ec
        if (rows && rows[0]) {
            var gentwo_enabled = ( rows[0].status == "A" ) ? true : false;
            var gentwo_left = parseFloat(rows[0].local_credit);
            var exchange = parseFloat(rows[0].ex);
            callback(gentwo_enabled, gentwo_left, exchange);
        }
    });
}

function gentwoGet(req, res) {
    ec.getCache(req.params.user_key, false, function (err, value) {
        if (err) {
            res.status(400).json(err);
        } else {
            var reply = new Object;
            var gentwo_used = 0;
            var gentwo_left = 0;
            reply.credits = new Object;
            gentwoCredits(value.prefix + "" + value.number, function (gentwo_enabled, gentwo_left, exchange) {
                reply.installed = gentwo_enabled;
                reply.credits.left = {"prefix": "$", "value": gentwo_left, "postfix": "SGD"};
                var cycle = ec.nextBill(value.billing_cycle);
                db.getUsage(value.prefix + "" + value.number, new Date(cycle.start).getTime() / 1000, function (total) {
                    reply.credits.used = {"prefix": "$", "value": Math.ceil(total * exchange * 100) / 100, "postfix": "SGD"};
                    reply.credits.start_date = new Date(cycle.start);
                    reply.credits.end_date = new Date(cycle.end);
                    reply.addons_subscribed = new Object;
                    reply.addons_subscribed.general = new Array;
                    value.general.forEach(function (item) {
                        ec.packages().general.forEach(function (gentwo) {
                            if (gentwo.enabled && (gentwo.app == "gentwo" ) && (gentwo.name == item.name)) {
                                gentwo.id = item.id;
                                gentwo.price = {"prefix": "$", "value": item.price, "postfix": "SGD"};
                                gentwo.payment = "immediate";
                                gentwo.action_available = ( item.price > 0 ) ? true : false;
                                gentwo.recurrent = false;
                                gentwo.order_id = 0;
                                reply.addons_subscribed.general.push(gentwo);
                            }
                        });
                    });
                    reply.history = [];				// fix this
                    res.json(reply);
                });
            });
        }
    });
}

function callDetailsGet(req, res) {
    var reply = [];
    res.json(reply);
}


