var config = require(__base + '/config');
var paas = require(__lib + '/paas');
var common = require(__lib + '/common');
var db = require(__lib + '/db_handler');
var ec = require(__lib + '/elitecore');
var geoIP = require(__lib + '/geoip');
const elitecoreUserService = require(`${__lib}/manager/ec/elitecoreUserService`);
const ecommManager = require(`${__manager}/ecomm/ecommManager`);
var creditCapManager = require('../../../core/manager/account/creditCapManager');
var usageManager = require('../../../core/manager/account/usageManager');
var contentsRates = require(__core + '/manager/contents/rates');
var profileManager = require(__core + '/manager/profile/profileManager');
var notificationSend = require(__core + '/manager/notifications/send');
var detailsManager = require(__core + '/manager/account/detailsManager');
var portInManager = require(__core + '/manager/porting/portInManager');
var option = require('../settings/option');
var request = require('request');
var async = require('async');
var addons = require('./addons');
var terminationManager = require('../../../core/manager/account/terminationManager');
var constants = require('../../../core/constants');
var configManager = require('../../../core/manager/config/configManager');
var resourceManager = require('../../../../res/resourceManager');
const logsManager = require(`${__manager}/log/logsManager`);
var BaseError = require('../../../core/errors/baseError');
const Bluebird = require('bluebird');
const betaUsersManager = require('../../../core/manager/beta/betaUsersManager');

var logsEnabled = config.LOGSENABLED;

var AUTO_BOOST_ADDON_NAME = "Auto Boost 100";
var GENTWO_APP = [ "gentwo", "gentwo_credit" ];
var ROAMING_GROUP = {
    "id" : "roamingGroup",
    "title" : "Roaming",
    "subtitle" : "",
    "description_short" : "Activate data, calls and SMS when travelling overseas.",
    "description_full" : "",
    "list" : [ ] };
var ROAMING_OFF = {
    "id" : "roamingOff",
    "title" : "Roaming Off",
    "subtitle" : "OFF",
    "description_short" : "Turn Off All Roaming",
    "description_full" : "You are about to disable roaming.\n\nThis might take up to 15 minutes. We will send you a notification when done." };
var TRIALSIM = [
    "87429915",
    "87428895",
    "87429548",
    "88080469",
    "88080952",
    "88081583",
    "88081852",
    "88081947",
    "88080583",
    "88080972",
    "88081597",
    "88081830",
    "88081863",
    "88081835",
    "88081936"
    ];
var TESTERS = [
    "87425765",
    "87420787",
    "87428443",
    "87420464"
    ];
exports.TRIALSIM = TRIALSIM;
exports.TESTERS = TESTERS;

//exports

exports.GENTWO_APP = GENTWO_APP;
exports.ROAMING_GROUP = ROAMING_GROUP;
exports.ROAMING_OFF = ROAMING_OFF;
exports.profileBoostsGet = profileBoostsGet;
exports.profileDetailsGet = profileDetailsGet;
exports.planDetailsGet = planDetailsGet;
exports.profileDevicesGet = profileDevicesGet;
exports.creditCardUpdate = creditCardUpdate;
exports.roamingCapUpdateSet = roamingCapUpdateSet;
exports.billingAddressUpdateSet = billingAddressUpdateSet;
exports.emailUpdateSet = emailUpdateSet;
exports.dobVerifyGet = dobVerifyGet;
exports.identityNumberVerifyGet = identityNumberVerifyGet;
exports.guideScreensGet = guideScreensGet;
exports.putPortInRequest = putPortInRequest;
exports.getPortInStatus = getPortInStatus;
exports.cancelPortInRequest = cancelPortInRequest;
exports.upgradePlanToCirclesSwitch = upgradePlanToCirclesSwitch;
exports.logsAppStates = logsAppStates;

//functions

function upgradePlanToCirclesSwitch(req, res) {
    if (!req.fields.start_date) {
        return res.status(400).json({code: -1, details: "No port in date specified in the params",
            description: "No port in date sent in the params"});
    }
    elitecoreUserService.loadUserInfoByUserKey(req.params.user_key, function (err, cache) {
        if (err) {
            return res.status(400).json({code: -1, details: "Failed to load customer",
                description: err.message});
        }

        if (!cache) {
            return res.status(400).json({code: -1, details: "Failed to load customer",
                description: "Not found"});
        }
        var portInStartDate = req.fields.start_date;
        portInManager.activateRequest(cache.serviceInstanceNumber, portInStartDate, (err, result) => {
            if (err) {
                return res.status(400).json({code: -1, details: "Failed to activate port in request",
                    description: err.message});
            }
            res.status(200).json({"code": 0, "result": "OK"});
        });
    }, false);
}

function getRates(iso, callback) {
    contentsRates.get("ppurroaming", undefined, iso, function(err, rates) {
        if (!rates) return callback(new Error("No Rates"));
        var reply = { "countryname" : rates.countryname };
        reply.roaming = {
            "title": rates.header.ppurroaming.label,
            "description": rates.tooltip.ppurroaming.label,
            "list": []
        };
        rates.rates.forEach(function (rate) {
            obj = {
                "destination": rate.country,
                "rates": []
            };
            var regularmccmnc = new Array;
            var mccmnc = (rate.mccmnc) ? rate.mccmnc.split(",") : [];
            if (mccmnc.length > 0) mccmnc.forEach(function (mccmncitem) {
                regularmccmnc.push({
                    "mcc": parseInt(mccmncitem.split(";")[0]),
                    "mnc": parseInt(mccmncitem.split(";")[1])
                });
            });
            obj.rates.push({
                "title": rates.header.ppurroaming.ppurdatamms.split("(")[0],
                "prefix": parseInt(rate.prefix),
                "price": {
                    "prefix": "$",
                    "value": rate.ppurdatamms,
                    "postfix": "SGD",
                    "unit": "10 KB"
                },
                "mccmnc": regularmccmnc
            });
            if (rate.discountdata) {
                var discountmccmnc = new Array;
                var dmccmnc = rate.discountmccmnc.split(",");
                if (dmccmnc.length > 0) dmccmnc.forEach(function (mccmncitem) {
                    discountmccmnc.push({
                        "mcc": parseInt(mccmncitem.split(";")[0]),
                        "mnc": parseInt(mccmncitem.split(";")[1])
                    });
                });
                obj.rates.push({
                    "title": rates.header.ppurroaming.discountdata.split("(")[0] + " (" + rate.discountoperator + ")",
                    "prefix": parseInt(rate.prefix),
                    "price": {
                        "prefix": "$",
                        "value": rate.discountdata,
                        "postfix": "SGD",
                        "unit": "100 MB"
                    },
                    "mccmnc": discountmccmnc,
                    "preferred": rate.discountoperator
                });
            }
            obj.rates.push({
                "title": rates.header.ppurroaming.ppurvoicesg.split("(")[0],
                "prefix": parseInt(rate.prefix),
                "price": {
                    "prefix": "$",
                    "value": rate.ppurvoicesg,
                    "postfix": "SGD",
                    "unit": "minutes"
                },
                "mccmnc": regularmccmnc
            });
            obj.rates.push({
                "title": rates.header.ppurroaming.ppurvoiceintl.split("(")[0],
                "prefix": parseInt(rate.prefix),
                "price": {
                    "prefix": "$",
                    "value": rate.ppurvoiceintl,
                    "postfix": "SGD",
                    "unit": "minutes"
                },
                "mccmnc": regularmccmnc
            });
            obj.rates.push({
                "title": rates.header.ppurroaming.ppurvoicelocal.split("(")[0],
                "prefix": parseInt(rate.prefix),
                "price": {
                    "prefix": "$",
                    "value": rate.ppurvoicelocal,
                    "postfix": "SGD",
                    "unit": "minutes"
                },
                "mccmnc": regularmccmnc
            });
            obj.rates.push({
                "title": rates.header.ppurroaming.ppurvoiceincoming.split("(")[0],
                "prefix": parseInt(rate.prefix),
                "price": {
                    "prefix": "$",
                    "value": rate.ppurvoiceincoming,
                    "postfix": "SGD",
                    "unit": "minutes"
                },
                "mccmnc": regularmccmnc
            });
            obj.rates.push({
                "title": rates.header.ppurroaming.ppursmssend,
                "prefix": parseInt(rate.prefix),
                "price": {
                    "prefix": "$",
                    "value": rate.ppursmssend,
                    "postfix": "SGD",
                    "unit": "sms"
                },
                "mccmnc": regularmccmnc
            });
            reply.roaming.list.push(obj);
        });
        callback(err, reply);
    });
}

function profileDetailsGet(req, res) {

    ec.getCache(req.params.user_key, false, function (err, cache) {
        if (err) {
            return res.status(400).json({code: -1, details: "Failed to load customer", description: err.message});
        }

        if (!cache) {
            return res.status(400).json({code: -1, details: "Failed to load customer", description: "Not found"});
        }

        var tasks = new Array;
        tasks.push(function (callback) {
            var ip = req.ip;
            geoIP.get(ip, function (err, result) {	// unused, remove
                if (!err) {
                    result.visible = true;
                    if (result.country == 'SG') {
                        callback(undefined, {"geoip": result, "rates": {"countryname": "Singapore"}});
                    } else {
                        getRates(result.country, function (error, rates) {
                            callback(undefined, {"geoip": result, "rates": rates});
                        });
                    }
                } else {
                    callback(undefined, err);
                }
            });
        });
        tasks.push(function (callback) {
            var optionList = ["settings_show_port_in_later"];
            option.loadConfigCMS(cache.prefix, cache.number, "selfcare", optionList, function (err, settings) {
                callback(undefined, {error: err ? err.message : undefined, settings: settings});
            });
        });
        tasks.push(function (callback) {
            option.loadActiveRequestCMS(cache.prefix, cache.number, function (err, result) {
                callback(undefined, {error: err ? err.message : undefined, activePortIn: result});
            })
        });
        tasks.push(function (callback) {
            return configManager.loadConfigMapForKey("selfcare", "circles_switch", function (err, item) {
                callback(undefined, {error: err ? err.message : undefined,
                    circleSwitchUpdateEndTime: new Date(item.options.upgrade_end_date)});
            });
        });
        tasks.push(function (callback) {
            return configManager.loadConfigMapForKey("dbs", "dbs_promo_campaign", function (err, item) {
                if (item && !item.options.enabled && 
                    cache.billingCreditCard && cache.billingCreditCard.customerBankName) {
                    betaUsersManager
                        .isBetaCustomer(cache.number, cache.billingCreditCard.customerBankName.toLowerCase(), 
                            (error, isBeta) => {
                                callback(undefined, {error: error ? error.message : undefined,
                                    allowBetaCustomer: isBeta,
                                    allowForAll: false
                                    // DBS campaign testing only beta customers now later we will make campaign TRUE
                                    // Which means betaCustomer check will be removed and enabled for all
                                });
                    });
                }else {
                    callback(undefined, {error: err ? err.message : undefined,
                        allowBetaCustomer: false,
                        allowForAll: true
                    });
                }
            });
        });
        tasks.push(function (callback) {
            terminationManager.getScheduledTerminationBySIN(cache.serviceInstanceNumber)
                .then((event) => {
                    callback(undefined, {error: undefined, terminationEvent: event});
                })
                .catch((err) => {
                    callback(undefined, {error: err ? err.message : undefined, terminationEvent: {}});
                });
        });

        async.parallel(tasks, function (err, result) {
            var geoip;
            var rates;
            var portInSetting;
            var activePortIn;
            var terminationEvent;
            var terminationDate;
            var circles_switch_details = {
                upgrade_banner_available: false,
                port_in_banner_available: false,
                port_in_pending: false
            }
            var circleSwitchUpdateEndTime;
            let allowBetaCustomer;
            let allowForAll;

            if (result) result.forEach(function (item) {
                if (item && item.geoip) {
                    geoip = item.geoip;
                    rates = item.rates;
                    if (rates) {
                        geoip.countryname = rates.countryname;
                        delete rates.countryname;
                    }
                    if (geoip && geoip.org) {
                        geoip.carrier = geoip.org.replace(geoip.org.split(" ")[0] + " ", "");
                    }
                }
                if (item && item.settings) {
                    item.settings.forEach(function (item) {
                        if (item.option_id === 'settings_show_port_in_later') {
                            portInSetting = item;
                        }
                    });
                }

                if (item && item.circleSwitchUpdateEndTime) {
                    circleSwitchUpdateEndTime = new Date(item.circleSwitchUpdateEndTime).getTime();
                }

                if (item && item.activePortIn) {
                    activePortIn = item.activePortIn;
                }
                if (item && item.terminationEvent) {
                    terminationEvent = item.terminationEvent;
                }
                if (item && item.allowBetaCustomer) {
                    allowBetaCustomer = item.allowBetaCustomer;
                }
                if (item && item.allowForAll) {
                    allowForAll = item.allowForAll;
                }
            });

            var reply = new Object;
            reply.service_instance_number = cache.serviceInstanceNumber;
            reply.first_name = cache.billingFullName;
            reply.middle_name = "";							// ec does not have middle name field
            reply.last_name = "";							// ec does not have last name field
            reply.roaming = {
                "prefix": "$",
                "value": (cache.roaming && cache.roaming.limit ? cache.roaming.limit : 100),
                "postfix": "SGD"
            }
            reply.plan_name = cache.base.name;
            reply.plan_id = cache.base.id;
            if (terminationEvent && terminationEvent.start) {
                terminationDate = new Date(parseInt(terminationEvent.start));
                reply.termination_date = terminationDate.toISOString();
            }

            var currentTime = new Date().getTime();
            if (!terminationDate && reply.plan_id === constants.CIRCLES_SWITCH_PLAN_ID && activePortIn &&
                !activePortIn.scheduled && circleSwitchUpdateEndTime && currentTime < circleSwitchUpdateEndTime) {
                    circles_switch_details.upgrade_banner_available = true
                }
            if (!terminationDate && reply.plan_id === constants.CIRCLES_SWITCH_PLAN_ID && activePortIn &&
                !activePortIn.scheduled && circleSwitchUpdateEndTime && currentTime > circleSwitchUpdateEndTime) {
                    circles_switch_details.port_in_banner_available = true
                }
            if (reply.plan_id === constants.CIRCLES_SWITCH_PLAN_ID && activePortIn && activePortIn.scheduled) {
                circles_switch_details.port_in_pending = true
            }
            reply.circles_switch_details = circles_switch_details;
            reply.circles_switch_details.upgrade_banner_available_until_ms = (circleSwitchUpdateEndTime - currentTime);

            var street_building_name = "";
            var floor_no = "";
            var unit_no = "";
            var hse_blk_tower = "";
            if (cache.billingAddress && cache.billingAddress.addressTwo) {
                street_building_name = cache.billingAddress.addressTwo.split(';')[0];
                floor_no = cache.billingAddress.addressTwo.split(';')[1];
                unit_no = cache.billingAddress.addressTwo.split(';')[2];
                hse_blk_tower = cache.billingAddress.addressTwo.split(';')[3];
            }

            reply.billing_address = {
                "street_building_name": street_building_name,
                "floor_no": floor_no,
                "unit_no": unit_no,
                "hse_blk_tower": hse_blk_tower,
                "zip_code": cache.billingAddress ? cache.billingAddress.postcode : "",
                "city": cache.billingAddress ? cache.billingAddress.city : "",
                "state": cache.billingAddress ? cache.billingAddress.state : "",
                "country": cache.billingAddress ? cache.billingAddress.country : "",
                "can_edit": "true",
                "visible": "true"
            };
            reply.email = {
                "can_edit": "true",
                "visible": "true",
                "value": cache.email
            };
            reply.credit_card = {
                "can_edit": "true",
                "visible": "true",
                "paytype": cache.billingCreditCard.type,
                "last_four_digits": cache.billingCreditCard.last4Digits,
                "bank_name": cache.billingCreditCard &&
                            cache.billingCreditCard.customerBankName ? cache.billingCreditCard.customerBankName : null
            };
            reply.port_in = {
                "visible": (!portInSetting || portInSetting.value)
                && (!cache.ported || (activePortIn && activePortIn.status !== "NOT_FOUND"))
            };
            reply.realtime = !cache.backupData;
            reply.boost_rewards = {
                "button_icon_url": "https://s3-ap-southeast-1.amazonaws.com/kirk.circles.asia/assets/boost_added_dbs/logo_dbs.png"
            }

            if (reply.credit_card.bank_name && allowBetaCustomer || reply.credit_card.bank_name && allowForAll) {
               reply.boost_rewards.purchase_allowed = true;
            }else {
                reply.boost_rewards.purchase_allowed = false;
            }

            // fetch from ecomm, temporary hard code
            // reply.geoip = geoip;
            // reply.rates = rates;

            if (cache.status == "Inactive") {
                reply.lockdown = {
                    "enabled": 1,
                    "title": "Account Suspended",
                    "message": "Please reach out to our Happiness Experts" +
                    " via Live Chat for resolution"
                };
            } else if (TRIALSIM.indexOf(cache.number) > -1) {
                reply.lockdown = {
                    "enabled": 1,
                    "title": "Trial SIM",
                    "message": "Please reach out to our Happiness Experts" +
                    " via Live Chat for more details"
                };
            } else {
                reply.lockdown = ec.isLockdown(cache.billing_cycle);
            }
            res.json(reply);
        });
    });
}

function colourNameToHex(colour) {
    var colours = {
        "aliceblue": "#f0f8ff",
        "antiquewhite": "#faebd7",
        "aqua": "#00ffff",
        "aquamarine": "#7fffd4",
        "azure": "#f0ffff",
        "beige": "#f5f5dc",
        "bisque": "#ffe4c4",
        "black": "#000000",
        "blanchedalmond": "#ffebcd",
        "blue": "#0000ff",
        "blueviolet": "#8a2be2",
        "brown": "#a52a2a",
        "burlywood": "#deb887",
        "cadetblue": "#5f9ea0",
        "chartreuse": "#7fff00",
        "chocolate": "#d2691e",
        "coral": "#ff7f50",
        "cornflowerblue": "#6495ed",
        "cornsilk": "#fff8dc",
        "crimson": "#dc143c",
        "cyan": "#00ffff",
        "darkblue": "#00008b",
        "darkcyan": "#008b8b",
        "darkgoldenrod": "#b8860b",
        "darkgray": "#a9a9a9",
        "darkgreen": "#006400",
        "darkkhaki": "#bdb76b",
        "darkmagenta": "#8b008b",
        "darkolivegreen": "#556b2f",
        "darkorange": "#ff8c00",
        "darkorchid": "#9932cc",
        "darkred": "#8b0000",
        "darksalmon": "#e9967a",
        "darkseagreen": "#8fbc8f",
        "darkslateblue": "#483d8b",
        "darkslategray": "#2f4f4f",
        "darkturquoise": "#00ced1",
        "darkviolet": "#9400d3",
        "deeppink": "#ff1493",
        "deepskyblue": "#00bfff",
        "dimgray": "#696969",
        "dodgerblue": "#1e90ff",
        "firebrick": "#b22222",
        "floralwhite": "#fffaf0",
        "forestgreen": "#228b22",
        "fuchsia": "#ff00ff",
        "gainsboro": "#dcdcdc",
        "ghostwhite": "#f8f8ff",
        "gold": "#ffd700",
        "goldenrod": "#daa520",
        "gray": "#808080",
        "green": "#008000",
        "greenyellow": "#adff2f",
        "honeydew": "#f0fff0",
        "hotpink": "#ff69b4",
        "indianred ": "#cd5c5c",
        "indigo": "#4b0082",
        "ivory": "#fffff0",
        "khaki": "#f0e68c",
        "lavender": "#e6e6fa",
        "lavenderblush": "#fff0f5",
        "lawngreen": "#7cfc00",
        "lemonchiffon": "#fffacd",
        "lightblue": "#add8e6",
        "lightcoral": "#f08080",
        "lightcyan": "#e0ffff",
        "lightgoldenrodyellow": "#fafad2",
        "lightgrey": "#d3d3d3",
        "lightgreen": "#90ee90",
        "lightpink": "#ffb6c1",
        "lightsalmon": "#ffa07a",
        "lightseagreen": "#20b2aa",
        "lightskyblue": "#87cefa",
        "lightslategray": "#778899",
        "lightsteelblue": "#b0c4de",
        "lightyellow": "#ffffe0",
        "lime": "#00ff00",
        "limegreen": "#32cd32",
        "linen": "#faf0e6",
        "magenta": "#ff00ff",
        "maroon": "#800000",
        "mediumaquamarine": "#66cdaa",
        "mediumblue": "#0000cd",
        "mediumorchid": "#ba55d3",
        "mediumpurple": "#9370d8",
        "mediumseagreen": "#3cb371",
        "mediumslateblue": "#7b68ee",
        "mediumspringgreen": "#00fa9a",
        "mediumturquoise": "#48d1cc",
        "mediumvioletred": "#c71585",
        "midnightblue": "#191970",
        "mintcream": "#f5fffa",
        "mistyrose": "#ffe4e1",
        "rose": "#ffe4e1",
        "moccasin": "#ffe4b5",
        "navajowhite": "#ffdead",
        "navy": "#000080",
        "oldlace": "#fdf5e6",
        "olive": "#808000",
        "olivedrab": "#6b8e23",
        "orange": "#ffa500",
        "orangered": "#ff4500",
        "orchid": "#da70d6",
        "palegoldenrod": "#eee8aa",
        "palegreen": "#98fb98",
        "paleturquoise": "#afeeee",
        "palevioletred": "#d87093",
        "papayawhip": "#ffefd5",
        "peachpuff": "#ffdab9",
        "peru": "#cd853f",
        "pink": "#ffc0cb",
        "plum": "#dda0dd",
        "powderblue": "#b0e0e6",
        "purple": "#800080",
        "red": "#ff0000",
        "rosybrown": "#bc8f8f",
        "royalblue": "#4169e1",
        "saddlebrown": "#8b4513",
        "salmon": "#fa8072",
        "sandybrown": "#f4a460",
        "seagreen": "#2e8b57",
        "seashell": "#fff5ee",
        "sienna": "#a0522d",
        "silver": "#c0c0c0",
        "skyblue": "#87ceeb",
        "slateblue": "#6a5acd",
        "slategray": "#708090",
        "snow": "#fffafa",
        "springgreen": "#00ff7f",
        "steelblue": "#4682b4",
        "tan": "#d2b48c",
        "teal": "#008080",
        "thistle": "#d8bfd8",
        "tomato": "#ff6347",
        "turquoise": "#40e0d0",
        "violet": "#ee82ee",
        "wheat": "#f5deb3",
        "white": "#ffffff",
        "whitesmoke": "#f5f5f5",
        "yellow": "#ffff00",
        "yellowgreen": "#9acd32"
    };
    if (!colour) return false;
    else if (typeof colours[colour.toLowerCase()] != 'undefined') return colours[colour.toLowerCase()];
    return false;
}

function planDetailsGet(req, res) {
    var start = new Date();
    ec.getCache(req.params.user_key, false, function (err, value) {
        if (err) {
            res.status(400).json(err);
        } else {
            usageManager.computeUsage(value.prefix, value.number, function(err, usageResult) {
                planDetails(value, usageResult ? usageResult.data : undefined, function (err, reply) {
                    res.json(reply);
                });
            });
        }
    });
}

function generalBuild(general, item, cycle) {
    var obj = new Object;
    obj.id = item.id;
    obj.title = general.title;
    obj.subtitle = general.subtitle;
    obj.description_short = general.description_short;
    obj.description_full = general.description_full;
    obj.disclaimer = general.disclaimer;
    obj.sub_effect = ec.effective(general.sub_effect);
    obj.unsub_effect = ec.effective(general.unsub_effect);
    obj.payment = "postponed";		// to be removed once app supports (un)sub_effect
    obj.action_available = (general.action > 0) ? true : false;
    obj.free_package = general.free_package;
    obj.order_id = parseInt(general.order_id);
    obj.price = {
        "prefix": "$",
        "value": item.price,
        "postfix": "SGD",
        "discount": parseInt(item.discount_price)
    };
    obj.recurrent = item.recurrent;
    obj.date = item.billStartDate;
    obj.start_date = item.billStartDate;
    obj.expiry_date = item.expiryDate;
    obj.current = (new Date(item.billStartDate).getTime() <= new Date().getTime()) ? true : false;
    obj.future = (new Date(item.expiryDate).getTime() > cycle.endDateUTC.getTime()) ? true : false;
    if (["Plus 2020", "Free Plus 2020"].indexOf(item.name) >= 0) {
        obj.kb = item.kb;
    }

    return obj;
}

function planDetails(value, usage, callback) {
    var reply = new Object;
//    db.monkey_test("cache", "monkey_" + value.account, function (monkey) {
//        if (monkey) {
//            reply.monkey = monkey;
//        }
        var cycle = ec.nextBill(value.billing_cycle);
        reply.billing_cycle = {"start_date": cycle.startDate.toISOString(), "end_date": cycle.endDate.toISOString()};
        var bill = new Object;
        bill.price = {"prefix": "$", "value": value.recurring, "postfix": "SGD"};
        bill.due_by_date = cycle.end;
        reply.bill = bill;
        reply.devices = new Array;
        var base = new Object;
        base.price = {"prefix": "$", "value": (value.base) ? value.base.price : 0, "postfix": "SGD"};
        base.data = {"value": (value.base) ? value.base.kb : 0, "unit": "kilobyte"};
        var base_mins = (value.base && value.base.mins) ? value.base.mins * 60 : 0;
        base.calls = {"value": base_mins, "unit": "seconds"};
        var base_sms = (value.base && value.base.sms) ? value.base.sms : 0;
        base.sms = {"value": base_sms, "unit": "sms"};
        base.components_changed = ( JSON.stringify(value.extra) == JSON.stringify(value.extra_current) ) ? false : true;
        reply.basic_plan = base;
        var addons = new Object;
        if (value.extra) {
            if (value.extra.data && value.extra.data.id) {
                extra_data = new Object;
                extra_data.id = value.extra.data.id;			// or value.extra.packageHistoryId if we need to unsubscribe triggered from app
                extra_data.type = "data";
                extra_data.value = value.extra.data.kb;
                extra_data.unit = "kilobyte";
                extra_data.price = {"prefix": "$", "value": value.extra.data.price, "postfix": "SGD"};
                extra_data.payment = "postponed";
                extra_data.date = value.extra.data.billStartDate;
                if (!addons.extra) addons.extra = new Object;
                addons.extra.data = extra_data;
            }
            if (value.extra && value.extra.voice && value.extra.voice.id) {
                extra_voice = new Object;
                extra_voice.id = value.extra.voice.id;			// or value.extra.packageHistoryId if we need to unsubscribe triggered from app
                extra_voice.type = "calls";
                extra_voice.value = value.extra.voice.mins * 60;
                extra_voice.unit = "seconds";
                extra_voice.price = {"prefix": "$", "value": value.extra.voice.price, "postfix": "SGD"};
                extra_voice.payment = "postponed";
                extra_voice.date = value.extra.voice.billStartDate;
                if (!addons.extra) addons.extra = new Object;
                addons.extra.calls = extra_voice;
            }
            if (value.extra && value.extra.sms && value.extra.sms.id) {
                extra_sms = new Object;
                extra_sms.id = value.extra.sms.id;			// or value.extra.packageHistoryId if we need to unsubscribe triggered from app
                extra_sms.type = "sms";
                extra_sms.value = value.extra.sms.sms;
                extra_sms.unit = "sms";
                extra_sms.price = {"prefix": "$", "value": value.extra.sms.price, "postfix": "SGD"};
                extra_sms.payment = "postponed";
                extra_sms.date = value.extra.sms.billStartDate;
                if (!addons.extra) addons.extra = new Object;
                addons.extra.sms = extra_sms;
            }
        } else {
            addons.extra = {};
        }
        addons.bonus = {"data": [], "total" : (usage ? usage.bonus : 0)};
        if (value.bonus) value.bonus.forEach(function (item) {
            if (item.kb) {
                var data = new Object;
                data.id = item.id;
                data.type = "data";
                data.value = item.kb;
                data.unit = "kilobyte";
                data.price = {
                    "prefix": "$",
                    "value": item.price,
                    "postfix": "SGD"
                };
                data.date = item.billStartDate;
                addons.bonus.data.push(data);
            }
        });
        var freePackages = new Array;
        ec.packages().general.forEach(function(o) {
            if (o.free_package) freePackages.push(o.free_package);
        });
        var generalAllowed = ec.packages().general.map(function (o) {
            if ( ((GENTWO_APP.indexOf(o.app) == -1) && (o.enabled == 1)) ||
                (freePackages.indexOf(o.id) > -1) ) return o.id;
            else return undefined;
        });
        addons.general = new Array;
        if (value.general) value.general.forEach(function (item) {
            var idx = generalAllowed.indexOf(item.id);
            if (idx > -1) {
                if ( item.id == "PRD00315" ) {	// remove this
//                    if ( TESTERS.indexOf(value.number) == -1 )
//                        addons.general.push(generalBuild(ec.packages().general[idx], item));
                } else {
                    addons.general.push(generalBuild(ec.packages().general[idx], item, cycle));
                }
            }
        });
        if (value.general_future) value.general_future.forEach(function (item) {	// combine current and future
            var idx = generalAllowed.indexOf(item.id);
            if (idx > -1) {
                var general = generalBuild(ec.packages().general[idx], item, cycle);
                var currentIdx = addons.general.map(function (o) {
                    return o.id;
                }).indexOf(item.id);
                if (currentIdx > -1) {	// merge identical addons
                    if ((new Date(general.start_date).getTime() -
                        new Date(addons.general[currentIdx].expiry_date).getTime()) < 5000) {	// 5 sec
                        general.start_date = addons.general[currentIdx].start_date;
                        general.date = addons.general[currentIdx].start_date;
                        general.current = true;
                        addons.general[currentIdx] = general;
                    } else {
                        addons.general.push(computeGeneralAddonKB(general));
                    }
                } else {
                    addons.general.push(computeGeneralAddonKB(general));
                }
            }
        });

        addons.topup = new Array;
        if (value.boost) value.boost.forEach(function (item) {
            var topup = new Object;
            topup.id = item.id;		// or item.packageHistoryId for unsubscribe
            topup.type = "data";
            topup.value = item.kb;
            topup.unit = "kilobyte";
            topup.price = {"prefix": "$", "value": item.price, "postfix": "SGD"};
            topup.payment = "postponed";
            topup.date = item.billStartDate;
            topup.order_id = 0;
            addons.topup.push(topup);
        });
        reply.addons_subscribed = addons;

        var product = ec.findPackageName(ec.packages(), AUTO_BOOST_ADDON_NAME);
        reply.autoboost = {enabled: true};
        if (product) {
            reply.autoboost.price = {"prefix": "$", "value": product.price, "postfix": "SGD"}
            reply.autoboost.data = {value: product.kb, unit: "kilobyte"};
        }

        profileManager.loadSetting(value.prefix, value.number, "selfcare", undefined, function (err, result) {
            var roaming_auto = 0;    // watch out! what is the default value?
            var roaming_data = 0;    // watch out! what is the default value?
            if (result && result.settings) {
                result.settings.forEach(function (item) {
                    if ( item && (item.option_id == "roaming_auto") ) roaming_auto = item.value;
                    if ( item && (item.option_id == "roaming_data") ) roaming_data = item.value;
                    if (item && (item.option_id === 'autoboost_limit' && item.value == 0)) {
                        reply.autoboost.enabled = false;
                    }
                });
            }
            var roamingGroup = JSON.parse(JSON.stringify(ROAMING_GROUP));
            var data = false;
            var auto = false;
            var pending = false;
            var roaming_data_m1RefId = ((roaming_data != "1") && (roaming_data != "0")) ? roaming_data : undefined;
            var roaming_auto_m1RefId = ((roaming_auto != "1") && (roaming_auto != "0")) ? roaming_auto : undefined;
            db.addonlog.findOne({ "m1RefId" : roaming_data_m1RefId }, function(dataErr, dataDoc) {
                db.addonlog.findOne({ "m1RefId" : roaming_auto_m1RefId }, function(autoErr, autoDoc) {
                    if ( (roaming_data == "1") || (dataDoc && (dataDoc.method == "AddDataRoaming")) ) data = true;
                    if ( (roaming_auto == "1") || (autoDoc && (autoDoc.method == "AddAutoRoaming")) ) auto = true;
                    ec.ROAMING_ADDONS.forEach(function (item) {
                        var product = JSON.parse(JSON.stringify(ec.findPackage(ec.packages(), item)));
                        if ( product.app == "vas_auto" ) {
                            if ( (roaming_auto != "0") && (roaming_auto != "1") ) product.pending = true;
                        } else if ( product.app == "vas_data" ) {
                            if ( (roaming_data != "0") && (roaming_data != "1") ) product.pending = true;
                        }
                        pending = (product.pending) ? true : false;
                        if ( (product.app == "vas_data") && data ) roamingGroup.list.push(product);
                        else if ( (product.app == "vas_auto") && !data && auto ) roamingGroup.list.push(product);
                    });
                    if ( roamingGroup.list.length == 0 ) {
                        var roamingOff = JSON.parse(JSON.stringify(ROAMING_OFF));
                        if (pending) roamingOff.pending = true;
                        roamingGroup.list.push(roamingOff);
                    }
//                    if ( TESTERS.indexOf(value.number) > -1 )   // remove this
                    reply.addons_subscribed.general.push(roamingGroup);
                    reply.basic_plan.unlimited_data = value.serviceInstanceUnlimitedData ? value.serviceInstanceUnlimitedData.enabled : false;
                    callback(false, reply);
                });
            });
        });
//    });
}

function computeGeneralAddonKB (addon) {
    var nextCycle = ec.nextBill(undefined, undefined, ec.nextBill().endDateUTC);
    var nextCycleEndDate = nextCycle.endDateUTC.getTime();
    // this is a workaround to make sure that addons which are subscribed
    // for 2 months down the lane are not shown in the UI. Need to fix this
    // in the app.
    var addonSubscriptionStartMonth = new Date(addon.start_date).getTime();
    if (addonSubscriptionStartMonth >= nextCycleEndDate && addon.future) {
        addon.kb = 0;
        if (addon.price && addon.price.value > 0) {
            addon.price.value = 0;
        }
    }
    return addon;
}

function profileBoostsGet(req, res) {
    ec.getCache(req.params.user_key, false, function (err, cache) {
        if (err) {
            res.status(400).json(err);
        } else {
            var reply = new Array;
            if (cache.boost) cache.boost.forEach(function (boost) {
                var uBoost = new Object;
                uBoost.payment = "immediate";	// boosts are always immediate?
                uBoost.type = "data";		// boosts are always data?
                uBoost.value = boost.kb;
                uBoost.unit = "kilobyte";
                uBoost.auto = (boost.app == "auto") ? true : false;
                uBoost.recurrent = boost.recurrent;
                uBoost.id = boost.id;
                uBoost.date = boost.billStartDate;
                if (boost.app === "dbs") {
                    //TODO: Remove the short_description field after we release the new app
                    uBoost.short_description = boost.description_short;
                    uBoost.description_short = boost.description_short;
                }
                uBoost.price = {"prefix": "$", "value": boost.price, "postfix": "SGD"};
                reply.push(uBoost);
            });
            res.json(reply);
        }
    });
}

function creditCardUpdate(req, res) {
    ec.getCache(req.params.user_key, false, (err, cache) => {
        if (err) return res.status(400).json(err);
        ecommManager.generateCreditCardUpdateLink(cache.account, (err, result) => {
            if (err || !result) return res.status(400).json(err);
            const returnUrl = 'https://' + config.CC_UPDATE_RETURN + ':6443/link/update/creditcard/status/json/' + result.refNum + '/' + result.linkId;
            res.redirect(`${result.url}&returnurl=${returnUrl}`);
        });
    });
}

function billingAddressUpdateSet(req, res) {
    var uuid = (req.fields.uuid) ? req.fields.uuid : "billingAddressUpdateSet";
    var cacheUUID = function(cb) {
        addons.uuidCheck(res, req.params.user_key, uuid, function() {
            ec.getCache(req.params.user_key, false, cb);
        });
    }
    var sendReply = function(code, reply) {
        db.cache_hput("uuid_cache", req.params.user_key + "_" + uuid, {
            "status": code,
            "reply": JSON.stringify(reply)
        }, 120000);
        res.status(code).json(reply); 
    }
    cacheUUID(function(err, cache) {
        if (err) {
            sendReply(400, { "code" : -1, "err" : err.message });
        } else {
            var hse_blk_tower = req.fields.hse_blk_tower;
            var street_building_name = req.fields.street_building_name;
            var floor_no = req.fields.floor_no;
            var unit_no = req.fields.unit_no;
            var zip_code = req.fields.zip_code;
            var path = "/api/mobile/v1/billing_address";

            var params = {
                "activity": "update",
                "account_number": cache.account,
                "floor_no": floor_no,
                "zip_code": zip_code,
                "unit_no": unit_no,
                "street_building_name": street_building_name,
                "hse_blk_tower": hse_blk_tower
            };
            paas.connect("post", path, params, function (err, result) {
                if (!err && result && (result.code == 0)) {
                    ec.setCache(cache.number, cache.prefix, req.params.user_key, cache.account, function (err, obj) {

                        sendReply(200, { "code" : 0 });

                        var notification = {
                            activity: "address_details_changed",
                            name: cache.billingFullName,
                            prefix: "65",
                            number: cache.number,
                            teamID: 1,
                            teamName: "Mobile"
                        };

                        notificationSend.deliver(notification, null, function (error, result) {
                            if (error) {
                                return common.error("Notification is not sent, error=" + error);
                            }
                            if (logsEnabled) common.log("Notification is sen , body=" + JSON.stringify(result));
                        });

                    });
                } else {
                    var details = (result) ? result.message : err;
                    sendReply(500, { "code" : -1, "details": details });
                }
            });
        }
    });
}

function emailUpdateSet(req, res) {
    ec.getCache(req.params.user_key, false, function (err, cache) {
        if (err) {
            res.status(400).json(err);
        } else {
            var email = common.escapeHtml(req.fields.email);
            var newemail = email;
            var oldemail = cache.email;
            var number = cache.number;

            if (newemail === oldemail) {
                if (logsEnabled) common.log("New emails is equal to old, no need to update and send a notification");
                return res.json({"code": 0});
            }

            detailsManager.updateAccountDetails(cache.account, {email: newemail}, function(err,result){
                    var notification = {
                        activity: "email_changed_old",
                        name: cache.billingFullName,
                        prefix: "65",
                        number: number,
                        email: oldemail,
                        teamID: 1,
                        teamName: "Mobile",
                        newemail: newemail,
                        oldemail: oldemail,
                        webportalemail: ""
                    };

                    notificationSend.deliver(notification, null, function (error, result) {
                        if (error) {
                            return common.error("Notification is sent to an old email, error=" + error);
                        }
                        if (logsEnabled) common.log("Notification is sent to an old email , body=" + JSON.stringify(result));
                    });


                    // send notification to old email
                    var notification = {
                        activity: "email_changed_new",
                        name: cache.billingFullName,
                        prefix: "65",
                        number: number,
                        email: newemail,
                        teamID: 1,
                        teamName: "Mobile",
                        newemail: newemail,
                        oldemail: oldemail,
                        webportalemail: ""
                    };

                    notificationSend.deliver(notification, null, function (error, result) {
                        if (error) {
                            return common.error("Notification is sent to a new email, error=" + error);
                        }
                        if (logsEnabled) common.log("Notification  is sent to a new email, body=" + JSON.stringify(result));
                    });

                    res.json({"code": 0});
            });
        }
    });
}

function roamingCapUpdateSet(req, res) {
    ec.getCache(req.params.user_key, false, function (err, cache) {
        if (err) {
            res.status(400).json(err);
        } else {

            if (cache.serviceInstanceBasePlanName == 'CirclesSwitch') {
                return configManager.loadConfigMapForKey("selfcare", "circles_switch", function (err, item) {
                    var upgradeEndDate = !err && item.options.upgrade_end_date  ? new Date(item.options.upgrade_end_date) : undefined;
                    var canUpgrade = !upgradeEndDate || upgradeEndDate.getTime() > new Date().getTime();
                    portInManager.loadActiveRequestByServiceInstanceNumber(cache.serviceInstanceNumber, {}, (err, result) => {
                        var modificationErr = new BaseError('Modifications are not allowed', canUpgrade && (!result || !result.scheduled)
                            ? 'ERROR_CIRCLES_SWITCH_UPGRADE_REQUIRED' : 'ERROR_CIRCLES_SWITCH_UPGRADE_NOT_ALLOWED');
                        var result = resourceManager.getErrorValues(modificationErr);
                        res.status(400).json(result);
                    });
                });
            }

            var limit = parseInt(req.fields.limit);
            var prefix = cache.prefix;
            var number = cache.number;

            if (limit !== 100 && limit !== 300) {
                var errorMessage = "Only 100$ and 300$ options can be selected in the app";
                return res.status(400).json({code: -1, details: "Incorrect limit param", description: errorMessage});
            }

            creditCapManager.updateRoamingCap(prefix, number, limit, function(err) {
                if (err) {
                    var errorMessage = "Failed to update limit, please try again";
                    return res.status(400).json({code: -1, details: err.message, description: errorMessage});
                }

                res.json({"code": 0});
            });
        }
    });
}

function dobVerifyGet(req, res) {
    ec.getCache(req.params.user_key, false, function (err, cache) {
        if (err) {
            res.status(400).json(err);
        } else {
            var year = parseInt(req.fields.year_of_birth);
            var month = parseInt(req.fields.month_of_birth);
            var day = parseInt(req.fields.day_of_birth);
            if (cache && cache.birthday && (year == parseInt(cache.birthday.split("-")[0])) &&
                (month == parseInt(cache.birthday.split("-")[1])) &&
                (day == parseInt(cache.birthday.split("-")[2]))) {
                res.json({"code": 0, "dob_valid": true, "valid": true});
            } else {
                res.json({"code": 0, "dob_valid": false, "valid": false});
            }
        }
    });
}

function identityNumberVerifyGet(req, res) {
    var id_digits = req.fields.id_digits;
    ec.getCache(req.params.user_key, false, function (err, cache) {
        if (err) {
            res.status(400).json(err);
        } else {
            option.loadActiveRequestCMS(cache.prefix, cache.number, function(err, result) {
                var identity = result && result.customer_id_number ? result.customer_id_number : cache.customerIdentity.number;

                var validLength = identity && identity.length == 9;
                var digitsString = validLength ? identity.substring(1, 8) : null;
                var validForVerification = validLength && /^\d+$/.test(digitsString);
                var digits = validForVerification ? digitsString : null;

                if (digits === id_digits) {
                    res.json({"code": 0, "valid": true});
                } else {
                    res.json({"code": 0, "valid": false});
                }
            });
        }
    });
}

function profileDevicesGet(req, res) {
    ec.getCache(req.params.user_key, false, function (err, cache) {
        if (err) {
            res.status(400).json(err);
        } else {
            // E-Comm already supplies code value and error format
            ecommManager.customerDeviceInfo(cache.account, function(err, result){
                if(err || !result){
                    res.status(400).json(err);
                } else {
                    result.devices.forEach(function(device){
                        // TODO Safer way of accessing nested properties
                        // TODO Get currency mapping from E-Comm - use SGD for now
                        device.instalment_plan.upfront_payment.prefix = '$';
                        device.instalment_plan.upfront_payment.postfix = 'SGD';
                        device.instalment_plan.monthly_payment.prefix = '$';
                        device.instalment_plan.monthly_payment.postfix = 'SGD';
                    })
                    res.json(result);
                }
            });
        }
    });
}

function putPortInRequest(req, res) {
    ec.getCache(req.params.user_key, false, function (err, cache) {
        if (err) {
            res.status(400).json(err);
        } else {
            var prefix = cache.prefix;
            var number = cache.number;
            var donorNetworkCode = req.fields.donor_network_code;
            var portingNumber = req.fields.porting_number;
            var startDate = req.fields.start_date;
            var options = new Object;
            options.fileIdFront = req.fields.file_id_front;
            options.fileIdBack = req.fields.file_id_back;
            options.fileAuthForm = req.fields.file_auth_form;
            options.needDocUpdate = (options.fileIdFront || options.fileIdBack || options.fileAuthForm) ? true : false;
            options.registeredOwnerName = req.fields.registered_owner_name;
            options.registeredOwnerIdType = req.fields.registered_owner_id_type;
            options.registeredOwnerIdNumber = req.fields.registered_owner_id_number;
            options.orderReferenceNumber = cache.orderReferenceNumber;
            options.initiator = "[MOBILE][" + number + "]";
            if (cache.base.id === constants.CIRCLES_SWITCH_PLAN_ID) {
                options.planName = "CIRCLES_SWITCH";
            } else {
                options.planName = cache.serviceInstanceBasePlanName;
            }

            if (cache.number == portingNumber) {
                return res.status(400).json({
                    "code": common.PORT_IN_CURRENT_NUMBER,
                    "details": "You have entered your current phone number. Please enter another phone number which you'd like us to port-in.",
                    "description": "You have entered your current phone number. Please enter another phone number which you'd like us to port-in."
                });
            }

            var date = new Date(startDate);
            date.setHours(6, 30, 0, 0);

            // just in case date is not correct,
            // schedule port in for the next hour

            if (date.getTime() - 60 * 60 * 1000 < new Date().getTime()) {
                date = new Date(new Date().getTime() + 60 * 60 * 1000);
            }

            portInManager.putRequest(prefix, number, prefix, portingNumber, donorNetworkCode, date,
                    "SELFCARE", options, function (error, result) {
                if (error) {
                    return res.status(400).json({
                        "code": -1,
                        "status" : error.id,
                        "error" : error.message,
                        "details": error && error.message ? error.message : "Failed to create a port-in request.",
                        "description": "Failed to create a port-in request."
                    });
                } else {
                    return res.json({
                        "code" : 0,
                        "result" : result
                    });
                }
            });
        }
    });
}

function cancelPortInRequest(req, res) {
    var uuid = (req.fields.uuid) ? req.fields.uuid : "cancelPortInRequest";
    addons.uuidCheck(res, req.params.user_key, uuid, function() {

        var saveResult = function (status, result) {
            if (logsEnabled) common.log("historyBillsResend", "done, uuid=" + req.fields.uuid);
            res.status(status).json(result);

            db.cache_hput("uuid_cache", req.params.user_key + "_" + uuid, {
                "status": status,
                "reply": JSON.stringify(result)
            }, 120000);
        }

        ec.getCache(req.params.user_key, false, function (err, cache) {
            if (err) {
                saveResult(400, {
                    "code": -1,
                    "details": err && err.message ? err.message : "Failed to cancel the port-in request.",
                    "description": "Failed to cancel the port-in request."
                });
            } else {
                var portingNumber = req.fields.porting_number;
                var key = config.NOTIFICATION_KEY;
                var url = "http://" + config.CMSHOST + ":" + config.CMSPORT
                    + "/api/1/portin/request/cancel/" + key;

                request({
                    uri: url,
                    method: 'PUT',
                    json: {
                        prefix: cache.prefix,
                        number: cache.number,
                        porting_number: portingNumber
                    }
                }, function (err, response, body) {
                    if (err) {
                        return saveResult(400, {
                            "code": -1,
                            "details": err && err.message ? err.message : "Failed to cancel the port-in request.",
                            "description": "Failed to cancel the port-in request."
                        });
                    } else {
                        var reply = common.safeParse(body);
                        if (reply.error) {
                            if (reply.error.indexOf("Transition IN_PROGRESS -> CANCELED") >= 0) {
                                return saveResult(400, {
                                    "code": common.PORT_IN_ALREADY_ACTIVE,
                                    "details": reply.error,
                                    "description": "We have already started port-in process. Please contact us if you " +
                                    "still want to cancel your port-in request."
                                });
                            } else {
                                return saveResult(400, {
                                    "code": -1,
                                    "details": reply.error,
                                    "description": "Failed to cancel the port-in request."
                                });
                            }
                        }
                        return saveResult(200, reply);
                    }
                });
            }
        });
    });
}

function getPortInStatus(req, res) {
    ec.getCache(req.params.user_key, false, function (err, cache) {
        if (err) {
            res.status(400).json(err);
        } else {

            option.loadActiveRequestCMS(cache.prefix, cache.number, function(err, result) {
                if (err || !result) {
                    return res.status(400).json({
                        code: -1,
                        details: err && err.message ? err.message : "Failed to load an active PortIn request",
                        description: "Failed to load an active PortIn request"
                    });
                }

                if (result.status == "DONOR_APPROVED" || result.status == "WAITING_FAILURE_REASON") {
                    result.status = "IN_PROGRESS";
                } else if (result.status == "WAITING_RETRY" || result.status == "WAITING_APPROVE") {
                    result.status = "WAITING";
                }

                res.json({code:0, result:result});
            });
        }
    });
}

function guideScreensGet(req, res) {
    var ignoredIds = [];
    if (req.query.ignore_ids) {
        var idsArray = req.query.ignore_ids.split(',');
        idsArray.forEach(function (id) {
            if (id) {
                ignoredIds.push(id.trim())
            }
        });
    }

    var roamingGuide = {
        "id": "1",
        "order_id": 0,
        "content": {
            "title": "Roaming Activation",
            "subtitle": "Four steps to activate roaming",
            "text": "1: Access the Menu\n2: Go to Customize\n3: On the PLUS menu, tap to\nenable/disable your roaming\n4: Success!",
            "transition_time_ms": 300,
            "display_time_ms": 2500,
            "images": [
                {"url": "https://s3-ap-southeast-1.amazonaws.com/kirk.circles.asia/images/roaming1.png"},
                {"url": "https://s3-ap-southeast-1.amazonaws.com/kirk.circles.asia/images/roaming2.png"},
                {"url": "https://s3-ap-southeast-1.amazonaws.com/kirk.circles.asia/images/roaming3.png"},
                {"url": "https://s3-ap-southeast-1.amazonaws.com/kirk.circles.asia/images/roaming4.png"}
            ]
        }
    }

    var result = [];
    if (ignoredIds.indexOf(roamingGuide.id) < 0) {
        result.push(roamingGuide);
    }

    res.json(result);
}

function logsAppStates(req, res) {
    logsManager.logsAppStates(req.fields.logs, (err, result) => {
        res.json({ code:0, insertedCount: result.insertedCount });
    });
}
