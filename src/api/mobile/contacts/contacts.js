var db = require(__lib + '/db_handler');
var common = require(__lib + '/common');

var version = require('./version');
var sync = require('./sync');
var details = require('./details');
var status = require('./status');

var checkSessionEnabled = true;
var postFieldsLimit = 20;

//exports

exports.init = init;

//functions

function init(baseApi, category, web, webs) {
    var path = baseApi + category;

    //contacts - status API    --- special case JSON
    var usagePath = path + '/status';
    web.post(usagePath + '/get/:user_key/:signature', parsePost);
    web.post(usagePath + '/get/:user_key/:signature', sessionCheckJson);
    web.post(usagePath + '/get/:user_key/:signature', status.statusGet);

    //contacts - sync API
    var usagePath = path + '/sync';
    web.post(usagePath + '/set/:user_key/:signature', parsePost);
    web.post(usagePath + '/set/:user_key/:signature', sessionCheckJson);
    web.post(usagePath + '/set/:user_key/:signature', sync.syncSet);

    //Parse params
    web.post(path + '/*', parsePost);

    //Check session
    web.post(path + '/*/:user_key/:signature', sessionCheckJson);
    web.get(path + '/*/:user_key/:signature', sessionCheckGet);

    //contacts - version API
    var usagePath = path + '/version';
    web.get(usagePath + '/get/:user_key/:signature', version.versionGet);

    //contacts - details API
    var usagePath = path + '/details';
    web.post(usagePath + '/get/:user_key/:signature', details.detailsGet);
}

function parsePost(req, res, next) {
    common.formParse(req, res, postFieldsLimit, function (fields) {
        req.fields = fields;
        next();
    });
}

function sessionCheckGet(req, res, next) {
    if (checkSessionEnabled) {
        db.sessionCheckGet(req, res, function (req, res) {
            next();
        });
    } else {
        next();
    }
}

function sessionCheckPost(req, res, next) {
    if (checkSessionEnabled) {
        db.sessionCheckPost(req, res, req.fields, function (req, res) {
            next();
        });
    } else {
        next();
    }
}

function sessionCheckJson(req, res, next) {
    if (checkSessionEnabled) {
        db.sessionCheckJson(req, res, req.fields, function (req, res) {
            next();
        });
    } else {
        next();
    }
}
