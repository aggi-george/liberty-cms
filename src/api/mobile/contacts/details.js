var db = require(__lib + '/db_handler');
var config = require(__base + '/config');
var common = require(__lib + '/common');

var LOG = config.LOGSENABLED;

//exports

exports.detailsGet = detailsGet;

//functions

function detailsGet(req, res) {
    var fields = req.fields ? req.fields : { "contacts" : [ ] };
    var str = "";
    for (var i = 0; i < fields.contacts.length; i++) {
        str += "'" + fields.contacts[i] + "',"
    }
    str = str.slice(0, -1);
    if (str == "") {
        var obj = new Object;
        obj.circle_users_updates = new Array;
        res.json(obj);
        return false;
    }
    var q = "SELECT m.firstname, m.lastname, m.app_picture, a.app_status, d.longitude, d.latitude, d.accuracy, CONCAT('+',a.number) num, UNIX_TIMESTAMP(a.app_lastseen) lastseen, IF(a.app_lastseen < DATE_SUB(NOW(),INTERVAL 6 minute),'false','true') online FROM msisdn m, app a, device d WHERE d.app_id=a.id AND a.number=m.number AND m.status='A' AND a.status='A' AND d.status='A' AND CONCAT('+',a.number) IN (" + str + ")";
    if (LOG) common.log("detailsGet", q);
    db.query(res, q, function (rows) {
        var obj = new Object;
        obj.circle_users_updates = new Array;
        if (rows.length == 0) {
            res.json(obj);
            return false;
        }
        for (var i = 0; i < rows.length; i++) {
            obj.circle_users_updates[i] = new Object;
            obj.circle_users_updates[i].num = rows[i].num;
            obj.circle_users_updates[i].fn = rows[i].firstname;
            obj.circle_users_updates[i].ln = rows[i].lastname;
            obj.circle_users_updates[i].ph = rows[i].app_picture;
            obj.circle_users_updates[i].st = rows[i].app_status;
            obj.circle_users_updates[i].lng = rows[i].longitude;
            obj.circle_users_updates[i].lat = rows[i].latitude;
            obj.circle_users_updates[i].acc = rows[i].accuracy;
            obj.circle_users_updates[i].ls = rows[i].lastseen;
            obj.circle_users_updates[i].on = rows[i].online;
        }
        res.json(obj);
    });
}
