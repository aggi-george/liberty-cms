var config = require(__base + '/config');
var db = require(__lib + '/db_handler');
var common = require(__lib + '/common');

var logsEnabled = config.LOGSENABLED;

//exports

exports.statusGet = statusGet;

//functions

function statusGet(req, res) {
	if (logsEnabled) {
		req.start = new Date();
	}
	var fields = req.fields;
	var str = "";
	for (var i = 0; i < fields.contacts.length; i++) {
		str += "'" + fields.contacts[i].replace('+','') + "',"
	}
	str = str.slice(0, -1);
	if (str == "") {
		var obj = new Object;
		obj.circle_users_stats = new Array;
		res.json(obj);
		return false;
	}

	var q = "SELECT CONCAT('+', a.number) num, a.app_status, d.longitude, d.latitude, d.accuracy, UNIX_TIMESTAMP(a.app_lastseen) lastseen, UNIX_TIMESTAMP(a.created) created, IF(a.app_lastseen < DATE_SUB(NOW(),INTERVAL 6 minute),'false','true') online FROM app a, device d WHERE a.app_type='gentwo' AND d.app_id=a.id AND a.number IN (" + str + ")";
	db.query(res, q, function (rows) {
		if (logsEnabled) {
			var step = new Date() - req.start;
			if ( step > 50 ) common.log("statusGet q", req.params.user_key + " " + step + " " + q);
		}

		var obj = new Object;
		obj.circle_users_stats = new Array;
		for (var i = 0; i < rows.length; i++) {

			var lastSeenDate = rows[i].lastseen > 0 ? new Date(rows[i].lastseen * 1000) : undefined;
			var createdDate = rows[i].created > 0 ? new Date(rows[i].created * 1000) : undefined;
			if (!lastSeenDate) {
				lastSeenDate = createdDate;
			}

			if (!lastSeenDate || new Date().getTime() - lastSeenDate.getTime() > 6 * 31 * 24 * 60 * 60 * 1000) {
				continue;
			}

			var status = new Date().getTime() - lastSeenDate.getTime() > 5 * 60 * 1000 ? 0 : rows[i].app_status;
			var online = new Date().getTime() - lastSeenDate.getTime() > 5 * 60 * 1000 ? false : rows[i].online;

			var userInfo = {};
			userInfo.num = rows[i].num;
			userInfo.lng = rows[i].longitude;
			userInfo.lat = rows[i].latitude;
			userInfo.acc = rows[i].accuracy;
			userInfo.ls = rows[i].lastseen;
			userInfo.on = online;
			userInfo.st = status;
			obj.circle_users_stats.push(userInfo);
		}
		res.json(obj);
	});
}
