var config = require(__base + '/config');
var db = require(__lib + '/db_handler');
var common = require(__lib + '/common');

var LOG = config.LOGSENABLED;

//exports

exports.syncSet = syncSet;

//functions

function syncSet2(number, str, callback) {
    var q = " SELECT m1.firstname, m1.lastname, m1.app_picture, a1.app_status, d1.longitude, " +
        " d1.latitude, d1.accuracy, CONCAT('+',a1.number) num, UNIX_TIMESTAMP(a1.app_lastseen) lastseen, " +
        " UNIX_TIMESTAMP(a1.created) created, IF(a1.app_lastseen < DATE_SUB(NOW(),INTERVAL 6 minute),'false','true') online " +
        " FROM msisdn m1, app a1, device d1 WHERE d1.app_id=a1.id AND a1.number=m1.number AND m1.status='A' " +
        " AND a1.status='A' AND d1.status='A' AND a1.app_type='gentwo' AND a1.number IN (" + str + ")";
    if (LOG) common.log("syncSet2", q);
    db.query_noerr(q, function (rows) {
        var obj = { "circle_users" : [] };
        if (rows && rows[0]) {
            for (var i = 0; i < rows.length; i++) {

                var lastSeenDate = rows[i].lastseen > 0 ? new Date(rows[i].lastseen * 1000) : undefined;
                var createdDate = rows[i].created > 0 ? new Date(rows[i].created * 1000) : undefined;
                if (!lastSeenDate) {
                    lastSeenDate = createdDate;
                }

                if (!lastSeenDate || new Date().getTime() - lastSeenDate.getTime() > 6 * 31 * 24 * 60 * 60 * 1000) {
                    continue;
                }

                var status = new Date().getTime() - lastSeenDate.getTime() > 5 * 60 * 1000 ? 0 : rows[i].app_status;
                var online = new Date().getTime() - lastSeenDate.getTime() > 5 * 60 * 1000 ? false : rows[i].online;

                var userInfo = {};
                userInfo.fn = rows[i].firstname;
                userInfo.ln = rows[i].lastname;
                userInfo.ph = rows[i].app_picture;
                userInfo.lng = rows[i].longitude;
                userInfo.lat = rows[i].latitude;
                userInfo.acc = rows[i].accuracy;
                userInfo.num = rows[i].num;
                userInfo.ls = rows[i].lastseen;
                userInfo.on = online;
                userInfo.st = status;
                obj.circle_users.push(userInfo);
            }
        }
        callback(undefined, obj);
    });
}

function syncSet(req, res) {
    var cInsert = function(callback) {
        db.query_err("SELECT a.number FROM app a, device d WHERE a.id=d.app_id AND d.user_key=" + db.escape(req.params.user_key),
                function(err, rows) {
            if (err || !rows || !rows[0]) {
                var error = err ? err.message : "Number not found";
                common.error("syncSet", error);
                return res.status(500).json({ "code" : -1 });
            }
            db.contacts.update({ "number" : rows[0].number }, {
                "$inc" : { "version" : 1 },
                "$set" : { "contacts" : fields.contacts, "updated" : new Date().getTime() }
            }, { "upsert" : true }, function (errUp, resultUp) {
                db.contacts.find({ "number" : rows[0].number }, { "version" : 1 }).toArray(function (errFind, resultFind) {
                    var version = resultFind && resultFind[0] ? resultFind[0].version : 0;
                    callback(undefined, rows[0].number, version);
                });
            });
        });
    }
    var fields = req.fields;
    var str = "";
    for (var i = 0; i < fields.contacts.length; i++) {
        for (var j = 0; j < fields.contacts[i].nums.length; j++) {
            str += "'" + fields.contacts[i].nums[j].replace('+','') + "',"
        }
    }
    str = str.slice(0, -1);
    if (str == "") return res.json({ "circle_users" : [] });
    cInsert(function(err, number, version) {
        syncSet2(number, str, function(error, reply) {
            reply.version = version;
            res.json(reply);
        });
    });
}
