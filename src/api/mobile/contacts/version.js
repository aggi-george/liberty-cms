var db = require(__lib + '/db_handler');
var common = require(__lib + '/common');

//exports

exports.versionGet = versionGet;

//functions

function versionGet(req, res) {
    db.query_noerr("UPDATE device d, app a SET a.app_lastseen=NOW() WHERE d.app_id=a.id AND d.user_key="
    + db.escape(req.params.user_key), function () {
    });

    var q = "SELECT a.number FROM app a, device d WHERE d.app_id=a.id AND d.user_key="
        + db.escape(req.params.user_key) + " LIMIT 1";

    db.query(res, q, function (rows) {
        if (rows.length > 0) {
            db.contacts.find({ "number" : rows[0].number }, { "version" : 1 }).toArray(function (err, result) {
                if (err || !result) res.json({"code": 0, "version": -1});
                else res.json({"code": 0, "version": result.version});
            });
        } else {
            res.json({"code": 0, "version": -1});
        }
    });
}
