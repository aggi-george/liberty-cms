var push = require(__base + '/src/notifications/push');
var postFieldsLimit = 20;

//exports

exports.init = init;

//functions

function init(baseApi, category, web, webs) {
    var path = baseApi + category;

    //Parse params
    webs.post(path + '/*', parsePost);
    webs.get(path + '/*', parseGet);
    web.post(path + '/*', parsePost);
    web.get(path + '/*', parseGet);

    //Account - Usage API
    web.get(path + '/push/send', appleSend);
}

function parsePost(req, res, next) {
    common.formParse(req, res, postFieldsLimit, function(fields) {
        req.fields = fields;
        next();
    });
}

function parseGet(req, res, next) {
    next();
}

function appleSend(req, res) {
    var data = {
        "mime_type" : "text",
        "title" : req.query.title,
        "message" : req.query.message
    };
    push.send(req.query.id, data, req.query.platform, req.query.flavor);
    res.json({});
}
