var async = require('async');
var fs = require('fs');
var csvParse = require('csv-parse');
var db = require(__lib + '/db_handler');
var common = require(__lib + '/common');
var ec = require(__lib + '/elitecore');
var config = require(__base + '/config');
var usage = require('../account/usage');
var usageManager = require('../../../core/manager/account/usageManager');

var putFieldsLimit = 20;

var LOG = config.LOGSENABLED;

//exports

exports.init = init;

//functions

function init(baseApi, category, web) {
	var path = baseApi + category;

	//Auth
	web.all(path + '/*/:key', authExternal);

	//external - rates set
	var usagePath = path + '/gentwo';
	web.put(usagePath + '/set/rates/:id/:key', parseFormFiles);
	web.put(usagePath + '/set/rates/:id/:key', ratesSet);

	//Parse params
	web.put(path + '/*', parseForm);
	web.post(path + '/*', parseForm);

	//external - get push tokens
	var usagePath = path + '/push';
	web.get(usagePath + '/tokens/get/:prefix/:number/:app_type/:key', getPushTokens);

	//external - delete user apps
	var usagePath = path + '/apps';
	web.delete(usagePath + '/delete/:prefix/:number/:app_type/:key', deleteApps);

	//external - selfcare get all
	var usagePath = path + '/apps';
	web.get(usagePath + '/get/installs/:app_type/:key', appGetInstalls);

	//external - finance get
	var usagePath = path + '/finance';
	web.get(usagePath + '/get/stats/:key', financeGet);

	//external - cdr get
	var usagePath = path + '/gentwo';
	web.get(usagePath + '/get/cdr/:first/:last/:key', cdrGet);
	web.post(usagePath + '/post/cdr/:callid/:key', cdrPost);

	//external - rates get
	web.get(usagePath + '/get/rates/:key', ratesGet);

	//external - gentwo customer manager
	web.post(usagePath + '/manage/customer/:prefix/:number/:key', customerManager);

	//external - api stats
	var usagePath = path + '/stats';
	web.get(usagePath + '/api/:key', apiStatsGet);

}

function authExternal(req, res, next) {
	var allowed = [ "10.20.1.12",
			"10.20.1.22",
			"10.20.1.23",
			"10.20.1.27",
			"10.20.1.28",
			"10.20.1.37",
			"10.20.1.38",
			"10.20.1.47",
			"10.20.1.48" ];
	if ( (allowed.indexOf(req.ip) > -1) && (req.params.key == config.MOBILE_API_KEY) ) {
		next();
	} else {
		res.status(403).json({ "code" : -1 });
	}
}

function parseForm(req, res, next) {
	common.formParse(req, res, putFieldsLimit, function(fields) {
		req.fields = fields;
		next();
	});
}

function parseFormFiles(req, res, next) {
	common.formParseFiles(req, res, putFieldsLimit, function(fields, files) {
		req.fields = fields;
		req.files = files;
		next();
	});
}

function getPushTokens(req, res) {
	var number = req.params.prefix + "" + req.params.number;
	var cond = "";
	if ( req.params.app_type != "all" ) {
		cond = " AND a.app_type=" + db.escape(req.params.app_type) + " ";
	} 
	var q = "SELECT a.app_type, d.device_id, d.status, d.type token_type, d.token, d.vapn, d.flavor FROM app a, device d WHERE a.id=d.app_id " + cond + " AND a.number=" + db.escape(number);	
	if (LOG) common.log("external getPushTokens", q);
	db.query_noerr(q, function(rows) {
		res.json({ "code" : 0, "list" : rows });
	});
}

function deleteApps(req, res) {
	var number = req.params.prefix + "" + req.params.number;
	var cond = "";
	if ( req.params.app_type != "all" ) {
		cond = " AND app_type=" + db.escape(req.params.app_type) + " ";
	} 
	var q = "DELETE FROM app WHERE number=" + db.escape(number) + "" + cond;
	if (LOG) common.log("external deleteApps", q);
	db.query_noerr(q, function(rows) {
		res.json({ "code" : 0 });
	});
}

function apiStatsGet(req, res) {
	db.cache_keys("api_stats", config.MYSELF + "_agg_*", function(keys) {
		var tasks = new Array;
		if (keys) {
			keys.forEach(function (key) {
				tasks.push(function (callback) {
					db.cache_get("api_stats", key, function (value) {
						callback(null, value);	
					});
				});
			});	
			async.parallel(tasks, function (err, result) {
				var total = new Array;
				if (result && result[0] && result[1]) {
					var a = result[0];
					var b = result[1];
					for ( var i=0; i<a.length; i++ ) {
						total[i] = [ i, (a[i] + b[i]) ];
					}
				}
				res.json({ "code" : 0, "total" : total });
			});
		} else {
			res.json({ "code" : 0, "total" : [] });
		}
	});
}

function appGetInstalls(req, res) {
	var q = "SELECT number FROM app WHERE app_type='selfcare'";
	db.query_noerr(q, function (rows) {
		var numbers = new Array;
		rows.forEach(function (number) {
			numbers.push(number);
		});
		res.json({ "code" : 0, "list" : numbers });
	});
}

function financeGet(req, res) {
	var monthNames = [ "January", "February", "March", "April",
				"May", "June","July", "August", 
				"September", "October", "November", "December" ];
	var month = req.query.month;
	var year = req.query.year;
	var today = (month && year) ? common.getDateTime(new Date(parseInt(year), parseInt(month) + 1, 0, 23, 59, 59, 999)) :
		common.getDateTime(new Date());
	var tasks = new Array;
	tasks.push(function (callback) {
		var q = "SELECT (m.total - IFNULL(a.total,0) - IFNULL(g.total,0)) total FROM (SELECT SUM(credit) total FROM msisdn) m, (SELECT SUM(user_credit) total FROM inapp_purchase WHERE DATE(ts_start)>DATE(" + db.escape(today) + ")) a, (SELECT SUM(user_credit) total FROM android_inapp WHERE DATE(creationdate)>DATE(" + db.escape(today) + ")) g";
		db.query_noerr(q, function (rows) {
			callback(undefined, { "name" : "System Liability", "date" : common.getDateTime(today), "value" : rows[0].total });
		});
	});
	tasks.push(function (callback) {
		var q = "SELECT IF(SUM(user_credit) IS NULL,0,SUM(user_credit)) total, CONCAT(MONTHNAME(" + db.escape(today) + "), ' ', YEAR(DATE(" + db.escape(today) + "))) monthyear FROM inapp_purchase WHERE YEAR(ts_start)=YEAR(" + db.escape(today) + ") AND MONTH(ts_start)=MONTH(DATE(" + db.escape(today) + "))";
		db.query_noerr(q, function (rows) {
			callback(undefined, { "name" : "Apple Topup", "date" : rows[0].monthyear, "value" : rows[0].total });
		});
	});
	tasks.push(function (callback) {
		var q = "SELECT IF(SUM(actual_credit) IS NULL,0,SUM(actual_credit)) total, CONCAT(MONTHNAME(" + db.escape(today) + "), ' ', YEAR(" + db.escape(today) + ")) monthyear FROM android_inapp WHERE YEAR(creationdate)=YEAR(" + db.escape(today) + ") AND MONTH(creationdate)=MONTH(" + db.escape(today) + ")";
		db.query_noerr(q, function (rows) {
			callback(undefined, { "name" : "Google Topup", "date" : rows[0].monthyear, "value" : rows[0].total });
		});
	});
	var today = new Date(today);
	var thisMonth = monthNames[today.getMonth()] + " " + today.getFullYear(); 
	var firstDay = new Date(today.getFullYear(), today.getMonth(), 1).getTime() / 1000;	// temp / 1000
	var lastDay = (new Date(today.getFullYear(), today.getMonth() + 1, 1).getTime()-1) / 1000;	// temp / 1000
	var agg = [ { "$match" : { "start_time" : { "$gte" : firstDay, "$lt" : lastDay } } },
				{ "$group" : { "_id" : "total" } } ];
	tasks.push(function (callback) {
		var agg_array = JSON.parse(JSON.stringify(agg));
		agg_array[1]["$group"].total = { "$sum" : "$ingress_cost" };
		db.cdrs.aggregate(agg_array, function (err, result) {
			var total = (result && result[0]) ? result[0].total : 0;
			callback(undefined, { "name" : "Charge of calls",
						"date" : thisMonth,
						"value" : Math.round(total * 1000000) / 1000000 });
		});
	});
	tasks.push(function (callback) {
		var agg_array = JSON.parse(JSON.stringify(agg));
		agg_array[1]["$group"].total = { "$sum" : "$egress_cost" };
		db.cdrs.aggregate(agg_array, function (err, result) {
			var total = (result && result[0]) ? result[0].total : 0;
			callback(undefined, { "name" : "Cost of calls",
						"date" : thisMonth,
						"value" : Math.round(total * 1000000) / 1000000 });
		});
	});

	// get usage in the future to adjust liability
	tasks.push(function (callback) {
		var agg_array = [ { "$match" : { "start_time" : { "$gt" : lastDay } } },
					{ "$group" : { "_id" : "total" } } ];
		agg_array[1]["$group"].total = { "$sum" : "$ingress_cost" };
		db.cdrs.aggregate(agg_array, function (err, result) {
			var total = (result && result[0]) ? result[0].total : 0;
			callback(undefined, { "future_usage" : Math.round(total * 1000000) / 1000000 });
		});
	});

	async.parallel(tasks, function(err, result) {
		res.json({ "code" : 0, "list" : result });
	});
}

function cdrGet(req, res) {
	var prefix = (req.query.prefix) ? req.query.prefix : undefined;
	var number = (req.query.number) ? req.query.number : undefined;
	var first = parseInt(req.params.first);
	var last = parseInt(req.params.last);
	var match = { "start_time" : { "$gte" : first, "$lt" : last } };
	if (prefix && number) {
		match["$or"] = [ { "orig_ani" : prefix + "" + number }, { "orig_dst_number" : prefix + "" + number } ];
	}
	db.cdrs.find(match).sort({ "start_time" : -1 }).toArray(function (err, result) {
		res.json({ "code" : 0, "list" : result });
	});
}

function cdrCache(call_id, release_cause, callback) {
	if ( release_cause == "BYE" ) {
		setTimeout(function () {
			db.cache_get("cache", "cdr_" + call_id, function(cdr) {
				callback(cdr);
			});
		}, 3000);
	} else {
		db.cache_get("cache", "cdr_" + call_id, function(cdr) {
			callback(cdr);
		});
	}
}

function fieldsFill(fields, cdr) {
	cdr.term_dst_number = fields.term_dst_number;
	cdr.ingress_rate = parseFloat(fields.ingress_rate);
	cdr.egress_carrier_name = fields.egress_carrier_name;
	cdr.egress_rate = parseFloat(fields.egress_rate);
	cdr.egress_bill_start = fields.egress_bill_start;
	cdr.egress_bill_increment = fields.egress_bill_increment;
	cdr.ingress_bill_start = fields.ingress_bill_start;
	cdr.ingress_bill_increment = fields.ingress_bill_increment;
	cdr.term_code_name = fields.term_code_name;
	cdr.term_code = fields.term_code;
	cdr.rate_id = fields.rate_id;
	cdr.pdd = fields.pdd;
	cdr.processing = fields.processing;
	cdr.term_ip = fields.term_ip;
	if (fields.media) cdr.media = fields.media;
	if (fields.release_side) cdr.release_side = fields.release_side;
	if (!cdr.duration) cdr.duration = 0;
	if (!cdr.billtime) cdr.ingress_billtime = 0;
	if (!cdr.egress_trunk_trace) cdr.egress_trunk_trace = "";
}

function cdrPost(req, res) {
	var fields = req.fields;
	cdrCache(fields.call_id, fields.release_cause, function (cdr) {
		// check if CCR happened
		var credit = (cdr && cdr.hasOwnProperty("credit")) ? cdr.credit : undefined;
		var credit_start = (cdr && cdr.hasOwnProperty("credit_start")) ? cdr.credit_start : undefined;
		if (!cdr || !cdr.start_time) {
			fields.credit = credit;
			fields.credit_start = credit_start;
			fields.start_time = parseInt(fields.start_time * 1000);
			if (LOG) common.log("CDR start", JSON.stringify(fields));
			db.cache_put("cache", "cdr_" + fields.call_id, JSON.stringify(fields), 60 * 1000);
		} else {
			if ( (fields.release_cause == "Credit Exhausted") ||
				(fields.release_cause == "BYE") ) {
				cdr.release_side =  fields.release_side;
				cdr.release_cause =  fields.release_cause;
				cdr.response_egress =  fields.response_egress;
				cdr.response_ingress =  fields.response_ingress;
				if (LOG) common.log("CDR bye", JSON.stringify(cdr));
				db.cdrs.insert(cdr, function () { });
				db.cache_del("cache", "active_" + cdr.orig_ani);
			} else if ( (fields.release_cause == "INVITE") && fields.egress_code ) {
				var codeMatch = [ 180, 183, 200 ];
				if ( codeMatch.indexOf(fields.egress_code) > -1 ) {
					if ( !cdr.answer_time && (fields.egress_code == 200) ) {
						fieldsFill(fields, cdr);
						cdr.answer_time = new Date().getTime();
						if (LOG) common.log("CDR answered", JSON.stringify(cdr));
					} else {
						if (LOG) common.log("CDR " + fields.egress_code, JSON.stringify(cdr));
					}
				} else {
					fieldsFill(fields, cdr);
					if (fields.egress_carrier_name && fields.response_egress) {
						cdr.egress_trunk_trace += fields.egress_carrier_name + ":"
								+ fields.response_egress + ";";
					}
					if (LOG) common.log("CDR lcr", JSON.stringify(cdr));
					if (fields.lcr_end != 0) db.cdrs.insert(cdr, function () { });
				}
				db.cache_put("cache", "cdr_" + fields.call_id, JSON.stringify(cdr), 60 * 1000);
			} else {
				fieldsFill(fields, cdr);
				if (fields.egress_carrier_name && fields.response_egress) {
					cdr.egress_trunk_trace += fields.egress_carrier_name + ":"
							+ fields.response_egress + ";";
				}
				if (LOG) common.log("CDR other", JSON.stringify(cdr));
				db.cache_put("cache", "cdr_" + fields.call_id, JSON.stringify(cdr), 60 * 1000);
				if (fields.lcr_end != 0) db.cdrs.insert(cdr, function () { });
			}
		}
		res.json({});
	});
}

function ratesGet(req, res) {
	var fields = req.query;
	var tasks = new Array;
	tasks.push(function (callback) {
		var q = "";
		var lcr = false;
		if ( fields.lcr == "true" ) {
			lcr = true;
			if ( fields.prefix == "" ) {
				res.status(400).json({"code":common.PARSE_ERROR,"details":"LCR Empty prefix","description":"Prefix cannot be empty on LCR"});
				return false;
			}
			var prefixes = "";
			for ( var i=fields.prefix.length-1;i>0;i-- ) prefixes += db.escape(fields.prefix.slice(0,-i)) + ",";
			prefixes += fields.prefix;
			q = "SELECT l.*, IF(pw.prefix IS NOT NULL,1,0) weight FROM (SELECT s.*, (SELECT GROUP_CONCAT(gn.groupName) groupName FROM groupMember gm, groupName gn WHERE gn.groupID=gm.groupID AND s.prefix LIKE CONCAT(gm.prefix,'%') AND s.buyrate < gn.maxRate GROUP BY gm.prefix ORDER BY gm.prefix DESC LIMIT 1) gname FROM (SELECT (SELECT MAX(lastupdate) FROM call_rates WHERE providerID=lcr_t.providerID AND dialprefix=lcr_t.dp AND lastupdate<=NOW()) eff, lcr_t.dp prefix, lcr_t.providerID, c_r.buyrate, c_r.sellrate, c_r.destination, c_r.billstart, c_r.billincrement, lcr_t.name provider, c_r.status crstatus FROM (SELECT MAX(lcr_s.dialprefix) dp, lcr_s.providerID, lcr_s.name FROM (SELECT SUM(IF(cr.status='D',1,0)) del, cr.dialprefix, cr.providerID, cp.name FROM call_rates cr LEFT JOIN call_providers cp ON cr.providerID=cp.providerID LEFT JOIN prefix_weight pw ON cr.dialprefix=pw.prefix AND pw.providerID=cr.providerID, plans p WHERE cr.dialprefix in (" + prefixes + ") AND cr.lastupdate<=NOW() AND p.planID=1 AND FIND_IN_SET(cp.providerID,p.call_providerID) GROUP BY cr.providerID, cr.dialprefix) lcr_s WHERE lcr_s.del=0 GROUP BY lcr_s.providerID) lcr_t LEFT JOIN call_rates c_r ON c_r.dialprefix=lcr_t.dp AND c_r.providerID=lcr_t.providerID WHERE DATE(c_r.lastupdate)<=DATE(NOW()) GROUP BY lcr_t.dp, lcr_t.providerID) s ORDER BY s.buyrate ASC) l LEFT JOIN prefix_weight pw ON l.prefix=pw.prefix AND l.providerID=pw.providerID ORDER BY weight DESC, l.buyrate ASC"
		} else {
			var destCond = "";
			var prefixCond = "";
			var providerCond = "";
			var groupCond1 = "";
			var groupCond2 = "";
			if ( fields.dest && fields.dest != "" ) destCond = " AND cr.destination LIKE " + db.escape(fields.dest + "%");
			if ( fields.prefix && fields.prefix != "" ) prefixCond = " AND cr.dialprefix LIKE " + db.escape(fields.prefix + "%");
			if ( fields.provider && fields.provider != "" ) providerCond = " AND cr.providerID IN (" + db.escape(fields.provider).slice(0,-1).slice(1) + ")";
			if ( fields.group && fields.groups != "" ) {
				groupCond1 = " AND gm.groupID IN (" + db.escape(fields.group).slice(0,-1).slice(1) + ")";
				groupCond2 = " WHERE l.gname != ''";
			}
			q = "SELECT l.* FROM (SELECT cr.dialprefix prefix, cr.destination, cr.buyrate, cr.sellrate, cr.billstart, cr.billincrement, cr.status crstatus, cr.lastupdate eff, cp.name provider, (SELECT GROUP_CONCAT(gn.groupName) groupName FROM groupMember gm, groupName gn WHERE gn.groupID=gm.groupID " + groupCond1 + " AND cr.dialprefix LIKE CONCAT(gm.prefix,'%') AND cr.buyrate < gn.maxRate GROUP BY gm.prefix ORDER BY gm.prefix DESC LIMIT 1) gname FROM call_rates cr, call_providers cp WHERE cr.providerID=cp.providerID " + prefixCond + " " + destCond + " " + providerCond + ") l " + groupCond2 + " LIMIT 10000";
		}
		if (LOG) common.log("ratesGet", q);
		db.query_noerr(q, function(rows) {
			callback(undefined, { "rates" : rows });
		});	
	});
	tasks.push(function (callback) {
		db.query_noerr("SELECT groupID, groupName FROM groupName WHERE qty!=0 AND status='A'", function(rows) {
			callback(undefined, { "groups" : rows });
		});
	});
	tasks.push(function (callback) {
		db.query_noerr("SELECT providerID, name, host, prefix, rounding FROM call_providers", function(rows) {
			callback(undefined, { "providers" : rows });
		});
	});
	async.parallel(tasks, function (err, result) {
		var reply = new Object;
		result.forEach(function (item) {
			if (item) {
				if (item.providers) reply.providers = item.providers;
				if (item.groups) reply.groups = item.groups;
				if (item.rates) reply.rates = item.rates;
			}
		});
		res.json({ "code" : 0, "rates" : reply.rates, "providers" : reply.providers, "groups" : reply.groups });
	});
}

function dash2Seq(dash) {
	var start = parseInt(dash.split("-")[0]);
	var end = parseInt(dash.split("-")[1]);
	var seq = new Array;
	for ( var i=start; i<=end; i++ ) {
		seq.push(i);
	}
	return seq;
}

function tmPulse(destination) {
	var pulse = { "start" : 1, "increment" : 1 };
	if ( (destination.toUpperCase().indexOf("MEXICO") > -1) ||
		(destination.toUpperCase().indexOf("TONGA") > -1) ||
		(destination.toUpperCase().indexOf("NAURU") > -1) ||
		(destination.toUpperCase().indexOf("PAPUA NEW GUINEA") > -1) ||
		(destination.toUpperCase().indexOf("SURINAM") > -1) ||
		(destination.toUpperCase().indexOf("KIRIBATI") > -1) ||
		(destination.toUpperCase().indexOf("WESTERN SAMOA") > -1) ||
		(destination.toUpperCase().indexOf("VANUATU") > -1) ) {
			pulse.start = 60;
			pulse.increment = 60;
	}
	return pulse;
}

function tataPulse(destination) {
	var pulse = { "start" : 1, "increment" : 1 };
	if ( (destination.toUpperCase().indexOf("MEXICO") > -1) ||
		(destination.toUpperCase().indexOf("TONGA") > -1) ||
		(destination.toUpperCase().indexOf("NAURU") > -1) ||
		(destination.toUpperCase().indexOf("PAPUA NEW GUINEA") > -1) ||
		(destination.toUpperCase().indexOf("SURINAM") > -1) ||
		(destination.toUpperCase().indexOf("KIRIBATI") > -1) ||
		(destination.toUpperCase().indexOf("WESTERN SAMOA") > -1) ||
		(destination.toUpperCase().indexOf("VANUATU") > -1) ) {
			pulse.start = 60;
			pulse.increment = 60;
	} else if ( destination.toUpperCase().indexOf("GAMBIA") > -1 ) {
			pulse.start = 60;
			pulse.increment = 1;
	}
	return pulse;
}

function isDate(s) {
	if (typeof(s) != "string") return false;
	else if (isNaN(Date.parse(s))) return false;
	else if ( Date.parse(s) < 0 ) return false;
	else if ( s.split("-").length != 3 ) return false; // will only accept xx-xx-xxxx
	else return true;
}

function tataParse(id, data) {
	var sheet = new Array;
	data.forEach(function (row) {
		var destination = row[0] + " - " + row[1];
		var countrycode = row[2];
		var areacodes = row[3].split(",");
		var pricing = (row[4] == "") ? "NULL" : parseFloat(row[4]) * 1.07;
		var effective = row[5];
		var sellrate = Math.ceil(parseFloat(row[4]) * 1.07 * 1.3 * 100) / 100;		// GST + 30% markup, 2 decimal
		var billstart = tataPulse(destination).start;
		var billincrement = tataPulse(destination).increment;
		var stat = (pricing == "NULL") ? "D" : "A";
		if (effective && isDate(effective)) {
			areacodes.forEach(function (code) {
				sheet += "(" + db.escape(id) + "," + countrycode + "" + code.trim() + "," + db.escape(destination) + "," + pricing + "," + sellrate + "," + billstart + "," + billincrement + "," + db.escape(new Date(effective).toISOString()) + "," + db.escape(stat) + "),";
			});
		}
	});
	return sheet;
}

function tmParse(id, data) {
	var sheet = new Array;
	data.forEach(function (row) {
		var destination = row[0];
		var product = row[1];
		var areacode = row[2];
		var asr = row[3];
		var unknown = row[4];
		var cli = row[5];
		var unknown = row[6];
		var pricing = (row[7] == "") ? "NULL" : parseFloat(row[7]);
		var effective = row[8];
		var sellrate = Math.ceil(parseFloat(row[7]) * 1.3 * 100) / 100;		// 30% markup, 2 decimal
		var billstart = tmPulse(destination).start;
		var billincrement = tmPulse(destination).increment;
		var stat = (pricing == "NULL") ? "D" : "A";
		if ( product.toUpperCase() == "TMSG PREMIUM" ) {
			var areacodes = new Array;
			areacode.split(";").forEach(function (code) {
				if ( code.indexOf("-") > -1 ) {
					if (areacodes.length) areacodes.concat(dash2Seq(code));
					else areacodes = dash2Seq(code);
				} else {
					areacodes.push(code);
				}
			});
			areacodes.forEach(function (code) {
				if (code == "628681") stat = "D";
				sheet += "(" + db.escape(id) + "," + code + "," + db.escape(destination) + "," + pricing + "," + sellrate + "," + billstart + "," + billincrement + "," + db.escape(new Date(effective).toISOString()) + "," + db.escape(stat) + "),";
			});
		}
	});
	return sheet;
}

function ratesSet(req, res) {
	var fields = req.fields;
	var q_cp = "UPDATE call_providers set name=" + db.escape(fields.name) + ",host=" + db.escape(fields.host) + ",prefix=" + db.escape(fields.prefix) + ",rounding=" + db.escape(fields.rounding) + " WHERE providerID=" + db.escape(req.params.id);
	db.query_noerr(q_cp, function(result) {
		if (req.files.content) {
			var path = req.files.content.path;
			var name = req.files.content.name;
			var providerID = req.params.id;
			csvParse(fs.readFileSync(path, "utf8"), function(err, data) {
				var values = (providerID == "10") ? tmParse(providerID, data) :
						(providerID == "7") ? tataParse(providerID, data) : undefined;
				fs.unlink(path);
				if (!values) {
					res.json({ "code" : -1 });
					return false;
				}
				var q_ins = "INSERT INTO call_rates (providerID,dialprefix,destination,buyrate,sellrate,billstart,billincrement,lastupdate,status) VALUES " + values.slice(0,-1) + " ON DUPLICATE KEY UPDATE buyrate=VALUES(buyrate), sellrate=VALUES(sellrate)";
				db.query_noerr(q_ins, function(result) {
					// do cleanup: remove old rates (more than 2 from latest)
					var q_old = "DELETE cr FROM call_rates cr LEFT JOIN (SELECT COUNT(c.callrateID) tot, MAX(c.lastupdate) eff, c.dialprefix FROM call_rates c WHERE c.providerID=" + db.escape(providerID) + " AND c.lastupdate<NOW() GROUP BY c.dialprefix) s ON cr.dialprefix=s.dialprefix WHERE s.tot>1 AND cr.lastupdate!=s.eff AND cr.providerID=" + db.escape(providerID);
					db.query_noerr(q_old, function(result) { });
					//remove "D" (deleted prefixes) if older than today
					var q_del = "DELETE FROM call_rates WHERE providerID=" + db.escape(providerID) + " AND dialprefix IN (SELECT dialprefix FROM call_rates WHERE providerID=" + db.escape(providerID) + " AND buyrate IS NULL AND sellrate IS NULL AND status='D' AND lastupdate<NOW())";
					db.query_noerr(q_del, function(result) { });
					// update prefix_weight ???
					res.json({ "code" : 0 });
				});
			});
		} else res.json({ "code" : 0 });
	});
}

function creditManage(number, amount, callback) {
	var q = "SELECT m.plan_id, m.credit, c.vsUSD, m.credit * c.vsUSD sgd FROM msisdn m, currencies c WHERE c.currencyCode='SGD' AND number=" + db.escape(number);
	if (LOG) common.log("creditManage", q);
	db.query_noerr(q, function (rows) {
		if (!rows || !rows[0]) {
			callback(undefined, { "credit" : 0, "sgd" : 0 });
		} else if (!amount) {
			callback(undefined, { "plan_id" : rows[0].plan_id, "credit" : rows[0].credit, "sgd" : rows[0].sgd });
		} else {
			var q2 = "UPDATE msisdn set credit=credit+" + db.escape(amount) + " WHERE number=" + db.escape(number);
			if (LOG) common.log("creditManage", q2);
			db.query_noerr(q2, function(result) {
				if ( result && (result.affectedRows > 0) ) { 
					callback(undefined,  { "plan_id" : rows[0].plan_id, "credit" : rows[0].credit, "sgd" : rows[0].sgd,
							"newCredit" : rows[0].credit + amount,
							"newSgd" : ((rows[0].credit + amount) * rows[0].vsUSD) });
				} else {
					callback(true, { "plan_id" : rows[0].plan_id, "credit" : rows[0].credit, "sgd" : rows[0].sgd });
				}
				db.cdrs.insert({ "start_time" : new Date().getTime(),
						"orig_ani" : number,
						"previous" : rows[0].credit,
						"top_up" : amount,
						"credit" : rows[0].credit + amount }, function () { });
			});
		}
	});
}

function customerManager(req, res) {
	var number = req.params.prefix + "" + req.params.number;
	var amount = (req.fields.amount == "1") ? 0.99 :
			(req.fields.amount == "5") ? 4.99 :
			(req.fields.amount == "10") ? 9.99 :
			(parseFloat(req.fields.amount) == req.fields.amount) ? parseFloat(req.fields.amount) : undefined;
	var plan = parseInt(req.fields.plan);
	if (plan) {
		db.query_noerr("UPDATE msisdn SET plan_id=" + db.escape(plan) + " WHERE number=" + db.escape(number), function () {
			res.json({ "code" : 0, "plan_id" : plan });
		});
	} else {
		creditManage(number, amount, function (err, result) {
			db.query_noerr("SELECT planID, name FROM plans", function (rows) {
				if (!err && result) {
					var reply = { "code" : 0, "plan_id" : result.plan_id, "credit" : result.credit, "sgd" : result.sgd, "plans" : rows };
					if (amount) {
						reply.newCredit = result.newCredit;
						reply.newSgd = result.newSgd;
					}
					res.json(reply);
				} else {
					res.json({ "code" : -1, "plan_id" : result.plan_id, "credit" : result.credit, "sgd" : result.sgd, "plans" : rows });
				}
			});
		});
	}
}
