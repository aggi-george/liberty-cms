var db = require(__lib + '/db_handler');
var common = require(__lib + '/common');
var inappManager = require(__core + '/manager/inapp/inappManager');

var checkSessionEnabled = true;
var postFieldsLimit = 20;

//exports

exports.init = init;

//functions

function init(baseApi, category, web, webs) {
    var path = baseApi + category;

    //Parse params
    webs.post(path + '/*', parsePost);

    //Check session
    webs.post(path + '/*/:user_key/:signature', sessionCheckPost);

    //inapp - Purchase API
    var usagePath = path + '/purchase';
    webs.post(usagePath + '/end/:user_key/:signature', end);
}

function parsePost(req, res, next) {
    common.formParse(req, res, postFieldsLimit, function(fields) {
        req.fields = fields;
        next();
    });
}

function sessionCheckPost(req, res, next) {
    if (checkSessionEnabled) {
        db.sessionCheckPost(req, res, req.fields, function(req, res) {
            req.fields.platform = "apple";
            next();
        });
    } else {
        req.fields.platform = "apple";
        next();
    }
}


function end(req, res) {
    common.logUrl(req);
    var user_key = req.params.user_key;
    var transactionId = req.fields.transactionId;
    var transactionReceipt = req.fields.transactionReceipt;
    var productId = req.fields.productId;
    var sandbox = req.fields.sandbox;
    inappManager.apple.validate(transactionId, transactionReceipt, productId, false, function (err, result) {
        if (err || !result || result.status != "Valid") {
            common.error("verifyPayment apple err " + (err ? err.message : ""), JSON.stringify(result));
            if ( err && (err.status || (err.message.indexOf("Wrong product ID") > -1)) ) {
                // err.status is known apple error
                // OR wrong product ID
                res.json({ "code" : -1, "err" : err });
            } else if ( err && (err.code && (err.code == "ETIMEDOUT")) ) {
                res.status(408).json({ "code" : 408, "err" : "Request Timeout" });
            } else {
                res.status(400).json({"code":400, "err" : err});
            }
            return;
        }
        inappManager.apple.end(user_key, transactionId, transactionReceipt, productId, sandbox, function(err, result) {
            if (err) res.json({ "code" : -1, "response" : result });
            else res.json({ "code" : 0 });
        });
    });
}
