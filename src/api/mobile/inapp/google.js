var db = require(__lib + '/db_handler');
var common = require(__lib + '/common');
var config = require(__base + '/config');
var md5 = require('MD5');
var inappManager = require(__core + '/manager/inapp/inappManager');

var checkSessionEnabled = true;
var postFieldsLimit = 20;

//exports

exports.init = init;

//functions

function init(baseApi, category, web, webs) {
    var path = baseApi + category;

    //Parse params
    webs.post(path + '/*', parsePost);

    //Check session
    webs.post(path + '/*/:user_key/:signature', sessionCheckPost);

    //inapp - Purchase API
    var usagePath = path + '/purchase';
    webs.post(usagePath + '/start/:user_key/:signature', start);
    webs.post(usagePath + '/cancel/:user_key/:signature', cancel);
    webs.post(usagePath + '/end/:user_key/:signature', end);
    webs.post(usagePath + '/subscription/cancel/:user_key/:signature', subCancel);
}

function parsePost(req, res, next) {
    common.formParse(req, res, postFieldsLimit, function(fields) {
        req.fields = fields;
        next();
    });
}

function sessionCheckPost(req, res, next) {
    if (checkSessionEnabled) {
        db.sessionCheckPost(req, res, req.fields, function(req, res) {
            req.fields.platform = "google";
            next();
        });
    } else {
        req.fields.platform = "google";
        next();
    }
}

function subCancel(req, res) {
    common.logUrl(req);
    inappManager.google.cancelSubscription(req.fields.token, req.fields.sku, function (err, response) {
        if ( !err ) res.json({ "code" : 0 });
        else res.json({ "code" : 500 });
    });
}

function start(req, res) {
    common.logUrl(req);
    var payload = md5(req.params.user_key + " " + new Date());
    inappManager.google.start(req.params.user_key, payload, req.fields.email, req.fields.sku, function(err, result) {
        if (!err) res.json({ "code" : 0, "payload" : payload });
        else res.json({ "code" : 500 });
    });
}

function cancel(req, res) {
    common.logUrl(req);
    inappManager.google.cancel(req.params.user_key, req.fields.payload, req.fields.error_code, function(err, result) {
        if (!err) res.json({ "code" : 0 });
        else res.json({ "code" : 500 });
    });
}

function end(req, res) {
    common.logUrl(req);
    var user_key = req.params.user_key;
    var order_id = req.fields.order_id;
    var token = req.fields.token;
    var sku = req.fields.sku;
    var payload = req.fields.payload;
    inappManager.google.validate(order_id, token, sku, false, function(err, result) {
        if (err || !result || result.status != "Valid") {
            common.error("verifyPayment google err", (err ? err.message : JSON.stringify(result)));
            if (err && err.status) {
                res.json({ "code" : -1, "err" : err });
            } else if ( err && err.code && (err.code == "ETIMEDOUT") ) {
                res.status(408).json({ "code" : 408, "err" : "Request Timeout" });
            } else {
                res.status(400).json({"code":400, "err" : err});
            }
            return;
        }
        var response = result.result;
        var process = function(callback) {
            if ( response.receipt.kind == "androidpublisher#subscriptionPurchase" ) { // check for purchaseState?
                var expiry = new Date(parseInt(response.receipt.expiryTimeMillis));
                var autoRenewing = ( !err && response && response.result && response.result.receipt &&
                    (response.result.receipt.kind == "androidpublisher#subscriptionPurchase") ) ? response.result.receipt.autoRenewing : undefined;
                inappManager.google.endSubscription(user_key, payload, token, sku, expiry, autoRenewing, callback);
            } else if ( (response.receipt.developerPayload == payload) && (response.receipt.purchaseState == 0) ) {
                inappManager.google.end(user_key, payload, token, sku, order_id,
                    response.receipt.purchaseTimeMillis, response.receipt.purchaseState, callback);
            } else {
                callback(new Error("Invalid Token"));
            }
        } 
        process(function(pError, pResult) {
            if (pError) res.json({ "code" : -1, "response" : response });
            else res.json({ "code" : 0 });
        });
    });
}
