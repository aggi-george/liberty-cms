var request = require('request');
var md5 = require('MD5');
var db = require(__lib + '/db_handler');
var common = require(__lib + '/common');
var config = require(__base + '/config');
const ecommManager = require(`${__manager}/ecomm/ecommManager`);

//exports

exports.init = init;

//functions

function init(baseApi, category, web, webs) {
    var path = baseApi + category;
    webs.get(path + '/update/creditcard/status/:type/:refNum/:signature', function (req, res) {
        const refNum = req.params.refNum;
        const linkId = req.params.signature;
        const type = req.params.type;
        let getHTML = (err, result) => {
            const header = `<!doctype html><html lang="en"><head><meta charset="utf-8"><title>Update Credit Card</title>
                <style>body{font-family: helvetica, sans-serif;font-size: 16px;text-align: center;}</style></head><body>`;
            const footer = `</body></html>`;
            if (err || result.status != 'updated_elitecore') {
                return `${header}
                    <h3 style="margin-bottom: 0px;">Card update failed :(</h3>
                    <p style="margin-top: 8px;">Please reach out to our Customer Happiness Experts.</p>
                    ${footer}`;
            } else {
                const paytype = result.paytype.toUpperCase();
                const tm_cc_last4_digit = result.tm_cc_last4_digit;
                return `${header}
                    <h3 style="margin-bottom: 0px;">Card update successful!</h3>
                    <p style="margin-top: 8px;">Your monthly bill will now be charged to ${paytype} ${tm_cc_last4_digit}</p>
                    ${footer}`;
            }
        }
        ecommManager.creditCardUpdateStatus(linkId, refNum, (err, result) => {
            if (err || !result) {
                if (type == 'html') return res.status(400).send(getHTML(err));
                else return res.status(400).json({ code: -1 });
            }
            if (type == 'html') res.send(getHTML(undefined, result));
            else res.json(result);
        });
    });

    webs.get(path + '/:linkId/:signature', function (req, res) {

        var linkId = req.params.linkId;
        var signature = req.params.signature;
        common.error('Credit Card Update TempLink [DEPRECATED]', linkId);

        if (signature !== md5(linkId + config.TEMP_LINK_SOLT)) {
            return res.status(400).send("Invalid");
        }

        db.templinks.find({linkId: linkId, expiryTs: {"$gt": new Date().getTime()}}).toArray(function (err, result) {
            if (err) {
                return res.status(500).send("Error");
            }

            if (!result || !result[0]) {
                return res.status(400).send('Expired');
            }

            res.redirect(result[0].url);
        });
    });

    webs.get(path + '/debug/uber/login', function (req, res) {
        //https://10.20.1.10:16443/link/debug/uber/login

        var host = "10.20.1.10:16443";
        //var host = config.PUBLIC_MYFQDN + ":" + config.PUBLIC_WEBSPORT;

        var loginUrl = "https://login.uber.com/oauth/v2/authorize?" +
            "client_id=" + config.UBER_CLIENT_ID + "&" +
            "response_type=code&" +
            "scope=partner.accounts&" +
            "redirect_uri=" + encodeURI("https://" + host + "/link/debug/uber/token");

        console.log("UBER: loginUrl=" + loginUrl)
        res.redirect(loginUrl);
    });

    webs.get(path + '/debug/uber/barrier', function (req, res) {
        res.json(req.query)
    });

    webs.get(path + '/debug/uber/token', function (req, res) {
        res.json(req.query)
    });

    webs.get(path + '/bonus/total/ahi24ns892fnut7ow9dp59hs9d73hbrs845bvsaqqu402ejbr6332jwsdv72234022834ha5dktg5736e234bsodjf0ff923sdg67sd',
        function (req, res) {

            var key = config.NOTIFICATION_KEY;
            var url = "http://" + config.CMSHOST + ":" + config.CMSPORT + "/api/1/bonus/data/total/get/" + key;

            request({
                uri: url,
                method: 'GET'
            }, function (err, response, body) {

                if (err) {
                    return res.status(500).send("Error");
                }

                var reply = common.safeParse(body);
                res.json(reply);
            });
    });
}
