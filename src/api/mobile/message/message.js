var db = require(__lib + '/db_handler');
var config = require(__base + '/config');
var push = require(__base + '/src/notifications/push');
var common = require(__lib + '/common');

var checkSessionEnabled = true;
var postFieldsLimit = 20;

var logsEnabled = config.LOGSENABLED;

//exports

exports.init = init;

//functions

function init(baseApi, category, web, webs) {
	var path = baseApi + category;

	//Parse params
	web.post(path + '/*', parsePost);

	//Check session
	web.post(path + '/*/:user_key/:signature', sessionCheckPost);

	// message - Buzz API
	var usagePath = path + '/buzz';
	web.post(usagePath + '/send/:user_key/:signature', buzzSend);

	// message - Wakeup API
	var usagePath = path + '/wakeup';
	web.get(usagePath + '/:from/:to', wakeup);

	// message - Dialog API
	var usagePath = path + '/dialog';
	web.post(usagePath + '', dialog);

	// message - Report API
	var usagePath = path + '/report';
	web.get(usagePath + '', report);
}

function parsePost(req, res, next) {
	common.formParse(req, res, postFieldsLimit, function(fields) {
		req.fields = fields;
		next();
	});
}

function sessionCheckPost(req, res, next) {
	if (checkSessionEnabled) {
		db.sessionCheckPost(req, res, req.fields, function(req, res) {
			next();
		});
	} else {
		next();
	}
}

function buzzSend(req, res) {
	var fields = req.fields;
	var q = "SELECT t.token, t.type, t.flavor, s.number num, co.contacts FROM (SELECT a1.number, d1.token, d1.type, d1.flavor FROM app a1, device d1 WHERE d1.app_id=a1.id AND a1.app_type='gentwo' AND d1.status='A') t LEFT JOIN contacts co ON co.number=t.number, (SELECT a2.number FROM app a2, device d2 WHERE d2.app_id=a2.id AND d2.user_key=" + db.escape(req.params.user_key) + ") s WHERE t.token IS NOT NULL  AND t.number=REPLACE(REPLACE(" + db.escape(fields.phone_number) + ",' ',''),'+','')";
	if (logsEnabled) common.log("buzzSend", q);
	db.query(res, q, function(rows) { 
		if ( rows[0] && rows[0].token ) {
			var data = { "phone" : "+" + rows[0].num, "mime_type" : common.MIME_TYPE_BUZZ };
			if ( rows[0].type == "apn" ) {
				data.sender = "";
				if ( rows[0].contacts )
					data.sender = common.getContactName(rows[0].num, common.safeParse(rows[0].contacts));
				data.loc_key = "NOTIFICATION_MESSAGE_BZZ_FROM";
			}
			push.send(rows[0].token, data, rows[0].type, rows[0].flavor);
			res.json({"code":0});
		} else res.status(404).json({"code":common.USER_NOT_FOUND,"details":"user not found", "description":"Unknown User"});
	});
}

function wakeup(req, res) {		// only applies to smartbzz
	if ( config.WAKEUPSRC.indexOf(req.ip) == -1 ) res.status(403).json({"code":common.USER_INVALID,"details":"Error","description":"Unauthorized"});
	else {
		var src = req.params.from ? req.params.from.replace(' ', '').replace('+', '') : "";
		var dst = req.params.to ? req.params.to.replace(' ', '').replace('+', '') : "";
		if (logsEnabled) common.log("wakeup", dst);
		var q = "SELECT d.token, d.vapn, d.type, d.flavor, a.number num FROM app a LEFT JOIN device d ON a.id=d.app_id WHERE a.app_type='gentwo' AND a.status='A' AND d.status='A' AND a.number=" + db.escape(dst);
		db.query(res, q, function(rows) { 
			var data = { "reason" : "UNREACHABLE", "mime_type" : common.MIME_TYPE_WAKEUP };
			if ( rows[0] && rows[0].vapn && (rows[0].type == "apn") ) {
				db.contacts.find({ "number" : dst },
						{ "contacts" : { "$elemMatch" : { "nums" : "+" + src } } })
						.toArray(function (err, result) {
					data.sender = (result && result[0] && result[0].contacts && result[0].contacts[0]) ?
						(result[0].contacts[0].fn + " " + result[0].contacts[0].ln) : "";
					data.loc_key = "NOTIFICATION_INCOMING_CALL_FROM";
					push.send(rows[0].vapn, data, "vapn", rows[0].flavor);
					res.json({"code":0});
				});
			} else if ( rows[0] && rows[0].token ) {
				push.send(rows[0].token, data, "gcm");
				res.json({"code":0});
			} else if ( rows[0] && rows[0].num ) {	// number has no gcmid
				res.status(412).json({"code":0});
			} else res.status(404).json({"code":common.USER_NOT_FOUND,"details":"user not found", "description":"Unknown User"});
		});
	}
}

function dialog(req, res) {
	var fields = req.fields;
//	var lang = ( fields.lang ) ? fields.lang : "eng";
	var lang = "eng";
	var cat = ( fields.cat ) ? fields.cat : "general";
	var q = "";
	if ( fields.package == "1" ) q = "SELECT groupName title,description msg FROM groupName WHERE sku=" + db.escape(fields.id);
	else q = "SELECT title,msg FROM msg_dialog WHERE lang=" + db.escape(lang) + " AND sid=" + db.escape(fields.id);
	db.query(res, q, function(rows) { 
		if ( rows[0] ) res.json({"code":0,"title":rows[0].title,"msg":rows[0].msg});
		else res.status(404).json({"code":common.USER_NOT_FOUND,"details":"ID not found","description":"Unknown ID"});;
	});
}

function report(req, res) {
	if ( req.query.msisdn && req.query.to ) {
		var q = "INSERT INTO sms_logs (ts, msisdn, sender, networkcode, messageId, price, status, scts, errcode) VALUES (" + db.escape(req.query["message-timestamp"]) + "," + db.escape(req.query.msisdn) + "," + db.escape(req.query.to) + "," + db.escape(req.query["network-code"]) + "," + db.escape(req.query.messageId) + "," + db.escape(req.query.price) + "," + db.escape(req.query.status) + "," + db.escape(req.query.scts) + "," + db.escape(req.query["err-code"]) + ")";
		db.query_noerr(q, function() {});
		res.json({"code":0});
	} else res.json({"code":400});
}
