var db = require(__lib + '/db_handler');
var common = require(__lib + '/common');
var config = require(__base + '/config');
var rates = require('./rates');

var checkSessionEnabled = true;
var postFieldsLimit = 20;
var logsEnabled = config.LOGSENABLED;

//exports

exports.init = init;

//functions

function init(baseApi, category, web, webs) {
    var path = baseApi + category;

    var usagePath = path + '/get';
    webs.get(usagePath + '/countries/list', rates.countriesGet);        // do not check session

    //Parse params
    web.post(path + '/*', parsePost);

    //Check session
    web.post(path + '/*/:user_key/:signature', sessionCheckPost);
    web.get(path + '/*/:user_key/:signature', sessionCheckGet);
    webs.get(path + '/*/:user_key/:signature', sessionCheckGet);

    // plan - ID Get API
    var usagePath = path + '/get';
    web.post(usagePath + '/id/:user_key/:signature', idGet);

    // plan - List Get API
    var usagePath = path + '/get';
    web.get(usagePath + '/list/:user_key/:signature', listGet);
    webs.get(usagePath + '/rates/:iso/:user_key/:signature', rates.isoGet);
}

function parsePost(req, res, next) {
    common.formParse(req, res, postFieldsLimit, function(fields) {
        req.fields = fields;
        next();
    });
}

function sessionCheckPost(req, res, next) {
    if (checkSessionEnabled) {
        db.sessionCheckPost(req, res, req.fields, function(req, res) {
            next();
        });
    } else {
        next();
    }
}

function sessionCheckGet(req, res, next) {
    if (checkSessionEnabled) {
        db.sessionCheckGet(req, res, function(req, res) {
            next();
        });
    } else {
        next();
    }
}

function listGet(req, res) {
    db.sessionCheck(req, res, common.sortMd5(req.query), function() {
        var q = "";
        if ( req.params.user_key == "null" ) {
            q = "SELECT gn.*, gn.price localPrice, '$', 'USD' FROM groupName gn WHERE gn.status='A' AND gn.platform='android'";
        } else {
            q = "SELECT (SELECT IF(MAX(voided) IS NULL, 0, MAX(voided)) FROM android_inapp WHERE device_id=d.device_id) blocked, " +
                " m.iso, gn.*, gn.price*cu.vsUSD localPrice, cu.currencySymbol, cu.currencyCode FROM msisdn m " +
                " LEFT JOIN groupName gn ON m.group_plan_allow = 0 OR FIND_IN_SET(gn.groupID,m.group_plan_allow) " +
                " LEFT JOIN countries co ON m.iso=co.alpha2code " +
                " LEFT JOIN currencies cu ON co.currency = cu.currencyCode, app a, device d WHERE " +
                " d.app_id=a.id AND a.number=m.number AND d.user_key=" + db.escape(req.params.user_key) +
                " AND d.status='A' AND gn.status='A'";
        }
        if (logsEnabled) common.log("listGet q", q);
        db.query_noerr(q, function(rows) {
            var reply = {"code":0, "android_subscriptions":[], "android_topups":[], "ios_subscriptions":[], "ios_topups":[]};
            for ( var i=0;i<rows.length;i++ ) {
                if ( !rows[i].blocked &&
                        ((rows[i].price < 4) || ((["US", "CA", "PH"]).indexOf(rows[i].iso) == -1)) ) {    // cheaters
                    var group = new Object;
                    group.group_id = rows[i].groupID;
                    group.group_name = rows[i].groupName;
                    group.sku = rows[i].sku;
                    group.qty = rows[i].qty;
                    group.period = rows[i].period;
                    group.price = rows[i].price;
                    group.symbol = "$";
                    group.currency = rows[i].currency;
                    group.local_price = rows[i].localPrice;
                    group.local_symbol = rows[i].currencySymbol;
                    group.local_currency = rows[i].currencyCode;
                    group.description = rows[i].description;
                    if ( rows[i].platform == "android" ) {
                        if ( group.qty == 0 ) reply.android_topups.push(group);
                        else reply.android_subscriptions.push(group);
                    } else if ( rows[i].platform == "ios" ) {
                        if ( group.qty == 0 ) reply.ios_topups.push(group);
                        else reply.ios_subscriptions.push(group);
                    }
                }
            }
            res.json(reply);
        });
    });
}

function idGet(req, res) {
    var fields = req.fields;
    var q = "SELECT gm.groupID, c.alpha2code FROM groupMember gm, countries c WHERE c.countryprefix=gm.prefix " +
        " AND groupID in (" + db.escape(fields.planID).slice(0,-1).slice(1) + ")";
    db.query_noerr(q, function(rows) {
        var reply = {"code":0};
        for ( var i=0;i<rows.length;i++ ) {
            if ( !reply["group_" + groupID] ) reply["group_" + groupID] = new Array;
            reply["group_" + groupID].push(rows[i].alpha2code);
        }
        res.json(json);
    });
}
