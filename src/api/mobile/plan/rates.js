var async = require('async');
var db = require(__lib + '/db_handler');
var common = require(__lib + '/common');
var config = require(__base + '/config');
var contentsRates = require(__core + '/manager/contents/rates');

var logsEnabled = config.LOGSENABLED;

//exports

exports.isoGet = isoGet;
exports.countriesGet = countriesGet;

//functions

function countriesGet(req, res) {
    var q = "SELECT * FROM countries";
    db.query_noerr(q, function (rows) {
        var list = new Array;
        if (rows) rows.forEach(function (item) {
            list.push({    "countryname" : item.countryname,
                    "countryprefix" : item.countryprefix,
                    "alpha2code" : item.alpha2code });
        });
        list.push({    "countryname" : "Unknown",
                "alpha2code" : "000" });
        res.json({ "code" : 0, "list" : list });
    });
}

function isoGet(req, res) {
    var userKey = req.params.user_key;
    var iso = (req.params.iso) ? req.params.iso.toUpperCase() : undefined;
    if (!iso) return res.status(400).json({ "code" : -1 });
    var tasks = new Array;
/* circles talk
    tasks.push(function (callback) {
            //prefixcond += " dialprefix LIKE " + db.escape(pref.countryprefix + "%") + " OR"; 
        var qPlan = "SELECT m.plan_id, p.call_providerID FROM msisdn m, app a, device d, plans p WHERE p.planID=m.plan_id AND m.number=a.number AND d.app_id=a.id AND d.status='A' AND a.status='A' AND d.user_key=" + db.escape(userKey);
        db.query_noerr(qPlan, function(plan) {
            var planID = plan[0].plan_id;
            var providers = plan[0].call_providerID;
            var qRate = "SELECT MAX(sellrate) maxrate, MIN(sellrate) minrate FROM call_rates WHERE status='A' AND providerID IN (" + providers + ") AND ( " + prefixcond.slice(0, -3) + " )";
            db.query_noerr(qRate, function(rate) {
                var minrate = rate[0].minrate;
                var maxrate = rate[0].maxrate;
                callback(undefined, {
                    "iso" : iso,
                    "minrate" : minrate,
                    "maxrate" : maxrate,
                    "prefix" : prefixes,
                    "planID" : planID,
                    "providers" : providers
                });
            });
        });
    });
*/
    if ( iso != "000" ) {
        tasks.push(function (callback) {
            contentsRates.get("idd002", undefined, iso, function(err, rates) {
                if (err) {
                    common.error("idd002 " + iso, err.message);
                    callback(undefined, { "idd002" : {} });
                } else if (rates.rates.length == 0) {
                    common.log("idd002 " + iso, "Empty");
                    callback(undefined, { "idd002" : {} });
                } else {
                    callback(undefined, { "idd002" : rates });
                }
            });
        });
        tasks.push(function (callback) {
            contentsRates.get("idd021", undefined, iso, function(err, rates) {
                if (err) {
                    common.error("idd021 " + iso, err.message);
                    callback(undefined, { "idd021" : {} });
                } else if (rates.rates.length == 0) {
                    common.log("idd021 " + iso, "Empty");
                    callback(undefined, { "idd021" : {} });
                } else {
                    callback(undefined, { "idd021" : rates });
                }
            });
        });
    }
    tasks.push(function (callback) {
        contentsRates.get("ppurroaming", undefined, iso, function(err, rates) {
            if (err) {
                common.error("ppurroaming " + iso, err.message);
                callback(undefined, { "roaming" : {} });
            } else if (rates.rates.length == 0) {
                common.log("ppurroaming " + iso, "Empty");
                callback(undefined, { "roaming" : {} });
            } else {
                callback(undefined, { "roaming" : rates });
            }
        });
    });
    async.parallel(tasks, function (err, result) {
        var reply = { "code" : 0 };
        result.forEach(function (item) {
            if (item) {
/*
                if (item.planID) {
                    reply.iso = item.iso;
                    reply.prefix = item.prefix;
                    reply.gentwo = { "title" : "Circles Talk",
                        "description" : "Circles Talk",
                        "planID" : item.planID,
                        "providers" : item.providers,
                        "list" : [ { "destination": countryname?,
                            "rates" : [ {
                                "title" : "Min",
                                "prefix" : item.prefix,
                                "price" : { "prefix": "$",
                                    "value": item.minrate,
                                    "postfix": "SGD",
                                    "unit": "minutes" } },
                                {
                                "title" : "Max",
                                "prefix" : item.prefix,
                                "price" : { "prefix": "$",
                                    "value": item.maxrate,
                                    "postfix": "SGD",
                                    "unit": "minutes" } } ] } ] };
                }
*/
                if (item.idd002 && item.idd002.header && item.idd002.tooltip && item.idd002.rates) {
                    reply.idd002 = {
                        "title" : item.idd002.header.idd002.label,
                        "description" : item.idd002.tooltip.idd002.label,
                        "list" : [ ] };
                    item.idd002.rates.forEach(function (rate) {
                        reply.idd002.list.push({
                            "destination" : rate.country,
                            "rates" : [ {
                                "title" : item.idd002.header.idd002.nonPeak.split("(")[0],
                                "prefix" : parseInt(rate.prefix),
                                "price" : {
                                    "prefix" : "$",
                                    "value" : rate.nonPeak,
                                    "postfix" : "SGD",
                                    "unit" : "minutes" }
                            }, {
                                "title" : item.idd002.header.idd002.peak.split("(")[0],
                                "prefix" : parseInt(rate.prefix),
                                "price" : {
                                    "prefix" : "$",
                                    "value" : rate.peak,
                                    "postfix" : "SGD",
                                    "unit" : "minutes" }
                            } ]
                        });
                    });
                }
                if (item.idd021 && item.idd021.header && item.idd021.tooltip && item.idd021.rates) {
                    reply.idd021 = {
                        "title" : item.idd021.header.idd021.label,
                        "description" : item.idd021.tooltip.idd021.label,
                        "list" : [ ] };
                    item.idd021.rates.forEach(function (rate) {
                        reply.idd021.list.push({
                            "destination" : rate.country,
                            "rates" : [ {
                                "title" : item.idd021.header.idd021.nonPeak.split("(")[0],
                                "prefix" : parseInt(rate.prefix),
                                "price" : {
                                    "prefix" : "$",
                                    "value" : rate.nonPeak,
                                    "postfix" : "SGD",
                                    "unit" : "minutes" }
                            }, {
                                "title" : item.idd021.header.idd021.peak.split("(")[0],
                                "prefix" : parseInt(rate.prefix),
                                "price" : {
                                    "prefix" : "$",
                                    "value" : rate.peak,
                                    "postfix" : "SGD",
                                    "unit" : "minutes" }
                            } ]
                        });
                    });
                }
                if (item.roaming && item.roaming.header && item.roaming.tooltip && item.roaming.rates) {
                    reply.roaming = {
                        "title" : item.roaming.header.ppurroaming.label,
                        "description" : item.roaming.tooltip.ppurroaming.label,
                        "list" : [ ] };
                    item.roaming.rates.forEach(function (rate) {
                        obj = {
                            "destination" : rate.country,
                            "rates" : [ ] };
                        var mccmnc = (rate.discountmccmnc) ? rate.discountmccmnc.split(",") : [ ];
                        var discountmccmnc = new Array;
                        if (mccmnc.length > 0) mccmnc.forEach(function (mccmncitem) {
                            discountmccmnc.push({
                                "mcc" : parseInt(mccmncitem.split(";")[0]),
                                "mnc" : parseInt(mccmncitem.split(";")[1])
                            });
                        });
                        obj.rates.push({
                            "title" : item.roaming.header.ppurroaming.ppurdatamms.split("(")[0],
                            "prefix" : parseInt(rate.prefix),
                            "price" : {
                                "prefix" : "$",
                                "value" : rate.ppurdatamms,
                                "postfix" : "SGD",
                                "unit" : "10 KB" }
                        });
                        if (rate.discountdata) obj.rates.push({
                            "title" : item.roaming.header.ppurroaming.discountdata.split("(")[0] + " (" + rate.discountoperator + ")",
                            "prefix" : parseInt(rate.prefix),
                            "price" : {
                                "prefix" : "$",
                                "value" : rate.discountdata,
                                "postfix" : "SGD",
                                "unit" : "100 MB" },
                            "mccmnc" : discountmccmnc,
                            "preferred" : rate.discountoperator
                        });
                        obj.rates.push({
                            "title" : item.roaming.header.ppurroaming.ppurvoicesg.split("(")[0],
                            "prefix" : parseInt(rate.prefix),
                            "price" : {
                                "prefix" : "$",
                                "value" : rate.ppurvoicesg,
                                "postfix" : "SGD",
                                "unit" : "minutes" } });
                        obj.rates.push({
                            "title" : item.roaming.header.ppurroaming.ppurvoiceintl.split("(")[0],
                            "prefix" : parseInt(rate.prefix),
                            "price" : {
                                "prefix" : "$",
                                "value" : rate.ppurvoiceintl,
                                "postfix" : "SGD",
                                "unit" : "minutes" } });
                        obj.rates.push({
                            "title" : item.roaming.header.ppurroaming.ppurvoicelocal.split("(")[0],
                            "prefix" : parseInt(rate.prefix),
                            "price" : {
                                "prefix" : "$",
                                "value" : rate.ppurvoicelocal,
                                "postfix" : "SGD",
                                "unit" : "minutes" } });
                        obj.rates.push({
                            "title" : item.roaming.header.ppurroaming.ppurvoiceincoming.split("(")[0],
                            "prefix" : parseInt(rate.prefix),
                            "price" : {
                                "prefix" : "$",
                                "value" : rate.ppurvoiceincoming,
                                "postfix" : "SGD",
                                "unit" : "minutes" } });
                        obj.rates.push({
                            "title" : item.roaming.header.ppurroaming.ppursmssend,
                            "prefix" : parseInt(rate.prefix),
                            "price" : {
                                "prefix": "$",
                                "value" : rate.ppursmssend,
                                "postfix" : "SGD",
                                "unit" : "sms" } });
                        reply.roaming.list.push(obj);
                    });
                } 
            }
        });
        res.json(reply);
    });
}
