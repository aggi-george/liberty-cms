var db = require(__lib + '/db_handler');
var common = require(__lib + '/common');
var config = require(__base + '/config');

var logsEnabled = config.LOGSENABLED;

//exports

exports.credit_get = credit_get;

//functions

function credit_get(req, res) {
	var fields = req.fields;
	if ( fields.pass ) {
		var q = "SELECT IF(m.credit<0,0,m.credit) credit, IF(m.credit*cu.vsUSD<0,0,m.credit*cu.vsUSD) local_credit, cu.currencySymbol, cu.currencyCode FROM msisdn m LEFT JOIN countries co ON m.iso=co.alpha2code LEFT JOIN currencies cu ON co.currency=cu.currencyCode, app a, device d WHERE d.app_id=a.id AND a.number=m.number AND d.user_key=" + db.escape(req.params.user_key) + " AND d.secret=MD5(CONCAT(m.number,':'," + db.escape(config.REALM) + ",':'," + db.escape(fields.pass) + ")) AND d.status='A' AND a.status='A' AND m.status='A'";
		if (logsEnabled) common.log("credit_get q", q);
		db.query(res, q, function(rows) { 
			if ( rows && rows[0] && rows[0].total != 0 ) res.json({"code":0,"credit":rows[0].credit,"symbol":"$","currency":"USD","local_credit":rows[0].local_credit,"local_symbol":rows[0].currencySymbol,"local_currency":rows[0].currencyCode});
			else res.status(403).json({"code":common.USER_INVALID,"details":"password error","description":"Credentials are Incorrect"});
		});
	} else res.status(403).json({"code":common.EMPTY_PASSWORD,"details":"Password Empty","description":"Password Empty"});
}
