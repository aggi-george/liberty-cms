var request = require('request');
var config = require(__base + '/config');
var db = require(__lib + '/db_handler');
var common = require(__lib + '/common');
var ec = require(__lib + '/elitecore');

//exports

exports.setDetail = setDetail;
exports.getDetails = getDetails;
exports.updateProfileCMS = updateProfileCMS;

//functions

function setDetail(req, res) {
    var fields = req.fields;
    if (!fields.uuid) {
        fields.uuid = "";
    }

    var updateContactBook = function (number) {
        db.contacts.update({ "contacts.nums" : new RegExp(number, "i"), "number" : { "$ne" : number } },
            { "$inc" : { "version" : 1 }, "$set" : { "updated" : new Date().getTime() } },
            { "multi" : true });
            //need to send profile update notification
    }

    var saveLocalDetails = function (callback) {
        if (fields.first_name || fields.last_name || fields.nickname) {

            var args = "";
            if (fields.first_name) args += "m.firstname=" + db.escape(fields.first_name) + ",";
            if (fields.last_name) args += "m.lastname=" + db.escape(fields.last_name) + ",";
            if (fields.nickname) args += "m.nickname=" + db.escape(fields.nickname) + ",";
            args = args.slice(0, -1);

            var q_select = "SELECT a.number FROM app a, device d WHERE d.app_id=a.id AND d.user_key=" + db.escape(req.params.user_key);
            db.query_noerr(q_select, function (rows) {
                if (!rows || !rows[0]) {
                    if (callback) callback();
                    return;
                }
                var q_update = "UPDATE msisdn m SET " + args +
                    " WHERE m.number=" + db.escape(rows[0].number);

                db.query_noerr(q_update, function () {
                    if (callback) callback();
                    updateContactBook(rows[0].number);
                });
            });
        } else {
            if (callback) callback();
        }
    }

    db.uuidCheck(res, req.params.user_key + "_" + fields.uuid, function () {

        var sendResponse = function (err) {
            var reply;
            if (err) {
                reply = {code: -1, details: err.message};
            } else {
                reply = {code: 0};
            }

            db.cache_put("uuid_cache", req.params.user_key + "_" + fields.uuid, reply, 120000);
            res.json(reply);
        };

        ec.getCache(req.params.user_key, false, function (err, cache) {
            if (err || !cache) {
                saveLocalDetails(sendResponse);
            } else {
                updateProfileCMS(cache.prefix, cache.number, fields.first_name,
                    fields.last_name, fields.nickname, undefined, function (err) {
                        saveLocalDetails(sendResponse);
                    });
            }
        });
    });
}

function getDetails(req, res) {

    var loadLocalDetails = function (callback) {
        var query = "SELECT m.firstname, m.lastname, m.app_picture FROM msisdn m, app a, device d " +
            " WHERE d.app_id=a.id AND a.number=m.number AND d.user_key=" +
            db.escape(req.params.user_key) + " LIMIT 1";

        db.query(res, query, function (rows) {
            res.json({
                code: 0,
                first_name: rows[0].firstname,
                last_name: rows[0].lastname,
                app_picture: rows[0].app_picture
            });
        });
    };

    ec.getCache(req.params.user_key, false, function (err, cache) {
        if (err || !cache) {
            loadLocalDetails();
        } else {
            loadProfileCMS(cache.prefix, cache.number, function (err, details) {
                if (err) {
                    return res.json({
                        code: -1,
                        details: err.message
                    });
                }

                res.json({
                    code: 0,
                    first_name: details.firstname,
                    last_name: details.lastname,
                    nickname: details.nickname,
                    app_picture: details.app_picture
                });
            });
        }
    });
}

function updateProfileCMS(prefix, number, firstname, lastname, nickname, picture, callback) {
    var key = config.NOTIFICATION_KEY;
    var url = "http://" + config.CMSHOST + ":" + config.CMSPORT + "/api/1/profile/set/" + key;

    request({
        uri: url,
        method: 'PUT',
        timeout: 20000,
        json: {
            prefix: prefix,
            number: number,
            firstname: firstname,
            lastname: lastname,
            nickname: nickname,
            app_picture: picture
        }
    }, function (error, response, body) {
        if (error) {
            common.error("updateProfileCMS", JSON.stringify(error) + " " + body)
            callback(error);
        } else {
            var reply = common.safeParse(body);
            if (reply.code == 0) {
                callback(undefined);
            } else {
                var error = new Error("Invalid profile information");
                error.status = body.status;
                callback(error);
            }
        }
    });
}

function loadProfileCMS(prefix, number, callback) {
    var key = config.NOTIFICATION_KEY;
    var url = "http://" + config.CMSHOST + ":" + config.CMSPORT + "/api/1/profile/get/" + prefix + "/" + number + "/" + key;

    request({
        uri: url,
        method: 'GET',
        timeout: 20000,
    }, function (error, response, body) {
        if (error) {
            common.error("loadProfileCMS", JSON.stringify(error) + " " + body)
            callback(error);
        } else {
            var reply = common.safeParse(body);
            if (reply.code == 0 && reply.details) {
                callback(undefined, reply.details);
            } else {
                var error = new Error("Invalid profile information");
                error.status = body.status;
                callback(error);
            }
        }
    });
}

