var db = require(__lib + '/db_handler');
var common = require(__lib + '/common');
var randomManager = require('../../../utils/randomManager');
var ec = require(__lib + '/elitecore');
var details = require('./details');
var fs = require("fs");
var mime = require("mime");
var md5 = require("MD5");
var config = require(__base + '/config');

var LOG = config.LOGSENABLED;

//exports

exports.setImage = setImage;
exports.deleteImage = deleteImage;
exports.getImage = getImage;
exports.setDoc = setDoc;
exports.getDoc = getDoc;

//functions

function setDoc(req, res) {
    setFile("doc", req, res);
}

function setImage(req, res) {
    setFile("image", req, res);
}

function setFile(type, req, res) {
    var files = req.files;
    var fileType = req.params.fileType;
    var filename = md5(files.content.name + "_" + new Date().getTime()
    + randomManager.randomPredefinedString(10));
    var tmpPath = files.content.path;

    if (LOG) common.log("FileManager", "setFile: filename=" + filename
    + ", type=" + type + ", tmpPath=" + tmpPath + ", mime=" + mime.lookup(tmpPath))

    var sendResponse = function (err) {
        var reply;
        if (err) {
            reply = {code: -1, details: err.message};
        } else {
            reply = {code: 0, file_id: filename};
        }
        if (!res.headersSent) res.json(reply);
    };

    fs.readFile(tmpPath, function (err, data) {
        if (err) {
            return sendResponse(err);
        }

        if (type === "doc") {
            ec.getCache(req.params.user_key, false, function (err, cache) {
                if (err) {
                    return sendResponse(err);
                }

                if (!cache) {
                    return sendResponse(new Error("Failed to load the customer"));
                }

                db.profile_doc_files_set({
                    fileType: fileType,
                    serviceInstanceNumber: cache.serviceInstanceNumber,
                    filename: filename,
                    mime: mime.lookup(tmpPath),
                    file: data
                }, function () {

                    sendResponse();
                    fs.unlink(tmpPath, function (err) {
                        //do nothing
                    });
                });
            });
        } else {
            if (LOG) common.log("ImageManager", "file upload status code = " + res.statusCode);
            if (parseInt(res.statusCode) > 200) {
                common.error("File upload error", "code = " + res.statusCode);
                return false;
            }

            var updateContactBook = function (number) {
                db.contacts.update({ "contacts.nums" : new RegExp(number, "i"), "number" : { "$ne" : number } },
                    { "$inc" : { "version" : 1 }, "$set" : { "updated" : new Date().getTime() } },
                    { "multi" : true });
            }

            var saveLocalDetails = function (callback) {
                var query = "SELECT a.number FROM app a, device d WHERE d.app_id=a.id AND d.user_key=" +
                    db.escape(req.params.user_key);

                db.query_noerr(query, function (rows) {
                    if (rows && rows[0] && rows[0].number) {
                        var query = "UPDATE msisdn m SET m.app_picture=" + db.escape(filename) + " WHERE " +
                            " number=" + db.escape(rows[0].number);

                        db.query_noerr(query, function () {
                            if (callback) callback();
                            updateContactBook(rows[0].number);
                        });
                        db.profile_pic_set({
                            number: rows[0].number,
                            filename: filename,
                            mime: mime.lookup(tmpPath),
                            file: data
                        }, function () {
                            fs.unlink(tmpPath, function (err) {
                                //do nothing
                            });
                        });
                    } else {
                        fs.unlink(tmpPath, function (err) {
                            if (callback) callback();
                        });
                    }
                });
            }

            ec.getCache(req.params.user_key, false, function (err, cache) {
                if (err || !cache) {
                    saveLocalDetails(sendResponse);
                } else {
                    details.updateProfileCMS(cache.prefix, cache.number, undefined,
                        undefined, undefined, filename, function (err) {
                            saveLocalDetails(sendResponse);
                        });
                }
            });
        }
    });
}

function deleteImage(req, res) {
    var fields = req.fields;
    if (!fields.uuid) {
        fields.uuid = "";
    }

    db.uuidCheck(res, req.params.user_key + "_" + fields.uuid, function () {
        var updateContactBook = function (number) {
            db.contacts.update({ "contacts.nums" : new RegExp(number, "i"), "number" : { "$ne" : number } },
            { "$inc" : { "version" : 1 }, "$set" : { "updated" : new Date().getTime() } },
            { "multi" : true });
            //need to send profile update notification
        }

        var saveLocalDetails = function (callback) {
            var querySelect = "SELECT a.number FROM app a, device d WHERE d.app_id=a.id " +
                " AND d.user_key = " + db.escape(req.params.user_key);
            db.query_noerr(querySelect, function (rows) {
                var number = "";
                if (rows && rows[0] && rows[0].number) {
                    db.profile_pic_del({"number": rows[0].number});
                    number = rows[0].number;

                    var queryUpdated = "UPDATE msisdn m, app a, device d SET m.app_picture='' " +
                        " WHERE d.app_id=a.id AND a.number=m.number AND d.user_key=" + db.escape(req.params.user_key);
                    db.query_noerr(queryUpdated, function (rows) {
                        if (callback) callback();
                        updateContactBook(number)
                    });
                } else {
                    if (callback) callback();
                }
            });
        }

        var sendResponse = function (err, filename) {
            var reply;
            if (err) {
                reply = {code: -1, details: err.message};
            } else {
                reply = {code: 0};
            }
            res.json(reply);
            db.cache_put("uuid_cache", req.params.user_key + "_" + fields.uuid, reply, 120000);
        };

        ec.getCache(req.params.user_key, false, function (err, cache) {
            if (err || !cache) {
                saveLocalDetails(sendResponse);
            } else {
                details.updateProfileCMS(cache.prefix, cache.number, undefined,
                    undefined, undefined, '', function (err) {
                        saveLocalDetails(sendResponse);
                    });
            }
        });
    });
}

function getDoc(req, res) {
    if (req.params.id) {
        db.profile_doc_files_get(req.params.id, function (err, item) {
            if (item) {
                res.set('Content-Type', item.mime);
                res.send(item.file.buffer);
            } else {
                res.status(404).send('Not Found');
            }
        });
    } else {
        res.status(404).send('Not Found');
    }
}

function getImage(req, res) {
    if (req.params.id) {
        db.profile_pic_get({"filename": req.params.id}, function (item) {
            if (item) {
                res.set('Content-Type', item.mime);
                res.send(item.file.buffer);
            } else {
                res.status(404).send('Not Found');
            }
        });
    } else {
        res.status(404).send('Not Found');
    }
}

