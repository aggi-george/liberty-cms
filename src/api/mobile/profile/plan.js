var config = require(__base + '/config');
var db = require(__lib + '/db_handler');
var common = require(__lib + '/common');
var inappManager = require(__core + '/manager/inapp/inappManager');
var async = require('async');

var logsEnabled = config.LOGSENABLED;

//exports

exports.plan_get = plan_get;

//functions

function plan_get(req, res) {
	var fields = req.fields;
	if ( fields.pass ) {
		var q = "SELECT s.number, cu.currencySymbol, cu.currencyCode, cs.orderID, cs.purchase_token, cs.group_plan_expire, cs.autorenew, gn.*, gn.price*cu.vsUSD localPrice FROM (SELECT a.number, m.iso FROM msisdn m, app a, device d WHERE d.app_id=a.id AND a.number=m.number AND d.user_key=" + db.escape(req.params.user_key) + " AND d.secret=MD5(CONCAT(a.number,':'," + db.escape(config.REALM) + ",':'," + db.escape(fields.pass) + ")) AND d.status='A' AND a.status='A' AND m.status='A') s LEFT JOIN customer_subscription cs ON cs.number=s.number LEFT JOIN countries co ON s.iso=co.alpha2code LEFT JOIN currencies cu ON co.currency = cu.currencyCode LEFT JOIN groupName gn ON cs.group_plan_expire >= NOW() AND gn.platform='android' AND gn.groupID = cs.group_plan ORDER BY gn.groupID ASC";
		if (logsEnabled) common.log("plan_get q",q);
		db.query(res, q, function(rows) {
			var reply = {"code":0, "android_subscriptions":[], "ios_subscriptions":[], "circles_subscriptions":[]};
			if ( rows && rows[0] && rows[0].number == "6586164389" ) {
				reply.circles_subscriptions[0] = {	"id" : "free_circles_calls",
									"price" : { "prefix" : "$", "value" : 0.00, "postfix" : "USD"},
									"priceLocal" : { "prefix" : "$", "value" : 0.00, "postfix" : "SGD"},
									"expire" : "2015-12-30",
									"description" : "Free app-app calls to other Circles users. (Free calls when using the app to call other Circles users)",
									"title" : "Free Circles Calls" };
				reply.circles_subscriptions[1] = {	"id" : "free_sg_calls",
									"price" : { "prefix" : "$", "value" : 5/1.26, "postfix" : "USD"},
									"priceLocal" : { "prefix" : "$", "value" : 5.00, "postfix" : "SGD"},
									"expire" : "2015-12-30",
									"description" : "Free calls to Singapore. (Free calls when using the app to call any number in Singapore)",
									"title" : "Free SG Calls" };
			}
			var tasks = new Array
			rows.forEach(function (row) {
				if ( row.groupName ) { // ! = no subscriptions
					var group = new Object;
					group.group_id = row.groupID;
					group.group_name = row.groupName;
					group.sku = row.sku;
					group.qty = row.qty;
					group.period = row.period;
					group.price = row.price;
					group.symbol = "$";
					group.currency = row.currency;
					group.local_price = row.localPrice;
					group.local_symbol = row.currencySymbol;
					group.local_currency = row.currencyCode;
					group.status = row.status;
					group.expire = row.group_plan_expire;
					if ( fields.lookup == "1" ) {
						group.autorenew = undefined;
						tasks.push(function (callback) {	
							inappManager.google.validate(row.orderID, row.purchase_token, group.sku, false, function(err, response) {
								var autoRenewing = ( !err && response && response.result && response.result.receipt &&
									(response.result.receipt.kind == "androidpublisher#subscriptionPurchase") ) ? response.result.receipt.autoRenewing : undefined;
								callback(undefined, { "group_id" : row.groupID, "autorenew" : autoRenewing });
							});
						});
					} else {
						group.autorenew = ( row.autorenew == "1" ) ? true : false;
					}
					group.description = row.description;
					reply.android_subscriptions.push(group);
				}
			});
			async.parallel(tasks, function(err, asyncResult) {
				asyncResult.forEach(function (item) {
					if (item) reply.android_subscriptions.forEach(function (group) {
						if ( item.group_id == group.group_id ) group.autorenew = item.autorenew;
					});
				});
				res.json(reply); 
			});
		});
	} else res.status(403).json({"code":common.EMPTY_PASSWORD,"details":"Password Empty","description":"Password Empty"});
}
