var sessionManager = require('../../base/sessionManager');

var image = require('./image');
var details = require('./details');
var status = require('./status');
var credit = require('./credit');
var plan = require('./plan');

// stubs

var detailsStub = require('./stub/details_stub_data1');
var imageStub = require('./stub/image_stub_data1');

//exports

exports.init = init;

//functions

function init(baseApi, category, web, webs) {
    var path = baseApi + category;

    /**
     * File upload methods
     */

    // Profile - Image Set API

    var imagePath = path + '/image';
    var docPath = path + '/doc';

    webs.post(imagePath + '/set/:user_key/:signature', sessionManager.parsePostFiles);
    webs.post(imagePath + '/set/:user_key/:signature', sessionManager.sessionCheckFiles);
    webs.post(imagePath + '/set/:user_key/:signature', image.setImage);
    webs.post(docPath + '/:fileType/set/:user_key/:signature', sessionManager.parsePostFiles);
    webs.post(docPath + '/:fileType/set/:user_key/:signature', sessionManager.sessionCheckFiles);
    webs.post(docPath + '/:fileType/set/:user_key/:signature', function (req, res) {
        sessionManager.executeMethod(req, res, image, imageStub, "setDoc");
    });

    // DEPRECATED
    web.post(imagePath + '/set/:user_key/:signature', sessionManager.parsePostFiles);
    web.post(imagePath + '/set/:user_key/:signature', sessionManager.sessionCheckFiles);
    web.post(imagePath + '/set/:user_key/:signature', image.setImage);
    web.post(docPath + '/:fileType/set/:user_key/:signature', sessionManager.parsePostFiles);
    web.post(docPath + '/:fileType/set/:user_key/:signature', sessionManager.sessionCheckFiles);
    web.post(docPath + '/:fileType/set/:user_key/:signature', function (req, res) {
        sessionManager.executeMethod(req, res, image, imageStub, "setDoc");
    });
    // DEPRECATED

    // Parse params

    webs.post(path + '/*', sessionManager.parsePost);
    web.post(path + '/*', sessionManager.parsePost);

    /**
     * No Signature methods
     */

    // Profile - Credit API
    var creditsPath = path + '/credit';

    webs.post(creditsPath + '/get/:user_key', credit.credit_get);

    // Profile - Plan API
    var planPath = path + '/Plan';

    webs.post(planPath + '/get/:user_key', plan.plan_get);

    webs.get(imagePath + '/get/:id', function (req, res) {
        // method does not pass session on iOS, disabled temporary
        image.getImage(req, res);
    });
    webs.get(docPath + '/get/:id', function (req, res) {
        // method does not pass session on iOS, disabled temporary
        image.getDoc(req, res);
    });

    // DEPRECATED
    web.get(imagePath + '/get/:id', function (req, res) {
        // method does not pass session on iOS, disabled temporary
        image.getImage(req, res);
    });
    web.get(docPath + '/get/:id', function (req, res) {
        // method does not pass session on iOS, disabled temporary
        image.getDoc(req, res);
    });
    // DEPRECATED

    /**
     * Check signature in methods below
     */

    webs.post(path + '/*/:user_key/:signature', sessionManager.sessionCheckPost);
    web.post(path + '/*/:user_key/:signature', sessionManager.sessionCheckPost);
    webs.get(path + '/*/:user_key/:signature', sessionManager.sessionCheckGet);
    web.get(path + '/*/:user_key/:signature', sessionManager.sessionCheckGet);

    // Profile - Image API
    var imagePath = path + '/image';
    var docPath = path + '/doc';

    webs.post(imagePath + '/del/:user_key/:signature', function (req, res) {
        sessionManager.executeMethod(req, res, image, undefined, "deleteImage");
    });


    // DEPRECATED
    web.post(imagePath + '/del/:user_key/:signature', function (req, res) {
        sessionManager.executeMethod(req, res, image, undefined, "deleteImage");
    });
    // DEPRECATED


    // Profile - Details API
    var detailsPath = path + '/details';

    webs.post(detailsPath + '/set/:user_key/:signature', function (req, res) {
        sessionManager.executeMethod(req, res, details, detailsStub, "setDetail");
    });
    webs.get(detailsPath + '/get/:user_key/:signature', function (req, res) {
        sessionManager.executeMethod(req, res, details, detailsStub, "getDetails");
    });


    // DEPRECATED
    web.post(detailsPath + '/set/:user_key/:signature', function (req, res) {
        sessionManager.executeMethod(req, res, details, detailsStub, "setDetail");
    });
    web.get(detailsPath + '/get/:user_key/:signature', function (req, res) {
        sessionManager.executeMethod(req, res, details, detailsStub, "getDetails");
    });
    // DEPRECATED


    // Profile - Status API
    var statusPath = path + '/status';

    web.post(statusPath + '/set/:user_key/:signature', function (req, res) {
        sessionManager.executeMethod(req, res, status, undefined, "status_set");
    });
}
