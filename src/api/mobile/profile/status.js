var config = require(__base + '/config');
var db = require(__lib + '/db_handler');
var common = require(__lib + '/common');

var log = config.LOGSENABLED;

//exports

exports.status_set = status_set;

//functions

function status_set(req, res) {
    var fields = req.fields;
    if (!fields.uuid) {
        fields.uuid = "";
    }

    db.uuidCheck(res, req.params.user_key + "_" + fields.uuid, function () {
        if (fields.status) {
            var q = "UPDATE device d, app a SET a.app_lastseen=NOW(), a.app_status=" + db.escape(fields.status)
                + ", d.longitude=" + db.escape(fields.lng) + ", d.latitude=" + db.escape(fields.lat)
                + ", d.accuracy=" + db.escape(fields.acc) + " WHERE d.app_id=a.id AND d.user_key=" + db.escape(req.params.user_key);

            if (log) common.log("status_set", "query: " + q);
            db.query(res, q, function (rows) {
                var reply = {"code": 0};
                res.json(reply);

                if (fields.uuid != "") {
                    db.cache_put("uuid_cache", req.params.user_key + "_" + fields.uuid, reply, 120000);
                }
            });
        } else {
            res.status(400).json({"code": common.PARSE_ERROR, "details": "Status Empty", "description": "Staus Empty"});
        }
    });
}
