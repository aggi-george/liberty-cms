
//exports

exports.setDetail = setDetail;
exports.getDetails = getDetails;

//functions

function setDetail(req, res) {
    res.json({code: 0});
}

function getDetails(req, res) {
    res.json({
        code: 0,
        first_name: "John",
        last_name: "Woo",
        nickname: "Johny",
        app_picture: "default2"
    });
}

