//var ami = require('asterisk-manager')('5038','120.50.43.188','aster1skus3r','*star123', true);
var db = require(__lib + '/db_handler');
var common = require(__lib + '/common');
var config = require(__base + '/config');

var channels = new Array;
var counter = 0;

var checkSessionEnabled = true;
var postFieldsLimit = 20;

//ami.keepConnected();

//exports

exports.init = init;

//functions

function init(baseApi, category, web, webs) {
	var path = baseApi + category;

	//qos - Limits API
	var usagePath = path + '/limits';
	web.get(usagePath + '/:user_key/:signature', limits);

	//qos - Get API
	var usagePath = path + '/get';
	web.get(usagePath + '/:user_key/:signature', get);
}

function limits(req, res) {
	db.cache_get("cache", "rssi_wifi", function(rssi_wifi) { db.cache_get("cache", "rssi_mobile", function(rssi_mobile) { db.cache_get("cache", "max_latency", function(max_latency) {
		res.json({"rssi_wifi":rssi_wifi, "rssi_mobile":rssi_mobile, "latency":max_latency});
	}); }); });
}

function get(req, res) {
	var index = getChannelIndex("user_key", req.params.user_key, -1);
	if ( index < 0 ) {
		res.json({"mos":0.00, "tx":0.00, "rx":0.00, "jitter":0, "rtt":0.00 , "status":"disconnected"});
	}
	else {
		res.json({"mos":channels[index].qos.mos, "tx":channels[index].qos.tx, "rx":channels[index].qos.rx, "jitter":channels[index].qos.jitter, "rtt":channels[index].qos.rtt, "status":"connected"});
	}
}

function mosCompute(loss) {	// return mos given loss in ratio
	var mos = 4 - (0.7 * Math.log(loss)) - (0.1 * Math.log(20));
	if ( mos > 5 ) mos = 5; 
	if ( mos < 0 ) mos = 0;
	return mos;
}

function splitQOS(qosStr) {
	var sArr = qosStr.split(';');
	for ( var i=0; i<sArr.length; i++ ) {
		var aVal = sArr[i].split('=');
		var rxjitter = ( aVal[0] == "rxjitter" ) ? aVal[1] : rxjitter;
		var txjitter = ( aVal[0] == "txjitter" ) ? aVal[1] : txjitter;
		var lp = ( aVal[0] == "lp" ) ? aVal[1] : lp;
		var rlp = ( aVal[0] == "rlp" ) ? aVal[1] : rlp;
		var txcount = ( aVal[0] == "txcount" ) ? aVal[1] : txcount;
		var rxcount = ( aVal[0] == "rxcount" ) ? aVal[1] : rxcount;
		var rtt = ( aVal[0] == "rtt" ) ? aVal[1] : rtt;
	}
	qos = new Object;
	qos.rtt = ( Math.floor(rtt) > 4200 ) ? rtt - Math.floor(rtt) : rtt;	// fix for bogus asterisk RTT
	qos.tx = ( rlp / txcount ) * 100;
	qos.rx = ( lp / rxcount ) * 100;
	qos.jitter = ( txjitter < 65536 ) ? txjitter : qos.jitter;	// opus sampling 16000 will yield ms
	qos.mos = mosCompute(Math.max(qos.tx,qos.rx));
	if ( isNaN(qos.mos) ) qos.mos = 0;

	qos.rlp = rlp;
	qos.txcount = txcount;
	qos.lp = lp;
	qos.rxcount = rxcount;

	return qos;
}
function setUser_key(num, index) {
	var q = "SELECT d.user_key FROM device d, app a WHERE d.app_id=a.id AND a.app_type='gentwo' AND a.status='A' AND d.status='A' AND a.number=" + db.escape(num);
	db.query_noerr(q, function(rows) {
		if ( rows[0] && rows[0].uniquecode ) {
			if ( channels[index] ) channels[index].user_key = rows[0].uniquecode;
		}
	}); 
}

function getChannelIndex(attrib, value, exclude) {
	for ( var i=0; i<channels.length; i++ ) {
		if ( exclude >= 0 && i == exclude ) continue;
		if ( channels[i] && channels[i][attrib] == value ) return i;
	}
	return -1;
}

/*
setInterval(function () { 
	ami.action({ 'action':'CoreShowChannels' }); 
	counter++;
	for ( var i=0; i<channels.length; i++ ) {
		if ( channels[i] && counter > channels[i].counter + 5 ) delete channels[i];	// 5 seconds of no activity, delete (hangup)
	}
}, 1000);

ami.on('coreshowchannel', function(evt) {
	var chanIndex = getChannelIndex("channel",evt.channel, -1);
	if ( chanIndex < 0 ) {
		channels.push(evt);
		chanIndex = channels.length - 1;
	}
	if ( !channels[chanIndex].bridgeid ) channels[chanIndex].bridgeid = evt.bridgeid;
	if ( !channels[chanIndex].user_key ) setUser_key(evt.calleridnum, chanIndex);
	if ( !channels[chanIndex].link ) {
		var linkIndex = getChannelIndex("bridgeid", evt.bridgeid, chanIndex);
		if ( linkIndex >= 0 ) channels[chanIndex].link = channels[linkIndex].channel;
	}
	if ( !channels[chanIndex].sipcallid ) ami.action({
		'action':'getvar',
		'channel':evt.channel,		// use AST channel ID to get SIP callID
		'variable':'SIPCALLID'}, function(err, res) {
			if ( channels[chanIndex] ) channels[chanIndex].sipcallid = res.value;
	});
	ami.action({
		'action':'getvar',
		'channel':evt.channel,		// use AST channel ID to get RTT
		'variable':'CHANNEL(rtpqos,audio,all)'}, function(err, res) {
			if ( res.value ) {
				if ( !channels[chanIndex] ) return false;
				var old_rlp = ( channels[chanIndex].qos ) ? channels[chanIndex].qos.rlp : 0;
				var old_txcount = ( channels[chanIndex].qos ) ? channels[chanIndex].qos.txcount : 0;
				var old_lp = ( channels[chanIndex].qos ) ? channels[chanIndex].qos.lp : 0;
				var old_rxcount = ( channels[chanIndex].qos ) ? channels[chanIndex].qos.rxcount : 0;

				channels[chanIndex].qos = splitQOS(res.value);

				if ( channels[chanIndex].calleridnum ) {
					var i_rlp = channels[chanIndex].qos.rlp - old_rlp;
					var i_txcount = channels[chanIndex].qos.txcount - old_txcount;
					var i_lp = channels[chanIndex].qos.lp - old_lp;
					var i_rxcount = channels[chanIndex].qos.rxcount - old_rxcount;
					channels[chanIndex].qos.itx = i_txcount == 0 ? 0 : ( i_rlp / i_txcount ) * 100;
					channels[chanIndex].qos.irx = i_rxcount == 0 ? 0 : ( i_lp / i_rxcount ) * 100;
					channels[chanIndex].qos.imos = mosCompute(Math.max(channels[chanIndex].qos.itx,channels[chanIndex].qos.irx));
					if ( isNaN(channels[chanIndex].qos.imos) ) channels[chanIndex].qos.imos = 0;

					var kam_qos = new Object;
					kam_qos.status = "connected";
					kam_qos.jitter = channels[chanIndex].qos.jitter;

					kam_qos.mos = channels[chanIndex].qos.imos;
					kam_qos.rx = channels[chanIndex].qos.irx;
					kam_qos.tx = channels[chanIndex].qos.itx;
					kam_qos.rtt = channels[chanIndex].qos.rtt;
//					var client = new rpc.Client({host:'128.199.167.163',port:6055,path:'/RPC',strict:true});
//					client.call({jsonrpc:'2.0',method:'htable.sets',params:[ 'qos', '_' + channels[chanIndex].calleridnum, "\'" + JSON.stringify(kam_qos) + "\'" ],id:chanIndex}, function (err, result) {
//						if ( err ) c.log("jsonrpc","set qos htable error " + err);
//					});
				}
			}
	});
	channels[chanIndex].counter = counter;
});

function dbDelay(sipcallid, mos) {
        setTimeout(function() {
		db.updateMOS(sipcallid, mos);
        }, 5000);       // delay 5secs to make sure already in Calls table and not in kam_acc (if BYE timeout > delay, mos won't be updated)
}

ami.on('hangup', function(evt) {
	var myIndex = getChannelIndex("channel", evt.channel, -1);
	var otherIndex = -1;
	if ( myIndex >= 0 ) { 
		if ( channels[myIndex].link ) otherIndex = getChannelIndex("channel", channels[myIndex].link, -1);
		if ( (otherIndex < 0) || 
			((evt.channel.slice(0,7) != "SIP/gen") && (otherIndex >= 0)) ) {
			var mos = 0;
			if ( (otherIndex >= 0) && channels[otherIndex] && channels[otherIndex].qos ) {
				if ( !channels[myIndex].qos )
					mos = channels[otherIndex].qos.mos;
				else
					mos = Math.min(channels[myIndex].qos.mos, channels[otherIndex].qos.mos);	// single leg, echo test
			} else if ( channels[myIndex] && channels[myIndex].qos ) {
				mos = channels[myIndex].qos.mos;
			} else {
				mos = 0;
			}
			dbDelay(channels[myIndex].sipcallid, mos);
			delete channels[otherIndex];
			delete channels[myIndex];
		}
	}
});
*/
