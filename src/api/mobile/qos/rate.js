var rpc = require('node-json-rpc');		// echo 1 > /proc/sys/net/ipv4/tcp_tw_reuse
var common = require(__lib + '/common');
var config = require(__base + '/config');
var db = require(__lib + '/db_handler');

//exports

exports.init = init;

//functions

function init(baseApi, category, web, webs) {
	var path = baseApi + category;

	//rate - Get API
	var usagePath = path + '/get';
	web.get(usagePath + '/:user_key/:signature', get);
}

function get(req, res) {
	var q1 = "SELECT s.number,gn.groupName,cu.currencySymbol,cu.vsUSD,cu.currencyCode FROM countries co, currencies cu, (SELECT a.number, m.iso FROM msisdn m, app a, device d WHERE d.user_key=" + db.escape(req.params.user_key) + " AND m.number=a.number AND a.id=d.app_id AND m.status='A' AND a.status='A' AND d.status='A') s LEFT JOIN customer_subscription cs ON cs.number=s.number AND cs.group_plan_expire>NOW() LEFT JOIN groupName gn ON cs.group_plan=gn.groupID WHERE s.iso=co.alpha2code AND co.currency=cu.currencyCode";
	db.query_noerr(q1, function(rows) {
		if ( !rows[0] ) {
			res.status(404).send('Not Found');
			return false;
		}
		var client = new rpc.Client({"host":config.VOICECORERPC_ADDR,"port":config.VOICECORERPC_PORT,"path":"/RPC","strict":true});
		client.call({jsonrpc:'2.0',method:'htable.get',params:[ 'channel', rows[0].number + "::rate" ],id:rows[0].number}, function (err, result) {
			if ( !err ) {
				if ( !result ) {
					res.status(404).send('Not Found');
					return false;
				}
				var o = JSON.parse(result.result.item.value);
				o.package_name = ( o.group_plan_apply == 1 ) ? rows[0].groupName : "";
				delete o.group_plan_apply;
				o.destination = o.destination.toUpperCase();
				o.symbol = "$";
				o.currency = "USD";
				o.local_symbol = rows[0].currencySymbol;
				o.local_currency = rows[0].currencyCode;
				o.price = Math.ceil(o.price*100)/100;
				var q3 = "SELECT co.alpha2code, " + db.escape(o.price) + "*" + db.escape(rows[0].vsUSD) + " local_price FROM countries co, isoPrefix ip WHERE ip.iso=co.alpha2code AND " + db.escape(rows[0].number) + " LIKE CONCAT(ip.prefix,'%') ORDER BY ip.prefix DESC LIMIT 1";
				db.query_noerr(q3, function(result) { 
					if (result && result[0]) {  
						o.local_price = Math.ceil(result[0].local_price*100)/100;
						o.iso = result[0].alpha2code;
					}
					delete o.prefix;
					res.json(o);	// app does not need this info
				});
			} else res.status(404).send('Not Found');
		});
	});
}
