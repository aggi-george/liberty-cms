var db = require(__lib + '/db_handler');
var common = require(__lib + '/common');
var config = require(__base + '/config');
var sessionManager = require('../../base/sessionManager');

//exports

exports.init = init;
exports.saveMobileStats = saveMobileStats;

//functions

var log = config.LOGSENABLED;

function init(baseApi, category, web, webs) {
    var path = baseApi + category;

    web.post(path + '/*', sessionManager.parsePost);

    // check session

//    web.post(path + '/*/:user_key/:signature', sessionManager.sessionCheckPost);
    web.post(path + '/*/:user_key/:signature', sessionManager.sessionCheckJson);
    web.get(path + '/*/:user_key/:signature', sessionManager.sessionCheckGet);

    // stats API

    var usagePath = path + '/app';
    web.post(usagePath + '/:platform/:user_key/:signature', function (req, res) {
        sessionManager.executeMethod (req, res, require("./stats"), undefined, "saveMobileStats");
    });
}

function saveMobileStats(req, res) {
    var platform = req.params.platform;
    var data = req.fields.data;

    if (!data || !platform) {
        common.error("StatsManager", "saveMobileStats: some params are missing");
        return res.status(400).json({
            code: -1,
            details: "Some params are missing",
            description: "Some params are missing"
        });
    }

    // save logs

    data.platform = platform;
    data.ts = new Date().getTime();
    db.circlesTalkStats.insert(data);

    return res.json({code: 0});
}
