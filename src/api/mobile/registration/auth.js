var request = require('request');
var config = require(__base + '/config');
var db = require(__lib + '/db_handler');
var common = require(__lib + '/common');
var push = require(__base + '/src/notifications/push');
var registration = require('./registration');
var settings = require('../settings/option');
var md5 = require('MD5');

var LOG = config.LOGSENABLED;

//exports

exports.auth = auth;

//functions

function auth(req, res) {
    var fields = req.fields;
    if (!fields.country_code || !fields.phone_number) {
        return res.status(400).json({
            "code": common.EMPTY_COUNTRY_CODE_PHONE,
            "details": "Country Code, Phone Empty",
            "description": "Country Code, Phone Empty"
        });
    }

    if (!registration.versionCheck(fields.version_key, res)) {
        return res.status(403).json({
            "code": common.VERSION_KEY_INVALID,
            "details": "Unauthorized",
            "description": "Unauthorized"
        });
    }

    if (!fields.uuid) {
        fields.uuid = "";
    }
    var fullNumber = fields.country_code + "" + fields.phone_number;
    var hashKey = fields.hash + "_" + fields.pin_code;

    db.uuidCheck(res, fields.device_id + "_" + fields.uuid, function () {
        db.cache_del("cache", "call_" + fullNumber);
        db.cache_get("cache", fullNumber + "_" + fields.device_id, function (value) {
            var memObj = value;
            if (LOG) common.log("Auth", "MemObj=" + JSON.stringify(memObj));

            var memKey;
            var circles = 0;
            if (memObj) {
                memKey = memObj.key;
                circles = memObj.circles;
            }

            if (hashKey != memKey) {
                return res.status(400).json({
                    "code": common.PIN_CODE_INVALID,
                    "details": "Hash/Pin error",
                    "description": "Credentials are Incorrect"
                });
            }
            db.cache_del("cache", "limit_" + fields.device_id);

            resetSession(fields.app_type, fullNumber);

            var params = {};
            params.fullnum = fields.country_code + "" + fields.phone_number;
            params.credit = 0;
            params.user_pass = registration.genPass();
            params.user_key = md5("" + params.fullnum + ":" + params.user_pass + "_" + new Date());
            params.country_code = fields.country_code;
            params.ipaddress = req.ip;
            params.device_id = fields.device_id;
            params.circles = circles;

            params.iso_code = fields.iso_code;
            params.app_type = fields.app_type;
            params.user_agent = req.headers['user-agent'];
            if (!params.app_type) {
                // default app type
                fields.app_type = 'gentwo';
            }

            db.contacts.update({ "contacts.nums" : new RegExp(params.fullnum, "i") }, { "$inc" : { "version" : 1 } }, { "multi" : true });

            db.query_noerr("UPDATE app a SET a.app_lastseen=NOW(), a.app_status=0 WHERE a.number="
            + db.escape(params.fullnum), function () {
            });

            registration.insertMSISDN(params, function (err, new_user, new_device) {
                if (err) {
                    common.error("auth", "DB insert error " + params.fullnum + " " + params.app_type);
                    return res.status(400).json({
                        "code" : common.ATTEMPT_RETRY,
                        "details" : "Please try again",
                        "description" : "Please try again"
                    });
                }
                // new_user params shows if user is new and he/she uses a device
                // which has never been used to register gentwo or selfcare before
                // if phone number is the same but device has been registered before new_user will be false

                var reply = new Object;
                reply.code = 0;
                reply.user_key = params.user_key;
                reply.pass = params.user_pass;
                reply.haptik_pass = "";
                reply.session_key = md5(params.user_key + ":" + new Date());
                reply.firstReg = new_device;

                if (params.fullnum == "6587654321") {
                    reply.firstReg = true;
                    res.json(reply);
                } else settings.loadSettings(params.fullnum, params.country_code, -1, "all", params.app_type, function (option) {
                    if (LOG) common.log("Auth", "Settings=" + JSON.stringify(option));
                    reply.settings = option;
                    res.json(reply);

                    setTimeout(function () {
                        notifyAppInstallation(params.app_type, new_user, new_device, fields.country_code, fields.phone_number);
                    }, 8000);
                });

                db.cache_put("uuid_cache", fields.device_id + "_" + fields.uuid, reply, 120000);
                db.cache_put("session_cache", params.user_key, reply.session_key, 900000);

                // only send pushJoined to new insert
                if ((fields.app_type == "gentwo") && new_user) {
                    pushJoined(params.fullnum, fields.app_type);
                }
            });
        });
    });
}

function notifyAppInstallation(app_type, newUser, newDevice, prefix, number) {
    var activity = app_type === "selfcare" ? "selfcare_install"
        : (app_type === "gentwo" ? "gentwo_install" : undefined);

    if (activity) {
        var key = config.NOTIFICATION_KEY;
        var url = "http://" + config.CMSHOST + ":" + config.CMSPORT
            + "/api/1/webhook/notifications/internal/" + key;

        request({
            uri: url,
            method: 'POST',
            json: {
                "activity": activity,
                "prefix": prefix,
                "number": number,
                "variables": {
                    "newUser": newUser,
                    "newDevice": newDevice
                }
            }
        }, function (error, response, body) {
            if (error) {
                common.error("Auth", "Notification is sent to a new email, error=" + error);
                return;
            }
            if (LOG) common.log("Auth", "Notification is sent to a new email, body=" + JSON.stringify(body));
        });
    }
}

function pushJoined(fullnum, app_type) {
    db.contacts.find({
            "contacts.nums" : new RegExp(fullnum, "i")
        }, {
            "number" : 1,
            "contacts" : { "$elemMatch" : { "nums" : new RegExp(fullnum, "i") } }
    }).toArray(function (err, result) {
        var contacts = "";
        if (result && result[0]) {
            result.forEach(function(o) { contacts += "'" + o.number.replace('+','') + "',"; });
            contacts = contacts.slice(0,-1);
        }
        var query = "SELECT d.token, d.type, d.flavor, a.number " +
            " FROM device d, app a WHERE d.app_id=a.id AND a.app_type=" + db.escape(app_type) + " AND d.status='A' AND a.status='A' " +
            " AND a.number!=" + db.escape(fullnum) + " AND d.token!='' AND number IN (" + contacts + ")";
        if (LOG) common.log("pushJoined", query);
        db.query_noerr(query, function (rows) {
            var numbers = (rows && rows[0]) ? rows.map(function(o) { return o.number }) : [];
            if (rows) for (var i = 0; i < rows.length; i++) {
                var data = {
                    mime_type: common.MIME_TYPE_CONTACT_REGISTRATION,
                    phone: "+" + fullnum
                };
                var name = "";
                if (result && result[0]) {
                    var list = result.filter(function(o) { if (o.number == rows[i].number) return o; else return undefined });
                    if (list && list[0] && list[0].contacts) {
                        var nameArr = list[0].contacts.filter(function(o) {
                            if (o.nums.indexOf("+" + fullnum) > -1) return o;
                            else return undefined;
                        });
                        if (nameArr && nameArr[0]) name = nameArr[0].ln ? nameArr[0].fn + " " + nameArr[0].ln : nameArr[0].fn;
                    }
                }
                if (rows[i].type == "apn") {
                    data.loc_key = "NOTIFICATION_CONTACT_JOINED";
                    data.sender = name;
                }
                push.send(rows[i].token, data, rows[i].type, rows[i].flavor);
            }
            db.contacts.update({ "number" : { "$in" : numbers } },
                { "$inc" : { "version" : 1 }, "$set" : { "updated" : new Date().getTime() } },
                { "multi" : true });
        });
    });
}

function resetSession(app_type, number) {
    var query = "SELECT d.user_key FROM app a, device d WHERE d.app_id=a.id AND a.app_type="
        + db.escape(app_type) + " AND a.number=" + db.escape(number);

    db.query_err(query, function (err, rows) {
        if (!err && rows) {

            // delete all active session from old devices

            for (var i = 0; i < rows.length; i++) {
                db.cache_del("session_cache", rows[i].user_key);
            }
        }
    });
}
