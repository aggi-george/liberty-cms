var db = require(__lib + '/db_handler');
var common = require(__lib + '/common');
var push = require(__base + '/src/notifications/push');
var config = require(__base + '/config');

var LOG = config.LOGSENABLED;

//exports

exports.deactivate = deactivate;
exports.setStatus = setStatus;

//functions

function setStatus(user_key, callback) {
    db.cache_del("session_cache", user_key);
    var q_update = "UPDATE device d SET d.status='D' WHERE d.user_key=" + db.escape(user_key);
    db.query_noerr(q_update, callback);
}

function deactivate(req, res) {
    common.log(req.ip,"api/2/registration/deactivate/" + req.params.user_key + "/" + req.params.signature);
    var fields = req.fields;
    var fields_app_type = fields ? fields.app_type : "";
    var getNumber = function(user_key, callback) {
        var q_select = "SELECT a.number, a.app_type FROM app a, device d WHERE a.id=d.app_id AND d.user_key=" + db.escape(req.params.user_key);
        db.query_noerr(q_select, function (rows) {
            setStatus(req.params.user_key, function (result) {
                var newDeact = (result.affectedRows > 0) ? true : false;
                var number = (rows && rows[0]) ? rows[0].number : "";
                var app_type = (!fields_app_type && rows && rows[0]) ? rows[0].app_type : fields_app_type;
                callback(undefined, number, app_type, newDeact);
            });
        });
    };
    var getContacts = function(number, app_type, callback) {
        db.contacts.find({ "contacts.nums" : new RegExp(number, "i") }, { "number" : 1 }).toArray(function (err, result) {
            var contacts = "";
            if (result && result[0]) {
                result.forEach(function(o) {
                    contacts += "'" + o.number.replace('+','') + "',";
                });
                contacts = contacts.slice(0,-1);
                db.contacts.update({ "number" : { "$in" : result.map(function (o) { return o.number }) } },
                    { "$inc" : { "version" : 1 }, "$set" : { "updated" : new Date().getTime() } },
                    { "multi" : true });
            }
            var query = "SELECT d.token, d.type, d.flavor, a.number " +
                " FROM device d, app a WHERE d.app_id=a.id AND a.app_type=" + db.escape(app_type) + " AND d.status='A' AND a.status='A' " +
                " AND a.number!=" + db.escape(number) + " AND d.token!='' AND number IN (" + contacts + ")";
            if (LOG) common.log("deactivate", query);
            db.query_noerr(query, function(rows) {
                callback(undefined, rows);
            });
        });
    };
    if ( !fields.uuid ) fields.uuid = "";
    db.uuidCheck(res, req.params.user_key + "_" + fields.uuid, function() {
        getNumber(req.params.user_key, function(errNum, number, app_type, newDeact) {
            if (!newDeact) {
                return res.status(403).json({
                    "code" : common.USER_INVALID,
                    "details" : "already deactivated",
                    "description" : "Already Deactivated"
                });
            } else {
                var reply = { "code" : 0 };
                res.json(reply);
                db.cache_put("uuid_cache", req.params.user_key + "_" + fields.uuid, reply, 120000);
            }
            getContacts(number, app_type, function(errContacts, rows) {
                if (rows) for ( var i=0; i<rows.length; i++ ) {
                    if ( rows[i].type == "gcm" ) {
                        var data =  { "mime_type" : common.MIME_TYPE_CONTACT_DEACTIVATION, "phone" : "+" + number };
                        push.send(rows[i].token, data, rows[i].type, rows[i].flavor, app_type);
                    }
                }
            });
        });
    });
}
