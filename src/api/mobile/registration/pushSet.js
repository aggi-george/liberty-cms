var db = require(__lib + '/db_handler');
var common = require(__lib + '/common');
var config = require(__base + '/config');

var logsEnabled = config.LOGSENABLED;

//exports

exports.pushSet = pushSet;

//functions

function pushSet(req, res) {
	var fields = req.fields;
	if ( req.params.type == "gcm" && fields.gcm_id ) {
		var q = "UPDATE device SET token=" + db.escape(fields.gcm_id) + ", type='gcm', flavor='', vapn=NULL WHERE user_key=" + db.escape(req.params.user_key);
		if (logsEnabled) common.log("pushSet", q);
		db.query(res, q, function(rows) { 
			res.json({"code":0});
		});
	} else if ( req.params.type == "apn" && fields.push_token ) {
		var flavor = ( !fields.flavor ) ? "''" : db.escape(fields.flavor);
		var q = "UPDATE device SET token=" + db.escape(fields.push_token) + ", type='apn', flavor=" + flavor + " WHERE user_key=" + db.escape(req.params.user_key);
		if (logsEnabled) common.log("pushSet", q);
		db.query(res, q, function(rows) { 
			res.json({"code":0});
		});
	} else if ( req.params.type == "vapn" && fields.push_token ) {
		var q = "UPDATE device SET vapn=" + db.escape(fields.push_token) + ", type='apn' WHERE user_key=" + db.escape(req.params.user_key);
		if (logsEnabled) common.log("pushSet", q);
		db.query(res, q, function(rows) { 
			res.json({"code":0});
		});
	} else res.status(400).json({"code":common.EMPTY_PASSWORD,"details":"Empty parameter","description":"Empty Parameter"});
}
