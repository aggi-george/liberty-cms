var config = require(__base + '/config');
var db = require(__lib + '/db_handler');
var common = require(__lib + '/common');
var md5 = require('MD5');

var LOG = config.LOGSENABLED;

//exports

exports.refresh = refresh;

//functions

function refresh(req, res) {
    var fields = req.fields;
    if ( fields.pass ) {
        var q = "SELECT IF(d.secret = MD5(CONCAT(m.number,':'," + db.escape(config.REALM) + ",':'," + db.escape(fields.pass) + ")), 1, 0) secret, " +
            " m.number, a.app_type, a.status app_status, d.status device_status FROM device d, app a, msisdn m WHERE " +
            " d.app_id=a.id AND a.number=m.number AND d.user_key=" + db.escape(req.params.user_key);
        db.query_noerr(q, function(rows) {
            if ( rows && rows[0] ) {
                if ( rows[0].secret != 1 ) {
                    common.error("refresh", req.ip + " - " + rows[0].number + " - " + rows[0].app_type + " - CREDENTIALS INCORRECT");
                    res.status(403).json({
                        "code" : common.USER_INVALID,
                        "details" : "password error",
                        "description" : "Credentials are Incorrect"
                    });
                } else if ( rows[0].app_status != "A" ) {
                    common.error("refresh", req.ip + " - " + rows[0].number + " - " + rows[0].app_type + " - APP NOT ACTIVE");
                    res.status(403).json({
                        "code" : common.USER_INVALID,
                        "details" : "App error",
                        "description" : "App status error"
                    });
                } else if ( rows[0].device_status != "A" ) {
                    common.error("refresh", req.ip + " - " + rows[0].number + " - " + rows[0].app_type + " - DEVICE NOT ACTIVE");
                    res.status(403).json({
                        "code" : common.USER_INVALID,
                        "details" : "Device error",
                        "description" : "Device status error"
                    });
                } else {
                    var reply = new Object;
                    reply.code = 0;
                    reply.session_key = md5(req.params.user_key + ":" + new Date());
                    res.json(reply);
                    db.cache_put("session_cache", req.params.user_key, reply.session_key, 1800000);    // 30 mins
                    if (LOG) common.log("refresh", req.ip + " - " + rows[0].number + " - " + rows[0].app_type + " - REFRESH OK");
                    db.query_noerr("UPDATE device set user_agent=" + db.escape(req.headers['user-agent']) +
                        " WHERE user_key=" + db.escape(req.params.user_key), function() {});
                }
            } else {
                common.error("refresh", req.ip + " - api/2/registration/refresh/" + req.params.user_key + " - CUSTOMER NOT FOUND");
                res.status(403).json({
                    "code" : common.USER_INVALID,
                    "details" : "Account error",
                    "description" : "Account not found"
                });
            }
        });
    } else {
        res.status(403).json({
            "code" : common.EMPTY_PASSWORD,
            "details" : "Password Empty",
            "description" : "Password Empty"
        });
    }
}
