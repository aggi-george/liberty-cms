var db = require(__lib + '/db_handler');
var common = require(__lib + '/common');
var ec = require(__lib + '/elitecore');
var config = require(__base + '/config');
var registration = require('./registration');
var exec = require('child_process').exec;
var md5 = require('MD5');
var request = require('request');
var notificationSend = require(__core + '/manager/notifications/send');
var portInManager = require(__core + '/manager/porting/portInManager');
const utils = require(`${global.__core}/utils`);

var logsEnabled = config.LOGSENABLED;

//exports

exports.register = register;

//functions

function signup_log(number, hash, address, verify) {
    var q = "INSERT INTO signup_logs (number, hash, address, verify) VALUES (" + db.escape(number) + "," + db.escape(hash) + "," + db.escape(address) + "," + db.escape(verify) + ") ON DUPLICATE KEY UPDATE verify=VALUES(verify)";
    db.query_noerr(q, function() {});
}

function register(req, res) {
    var fields = req.fields;
    if ( registration.versionCheck(fields.version_key,res) ) {
        if ( !fields.uuid ) fields.uuid = "";
        db.uuidCheck(res, fields.device_id + "_" + fields.uuid, function() {
            if ( !fields.country_code || !fields.phone_number ) {
                return res.status(400).json({
                    "code":common.EMPTY_SIM_ID_COUNTRY_CODE_PHONE,
                    "details":"SIM ID, Country Code, Phone Empty",
                    "description":"SIM ID, Country Code, Phone Empty" });
            }
            const prefixList = utils.prefixes(`${fields.country_code}${fields.phone_number}`, 8, false);
            const riskyQ = `SELECT prefix FROM ratesGentwoRisky WHERE prefix IN (${prefixList})`;
            if (logsEnabled) common.log('risky query', riskyQ);
            db.query_err(riskyQ, (validErr, validRows) => {
                if (validErr || (validRows && validRows.length > 0)) {
                    common.error(`Registration Blocked ${req.ip}`, `${fields.country_code}${fields.phone_number}`);
                    return res.status(400).json({
                        "code":common.EMPTY_COUNTRY_CODE_PHONE,
                        "details":"Invalid Number",
                        "description":"Number has invalid digits" });
                }

                if ( (parseInt(fields.phone_number) != fields.phone_number) || (fields.phone_number.indexOf(" ") > -1) ) {
                    return res.status(400).json({
                        "code":common.EMPTY_COUNTRY_CODE_PHONE,
                        "details":"Invalid Number",
                        "description":"Number has invalid digits" });
                }
                db.cache_incr("cache", "limit_" + fields.device_id, 15 * 60 * 1000, function (count) {
                    if ( parseInt(count) < 5 ) {
                        if ( fields.country_code == "65" ) {
                            ec.is_circles(fields.phone_number, fields.country_code, function(circles, basePlan) {
                                if ((basePlan == "CirclesSwitch") &&
                                        config.REGISTRATION_API_KEY_3 && (config.REGISTRATION_API_KEY_3 != fields.version_key)) {
                                    return res.status(400).json({
                                        "code": common.VERSION_KEY_INVALID,
                                        "details": "version_key error",
                                        "description": "Version Key is Invalid"
                                    });
                                }
                                var dev_phones = [ "87654321" ];
                                if ( dev_phones.indexOf(fields.phone_number) != -1 ) {
                                    sendPIN(req, res, fields, circles, basePlan);
                                } else if ( fields.app_type == "selfcare" ) {
                                    if ( circles == 0 ) {
                                        checkPortIn(fields.country_code, fields.phone_number, (err, result) => {
                                            if (!err && result) {
                                                if (result.inputType == "PORT_IN_NUMBER") {
                                                    var portInStatus = result.request && result.request.status
                                                        ? result.request.status : "UNKNWON";

                                                    if (result.active) {
                                                        return res.status(400).json({
                                                            "code": common.REGISTER_PORT_IN_NUMBER_ACTIVE,
                                                            "details": "Port-In status " + portInStatus,
                                                            "title": "Login with temporary number",
                                                            "description": "Your number " + result.portinNumber +
                                                            " will be ported over to Circles.Life soon." +
                                                            " \n\nMeanwhile, you may login using this temporary number:" +
                                                            " \n\n" + result.tempNumber,
                                                            "data": result
                                                        });
                                                    } else {
                                                        return res.status(400).json({
                                                            "code": common.REGISTER_PORT_IN_NUMBER_NOT_ACTIVE,
                                                            "details": "Port-In status " + portInStatus,
                                                            "title": "Login with temporary number",
                                                            "description": "Port-in of your number " + result.portinNumber +
                                                            " has either failed or been canceled." +
                                                            " \n\nMeanwhile, you may login using this temporary number" +
                                                            " and retry port-in from Settings:" +
                                                            " \n\n" + result.tempNumber,
                                                            "data": result
                                                        });
                                                    }
                                                } else if (result.inputType == "TEMP_NUMBER") {
                                                    return res.status(400).json({
                                                        "code": common.REGISTER_PORT_IN_NUMBER_NOT_ACTIVE,
                                                        "details": "Port-In status " + portInStatus,
                                                        "title": "Login with ported number",
                                                        "description": result.tempNumber + "\n\n This temporary number is no" +
                                                        " longer active because porting has already completed." +
                                                        " \n\nYou may login with your old number:" +
                                                        " \n\n" + result.portinNumber,
                                                        "data": result
                                                    });
                                                }
                                            }

                                            common.log("register selfcare not circles", fields.country_code + " " + fields.phone_number);
                                            res.status(400).json({
                                                "code": common.NOT_CIRCLES,
                                                "details": "Invalid Circles Number",
                                                "description": "Not a Circles Customer"
                                            });
                                        });
                                    } else if ( (basePlan == "CirclesRegistration") || (fields.phone_number == "87654322") ) {
                                        res.status(400).json({
                                            "code" : common.CIRCLES_STILL_PROCESSING,
                                            "details" : "Initial Base Plan",
                                            "description" : "Please try again in a while, account still under processing" });
                                    } else {
                                        sendPIN(req, res, fields, circles, basePlan);
                                    }
                                } else {
                                    sendPIN(req, res, fields, circles, basePlan);
                                }
                            });
                        } else {
                            sendPIN(req, res, fields, 0, undefined);
                        }
                    } else return res.status(400).json({
                        "code":common.ATTEMPT_RETRY,
                        "details":"Max Attempts",
                        "description":"Too many attempts made, try again later" });
                });
            });
        });
    } // error catch is done by versionCheck function
}

function sendSMS(prefix, number, sender, pin) {
    var notification = {
        "activity" : "activate_app",
        "prefix" : prefix,
        "number" : number,
        "teamID" : 1,
        "teamName" : "Mobile App",
        "sender" : sender,
        "pin" : pin
    };
    notificationSend.deliver(notification, null, function() {});
}

function checkPortIn(prefix, number, callback) {
    if (!callback) callback = () => { };
    portInManager.loadRecentRequest(prefix, number, function (err, result) {
        if (err) {
            return callback(err);
        }

        if (result) {
            callback(undefined, result);
        } else {
            callback();
        }
    });
}

function sendPIN(req, res, fields, circles) {
    var fullnum = fields.country_code + "" + fields.phone_number;
    var hash = md5(new Date());
    if ( fullnum == "6587654321" ) fields.fakepin = "1";        // fake pin for apple app submission
    db.cache_get("cache", fullnum + "_" + fields.device_id, function(value) {
//        mem_obj = value ? JSON.parse(value) : undefined;
        var mem_obj = value;
        var digit1, digit2, digit3, digit4, digit5, prefix;
        if ( mem_obj ) {
            digit1 = parseInt(mem_obj.digits[0]);
            digit2 = parseInt(mem_obj.digits[1]);
            digit3 = parseInt(mem_obj.digits[2]);
            digit4 = parseInt(mem_obj.digits[3]);
            digit5 = parseInt(mem_obj.digits[4]);
            prefix = mem_obj.prefix;
        } else {
            digit1 = Math.floor((Math.random() * 10));
            digit2 = Math.floor((Math.random() * 10));
            digit3 = Math.floor((Math.random() * 10));
            digit4 = Math.floor((Math.random() * 10));
            digit5 = Math.floor((Math.random() * 10));
            prefix = Math.floor(Math.random()*900) + 100;
            if  ( fields.fakepin == "1" ) {                // debug only, remove.
                digit1=1;
                digit2=1;
                digit3=1;
                digit4=2;
                digit5=2;
            }
        }
        var pin1 = (digit1 + digit2) % 10;
        var pin2 = (digit2 + digit3) % 10;
        var pin3 = (digit3 + digit4) % 10;
        var pin4 = (digit4 + digit5) % 10;
        var pin = "" + pin1 + "" + pin2 + "" + pin3 + "" + pin4;
        var cid = "63" + prefix + "" + digit1 + "" + digit2 + "" + digit3 + "" + digit4 + "" + digit5;
        var cache = {    "key" : hash + "_" + pin,
                "digits" : digit1 + "" + digit2 + "" + digit3 + "" + digit4 + "" + digit5,
                "prefix" : prefix,
                "circles": circles };
        db.cache_put("cache", fullnum + "_" + fields.device_id, cache, 900000);
//        db.cache_put("cache", fullnum + "_" + fields.device_id, "{\"key\":\"" + hash + "_" + pin + "\",\"digits\":\"" + digit1 + "" + digit2 + "" + digit3 + "" + digit4 + "" + digit5 + "\",\"prefix\":" + prefix + "}", 900000);
        db.cache_put("cache", "call_" + fullnum, fields.device_id, 20000);
        var reply = new Object;
        reply.code = 0;
        reply.hash = hash;
        reply.prefix = "63" + prefix;
        res.json(reply);
        db.cache_put("uuid_cache", fields.device_id + "_" + fields.uuid, reply, 120000);
        var sender = "";
        if ( fields.app_type == "selfcare" ) {
            sender = "CirclesCare";
        } else {        // catch all, old smartbzz don't have app_type
            sender = "CirclesTalk";
        }
        if ( fields.callblock != "0" ) {
            signup_log(fullnum, hash, req.ip, "call");
            var sipp = config.SIPP + " -bg -skip_rlimit -cid_str " + md5(cid + " " + new Date()) + " -key from_user " + cid + " -sf ./src/api/mobile/verify.xml -m 1 -l 1 -r 1 -s " + fullnum + " " + config.ACTIVATEPROXY;
            if (logsEnabled) common.log("sendPIN", sipp);
            exec(sipp);
            setTimeout(function () {
                db.cache_get("cache", "call_" + fullnum, function(value) {
                    if ( value ) {
                        signup_log(fullnum, hash, req.ip, "sms");
                        sendSMS(fields.country_code, fields.phone_number, sender, pin);

//                        common.sms(sender, fullnum, sender + " SMS verification code is: " + pin);
                    }    // no need for error catch, client will resend reg if no sms
                });
            }, 13000);        // 13 seconds delay
        } else {
            signup_log(fullnum, hash, req.ip, "sms");
            sendSMS(fields.country_code, fields.phone_number, sender, pin);
//            common.sms(sender, fullnum, sender + " SMS verification code is: " + pin);
        }
    });
}
