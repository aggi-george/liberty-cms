var md5 = require('MD5');
var config = require(__base + '/config');
var db = require(__lib + '/db_handler');
var common = require(__lib + '/common');

var register = require('./register');
var auth = require('./auth');
var pushSet = require('./pushSet');
var refresh = require('./refresh');
var deactivate = require('./deactivate');

var LOG = config.LOGSENABLED;
var checkSessionEnabled = true;
var postFieldsLimit = 20;

//exports

exports.init = init;
exports.versionCheck = versionCheck;
exports.genPass = genPass;
exports.insertMSISDN = insertMSISDN;

//functions

function init(baseApi, category, web, webs) {
    var path = baseApi + category;

    //Parse params
    webs.post(path + '/*', parsePost);
    web.post(path + '/*', parsePost);

    //Registration API
    var usagePath = path + '';

    webs.post(usagePath + '/register', register.register);
    webs.post(usagePath + '/auth', auth.auth);
    webs.post(usagePath + '/:type/set/:user_key', pushSet.pushSet);
    webs.post(usagePath + '/refresh/:user_key', refresh.refresh);

    webs.post(usagePath + '/deactivate/:user_key/:signature', sessionCheckPost);
    webs.post(usagePath + '/deactivate/:user_key/:signature', deactivate.deactivate);


    // DEPRECATED
    web.post(usagePath + '/deactivate/:user_key/:signature', sessionCheckPost);
    web.post(usagePath + '/deactivate/:user_key/:signature', deactivate.deactivate);
    // DEPRECATED

}

function parsePost(req, res, next) {
    common.formParse(req, res, postFieldsLimit, function (fields) {
        req.fields = fields;
        next();
    });
}

function sessionCheckPost(req, res, next) {
    if (checkSessionEnabled) {
        db.sessionCheckPost(req, res, req.fields, function (req, res) {
            next();
        });
    } else {
        next();
    }
}

function versionCheck(key, res) {
    if (key && config.REGISTRATION_API_KEY && (config.REGISTRATION_API_KEY == key)) return true;
    else if (key && config.REGISTRATION_API_KEY_NEW && (config.REGISTRATION_API_KEY_NEW == key)) return true;
    else if (key && config.REGISTRATION_API_KEY_3 && (config.REGISTRATION_API_KEY_3 == key)) return true;
    else {
        res.status(400).json({
            "code": common.VERSION_KEY_INVALID,
            "details": "version_key error",
            "description": "Version Key is Invalid"
        });
        return false;
    }
}

function insertMSISDN(params, callback) {

    var insertAppDevice = function (connection, params, devId, appId, callback) {
        var queryApp = "INSERT INTO app (number, app_type, status) VALUES (" + db.escape(params.fullnum)
            + ", " + db.escape(params.app_type) + ", 'A') ON DUPLICATE KEY UPDATE status=VALUES(status)";

        if (LOG) common.log("insertMSISDN", "insertAppDevice: queryApp=" + queryApp);
        connection.query(queryApp, function (err, result) {
            if (err) {
                common.error("insertAppDevice " + queryApp, err.message);
                callback(err);
            } else {
                var secret = secretHash(params.fullnum, params.user_pass);
                var newDevice = !devId;
                var newUser = !appId;
                var queryDevice;

                if (devId > 0) {
                    queryDevice = "UPDATE device d, app a SET d.app_id=a.id, d.secret=" + db.escape(secret)
                    + " , d.user_key='" + params.user_key + "', d.user_agent=" + db.escape(params.user_agent)
                    + ", d.status='A' WHERE a.number=" + db.escape(params.fullnum)
                    + " AND a.app_type=" + db.escape(params.app_type) + " AND d.id=" + devId;
                } else {
                    queryDevice = "INSERT INTO device (device_id, user_key, secret, app_id, status, user_agent) SELECT "
                    + db.escape(params.device_id) + ", '" + params.user_key + "', " + db.escape(secret)
                    + ", a.id, 'A', " + db.escape(params.user_agent) + " FROM app a WHERE a.app_type="
                    + db.escape(params.app_type) + " AND a.number=" + db.escape(params.fullnum);
                }

                if (LOG) common.log("insertMSISDN", "insertAppDevice: queryDevice=" + queryDevice);
                connection.query(queryDevice, function (err, result) {
                    if (err) {
                        common.error("insertAppDevice " + queryDevice, err.message);
                        callback(err);
                    } else {
                        callback(null, newUser, newDevice);
                    }
                });
            }
        });
    }

    function insertGenTwo(connection, params, dev_id, app_id, callback) {
        var queryGenTwo = "UPDATE app a, device d SET a.status='D', d.status='D' WHERE a.app_type="
            + db.escape(params.app_type) + " AND d.app_id=a.id AND (d.device_id=" + db.escape(params.device_id)
            + " OR a.number=" + db.escape(params.fullnum) + ")";

        if (LOG) common.log("insertMSISDN", "insertGenTwo: queryGenTwo=" + queryGenTwo);
        connection.query(queryGenTwo, function (err, result) {
            if (err) {
                common.error("insertGenTwo " + queryGenTwo, err.message);
                callback(err);
            } else {
                insertAppDevice(connection, params, dev_id, app_id, callback);
            }
        });
    }

    var insertAppType = function (connection, params, callback) {
        var queryAppType = "SELECT (SELECT d.id FROM app a, device d WHERE d.app_id=a.id AND a.app_type="
            + db.escape(params.app_type) + " AND d.device_id=" + db.escape(params.device_id)
            + ") dev_id, (SELECT a.id FROM app a WHERE a.app_type=" + db.escape(params.app_type)
            + " AND a.number=" + db.escape(params.fullnum) + ") app_id;"
        if (LOG) common.log("insertMSISDN", "insertAppType: queryAppType=" + queryAppType);

        connection.query(queryAppType, function (err, result) {
            var dev_id = (result && result[0]) ? result[0].dev_id : undefined;
            var app_id = (result && result[0]) ? result[0].app_id : undefined;
            if (err) {
                common.error("insertAppType " + queryAppType, err.message);
                callback(err);
            } else {
                if (params.app_type == "gentwo") {
                    insertGenTwo(connection, params, dev_id, app_id, callback);
                } else if (params.app_type == "selfcare") {
                    insertAppDevice(connection, params, dev_id, app_id, callback);
                }
            }
        });
    }

    var insertMSISDN = function (connection, params, callback) {
        var queryMSISDN = "INSERT INTO msisdn (number, circles, prefix, iso, credit, ipaddress, status)"
            + " SELECT " + db.escape(params.fullnum) + ", " + params.circles + ", " + db.escape(params.country_code) + ","
            + " IF(iso IS NULL," + db.escape(params.iso_code) + ",iso), " + params.credit + ", " + db.escape(params.ipaddress) + ", 'A'"
            + " FROM isoPrefix WHERE " + db.escape(params.fullnum) + " LIKE CONCAT(prefix, '%') ORDER BY LENGTH(prefix) DESC LIMIT 1"
            + " ON DUPLICATE KEY UPDATE circles=VALUES(circles), status=VALUES(status)";
        connection.query(queryMSISDN, function (err, result) {
            var dev_id = (result && result[0]) ? result[0].dev_id : undefined;
            if (err) {
                common.error("insertMSISDN " + queryMSISDN, err.message);
                callback(err);
            } else {
                insertAppType(connection, params, callback);
            }
        });
    }

    db.transaction(function (err, connection) {
        if (!connection || err) {
            common.error("insertMSISDN Connection Error", err.message);
            callback(err);
        } else {
            insertMSISDN(connection, params, function (err, newUser, newDevice) {
                if (err) {
                    common.error("insertMSISDN Transaction rollback", err.message);
                    db.rollback(connection, function () {
                        callback(err);
                    });
                } else {
                    connection.commit(function (err) {
                        if (err) {
                            common.error("insertMSISDN Commit rollback", err.message);
                            db.rollback(connection, function () {
                                callback(err);
                            });
                        } else {
                            connection.release();
                            callback(null, newUser, newDevice);
                        }
                    });
                }
            });
        }
    });
}

function genPass() {
    var pwd = '';
    var chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    for (i = 1; i < 12; i++) {
        var c = Math.floor(Math.random() * chars.length + 1);
        pwd += chars.charAt(c)
    }
    return pwd;
}

function secretHash(user, clear) {
    var hash = md5(user + ":" + config.REALM + ":" + clear);
    return hash;
}
