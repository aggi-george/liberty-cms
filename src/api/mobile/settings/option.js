var request = require('request');
var config = require(__base + '/config');
var db = require(__lib + '/db_handler');
var common = require(__lib + '/common');
var ec = require(__lib + '/elitecore');
var scp = require(__lib + '/scp');
var betaUsersManager = require(__core + '/manager/beta/betaUsersManager');
var async = require('async');

var LOG = config.LOGSENABLED;

var CMS_SETTINGS = [
    'autoboost_limit',
    'location_based_service_enable',
    'roaming_cap_notification_amount'];

var SUPPORTED_INTEGER_SETTINGS = [
    'autoboost_limit',
    'roaming_cap_notification_amount'];

var SUPPORTED_STRING_SETTINGS = [
    'security_verification_type'];

var SUPPORTED_BOOLEAN_SETTINGS = [
    'security_email_update_enabled',
    'security_address_update_enabled',
    'security_boost_enabled',
    'security_credit_card_enabled',
    'security_credits_enabled',
    'security_plan_enabled'];

//exports

exports.settingSet = settingSet;
exports.settingsGet = settingsGet;
exports.supportGet = supportGet;
exports.loadSettings = loadSettings;
exports.loadSettingsCMS = loadSettingsCMS;
exports.loadConfigCMS = loadConfigCMS;
exports.loadActiveRequestCMS = loadActiveRequestCMS;

//functions

function settingSet(req, res) {
    var fields = req.fields;
    if (!fields.uuid) fields.uuid = "";

    db.uuidCheck(res, req.params.user_key + "_" + fields.uuid, function () {
        if (!fields.option_id || !fields.value) {
            return res.status(400).json({
                "code": common.PARSE_ERROR,
                "details": "Parameters Empty",
                "description": "Parameters Empty"
            });
        }

        var appType = (fields.app_type) ? fields.app_type : "gentwo";
        var query = "SELECT a.number, a.id FROM app a, device d WHERE a.app_type=" + db.escape(appType)
            + " AND d.app_id=a.id AND d.user_key=" + db.escape(req.params.user_key);

        if (LOG) common.log("settings option", "set query=" + query);
        db.query(res, query, function (rows) {
            if (!rows || !rows[0]) {
                res.status(400).json({
                    "code": -1,
                    "details": "App information is not found",
                    "description": "App information is not found"
                });
            }

            var appId = rows[0].id;
            var number = rows[0].number;
            var logSettings = function(type, method, appId, serviceInstanceNumber, lErr, lResult) {
                var msisdn = (number.length == 10) && (number.slice(0,-8) == "65") ? number.substring(2) : number;
                db.addonlog.insert({
                    "ts" : new Date().getTime(),
                    "myfqdn" : config.MYFQDN,
                    "method" : method,
                    "type" : type,
                    "serviceInstanceNumber" : serviceInstanceNumber,
                    "appId" : appId,
                    "result" : lResult,
                    "error" : lErr,
                    "number" : msisdn,
                    "status" : (lErr ? "ERROR" : "OK")
                });
            }
            if (fields.option_id == "scp_incoming") {

                var method = (fields.value == "0") ? "update" : "set";
                if (rows && rows[0]) opencodeOps(method, number, function (reply) {
                    if (reply) {
                        reply.code = parseInt(reply.response_code);
                        if (reply.code == "-100") reply.code = 0;    // no records affected
                        delete reply.response_code;
                        reply.details = reply.response_text;
                        delete reply.response_text;
                        res.json(reply);

                        if (fields.uuid != "") {
                            db.cache_put("uuid_cache", req.params.user_key + "_" + fields.uuid, reply, 120000);
                        }
                        logSettings(fields.option_id, method, undefined, undefined, undefined, reply);
                    } else {
                        res.status(500).json({
                            "code": common.PARSE_ERROR,
                            "details": "SCP Error",
                            "description": "SCP Error"
                        });
                    }
                });
            } else if (SUPPORTED_STRING_SETTINGS.indexOf(fields.option_id) >= 0
                || SUPPORTED_BOOLEAN_SETTINGS.indexOf(fields.option_id) >= 0
                || SUPPORTED_INTEGER_SETTINGS.indexOf(fields.option_id) >= 0) {

                var type;
                if (SUPPORTED_STRING_SETTINGS.indexOf(fields.option_id) >= 0) {
                    type = 'String';
                } else if (SUPPORTED_BOOLEAN_SETTINGS.indexOf(fields.option_id) >= 0) {
                    type = 'Boolean';
                } else if (SUPPORTED_INTEGER_SETTINGS.indexOf(fields.option_id) >= 0) {
                    type = 'Integer';
                } else {
                    return res.status(400).json({
                        "code": -1,
                        "details": "Inconsistent data in option keys",
                        "description": "Inconsistent data in option keys"
                    });
                }

                var saveResponse = function (err) {
                    if (err) {
                        return res.status(400).json({
                            "code": -1,
                            "details": err.message ? err.message : "Failed to save setting option",
                            "description": "Failed to save setting option"
                        });
                    }
                    return res.json({
                        "code": 0
                    });
                };

                var saveLocalSetting = function (option_id, value, type, appId, serviceInstanceNumber, callback) {
                    var query = "INSERT INTO setting (skey, svalue, stype, app_id) VALUES ("
                        + db.escape(fields.option_id) + "," + db.escape(fields.value + "") + ","
                        + db.escape(type) + "," + db.escape(appId) + ")"
                        + " ON DUPLICATE KEY UPDATE svalue="
                        + db.escape(fields.value + "");

                    db.query_err(query, function (err, rows) {
                        logSettings(option_id, value, appId, serviceInstanceNumber, err, {
                            "affectedRows" : (rows ? rows.affectedRows : 0),
                            "insertId" : (rows ? rows.insertId : 0),
                        });
                        callback(err);
                    });
                }

                if (CMS_SETTINGS.indexOf(fields.option_id) >= 0) {
                    ec.getCache(req.params.user_key, false, function (err, cache) {
                        if (err || !cache) {
                            return saveLocalSetting(fields.option_id, fields.value,
                                type, appId, undefined, saveResponse);
                        }

                        updateSettingsCMS(cache.prefix, cache.number, appType,
                            fields.option_id, fields.value, type, function (err) {
                                if (err) {
                                    return saveResponse(err);
                                }

                                return saveLocalSetting(fields.option_id, fields.value,
                                    type, appId, cache.serviceInstanceNumber, saveResponse);
                            });
                    });
                } else {
                    saveLocalSetting(fields.option_id, fields.value,
                        type, appId, undefined, saveResponse);
                }
            } else {
                return res.status(400).json({
                    "code": -1,
                    "details": "Not supported setting option",
                    "description": "Not supported setting option"
                });
            }
        });

    });
}

function settingsGet(req, res) {
    var fields = req.query;
    if (!fields.uuid) fields.uuid = "";
    db.uuidCheck(res, req.params.user_key + "_" + fields.uuid, function () {
        if (fields.option_id) {
            var query = "SELECT m.number, m.prefix, a.id FROM msisdn m, app a, device d WHERE a.number=m.number " +
                "AND d.app_id=a.id AND d.user_key=" + db.escape(req.params.user_key);

            db.query(res, query, function (rows) {
                var option_id = fields.option_id.split(",");
                var app_type = (fields.app_type) ? fields.app_type : "gentwo";
                if (LOG) common.log("list_get rows option_id", JSON.stringify(rows) + " " + JSON.stringify(option_id));

                if (!rows || rows.length == 0) {
                    res.json([]);
                    return;
                }

                loadSettings(rows[0].number, rows[0].prefix, rows[0].id, option_id, app_type, function (reply) {
                    res.json(reply);
                    if (fields.uuid != "") {
                        db.cache_put("uuid_cache", req.params.user_key + "_" + fields.uuid, reply, 120000);
                    }
                });
            });
        } else {
            res.status(400).json({
                "code": common.PARSE_ERROR,
                "details": "Parameters Empty",
                "description": "Parameters Empty"
            });
        }
    });
}

function loadSettings(fullnum, prefix_num, app_id, option_list, app_type, callback) {
    var settings = new Array;

    var number = fullnum.substring(prefix_num.toString().length);
    var prefix = prefix_num.toString();
    var tasks = new Array();

    if (app_type === "gentwo") {

        if ((option_list === "all") || (option_list.indexOf("scp_incoming") != -1)) {
            tasks.push(function (callback) {
                if (LOG) common.log("opencode_get", number);
                scp.connect("get", prefix + "" + number, "mt", function (err, result) {
                    if (!err && result && result.data) {
                        callback(undefined, {"mt": result.data});
                    } else {
                        callback(undefined, {"mt": []});
                    }
                });
            });
        }

        if ((option_list == "all") || (option_list.indexOf("is_circles") != -1)) {
            tasks.push(function (callback) {
                ec.is_circles(number, prefix, function (circles, basePlan) {
                    callback(undefined, {"circles": circles});
                });
            });
        }

        if ((option_list == "all") || (option_list.indexOf("roaming") != -1)) {
            settings.push({
                "option_id": "roaming",
                "value_type": "Boolean",
                "value": 1
            });
        }

        if ((option_list == "all") || (option_list.indexOf("scp_incoming_show") != -1)) {
            tasks.push(function (callback) {
                betaUsersManager.isBetaCustomer(number, "circles_internal", function (err, visible) {
                    callback(undefined, {
                        scpIncomingShow: {
                        "option_id": "scp_incoming_show",
                        "value_type": "Boolean",
                        "value": visible ? 1 : 0
                    }});
                });
            });
        }

        if ((option_list == "all") || (option_list.indexOf("is_selfcare_enable") != -1)) {
            settings.push({
                "option_id": "is_selfcare_enable",
                "value_type": "Boolean",
                "value": 0
            });
        }

        if ((option_list == "all") || (option_list.indexOf("invite_show") != -1)) {
            settings.push({
                "option_id": "invite_show",
                "value_type": "Boolean",
                "value": 0
            });
        }
    }

    if ((option_list == "all") || (option_list.indexOf("identity_info") != -1)) {
        tasks.push(function (callback) {
            ec.getCustomerDetailsNumber(prefix, number, false, function (err, cache) {
                if (cache && cache.customerIdentity) {
                    loadActiveRequestCMS(prefix, number, function(err, result) {
                        var identity = result && result.customer_id_number ? result.customer_id_number : cache.customerIdentity.number;

                        var validLength = identity && identity.length == 9;
                        var digitsString = validLength ? identity.substring(1, 8) : null;
                        var validForVerification = validLength && /^\d+$/.test(digitsString);
                        var digits = validForVerification ? digitsString : null;

                        var firstLetter = validForVerification ? identity.substring(0, 1) : null;
                        var lastLetter = validForVerification ? identity.substring(8, 9) : null;

                        callback(undefined, {
                            identity_info: {
                                validForVerification: validForVerification,
                                firstLetter: firstLetter,
                                lastLetter: lastLetter
                            }
                        });
                    });
                } else {
                    callback(undefined, { identity_info : undefined }); 
                }
            });
        });
    }

    if (app_type === "selfcare") {
        if ((option_list == "all")
            || (option_list.indexOf("help_show_whatsapp") != -1)
            || (option_list.indexOf("settings_show_port_in") != -1)
            || (option_list.indexOf("settings_show_autoboost_limit") != -1)
            || (option_list.indexOf("theme_header_primary_color") != -1)
            || (option_list.indexOf("theme_header_secondary_color") != -1)
            || (option_list.indexOf("theme_header_icon") != -1)
            || (option_list.indexOf("theme_header_title") != -1)
            || (option_list.indexOf("theme_image_preview") != -1)
            || (option_list.indexOf("theme_image_full") != -1)
            || (option_list.indexOf("theme_enabled") != -1)
            || (option_list.indexOf("latest_app_version_ios") != -1)
            || (option_list.indexOf("latest_app_version_android") != -1)) {

            tasks.push(function (callback) {
                loadConfigCMS(prefix, number, app_type, option_list, function (err, remoteSettings) {
                    if (err) {
                        return callback(err);
                    }

                    callback(undefined, {remoteSettings: remoteSettings});
                });
            });
        }

        if ((option_list == "all")
            || (option_list.indexOf("autoboost_limit") != -1)
            || (option_list.indexOf("location_based_service_enable") != -1)
            || (option_list.indexOf("roaming_cap_notification_amount") != -1)) {

            tasks.push(function (callback) {
                loadSettingsCMS(prefix, number, app_type, function (err, userSettings) {
                    if (err) {
                        return callback(err);
                    }

                    callback(undefined, {userSettings: userSettings});
                });
            });
        }
    } else {
        if ((option_list == "all") || (option_list.indexOf("help_show_whatsapp") != -1)) {
            settings.push({
                "option_id": "help_show_whatsapp",
                "value_type": "Boolean",
                "value": 0
            });
        }
    }

    if ((option_list == "all") || (option_list.indexOf("social_image_url") != -1)) {
        settings.push({
            "option_id": "social_image_url",
            "value_type": "String",
            "value": "https://s3-ap-southeast-1.amazonaws.com/kirk.circles.asia/images/mobile_bonus.jpg"
        });
    }

    if ((option_list == "all") || (option_list.indexOf("dff_dialog_image_url") != -1)) {
        settings.push({
            "option_id": "dff_dialog_image_url",
            "value_type": "String",
            "value": "https://s3-ap-southeast-1.amazonaws.com/kirk.circles.asia/assets/PRD00711/ic_data_free_flow_popup_image.png"
        });
    }

    if ((option_list == "all") || (option_list.indexOf("social_referral_image_url") != -1)) {
        settings.push({
            "option_id": "social_referral_image_url",
            "value_type": "String",
            "value": "https://s3-ap-southeast-1.amazonaws.com/kirk.circles.asia/assets/PRD00474/fb_share_03212017.jpg"
        });
    }

    if ((option_list == "all") || (option_list.indexOf("help_show_email") != -1)) {
        settings.push({
            "option_id": "help_show_email",
            "value_type": "Boolean",
            "value": (app_type === "gentwo") ? 1 : 1
        });
    }

    if ((option_list == "all") || (option_list.indexOf("help_show_chat") != -1)) {
        settings.push({
            "option_id": "help_show_chat",
            "value_type": "Boolean",
            "value": (app_type === "gentwo") ? 0 : 1
        });
    }

    if ((option_list == "all") || (option_list.indexOf("help_show_faq") != -1)) {
        settings.push({
            "option_id": "help_show_faq",
            "value_type": "Boolean",
            "value": (app_type === "gentwo") ? 0 : 1
        });
    }

    if ((option_list == "all") || (option_list.indexOf("help_show_network_diagnostics") != -1)) {
        settings.push({
            "option_id": "help_show_network_diagnostics",
            "value_type": "Boolean",
            "value": (app_type === "gentwo") ? 0 : 1
        });
    }

    if ((option_list == "all") || (option_list.indexOf("usage_show_seconds") != -1)) {
        settings.push({
            "option_id": "usage_show_seconds",
            "value_type": "Boolean",
            "value": 0
        });
    }

    if ((option_list == "all") || (option_list.indexOf("customer_service_start_time") != -1)) {
        settings.push({
            "option_id": "customer_service_start_time",
            "value_type": "Time",
            "value": "01:00:00" //UTC time
        });
    }

    if ((option_list == "all") || (option_list.indexOf("customer_service_end_time") != -1)) {
        settings.push({
            "option_id": "customer_service_end_time",
            "value_type": "Time",
            "value": "14:00:00" //UTC time
        });
    }


    if ((option_list == "all") || (option_list.indexOf("customer_service_start_time_weekend") != -1)) {
        settings.push({
            "option_id": "customer_service_start_time_weekend",
            "value_type": "Time",
            "value": "01:00:00" //UTC time
        });
    }

    if ((option_list == "all") || (option_list.indexOf("customer_service_end_time_weekend") != -1)) {
        settings.push({
            "option_id": "customer_service_end_time_weekend",
            "value_type": "Time",
            "value": "10:00:00" //UTC time
        });
    }

    if ((option_list == "all") || (option_list.indexOf("help_live_chat_timeout") != -1)) {
        settings.push({
            "option_id": "help_live_chat_timeout",
            "value_type": "Integer",
            "value": "600000"
        });
    }

    if ((option_list == "all") || (option_list.indexOf("help_live_chat_description") != -1)) {
        settings.push({
            "option_id": "help_live_chat_description",
            "value_type": "String",
            "value": "You can reach out to our Happiness Experts via Live Chat from $start_time until $end_time on weekdays. " +
            "Outside business hours, weekends and Public Holidays, you may send us an email at happiness@circles.life."
        });
    }

    if ((option_list == "all") || (option_list.indexOf("help_whatsapp_chat_description") != -1)) {
        settings.push({
            "option_id": "help_whatsapp_chat_description",
            "value_type": "String",
            "value": "If WhatsApp is more natural for you, chat with us there from $start_time to $end_time on weekdays."
        });
    }

    if ((option_list == "all") || (option_list.indexOf("help_faq_description") != -1)) {
        settings.push({
            "option_id": "help_faq_description",
            "value_type": "String",
            "value": "If you're looking for quick answers, you can always check out our FAQs."
        });
    }

    if ((option_list == "all") || (option_list.indexOf("help_email_description") != -1)) {
        settings.push({
            "option_id": "help_email_description",
            "value_type": "String",
            "value": "You can always reach out to us even on weekends and Public Holidays by sending us an email at happiness@circles.life."
        });
    }

    if ((option_list == "all") || (option_list.indexOf("settings_roaming_addon_id") != -1)) {
        settings.push({
            "option_id": "settings_roaming_addon_id",
            "value_type": "String",
            "value": "roamingGroup"
        });
    }

    var query = "SELECT skey, svalue, stype FROM setting WHERE app_id=" + app_id;
    db.query_err(query, function (err, rows) {
        if (err || !rows) {
            if (LOG) common.error("can not read settings from db");
        } else {
            rows.forEach(function (option) {
                if ((option_list == "all") || (option_list.indexOf(option.skey) != -1)) {
                    settings.push({
                        "option_id": option.skey,
                        "value_type": option.stype,
                        "value": option.svalue
                    });
                }
            });
        }

        if (tasks.length > 0) {
            async.parallel(tasks, function (err, asyncResult) {

                asyncResult.forEach(function (item) {
                    if (item && item.mt && item.mt[0]) {
                        var sip_allowed = (item.mt[0].sip_allowed) ? parseInt(item.mt[0].sip_allowed) : 0;
                        settings.push({
                            "option_id": "scp_incoming",
                            "value_type": "Boolean",
                            "value": sip_allowed
                        });
                    } else if (item && item.circles) {
                        var circles = item.circles;
                        settings.push({
                            "option_id": "is_circles",
                            "value_type": "Boolean",
                            "value": circles
                        });
                    } else if (item && item.identity_info) {
                        var identity_info = item.identity_info;
                        settings.push({
                            "option_id": "identity_info",
                            "value_type": "JSON",
                            "value": identity_info
                        });
                    } else if (item && item.remoteSettings) {
                        item.remoteSettings.forEach(function (item) {
                            settings.push(item);
                        });
                    } else if (item && item.userSettings) {
                        item.userSettings.forEach(function (item) {
                            settings.push(item);
                        });
                    } else if (item && item.scpIncomingShow) {
                        settings.push(item.scpIncomingShow);
                    }
                });

                if (LOG) common.log("tasks finished, result accounts option list length=" + settings.length);
                callback(settings);
            });
        } else {
            if (LOG) common.log("no execution of any tasks is required, result accounts option list=",
                JSON.stringify(settings));
            callback(settings);
        }
    });
}

function supportGet(req, res) {
    res.json({
        "code": 0,
        "faq_category": "",
        "faq_section": "",
        "email": "happiness@circles.life",
        "whatsapp": config.WHATSAPP_SUPPORT_NUMBER.replace(/[ ,.\-()]/g, '')
    });
}

function updateSettingsCMS(prefix, number, appType, optionId, value, type, callback) {
    var key = config.NOTIFICATION_KEY;
    var url = "http://" + config.CMSHOST + ":" + config.CMSPORT + "/api/1/profile/settings/set/" + key;

    request({
        uri: url,
        method: 'PUT',
        timeout: 20000,
        json: {
            prefix: prefix,
            number: number,
            app_type: appType,
            option_id: optionId,
            value: value,
            type: type
        }
    }, function (error, response, body) {
        if (error) {
            common.error("updateSettingsCMS", JSON.stringify(error) + " " + body)
            callback(error);
        } else {
            var reply = common.safeParse(body);
            if (reply.code == 0) {
                callback(undefined);
            } else {
                var error = new Error(reply.error ? reply.error : "Can not update setting");
                error.status = body.status;
                callback(error);
            }
        }
    });
}

function loadSettingsCMS(prefix, number, appType, callback) {
    var key = config.NOTIFICATION_KEY;
    var url = "http://" + config.CMSHOST + ":" + config.CMSPORT + "/api/1/profile/settings/get/"
        + appType + "/" + prefix + "/" + number + "/" + key;

    request({
        uri: url,
        method: 'GET',
        timeout: 20000
    }, function (error, response, body) {
        if (error) {
            common.error("loadSettingsCMS", JSON.stringify(error) + " " + body)
            callback(error);
        } else {
            var reply = common.safeParse(body);
            callback(undefined, reply.settings);
        }
    });
}

function loadConfigCMS(prefix, number, appType, optionList, callback) {
    var key = config.MOBILE_APP_KEY;
    var url = "http://" + config.CMSHOST + ":" + config.CMSPORT
        + "/api/1/config/map/get/" + prefix + "/" + number + "/" + appType + "/" + key;

    request({
        uri: url,
        method: 'GET',
        timeout: 20000
    }, function (error, response, body) {
        if (error) {
            common.error("loadConfigCMS", JSON.stringify(error) + " " + body)
            callback(error);
        } else {
            var reply = common.safeParse(body);
            parseConfigSettings(reply.config, optionList, function (remoteSettings) {
                callback(undefined, remoteSettings);
            });
        }
    });
}

function parseConfigSettings(items, optionList, callback) {
    var remoteSettings = [];

    if (!items) {
        return callback(remoteSettings);
    }

    items.forEach(function (configMap) {
        configMap.metadata.optionKeys.forEach(function (key) {
            if ((optionList == "all") || (optionList.indexOf(key) != -1)
                && configMap.options && configMap.options[key] != undefined
                && configMap.options.metadata && configMap.options.metadata[key] != undefined) {

                var value;
                if (configMap.options.metadata[key].type === 'Boolean') {
                    value = configMap.options[key] ? 1 : 0;
                } else {
                    value = configMap.options[key];
                }

                remoteSettings.push({
                    "option_id": key,
                    "value_type": configMap.options.metadata[key].type,
                    "value": value
                });
            }
        });
    });

    return callback(remoteSettings);
}

function opencodeOps(method, number, callback) {
    scp.connect(method, number, "MT", function (err, result) {
        if (result) result.code = (result && result.data && result.data[0]) ? result.data[0].resp_code :
            (result) ? result.response_code : -1;
        callback(result);
    });
}

function loadActiveRequestCMS(prefix, number, callback) {
    var key = config.NOTIFICATION_KEY;
    var url = "http://" + config.CMSHOST + ":" + config.CMSPORT
        + "/api/1/portin/request/active/" + prefix + "/" + number + "/" + key;

    request({
        uri: url,
        method: 'GET'
    }, function (error, response, body) {
        if (error) {
            if (callback) callback(error)
        } else {
            var reply = common.safeParse(body);
            if (callback) callback(undefined, reply ? reply.result : undefined);
        }
    });
}
