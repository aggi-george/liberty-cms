var request = require('request');
var sessionManager = require('../../base/sessionManager');
var db = require(__lib + '/db_handler');
var common = require(__lib + '/common');
var config = require(__base + '/config');

var option = require('./option');
var optionStub = require('./stub/option_stub_data1');

//exports

exports.init = init;

//functions

function init(baseApi, category, web, webs) {
    var path = baseApi + category;

    //Parse params

    webs.post(path + '/*', sessionManager.parsePost);

    //Check session

    webs.post(path + '/*/:user_key/:signature', sessionManager.sessionCheckPost);
    webs.get(path + '/*/:user_key/:signature', sessionManager.sessionCheckGet);

    // Settings - Option API
    var optionPath = path + '/option';

    webs.post(optionPath + '/set/:user_key/:signature', function (req, res) {
        sessionManager.executeMethod(req, res, option, optionStub, "settingSet");
    });
    webs.get(optionPath + '/list/get/:user_key/:signature', function (req, res) {
        sessionManager.executeMethod(req, res, option, optionStub, "settingsGet");
    });

    // Settings - Support API
    var supportPath = path + '/support';

    webs.get(supportPath + '/details/get/:user_key/:signature', function (req, res) {
        sessionManager.executeMethod(req, res, option, optionStub, "supportGet");
    });
}
