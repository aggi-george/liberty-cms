
//exports

exports.settingSet = settingSet;
exports.settingsGet = settingsGet;
exports.supportGet = supportGet;

//functions

function settingSet(req, res) {
    return res.json({
        "code": 0
    });
}

function settingsGet(req, res) {
    var settings = [];
    settings.push({
        option_id: "scp_incoming",
        value_type: "Boolean",
        value: 0
    });
    settings.push({
        option_id: "is_circles",
        value_type: "Boolean",
        value: 1
    });
    settings.push({
        option_id: "identity_info",
        value_type: "JSON",
        value: {
            identity_info: {
                validForVerification: true,
                firstLetter: 'A',
                lastLetter: 'B'
            }
        }
    });
    settings.push({
        option_id: "roaming",
        value_type: "Boolean",
        value: 1
    });
    settings.push({
        option_id: "scp_incoming_show",
        value_type: "Boolean",
        value: 0
    });
    settings.push({
        option_id: "is_selfcare_enable",
        value_type: "Boolean",
        value: 0
    });
    settings.push({
        option_id: "theme_enabled",
        value_type: "Boolean",
        value: 0
    });
    settings.push({
        option_id: "social_image_url",
        value_type: "String",
        value: "https://s3-ap-southeast-1.amazonaws.com/kirk.circles.asia/images/mobile_bonus.jpg"
    });
    settings.push({
        option_id: "social_referral_image_url",
        value_type: "String",
        value: "https://s3-ap-southeast-1.amazonaws.com/kirk.circles.asia/images/mobile_bonus.jpg"
    });
    settings.push({
        option_id: "help_show_whatsapp",
        value_type: "Boolean",
        value: 0
    });
    settings.push({
        option_id: "help_show_email",
        value_type: "Boolean",
        value: 1
    });
    settings.push({
        option_id: "help_show_chat",
        value_type: "Boolean",
        value: 1
    });
    settings.push({
        option_id: "help_show_faq",
        value_type: "Boolean",
        value: 1
    });
    settings.push({
        option_id: "help_show_network_diagnostics",
        value_type: "Boolean",
        value: 1
    });
    settings.push({
        option_id: "usage_show_seconds",
        value_type: "Boolean",
        value: 0
    });
    settings.push({
        option_id: "customer_service_start_time",
        value_type: "Time",
        value: "01:00:00" //UTC time
    });
    settings.push({
        option_id: "customer_service_end_time",
        value_type: "Time",
        value: "14:00:00" //UTC time
    });
    settings.push({
        option_id: "help_live_chat_description",
        value_type: "String",
        value: "You can reach out to our Happiness Experts via Live Chat from $start_time until $end_time on weekdays. Outside business hours, weekends and Public Holidays, you may send us an email at happiness@circles.life."
    });
    settings.push({
        option_id: "help_whatsapp_chat_description",
        value_type: "String",
        value: "If WhatsApp is more natural for you, chat with us there from $start_time to $end_time on weekdays."
    });
    settings.push({
        option_id: "help_faq_description",
        value_type: "String",
        value: "If you're looking for quick answers, you can always check out our FAQs."
    });
    settings.push({
        option_id: "help_email_description",
        value_type: "String",
        value: "You can always reach out to us even on weekends and Public Holidays by sending us an email at happiness@circles.life."
    });
    settings.push({
      "value_type": "String",
      "option_id": "settings_roaming_addon_id",
      "value": "roamingGroup"
    });
    res.json(settings);
}

function supportGet(req, res) {
    res.json({
        "code": 0,
        "faq_category": "",
        "faq_section": "",
        "email": "happiness@circles.life",
        "whatsapp": "+65 8742 0692"
    });
}

