var config = require('../../../config');
var common = require('../../../lib/common');
var db = require('../../../lib/db_handler');
var web = require('./web');

var logsEnabled = config.LOGSENABLED;

//exports

exports.getUsersGroups = getUsersGroups;
exports.addUser = addUser;
exports.deleteUser = deleteUser;
exports.updateUser = updateUser;
exports.addGroup = addGroup;
exports.updateName = updateName;
exports.updatePage = updatePage;
exports.updateGroup = updateGroup;
exports.getAccessLogs = getAccessLogs;
exports.getBannedEmails = getBannedEmails;
exports.deleteBannedEmail = deleteBannedEmail;
exports.addBannedEmail = addBannedEmail;

//functions

function getUsersGroups(req, res) {
	web.checkPermission("admin", req.gid, function(allowed) {
		if (!allowed) {
			res.status(403).json({ "code" : -1 });
		} else {
			var result = new Object;
			var p = "SELECT id, name, page FROM permissions ORDER BY name ASC";
			db.query_noerr(p, function (rows) {
				result.permissions = rows;
				var q = "SELECT g.id groupID, g.name groupName, g.defaultPage, gp.permissionID FROM groups g LEFT JOIN groupsPermissions gp ON g.id=gp.groupID ORDER BY g.name ASC";
				db.query_noerr(q, function (rows) {
					if (rows) rows.forEach(function (row) {
						if (!result.groups) result.groups = new Object;
						if (!result.groups["_" + row.groupID]) {
							result.groups["_" + row.groupID] = new Object;
							result.groups["_" + row.groupID].list = new Array;
						}
						result.groups["_" + row.groupID].name = row.groupName;
						result.groups["_" + row.groupID].default = row.defaultPage;
						result.groups["_" + row.groupID].list.push(row.permissionID);
					});
					var q_users = "SELECT a.adminID, a.email, g.id groupID, g.name groupName, a.renew, a.enabled FROM admin a LEFT JOIN groups g ON a.groupID=g.id";
					db.query_noerr(q_users, function (users) {
						result.users = users;
						res.json({ "code" : 0, "result" : result, "write" : allowed.write});
					});
				});
			});
		}
	});
}

function addUser(req, res) {
	web.checkPermission("admin", req.gid, function(allowed) {
		if (!allowed || !allowed.write) {
			res.status(403).json({ "code" : -1 });
		} else {
			var email = req.fields.email;
			var user = email.split("@")[0];
			var domain = email.split("@")[1];
			var password = req.fields.password;
			var group = req.params.group;
			var q = "INSERT INTO admin (email, password, groupID) VALUES (" + db.escape(email) + ", MD5(" + db.escape(user + ":" + domain + ":" + password) + "), " + db.escape(group) + ")";
			db.query_noerr(q, function (rows) {
				res.json({ "code" : 0 });
				db.weblog.insert({	"ts" : new Date().getTime(),
							"ip" : req.ip, 
							"type" : "addUser",
							"id" : common.escapeHtml(email),
							"action" : "INSERT",
							"username" : req.user,
							"code" : 0
						});
			});
		}
	});
}

function deleteUser(req, res) {
	web.checkPermission("admin", req.gid, function(allowed) {
		if (!allowed || !allowed.write) {
			res.status(403).json({ "code" : -1 });
		} else {
			var email = req.params.email;
			var q = "DELETE FROM admin WHERE email=" + db.escape(email);
			db.query_noerr(q, function (rows) {
				res.json({ "code" : 0 });
				db.weblog.insert({	"ts" : new Date().getTime(),
							"ip" : req.ip, 
							"type" : "deleteUser",
							"id" : common.escapeHtml(email),
							"action" : "DELETE",
							"username" : req.user,
							"code" : 0
						});
			});
		}
	});
}

function updateUser(req, res) {
	web.checkPermission("admin", req.gid, function(allowed) {
		if (!allowed || !allowed.write) {
			res.status(403).json({ "code" : -1 });
		} else {
			var id = req.params.id;
			var name = req.params.name;
			var type = req.params.type;
			if ( type == "group" ) {
				var q = "UPDATE admin SET groupID=" + db.escape(id) + " WHERE email=" + db.escape(name); 
				db.weblog.insert({	"ts" : new Date().getTime(),
							"ip" : req.ip, 
							"type" : "updateUser",
							"id" : common.escapeHtml(name),
							"action" : "groupID " + common.escapeHtml(id),
							"username" : req.user,
							"code" : 0
						});
			} else if ( type == "password" ) {
				var password = req.fields.password;
				var user = name.split("@")[0];
				var domain = name.split("@")[1];
				var q = "UPDATE admin SET password=MD5(" + db.escape(user + ":" + domain + ":" + password) + ") WHERE email=" + db.escape(name); 
				db.weblog.insert({	"ts" : new Date().getTime(),
							"ip" : req.ip, 
							"type" : "updateUser",
							"id" : common.escapeHtml(name),
							"action" : "password",
							"username" : req.user,
							"code" : 0
						});
			} else if ( type == "renew" ) {
				var q = "UPDATE admin SET renew=" + db.escape(id) + " WHERE email=" + db.escape(name);
			} else if ( type == "enabled" ) {
				var q = "UPDATE admin SET enabled=" + db.escape(id) + " WHERE email=" + db.escape(name);
			}
			db.query_noerr(q, function (rows) {
				res.json({ "code" : 0 });
			});
		}
	});
}

function addGroup(req, res) {
	web.checkPermission("admin", req.gid, function(allowed) {
		if (!allowed || !allowed.write) {
			res.status(403).json({ "code" : -1 });
		} else {
			var group = req.params.group;
			var q = "INSERT INTO groups (name) VALUES (" + db.escape(group) + ")";
			db.query_noerr(q, function (rows) {
				res.json({ "code" : 0 });
				db.weblog.insert({	"ts" : new Date().getTime(),
							"ip" : req.ip, 
							"type" : "addGroup",
							"id" : common.escapeHtml(group),
							"action" : "INSERT",
							"username" : req.user,
							"code" : 0
						});
			});
		}
	});
}

function updateGroup(req, res) {
	web.checkPermission("admin", req.gid, function(allowed) {
		if (!allowed || !allowed.write) {
			res.status(403).json({ "code" : -1 });
		} else {
			var group = req.params.group;
			var perm = req.params.perm;
			var q = "";
			if ( req.params.checked == "on" ) {
				q = "INSERT INTO groupsPermissions (groupID, permissionID) VALUES (" + db.escape(group) + "," + db.escape(perm) + ")"; 
				db.weblog.insert({	"ts" : new Date().getTime(),
							"ip" : req.ip, 
							"type" : "updatePermissionGroup",
							"id" : common.escapeHtml(group),
							"action" : "INSERT " + common.escapeHtml(perm),
							"username" : req.user,
							"code" : 0
						});
			} else if ( req.params.checked == "off" ) {
				q = "DELETE FROM groupsPermissions WHERE groupID=" + db.escape(group) + " AND permissionID=" + db.escape(perm); 
				db.weblog.insert({	"ts" : new Date().getTime(),
							"ip" : req.ip, 
							"type" : "updatePermissionGroup",
							"id" : common.escapeHtml(group),
							"action" : "DELETE " + common.escapeHtml(perm),
							"username" : req.user,
							"code" : 0
						});
			}
			db.query_noerr(q, function (rows) {
				res.json({ "code" : 0 });
			});
		}
	});
}

function updateName(req, res) {
	web.checkPermission("admin", req.gid, function(allowed) {
		if (!allowed || !allowed.write) {
			res.status(403).json({ "code" : -1 });
		} else {
			var group = req.params.group;
			var name = req.params.name;
			q = "UPDATE groups SET name=" + db.escape(name) + " WHERE id=" + db.escape(group); 
			db.query_noerr(q, function (rows) {
				res.json({ "code" : 0 });
				db.weblog.insert({	"ts" : new Date().getTime(),
							"ip" : req.ip, 
							"type" : "updateNameGroup",
							"id" : common.escapeHtml(name),
							"action" : common.escapeHtml(group),
							"username" : req.user,
							"code" : 0
						});
			});
		}
	});
}

function updatePage(req, res) {
	web.checkPermission("admin", req.gid, function(allowed) {
		if (!allowed || !allowed.write) {
			res.status(403).json({ "code" : -1 });
		} else {
			var group = req.params.group;
			var page = req.params.page;
			q = "UPDATE groups SET defaultPage=" + db.escape(page) + " WHERE id=" + db.escape(group); 
			db.query_noerr(q, function (rows) {
				res.json({ "code" : 0 });
				db.weblog.insert({	"ts" : new Date().getTime(),
							"ip" : req.ip, 
							"type" : "updatePageGroup",
							"id" : common.escapeHtml(page),
							"action" : common.escapeHtml(group),
							"username" : req.user,
							"code" : 0
						});
			});
		}
	});
}

function getAccessLogs(req, res) {
	web.checkPermission("admin", req.gid, function(allowed) {
		if (!allowed) {
			res.status(403).json({ "code" : -1 });
		} else {
			var search = common.escapeHtml(req.query.search);
			var match = { "ts" : { "$gt" : new Date().getTime() - 1000 * 60 * 60 * 24 * 3 } };
			if (req.query.search) {
				match["$or"] = [
						{ "type" : new RegExp(search,"i") },
						{ "username" : new RegExp(search,"i") }
					];
			}
			db.weblog.find(match).sort({ "_id" : -1 }).limit(50).toArray(function(err, item) { // 3 days
				if ( item ) res.json({ "code" : 0, "list" : item, "write" : allowed.write });
				else res.status(404).send('Not Found');
			});
		}
	});
}

function getBannedEmails(req, res) {
	web.checkPermission("admin", req.gid, function(allowed) {
		if (!allowed) {
			res.status(403).json({ "code" : -1 });
		} else {
			var search = req.query.search;
			var query = "SELECT email FROM bannedEmails" + (search ? " WHERE email LIKE " + db.escape('%' + search + '%')  : "");
			db.query_noerr(query, function (rows) {
				res.json({ "code" : 0, "list" : rows, "write" : allowed.write });
			});
		}
	});
}

function deleteBannedEmail(req, res) {
	web.checkPermission("admin", req.gid, function(allowed) {
		if (!allowed) {
			res.status(403).json({ "code" : -1 });
		} else {
			var email = req.params.email;
			var query = "DELETE FROM bannedEmails WHERE email=" + db.escape(email);
			db.query_noerr(query, function () {
				res.json({ "code" : 0, "write" : allowed.write });
			});
		}
	});
}

function addBannedEmail(req, res) {
	web.checkPermission("admin", req.gid, function(allowed) {
		if (!allowed) {
			res.status(403).json({ "code" : -1 });
		} else {
			var email = req.params.email;
			var query = "INSERT INTO bannedEmails (email) VALUES (" + db.escape(email) + ")";
			db.query_noerr(query, function () {
				res.json({ "code" : 0, "write" : allowed.write });
			});
		}
	});
}
