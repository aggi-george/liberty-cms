var async = require('async');

var config = require('../../../config');
var common = require('../../../lib/common');
var db = require('../../../lib/db_handler');
var ec = require('../../../lib/elitecore');
var elitecoreUserService = require('../../../lib/manager/ec/elitecoreUserService');
var web = require('./web');

var classManager = require('../../core/manager/analytics/classManager');
var classNotificationManager = require('../../core/manager/analytics/classNotificationManager');
var analyticsManager = require('../../core/manager/analytics/analyticsManager');
var syncQueueService = require(`${global.__queue}/sync`);

var log = config.LOGSENABLED;

//exports

exports.getClasses = getClasses;
exports.getClassesStats = getClassesStats;
exports.getCustomers = getCustomers;
exports.getNotificationIds = getNotificationIds;
exports.classifyCustomers = classifyCustomers;
exports.classesSendNotification = classesSendNotification;
exports.classifyCustomersProgress = classifyCustomersProgress;
exports.sendNotificationsProgress = sendNotificationsProgress;

//functions

function getClasses(req, res) {
    web.checkPermission("marketing", req.gid, function (allowed) {
        if (!allowed) {
            res.status(403).json({"code": -1});
        } else {
            var limit = parseInt(req.query.iDisplayLength);
            var offset = parseInt(req.query.iDisplayStart);
            var filter = req.query.sSearch;

            classManager.loadClasses(undefined, (err, result) => {
                if (err) {
                    return res.status(400).json({
                        code: -1,
                        error: err.message,
                        write: allowed.write
                    });
                }

                res.json({
                    code: 0,
                    write: allowed.write,
                    iTotalRecords: result.totalCount,
                    iTotalDisplayRecords: result.totalCount,
                    data: result.data
                });
            }, limit, offset, filter);
        }
    });
}

function getClassesStats(req, res) {
    web.checkPermission("marketing", req.gid, function (allowed) {
        if (!allowed) {
            res.status(403).json({"code": -1});
        } else {
            var type = req.params.type;
            var start = req.query.start ? new Date(req.query.start) : undefined;
            var end = req.query.end ? new Date(req.query.end) : undefined;
            var points = req.query.points ? parseInt(req.query.points) : undefined;

            if (!start || !start.getTime()) {
                start = new Date(new Date().getTime() - 7 * 24 * 60 * 60 * 1000);
            }
            if (!end || !end.getTime()) {
                end = new Date();
            }

            start.setHours(16, 0, 0, 0);
            end.setHours(16, 0, 0, 0);

            if (!points) {
                points = (end.getTime() - start.getTime()) / (24 * 60 * 60 * 1000) + 1;
            }

            analyticsManager.loadClassesStats(start, end, points, type, (err, result) => {
                if (err) {
                    return res.status(400).json({
                        code: -1,
                        error: err.message,
                        write: allowed.write
                    });
                }

                res.json({
                    code: 0,
                    write: allowed.write,
                    data: result
                });
            });
        }
    });
}

function getCustomers(req, res) {
    web.checkPermission("marketing", req.gid, function (allowed) {
        if (!allowed) {
            res.status(403).json({"code": -1});
        } else {
            var classId = req.params.classId;
            var limit = parseInt(req.query.iDisplayLength);
            var offset = parseInt(req.query.iDisplayStart);
            var filter = req.query.sSearch;

            classManager.loadCustomers(undefined, classId, (err, result) => {
                if (err) {
                    return res.status(400).json({
                        code: -1,
                        error: err.message,
                        write: allowed.write
                    });
                }

                res.json({
                    code: 0,
                    write: allowed.write,
                    iTotalRecords: result.totalCount,
                    iTotalDisplayRecords: result.totalCount,
                    dataInt1Average: result.dataInt1Average,
                    data: result.data
                });
            }, limit, offset, filter);
        }
    });
}

function getNotificationIds(req, res) {
    web.checkPermission("marketing", req.gid, function (allowed) {
        if (!allowed) {
            res.status(403).json({"code": -1});
        } else {
            var search = req.params.query;
            var query = "SELECT activity FROM notifications WHERE activity LIKE " +
                db.escape("%" + search + "%") + " ORDER BY activity LIMIT 10";

            db.query_err(query, function (err, result) {
                if (err) {
                    return res.status(400).json({
                        code: -1,
                        error: err.message,
                        write: allowed.write
                    });
                }

                var activities = [];
                result.forEach((notification) => {
                    activities.push({value: notification.activity});
                });
                res.json(activities);
            })
        }
    });
}

function classifyCustomers(req, res) {
    web.checkPermission("marketing", req.gid, function (allowed) {
        if (!allowed) {
            res.status(403).json({"code": -1});
        } else {
            var initiator = "[CMS][" + req.user + "]";

            syncQueueService.run("SYNC_CUSTOMER_CLASSES", {}, {
                errorIfRunning: true,
                initiator: initiator
            }, (err, result) => {
                if (err) {
                    return res.status(400).json({
                        code: -1,
                        error: err.message
                    });
                }

                res.json({
                    code: 0,
                    message: "Classes Sync will be Started Shortly."
                });
            });
        }
    });
}

function classesSendNotification(req, res) {
    web.checkPermission("marketing", req.gid, function (allowed) {
        if (!allowed) {
            res.status(403).json({"code": -1});
        } else {
            var initiator = "[CMS][" + req.user + "]";
            var activity = req.fields.activity;
            var classId = req.fields.classId ? parseInt(req.fields.classId) : undefined;
            var delay = req.fields.delay ? parseInt(req.fields.delay) : undefined;
            var timeUnique = req.fields.timeUnique ? parseInt(req.fields.timeUnique) : undefined;
            var emailUnique = req.fields.emailUnique == "true";

            if (!classId || !activity) {
                return res.status(400).json({
                    code: -1,
                    error: "Invalid Class ID or Notification Activity",
                    write: allowed.write
                });
            }

            syncQueueService.run("SEND_NOTIFICATION_TO_CLASS", {
                activity: activity,
                classId: classId,
                options: {
                    delay: delay,
                    emailUnique: emailUnique,
                    timeUnique: timeUnique
                }
            }, {
                errorIfRunning: true,
                initiator: initiator
            }, (err, result) => {
                if (err) {
                    return res.status(400).json({
                        code: -1,
                        error: err.message
                    });
                }

                res.json({
                    code: 0,
                    message: "Notification sending will be Started Shortly."
                });
            });
        }
    });
}

function classifyCustomersProgress(req, res) {
    web.checkPermission("marketing", req.gid, function (allowed) {
        if (!allowed) {
            res.status(403).json({"code": -1});
        } else {
            classManager.classifyCustomersProgress((err, result) => {
                if (err) {
                    return res.status(400).json({
                        code: -1,
                        error: err.message,
                        write: allowed.write
                    });
                }

                res.json({
                    code: 0,
                    stepsCount: result.stepsCount,
                    stepNumber: result.stepNumber,
                    stepProgress: result.stepProgress
                });
            });
        }
    });
}

function sendNotificationsProgress(req, res) {
    web.checkPermission("marketing", req.gid, function (allowed) {
        if (!allowed) {
            res.status(403).json({"code": -1});
        } else {
            classNotificationManager.sendNotificationsProgress((err, result) => {
                if (err) {
                    return res.status(400).json({
                        code: -1,
                        error: err.message,
                        write: allowed.write
                    });
                }

                res.json({
                    code: 0,
                    stepsCount: result.stepsCount,
                    stepNumber: result.stepNumber,
                    stepProgress: result.stepProgress
                });
            });
        }
    });
}
