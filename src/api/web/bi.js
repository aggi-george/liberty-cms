var async = require('async');
var math = require('mathjs');
var config = require('../../../config');
var db = require('../../../lib/db_handler');
var common = require('../../../lib/common');
var web = require('./web');
var ObjectId = require('mongodb').ObjectId;

var logsEnabled = config.LOGSENABLED;

exports.getPermissions = getPermissions;
exports.getAllStats= getAllStats;
exports.generateKpiValues = generateKpiValues;
exports.rawQuery= rawQuery;
exports.saveKpiDefinition = saveKpiDefinition;
exports.saveKpiGenerator = saveKpiGenerator;
exports.getKpiDefinitions = getKpiDefinitions;
exports.getKpiGenerators = getKpiGenerators;
exports.deleteKpiDefinition = deleteKpiDefinition;
exports.deleteKpiGenerator = deleteKpiGenerator;
exports.deleteKpiView = deleteKpiView;
exports.regenerate = regenerate;
exports.getKpiGenKeys = getKpiGenKeys;
exports.saveKpiView = saveKpiView;
exports.getKpiViews = getKpiViews;
exports.savekpiToView = savekpiToView;
exports.getKpiViewsWithValues = getKpiViewsWithValues;
exports.deleteKpiGenFromView = deleteKpiGenFromView;
exports.listKpiViewItems = listKpiViewItems;
exports.generateKpiValuesFromSchedulerEvent = generateKpiValuesFromSchedulerEvent;
exports.generateKpiValuesFromApi = generateKpiValuesFromApi;
exports.manualAddKpiValue = manualAddKpiValue;
exports.crashTestErroredMethod = crashTestErroredMethod;


var circlsLifeBirth = '01-05-2016 00:00:00';
var circlsLifeBirthTimestamp = 1462032000000;
var currentTime;
var currentTimeFormatted;
var billdatesTablePrefix = "Anurag.billdetails_";

var singleValueViewTypes = ["number", "target", "list"];

 var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
 var monthlyBounds = {
    "May 2016": {month: "May 2016", startDate: '01-05-2016 00:00:00', startTimestamp: 1462060800000, endDate: '01-06-2016 00:00:00', endTimestamp: 1464739200000},
    "Jun 2016": {month: "Jun 2016", startDate: '01-06-2016 00:00:00', startTimestamp: 1464739200000, endDate: '01-07-2016 00:00:00', endTimestamp: 1467331200000},
    "Jul 2016": {month: "Jul 2016", startDate: '01-07-2016 00:00:00', startTimestamp: 1467331200000, endDate: '01-08-2016 00:00:00', endTimestamp: 1470009600000},
    "Aug 2016": {month: "Aug 2016", startDate: '01-08-2016 00:00:00', startTimestamp: 1470009600000, endDate: '01-09-2016 00:00:00', endTimestamp: 1472688000000},
    "Sep 2016": {month: "Sep 2016", startDate: '01-09-2016 00:00:00', startTimestamp: 1472688000000, endDate: '01-10-2016 00:00:00', endTimestamp: 1475280000000},
    "Oct 2016": {month: "Oct 2016", startDate: '01-10-2016 00:00:00', startTimestamp: 1475280000000, endDate: '01-11-2016 00:00:00', endTimestamp: 1477958400000},
    "Nov 2016": {month: "Nov 2016", startDate: '01-11-2016 00:00:00', startTimestamp: 1477958400000, endDate: '01-12-2016 00:00:00', endTimestamp: 1480550400000},
    "Dec 2016": {month: "Dec 2016", startDate: '01-12-2016 00:00:00', startTimestamp: 1480550400000, endDate: '01-01-2017 00:00:00', endTimestamp: 1483228800000},
    "Jan 2017": {month: "Jan 2017", startDate: '01-01-2017 00:00:00', startTimestamp: 1483228800000, endDate: '01-02-2017 00:00:00', endTimestamp: 1485907200000},
    "Feb 2017": {month: "Feb 2017", startDate: '01-02-2017 00:00:00', startTimestamp: 1485907200000, endDate: '01-03-2017 00:00:00', endTimestamp: 1488326400000},
    "Mar 2017": {month: "Mar 2017", startDate: '01-03-2017 00:00:00', startTimestamp: 1488326400000, endDate: '01-04-2017 00:00:00', endTimestamp: 1491004800000},
    "Apr 2017": {month: "Apr 2017", startDate: '01-04-2017 00:00:00', startTimestamp: 1491004800000, endDate: '01-05-2017 00:00:00', endTimestamp: 1493596800000},
    "May 2017": {month: "May 2017", startDate: '01-05-2017 00:00:00', startTimestamp: 1493596800000, endDate: '01-06-2017 00:00:00', endTimestamp: 1496275200000},
    "Jun 2017": {month: "Jun 2017", startDate: '01-06-2017 00:00:00', startTimestamp: 1496275200000, endDate: '01-07-2017 00:00:00', endTimestamp: 1498867200000},
    "Jul 2017": {month: "Jul 2017", startDate: '01-07-2017 00:00:00', startTimestamp: 1498867200000, endDate: '01-08-2017 00:00:00', endTimestamp: 1501545600000},
    "Aug 2017": {month: "Aug 2017", startDate: '01-08-2017 00:00:00', startTimestamp: 1501545600000, endDate: '01-09-2017 00:00:00', endTimestamp: 1504224000000},
    "Sep 2017": {month: "Sep 2017", startDate: '01-09-2017 00:00:00', startTimestamp: 1504224000000, endDate: '01-10-2017 00:00:00', endTimestamp: 1506816000000},
    "Oct 2017": {month: "Oct 2017", startDate: '01-10-2017 00:00:00', startTimestamp: 1506816000000, endDate: '01-11-2017 00:00:00', endTimestamp: 1509494400000},
    "Nov 2017": {month: "Nov 2017", startDate: '01-11-2017 00:00:00', startTimestamp: 1509494400000, endDate: '01-12-2017 00:00:00', endTimestamp: 1512086400000},
    "Dec 2017": {month: "Dec 2017", startDate: '01-12-2017 00:00:00', startTimestamp: 1512086400000, endDate: '01-01-2018 00:00:00', endTimestamp: 1514764800000},
    "Jan 2018": {month: "Jan 2018", startDate: '01-01-2018 00:00:00', startTimestamp: 1514764800000, endDate: '01-02-2018 00:00:00', endTimestamp: 1517443200000},
    "Feb 2018": {month: "Feb 2018", startDate: '01-02-2018 00:00:00', startTimestamp: 1517443200000, endDate: '01-03-2018 00:00:00', endTimestamp: 1519862400000},
    "Mar 2018": {month: "Mar 2018", startDate: '01-03-2018 00:00:00', startTimestamp: 1519862400000, endDate: '01-04-2018 00:00:00', endTimestamp: 1522540800000},
    "Apr 2018": {month: "Apr 2018", startDate: '01-04-2018 00:00:00', startTimestamp: 1522540800000, endDate: '01-05-2018 00:00:00', endTimestamp: 1525132800000},
    "May 2018": {month: "May 2018", startDate: '01-05-2018 00:00:00', startTimestamp: 1525132800000, endDate: '01-06-2018 00:00:00', endTimestamp: 1527811200000},
    "Jun 2018": {month: "Jun 2018", startDate: '01-06-2018 00:00:00', startTimestamp: 1527811200000, endDate: '01-07-2018 00:00:00', endTimestamp: 1530403200000},
    "Jul 2018": {month: "Jul 2018", startDate: '01-07-2018 00:00:00', startTimestamp: 1530403200000, endDate: '01-08-2018 00:00:00', endTimestamp: 1533081600000},
    "Aug 2018": {month: "Aug 2018", startDate: '01-08-2018 00:00:00', startTimestamp: 1533081600000, endDate: '01-09-2018 00:00:00', endTimestamp: 1535760000000},
    "Sep 2018": {month: "Sep 2018", startDate: '01-09-2018 00:00:00', startTimestamp: 1535760000000, endDate: '01-10-2018 00:00:00', endTimestamp: 1538352000000},
    "Oct 2018": {month: "Oct 2018", startDate: '01-10-2018 00:00:00', startTimestamp: 1538352000000, endDate: '01-11-2018 00:00:00', endTimestamp: 1541030400000},
    "Nov 2018": {month: "Nov 2018", startDate: '01-11-2018 00:00:00', startTimestamp: 1541030400000, endDate: '01-12-2018 00:00:00', endTimestamp: 1543622400000},
    "Dec 2018": {month: "Dec 2018", startDate: '01-12-2018 00:00:00', startTimestamp: 1543622400000, endDate: '01-01-2019 00:00:00', endTimestamp: 1546300800000}
};
var monthlyBoundKeys = ["May 2016", "Jun 2016", "Jul 2016", "Aug 2016", "Sep 2016", "Oct 2016", "Nov 2016", "Dec 2016", "Jan 2017", "Feb 2017", 
                        "Mar 2017", "Apr 2017", "May 2017", "Jun 2017", "Jul 2017", "Aug 2017", "Sep 2017", "Oct 2017", "Nov 2017", "Dec 2017",
                        "Jan 2018", "Feb 2018", "Mar 2018", "Apr 2018", "May 2018", "Jun 2018",
                        "Jul 2018", "Aug 2018", "Sep 2018", "Oct 2018", "Nov 2018", "Dec 2018"];



function getPermissions(req, res){
    web.checkPermission("bint", req.gid, function (allowed) {
        if(!allowed){
            res.status(403).json({"code": -1});
        }else{
            res.json({code: 0, permissions: allowed});
        }
    });
}

function getAllStats(req, res){
    web.checkPermission("bint", req.gid, function (allowed) {
        if(!allowed || allowed.write == 0){
            res.status(403).json({"code": -1});
        }else{
            var lookup = {"$lookup": {from: "kpivalues", localField: "key", foreignField: "kpiGeneratorKey", as: "kpivalues"}};
            db.kpiGenerators.aggregate([lookup]).toArray(function (err, rows) {
                if(err){
                    res.json({code: -1, error: err});
                }else{
                    res.json({code: 0, list: rows});
                }
            });
        }
    });
}


function rawQuery(req, res){
    web.checkPermission("bint", req.gid, function (allowed) {
        if(!allowed || allowed.write == 0){
            res.status(403).json({"code": -1});
        }else{
            var query = req.query.query;
            db.oracle.query(query, function(err, result) {
                if (err) {
                    res.json({code: -1});
                } else {
                    res.json({code: 0, results: result.rows});
                }
            });
        }
    });
}

function saveKpiDefinition(req, res){
    web.checkPermission("bint", req.gid, function (allowed) {
        if(!allowed || allowed.write == 0){
            res.status(403).json({"code": -1});
        }else{
            if(req.fields.oldKey && req.fields.oldKey != req.fields.key){
                res.status(400).json({
                    "code": -1,
                    "status": -1,
                    "error": "Changing key is not allowed"
                });
            }else if(!req.fields.query){
                res.status(400).json({
                    "code": -1,
                    "status": -1,
                    "error": "Query missing"
                });
            }else{
                var query = req.fields.query.replace(/[\r\n]/g, ' ');
                var result = validateQuery(req.fields.type, query);
                if(result === true){
                    var newKpiDefinition = {
                        key: req.fields.key,
                        name: req.fields.name,
                        description: req.fields.description,
                        type: req.fields.type,
                        dataType: req.fields.dataType,
                        query: query.replace(/"/g, "'")
                    };
                    var id = req.fields._id ? ObjectId(req.fields._id) : new ObjectId();
                    db.kpiDefinitions.update({_id: id}, {$set: newKpiDefinition}, {upsert: true}, function(err, result){
                        if(err){
                            res.status(400).json({
                                "code": -1,
                                "status": -1,
                                "error": "Failed to save KPI def"
                            });
                        }else{
                            res.json({code: 0});
                        }
                    });
                } else {
                    res.status(400).json({
                        "code": -1,
                        "status": -1,
                        "error": result
                    });
                }
            }
        }
    });
}

function validateQuery(type, query){
    var result;
    switch(type){
        case "manual":
            result = true;
            break;
        case "query":
            if(!query.startsWith("select") && !query.startsWith("SELECT")){
                result = "Query should start with 'select'";
            }else if(query.indexOf(";") != -1){
                result = "Query should not contain semicolons";
            }else{
                result = true;
            }
            break;
        case "derive":
            result = true;
            break;
        case "mariaDB":
            if(!query.startsWith("select") && !query.startsWith("SELECT")){
                result = "Query should start with 'select'";
            }else if(query.indexOf(";") != -1){
                result = "Query should not contain semicolons";
            }else{
                result = true;
            }
            break;
        case "mongoDB":
            var items = query.split(".");
            if(!items[2].startsWith("count")){
                result = "Query should contain a count";
            }else if(query.indexOf(";") != -1){
                result = "Query should not contain semicolons";
            }else{
                result = true;
            }
            break;
    }
    return result;
}

function saveKpiGenerator(req, res){
    web.checkPermission("bint", req.gid, function (allowed) {
        if(!allowed || allowed.write == 0){
            res.status(403).json({"code": -1});
        }else{
            if(req.fields.oldKey && req.fields.oldKey != req.fields.key){
                res.status(400).json({
                    "code": -1,
                    "status": -1,
                    "error": "Changing key is not allowed"
                });
            }else{
                var newKpiGenerator = {
                    key: req.fields.key,
                    definitionKey: req.fields.definitionKey,
                    name: req.fields.name,
                    period: req.fields.period,
                    status: 'completed',
                    upperBound: req.fields.upperBound,
                    lowerBound: req.fields.lowerBound
                };
                var id = req.fields._id ? ObjectId(req.fields._id) : new ObjectId();
                db.kpiGenerators.update({_id: id}, {$set: newKpiGenerator}, {upsert: true}, function(err, result){
                    if(err){
                        res.status(400).json({
                            "code": -1,
                            "status": -1,
                            "error": "Failed to save KPI generator"
                        });
                    }else{
                        res.json({code: 0});
                    }
                });
            }
        }
    });
}

function saveKpiView(req, res){
    web.checkPermission("bint", req.gid, function (allowed) {
        if(!allowed || allowed.write == 0){
            res.status(403).json({"code": -1});
        }else if(singleValueViewTypes.indexOf(req.fields.viewType) != -1 && req.fields.timeline != "alltime"){
            res.status(400).json({
                "code": -1,
                "status": -1,
                "error": req.fields.viewType + " view can't be used for " + req.fields.timeline + " values"
            });
        } else if(singleValueViewTypes.indexOf(req.fields.viewType) == -1 && req.fields.timeline == "alltime"){
            res.status(400).json({
                "code": -1,
                "status": -1,
                "error": req.fields.viewType + " view can't be used for " + req.fields.timeline + " values"
            });
        } else {
            var newKpiView = {
                key: req.fields.key,
                name: req.fields.name,
                description: req.fields.description,
                tab: req.fields.tab,
                viewType: req.fields.viewType,
                timeline: req.fields.timeline,
                favourite: (req.fields.tab == "favourites") ? req.user : "none"
            };
            if(!req.fields.updating){
                newKpiView.items = [];
            }
            var id = req.fields._id ? ObjectId(req.fields._id) : new ObjectId();
            db.kpiViews.update({_id: id}, {$set: newKpiView}, {upsert: true}, function(err, result){
                if(err){
                    res.status(400).json({
                        "code": -1,
                        "status": -1,
                        "error": "Failed to save KPI view"
                    });
                }else{
                    res.json({code: 0});
                }
            });
        }
    });
}

function getKpiDefinitions(req, res){
    web.checkPermission("bint", req.gid, function (allowed) {
        if(!allowed){
            res.status(403).json({"code": -1});
        }else{
            db.kpiDefinitions.find({}).toArray(function(err, result){
                if(err){
                    res.json({code: -1, error: err});
                }else{
                    res.json({code: 0, list: result});
                }
            });
        }
    });
}

function getKpiGenerators(req, res){
    web.checkPermission("bint", req.gid, function (allowed) {
        if(!allowed){
            res.status(403).json({"code": -1});
        }else{
            async.parallel([
                function(callback){
                    db.kpiDefinitions.find({"type" : "manual"}).toArray(function(err, result){
                        if(err){
                            callback({code: -1, error: err});
                        }else{
                            var manualDefs = [];
                            if(result.length > 0){
                                result.forEach(function(item){
                                    manualDefs.push(item.key);
                                });
                            }
                            callback(null, manualDefs);
                        }
                    });
            }, function(callback){
                    db.kpiGenerators.find({}).toArray(function(err, result){
                        if(err){
                            callback({code: -1, error: err});
                        }else{
                            callback(null, result);
                        }
                    });
            }], function(err, results){
                if(err){
                    res.json({code: -1, error: err});
                }else{
                    var finalList = [];
                    results[1].forEach(function(item){
                        if(results[0].indexOf(item.definitionKey) != -1){
                            item.manual = true;
                        }else{
                            item.manual = false;
                        }
                        finalList.push(item);
                    });
                    res.json({code: 0, list: finalList});
                }
            });
        }
    });
}

function getKpiViews(req, res){
    web.checkPermission("bint", req.gid, function (allowed) {
        if(!allowed){
            res.status(403).json({"code": -1});
        }else{
            db.kpiViews.find({ $or: [{favourite: req.user}, {favourite: "none"}] }).toArray(function(err, result){
                if(err){
                    res.json({code: -1, error: err});
                }else{
                    res.json({code: 0, list: result});
                }
            });
        }
    });
}

function listKpiViewItems(req, res){
    web.checkPermission("bint", req.gid, function (allowed) {
        if(!allowed){
            res.status(403).json({"code": -1});
        }else{
            var key = req.query.key;
            db.kpiViews.findOne({key: key}, function(err, result){
                if(err){
                    res.json({code: -1, error: err});
                }else{
                    list = [];
                    result.items.forEach(function (item) {
                        item.viewKey = key;
                        list.push(item);
                    });
                    res.json({code: 0, list: list});
                }
            });
        }
    });
}

function getKpiViewsWithValues(req, res){
    web.checkPermission("bint", req.gid, function (allowed) {
        if(!allowed){
            res.status(403).json({"code": -1});
        }else{
            var match = {$match: { $or: [{favourite: req.user}, {favourite: "none"}] }};
            var unwind = {"$unwind": "$items"};
            var lookup = {"$lookup": { from: "kpigenerators", localField: "items.genKey", foreignField: "key", as: "kpiGenerator"}};
            var lookup2 = {"$lookup": { from: "kpivalues", localField: "items.genKey", foreignField: "kpiGeneratorKey", as: "kpiValue"}};
            var unwind2 = {"$unwind": "$kpiValue"};
            var sort = {"$sort": {"kpiValue.startDate": 1}};
            var group = { "$group": {"_id" : {"key":"$key", "name": "$name", "description": "$description", "tab":"$tab", "favourite": "$favourite" ,"viewType":"$viewType", "genKey": "$items.genKey", "rank": "$items.rank", "kpiGenName": "$kpiGenerator.name", "timeline": "$timeline"},
            "values": {"$push": {"startDate": "$kpiValue.startDate", "endDate": "$kpiValue.endDate", "value": "$kpiValue.value", "dataType": "$kpiValue.dataType", "upperBound": "$kpiGenerator.upperBound", "lowerBound": "$kpiGenerator.lowerBound"}}}};
            var sort2 = {"$sort": {"_id.rank": 1}};
            var group2 = {"$group": {"_id": {"key": "$_id.key", "name": "$_id.name", "description": "$_id.description", "tab":"$_id.tab", "timeline": "$_id.timeline","viewType":"$_id.viewType", "favourite": "$_id.favourite"}, "items": {"$push": {"genKey": "$_id.genKey", "rank": "$_id.rank", "kpiGenName": "$_id.kpiGenName", "values": "$values"}}}};
            db.kpiViews.aggregate([match, unwind, lookup, lookup2, unwind2, sort, group, sort2, group2], function (err, rows){
                if(err){
                    res.json({code: -1, error: err});
                }else{
                    res.json({code: 0, list: rows});
                }
            });
        }
    });
}

function savekpiToView(req, res){
    web.checkPermission("bint", req.gid, function (allowed) {
        if(!allowed || allowed.write == 0){
            res.status(403).json({"code": -1});
        }else{
            if(req.fields.displayType == "alltime" && req.fields.list == "false"){
                setQuery = {$set: {items: [{genKey: req.fields.generator, rank: parseInt(req.fields.rank)}]}};
            }else{
                setQuery = {$addToSet: {items: {genKey: req.fields.generator, rank: parseInt(req.fields.rank)}}};
            }
            db.kpiViews.update({key: req.fields.key}, setQuery, {upsert: false}, function(err, result){
                if(err){
                    res.status(400).json({
                        "code": -1,
                        "status": -1,
                        "error": "Failed to delete KPI generator"
                    });
                }else{
                    res.json({code: 0});
                }
            });
        }
    });
}

function deleteKpiDefinition(req, res){
    web.checkPermission("bint", req.gid, function (allowed) {
        if(!allowed || allowed.write == 0){
            res.status(403).json({"code": -1});
        }else{
            db.kpiGenerators.find({definitionKey: req.fields.key}).toArray(function(err, result){
                if(result.length > 0){
                    res.status(400).json({
                        "code": -1,
                        "status": -1,
                        "error": "KPI definition used by other generators"
                    });
                }else{
                    db.kpiDefinitions.remove({_id: ObjectId(req.fields._id)}, function(err, result){
                        if(err){
                            res.status(400).json({
                                "code": -1,
                                "status": -1,
                                "error": "Failed to delete KPI definition"
                            });
                        }else{
                            res.json({code: 0});
                        }
                    });
                }
            });
        }
    });
}


function deleteKpiGenFromView(req, res){
    web.checkPermission("bint", req.gid, function (allowed) {
        if(!allowed || allowed.write == 0){
            res.status(403).json({"code": -1});
        }else{
            db.kpiViews.update({key: req.fields.viewKey}, {$pull: {"items": {"genKey": req.fields.genKey, "rank": parseInt(req.fields.rank)}}}, {upsert: false}, function(err, result){
                if(err){
                    res.status(400).json({
                        "code": -1,
                        "status": -1,
                        "error": "Failed to delete KPI generator"
                    });
                }else{
                    res.json({code: 0});
                }
            });
        }
    });
}

function deleteKpiGenerator(req, res){
    web.checkPermission("bint", req.gid, function (allowed) {
        if(!allowed || allowed.write == 0){
            res.status(403).json({"code": -1});
        }else{
            var key = req.fields.key;
            db.kpiViews.findOne({items: {$elemMatch: {genKey: key}}}, function(err, kpiView){
                if(kpiView){
                    res.status(400).json({
                        "code": -1,
                        "status": -1,
                        "error": "Failed to delete KPI generator. Its been used by a view"
                    });
                }else{
                    db.kpiGenerators.remove({_id: ObjectId(req.fields._id)}, function(err, result){
                        if(err){
                            res.status(400).json({
                                "code": -1,
                                "status": -1,
                                "error": "Failed to delete KPI generator"
                            });
                            res.json({code: -1, error: err});
                        }else{
                            db.kpiValues.remove({"kpiGeneratorKey": key}, function(err, result){
                                if(err){
                                    res.status(400).json({
                                        "code": -1,
                                        "status": -1,
                                        "error": "Failed to delete KPI values for generator"
                                    });
                                    res.json({code: -1, error: err});
                                }else{
                                    res.json({code: 0});
                                }
                            });
                        }
                    });
                }
            });
        }
    });
}

function deleteKpiView(req, res){
    web.checkPermission("bint", req.gid, function (allowed) {
        if(!allowed || allowed.write == 0){
            res.status(403).json({"code": -1});
        }else{
            db.kpiViews.remove({_id: ObjectId(req.fields._id)}, function(err, result){
                if(err){
                    res.status(400).json({
                        "code": -1,
                        "status": -1,
                        "error": "Failed to delete KPI generator"
                    });
                }else{
                    res.json({code: 0});
                }
            });
        }
    });
}

function generateKpiValues(type, queryType, callback){
    if(queryType == "query"){
        var types = ["query", "mariaDB", "mongoDB"];
    }else{
        var types = ["derive"];
    }
    var dateObj = new Date();
    var match1 = { "$match" : { "period" : type } };
    var match2 = { "$match" : { "kpidefinition.type" : {"$in": types} } };
    var lookup = {"$lookup": { from: "kpidefinitions", localField: "definitionKey", foreignField: "key", as: "kpidefinition"}};
    var errorList = [];

    db.kpiGenerators.aggregate([match1, lookup, match2]).toArray(function (err, rows) {
        async.each(rows, function(row, callback){
            var timeLimits = getTimeLimits(row.period, dateObj);
            var queryCallback = function(err, val){
                if(err){
                    errorList.push({code: -1, error: err, message: "Error running query for " + row.key});
                }
                cleanKpiValues(row.key, row.period, timeLimits.startTimestamp, timeLimits.endTimestamp, function(err, results){
                    if(err){
                        errorList.push({code: -1, error: err, message: "Error deleting kpi value for " + row.key});
                        callback(null, {code: -1, error: err, message: "Error deleting kpi value for " + row.key});
                    }else{
                        saveKpiValue(row.key, val, row.period, timeLimits.currentTime, timeLimits.startTimestamp, timeLimits.endTimestamp, row.kpidefinition[0].dataType, function(err, result){
                            if(err){
                                errorList.push({code: -1, error: err, message: "Error saving kpi value for " + row.key});
                                callback(null, {code: -1, error: err, message: "Error saving kpi value for " + row.key});
                            }else{
                                callback(null, {code: 0});
                            }
                        });
                    }
                });
            }
            if(row.kpidefinition[0].type == "query"){
                runOracleQuery(row.kpidefinition[0].query, timeLimits.startDate, timeLimits.endDate, dateObj.getMonth(), queryCallback);
            }else if(row.kpidefinition[0].type == "mariaDB"){
                runMariaDbQuery(row.kpidefinition[0].query, timeLimits.startDate, timeLimits.endDate, dateObj.getMonth(), queryCallback);
            }else if(row.kpidefinition[0].type == "mongoDB"){
                runMongoQuery(row.kpidefinition[0].query, timeLimits.startTimestamp, (timeLimits.endTimestamp), dateObj.getMonth(), queryCallback);
            }else if(row.kpidefinition[0].type == "manual"){
                queryCallback(null, 0);
            }else{
                runMathExpression(row.kpidefinition[0].query, row.period, timeLimits.startTimestamp, timeLimits.endTimestamp, queryCallback);
            }
        }, function(err){
            if(err){
                callback(null, {code: -1, error: err, errorList: errorList});
            }else{
                callback(null, {code: 0, errorList: errorList});
            }
        });
    });
}

function generateKpiValuesFromApi(req, res){
    web.checkPermission("bint", req.gid, function (allowed) {
        if(!allowed || allowed.write == 0){
            res.status(403).json({"code": -1});
        }else{
            generateKpiValues(req.fields.type, req.fields.queryType, function(err, result){
                res.json(result);
            });
        }
    });
}

function generateKpiValuesFromSchedulerEvent(event, eventCallback){
    async.series([function(callback){
        generateKpiValues(event.bi_type, "query", function(err, result){
            callback(err, result);
        });
    }, function(callback){
        generateKpiValues(event.bi_type, "derive", function(err, result){
            callback(err, result);
        });
    }], function(err, data){
        var erros = false;
        data.forEach(function (record) {
            record.errorList.forEach(function (error) {
                erros = true;
                db.weblog.insert({
                    "ts": event.start,
                    "ip": "N/A",
                    "type": "biJob_" + event.bi_type,
                    "action": "generateBi",
                    "username": "N/A",
                    "result": "Failed",
                    "code": -1,
                    "error": error.message
                });
            });
        });
        if(erros){
            eventCallback({message: "error occoured when generating kpi values"}, false);
        }else{
            eventCallback(null, true);
        }
    });
}

function getKpiGenKeys(req, res){
    web.checkPermission("bint", req.gid, function (allowed) {
        if(!allowed){
            res.status(403).json({"code": -1});
        }else{
            var match = { "$match" : { "status" : "completed" } };
            var group = { "$group": {"_id" : "$period" , "values": {"$push": "$key"}}};
            db.kpiGenerators.aggregate([match, group], function (err, rows){
                if(err){
                    res.json({code: -1, error: err});
                }else{
                    res.json({code: 0, list: rows});
                }
            });
        }
    });
}

function regenerate(req, res){
    web.checkPermission("bint", req.gid, function (allowed) {
        if(!allowed || allowed.write == 0){
            res.status(403).json({"code": -1});
        }else{
            var kpiGenerator;
            var finishedKpiGenerator;
            var lastMonthOnly = req.query.lastMonthOnly;
            db.kpiGenerators.findAndModify({key: req.query.key, status: 'completed'}, [], {"$set": {status: 'pending'}}, {new: 'true'}, function(err, result1){
                kpiGenerator = result1.value;
                if(kpiGenerator){
                    var finishCallback = function(errorList){
                        db.kpiGenerators.findAndModify({key: kpiGenerator.key, status: 'pending'}, [], {$set: {status: 'completed'}}, {new: 'true'}, function(err, result2){
                            finishedKpiGenerator = result1.value;
                            if(err){
                                errorList.push('Failed updating kpi generator status');
                            }
                            if(errorList.length > 0){
                                res.json({code: -1, error: err, errorList: errorList});
                            }else{
                                res.json({code: 0, errorList: errorList});
                            }
                        });
                    }
                    db.kpiDefinitions.findOne({key: kpiGenerator.definitionKey}, function(err, kpiDefinition){
                        if(err){
                            res.json({code: -1, error: err});
                        }else{
                            switch(kpiGenerator.period){
                                case 'alltime':
                                    regenerateForAlltime(kpiGenerator, kpiDefinition, finishCallback);
                                    break;
                                case 'annually':
                                    break;
                                case 'monthly':
                                    regenerateForMonthly(kpiGenerator, kpiDefinition, lastMonthOnly, finishCallback);
                                    break;
                                case 'daily':
                                    break;
                                case 'hourly':
                                    break;
                            }
                        }
                    });
                }else{
                    res.json({code: -1, error: err, errorList: ['KPI definition is not avaialbe or its already been generated']});
                }
            });
        }
    });
}

function regenerateForAlltime(kpiGenerator, kpiDefinition, cb){
    var period = kpiGenerator.period;
    var dateObj = new Date();
    var timeLimits = getTimeLimits(kpiGenerator.period, dateObj);
    var match1 = { "$match" : { "period" : period } };
    var match2 = { "$match" : { "kpidefinition.type" : "query" } };
    var lookup = {"$lookup": { from: "kpidefinitions", localField: "definitionKey", foreignField: "key", as: "kpidefinition"}};
    var errorList = [];
    var queryCallback = function(err, val){
        if(err){
            errorList.push({code: -1, error: err, message: "Error running query for " + kpiGenerator.key});
        }
        cleanKpiValues(kpiGenerator.key, kpiGenerator.period, timeLimits.startTimestamp, timeLimits.endTimestamp, function(err, results){
            if(err){
                errorList.push({code: -1, error: err, message: "Error deleting kpi value for " + kpiGenerator.key});
                cb(errorList);
            }else{
                saveKpiValue(kpiGenerator.key, val, kpiGenerator.period, timeLimits.currentTime, timeLimits.startTimestamp, timeLimits.endTimestamp, kpiDefinition.dataType, function(err, result){
                    if(err){
                        errorList.push({code: -1, error: err, message: "Error saving kpi value for " + kpiGenerator.key});
                        cb(errorList);
                    }else{
                        cb(errorList);
                    }
                });
            }
        });
    }
    if(kpiDefinition.type == "query"){
        runOracleQuery(kpiDefinition.query, timeLimits.startDate, timeLimits.endDate, dateObj.getMonth(), queryCallback);
    }else if(kpiDefinition.type == "mariaDB"){
        runMariaDbQuery(kpiDefinition.query, timeLimits.startDate, timeLimits.endDate, dateObj.getMonth(), queryCallback);
    }else if(kpiDefinition.type == "mongoDB"){
        runMongoQuery(kpiDefinition.query, timeLimits.startTimestamp, timeLimits.endTimestamp, dateObj.getMonth(), queryCallback);
    }else if(kpiDefinition.type == "manual"){
        queryCallback(null, 0);
    }else{
        runMathExpression(kpiDefinition.query, "alltime", timeLimits.startTimestamp, timeLimits.endTimestamp, queryCallback);
    }
}

function regenerateForAnnually(kpiGenerator, kpiDefinition, cb){

}

function regenerateForMonthly(kpiGenerator, kpiDefinition, lastMonthOnly, cb){
    var dateObj = new Date();
    var currentTime = dateObj.getTime();
    var currentMonth = dateObj.getMonth();
    var currentYear = dateObj.getFullYear();
    var monthString = (currentMonth == 0) ? months[11] + " " + (currentYear - 1) : months[currentMonth-1] + " " + currentYear;
    var boundStartIndex = lastMonthOnly ? monthlyBoundKeys.indexOf(monthString) : 0;
    var monthlyBoundsTillNow = monthlyBoundKeys.slice(boundStartIndex, monthlyBoundKeys.indexOf(monthString) + 1);
    var errorList = [];
    async.each(monthlyBoundsTillNow, function(monthlyBoundKey, callback){
        var timeLimits = monthlyBounds[monthlyBoundKey];
        var monthIndex = (months.indexOf(monthlyBoundKey.split(" ")[0]) == 11) ? 0 : months.indexOf(monthlyBoundKey.split(" ")[0]) + 1;
        var queryCallback = function(err, val){
            if(err){
                errorList.push({code: -1, error: err, message: "Error running query for " + kpiGenerator.key + " between " + timeLimits.startDate + " and " +  timeLimits.endDate});
            }
            cleanKpiValues(kpiGenerator.key, kpiGenerator.period, timeLimits.startTimestamp, timeLimits.endTimestamp, function(err, results){
                if(err){
                    errorList.push({code: -1, error: err, message: "Error deleting kpi value for " + row.key});
                    callback(null, {code: -1, error: err, message: "Error deleting kpi value for " + row.key});
                }else{
                    saveKpiValue(kpiGenerator.key, val, kpiGenerator.period, currentTime, timeLimits.startTimestamp, timeLimits.endTimestamp, kpiDefinition.dataType, function(err, result){
                        if(err){
                            errorList.push({code: -1, error: err, message: "Error saving kpi value for " + row.key});
                            callback(null, {code: -1, error: err, message: "Error saving kpi value for " + row.key});
                        }else{
                            callback(null, {code: 0});
                        }
                    });
                }
            });
        }
        if(kpiDefinition.type == "query"){
            runOracleQuery(kpiDefinition.query, timeLimits.startDate, timeLimits.endDate, monthIndex, queryCallback);
        }else if(kpiDefinition.type == "mariaDB"){
            runMariaDbQuery(kpiDefinition.query, timeLimits.startDate, timeLimits.endDate, monthIndex, queryCallback);
        }else if(kpiDefinition.type == "mongoDB"){
            runMongoQuery(kpiDefinition.query, timeLimits.startTimestamp, timeLimits.endTimestamp, monthIndex, queryCallback);
        }else if(kpiDefinition.type == "manual"){
            queryCallback(null, 0);
        }else{
            runMathExpression(kpiDefinition.query, "monthly", timeLimits.startTimestamp, timeLimits.endTimestamp, queryCallback);
        }
    }, function(err){
        cb(errorList);
    });
}

function regenerateForDaily(kpiGenerator, kpiDefinition, cb){

}

function regenerateForHourly(kpiGenerator, kpiDefinition, cb){
    
}

function cleanKpiValues(generatorKey, period, startTimestamp, endTimestamp, callback){
    var deleteQuery = {};
    switch(period){
        case 'alltime':
            deleteQuery = {kpiGeneratorKey: generatorKey, period: period};
            break;
        case 'annually':
            break;
        case 'monthly':
            deleteQuery = {kpiGeneratorKey: generatorKey, period: period, startDate: startTimestamp, endDate: endTimestamp};
            break;
        case 'weekly':
            deleteQuery = {kpiGeneratorKey: generatorKey, period: period, startDate: startTimestamp, endDate: endTimestamp};
            break;
        case 'daily':
            deleteQuery = {kpiGeneratorKey: generatorKey, period: period, startDate: startTimestamp, endDate: endTimestamp};
            break;
        case 'hourly':
            break;
    }
    db.kpiValues.remove(deleteQuery, callback);
}


 function getTimeLimits(type, dateObj){
    switch(type){
        case 'alltime':
            timeLimits = {};
            timeLimits.currentTime = dateObj.getTime();
            timeLimits.currentTimeFormatted = getFormattedDate(timeLimits.currentTime);
            timeLimits.startDate = circlsLifeBirth;
            timeLimits.startTimestamp = circlsLifeBirthTimestamp;
            timeLimits.endDate = timeLimits.currentTimeFormatted;
            timeLimits.endTimestamp = timeLimits.currentTime;
            break;
        case 'annually':
            break;
        case 'monthly':
            var timeLimits = {};
            var currentMonth = dateObj.getMonth();
            var currentYear = dateObj.getFullYear();
            var monthString = (currentMonth == 0) ? months[11] + " " + (currentYear - 1) : months[currentMonth-1] + " " + currentYear;
            var timeLimits = monthlyBounds[monthString];
            timeLimits.currentTime = dateObj.getTime();
            timeLimits.currentTimeFormatted = getFormattedDate(timeLimits.currentTime);
            break;
        case 'weekly':
            var timeLimits = {};
            dateObj.setHours(0,0,0,0);
            var currentWeekStartTime = dateObj.getTime() - ((dateObj.getDay() - 1) * 3600000*24);
            var beforeWeekStartTime = currentWeekStartTime - (3600000 * 24 * 7);
            timeLimits.startTimestamp = beforeWeekStartTime;
            timeLimits.endTimestamp = currentWeekStartTime;
            timeLimits.startDate = common.getDateDayFirst(new Date(timeLimits.startTimestamp)) + " " + common.getTimeString(new Date(timeLimits.startTimestamp));
            timeLimits.endDate = common.getDateDayFirst(new Date(timeLimits.endTimestamp)) + " " + common.getTimeString(new Date(timeLimits.endTimestamp));
            timeLimits.currentTime = dateObj.getTime();
            timeLimits.currentTimeFormatted = getFormattedDate(timeLimits.currentTime);
            break;
        case 'daily':
            var timeLimits = {};
            dateObj.setHours(0,0,0,0);
            var currentDayStartTime = dateObj.getTime();
            var beforeDayStartTime = currentDayStartTime - 3600000 * 24;
            timeLimits.startTimestamp = beforeDayStartTime;
            timeLimits.endTimestamp = currentDayStartTime;
            timeLimits.startDate = common.getDateDayFirst(new Date(timeLimits.startTimestamp)) + " " + common.getTimeString(new Date(timeLimits.startTimestamp));
            timeLimits.endDate = common.getDateDayFirst(new Date(timeLimits.endTimestamp)) + " " + common.getTimeString(new Date(timeLimits.endTimestamp));
            timeLimits.currentTime = dateObj.getTime();
            timeLimits.currentTimeFormatted = getFormattedDate(timeLimits.currentTime);
            break;
        case 'hourly':
            break;            
    }
    return timeLimits;
}

function getFormattedDate(timestamp) {
    var date = new Date(timestamp);

    var month = date.getMonth() + 1;
    var day = date.getDate();
    var hour = date.getHours();
    var min = date.getMinutes();
    var sec = date.getSeconds();

    month = (month < 10 ? "0" : "") + month;
    day = (day < 10 ? "0" : "") + day;
    hour = (hour < 10 ? "0" : "") + hour;
    min = (min < 10 ? "0" : "") + min;
    sec = (sec < 10 ? "0" : "") + sec;

    var str = day + "-" + month + "-" + date.getFullYear() + " " + hour + ":" + min + ":" + sec;
    return str;
}

function runOracleQuery(query, startDate, endDate, monthIndex, callback){
    var finalQuery = query.split('{startDate}').join(startDate).split('{endDate}').join(endDate).split('{billdatesTableName}').join(billdatesTablePrefix + months[monthIndex]);
    db.oracle.query(finalQuery, function(err, result){
        if(err){
            callback(err, 0);
        }
        else if(result.rows && result.rows[0] && result.rows[0][0]){
            callback(null, result.rows[0][0]);
        }else{
            callback(null, 0);
        }
    });
}

function runMariaDbQuery(query, startDate, endDate, monthIndex, callback){
    var finalQuery = query.split('{startDate}').join(startDate).split('{endDate}').join(endDate);
    db.query_noerr(query, function(result) {
		if(!result){
            callback(null, 0);
        }else{
            callback(null, result[0]["count"] ? result[0]["count"] : 0);
        }
	});
}

function runMongoQuery(query, startDate, endDate, monthIndex, callback){
    var collectionName = query.split(".")[1];
    var temp = query.split(".count(")[1];
    temp = temp.split(")")[0];
    var queryAsString = temp.split('{startDate}').join(startDate).split('{endDate}').join(endDate);
    var finalQuery = common.safeParse(queryAsString.replace(/'/g, '"'));
    if(db[collectionName]){
        db[collectionName].count(finalQuery, function(err, count){
            callback(err, count ? count : 0);
        });
    }else{
        callback(null, 0);
    }
}

function runMathExpression(query, period, startTimestamp, endTimestamp, callback){
    var queryKeys = getQueryKeys(query);
    db.kpiGenerators.find({period: period, definitionKey: {$in: queryKeys}}, {key:1, definitionKey: 1, _id:0}).toArray(function(err, result){
        if(err || result.length == 0 || queryKeys.length != result.length){
           callback(err, 0); 
        }else{
            var kpiGenKeys = [];
            var newQuery = query;
            result.forEach(function (item) {
                kpiGenKeys.push(item.key);
                newQuery = newQuery.split(item.definitionKey).join(item.key);
            });
            var mongoQuery = {startDate: startTimestamp, kpiGeneratorKey: {$in: kpiGenKeys}}
            if(period != "alltime"){
                mongoQuery.endDate = endTimestamp;
            }
            db.kpiValues.find(mongoQuery).toArray(function(err, result){
                if(err || result.length != queryKeys.length){
                    callback(err, 0); 
                }else{
                    var scope = {};
                    result.forEach(function (item) {
                        scope[item.kpiGeneratorKey] = item.value;
                    });
                    try{
                        var finalValue = math.eval(newQuery, scope);
                        callback(null, isNaN(finalValue) ? 0 : finalValue);
                    }catch(e){
                        callback(e, 0);
                    }
                }
            });
        }
    });
}

function getQueryKeys(query){
    var reg = /[A-Z0-9|_]+/g;
    var output = [];
    do{
        m = reg.exec(query);
        if(m && m[0] && isNaN(m[0])){
            output.push(m[0]);
        }
    }while(m);
    return output;
}

function manualAddKpiValue(req, res){
    web.checkPermission("bint", req.gid, function (allowed) {
        if(!allowed || allowed.write == 0){
            res.status(403).json({"code": -1});
        }else{
            var startDate = new Date(req.fields.startDate).getTime() + 8 * 3600000;
            var endDate = new Date(req.fields.endDate).getTime() + 8 * 3600000;
            db.kpiValues.remove({kpiGeneratorKey: req.fields.generatorKey, startDate : startDate, endDate: endDate}, function(err, result){
                if(err){
                    res.status(400).json({
                        "code": -1,
                        "status": -1,
                        "error": "Failed to remove existing KPI value"
                    });
                }else{
                    db.kpiDefinitions.findOne({key: req.fields.definitionKey}, function(err, result){
                        saveKpiValue(
                            req.fields.generatorKey,
                            req.fields.value,
                            req.fields.period,
                            new Date().getTime(),
                            startDate,
                            endDate,
                            result.dataType,
                            function(err, result){
                                if(err){
                                    res.status(400).json({
                                        "code": -1,
                                        "status": -1,
                                        "error": "Failed to save KPI value"
                                    });
                                }else{
                                    res.json({code: 0});
                                }
                            }
                        );
                    });
                }
            });
        }
    });
}

function saveKpiValue(kpiGeneratorKey, value, period, createdTime, startDate, endDate, dataType, callback){
    var kpiValue = {
        kpiGeneratorKey: kpiGeneratorKey,
        value: roundValue(value),
        period: period,
        createdTime: createdTime,
        startDate : startDate,
        endDate: endDate,
        dataType: dataType
    };
    db.kpiValues.insertOne(kpiValue, function(err, result){
        callback(err, result);
    });
}

function roundValue(value){
    return Math.round(value * 100)/100;
}

function crashTestErroredMethod(event, callback){
    setTimeout(function(){
        if(event.property.someProperty){
            callback(null, true);
        }else{
            callback(null, false);
        }
    }, 4000);
}
