/**
 *
 *  Bill related functions
 *
 */

var async = require('async');
var config = require('../../../config');
var common = require('../../../lib/common');
var db = require('../../../lib/db_handler');
var billHelper = require('../../core/manager/account/pdfBillingManager');
var web = require('./web');
var notificationSend = require(__core + '/manager/notifications/send');
var billManager = require('../../core/manager/account/billManager');
var fs = require('fs');

var LOG = true;
var cacheRootKey = "job_bill_broadcasting";
var cacheRootResultKey = "job_bill_broadcasting_result";
var cacheLifeTime = 10 * 60 * 1000;


exports.getBillsArchive = function getBillsArchive(req, res) {
    web.checkPermission("bills", req.gid, function (allowed) {
        if (!allowed) {
            res.status(403).json({"code": -1});
        } else {
            if (LOG) common.log('BillApi', 'load archives - start');
            var type = req.query.type;
            var displayLength = parseInt(req.query.iDisplayLength);
            var displayStart = parseInt(req.query.iDisplayStart);
            var rootFolder = config.BILLS_PATH + '/' + type + '/';

            billHelper.loadBillsInformation(rootFolder, function (dirs) {
                if (LOG) common.log('BillApi', 'load archives - end');

                var data = []
                for (var i = displayStart; i < (displayStart + displayLength) && i < dirs.length; i++) {
                    data.push(dirs[i]);
                }

                res.json({
                    "code": 0,
                    "write": allowed.write,
                    "data": data,
                    "iTotalRecords": dirs.length,
                    "iTotalDisplayRecords": dirs.length
                });
            })
        }
    });
}

exports.getBillsList = function getBillsList(req, res) {
    web.checkPermission("bills", req.gid, function (allowed) {
        if (!allowed) {
            res.status(403).json({"code": -1});
        } else {
            if (LOG) common.log('BillApi', 'load list - start');
            var archive = req.query.folder;
            var displayLength = parseInt(req.query.iDisplayLength);
            var displayStart = parseInt(req.query.iDisplayStart);

            billHelper.loadAllFilesInformation(archive,
                function (list, invalidCount, totalCount) {
                    if (LOG) common.log('BillApi', 'load list - end');
                    res.json({
                        "code": 0,
                        "write": allowed.write,
                        "data": list,
                        "invalidCount": invalidCount,
                        "iTotalRecords": totalCount,
                        "iTotalDisplayRecords": totalCount
                    });
                },
                function (countLoaded, totalCount, loadedItems, done) {
                    done();
                }, displayStart, displayLength);
        }
    });
}

exports.getBillLogs = function getBillLogs(req, res) {
    web.checkPermission("bills", req.gid, function (allowed) {
        if (!allowed) {
            res.status(403).json({"code": -1});
        } else {
            var search = common.escapeHtml(req.query.search);
            var match = {};

            if (req.query.search) {
                match["$or"] = [
                    {"billingAccount": new RegExp(search, "i")},
                    {"email": new RegExp(search, "i")},
                    {"number": new RegExp(search, "i")},
                    {"filename": new RegExp(search, "i")},
                    {"status": new RegExp(search, "i")}
                ];
            } else {
                match.ts = {
                    "$gt": new Date().getTime() - 1000 * 60 * 60 * 24 * 3
                };
            }

            db.notifications_logbookbill_get(match, {}, {"_id": -1}, function (item) {
                if (item) {
                    res.json({
                        "code": 0,
                        "list": item,
                        "write": allowed.write
                    });
                } else {
                    res.status(404).send('Not Found');
                }
            });
        }
    });
}

exports.sendSingleBill = function sendSingleBill(req, res) {
    web.checkPermission("bills", req.gid, function (allowed) {
        if (!allowed) {
            res.status(403).json({"code": -1});
        } else {
            db.cache_hget("cache", cacheRootKey, function (value) {
                if (value && value["valid"]) {
                    if (LOG) common.log('BillApi', 'can not send bill');
                    return res.status(400).json({
                        "code": -1,
                        "error": "Broadcasting in progress. Please wait unting the end of the operation"
                    });
                }

                var archive = req.fields.folder;
                var filePdf = req.fields.pdf;
                var folder = config.BILLS_PATH + '/' + archive + '/';
                billHelper.loadSingleFileInformation(archive, filePdf, function (fileInfo) {
                    if (!fileInfo) {
                        res.status(400).json({"code": -1, "error": "File is not found or invalid"});
                    } else {
                        sendEmail(req.ip, fileInfo, function (fileInfo) {
                            res.json({
                                "code": 0,
                                "resent": false,
                                "name": fileInfo.name,
                                "number": fileInfo.number,
                                "billingAccount": fileInfo.billingAccount
                            });
                        });
                    }
                });
            });
        }
    });
}

//var countRecreated = 0;
function sendEmail(ip, fileInfo, callback) {
    var obj = {
        teamID: 5,
        teamName: "Operations",
        activity:
            (fileInfo.paymentNegative ? (fileInfo.number ? "bill_payment_negative" : "bill_payment_negative_no_number")
                : (fileInfo.paymentFailed ? (fileInfo.number ? "bill_payment_failure" : "bill_payment_failure_no_number")
                    : (fileInfo.paymentPartial ? (fileInfo.number ? "bill_payment_partially" : "bill_payment_partially_no_number")
                        : (fileInfo.number ? "bill_payment_success" : "bill_payment_success_no_number")))),
        prefix: fileInfo.prefix,
        number: fileInfo.number,
        name: fileInfo.name,
        email: fileInfo.email,
        billingAccount: fileInfo.billingAccount,
        invoicenumber: fileInfo.regNumber,
        month: fileInfo.month,
        fileInfo: fileInfo,
        delay: 4000,
        pdfpassword: fileInfo.pdfpassword,
        attachment: {
            global: true,
            filename: fileInfo.pdf,
            path: fileInfo.path
        }
    };

    var notificationCallback = function (err, result) {

        if (!err) {
            if (obj.transport.indexOf("email") < 0) {
                err = new Error("Email transport is not saved, bill is not sent");
            } else if (obj.attachmentError) {
                err = new Error(obj.attachmentError + ", bill is not sent");
            }
        }

        var billSavedLog = {
            ip: ip,
            notificationId: result ? result.notificationId : undefined,
            statusEmail: result ? result.statusEmail : undefined,
            month: obj.month,
            status: err ? "ERROR" : "OK",
            error: err ? err.message : undefined,
            filename: obj.attachment.filename,
            path: obj.attachment.path,
            number: obj.number,
            email: obj.email,
            name: obj.name,
            account: obj.account,
            transport: obj.transport,
            customerAccountNumber: obj.customerAccountNumber,
            serviceInstanceNumber: obj.serviceInstanceNumber,
            billingAccountNumber: obj.billingAccountNumber,
            billingAccount: obj.billingAccount
        }

        db.notifications_insert("bill", billSavedLog, function () {
            if (LOG) common.log("webHook", "notification added to 'bill' collection, activity="
            + obj.activity + ", log=" + JSON.stringify(billSavedLog));

            callback(fileInfo);
        });
    };

    notificationSend.deliver(obj, null, notificationCallback);
}

exports.sendAllBills = function sendAllBills(req, res) {
    web.checkPermission("bills", req.gid, function (allowed) {
        if (!allowed) {
            res.status(403).json({"code": -1});
        } else {
            db.cache_hget("cache", cacheRootKey, function (value) {
                if (value && value["valid"]) {
                    if (LOG) common.log('BillApi', 'can not broadcast bills');
                    return res.status(400).json({
                        "code": -1,
                        "error": "Broadcasting in progress. Please wait until the end of the operation"
                    });
                }

                var archive = req.query.folder;
                if (LOG) common.log('BillApi', archive + ', load user info list - start');
                db.cache_hput("cache", cacheRootKey, {
                    valid: true,
                    profiles_total_count: 0,
                    profiles_loaded_count: 0,
                    profiles_invalid_count: 0,
                    emails_ignored_count: 0,
                    emails_sent_count: 0
                }, cacheLifeTime);

                var emailsIgnoredCount = 0;
                var emailsSentCount = 0;

                var checkIfFinished = function () {
                    db.cache_hget("cache", cacheRootKey, function (value) {

                        if (LOG) common.log('BillApi', 'cache=' + JSON.stringify(value));

                        if (parseInt(value.profiles_total_count) == parseInt(value.profiles_loaded_count)
                            && parseInt(value.profiles_total_count) == (parseInt(value.emails_sent_count)
                            + parseInt(value.emails_ignored_count)) + parseInt(value.profiles_invalid_count)) {

                            db.cache_del("cache", cacheRootKey);
                            db.cache_hput("cache", cacheRootResultKey, value, 10 * 1000);
                        }
                    });
                }

                res.status(200).json({"code": 0});
                billHelper.loadAllFilesInformation(archive,
                    function (list, invalidCount, totalCount) {
                        if (LOG) common.log('BillApi', archive + ', load user info list - end');

                        db.cache_hput("cache", cacheRootKey, {
                            "profiles_total_count": totalCount,
                            "profiles_loaded_count": list.length,
                            "profiles_invalid_count": invalidCount
                        }, cacheLifeTime);

                        checkIfFinished();
                    },
                    function (countLoaded, totalCount, loadedItems, done) {
                        if (countLoaded % 10 == 0) {
                            if (LOG) common.log('BillApi', 'loading user information, countLoaded=' + countLoaded
                            + ', totalCount=' + totalCount);
                        }

                        db.cache_hput("cache", cacheRootKey, {
                            "profiles_total_count": totalCount,
                            "profiles_loaded_count": countLoaded
                        }, cacheLifeTime);

                        if (!loadedItems || loadedItems.length == 0) {
                            checkIfFinished();
                            done();
                        }

                        function addSentTask(tasks, item) {
                            tasks.push(function (callback) {
                                if (item.sentAt) {
                                    callback(undefined, {ignoredEmail: true, sendEmail: false});
                                } else {
                                    sendEmail(req.ip, item, function () {
                                        callback(undefined, {ignoredEmail: false, sendEmail: true});
                                    });
                                }
                            });
                        };

                        var tasks = [];
                        loadedItems.forEach(function (item) {
                            addSentTask(tasks, item)
                        });

                        async.parallelLimit(tasks, 5, function (err, result) {
                            result.forEach(function (item) {
                                if (item.ignoredEmail) {
                                    emailsIgnoredCount++;
                                    db.cache_hput("cache", cacheRootKey, {"emails_ignored_count": emailsIgnoredCount}, cacheLifeTime);
                                } else if (item.sendEmail) {
                                    emailsSentCount++;
                                    db.cache_hput("cache", cacheRootKey, {"emails_sent_count": emailsSentCount}, cacheLifeTime);
                                }
                            });

                            checkIfFinished();
                            done();
                        });
                    });
            });
        }
    });
}

exports.getBillsBroadcastingStatus = function getBillsBroadcastingStatus(req, res) {
    web.checkPermission("bills", req.gid, function (allowed) {
        if (!allowed) {
            res.status(403).json({"code": -1});
        } else {
            db.cache_hget("cache", cacheRootKey, function (value) {
                var broadcasting = value != null;
                if (value && value["valid"]) {
                    if (LOG) common.log('BillApi', 'bills broadcasting is in progress, totalCount='
                    + value.profiles_total_count + ", loadedCount=" + value.profiles_loaded_count);
                    res.json({
                        "code": 0,
                        "broadcasting": broadcasting,
                        "totalCount": parseInt(value.profiles_total_count),
                        "loadedCount": parseInt(value.profiles_loaded_count),
                        "emailsIgnoredCount": parseInt(value.emails_ignored_count),
                        "emailsSentCount": parseInt(value.emails_sent_count)
                    });
                } else {
                    if (LOG) common.log('BillApi', 'bills broadcasting is NOT in progress');
                    db.cache_hget("cache", cacheRootResultKey, function (value) {
                        if (value && value["valid"]) {
                            if (LOG) common.log('BillApi', 'bills broadcasting result is found');
                            db.cache_del("cache", cacheRootResultKey);
                            res.json({
                                "code": 0,
                                "broadcasting": false,
                                "totalCount": parseInt(value.profiles_total_count),
                                "invalidCount": parseInt(value.profiles_invalid_count),
                                "loadedCount": parseInt(value.profiles_loaded_count),
                                "emailsIgnoredCount": parseInt(value.emails_ignored_count),
                                "emailsSentCount": parseInt(value.emails_sent_count)
                            });
                        } else {
                            res.json({
                                "code": 0,
                                "broadcasting": false
                            });
                        }
                    });
                }
            });
        }
    });
}

exports.getBillFile = function getBillFile(req, res) {
    web.checkPermission("customers", req.gid, function (allowed) {
        if (!allowed) {
            res.status(403).json({"code": -1});
        } else {
            var archive = req.params.archive;
            var pdf = req.params.pdf;
            var type = req.params.type;
            var file = config.BILLS_PATH + '/' + type + '/' + archive + '/' + pdf;

            fs.stat(file, (err, stat) => {
                if (err) {
                    return res.status(400).json({
                        code: -1,
                        error: err.message
                    });
                }

                if (!stat) {
                    return res.status(400).json({
                        code: -1,
                        error: "No File Found"
                    });
                }

                res.writeHead(200, {
                    'Content-Type': 'application/pdf',
                    'Content-Length': stat.size
                });

                var readStream = fs.createReadStream(file);
                readStream.pipe(res);

                if (LOG) common.log('BillApi', 'load file, archive=' + archive + ', pdf=' + pdf);
            });
        }
    });
}

exports.addBlockedBillingAccount = function addBlockedBillingAccount(req, res) {
    web.checkPermission("bills", req.gid, function (allowed) {
        if (!allowed || !allowed.write) {
            res.status(403).json({"code": -1});
        } else {
            var data = JSON.parse(req.fields.data);

            billManager.addBlockedBillingAccount(data, function (err, result) {
                if (err) {
                    return res.status(400).json({
                        code: -1,
                        error: err.message,
                        write: allowed.write
                    });
                } else {
                    return res.json({
                        code: 0,
                        addedCount: result.addedCount,
                        skippedCount: result.skippedCount,
                        write: allowed.write
                    });
                }
            });
        }
    });
}

exports.loadBlockedBillingAccount = function loadBlockedBillingAccount(req, res) {
    web.checkPermission("bills", req.gid, function (allowed) {
        if (!allowed || !allowed.write) {
            res.status(403).json({"code": -1});
        } else {
            var displayLength = parseInt(req.query.iDisplayLength);
            var displayStart = parseInt(req.query.iDisplayStart);

            billManager.loadBlockedBillingAccount(function (err, result) {
                if (err) {
                    return res.status(400).json({
                        code: -1,
                        error: err.message,
                        write: allowed.write
                    });
                }

                var data = []
                for (var i = displayStart; i < (displayStart + displayLength) && i < result.list.length; i++) {
                    data.push(result.list[i]);
                }

                res.json({
                    "code": 0,
                    "write": allowed.write,
                    "data": data,
                    "iTotalRecords": result.list.length,
                    "iTotalDisplayRecords": result.list.length
                });
            });
        }
    });
}

exports.cleanBlockedBillingAccount = function cleanBlockedBillingAccount(req, res) {
    web.checkPermission("bills", req.gid, function (allowed) {
        if (!allowed || !allowed.write) {
            res.status(403).json({"code": -1});
        } else {
            billManager.cleanBlockedBillingAccount(function (err, result) {
                if (err) {
                    return res.status(400).json({
                        code: -1,
                        error: err.message,
                        write: allowed.write
                    });
                } else {
                    return res.json({
                        code: 0,
                        deletedCount: result.deletedCount,
                        write: allowed.write
                    });
                }
            });
        }
    });
}



