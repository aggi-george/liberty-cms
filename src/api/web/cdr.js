var async = require('async');
var config = require('../../../config');
var db = require('../../../lib/db_handler');
var ec = require('../../../lib/elitecore');
var common = require('../../../lib/common');
var cdrManager = require('../../core/manager/cdr/cdrManager');
var web = require('./web');

var LOG = config.LOGSENABLED;
var SG_PREFIX = "65";

//exports

exports.getCDR = getCDR;

//functions
function getCDR(req, res) {
    web.checkPermission("cdr", req.gid, function(allowed) {
        if (!allowed) {
            res.status(403).json({ "code" : -1 });
        } else {
            if ( req.params.type == "m1" ) {
                getM1(req.query, function (err, result) {
                    if (err && !result) res.json(err);
                    else res.json(result);
                });
            } else if ( req.params.type == "ec" ) { 
                getEC(req.query, function (err, result) {
                    if (err && !result) res.json(err);
                    else res.json(result);
                });
            } else if ( req.params.type == "ocserrors" ) {
                getOCSErrors(req.query, function (err, result) {
                    if (err && !result) res.json(err);
                    else res.json(result);
                });
            } else if ( req.params.type == "scp" ) {
                getSCP(req.query, function (err, result) {
                    if (err && !result) res.json(err);
                    else res.json(result);
                });
            } else if ( req.params.type == "offlineeccdrs" ) {
                getOfflineECCdrs(req.query, function (err, result) {
                    if (err && !result) res.json(err);
                    else res.json(result);
                });
            } else if ( req.params.type == "reassurance" ) {
                getReassurance(req.query, function (err, result) {
                    if (err && !result) res.json(err);
                    else res.json(result);
                });
            }
        }
    });
}

function getIMSI(mobile, callback){
    ec.getCustomerDetailsNumber("65", mobile, false, function (err, cache) { 
        if (!err && cache) {
            callback(undefined, cache.imsi);
        } else {
            return console.log('Mobile number not found: ' + mobile );
        }
    });	
}

function getM1(params, callback) {
    var displayLength = parseInt(params.iDisplayLength);
    var displayStart = parseInt(params.iDisplayStart);
    //iSortCol_0 == "4"  -- transaction_date
    var sortDir = (params.sSortDir_0 == "desc") ? 1 : -1;
    var sort = { "transaction_date" : sortDir };
    var imsi = params.imsi;
    var mobile = params.mobile;
    var test;
    async.series(
        [
            function (callback) {
                if (mobile !== undefined && mobile != null && mobile != "") {
                    getIMSI(params.mobile, function(err, result){
                        if (err && !result) {
                            console.log(err);
                        }
                        else {
                            if(LOG) console.log("Searching for IMSI of " + mobile + ": " + result);
                            imsi = result;
                        }
                        callback(err, result);
                    });
                } else {
                    callback(undefined, "No mobile number input");
                }
            },
            function (callback) {
                if(LOG) console.log("PARAMETERS" + JSON.stringify(params));
                var year = (params.year) ? parseInt(params.year) : undefined;
                var month = (params.month) ? parseInt(params.month) : undefined;
                var query = new Object;
                if (imsi) query.imsi = imsi;
                //if (mobile) query.point_origin = mobile;
                if (year && month) { 
                    var low = new Date(year, month-1, 1).getTime();
                    var high = new Date(year, month, 0, 23, 59, 59, 999).getTime();
                    query.transaction_date = { "$gte" : low, "$lt" : high };
                }		
                var usage_type_id = [];
                if (params.usage_type == "voice") {
                    usage_type_id = ["3300", "3600", "3603", "3604", "3630", "3633", "3634", "3606", "3636"];
                    query.type_id_usg = { "$in" : usage_type_id };
                } else if (params.usage_type == "sms") {
                    usage_type_id = ["3610", "3366"];
                    query.type_id_usg = { "$in" : usage_type_id };
                } else if (params.usage_type == "data") {
                    usage_type_id = ["6290", "6470", "6471", "6472"];
                    query.type_id_usg = { "$in" : usage_type_id };
                }
                return db.m1cdrs.find(query).sort(sort).skip(displayStart).limit(displayLength).toArray(function (err, rows) {
                    if (rows && rows[0]) {
                        var list = new Array;
                        rows.forEach(function (row) {
                            var item = new Object;
                            item.record_type = row.record_type;
                            item.plmn_desc = row.plmn_desc;
                            item.tap_seq_num = row.tap_seq_num;
                            item.imsi = row.imsi;
                            item.transaction_date = common.getDateTime(new Date(row.transaction_date));
                            item.type_id_usg = row.type_id_usg;
                            item.primary_units = row.primary_units;
                            item.accounted_duration = row.accounted_duration;
                            item.point_origin = row.point_origin;
                            item.cost = row.cost
                            list.push(item);
                        });
                        db.m1cdrs.find(query).count(function (err, count) {
                            callback(undefined, { "code" : 0, "list" : list, "total" : count });
                        });
                    } else {
                        callback({ "code" : -1});
                    }
                });
            },
            function (callback) {
                var year = (params.year) ? parseInt(params.year) : undefined;
                var month = (params.month) ? parseInt(params.month) : undefined;
                var start = new Date(year, month-1, 1).getTime();
                var end = new Date(year, month, 0, 23, 59, 59, 999).getTime();
                var match = { "$match": { "imsi": imsi, "transaction_date": {  '$gte': start, '$lt': end  } } }
                var project = { "$project" : { "imsi": 1, "primary_units": 1, "type_id_usg": 1, "cost": 1, "total": {  "$multiply": [ "$accounted_duration", "$cost" ] } } }
                var group = { "$group" : { "_id" : { "imsi" : "$imsi", "typeIdUsg" : "$type_id_usg"}, "totalCost" : {"$sum" : "$total"}, "totalUnits" : {"$sum" : "$primary_units"}}}
                return db.m1cdrs.aggregate([match, project, group]).toArray(function (err, rows) {
                    if (rows && rows[0]) {
                        var list = new Array;
                        rows.forEach(function (row) {
                            var item = new Object;
                            item.typeIdUsg = row._id.typeIdUsg;
                            item.totalUnits = row.totalUnits;
                            item.totalCost = row.totalCost;
                            list.push(item);
                        });
                        callback(undefined, { "code" : 0, "list" : list});
                    } else {
                        callback({ "code" : -1});
                    }
                });
            }
        ],
        function(err, results) {
            if(LOG) console.log(JSON.stringify(results));
            callback(undefined, results);
        }
    );
}

function getEC(params, callback) {	
    var prefix = SG_PREFIX;
    var number = params.mobile;
    var serviceInstanceNumber = params.serviceInstanceNumber;

    if (!number && !serviceInstanceNumber && params.getRGID) {
        return callback({ "code" : 0, "result" : cdrManager.ecRGID });
    }

    var args = {
        "type" : params.type,
        "start" : params.start,
        "end" : params.end,
        "rgid" : params.rgid,
        "displayLength" : parseInt(params.iDisplayLength),
        "displayStart" : parseInt(params.iDisplayStart),
        "sortDir" : params.sSortDir_0,
    };
    var getSIN = function(cb) {
        if (serviceInstanceNumber) cb(undefined, { "serviceInstanceNumber" : serviceInstanceNumber });
        else ec.getCustomerDetailsNumber(prefix, number, false, cb);
    }
    getSIN(function(err, cache) {
        if (cache) {
            args.serviceInstanceNumber = cache.serviceInstanceNumber;
            cdrManager.elitecore(args, function (err, result) {
                if (!err && result) {
                    callback(undefined, { "code" : 0, "result" : result });
                } else {
                    callback({ "code" : -1, "error" : err.message});
                }
            });
        } else {
            callback({ "code" : -1 });
        }
    });
}

function getReassurance(params, callback) {
    var args = {
        mobile: params.mobile,
        session: params.session,
        request: parseInt(params.request),
        type: params.type,
        start: params.start,
        end: params.end,
        displayLength: parseInt(params.iDisplayLength),
        displayStart: parseInt(params.iDisplayStart),
        sortDir: params.sSortDir_0,
    };
    cdrManager.reassurance(args, function (err, result) {
        if (!err && result) {
            callback(undefined, { "code" : 0, "result" : result });
        } else {
            callback({ "code" : -1, "error" : err.message});
        }
    });
}

function getOCSErrors(params, callback) {
    var displayLength = parseInt(params.iDisplayLength);
    var displayStart = parseInt(params.iDisplayStart);
    var sortDir = (params.sSortDir_0 == "desc") ? 1 : -1;
    var sort = { "ETS" : sortDir };
    var msisdn = params.msisdn;
    var err = params.err;
    var year = (params.year) ? parseInt(params.year) : undefined;
    var month = (params.month) ? parseInt(params.month) : undefined;
    var day = (params.day) ? parseInt(params.day) : undefined;
    var hour_start = (params.hour_start) ? parseInt(params.hour_start) :undefined;
    var hour_end = (params.hour_end) ? parseInt(params.hour_end) :undefined;
    var minute_start = (params.minute_start) ? parseInt(params.minute_start) :undefined;
    var minute_end = (params.minute_end) ? parseInt(params.minute_end) :undefined;
    var query = new Object;
    if (msisdn) query.MSISDN = msisdn;
    if (err) query.ERR = err;
    if (year && month) {
        var low = new Date(year, month-1, day, hour_start-8, minute_start).getTime();
        var high = new Date(year, month-1, day, hour_end-8, minute_end, 59, 999).getTime();
        query.ETS = { "$gte" : low, "$lt" : high };
    }
    db.ocslogs.find(query).sort(sort).skip(displayStart).limit(displayLength).toArray(function (err, rows) {
        if(LOG) console.log(query);
        if (rows && rows[0]) {
            var list = new Array;
            rows.forEach(function (row) {
                var item = new Object;
                item.ets = common.getDateTime(new Date(row.ETS + 28800000));
                item.msisdn = row.MSISDN;
                item.err = row.ERR;
                item.errdesc = row.ERRDESC;
                item.sid = row.SID;
                item.gen5 = row.GEN5;
                list.push(item);
            });
            db.ocslogs.find(query).count(function (err, count) {
                callback(undefined, { "code" : 0, "list" : list, "total" : count });
            });
        } else {
            callback({ "code" : -1});
        }
    });
}

function getSCP(params, callback) {
    var displayLength = parseInt(params.iDisplayLength);
    var displayStart = parseInt(params.iDisplayStart);
    var sortDir = (params.sSortDir_0 == "desc") ? 1 : -1;
    var sort = { "startDateTime" : sortDir };
    var aMsisdn = params.aMsisdn;
    var bMsisdn = params.bMsisdn;
    var year = (params.year) ? parseInt(params.year) : undefined;
    var month = (params.month) ? parseInt(params.month) : undefined;
    var day = (params.day) ? parseInt(params.day) : undefined;
    var hour_start = (params.hour_start) ? parseInt(params.hour_start) :undefined;
    var hour_end = (params.hour_end) ? parseInt(params.hour_end) :undefined;
    var minute_start = (params.minute_start) ? parseInt(params.minute_start) :undefined;
    var minute_end = (params.minute_end) ? parseInt(params.minute_end) :undefined;
    var query = new Object;
    if (aMsisdn) query.aMsisdn = new RegExp(aMsisdn, "i");
    if (bMsisdn) query.bMsisdn = new RegExp(bMsisdn, "i");
    if (year && month) {
        var low = new Date(year, month-1, day, hour_start, minute_start).getTime();
        var high = new Date(year, month-1, day, hour_end, minute_end, 59, 999).getTime();
        query.startDateTime = { "$gte" : low, "$lt" : high };
    }
    db.scpcdrs.find(query).sort(sort).skip(displayStart).limit(displayLength).toArray(function (err, rows) {
        if(LOG) console.log(query);
        if (rows && rows[0]) {
            var list = new Array;
            rows.forEach(function (row) {
                var item = new Object;
                item.recordType = row.recordType;
                item.aMsisdn = row.aMsisdn;
                item.bMsisdn = row.bMsisdn;
                item.startDateTime = common.getDateTime(new Date(row.startDateTime));
                item.routingToSip = row.routingToSip;
                item.responseDelay = row.responseDelay;
                item.totalCallDuration = row.totalCallDuration;
                item.callOrSmsStatus = row.callOrSmsStatus;
                item.statusDescription = row.statusDescription;
                list.push(item);
            });
            db.scpcdrs.find(query).count(function (err, count) {
                callback(undefined, { "code" : 0, "list" : list, "total" : count });
            });
        } else {
            callback({ "code" : -1});
        }
    });
}
function getOfflineECCdrs(params, callback) {
    var displayLength = parseInt(params.iDisplayLength);
    var displayStart = parseInt(params.iDisplayStart);
    var sortDir = (params.sSortDir_0 == "desc") ? 1 : -1;
    var sort = { "processdate" : sortDir };
    var subscriberidentifier = params.subscriberidentifier;
    var serviceid = (params.serviceid) ? parseInt(params.serviceid) : undefined;
    var year = (params.year) ? parseInt(params.year) : undefined;
    var month = (params.month) ? parseInt(params.month) : undefined;
    var day = (params.day) ? parseInt(params.day) : undefined;
    var hour_start = (params.hour_start) ? parseInt(params.hour_start) :undefined;
    var hour_end = (params.hour_end) ? parseInt(params.hour_end) :undefined;
    var minute_start = (params.minute_start) ? parseInt(params.minute_start) :undefined;
    var minute_end = (params.minute_end) ? parseInt(params.minute_end) :undefined;
    var query = new Object;
    if (subscriberidentifier) query.subscriberidentifier = new RegExp(subscriberidentifier, "i");
    if (serviceid) query.serviceid = serviceid;
    if (year && month) {
        var low = new Date(year, month-1, day, hour_start, minute_start).getTime();
        var high = new Date(year, month-1, day, hour_end, minute_end, 59, 999).getTime();
        query.processdate = { "$gte" : low, "$lt" : high };
    }
    db.eccdrs.find(query).sort(sort).skip(displayStart).limit(displayLength).toArray(function (err, rows) {
        if(LOG) console.log(query);
        if (rows && rows[0]) {
            var list = new Array;
            rows.forEach(function (row) {
                var item = new Object;
                item.subscriberidentifier = row.subscriberidentifier;
                item.processdate = common.getDateTime(new Date(row.processdate));
                item.sessiontime = row.sessiontime;
                item.accountedtime = row.accountedtime;
                item.accountedcost = row.accountedcost;
                item.sessiondatatransfer = row.sessiondatatransfer;
                item.accounteddatatransfer = row.accounteddatatransfer;
                item.ratinggroupid = row.ratinggroupid;
                item.general5 = row.general5;
                list.push(item);
            });
            db.eccdrs.find(query).count(function (err, count) {
                callback(undefined, { "code" : 0, "list" : list, "total" : count });
            });
        } else {
            callback({ "code" : -1});
        }
    });
}
