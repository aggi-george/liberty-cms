/**
 *
 *  Config related functions
 *
 */

var config = require('../../../config');
var common = require('../../../lib/common');
var db = require('../../../lib/db_handler');
var ec = require('../../../lib/elitecore');
var web = require('./web');
var configWrapper = require('../../core/manager/config/configManager');

var LOG =  config.LOGSENABLED;

exports.getConfigMapList = function getBillsArchive(req, res) {
    // admin - "bonus", "notification", "selfcare"
    // bills - "bill"

    var type = req.query.type;
    var permission = "";

    if(type == "elitecore"){
        permission = "noc"
    }
    else if (type === "bill") {
        permission = "bills";
    } else {
        permission = "admin";
    }

    web.checkPermission(permission, req.gid, function (allowed) {
        if (!allowed) {
            return res.status(403).json({"code": -1});
        }

        var type = req.query.type;
        configWrapper.loadConfigMapForGroup(type, function (error, configList, configMap) {
            if (error) {
                if (LOG) common.log('config', 'can not load config, error=' + error);
                return res.status(400).json({"code": -1, error: error.message});
            }

            //load promo categories
            var q = "SELECT id, name, type, partner " +
                " FROM promoCodesCategories ORDER BY id DESC";

            db.query_noerr(q, function (rows) {
                return res.json({
                    "code": 0,
                    data: configList,
                    packages: ec.packages(),
                    promoCategories:rows
                });
            });
        });
    });
}

exports.updateConfigItem = function getBillsArchive(req, res) {
    // admin - "bonus", "notification", "selfcare"
    // bills - "bill"

    var key = req.fields.key;
    var options = req.fields;
    var permission = "";

    if(key == "elitecore_api" || key=="elitecore_bss_pool"){
        permission = "noc"
    }
    else if (key === "pay_now") {
        permission = "bills";
    } else {
        permission = "admin";
    }

    web.checkPermission(permission, req.gid, function (allowed) {
        if (!allowed || !allowed.write) {
            return res.status(403).json({"code": -1});
        }

        var key = req.fields.key;
        var options = req.fields;
        delete options.key;

        configWrapper.updateConfigItem(key, options, function (error) {
            if (error) {
                if (LOG) common.log('config', 'can not update config, error=' + error);
                return res.status(400).json({"code": -1, error: error.message});
            }

            db.weblog.insert({
                "ts": new Date().getTime(),
                "ip": req.ip,
                "type": "updateConfigItem",
                "id": common.escapeHtml(key),
                "action": common.escapeHtml("update:" + JSON.stringify(options)),
                "username": req.user,
                "code": 0
            });

            return res.json({"code": 0, key: key});
        });
    });
}