var fs = require('fs');
var csvParse = require('csv-parse');
var config = require('../../../config');
var common = require('../../../lib/common');
var db = require('../../../lib/db_handler');
var web = require('./web');
var contents = require('../internal/contentApi');
var contentsRates = require(__core + '/manager/contents/rates');

var logsEnabled = config.LOGSENABLED;

//exports

exports.get = get;
exports.addNew = addNew;
exports.deleteQuestion = deleteQuestion;
exports.upload = upload;
exports.updateFAQ = updateFAQ;

//functions

function get(req, res) {
    web.checkPermission("contents", req.gid, function(allowed) {
        if (!allowed) {
            res.status(403).json({ "code" : -1 });
        } else {
            var cat = req.params.cat;
            var type = req.params.type;
            var iso = req.query ? req.query.iso : undefined;
            if (iso) {
                contentsRates.getIngressISO(iso, function (err, result) {
                    res.json({ "code" : 0, "result" : result, "write" : allowed.write });
                });
            } else {
                contents.getContents(cat, type, undefined, undefined, function (err, result) {
                    var content = { "cat" : cat, "type" : type };
                    if (cat == "rates") {
                        content.content = result.rates;
                        content.header = result.header[type];
                        content.tooltip = result.tooltip[type];
                    } else {
                        if (!err && result) {
                            content.content = result.content;
                        } else {
                            content.content = "";
                        }
                    }
                    res.json({ "code" : 0, "result" : content, "write" : allowed.write });
                });
            }
        }
    });
}

function addNew(req, res) {
    web.checkPermission("contents", req.gid, function(allowed) {
        if (!allowed || !allowed.write) {
            res.status(403).json({ "code" : -1 });
        } else {
            var cat = req.params.cat;
            var question = req.fields.question;
            var q = "INSERT IGNORE INTO faq (category, question) VALUES (" + db.escape(cat) + ", " + db.escape(question) + ")";
            db.query_noerr(q, function(rows) {
                res.json({ "code" : 0, "write" : allowed.write });
                db.weblog.insert({  "ts" : new Date().getTime(),
                                    "ip" : req.ip,
                                    "type" : "addNewFAQ",
                                    "id" : common.escapeHtml(question),
                                    "action" : "INSERT",
                                    "username" : req.user,
                                    "code" : 0
                });
            });
        }
    });
}

function deleteQuestion(req, res) {
    web.checkPermission("contents", req.gid, function(allowed) {
        if (!allowed || !allowed.write) {
            res.status(403).json({ "code" : -1 });
        } else {
            var id = req.params.id;
            var q = "DELETE FROM faq WHERE id=" + db.escape(id);
            if (logsEnabled) common.log("contents deleteQuestion", q);
            db.query_noerr(q, function(rows) {
                res.json({ "code" : 0, "write" : allowed.write });
                db.weblog.insert({  "ts" : new Date().getTime(),
                                    "ip" : req.ip,
                                    "type" : "deleteQuestion",
                                    "id" : common.escapeHtml(id),
                                    "action" : "DELETE",
                                    "username" : req.user,
                                    "code" : 0
                });
            });
        }
    });
}

function rates(path, type, callback) {
    var stream = fs.readFileSync(path, "utf8");
    var values = "";
    var tblArr = stream.split("\r\n");
    tblArr.forEach(function (row, i) {
        if ( i > 0 ) {
            var rowArr = row.split(",");
            if ( type == "circlestalk" ) {
                var country = rowArr[0];
                var rate = rowArr[1];
                if (country && rate) {
                    values += "(" + db.escape(country) + "," + db.escape(rate) + "),";
                }
            } else if ( type == "idd002" ) {
                var country = rowArr[0];
                var peak = rowArr[1];
                var nonpeak = rowArr[2];
                if (country && peak && nonpeak) {
                    values += "(" + db.escape(country) + "," + db.escape(peak) + "," + db.escape(nonpeak) + "),";
                }
            } else if ( type == "idd021" ) {
                var country = rowArr[0];
                var peak = rowArr[1];
                var nonPeak = rowArr[2];
                if (country && peak && nonPeak) {
                    values += "(" + db.escape(country) + "," + db.escape(peak) + "," + db.escape(nonPeak) + "),";
                }
            } else if ( type == "ppurroaming" ) {
                var country = rowArr[0];
                var ppurdatamms = rowArr[1];
                var ppurvoicesg = rowArr[2];
                var ppurvoiceintl = rowArr[3];
                var ppurvoicelocal = rowArr[4];
                var ppurvoiceincoming = rowArr[5];
                var ppursmssend = rowArr[6];
                if (country && ppurdatamms && ppurvoicesg && ppurvoiceintl && ppurvoicelocal && ppurvoiceincoming && ppursmssend) {
                    values += "(" + db.escape(country) + "," + db.escape(ppurdatamms) + "," + db.escape(ppurvoicesg) + "," + db.escape(ppurvoiceintl) + "," + db.escape(ppurvoicelocal) + "," + db.escape(ppurvoiceincoming) + "," + db.escape(ppursmssend) + "),";
                }
            }
        }
    });
    if ( type == "circlestalk" ) {
        q = "INSERT into ratesCirclesTalk (country, rate) VALUES " + values.slice(0,-1) + " ON DUPLICATE KEY UPDATE rate=VALUES(rate)";
    } else if ( type == "idd002" ) {
        q = "INSERT into ratesIDD002 (country, peak, nonPeak) VALUES " + values.slice(0,-1) + " ON DUPLICATE KEY UPDATE peak=VALUES(peak), nonPeak=VALUES(nonPeak)";
    } else if ( type == "idd021" ) {
        q = "INSERT into ratesIDD021 (country, peak, nonPeak) VALUES " + values.slice(0,-1) + " ON DUPLICATE KEY UPDATE peak=VALUES(peak), nonPeak=VALUES(nonPeak)";
    } else if ( type == "ppurroaming" ) {
        q = "INSERT into ratesPPURRoaming (country, ppurdatamms, ppurvoicesg, ppurvoiceintl, ppurvoicelocal, ppurvoiceincoming, ppursmssend) VALUES " + values.slice(0,-1) + " ON DUPLICATE KEY UPDATE ppurdatamms=VALUES(ppurdatamms), ppurvoicesg=VALUES(ppurvoicesg), ppurvoiceintl=VALUES(ppurvoiceintl), ppurvoicelocal=VALUES(ppurvoicelocal), ppurvoiceincoming=VALUES(ppurvoiceincoming), ppursmssend=VALUES(ppursmssend)";
    }
    callback(q);
}

function category(path, cat, type, callback) {
    if ( cat == "rates" ) {
        rates(path, type, function (q) {
            callback(q);
        });
    } else if ( cat == "faq" ) {
        csvParse(fs.readFileSync(path, "utf8"), function(err, data) {
            var values = "";
            data.forEach(function (row, i) {
                var category = row[0].toLowerCase();
                var question = row[1];
                var answer = row[2];
                if (category && question && answer) {
                    values += "(" + db.escape(category) + "," + db.escape(question) + "," + db.escape(answer) + "),";
                }
            });
            q = "INSERT INTO faq (category, question, answer) VALUES " + values.slice(0,-1) + " ON DUPLICATE KEY UPDATE answer=VALUES(answer)";
            callback(q);
        });
    } else {
        var stream = fs.readFileSync(path, "utf8");
        q = "INSERT INTO contents (contentName, content) VALUES (" + db.escape(cat + "_" + type) + "," + db.escape(stream) + ") ON DUPLICATE KEY UPDATE content=VALUES(content)";
        callback(q);
    }
}

function upload(req, res) {
    web.checkPermission("contents", req.gid, function(allowed) {
        if (!allowed || !allowed.write) {
            res.status(403).json({ "code" : -1 });
        } else {
            var cat = req.params.cat;
            var type = req.params.type;
            var path = req.files.content.path;
            var name = req.files.content.name;
            if (!cat || !type || !path || !name) return res.json({ "code" : -1, "cat" : cat, "type" : type, "write" : allowed.write });
            category(path, cat, type, function (q) {
                db.query_noerr(q, function(result) {
                    fs.unlink(path);
                    if ( result && (result.affectedRows > 0) ) {
                        res.json({ "code" : 0, "cat" : cat, "type" : type, "write" : allowed.write });
                    } else {
                        res.json({ "code" : -1, "cat" : cat, "type" : type, "write" : allowed.write });
                    }
                });
                db.weblog.insert({    "ts" : new Date().getTime(),
                            "ip" : req.ip,
                            "type" : "uploadContent",
                            "id" : common.escapeHtml(cat),
                            "action" : "INSERT " + common.escapeHtml(type),
                            "username" : req.user,
                            "code" : 0
                        });
            });
        }
    });
}


function updateFAQ(req, res) {
    web.checkPermission("contents", req.gid, function(allowed) {
        if (!allowed || !allowed.write) {
            res.status(403).json({ "code" : -1 });
        } else {
            var id = req.params.id;
            var type = (req.params.type == "answer") ? "answer" :
                (req.params.type == "question") ? "question" :
                (req.params.type == "orderID") ? "orderID" : undefined;
            var text = req.fields.text;
            var q = "UPDATE faq SET " + type + "=" + db.escape(text) + " WHERE id=" + db.escape(id);
            db.query_noerr(q, function(rows) {
                res.json({ "code" : 0, "write" : allowed.write });
                db.weblog.insert({    "ts" : new Date().getTime(),
                            "ip" : req.ip,
                            "type" : "updateFAQ",
                            "id" : common.escapeHtml(id),
                            "action" : common.escapeHtml(type),
                            "username" : req.user,
                            "code" : 0
                        });
            });
        }
    });
}
