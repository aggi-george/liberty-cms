var async = require('async');
var config = require('../../../config');
var common = require('../../../lib/common');
var db = require('../../../lib/db_handler');
var ec = require('../../../lib/elitecore');
var elitecoreUserService = require('../../../lib/manager/ec/elitecoreUserService');
var elitecoreBillService = require('../../../lib/manager/ec/elitecoreBillService');
var paas = require('../../../lib/paas');
var pdfBillingManager = require('../../core/manager/account/pdfBillingManager');
var billManager = require('../../core/manager/account/billManager');
var ecommManager = require('../../core/manager/ecomm/ecommManager');
var push = require('../../notifications/push');
var notificationManager = require('../../core/manager/notifications/notificationManager');
var notificationSend = require('../../core/manager/notifications/send');
var scp = require('../../../lib/scp');
var appManager = require(__manager+ '/app/appManager');
var usageManager = require('../../core/manager/account/usageManager');
var web = require('./web');
var boostManager = require('../../core/manager/boost/boostManager');
var bonusManager = require('../../core/manager/bonus/bonusManager');
var leaderboardManager = require('../../core/manager/bonus/leaderboardManager');
var subscriptionManager = require('../../core/manager/bonus/subscriptionManager');
var referralManager = require('../../core/manager/promotions/referralManager');
var packageManager = require(__core + '/manager/packages/packageManager');
var hlrManager = require('../../core/manager/hlr/hlrManager');
var accountManager = require('../../core/manager/account/accountManager');
const validationQ = require(`${global.__manager}/account/validationQ`);
const validation = require(`${global.__manager}/account/validation`);
const accountActions = require(`${global.__manager}/account/actions`);
var paymentManager = require('../../core/manager/account/paymentManager');
var repairManager = require('../../core/manager/account/repairManager');
var portInManager = require('../../core/manager/porting/portInManager');
var portOutManager = require('../../core/manager/porting/portOutManager');
var profileManager = require('../../core/manager/profile/profileManager');
var invoiceManager = require('../../core/manager/account/invoiceManager');
var promotionsManager = require('../../core/manager/promotions/promotionsManager');
var cdrManager = require('../../core/manager/cdr/cdrManager');
var creditCapManager = require('../../core/manager/account/creditCapManager');
var diameterClient = require('../../core/manager/diameter/diameterClient');
var smsManager = require('../../core/manager/notifications/smsManager');

var notificationScheduler = require('../../core/scheduler/action/notification');
var addonScheduler = require('../../core/scheduler/action/addon');
var accountStatusScheduler = require('../../core/scheduler/action/accountStatus');
var reportScheduler = require('../../core/scheduler/action/report');
var autorepairScheduler = require('../../core/scheduler/action/autorepair');
var portInScheduler = require('../../core/scheduler/action/portIn');
var paymentScheduler = require('../../core/scheduler/action/payment');
var bi = require('../../core/scheduler/action/bi');
var zendesk = require('../../core/scheduler/action/zendesk');
var bonuses = require('../../api/mobile/account/bonuses');
var constants = require('../../core/constants');

var logsEnabled = config.LOGSENABLED;
var SUSPEND_CHARGE = 10;

//exports

exports.search = search;
exports.searchBills = searchBills;
exports.getDoc = getDoc;
exports.updateAddon = updateAddon;
exports.updateRoamingCap = updateRoamingCap;
exports.makePayment = makePayment;
exports.updateFreeBoost = updateFreeBoost;
exports.addLoyaltyBonus = addLoyaltyBonus;
exports.addContractBusterBonus = addContractBusterBonus;
exports.addCareinstallation = addCareinstallation;
exports.addPortInBonus = addPortInBonus;
exports.addPendingBonuses = addPendingBonuses;
exports.addAutoBoost = addAutoBoost;
exports.updateMonkey = updateMonkey;
exports.updateDOB = updateDOB;
exports.recache = recache;
exports.pay200 = pay200;
exports.changeStatus = changeStatus;
exports.validateStatus = validateStatus;
exports.repair = repair;
exports.portInAction = portInAction;
exports.portInReportUpload = portInReportUpload;
exports.portInNotification = portInNotification;
exports.getSchedules = getSchedules;
exports.addSchedules = addSchedules;
exports.moveSchedules = moveSchedules;
exports.deleteSchedules = deleteSchedules;
exports.executeSchedules = executeSchedules;
exports.checkNumberAvailable = checkNumberAvailable;
exports.releaseNumber = releaseNumber;
exports.updateDetails = updateDetails;
exports.updateProfileSettings = updateProfileSettings;
exports.getCDR = getCDR;
exports.testPush = testPush;
exports.manageGentwoAPI = manageGentwoAPI;
exports.addCreditNotes = addCreditNotes;
exports.useReferralCode = useReferralCode;
exports.deactivateReferrerBonuses = deactivateReferrerBonuses;
exports.deleteReferralCode = deleteReferralCode;
exports.deleteBonusHistory = deleteBonusHistory;
exports.addGoldenTicket = addGoldenTicket;
exports.deleteGoldenTicket = deleteGoldenTicket;
exports.addBirthdayBonus = addBirthdayBonus;
exports.addWinbackBonus = addWinbackBonus;
exports.useAvailableBonus = useAvailableBonus;
exports.resubscribeAddons = resubscribeAddons;
exports.blockNotif = blockNotif;
exports.overrideNotif = overrideNotif;
exports.getFreeICCID = getFreeICCID;
exports.consumeData = consumeData;
exports.createUpdateCCLink = createUpdateCCLink;
exports.ecommNotification = ecommNotification;
exports.removeAppRegistration = removeAppRegistration;
exports.smsTest = smsTest;
exports.simDamageConfirm = simDamageConfirm;
exports.addonExist = addonExist;
exports.updateRoaming = updateRoaming;
exports.getCustomerStatusSchedules = getCustomerStatusSchedules;
exports.corporatePortinAction = corporatePortinAction;

//functions

function search(req, res) {
    web.checkPermission("customers", req.gid, function (allowed) {
        if (!allowed) {
            res.status(403).json({"code": -1});
        } else {
            if (req.params.type == "mobile") {
                searchMobile(req.query.prefix, req.query.number, undefined, function (err, result) {
                    if (err) return res.status(400).json({code: -1, error: err.message, status: err.status, write: allowed.write});
                    res.json({"code": 0, "result": result, "write": allowed.write});
                });
            } else if (req.params.type == "acctnumber") {
                searchMobile(undefined, undefined, req.query.accountNumber, function (err, result) {
                    if (err) return res.status(400).json({code: -1, error: err.message, status: err.status, write: allowed.write});
                    res.json({"code": 0, "result": result, "write": allowed.write});
                });
            } else if (req.params.type == "acctname") {
                searchAccount({"accountName": req.query.accountName}, function (err, result) {
                    if (err) return res.status(400).json({code: -1, error: err.message, status: err.status, write: allowed.write});
                    res.json({"code": 0, "result": {"searchName": result}, "write": allowed.write});
                });
            } else if (req.params.type == "referralcode") {
                searchReferral({"code": req.query.referralCode}, function (err, result) {
                    if (err) return res.status(400).json({code: -1, error: err.message, status: err.status, write: allowed.write});
                    res.json({"code": 0, "result": {"searchNumber": result}, "write": allowed.write});
                });
            } else if (req.params.type == "gentwo") {
                manageGentwo(req.query.prefix, req.query.number, undefined, undefined, undefined, undefined, function (err, result) {
                    if (err) return res.status(400).json({code: -1, error: err.message, status: err.status, write: allowed.write});
                    res.json({
                        "code": 0, "result": {
                            "credit": result.credit,
                            "creditSGD": result.sgd,
                            "plan_id": result.plan_id,
                            "plans": result.plans,
                            "mo": result.mo,
                            "mt": result.mt
                        }, "write": allowed.write
                    });
                });
            }
        }
    });
}

function searchBills(req, res) {
    web.checkPermission("customers", req.gid, function (allowed) {
        if (!allowed) {
            res.status(403).json({"code": -1});
        } else {
            elitecoreUserService.loadUserInfoByCustomerAccountNumber(req.query.accountNumber, (err, cache) => {
                if (err) return res.status(400).json({code: -1, error: err.message, status: err.status, write: allowed.write});

                invoiceManager.loadBills(undefined, {billingAccountNumbers:[cache.billingAccountNumber]}, (err, result) => {
                    if (err) return res.status(400).json({code: -1, error: err.message, status: err.status, write: allowed.write});

                    res.json({
                        "code": 0,
                        "result": {
                            bills: result && result.data ? result.data : [],
                            pdfpassword: pdfBillingManager.getPdfPassword(cache)
                        },
                        "write": allowed.write
                    });
                });
            });
        }
    });
}

function searchService(accountNumber, callback) {
    if (!accountNumber) {
        return callback(new Error("Invalid params"));
    }

    ec.getCustomerHierarchyDetails({"accountNumber": accountNumber}, function (err, service) {
        if (err) {
            common.error("WebCustomerAPI", "searchService:getCustomerHierarchyDetails: error=" + err.message);
            return callback(new Error("Invalid response for " + accountNumber
            + " (getCustomerHierarchyDetails API: " + err.message + ")"));
        }
        if (!service || !service.return || !service.return.serviceInstanceDetailResponseobj
            || !service.return.serviceInstanceDetailResponseobj[0]
            || !service.return.serviceInstanceDetailResponseobj[0].serviceInstanceNumber) {
            return callback(new Error("Invalid response for " + accountNumber + " (getCustomerHierarchyDetails API)"));
        }
        var serviceInstanceNumber = service.return.serviceInstanceDetailResponseobj[0].serviceInstanceNumber;
        ec.listInventory({"serviceInstanceNumber": serviceInstanceNumber}, function (err, inventory) {
            if (err) {
                common.error("WebCustomerAPI", "searchService:listInventory: error=" + err.message);
                return callback(new Error("Invalid response for " + serviceInstanceNumber
                + " (getCustomerHierarchyDetails API: " + err.message + ")"));
            }

            var number = (inventory && inventory.return && inventory.return.inventories)
                ? inventory.return.inventories[0].inventoryNumber : "";
            callback(err, {
                "accountNumber": accountNumber,
                "serviceInstanceNumber": serviceInstanceNumber,
                "number": number
            });
        });
    });
}

function searchReferral(search, callback) {
    referralManager.loadUserAccount(search.code, function (err, result) {
        if (err) {
            return callback(err);
        }
        if (!result || result.reason === "NOT_FOUND") {
            callback(undefined, []);
        }else{
            searchAccount({"accountNumber": result.accountNumber}, function (err, result) {
                callback(undefined, result);
            });
        }
    });
}

function searchAccount(search, callback) {
    ec.searchAccount(search, function (err, result) {
        var list = (result && result.return && result.return.searchAccountResponseVO) ? result.return.searchAccountResponseVO : [];
        var tasks = new Array;
        list.forEach(function (item) {
            tasks.push(function (callback) {
                searchService(item.accountNumber, function (err, result) {
                    callback(err, result);
                });
            });
        });
        async.parallel(tasks, function (err, asyncResult) {
            var resultmap = asyncResult.map(function (o) {
                return o ? o.accountNumber : null;
            });
            list.forEach(function (item) {
                idx = resultmap.indexOf(item.accountNumber);
                if (idx > -1) {
                    item.serviceInstanceNumber = asyncResult[idx].serviceInstanceNumber;
                    item.number = asyncResult[idx].number;
                }
            });
            callback(false, list);
        });
    });
}

function getCirclsCarePins(prefix, numbers, callback) {
    var numberPinMap = [];
    async.each(numbers, function (number, cb) {
        db.cache_keys("cache", prefix + "" + number + "_*", function (key) {
            if (key) {
                db.cache_get("cache", key, function (value) {
                    var pin = "";
                    if (value) {
                        if (value.key) {
                            pin = value.key.split("_")[1];
                        } else if (typeof(value) == "object") {
                            value.forEach(function (v) {
                                var obj = (typeof(v) != "object") ? common.safeParse(v) : v;
                                if (obj && obj.key) {
                                    pin += obj.key.split("_")[1] + "/"
                                } else {
                                    pin = "";
                                }
                            });
                        } else {
                            pin = "";
                        }
                    }
                    numberPinMap.push({number: number, pin: pin});
                    cb();
                });
            } else {
                numberPinMap.push({number: number, pin: "Undefined"});
                cb();
            }
        });
    }, function (err) {
        callback(undefined, {"pins": numberPinMap});
    });
}

var requestId = 0;
function searchMobile(prefix, number, account, callback) {
    var operationId = requestId++;
    if (logsEnabled) common.log("CustomerApi", "searchMobile[" + operationId + "]: load customer, prefix=" + prefix + ", number="
    + number + ", account=" + account)

    var cacheStartDate = new Date();
    common.log("CustomerApi", "searchMobile[" + operationId + "]->loadCustomerCache: starting...");

    var tasksKeys = [];
    setTimeout(() => {
        if (tasksKeys.length) {
            console.log("CustomerApi", "WARNING: slow operations (> 1 sec) => " + JSON.stringify(tasksKeys));
            setTimeout(() => {
                if (tasksKeys.length) {
                    console.log("CustomerApi", "WARNING: stuck operations (> 5 sec) => " + JSON.stringify(tasksKeys));
                }
            }, 4000);
        }
    }, 1000);

    var loadCache = function (err, cache) {
        var time = new Date().getTime() - cacheStartDate.getTime();
        common.log("CustomerApi", "searchMobile[" + operationId + "]->loadCustomerCache: completed, time=" + time);

        var tasks = [];
        var allNumbers = [];
        if (cache && cache.serviceInstanceInventories) {
            cache.serviceInstanceInventories.forEach((item) => {
                if (item.number) {
                    allNumbers.push(item.number)
                }
            })
        }
        if (number && allNumbers.indexOf(number) == -1) {
            allNumbers.push(number);
        }
        if (cache) {
            number = cache.number;
        }

        tasks.push(function (callback) {
            var startDate = new Date();
            tasksKeys.push("getCirclesCarePins");
            common.log("CustomerApi", "searchMobile[" + operationId + "]->getCirclesCarePins: starting...");

            getCirclsCarePins(prefix, allNumbers, function (err, pins) {
                var time = new Date().getTime() - startDate.getTime();
                common.log("CustomerApi", "searchMobile[" + operationId + "]->getCirclesCarePins: completed, time=" + time);

                tasksKeys.splice(tasksKeys.indexOf("getCirclesCarePins"), 1);
                callback(err, pins);
            });
        });

        tasks.push(function (callback) {
            var startDate = new Date();
            tasksKeys.push("loadAppsPushTokens");
            common.log("CustomerApi", "searchMobile[" + operationId + "]->loadAppsPushTokens: starting...");

            appManager.getDevices(number, prefix, undefined, function(rows){
                var time = new Date().getTime() - startDate.getTime();
                common.log("CustomerApi", "searchMobile[" + operationId + "]->loadAppsPushTokens: completed, time=" + time);

                tasksKeys.splice(tasksKeys.indexOf("loadAppsPushTokens"), 1);
                if(rows){
                    callback(undefined, {"apps": rows});
                }else{
                    callback(undefined);
                }
            });
        });
        tasks.push(function (callback) {
            var startDate = new Date();
            tasksKeys.push("loadSelfcareAppViewMobile");
            common.log("CustomerApi", "searchMobile[" + operationId + "]->loadSelfcareAppViewMobile: starting...");

            usageManager.computeUsage(prefix, number, function (errD, result) {
                var time = new Date().getTime() - startDate.getTime();
                common.log("CustomerApi", "searchMobile[" + operationId + "]->loadSelfcareAppViewMobile: completed, time=" + time);

                tasksKeys.splice(tasksKeys.indexOf("loadSelfcareAppViewMobile"), 1);
                if (!err && result && result.data && result.other) {
                    callback(undefined, {"appview": [result.data, result.other]});
                } else {
                    callback(undefined);
                }
            });
        });
        tasks.push(function (callback) {
            var startDate = new Date();
            tasksKeys.push("loadGentwoInfo");
            common.log("CustomerApi", "searchMobile[" + operationId + "]->loadGentwoInfo: starting...");

            manageGentwo(prefix, number, undefined, undefined, undefined, undefined, function (err, result) {
                var time = new Date().getTime() - startDate.getTime();
                common.log("CustomerApi", "searchMobile[" + operationId + "]->loadGentwoInfo: completed, time=" + time);

                tasksKeys.splice(tasksKeys.indexOf("loadGentwoInfo"), 1);
                if (!err && result && (result.code == 0)) {
                    callback(undefined, {
                        "credit": result.credit,
                        "creditSGD": result.sgd,
                        "plan_id": result.plan_id,
                        "plans": result.plans,
                        "mo": result.mo,
                        "mt": result.mt
                    });
                } else {
                    callback(undefined);
                }
            });
        });
        tasks.push(function (callback) {
            var startDate = new Date();
            tasksKeys.push("loadTerminalEvents");
            common.log("CustomerApi", "searchMobile[" + operationId + "]->loadTerminalEvents: starting...");

            db.scheduler.find({"start": {"$gt": new Date().getTime()}, "action": "terminate", "number": number})
                .toArray(function (err, result) {
                    if (err) {
                        common.error("CustomerApi", "searchMobile[" + operationId + "]->loadTerminalEvents: error=" + err.message);
                        return callback(undefined, {"termination": [], "error": err.message});
                    }

                    var time = new Date().getTime() - startDate.getTime();
                    common.log("CustomerApi", "searchMobile[" + operationId + "]->loadTerminalEvents: completed, time=" + time);

                    var list = "";
                    if (result) {
                        result.forEach(function (item) {
                            if (item) list += common.getDate(new Date(item.start + (8 * 60 * 60 * 1000))) + ",";
                        });
                    }

                    tasksKeys.splice(tasksKeys.indexOf("loadTerminalEvents"), 1);
                    callback(undefined, {"termination": list});
                });
        });
        tasks.push(function (callback) {
            var startDate = new Date();
            tasksKeys.push("loadSuspendEvents");
            common.log("CustomerApi", "searchMobile[" + operationId + "]->loadSuspendEvents: starting...");

            db.scheduler.find({
                "start": {"$gt": new Date().getTime()},
                "$or": [{"action": "suspend"}, {"action": "activate"}],
                "number": number
            }).toArray(function (err, result) {
                if (err) {
                    common.error("CustomerApi", "searchMobile[" + operationId + "]->loadSuspendEvents: error=" + err.message);
                    return callback(undefined, {"suspension": [], "error": err.message});
                }

                var time = new Date().getTime() - startDate.getTime();
                common.log("CustomerApi", "searchMobile[" + operationId + "]->loadSuspendEvents: completed, time=" + time);

                var list = "";
                if(result){
                    result.forEach(function (item) {
                        if (item) list += common.getDate(new Date(item.start + (8 * 60 * 60 * 1000))) + ",";
                    });
                }

                tasksKeys.splice(tasksKeys.indexOf("loadSuspendEvents"), 1);
                callback(undefined, {"suspension": list});
            });
        });

        var freeBoostAvailable = [];
        var boostAvailableIdMap = {};
        ec.packages().boost.forEach(function (boost) {
            if (!boost.price || boost.price == 0) {
                freeBoostAvailable.push(boost);
            }
            boostAvailableIdMap[boost.id] = boost;
        });

        var result = {
            "number": number,
            "prefix": prefix,
            "bonusAvailable": ec.packages().bonus,
            "freeBoostAvailable": freeBoostAvailable
        };
        result.generalAvailable = ec.packages().general.filter((item) => {
            if(item.id == hlrManager.datavoicesms() || item.id == hlrManager.voicesms() || item.id == hlrManager.roamingOff){
                return false;
            }else{
                return true;
            }
        });

        if (cache) {
            tasks.push(function (callback) {
                var startDate = new Date();
                tasksKeys.push("loadMonkeyStatus");
                common.log("CustomerApi", "searchMobile[" + operationId + "]->loadMonkeyStatus: starting...");

                db.cache_get("cache", "monkey_" + cache.account, function (value) {
                    var time = new Date().getTime() - startDate.getTime();
                    common.log("CustomerApi", "searchMobile[" + operationId + "]->loadMonkeyStatus: completed, time=" + time);

                    db.cache_ttl("cache", "monkey_" + cache.account, function (diff) {
                        var monkey = (value) ? { "remaining" : diff, "count" : value } : { "remaining" : 0, "count" : 0 };

                        tasksKeys.splice(tasksKeys.indexOf("loadMonkeyStatus"), 1);
                        callback(undefined, {"cache": cache, "monkey": monkey});
                    });
                });
            });
            tasks.push(function (callback) {
                var startDate = new Date();
                tasksKeys.push("loadCreditCardInfoEComm");
                common.log("CustomerApi", "searchMobile[" + operationId + "]->loadCreditCardInfoEComm: starting...");

                var path = "/api/mobile/v1/latest_creditcard.json";
                var params = {"account_number": cache.account};
                paas.connect("get", path, params, function (err, result) {
                    var time = new Date().getTime() - startDate.getTime();
                    common.log("CustomerApi", "searchMobile[" + operationId + "]->loadCreditCardInfoEComm: completed, time=" + time);

                    tasksKeys.splice(tasksKeys.indexOf("loadCreditCardInfoEComm"), 1);
                    if (!err && result && (result.code == 0)) {
                        callback(undefined, {"creditcard": result.data});
                    } else {
                        callback(undefined);
                    }
                });
            });
            tasks.push(function (callback) {
                var startDate = new Date();
                tasksKeys.push("loadBillingInfoEComm");
                common.log("CustomerApi", "searchMobile[" + operationId + "]->loadBillingInfoEComm: starting...");

                var path = "/api/mobile/v1/billing_address";
                var params = {"activity": "get", "account_number": cache.account};
                paas.connect("post", path, params, function (err, result) {
                    var time = new Date().getTime() - startDate.getTime();
                    common.log("CustomerApi", "searchMobile[" + operationId + "]->loadBillingInfoEComm: completed, time=" + time);

                    tasksKeys.splice(tasksKeys.indexOf("loadBillingInfoEComm"), 1);
                    if (!err && result && (result.code == 0)) {
                        callback(undefined, {"billingaddress": result.data});
                    } else {
                        callback(undefined);
                    }
                });
            });
            tasks.push(function (callback) {
                var startDate = new Date();
                tasksKeys.push("loadEcommBlacklist");
                common.log("CustomerApi", "searchMobile[" + operationId + "]->loadEcommBlacklist: starting...");
                ecommManager.checkBlacklist(cache.billingAccountNumber, function (err, result) {
                    var time = new Date().getTime() - startDate.getTime();
                    common.log("CustomerApi", "searchMobile[" + operationId + "]->loadEcommBlacklist: completed, time=" + time);
                    tasksKeys.splice(tasksKeys.indexOf("loadEcommBlacklist"), 1);
                    if (result && result.status == 0) {
                        callback(undefined, { "ecommBlacklist" : "yes" });
                    } else if (result && result.status == 1) {
                        callback(undefined, { "ecommBlacklist" : "no" });
                    } else {
                        callback(undefined, { "ecommBlacklist" : "unknown" });
                    }
                });
            });
            tasks.push(function (callback) {
                var startDate = new Date();
                tasksKeys.push("loadPortinNotifications");
                common.log("CustomerApi", "searchMobile[" + operationId + "]->loadPortinNotifications: starting...");

                db.notifications_logbook_get({
                        "serviceInstanceNumber": cache.serviceInstanceNumber,
                        "activity": new RegExp("port", "i")
                    }, {
                        "ts": 1,
                        "activity": 1
                    }, {"_id": -1},
                    function (items) {
                        var time = new Date().getTime() - startDate.getTime();
                        common.log("CustomerApi", "searchMobile[" + operationId + "]->loadPortinNotifications: completed, time=" + time);

                        tasksKeys.splice(tasksKeys.indexOf("loadPortinNotifications"), 1);
                        callback(undefined, {"porting": items});
                    });
            });
            tasks.push(function (callback) {
                var startDate = new Date();
                tasksKeys.push("loadPortinRequest");
                common.log("CustomerApi", "searchMobile[" + operationId + "]->loadPortinRequest: starting...");

                portInManager.loadRequest(cache.serviceInstanceNumber, {status: "ALL"}, function (err, result) {
                    var time = new Date().getTime() - startDate.getTime();
                    common.log("CustomerApi", "searchMobile[" + operationId + "]->loadPortinRequest: completed, time=" + time);

                    tasksKeys.splice(tasksKeys.indexOf("loadPortinRequest"), 1);
                    if (err) {
                        callback(undefined, {"portInRequests": []});
                    } else {
                        callback(undefined, {"portInRequests": result.data});
                    }
                }, 10, 0);
            });
            tasks.push(function (callback) {
                var startDate = new Date();
                tasksKeys.push("loadPortoutRequest");
                common.log("CustomerApi", "searchMobile[" + operationId + "]->loadPortoutRequest: starting...");

                portOutManager.loadRequest(cache.serviceInstanceNumber, {status: "DONE"}, function (err, result) {
                    var time = new Date().getTime() - startDate.getTime();
                    common.log("CustomerApi", "searchMobile[" + operationId + "]->loadPortoutRequest: completed, time=" + time);

                    tasksKeys.splice(tasksKeys.indexOf("loadPortoutRequest"), 1);
                    if (err) {
                        callback(undefined, {"portOutRequests": []});
                    } else {
                        callback(undefined, {"portOutRequests": result.data});
                    }
                }, 10, 0);
            });
            tasks.push(function (callback) {
                var startDate = new Date();
                tasksKeys.push("loadCreditCapPayments");
                common.log("CustomerApi", "searchMobile[" + operationId + "]->loadCreditCapPayments: starting...");

                db.notifications_credit_cap
                    .find({billingAccountNumber: cache.billingAccountNumber}, {ts: 1, billingAccountNumber: 1, amount: 1, status: 1})
                    .sort({ts: -1})
                    .limit(100)
                    .toArray((err, items) => {
                        var time = new Date().getTime() - startDate.getTime();
                        common.log("CustomerApi", "searchMobile[" + operationId + "]->loadCreditCapPayments: completed, time=" + time);

                        tasksKeys.splice(tasksKeys.indexOf("loadCreditCapPayments"), 1);
                        callback(undefined, {"pay200": items});
                    });
            });
            tasks.push(function (callback) {
                var startDate = new Date();
                tasksKeys.push("loadCustomerCharger");
                common.log("CustomerApi", "searchMobile[" + operationId + "]->loadCustomerCharger: starting...");

                elitecoreBillService.loadCustomerUsage(cache.serviceInstanceNumber, (err, result) => {
                    var time = new Date().getTime() - startDate.getTime();
                    common.log("CustomerApi", "searchMobile[" + operationId + "]->loadCustomerCharger: completed, time=" + time);

                    tasksKeys.splice(tasksKeys.indexOf("loadCustomerCharger"), 1);
                    callback(undefined, {"pay200Charges": result});
                });
            });
            tasks.push(function (callback) {
                var startDate = new Date();
                tasksKeys.push("loadCustomerAdvancedPayments");
                common.log("CustomerApi", "searchMobile[" + operationId + "]->loadCustomerAdvancedPayments: starting...");

                billManager.loadAdvancedPayments(cache.billingAccountNumber, {filterAmountCents: [20000, 40000]}, function (err, result) {
                    var time = new Date().getTime() - startDate.getTime();
                    common.log("CustomerApi", "searchMobile[" + operationId + "]->loadCustomerAdvancedPayments: completed, time=" + time);

                    tasksKeys.splice(tasksKeys.indexOf("loadCustomerAdvancedPayments"), 1);
                    callback(undefined, {"pay200Payments": result});
                });
            });
            tasks.push(function (callback) {
                var startDate = new Date();
                tasksKeys.push("checkNotificationsBlock");
                common.log("CustomerApi", "searchMobile[" + operationId + "]->checkNotificationsBlock: starting...");

                notificationManager.suppressNotificationsStatus(cache.number, cache.serviceInstanceNumber, function(cache) {
                    var time = new Date().getTime() - startDate.getTime();
                    common.log("CustomerApi", "searchMobile[" + operationId + "]->checkNotificationsBlock: completed, time=" + time);

                    tasksKeys.splice(tasksKeys.indexOf("checkNotificationsBlock"), 1);
                    callback(undefined, {"noNotif": cache});
                });
            });
            tasks.push(function (callback) {
                var startDate = new Date();
                tasksKeys.push("loadFreeBoost");
                common.log("CustomerApi", "searchMobile[" + operationId + "]->loadFreeBoost: starting...");

                var query = "SELECT * FROM promoBoost WHERE service_instance_number="
                    + db.escape(cache.serviceInstanceNumber) + " ORDER BY added_ts";
                db.query_noerr(query, function (result) {
                    var items = [];
                    if (result) {
                        result.forEach(function (freeBoost) {
                            var boost = boostAvailableIdMap[freeBoost.boost_product_id];
                            if (boost) {
                                freeBoost.name = boost.name;
                                if (!freeBoost.name) {
                                    freeBoost.name = freeBoost.boost_product_id;
                                }
                                items.push(freeBoost);
                            }
                        });
                    }

                    var time = new Date().getTime() - startDate.getTime();
                    common.log("CustomerApi", "searchMobile[" + operationId + "]->loadFreeBoost: completed, time=" + time);

                    tasksKeys.splice(tasksKeys.indexOf("loadFreeBoost"), 1);
                    callback(undefined, {"freeBoost": items});
                });
            });
            tasks.push(function (callback) {
                var startDate = new Date();
                tasksKeys.push("loadAvailableBonusList");
                common.log("CustomerApi", "searchMobile[" + operationId + "]->loadAvailableBonusList: starting...");

                bonusManager.loadAvailableBonusList(cache.prefix, cache.number, false, function (err, items) {
                    var time = new Date().getTime() - startDate.getTime();
                    common.log("CustomerApi", "searchMobile[" + operationId + "]->loadAvailableBonusList: completed, time=" + time);

                    tasksKeys.splice(tasksKeys.indexOf("loadAvailableBonusList"), 1);
                    callback(undefined, {"bonusesOnDemand": items});
                });
            });
            tasks.push(function (callback) {
                var startDate = new Date();
                tasksKeys.push("loadHistoryBonusList");
                common.log("CustomerApi", "searchMobile[" + operationId + "]->loadHistoryBonusList: starting...");

                bonusManager.loadHistoryBonusList(cache.prefix, cache.number, false, function (err, items) {
                    var time = new Date().getTime() - startDate.getTime();
                    common.log("CustomerApi", "searchMobile[" + operationId + "]->loadHistoryBonusList: completed, time=" + time);

                    tasksKeys.splice(tasksKeys.indexOf("loadHistoryBonusList"), 1);
                    callback(undefined, {"bonusHistory": items});
                });
            });
            tasks.push(function (callback) {
                var startDate = new Date();
                tasksKeys.push("loadGoldenTicketsList");
                common.log("CustomerApi", "searchMobile[" + operationId + "]->loadGoldenTicketsList: starting...");

                leaderboardManager.loadGoldenTicketsListBySI(cache.serviceInstanceNumber, function (err, items) {
                    var time = new Date().getTime() - startDate.getTime();
                    common.log("CustomerApi", "searchMobile[" + operationId + "]->loadGoldenTicketsList: completed, time=" + time);

                    tasksKeys.splice(tasksKeys.indexOf("loadHistoryBonusList"), 1);
                    callback(undefined, {"goldenTicketsHistory": items});
                });
            });
            tasks.push(function (callback) {
                var startDate = new Date();
                tasksKeys.push("loadSetting");
                common.log("CustomerApi", "searchMobile[" + operationId + "]->loadSetting: starting...");

                profileManager.loadSetting(cache.prefix, cache.number, "selfcare", undefined, function (err, result) {
                    var time = new Date().getTime() - startDate.getTime();
                    common.log("CustomerApi", "searchMobile[" + operationId + "]->loadSetting: completed, time=" + time);

                    tasksKeys.splice(tasksKeys.indexOf("loadSetting"), 1);
                    callback(undefined, {selfcaresettings: result ? result.settings : []});
                });
            });
            tasks.push(function (callback) {
                var startDate = new Date();
                tasksKeys.push("loadPayments");
                common.log("CustomerApi", "searchMobile[" + operationId + "]->loadPayments: starting...");

                billManager.loadPayments(cache.billingAccountNumber, function (err, payments) {
                    var time = new Date().getTime() - startDate.getTime();
                    common.log("CustomerApi", "searchMobile[" + operationId + "]->loadPayments: completed, time=" + time);

                    tasksKeys.splice(tasksKeys.indexOf("loadPayments"), 1);
                    callback(undefined, {
                        payments: payments
                    });
                });
            });
            tasks.push(function (callback) {
                var startDate = new Date();
                tasksKeys.push("loadUserReferralCode");
                common.log("CustomerApi", "searchMobile[" + operationId + "]->loadUserReferralCode: starting...");

                referralManager.loadUserReferralCode(cache.prefix, cache.number, function (err, result) {
                    var time = new Date().getTime() - startDate.getTime();
                    common.log("CustomerApi", "searchMobile[" + operationId + "]->loadUserReferralCode: completed, time=" + time);

                    tasksKeys.splice(tasksKeys.indexOf("loadUserReferralCode"), 1);
                    callback(undefined, {"referrals": result});
                });
            });
            tasks.push(function (callback) {
                var customerOptionsQuery = "select customerOptions.option_key, customerOptions.value from customerOptions left join " +
                "customerProfile on customerOptions.profile_id = customerProfile.id where customerProfile.service_instance_number = '" +
                cache.serviceInstanceNumber + "' and customerOptions.option_key is not null";
                var customerOptions = {};
                db.query_noerr(customerOptionsQuery, function(results) {
                    if(results && results.length > 0){
                        results.forEach(function(result){
                            customerOptions[result.option_key] = result.value;
                        });
                    }
                    callback(undefined, {"customerOptions": customerOptions});
                });
            });

            tasks.push(function(callback) {
                ec.getCustomerDetailsNumber(prefix, number, false, (err, result) => {
                    if (!result) {
                        return callback(undefined, {"bonusAvail": "Not available"});
                    }

                    bonuses.getDataFromManager(result)
                        .then(response => {
                            if (response && response.bonus_banner_data) {
                                let bonus = response.bonus_banner_data.earned_data.value;

                                callback(undefined, {"bonusAvail": bonus});
                            }else {
                                callback(undefined, {"bonusAvail": "Not available"});
                            }
                        })
                        .catch(error => {
                            common.log("Error Occurred => ", error);
                            callback(undefined, {"bonusAvail": "Not available"});
                        });
                });
            });
        }

        var startDate = new Date();
        common.log("CustomerApi", "searchMobile[" + operationId + "]->executeAsyncTasks: starting...");
        async.parallel(tasks, function (err, asyncResult) {
            var time = new Date().getTime() - startDate.getTime();
            common.log("CustomerApi", "searchMobile[" + operationId + "]->executeAsyncTasks: completed, time=" + time);

            if (err) common.log("searchMobile", JSON.stringify(err));

            asyncResult.forEach(function (item) {
                if (item) {
                    if (item.cache) result.cache = item.cache;
                    if (item.monkey) result.monkey = item.monkey;
                    if (item.pins) result.pins = item.pins;
                    if (item.apps) result.apps = item.apps;
                    if (item.appview) result.appview = item.appview;
                    if (typeof(item.credit) != "undefined") {
                        result.credit = item.credit;
                        result.creditSGD = item.creditSGD;
                        result.plan_id = item.plan_id;
                        result.plans = item.plans;
                        if (item.mo) {
                            if (!result.scp) result.scp = new Object;
                            result.scp.mo = item.mo;
                        }
                        if (item.mt) {
                            if (!result.scp) result.scp = new Object;
                            result.scp.mt = item.mt;
                        }
                    }
                    if (item.creditcard) result.creditcard = item.creditcard;
                    if (item.billingaddress) result.billingaddress = item.billingaddress;
                    if (item.pay200) result.pay200 = item.pay200;
                    if (item.pay200Charges) result.pay200Charges = item.pay200Charges;
                    if (item.pay200Payments) result.pay200Payments = item.pay200Payments;
                    if (item.porting) result.porting = item.porting;
                    if (item.portInRequests) result.portInRequests = item.portInRequests;
                    if (item.portOutRequests) result.portOut = item.portOutRequests.length;
                    if (item.termination) result.termination = item.termination;
                    if (item.payments) result.payments = item.payments;
                    if (item.suspension) result.suspension = item.suspension;
                    if (item.freeBoost) result.freeBoost = item.freeBoost;
                    if (item.referrals) result.referrals = item.referrals;
                    if (item.customerOptions) result.customerOptions = item.customerOptions;
                    if (item.bonusesOnDemand) result.bonusesOnDemand = item.bonusesOnDemand;
                    if (item.bonusHistory) result.bonusHistory = item.bonusHistory;
                    if (item.goldenTicketsHistory) result.goldenTicketsHistory = item.goldenTicketsHistory;
                    if (item.noNotif) result.noNotif = item.noNotif;
                    if (item.selfcaresettings) result.selfcaresettings = item.selfcaresettings;
                    if (item.ecommBlacklist) result.ecommBlacklist = item.ecommBlacklist;
                    if (item.bonusAvail) result.bonusAvail = item.bonusAvail;
                }
            });
            callback(undefined, result);
        });
    };

    if (number) {
        ec.getCustomerDetailsNumber(prefix, number, false, loadCache);
    } else {
        elitecoreUserService.loadUserInfoByInternalId(account, loadCache);
    }
}

function updateRoaming(req, res) {
    web.checkPermission("customers", req.gid, function (allowed) {
        if (!allowed || (allowed.write < 2)) {
            res.status(403).json({"code": -1});
        } else {
            var number = req.fields.number;
            var prefix = req.fields.prefix;
            var type = req.fields.type;
            var callback = function(err, results){
                db.hlrlog.insert({
                    "ts": new Date().getTime(),
                    "ip": req.ip,
                    "number": number,
                    "action": type,
                    "username": req.user,
                    "err": err ? err.message : "",
                    "code": err ? -1 : 0
                });
                if(err){
                    return res.status(400).json({
                        code: -1,
                        error: err.message
                    });
                } else {
                    return res.json({
                        code: 0,
                        message: "Success"
                    });
                }
            }
            if (type == "off") {
                hlrManager.set(hlrManager.roamingOff, prefix, number, true, callback);
            } else if (type == "datavoicesms") {
                hlrManager.set(hlrManager.datavoicesms(), prefix, number, true, callback);
            } else if (type == "voicesms") {
                hlrManager.set(hlrManager.voicesms(), prefix, number, true, callback);
            }
        }
    });
}

function updateAddon(req, res) {
    web.checkPermission("customers", req.gid, function (allowed) {
        if (!allowed || (allowed.write < 2)) {
            res.status(403).json({"code": -1});
        } else {
            var number = req.fields.number;
            var prefix = req.fields.prefix;
            var product = req.fields.product;
            var paymentsDisabled = req.fields.paymentsDisabled == "true";
            var overrideEffect = req.fields.overrideEffect;
            var overrideRecurrent = req.fields.overrideRecurrent;
            var historyId = (req.fields && req.fields.historyId) ? [req.fields.historyId] : undefined;
            var options = {
                paymentsDisabled: paymentsDisabled
            }
            if (!number || !prefix || !product) {
                return res.json({
                    "code": -1, "number": number, "prefix": prefix,
                    "write": allowed.write, "error": "Incomplete Parameters"
                });
            }
            var add = function (callback) {
                if (product == hlrManager.roamingOff) { // off
                    hlrManager.set(hlrManager.roamingOff, prefix, number, true, callback);
                } else if (product == hlrManager.datavoicesms()) { // data voice and sms
                    hlrManager.set(hlrManager.datavoicesms(), prefix, number, true, callback);
                } else if (product == hlrManager.voicesms()) { // voice and sms
                    hlrManager.set(hlrManager.voicesms(), prefix, number, true, callback);
                } else {
                    packageManager.addonUpdate(prefix, number, req.method, product, historyId, true, callback, overrideEffect, overrideRecurrent, options);
                }
            }
            add(function (err, result) {
                var code = 0;
                if (!err && result) {
                    ec.getCustomerDetailsNumber(prefix, number, true, function (err, cache) {
                        if(!res.headersSent){
                            res.json({
                                "code": code, "number": number, "prefix": prefix,
                                "write": allowed.write
                            });
                        }
                    });
                } else {
                    code = -1;
                    if(!res.headersSent){
                        res.json({
                            "code": code, "number": number, "prefix": prefix,
                            "write": allowed.write, "error": err.message
                        });
                    }
                }
                if(product == hlrManager.roamingOff || product == hlrManager.datavoicesms() || product == hlrManager.voicesms()){
                    db.hlrlog.insert({
                        "ts": new Date().getTime(),
                        "ip": req.ip,
                        "number": number,
                        "action": (product == hlrManager.roamingOff) ? "roamingOff" : (product == hlrManager.datavoicesms()) ? "roamingDataVoiceSms": (product == hlrManager.voicesms()) ? "roamingVoiceSms": "N/A",
                        "username": req.user,
                        "err": err,
                        "code": err ? -1 : 0
                    });
                }else{
                    db.weblog.insert({
                        "ts": new Date().getTime(),
                        "ip": req.ip,
                        "type": "updateAddon",
                        "id": common.escapeHtml(number),
                        "action": common.escapeHtml(product + " - " + historyId),
                        "username": req.user,
                        "result": result,
                        "err": err,
                        "code": code
                    });
                }
            });
        }
    });
}

function updateRoamingCap(req, res) {
    web.checkPermission("customers", req.gid, function (allowed) {
        if (!allowed || (allowed.write < 2)) {
            res.status(403).json({"code": -1});
        } else {
            var number = req.fields.number;
            var prefix = req.fields.prefix;
            var limit = parseInt(req.fields.limit);

            creditCapManager.updateRoamingCap(prefix, number, limit, function (err, result) {
                if (err) {
                    return res.status(400).json({
                        code: -1,
                        error: err.message,
                        status: err.status,
                        number: number,
                        prefix: prefix,
                        write: allowed.write

                    });
                }

                res.json({
                    code: 0,
                    number: number,
                    prefix: prefix,
                    write: allowed.write
                });


                db.weblog.insert({
                    "ts": new Date().getTime(),
                    "ip": req.ip,
                    "type": "updateRoamingCap",
                    "id": common.escapeHtml(number),
                    "action": common.escapeHtml("Set Cap " + limit),
                    "username": req.user,
                    "result": result,
                    "err": err,
                    "limit": limit
                });
            });
        }
    });
}

function makePayment(req, res) {
    web.checkPermission("customers", req.gid, function (allowed) {
        if (!allowed || (allowed.write < 2)) {
            res.status(403).json({"code": -1});
        } else {
            var number = req.fields.number;
            var prefix = req.fields.prefix;
            var account = req.fields.account;
            var amount = parseFloat(req.fields.amount);
            var invoiceId = req.fields.invoiceId;
            var invoicesInfo = req.fields.invoicesInfo ? JSON.parse(req.fields.invoicesInfo) : undefined;

            ec.getCustomerDetailsAccount(account, false, function (err, cache) {
                if (err || !cache) {
                    return res.status(400).json({
                        code: -1,
                        error: "Customer not found",
                        write: allowed.write
                    });
                }

                paymentManager.makeBillPaymentByBA(cache.billingAccountNumber,
                    invoicesInfo ? invoicesInfo : [{invoiceId: invoiceId, amount: amount}],
                    {platform: "CMS", ignoreSuspension: true},
                    function (err, result) {
                        if (err) {
                            return res.status(400).json({
                                code: -1,
                                error: err.message,
                                status: err.status,
                                number: number,
                                prefix: prefix,
                                write: allowed.write

                            });
                        }

                        if (!result || !result.paymentResults || !result.paymentResults[0]
                            || !result.paymentResults[0].paymentStatus) {
                            return res.status(400).json({
                                code: -1,
                                error: "Transaction status missing",
                                status: "ERROR_NO_TRANSACTION_STATUS",
                                number: number,
                                prefix: prefix,
                                write: allowed.write
                            });
                        }

                        if (result.paymentResults[0].paymentStatus != 'OK') {
                            return res.status(400).json({
                                code: -1,
                                error: "Payment failed due to " + result.paymentResults[0].paymentStatus,
                                status: result.paymentResults[0].paymentStatus,
                                number: number,
                                prefix: prefix,
                                write: allowed.write
                            });
                        }

                        res.json({
                            code: 0,
                            number: number,
                            prefix: prefix,
                            write: allowed.write
                        });
                    });
            });


        }
    });
}

function removeAppRegistration(req, res){
    appManager.removeDevice(req.fields.userKey, function(err, result){
        if(err){
            res.status(403).json({"code": -1});
        }else{
            res.json({"code": 0});
        }
    });
}

function getCustomerStatusSchedules(req, res){
    db.scheduler.find({"serviceInstanceNumber": req.params.serviceInstanceNumber, "start": {"$exists": true}, action: {"$in": ["activate", "suspend", "terminate"]}}).toArray(function(err, items){
        if(err){
            res.status(400).json({"code": -1, "error": "Failed to fetch list"});
        }else{
            res.json({
                "code": 0,
                "list": items
            });
        }
    });
}

function updateFreeBoost(req, res) {
    web.checkPermission("customers", req.gid, function (allowed) {
        if (!allowed || (allowed.write < 2)) {
            res.status(403).json({"code": -1});
        } else {
            var number = req.fields.number;
            var prefix = req.fields.prefix;
            var count = parseInt(req.fields.count);
            var account = req.params.account;
            var serviceInstance = req.params.serviceInstance;
            var productId = req.fields.id;
            var product = ec.findPackage(ec.packages(), req.params.id);

            if (!number || !prefix || !serviceInstance || !account || count <= 0) {
                return res.status(400).json({"code": -1, "error": "Params are incorrect"});
            }

            if (!product) {
                return res.status(400).json({"code": -1, "error": "Product not found"});
            }

            boostManager.addFreeBoost(productId, count, prefix, number, config.OPERATIONS_KEY,
                function (error, result) {
                    if (error) {
                        res.status(400).json({
                            "code": -1,
                            "status": error.id,
                            "error": error.message
                        });
                    } else {
                        res.json({
                            "code": 0,
                            "prefix": prefix,
                            "number": number
                        });
                        db.weblog.insert({
                            "ts": new Date().getTime(),
                            "ip": req.ip,
                            "type": "updateFreeBoost",
                            "id": common.escapeHtml(number),
                            "action": common.escapeHtml(productId),
                            "username": req.user,
                            "code": 0
                        });
                    }
                });
        }
    });
}

function updateMonkey(req, res) {
    web.checkPermission("customers", req.gid, function (allowed) {
        if (!allowed || (allowed.write < 2)) {
            res.status(403).json({"code": -1});
        } else {
            var number = req.fields.number;
            var prefix = req.fields.prefix;
            var account = req.params.account;
            var sec = req.fields.sec;
            if (number && prefix) {
                db.cache_get("cache", "monkey_" + account, function (count) {
                    if (!count) count = 5;
                    else count++;
                    db.cache_put("cache", "monkey_" + account, count, sec * 1000);
                    res.json({"code": 0, "number": number, "prefix": prefix, "write": allowed.write});
                    db.weblog.insert({
                        "ts": new Date().getTime(),
                        "ip": req.ip,
                        "type": "updateMonkey",
                        "id": common.escapeHtml(number),
                        "action": common.escapeHtml(sec),
                        "username": req.user,
                        "code": 0
                    });
                });
            } else {
                res.status(400).json({"code": -1});
            }
        }
    });
}

function updateDOB(req, res) {
    web.checkPermission("customers", req.gid, function (allowed) {
        if (!allowed || (allowed.write < 2)) {
            res.status(403).json({"code": -1});
        } else {
            var account = req.params.account;
            var number = req.fields.number;
            var prefix = req.fields.prefix;
            var dob = new Date(req.fields.dob);
            if (number && prefix) {
                ec.updateCustomerAccountData({
                        "accountNumber": account,
                        "accountProfiles": {"datecustom1": common.getDate(dob) + "T12:00:00.000Z"}
                    },
                    function (err, result) {
                        ec.setCache(number, prefix, null, account, function (err, obj) {
                            res.json({"code": 0, "number": number, "prefix": prefix, "write": allowed.write});
                            db.weblog.insert({
                                "ts": new Date().getTime(),
                                "ip": req.ip,
                                "type": "updateDOB",
                                "id": common.escapeHtml(number),
                                "action": common.escapeHtml(dob),
                                "username": req.user,
                                "code": 0
                            });
                        });
                    });
            }
        }
    });
}

function recache(req, res) {
    web.checkPermission("customers", req.gid, function (allowed) {
        if (!allowed || !allowed.write) {
            res.status(403).json({"code": -1});
        } else {
            var number = req.fields.number;
            var prefix = req.fields.prefix;
            var account = req.params.account;
            var reload = req.fields.reload;
            if (reload == "true") {
                ec.setCache(number, prefix, undefined, account, function (err, result) {
                    if (result) {
                        res.json({"code": 0, "number": number, "prefix": prefix, "reload": reload, "write": allowed.write});
                    } else {
                        res.status(500).json({"code": -1});
                    }
                });
            } else {
                db.cache_del("account_cache", account);
                res.json({"code": 0, "number": number, "prefix": prefix, "reload": reload, "write": allowed.write});
            }
            db.weblog.insert({
                "ts": new Date().getTime(),
                "ip": req.ip,
                "type": "recache",
                "id": common.escapeHtml(number),
                "action": common.escapeHtml(reload),
                "username": req.user,
                "code": 0
            });
        }
    });
}

function pay200(req, res) {
    web.checkPermission("customers", req.gid, function (allowed) {
        if (!allowed || (allowed.write < 2)) {
            res.status(403).json({"code": -1});
        } else {
            var number = req.fields.number;
            var prefix = req.fields.prefix;
            var delayed = req.fields.delayed == "true";
            var warningOnly = req.fields.warningOnly == "true";
            var overrideUsage = req.fields.overrideUsage == "true";
            var overrideLimit = req.fields.overrideLimit == "true";
            var overrideLock = req.fields.overrideLock == "true";
            var overridePaymentCents = req.fields.overridePaymentCents ? parseInt(req.fields.overridePaymentCents) : undefined;
            var overrideDelay = req.fields.overrideDelay ? parseInt(req.fields.overrideDelay) : undefined;
            var initiator = "[CMS][" + req.user + "]";

            ec.getCustomerDetailsNumber(prefix, number, false, function (err, cache) {
                if (err || !cache) {
                    return res.status(400).json({code: -1, error: "Can not load account info"});
                }

                var paymentHandler = delayed
                    ? paymentManager.queueCreditCapPaymentByBA
                    : paymentManager.makeCreditCapPaymentByBA;

                paymentHandler(cache.billingAccountNumber, {
                    platform: "CMS",
                    initiator: initiator,
                    warningOnly: warningOnly,
                    overridePaymentCents: overridePaymentCents,
                    overrideDelay: overrideDelay,
                    overrideLimit: overrideLimit,
                    overrideUsage: overrideUsage,
                    overrideLock: overrideLock
                }, (err) => {
                    if (err) {
                        return res.status(400).json({code: -1, error: err.message});
                    }

                    res.json({"code": 0, message: !delayed ? "Cap Payment has been processed" : "Cap Payment has been queued"});
                });

                db.weblog.insert({
                    "ts": new Date().getTime(),
                    "ip": req.ip,
                    "type": "payment",
                    "action": "pay200",
                    "id": common.escapeHtml(cache.serviceInstanceNumber),
                    "username": req.user,
                    "code": 0
                });
            });
        }
    });
}

function changeStatus(req, res) {
    web.checkPermission("customers", req.gid, function (allowed) {
        if (!allowed || (allowed.write < 3)) {
            res.status(403).json({"code": -1});
        } else {
            var number = req.fields.number;
            var prefix = req.fields.prefix;
            var account = req.params.account;
            var serviceInstance = req.params.serviceInstance;
            var type = req.fields.type;

            var volunteer = (req.fields) ? req.fields.volunteer === "true" : false;
            var effective = (req.fields && req.fields.effective) ? new Date(req.fields.effective).getTime() + 8 * 60 * 60 * 1000 : new Date().getTime();
            var months = (req.fields && req.fields.months) ? parseInt(req.fields.months) : undefined;
            var perMonthAmount = (req.fields && req.fields.perMonthAmount) ? req.fields.perMonthAmount : undefined;
            var executionKey = config.OPERATIONS_KEY;
            var earliestTerminationDate = (req.fields && req.fields.earliestTerminationDate) ? new Date(new Date(req.fields.earliestTerminationDate).getTime() + 8 * 60 * 60 * 1000).toISOString() : undefined;
            var logSuspentionAction = function(logMessage, actionResult){
                db.weblog.insert({
                    "ts": new Date().getTime(),
                    "ip": req.ip,
                    "type": type,
                    "id": common.escapeHtml(number),
                    "action": logMessage,
                    "username": req.user,
                    "result": actionResult,
                    "code": 0
                });
            }

            var changeStatusInternal = function () {
                if (type == "Terminated") {
                    accountManager.scheduleTermination(serviceInstance, effective, req.user, executionKey,
                        function (err, result) {
                            if(err){
                                logSuspentionAction("Termination", "Error occurred from account manager");
                            }else{
                                logSuspentionAction("Termination", "Success");
                            }
                            res.json({"code": err ? -1 : 0, "number": number, "prefix": prefix, "write": allowed.write});
                        });
                } else if (type == "Suspended" && months > 0) {
                    effective = (new Date(new Date(earliestTerminationDate).getTime() - (16 * 60 * 60 * 1000) - 1000)).getTime();
                    accountManager.scheduleTemporarySuspension(serviceInstance, effective, months, perMonthAmount, req.user, executionKey,
                        function (err, result) {
                            if(err){
                                logSuspentionAction("Suspention Volunteer", err.description);
                                res.status(400).json({
                                    "code": -1,
                                    "status": err.code,
                                    "error": err.description
                                });
                            }else{
                                logSuspentionAction("Suspention Volunteer", "Success");
                                res.json({"code": 0, "number": number, "prefix": prefix, "write": allowed.write});
                            }
                        });
                } else if ((type == "Suspended") || (type == "Active")) {
                    var logMessage = (type == "Active") ? "Activating user": "Suspention Non Volunteer";
                    effective = new Date("1970-01-01");
                    var reason = months == -1 ? "SUSPENDED_DUE_TO_LOST_SIM"
                        : months == -2 ? "SUSPENDED_DUE_TO_NON_PAYMENT"
                        : months == 0 ? "SUSPENDED_MANUAL" : undefined;
                    var initiator = "[CMS][" + req.user + "]";
                    accountActions.changeStatus(serviceInstance, type, "CS Level3", reason, initiator, executionKey,
                        function (err, result) {
                            if(err){
                                logSuspentionAction(logMessage, "Error occoured from account manager");
                            }else{
                                logSuspentionAction(logMessage, "Success");
                            }
                            ec.setCache(number, prefix, undefined, account, function (err, cache) {
                                res.json({
                                    code: err || !result || !result.statusChanged ? -1 : 0, number: number,
                                    prefix: prefix, write: allowed.write
                                });
                            });
                        });
                } else {
                    res.json({"code": -1, "number": number, "prefix": prefix, "write": allowed.write});
                }

            }

            if (type === "Suspended") {
                profileManager.saveSetting(prefix, number, 'selfcare', 'voluntary_suspension',
                    'Integer', volunteer ? 1 : 0, executionKey,
                    function (err, result) {
                        if (err) {
                            return res.json({
                                "code": -1,
                                "number": number,
                                "prefix": prefix,
                                "write": allowed.write
                            });
                        }else{
                            changeStatusInternal();
                        }
                    });
            } else {
                changeStatusInternal();
            }
        }
    });
}

function validateStatus(req, res) {
    web.checkPermission("customers", req.gid, function (allowed) {
        if (!allowed || (allowed.write < 3)) {
            res.status(403).json({"code": -1});
        } else {
            var account = req.params.account;
            var serviceInstance = req.params.serviceInstance;
            var number = req.fields.number;
            var prefix = req.fields.prefix;
            var delayed = req.fields.delayed == 'true';
            var immediateSuspension = req.fields.immediateSuspension == 'true';

            var options = {
                immediateSuspension: immediateSuspension
            };

            elitecoreUserService.loadUserInfoByServiceInstanceNumber(serviceInstance, function (err, cache) {
                if (err) {
                    if (err) {
                        return res.status(400).json({code: -1, status: err.status, error: err.message});
                    }
                }
                if (!cache) {
                    return res.status(400).json({code: -1, status: "ERROR_CUSTOMER_NOT_FOUND", error: "Customer not found"});
                }

                var resultHandler = (err, result) => {
                    if (err) {
                        return res.status(400).json({code: -1, status: err.status, error: err.message});
                    }
                    return res.json({code: 0, message: result && result.message ? result.message : "Done"});
                }

                if (delayed) {
                    validationQ.queueValidateStatusByBA(cache.billingAccountNumber, options, resultHandler);
                } else {
                    validation.validateStatusByBA(cache.billingAccountNumber, options, resultHandler);
                }
            });

            db.weblog.insert({
                "ts": new Date().getTime(),
                "ip": req.ip,
                "type": "validateStatus",
                "id": common.escapeHtml(number),
                "username": req.user,
                "code": 0
            });
        }
    });
}

function repair(req, res) {

    web.checkPermission("customers", req.gid, function (allowed) {
        if (!allowed || (allowed.write < 3)) {
            res.status(403).json({"code": -1});
        } else {
            var number = req.fields.number;
            var prefix = req.fields.prefix;
            var name = req.fields.name;
            var account = req.params.account;
            var delivery = req.fields.delivery;
            var serviceInstance = req.params.serviceInstance;
            var email = req.fields.email;
            var type = req.fields.type;
            var newNumber = (req.fields) ? req.fields.newNumber : undefined;
            var newIccid = (req.fields) ? req.fields.newIccid : undefined;
            var iccid = (req.fields) ? req.fields.iccid : undefined;
            var iccidMap = (req.fields) ? JSON.parse(req.fields.iccidMap) : undefined;
            iccid = (iccid && !iccidMap) ? iccid : iccidMap[number];
            var category = (req.fields) ? req.fields.category : undefined;
            var billing_cycle = (req.fields) ? req.fields.billing_cycle : undefined;
            if (type == "sim") {
                repairManager.checkEcommStatusForRepairRequest(serviceInstance, function(err, results){
                    if(err){
                        return res.status(400).json({code: -1, error: err.message});
                    }else{
                        portInManager.loadActiveRequestByServiceInstanceNumber(serviceInstance, {}, (err, request) => {
                            if (request && (request.status === 'NOT_FOUND' || request.status === 'WAITING')) {
                                repairManager.simChange(serviceInstance, number, iccid, newIccid, function (err, result) {
                                    var code = 0;
                                    if (err) {
                                        code = -1;
                                        common.error("repair", err.message);
                                    } else {
                                        repairManager.inventoryAlert();
                                        if(delivery == "true"){
                                            repairManager.updateSimChangeStatusAndGetLinkFromEcomm(serviceInstance, "SUCCESS", newIccid, "DAMAGED", function(err2, results){
                                                if(err2 || results.code != 0){
                                                    common.error("notifiy Ecomm for sim repair", "SIM_CHANGE_NOTIFY_FAILURE");
                                                }else{
                                                    var notification = {
                                                        activity: (req.fields.charge == "true") ? "sim_loss_schedule_delivery" : "damaged_sim_schedule_delivery",
                                                        name: name,
                                                        prefix: prefix,
                                                        number: number,
                                                        email: email,
                                                        teamID: 4,
                                                        teamName: "Operations",
                                                        sim_schedule_url: results.schedule_delivery_url
                                                    };
                                                    notificationSend.deliver(notification, null, function(err3, result){
                                                        if(err3){
                                                            common.error("Notification", "Failed to send sim redel link");
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                        if(req.fields.charge == "true"){
                                            packageManager.addonUpdate(prefix, number, "PUT", "PRD00548", undefined, true, function (err, result) {
                                                db.weblog.insert({
                                                    "ts": new Date().getTime(),
                                                    "ip": req.ip,
                                                    "type": "updateAddon",
                                                    "id": common.escapeHtml(number),
                                                    "action": "Adding sim change charge (PRD00548)",
                                                    "username": req.user,
                                                    "result": result,
                                                    "err": err,
                                                    "code": code
                                                });
                                                if(!res.headerSent){
                                                    if(err){
                                                        res.json({
                                                            "code": code, "number": number, "prefix": prefix,
                                                            "write": allowed.write, "error": err.message
                                                        });
                                                    }else{
                                                        res.json({
                                                            "code": code,
                                                            "number": number,
                                                            "prefix": prefix,
                                                            "write": allowed.write,
                                                            "error": ((err) ? err.message : undefined)
                                                        });
                                                    }
                                                }
                                            }, '0', 'false');
                                        }else{
                                            if(!res.headerSent){
                                                res.json({
                                                    "code": code,
                                                    "number": number,
                                                    "prefix": prefix,
                                                    "write": allowed.write,
                                                    "error": ((err) ? err.message : undefined)
                                                });
                                            }
                                        }
                                    }
                                    db.weblog.insert({
                                        "ts": new Date().getTime(),
                                        "ip": req.ip,
                                        "type": "repair sim",
                                        "id": common.escapeHtml(number),
                                        "action": common.escapeHtml(newNumber + " " + iccid),
                                        "username": req.user,
                                        "error": ((err) ? err.message : undefined),
                                        "code": code
                                    });
                                });
                            }else {
                                code = -1;
                                common.error("repair", "Sim change not allowed while portin is in progress");
                            }
                        })
                    }
                });
            }
            else if (type == "number") {
                if (!category) {
                    var path = "/api/v1/numbers.json";
                    var params = {"qs": JSON.parse(JSON.stringify(config.PAAS_CREDS))};
                    params.qs.number = newNumber;
                    paas.connect("get", path, params, function (err, result) {
                        if (!err && result && result.success && (result.status == "free")) {
                            var category;
                            switch (result.price) {
                                case 388:
                                    category = "GOLD";
                                    break;
                                case 53.5:
                                    category = "SILVER";
                                    break;
                                case 0:
                                    category = "NORMAL";
                                    break;
                                default:
                                    category = "UNKNOWN";
                            }

                            res.json({
                                "code": 0,
                                "message": result.message,
                                "prefix": prefix,
                                "number": number,
                                "category": category
                            });

                        } else {
                            res.json({
                                "code": -1,
                                "message": result.message,
                                "prefix": prefix,
                                "number": number,
                                "error": ((err) ? err : "Message : " + result.message + "<br>Status : " + result.status)
                            });
                        }
                    });
                } else repairManager.numberChange(serviceInstance ,number, iccid, newNumber,
                    category, billing_cycle, function (err, result) {
                        var code = 0;
                        if (err) {
                            code = -1;
                        } else {
                            db.cache_del("account_cache", account);
                            ecommManager.numberChange(serviceInstance, number, newNumber, function (err, result) {
                                if (logsEnabled) {
                                    if (err) common.error("numberChange ecomm", JSON.stringify(err));
                                    else common.log("numberChange ecomm", JSON.stringify(result));
                                }
                            });
                        }
                        if(!res.headersSent){
                            res.json({
                                "code": code,
                                "number": number,
                                "prefix": prefix,
                                "write": allowed.write,
                                "error": ((err) ? err.message : undefined)
                            });
                        }
                        db.weblog.insert({
                            "ts": new Date().getTime(),
                            "ip": req.ip,
                            "type": "repair number",
                            "id": common.escapeHtml(number),
                            "action": common.escapeHtml(newNumber + " " + iccid),
                            "username": req.user,
                            "error": ((err) ? err.message : undefined),
                            "code": code
                        });
                    });
            } else {
                res.json({"code": -1, "number": number, "prefix": prefix, "write": allowed.write, "error": "Unknown Type"});
            }
        }
    });
}

function portInNotification(req, res) {
    web.checkPermission("customers", req.gid, function (allowed) {
        if (!allowed || (allowed.write < 3)) {
            res.status(403).json({"code": -1});
        } else {
            var initiator = "[CMS][" + req.user + "]";
            var serviceInstance = req.fields.serviceInstanceNumber;
            var portInNumber = req.fields.portInNumberSelected || req.fields.portInNumberTyped;
            var portInPrefix = req.fields.prefix;
            var type = req.fields.type;

            var resultHandler = (err, result) => {
                if (err) {
                    return res.status(400).json({code: -1, error: err.message});
                }

                res.json({code: 0});
            }

            ec.getCustomerDetailsServiceInstance(serviceInstance, false, function (err, cache) {
                if (err || !cache) {
                    return resultHandler(new Error("Customer Not Found"));
                }

                if (type === "SUCCESS" || type === "FAILURE" || type === "APPROVE") {
                    notificationSend.elitecore({
                        message: "Manual port-in notification triggered",
                        number: portInNumber,
                        email: cache.email,
                        activity: (type === "SUCCESS" ? "PortInSuccess" : (type === "APPROVE" ? "PortInInterimStatus" : "PortInFailure")),
                        name: cache.billingFullName,
                        type: "data",
                        initiator: initiator
                    }, (err) => {
                        resultHandler(err);
                    });
                }
            });

            db.weblog.insert({
                "ts": new Date().getTime(),
                "ip": req.ip,
                "type": "portInNotification",
                "id": common.escapeHtml(serviceInstance),
                "action": type,
                "username": req.user,
                "code": 0
            });
        }
    });
}

function corporatePortinAction(req, res) {
    web.checkPermission("customers", req.gid, function (allowed) {
        if (!allowed || (allowed.write < 3)) {
            res.status(403).json({"code": -1});
        } else {
            portInManager.putCorporateRequest(req.fields, req.user, req.ip, function(err, result){
                if (err) {
                    res.status(400).json({
                        code: -1,
                        status: "N/A",
                        error: err
                    });
                } else {
                    res.json({
                        code: 0,
                        result: result
                    });
                }
            });
        }
    });
}

function portInAction(req, res) {
    web.checkPermission("customers", req.gid, function (allowed) {
        if (!allowed || (allowed.write < 3)) {
            res.status(403).json({"code": -1});
        } else {
            var initiator = "[CMS][" + req.user + "]";
            var serviceInstance = req.params.serviceInstance;

            var portInNumber = req.fields.portInNumberSelected || req.fields.portInNumberTyped || req.fields.portInNumber;
            var portInPrefix = req.fields.portInPrefix;
            var donorNetworkCode = req.fields.donorNetworkCode;
            var type = req.fields.type;
            var startDate = req.fields.startDate ? (req.fields.startDate === "IN_ONE_HOUR"
                ? new Date(new Date().getTime() + 90 * 60 * 1000) : new Date(req.fields.startDate)) : undefined;

            var resultHandler = (err, result) => {
                if (err) {
                    return res.status(400).json({code: -1, error: err.message});
                }

                res.json({code: 0});
            }

            ec.getCustomerDetailsServiceInstance(serviceInstance, false, function (err, cache) {
                if (err || !cache) {
                    return resultHandler(new Error("Customer Not Found"));
                }
                var planName = cache.serviceInstanceBasePlanName
                if (cache.base.id === constants.CIRCLES_SWITCH_PLAN_ID) {
                    planName = "CIRCLES_SWITCH";
                }
                if (type === "CANCEL") {
                    portInManager.cancelRequest(cache.prefix, cache.number, portInPrefix, portInNumber,
                        resultHandler, undefined, undefined, initiator);
                } else if (type === "REINIT") {
                    portInManager.putRequest(cache.prefix, cache.number, portInPrefix, portInNumber,
                        donorNetworkCode, startDate, "CMS", {
                        "initiator" : initiator,
                        "orderReferenceNumber" : cache.orderReferenceNumber,
                        "planName": planName,
                    }, resultHandler);
                }
            });

            db.weblog.insert({
                "ts": new Date().getTime(),
                "ip": req.ip,
                "type": "portInAction",
                "id": common.escapeHtml(serviceInstance),
                "action": type,
                "username": req.user,
                "code": 0
            });
        }
    });
}

function portInReportUpload(req, res) {
    web.checkPermission("customers", req.gid, function (allowed) {
        if (!allowed || (allowed.write < 3)) {
            res.status(403).json({"code": -1});
        } else {
            var initiator = "[CMS][" + req.user + "]";
            var file = req.fields.file;

            portInManager.syncGAReport(file, "temp", {
                initiator: initiator
            }, (err, result) => {
                if (err) {
                    return res.status(400).json({
                        code: -1,
                        error: err.message
                    });
                }

                res.json({
                    code: 0,
                    message: "GA report has been synced, updated "
                    + (result && result.update ? result.update.totalCount : -1) + " port-in requests"
                });
            });

            db.weblog.insert({
                "ts": new Date().getTime(),
                "ip": req.ip,
                "type": "portInReportUpload",
                "username": req.user,
                "code": 0
            });
        }
    });
}

function getSchedules(req, res) {
    web.checkPermission("customers", req.gid, function (allowed) {
        if (!allowed) {
            res.status(403).json({"code": -1});
        } else {
            var start = new Date(req.query.start).getTime();
            var end = new Date(req.query.end).getTime();
            var query = {"start": {"$gte": start, "$lt": end}};
            if (req.query.action) query.action = req.query.action;
            if (req.query.activity) query.activity = req.query.activity;
            db.scheduler.find(query).toArray(function (err, result) {
                var repeat = new Array;
                result.forEach(function (item) {
                    item.start = new Date(item.start).toISOString();
                    item.id = item._id;
                    delete item._id;
                    if (item.nextEvent) {
                        var itemEnd = (item.end) ? new Date(item.end).getTime() : end;
                        for (var next = new Date(item.start).getTime() + item.nextEvent;
                             next <= end; next += item.nextEvent) {
                            if ((next >= start) && (next <= itemEnd)) {
                                var newItem = JSON.parse(JSON.stringify(item));
                                newItem.start = new Date(next).toISOString();
                                repeat.push(newItem);
                            }
                        }
                    }
                });
                if (repeat.length > 0) result = result.concat(repeat);
                res.json(result);
            });
        }
    });
}


function addSchedules(req, res) {
    web.checkPermission("customers", req.gid, function (allowed) {
        if (!allowed || (allowed.write < 3)) {
            res.status(403).json({"code": -1});
        } else {
            // validation
            var events = (req.fields) ? common.safeParse(req.fields.events) : undefined;
            if (events && (typeof(events) == "object") && events.list) {
                events.list.forEach(function (item) {
                    Object.keys(item).forEach(function (key) {
                        if (key == "start") item[key] = parseInt(common.escapeHtml(item[key]));
                        else if (key == "allDay") item[key] = (common.escapeHtml(item[key]) == "true") ? true : false;
                        else item[key] = common.escapeHtml(item[key]);
                    });
                });
                db.scheduler.insertMany(events.list);
                res.json({"code": 0});
            } else {
                res.json({"code": -1});
            }
        }
    });
}

function moveSchedules(req, res) {
    web.checkPermission("customers", req.gid, function (allowed) {
        if (!allowed || (allowed.write < 3)) {
            res.status(403).json({"code": -1});
        } else {
            var match = {"_id": db.objectID(req.params.id)};
            var datetime = (req.fields.datetime == parseInt(req.fields.datetime)) ? parseInt(req.fields.datetime) : undefined;
            var start = (req.fields.start) ? req.fields.start : undefined;
            var processed = (req.fields.processed) ? common.safeParse(req.fields.processed) : undefined;
            var color = (req.fields.color) ? req.fields.color : undefined;
            if (datetime && start) {
                if (processed && (typeof(processed) == "object")) {
                    processed.by = req.cookies.cmsuser;
                    if (color) processed.color = color;
                } else {
                    processed = new Object;
                }
                if (start) processed.start = common.safeDate(start).getTime() - (8 * 60 * 60 * 1000);
                db.scheduler.update(match, {
                    "$set": {"start": datetime},
                    "$push": {"previous": processed}
                }, function (err, result) {
                    db.scheduler.update(match, {"$unset": {"processed": "", "color": ""}}, function () {
                    });
                    var code = (err) ? -1 : 0;
                    res.json({"code": code});
                });
            } else {
                res.json({"code": -1});
            }
        }
    });
}

function deleteSchedules(req, res) {
    web.checkPermission("customers", req.gid, function (allowed) {
        if (!allowed || (allowed.write < 3)) {
            res.status(403).json({"code": -1});
        } else {
            var match = {"_id": db.objectID(req.params.id)};
            db.scheduler.update(match, {"$set": {"start": 0}}, function (err, result) {
                var code = (err) ? -1 : 0;
                res.json({"code": code});
                db.weblog.insert({
                    "ts": new Date().getTime(),
                    "ip": req.ip,
                    "type": "deleteSchedules",
                    "id": common.escapeHtml(req.params.id),
                    "action": "release",
                    "username": req.user,
                    "code": 0
                });
            });
        }
    });
}

function executeSchedules(req, res) {
    web.checkPermission("customers", req.gid, function (allowed) {
        if (!allowed || (allowed.write < 3)) {
            res.status(403).json({"code": -1});
        } else {
            var match = {"_id": db.objectID(req.params.id)};
            db.scheduler.find(match).toArray(function (err, result) {
                if (err) {
                    return res.json({"code": -1, error: err.message});
                }
                if (!result || !result[0]) {
                    return res.json({"code": -1, error: "Not Found"});
                }
                if (result[0].processed) {
                    return res.json({"code": -1, error: "Already Processed"});
                }

                var tEvent = result[0];
                var callback = function () {
                    res.json({"code": 0});
                }

                if (tEvent.action == "notification") {
                    notificationScheduler.detail(tEvent, callback);
                } else if (tEvent.action == "addon") {
                    addonScheduler.addon(tEvent, callback);
                } else if (tEvent.action == "portin") {
                    portInScheduler.initiatePortIn(tEvent, callback);
                } else if (tEvent.action == "invoice_payment") {
                    paymentScheduler.initiateInvoicePayment(tEvent, callback);
                } else if (tEvent.action == "portin_failure_check") {
                    portInScheduler.initiatePortInFailureCheck(tEvent, callback);
                } else if (tEvent.action == "portin_completion_status_check") {
                    portInScheduler.initiatePortInCompletionStatusCheck(tEvent, callback);
                } else if ((tEvent.action == "suspend") ||
                    (tEvent.action == "terminate") ||
                    (tEvent.action == "activate")) {
                    accountStatusScheduler.changeAccount(tEvent, callback);
                } else if (tEvent.action == "release") {
                    accountStatusScheduler.release(tEvent, callback);
                } else if (tEvent.action == "report") {
                    reportScheduler.detail(tEvent, callback);
                } else if (tEvent.action == "autorepair") {
                    autorepairScheduler.detail(tEvent, callback);
                } else if (tEvent.action == "biRun") {
                    bi.executeBiRun(tEvent, callback);
                } else if (tEvent.action == "zendesk") {
                    zendesk.zendeskTicketsRun(tEvent, callback);
                }

                db.weblog.insert({
                    "ts": new Date().getTime(),
                    "ip": req.ip,
                    "type": "executeSchedules",
                    "id": common.escapeHtml(req.params.id),
                    "action": "execute",
                    "tEvent": tEvent,
                    "username": req.user,
                    "code": 0
                });
            });
        }
    });
}

function checkNumberAvailable(req, res) {
    web.checkPermission("customers", req.gid, function (allowed) {
        if (!allowed) {
            res.status(403).json({"code": -1});
        } else {
            var prefix = req.params.prefix;
            var number = req.params.number;
            return accountManager.getInventoryStatusFromEC(number)
                .then(({type, status}) => {
                    return res.json({
                        "code": 0,
                        "write": allowed.write,
                        "prefix": prefix,
                        "number": number,
                        type,
                        status
                    });
                })
                .catch(err => {
                    common.error('AccountManager.checkNumberAvailable() Error: ', err);
                    return res.json({
                        code: -1,
                        number,
                        status: 'Failed'
                      });
                });
        }
    });
}

function releaseNumber(req, res) {
    web.checkPermission("customers", req.gid, function (allowed) {
        if (!allowed || (allowed.write < 3)) {
            res.status(403).json({"code": -1});
        } else {
            var prefix = req.params.prefix;
            var number = req.params.number;
            ec.doInventoryStatusOperation({
                "inventoryNumber": number,
                "operationAlias": "FREE"
            }, function (err, result) {
                res.json({"code": 0, "write": allowed.write, "prefix": prefix, "number": number});
                db.weblog.insert({
                    "ts": new Date().getTime(),
                    "ip": req.ip,
                    "type": "releaseNumber",
                    "id": common.escapeHtml(number),
                    "action": "release",
                    "username": req.user,
                    "code": 0
                });
            });
        }
    });
}

function updateDetails(req, res) {
    web.checkPermission("customers", req.gid, function (allowed) {
        if (!allowed || (allowed.write < 3)) {
            res.status(403).json({"code": -1});
        } else {
            var params = req.params;
            var fields = req.fields;

            if (!params.account || !fields.name || !fields.idType || !fields.idNumber || !fields.portedStatus || !fields.birthday) {
                return res.status(400).json({code: -1, error: "Mandatory params missing"});
            }

            // TODO add async or promices

            elitecoreUserService.updateAllCustomerAccountsDetails(params.account, {name: fields.name}, (err) => {
                if (err) return res.status(400).json({code: -1, error: err.message});

                elitecoreUserService.updateCustomerID(params.account, fields.idType, fields.idNumber, (err) => {
                    if (err) return res.status(400).json({code: -1, error: err.message});

                    elitecoreUserService.updateCustomerOrderReferenceNumber(params.account, fields.orderRefNo, (err) => {
                        if (err) return res.status(400).json({code: -1, error: err.message});

                        ec.updateCustomerAccountData({
                                accountNumber: params.account,
                                accountProfiles: {strcustom3: (fields.portedStatus ? fields.portedStatus : "NORMAL")}
                            },
                            function (err, result) {
                                if (err) return res.status(400).json({code: -1, error: err.message});
                                elitecoreUserService.updateCustomerDOB(params.account, fields.birthday, (err) => {
                                    if (err) return res.status(400).json({code: -1, error: err.message});
                                    res.json({});
                                });
                            });
                    });
                });
            });

            db.weblog.insert({
                "ts": new Date().getTime(),
                "ip": req.ip,
                "type": "updateDetails",
                "params": params,
                "fields": fields,
                "username": req.user
            });
        }
    });
}

function updateProfileSettings(req, res) {
    web.checkPermission("customers", req.gid, function (allowed) {
        if (!allowed || (allowed.write < 3)) {
            res.status(403).json({"code": -1});
        } else {
            var params = req.params;
            var fields = req.fields;
            if(fields.optionId.toLocaleLowerCase() == "roaming_auto" || fields.optionId.toLocaleLowerCase() == "roaming_data"){
                return res.status(400).json({code: -1, error: "Roaming not allowed."});
            }
            ec.getCustomerDetailsAccount(params.account, false, function (err, cache) {
                if (err || !cache) {
                    return res.status(400).json({code: -1, error: "Can not load account info"});
                }

                profileManager.saveSetting(cache.prefix, cache.number, "selfcare", fields.optionId,
                    fields.optionType, fields.optionValue, config.NOTIFICATION_KEY, (err) => {
                        if (err) {
                            return res.status(400).json({code: -1, error: "Can not load account info"});
                        }

                        res.json({"code": 0});
                    });
            });

            db.weblog.insert({
                "ts": new Date().getTime(),
                "ip": req.ip,
                "type": "updateProfileSettings",
                "params": params,
                "fields": fields,
                "username": req.user
            });
        }
    });
}

function getCDR(req, res) {
    web.checkPermission("customers", req.gid, function (allowed) {
        if (!allowed) {
            res.status(403).json({"code": -1});
        } else {
            var prefix = req.params.prefix;
            var number = req.params.number;
            var today = new Date();
            var tasks = new Array;
            tasks.push(function (callback) {
                var first = new Date(today.getFullYear(), today.getMonth() - 1).getTime();
                var last = new Date(today.getFullYear(), today.getMonth(), today.getDate() + 1).getTime();
                var match = {"start_time": {"$gte": first, "$lt": last}, "orig_ani": (prefix + "" + number)};
                db.cdrs.find(match).sort({"start_time": -1}).toArray(function (err, result) {
                    if (!err && result) {
                        callback(undefined, {"type": "gentwo", "list": result});
                    } else {
                        callback({"error": err, "type": "gentwo"});
                    }
                });
            });
            var cdrEC = function (serviceInstanceNumber, type, i) {
                tasks.push(function (callback) {
                    cdrManager.elitecore(serviceInstanceNumber,
                        type,
                        today.getFullYear(),
                        today.getMonth() - i,
                        undefined,
                        function (err, result) {
                            if (!err) {
                                var list = new Array;
                                if (result) result.forEach(function (item) {
                                    if (item) list.push({
                                        "answer_time": new Date(parseInt(item.callstart)).getTime(),
                                        "orig_ani": item.callingstationid,
                                        "orig_dst_number": item.calledstationid,
                                        "egress_carrier_name": "Elitecore",
                                        "term_code_name": item.planname,
                                        "ingress_billtime": parseInt(item.sessiontime),
                                        "egress_cost": item.accountedcost, // should be M1's cost
                                        "ingress_cost": item.accountedcost,
                                        "media": "",
                                        "release_side": "",
                                        "mos": ""
                                    });
                                });
                                callback(undefined, {"type": "elitecore", "list": list});
                            } else {
                                callback({"error": err, "type": "elitecore"});
                            }
                        });
                });
            }
            res.json({"code": 0, "list": [], "write": allowed.write}); // disable for now
            /*            ec.getCustomerDetailsNumber(prefix, number, false, function (err, cache) {
             if (cache) {
             for (var i=0; i<2; i++) {
             cdrEC(cache.serviceInstanceNumber, "VOICE_CDMA", i);
             cdrEC(cache.serviceInstanceNumber, "SMS_SERVICE_VAS", i);
             }
             }
             async.parallelLimit(tasks, 2, function (err, result) {
             if (logsEnabled && err) common.error("getCDR " + number, JSON.stringify(err));
             var merged;
             if (result) result.forEach(function (item) {
             if (item && item.list && (item.list.length > 0)) {
             if (!merged) merged = item.list;
             else merged = merged.concat(item.list);
             }
             });
             res.json({"code": 0, "list": merged, "write": allowed.write});
             });
             });
             */
        }
    });
}

function testPush(req, res) {
    web.checkPermission("customers", req.gid, function (allowed) {
        if (!allowed) {
            res.status(403).json({"code": -1});
        } else {
            var msg = {"mime_type": common.MIME_TYPE_TEXT, "title": "test title", "text": "test text", "serviceInstance": req.fields.serviceInstanceNumber};
            push.sendWithBadge(req.fields.token, msg, req.fields.token_type, req.fields.flavor, req.fields.app_type);
            res.json({"code": 0});
        }
    });
}

function manageGentwo(prefix, number, amount, plan, mt, mo, callback) {
    var tasks = new Array;
    tasks.push(function (cb) {
        appManager.gentwo.plans(function(err, plans) {
            appManager.gentwo.setPlan(prefix + "" + number, plan, function() {
                appManager.gentwo.setCredit(prefix + "" + number, amount, function(rows) {
                    appManager.gentwo.getPlanCredit(prefix + "" + number, function(err, result) {
                        var obj = { "plan_id" : result.plan_id, plans: plans};
                        if (rows && rows.affectedRows > 0) {
                        obj.newCredit = result.crediit;
                        obj.newSgd = result.sgd;
                        obj.credit = result.credit - amount;
                        obj.sgd = result.sgd - amount;
                        } else {
                        obj.credit = result.credit;
                        obj.sgd = result.sgd;
                        }
                        cb(undefined, obj);
                    });
                });
            });
        });
    });
    if (typeof(mt) == "undefined") {
        tasks.push(function (cb) {
            scp.connect("get", prefix + "" + number, "mt", function (err, result) {
                if (!err && result && result.data) {
                    var state = (result.data[0] && (result.data[0].resp_code == "0")) ? parseInt(result.data[0].sip_allowed) : 0;
                    cb(undefined, {"mt": state});
                } else {
                    cb(undefined);
                }
            });
        });
    } else {
        tasks.push(function (cb) {
            var cmd = (mt == "1") ? "set" : "update";
            scp.connect(cmd, prefix + "" + number, "mt", function (err, result) {
                if (!err && result && result.data) {
                    cb(undefined, {"mt": mt});
                } else {
                    cb(undefined);
                }
            });
        });
    }
    if (typeof(mo) == "undefined") {
        tasks.push(function (cb) {
            scp.connect("get", prefix + "" + number, "mo", function (err, result) {
                if (!err && result && result.data) {
                    var state = (result.data[0] && (result.data[0].resp_code == "0")) ? parseInt(result.data[0].sip_allowed) : 0;
                    cb(undefined, {"mo": state});
                } else {
                    cb(undefined);
                }
            });
        });
    } else {
        tasks.push(function (cb) {
            var cmd = (mt == "1") ? "set" : "update";
            scp.connect(cmd, prefix + "" + number, "mo", function (err, result) {
                if (!err && result && result.data) {
                    cb(undefined, {"mo": mo});
                } else {
                    cb(undefined);
                }
            });
        });
    }
    var out = {"code": 0};
    async.parallel(tasks, function (err, result) {
        if (err) out.code = -1;
        result.forEach(function (item) {
            if (item && item.credit) out.credit = item.credit;
            if (item && item.sgd) out.sgd = item.sgd;
            if (item && item.plan_id) out.plan_id = item.plan_id;
            if (item && item.plans) out.plans = item.plans;
            if (item && item.mo) out.mo = item.mo;
            if (item && item.mt) out.mt = item.mt;
        });
        callback(err, out);
    });
}

function manageGentwoAPI(req, res) {
    web.checkPermission("customers", req.gid, function (allowed) {
        if (!allowed || (allowed.write < 3)) {
            res.status(403).json({"code": -1});
        } else {
            var prefix = req.fields.prefix;
            var number = req.fields.number;
            var amount = req.fields.amount;
            var plan = req.fields.plan;
            var mt = req.fields.mt;
            var mo = req.fields.mo;
            manageGentwo(prefix, number, amount, plan, mt, mo, function (err, result) {
                var code = -1;
                if (!err && result && (result.code == 0)) {
                    code = 0;
                }
                var credit = (result) ? result.credit : undefined;
                var newCredit = (result) ? result.newCredit : undefined;
                res.json({
                    "code": code,
                    "prefix": prefix,
                    "number": number,
                    "credit": credit,
                    "newCredit": newCredit,
                    "write": allowed.write
                });
                db.weblog.insert({
                    "ts": new Date().getTime(),
                    "ip": req.ip,
                    "type": "manageGentwoAPI",
                    "id": common.escapeHtml(number),
                    "action": req.fields,
                    "username": req.user,
                    "code": code
                });
            });
        }
    });
}

function addCreditNotes(req, res) {
    web.checkPermission("customers", req.gid, function (allowed) {
        if (!allowed || (allowed.write < 3)) {
            res.status(403).json({"code": -1});
        } else {
            var code = req.fields.code;
            var prefix = req.fields.prefix;
            var number = req.fields.number;
            var amount = req.fields.amount ? parseInt(req.fields.amount * 100) : 0;
            var reason = req.fields.reason;
            var date = new Date().toISOString().split("T")[0];

            ec.getCustomerDetailsNumber(prefix, number, false, function (err, cache) {
                if (err || !cache) {
                    return res.status(400).json({code: -1, error: "Can not load account info"});
                }

                var params = {
                    "accountCurrencyAlias": "SGD",
                    "accountList": [{
                        "accountName": cache.billingFullName,
                        "accountNumber": cache.billingAccountNumber,
                        "applyTax": "Y",
                        "exReceivedAmount": amount,
                        "receivedAmount": amount
                    }],
                    "eventAlias": "CREATE_CREDIT_EVENT",
                    "paymentCurrencyAlias": "SGD",
                    "paymentDate": date,
                    "reasonAlias": reason,
                    "staffId": "S000103",
                    "staffName": "LWUser"
                };

                ec.createCreditNote(params, function (err, result) {
                    if (err || !result) {
                        common.error("addCreditNotes", err)
                        return res.status(400).json({code: -1, error: "Failed to add credit notes"});
                    }

                    if (logsEnabled) common.log("CreditNotes", JSON.stringify(result));

                    db.weblog.insert({
                        "ts": new Date().getTime(),
                        "ip": req.ip,
                        "type": "addCreditNotes",
                        "id": common.escapeHtml(number),
                        "action": common.escapeHtml("Added Credit Note (" + amount + ")"),
                        "reason": common.escapeHtml(reason),
                        "username": req.user,
                        "code": code
                    });

                    res.json({
                        "prefix": prefix,
                        "number": number,
                        "write": allowed.write
                    });
                });
            });
        }
    });
}

function simDamageConfirm(req, res) {
    web.checkPermission("customers", req.gid, function (allowed) {
        if (!allowed) {
            res.status(403).json({"code": -1});
        } else {
            var serviceInstanceNumber = req.fields.serviceInstanceNumber;
            repairManager.simChangeAndNotify(serviceInstanceNumber, function(err, result){
                if(err){
                    return res.status(400).json({
                        "code": -1,
                        "error": err.message,
                        "code": err.status
                    });
                }else{
                   res.json({code: 0});
                }
            });
        }
    });
}

function useReferralCode(req, res) {
    web.checkPermission("customers", req.gid, function (allowed) {
        if (!allowed) {
            res.status(403).json({"code": -1});
        } else {
            var prefix = req.fields.prefix;
            var number = req.fields.number;
            var code = req.fields.code;

            var handleResult = (err) => {
                if (err) {
                    return res.status(400).json({
                        "code": -1,
                        "error": err.message
                    });
                }
                res.json({code: 0});
            }

            ec.getCustomerDetailsNumber(prefix, number, false, function (err, cache) {
                if (err) {
                    return handleResult(err);
                }

                var executionKey = config.OPERATIONS_KEY;
                referralManager.addReferralBonus(prefix, number, code, config.OPERATIONS_KEY, function (err, result) {

                    if (err && err.status === "CODE_NOT_FOUND") {
                        var orn = cache.orderReferenceNumber;
                        promotionsManager.useCode("APP", prefix, number, code, orn, executionKey, function (err, result) {
                            handleResult(err, result);
                        });
                    } else {
                        handleResult(err, result);
                    }
                });
            });

            db.weblog.insert({
                "ts": new Date().getTime(),
                "ip": req.ip,
                "type": "useReferralCode",
                "id": common.escapeHtml(number),
                "action": code,
                "username": req.user,
                "code": 0
            });
        }
    });
}

function deactivateReferrerBonuses(req, res) {
    web.checkPermission("customers", req.gid, function (allowed) {
        if (!allowed) {
            res.status(403).json({"code": -1});
        } else {
            var prefix = req.fields.prefix;
            var number = req.fields.number;
            var code = req.params.code;

            referralManager.deactivateReferrerBonus(prefix, number, config.OPERATIONS_KEY, function (err, result) {
                if (err) {
                    return res.status(400).json({
                        "code": -1,
                        "error": err.message
                    });
                }

                res.json({
                    "code": 0,
                    "number": number,
                    "prefix": prefix,
                    "unsubscribed": (result ? result.unsubscribed : false),
                    "reason": (result ? result.reason : undefined)
                });

                db.weblog.insert({
                    "ts": new Date().getTime(),
                    "ip": req.ip,
                    "type": "deactivateReferrerBonuses",
                    "id": common.escapeHtml(number),
                    "action": "",
                    "username": req.user,
                    "code": 0
                });
            });
        }
    });
}

function deleteReferralCode(req, res) {
    web.checkPermission("customers", req.gid, function (allowed) {
        if (!allowed || allowed < 3) {
            res.status(403).json({
                "code": -1
            });
        } else {
            var prefix = req.fields.prefix;
            var number = req.fields.number;
            var referrer = req.fields.referrer;
            var id = req.fields.id;

            return res.status(400).json({
                "code": -1,
                "error": "Not supported"
            });
        }
    });
}

function deleteBonusHistory(req, res) {
    web.checkPermission("customers", req.gid, function (allowed) {
        if (!allowed || allowed < 3) {
            res.status(403).json({
                "code": -1
            });
        } else {
            var prefix = req.fields.prefix;
            var number = req.fields.number;
            var id = req.fields.id;

            bonusManager.deleteBonusHistory(prefix, number, parseInt(id), function (err, result) {
                if (err) {
                    return res.status(400).json({
                        "code": -1,
                        "error": err.message
                    });
                }
                res.json({
                    "code": 0,
                    "number": number,
                    "prefix": prefix,
                    "removed": true
                });
                db.weblog.insert({
                    "ts": new Date().getTime(),
                    "ip": req.ip,
                    "type": "deleteBonusHistory",
                    "id": common.escapeHtml(number),
                    "action": common.escapeHtml(id),
                    "username": req.user,
                    "code": 0
                });
            });
        }
    });
}

function deleteGoldenTicket(req, res) {
    web.checkPermission("customers", req.gid, function (allowed) {
        if (!allowed || allowed < 3) {
            res.status(403).json({
                "code": -1
            });
        } else {
            var prefix = req.fields.prefix;
            var number = req.fields.number;
            var id = req.fields.id;

            leaderboardManager.deleteGoldenTicket(parseInt(id), function (err, result) {
                if (err) {
                    return res.status(400).json({
                        "code": -1,
                        "error": err.message
                    });
                }
                res.json({code: 0});
            });

            db.weblog.insert({
                "ts": new Date().getTime(),
                "ip": req.ip,
                "type": "goldenTicket",
                "id": common.escapeHtml(number),
                "action": "deleteGoldenTicket",
                "username": req.user,
                "code": 0
            });
        }
    });
}


function addGoldenTicket(req, res) {
    web.checkPermission("customers", req.gid, function (allowed) {
        if (!allowed || allowed < 3) {
            res.status(403).json({
                "code": -1
            });
        } else {
            var date = req.fields.date;
            var addWaiver = req.fields.addWaiver == "true";
            var silent = req.fields.silent == "true";
            var serviceInstanceNumber = req.fields.serviceInstanceNumber;

            leaderboardManager.addGoldenTicket(serviceInstanceNumber, {
                retrospectiveDate: date,
                waiver: addWaiver,
                silent: silent
            }, function (err) {
                if (err) {
                    return res.status(400).json({
                        "code": -1,
                        "error": err.message
                    });
                }
                res.json({code: 0});
            });

            db.weblog.insert({
                "ts": new Date().getTime(),
                "ip": req.ip,
                "type": "goldenTicket",
                "id": common.escapeHtml(serviceInstanceNumber),
                "action": "addGoldenTicket",
                "username": req.user,
                "code": 0
            });
        }
    });
}

function addBirthdayBonus(req, res) {
    web.checkPermission("customers", req.gid, function (allowed) {
        if (!allowed || (allowed.write < 2)) {
            res.status(403).json({"code": -1});
        } else {
            var number = req.fields.number;
            var prefix = req.fields.prefix;
            var account = req.params.account;
            var serviceInstance = req.params.serviceInstance;

            if (!number || !prefix || !serviceInstance || !account) {
                return res.status(400).json({"code": -1, "error": "Params are incorrect"});
            }

            bonusManager.addBirthdayBonus(prefix, number, config.OPERATIONS_KEY,
                function (error, result) {
                    if (error) {
                        res.status(400).json({
                            "code": -1,
                            "status": error.id,
                            "error": error.message
                        });
                    } else if (!result.added) {
                        res.status(400).json({
                            "code": -1,
                            "status": "NOT_ADDED",
                            "error": "Bonus is not added"
                        });
                    } else {
                        res.json({
                            "code": 0,
                            "added": result.added,
                            "prefix": prefix,
                            "number": number
                        });
                        db.weblog.insert({
                            "ts": new Date().getTime(),
                            "ip": req.ip,
                            "type": "addBirthdayBonus",
                            "id": common.escapeHtml(number),
                            "action": "addBirthdayBonus",
                            "username": req.user,
                            "code": 0
                        });
                    }
                });
        }
    });
}

function addWinbackBonus(req, res) {
    web.checkPermission("customers", req.gid, function (allowed) {
        if (!allowed || (allowed.write < 2)) {
            res.status(403).json({"code": -1});
        } else {
            var number = req.fields.number;
            var prefix = req.fields.prefix;
            var productId = req.fields.productId;
            var account = req.params.account;
            var serviceInstance = req.params.serviceInstance;

            if (!number || !prefix || !serviceInstance || !account) {
                return res.status(400).json({"code": -1, "error": "Params are incorrect"});
            }

            bonusManager.addSurpriseBonus(prefix, number, productId, config.OPERATIONS_KEY,
                function (error, result) {
                    if (error) {
                        res.status(400).json({
                            "code": -1,
                            "status": error.id,
                            "error": error.message
                        });
                    } else if (!result.added) {
                        res.status(400).json({
                            "code": -1,
                            "status": "NOT_ADDED",
                            "error": "Bonus is not added"
                        });
                    } else {
                        res.json({
                            "code": 0,
                            "added": result.added,
                            "prefix": prefix,
                            "number": number
                        });
                    }
                });
        }
    });
}

function addLoyaltyBonus(req, res) {
    web.checkPermission("customers", req.gid, function (allowed) {
        if (!allowed || (allowed.write < 2)) {
            res.status(403).json({"code": -1});
        } else {
            var number = req.fields.number;
            var prefix = req.fields.prefix;
            var account = req.params.account;
            var serviceInstance = req.params.serviceInstance;

            if (!number || !prefix || !serviceInstance || !account) {
                return res.status(400).json({"code": -1, "error": "Params are incorrect"});
            }

            bonusManager.addLoyaltyBonus(prefix, number, config.OPERATIONS_KEY,
                function (error, result) {
                    if (error) {
                        res.status(400).json({
                            "code": -1,
                            "status": error.id,
                            "error": error.message
                        });
                    } else {
                        res.json({
                            "code": 0,
                            "added": result.added,
                            "prefix": prefix,
                            "number": number
                        });
                        db.weblog.insert({
                            "ts": new Date().getTime(),
                            "ip": req.ip,
                            "type": "addLoyaltyBonus",
                            "id": common.escapeHtml(number),
                            "action": common.escapeHtml(account),
                            "username": req.user,
                            "code": 0
                        });
                    }
                });
        }
    });
}

function addContractBusterBonus(req, res) {
    web.checkPermission("customers", req.gid, function (allowed) {
        if (!allowed || (allowed.write < 2)) {
            res.status(403).json({"code": -1});
        } else {
            var number = req.fields.number;
            var prefix = req.fields.prefix;
            var months = req.fields.months;
            var account = req.params.account;
            var serviceInstance = req.params.serviceInstance;

            if (!number || !prefix || !serviceInstance || !months) {
                return res.status(400).json({"code": -1, "error": "Params are incorrect"});
            }

            res.status(400).json({
                "code": -1,
                "status": "ERROR_NOT_SUPPORTED",
                "error": "Bonus no longer supported"
            });
        }
    });
}

function addCareinstallation(req, res) {
    web.checkPermission("customers", req.gid, function (allowed) {
        if (!allowed || (allowed.write < 2)) {
            res.status(403).json({"code": -1});
        } else {
            var number = req.fields.number;
            var prefix = req.fields.prefix;
            var onegb = req.fields.onegb === 'true';
            var account = req.params.account;
            var serviceInstance = req.params.serviceInstance;

            if (!number || !prefix || !serviceInstance || !account) {
                return res.status(400).json({"code": -1, "error": "Params are incorrect"});
            }

            var callback = function (error, result) {
                if (error) {
                    res.status(400).json({
                        "code": -1,
                        "status": error.id,
                        "error": error.message
                    });
                } else {
                    res.json({
                        "code": 0,
                        "added": result.added,
                        "prefix": prefix,
                        "number": number
                    });
                    db.weblog.insert({
                        "ts": new Date().getTime(),
                        "ip": req.ip,
                        "type": "addCareInstallation",
                        "id": common.escapeHtml(number),
                        "action": common.escapeHtml(account),
                        "username": req.user,
                        "code": 0
                    });
                }
            };

            if (onegb) {
                bonusManager.add1GBSelfcareInstallationBonus(prefix, number, config.OPERATIONS_KEY, callback);
            } else {
                bonusManager.addSelfcareInstallationBonus(prefix, number, true, false, config.OPERATIONS_KEY, callback);
            }
        }
    });
}

function addPortInBonus(req, res) {
    web.checkPermission("customers", req.gid, function (allowed) {
        if (!allowed || (allowed.write < 2)) {
            res.status(403).json({"code": -1});
        } else {
            var number = req.fields.number;
            var prefix = req.fields.prefix;
            var account = req.params.account;
            var serviceInstance = req.params.serviceInstance;

            if (req.fields.extra) {
                res.status(400).json({
                    "code": -1,
                    "status": "ERROR_NOT_SUPPORTED",
                    "error": "Extra port-in bonis is not longer supported"
                });
            }

            if (!number || !prefix || !serviceInstance || !account) {
                return res.status(400).json({"code": -1, "error": "Params are incorrect"});
            }

            var cb = function (error, result) {
                if (error) {
                    res.status(400).json({
                        "code": -1,
                        "status": error.id,
                        "error": error.message
                    });
                } else {
                    res.json({
                        "code": 0,
                        "prefix": prefix,
                        "added": result.added,
                        "number": number
                    });
                    db.weblog.insert({
                        "ts": new Date().getTime(),
                        "ip": req.ip,
                        "type": "addPortInBonus",
                        "id": common.escapeHtml(number),
                        "action": common.escapeHtml(account),
                        "username": req.user,
                        "code": 0
                    });
                }
            }
            bonusManager.addPortInBonus(prefix, number, config.OPERATIONS_KEY, cb);
        }
    });
}

function addPendingBonuses(req, res) {
    web.checkPermission("customers", req.gid, function (allowed) {
        if (!allowed || (allowed.write < 2)) {
            res.status(403).json({"code": -1});
        } else {
            var number = req.fields.number;
            var prefix = req.fields.prefix;
            var orderReferenceNumber = req.fields.order_reference_number;
            var account = req.params.account;
            var serviceInstance = req.params.serviceInstance;

            if (!number || !prefix || !serviceInstance || !account) {
                return res.status(400).json({"code": -1, "error": "Params are incorrect"});
            }

            var callback = function (error, result) {
                if (error) {
                    res.status(400).json({
                        "code": -1,
                        "status": error.id,
                        "error": error.message
                    });
                } else {
                    res.json({
                        "code": 0,
                        "prefix": prefix,
                        "number": number,
                        "added": result.added,
                        "reason": result.reason
                    });

                    db.weblog.insert({
                        "ts": new Date().getTime(),
                        "ip": req.ip,
                        "type": "addPendingBonuses",
                        "id": common.escapeHtml(number),
                        "action": common.escapeHtml(account),
                        "username": req.user,
                        "code": 0
                    });
                }
            };

            promotionsManager.applyPendingPromo(prefix, number, orderReferenceNumber, "DEFAULT",
                config.OPERATIONS_KEY, callback);
        }
    });
}

function addAutoBoost(req, res) {
    web.checkPermission("customers", req.gid, function (allowed) {
        if (!allowed || (allowed.write < 2)) {
            res.status(403).json({"code": -1});
        } else {
            var number = req.fields.number;
            var prefix = req.fields.prefix;
            var checkData = req.fields.checkData === "true";
            var enable = req.fields.enable === "true";
            var account = req.params.account;
            var serviceInstance = req.params.serviceInstance;

            if (!number || !prefix || !serviceInstance || !account) {
                return res.status(400).json({"code": -1, "error": "Params are incorrect"});
            }

            var handleResponse = function (error, result) {
                if (error) {
                    res.status(400).json({
                        "code": -1,
                        "status": error.id,
                        "error": error.message
                    });
                } else {
                    res.json({
                        "code": 0,
                        "prefix": prefix,
                        "added": result.added,
                        "number": number
                    });
                    db.weblog.insert({
                        "ts": new Date().getTime(),
                        "ip": req.ip,
                        "type": enable ? "enableAutoBoost" : "addAutoBoost",
                        "id": common.escapeHtml(number),
                        "action": common.escapeHtml(account),
                        "username": req.user,
                        "code": 0
                    });
                }
            };

            if (enable) {
                profileManager.saveSetting(prefix, number, "selfcare", "autoboost_limit",
                    "Integer", -1, config.OPERATIONS_KEY, (err) => {
                        if (err) {
                            res.status(400).json({
                                "code": -1,
                                "status": err.id,
                                "error": err.message
                            });
                        }

                        boostManager.addAutoBoost(prefix, number, checkData,
                            config.OPERATIONS_KEY, handleResponse);
                    });
            } else {
                boostManager.addAutoBoost(prefix, number, checkData,
                    config.OPERATIONS_KEY, handleResponse);
            }

        }
    });
}

function useAvailableBonus(req, res) {
    web.checkPermission("customers", req.gid, function (allowed) {
        if (!allowed || (allowed.write < 2)) {
            res.status(403).json({"code": -1});
        } else {
            var number = req.fields.number;
            var prefix = req.fields.prefix;
            var account = req.params.account;
            var serviceInstance = req.params.serviceInstance;
            var id = req.fields.id;

            if (!number || !prefix || !serviceInstance || !account || !id) {
                return res.status(400).json({"code": -1, "error": "Params are incorrect"});
            }

            bonusManager.useAvailableBonus(prefix, number, id, function (error, result) {
                if (error) {
                    res.status(400).json({
                        "code": -1,
                        "status": error.id,
                        "error": error.message
                    });
                } else {
                    res.json({
                        "code": 0,
                        "prefix": prefix,
                        "number": number
                    });
                    db.weblog.insert({
                        "ts": new Date().getTime(),
                        "ip": req.ip,
                        "type": "useAvailableBonus",
                        "id": common.escapeHtml(number),
                        "action": common.escapeHtml(id),
                        "username": req.user,
                        "code": 0
                    });
                }
            });
        }
    });
}

function resubscribeAddons(req, res) {
    web.checkPermission("customers", req.gid, function (allowed) {
        if (!allowed || (allowed.write < 2)) {
            res.status(403).json({"code": -1});
        } else {
            var number = req.fields.number;
            var prefix = req.fields.prefix;
            var current = req.fields.current === "true";
            var schedule = req.fields.schedule === "true";
            var account = req.params.account;
            var serviceInstance = req.params.serviceInstance;

            if (!number || !prefix || !serviceInstance || !account) {
                return res.status(400).json({"code": -1, "error": "Params are incorrect"});
            }

            var handleResult = function (error, result) {
                if(!res.headersSent){
                    if (error) {
                        return res.status(400).json({
                            "code": -1,
                            "status": error.id,
                            "error": error.message
                        });
                    }

                    res.json({
                        "code": 0,
                        "prefix": prefix,
                        "number": number
                    });
                }
            };

            var initiator = "[CMS][" + req.user + "]";
            var key = config.OPERATIONS_KEY;
            if (current) {
                subscriptionManager.updateCurrentBillBonuses(prefix, number, initiator, key, handleResult);
            } else if (schedule) {
                bonusManager.scheduleAggregation(prefix, number, initiator, key, handleResult);
            } else {
                subscriptionManager.updateNextBillBonuses(prefix, number, initiator, key, handleResult);
            }

            db.weblog.insert({
                "ts": new Date().getTime(),
                "ip": req.ip,
                "type": "resubscribeAddons",
                "id": common.escapeHtml(number),
                "action": common.escapeHtml("updateNextBillBonuses"),
                "username": req.user,
                "code": 0
            });
        }
    });
}

function addonExist(req, res) {
    web.checkPermission("customers", req.gid, function (allowed) {
        if (!allowed || (allowed.write < 2)) {
            res.status(403).json({"code": -1});
        } else {
            var number = req.params.number;
            var prefix = req.params.prefix;
            var productId = req.params.productId;
            var date = new Date();
            var firstDay = new Date(date.getFullYear(), date.getMonth(), 1).getTime();
            var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0).getTime();
            subscriptionManager.getAddonSubscriptionForCustomer(number, productId, firstDay, lastDay, function(err, result){
                if(err){
                    return res.status(400).json({
                        "code": -1,
                        "status": err.status,
                        "error": err.message
                    });
                }else if(result){
                    res.json({
                        "code": 0,
                        "subscribed": true
                    });
                }else{
                    res.json({
                        "code": 0,
                        "subscribed": false
                    });
                }
            });
        }
    });
}

function blockNotif(req, res) {
    web.checkPermission("customers", req.gid, function (allowed) {
        if (!allowed || (allowed.write < 3)) {
            res.status(403).json({"code": -1});
        } else {
            var number = req.fields.number;
            var prefix = req.fields.prefix;
            var serviceInstanceNumber = req.params.serviceInstance;
            var block = req.fields.block;
            if (block === "true") {
                notificationManager.suppressNotifications(number, serviceInstanceNumber, req.user);
            } else {
                notificationManager.enableNotifications(number, serviceInstanceNumber, req.user);
            }

            res.json({
                "code": 0,
                "prefix": prefix,
                "number": number,
                "timeout": notificationManager.suppressNotificationsTimeout(),
                "list": notificationManager.suppressNotificationsList()
            });

            db.weblog.insert({
                "ts": new Date().getTime(),
                "ip": req.ip,
                "type": "blockNotif",
                "id": common.escapeHtml(number),
                "action": common.escapeHtml(block),
                "username": req.user,
                "code": 0
            });
        }
    });
}

function overrideNotif(req, res) {
    web.checkPermission("customers", req.gid, function (allowed) {
        if (!allowed || (allowed.write < 3)) {
            res.status(403).json({"code": -1});
        } else {
            var number = req.fields.number;
            var prefix = req.fields.prefix;
            var ts = req.fields.ts;
            var activity = req.fields.activity;
            db.notifications_logbook.insert({
                "ts": ts,
                "activity": activity,
                "ip": config.MYSELF,
                "number": number
            });
            res.json({
                "code": 0,
                "prefix": prefix,
                "number": number
            });
            db.weblog.insert({
                "ts": new Date().getTime(),
                "ip": req.ip,
                "type": "overrideNotif",
                "id": common.escapeHtml(number),
                "action": common.escapeHtml(activity),
                "username": req.user,
                "code": 0
            });
        }
    });
}

function getFreeICCID(req, res) {
    web.checkPermission("customers", req.gid, function (allowed) {
        if (!allowed) {
            res.status(403).json({"code": -1});
        } else {
            var cookie = req.cookies.cmscookie;
            repairManager.loadFreeICCIDFromPool("replacement" ,cookie, function (err, result) {
                if (err) {
                    return res.status(400).json({
                        code: -1,
                        error: err.message
                    });
                } else {
                    return res.json({
                        code: 0,
                        iccid: result ? result.iccid : undefined
                    });
                }
            });
        }
    });
}

function consumeData(req, res) {
    web.checkPermission("customers", req.gid, function (allowed) {
        if (!allowed) {
            res.status(403).json({"code": -1});
        } else {
            var number = req.fields.number;
            var prefix = req.fields.prefix;
            var amountMb = parseInt(req.fields.amountMb);
            var rgId = parseInt(req.fields.rgId);
            var type = req.fields.type;
            var finalCb = function (err) {
                if (err) {
                    common.error("consumeData", err);
                    return res.status(400).json({
                        code: -1,
                        error: "failed to burn data"
                    });

                } else {
                    return res.json({
                        code: 0,
                        prefix: prefix,
                        number: number
                    });
                }
                ;
            };
            if(type == "single"){
                diameterClient.spendData(prefix + number, amountMb, rgId, finalCb);
            }else{
                diameterClient.spendDataChunks(prefix + number, amountMb, rgId, finalCb);
            }
        }
    });
}

function createUpdateCCLink(req, res) {
    web.checkPermission("customers", req.gid, function (allowed) {
        if (!allowed) {
            res.status(403).json({"code": -1});
        } else {
            var account = req.params.account;
            ecommManager.generateCreditCardUpdateLink(account, function (err, result) {
                if (err) return res.status(400).json({ code: -1});
                return res.json({
                    code: result.url ? 0 : -1,
                    url: result.url,
                    returnUrl: 'https://' + config.CC_UPDATE_RETURN + ':6443/link/update/creditcard/status/html/' + result.refNum + '/' + result.linkId,
                    expiryTime: result.expiryTime,
                    expiryDuration: result.expiryDuration
                });
            });
        }
    });
}

function ecommNotification(req, res) {
    web.checkPermission("customers", req.gid, function (allowed) {
        if (!allowed || allowed.write < 1) {
            res.status(403).json({"code": -1});
        } else {
            var type = req.fields.type;
            var prefix = (req.fields) ? req.fields.prefix : undefined;
            var number = (req.fields) ? req.fields.number : undefined;
            var serviceInstanceNumber = (req.fields) ? req.fields.serviceInstanceNumber : undefined;
            var path = (type == "delivery") ? "/api/v1/notifications/" + serviceInstanceNumber + "/send_delivery_failed_email" :
                (type == "document") ? "/api/v1/notifications/" + number + "/send_disapproval_reminder" : undefined;
            var params = {"qs": JSON.parse(JSON.stringify(config.PAAS_CREDS))};
            paas.connect("get", path, params, function (err, result) {
                if (!err && result && result.success) {
                    res.json({"code": 0, "message": result.message, "prefix": prefix, "number": number});
                } else {
                    res.json({"code": -1, "message": result.message, "prefix": prefix, "number": number});
                }
            });
        }
    });
}

function getDoc(req, res) {
    if (req.params.id) {
        db.profile_doc_files_get(req.params.id, function (err, item) {
            if (item) {
                res.set('Content-Type', item.mime);
                res.send(item.file.buffer);
            } else {
                res.status(404).send('Not Found');
            }
        });
    } else {
        res.status(404).send('Not Found');
    }
}

function smsTest(req, res) {
    web.checkPermission("customers", req.gid, function (allowed) {
        if (!allowed || allowed.write < 1) {
            res.status(403).json({"code": -1});
        } else {
            var prefix = req.params.prefix;
            var number = req.params.number;
            smsManager.test(prefix, number, (e,r)=>{
                res.json({ "code": 0, "result": r });
            });
        }
    });
}
