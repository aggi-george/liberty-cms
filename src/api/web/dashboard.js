var async = require('async');
var request = require('request');
var spawn = require('child_process').spawn;
var config = require('../../../config');
var common = require('../../../lib/common');
var db = require('../../../lib/db_handler');
var paas = require('../../../lib/paas');
var web = require('./web');
var analytics = require('../internal/analyticsApi');
const hostManager = require('../../core/manager/system/hostManager');
const healthManager = require('../../core/manager/system/healthManager');

var LOG = config.LOGSENABLED;

//exports

exports.get = get;
exports.put = put;

//functions

function get(req, res) {
    if ( req.params.type == "notifications" ) getNotifications(req, res);
    else if ( req.params.type == "statsCMS" ) getStatsCMS(req, res);
    else if ( req.params.type == "statsMobile" ) getStatsMobile(req, res);
    else if ( req.params.type == "ecomm" ) getEcomm(req, res);
    else if ( req.params.type == "queueStats" ) queueStats(req, res);
    else if ( req.params.type == "statsPm2" ) getPm2RawStats(req, res);
}

function put(req, res) {
    if ( req.params.type == "queueStats" ) putQueueStats(req, res);
}

function getNotifications(req, res) {
    web.checkPermission("notifications", req.gid, function(allowed) {
        if (!allowed) {
            res.status(403).json({ "code" : -1 });
        } else {
            var groupby = req.params.groupby;
            var hour = new Date().toISOString().split(":")[0] + ":00:00.000Z";
            var end =  new Date(hour).getTime();
            var start = end - (10 * 24 * 60 * 60 * 1000);    // 10 days ago
            var match = { "$match" : { "ts" : { "$gte" : start, "$lt" : end } } };
            var project = { "$project" : { "stamp" : { "$dateToString" : { "format" : "%Y-%m-%d", "date" : { "$add" : [ new Date(0), "$ts" ] } } }, "groups" : "$groups" } };
            var unwind = { "$unwind" : "$groups" };
            var activity_group = { "$group" : { "_id" : { "stamp" : "$stamp", "activity" : "$groups.activity" }, "notification_count" : { "$sum" : "$groups.count" } } };
            var team_group = { "$group" : { "_id" : { "stamp" : "$stamp", "teamName" : "$groups.teamName" }, "notification_count" : { "$sum" : "$groups.count" } } };
            var transport_project = { "$project" : { "stamp" : "$stamp",
                                "sms" : { "$cond" : { "if" : "$groups.sms", "then" : "$groups.count", "else" : 0 } },
                                "email" : { "$cond" : { "if" : "$groups.email", "then" : "$groups.count", "else" : 0 } },
                                "ussd" : { "$cond" : { "if" : "$groups.ussd", "then" : "$groups.count", "else" : 0 } },
                                "gentwo" : { "$cond" : { "if" : "$groups.gentwo", "then" : "$groups.count", "else" : 0 } },
                                "selfcare" : { "$cond" : { "if" : "$groups.selfcare", "then" : "$groups.count", "else" : 0 } }
                        } };
            var transport_group = { "$group" : { "_id" : { "stamp" : "$stamp" },
                            "email" : { "$sum" : "$email" },
                            "sms" : { "$sum" : "$sms" },
                            "ussd" : { "$sum" : "$ussd" },
                            "gentwo" : { "$sum" : "$gentwo" },
                            "selfcare" : { "$sum" : "$selfcare" } 
                        } };
            var tasks = new Array;
            tasks.push(function (callback) {
                db.agg.aggregate([ match, project, unwind, activity_group ], { readPreference: db.readPreference.SECONDARY_PREFERRED })
                        .toArray(function (err, result) {
                    callback(err, { "group" : "activity", "list" : result });
                });
            });
            tasks.push(function (callback) {
                db.agg.aggregate([ match, project, unwind, team_group ], { readPreference: db.readPreference.SECONDARY_PREFERRED })
                        .toArray(function (err, result) {
                    callback(err, { "group" : "teamName", "list" : result });
                });
            });
            tasks.push(function (callback) {
                db.agg.aggregate([ match, project, unwind, transport_project, transport_group ], { readPreference: db.readPreference.SECONDARY_PREFERRED })
                        .toArray(function (err, result) {
                    callback(err, { "group" : "transport", "list" : result });
                });
            });
            async.parallel(tasks, function(err, result) {
                res.json({ "code" : 0, "result" : result });
            });
        }
    });
}

function getPm2RawStats(req, res){
    let serverDetails = hostManager.getNodejsServers();
    let finalResult = [];
    async.forEach(serverDetails, (item ,callback) => {
        let requestUrl = "http://" + item.ip + ":" + ((item.name == "worf" || item.name == "wesley") ? item.mobileport : item.webport) + "/pm2raw";
        request({
            uri: requestUrl,
            method: 'GET',
        },  (err, res, responseString) => {
            if(err){
                finalResult.push({"name": item.name, "data": null});
            }else{
                let resObj = common.safeParse(responseString);
                if(resObj.code == -1 && !resObj.data && !Array.isArray(resObj.data)){
                    finalResult.push({"name": item.name, "data": null});
                }else{
                    resObj.data.sort(function(a, b){
                        if(a.name.localeCompare(b.name) == -1){
                            return false;
                        }else{
                            return true;
                        }
                    });
                    finalResult.push({"name": item.name, "data": resObj.data});
                }
            }
            callback();
        });
    }, (err) => {
        var othersItem;
        var otherItemData = [];
        healthManager.executeBasicHealth((result) => {
            var othersItem = {"name": "others"};
            result.results.forEach((item) => {
                if(item.item != "Pm2 Details"){
                    otherItemData.push({"name": item.item, "latency": item.latency, "errorCode": item.errorCode});
                }
            });
            finalResult.sort(function(a, b){
                if(a.name.localeCompare(b.name) == -1){
                    return false;
                }else{
                    return true;
                }
            });
            othersItem.data = otherItemData;
            finalResult.unshift(othersItem);
            res.json({code: 0, result: finalResult});
        });
    });
}

function getStatsMobile(req, res) {
    web.checkPermission("dashboard", req.gid, function(allowed) {
        if (!allowed) {
            res.status(403).json({ "code" : -1 });
        } else {
            var servers = config.DEV ? [ "riker", "checkov" ] : [ "worf", "wesley" ];
            getStats(servers, function (err, result) {
                if (!err && result) {
                    res.json({ "code" : 0, "result" : result });
                } else {
                    if (err) common.error('getStatsMobile', err);
                    res.json({ "code" : -1 });
                }
            });
        }
    });
}

function queueStats(req, res) {
    web.checkPermission("dashboard", req.gid, function(allowed) {
        if (!allowed) {
            res.status(403).json({ "code" : -1 });
        } else {
            if (req.query.id) {
                db.queueJobs.rangeByType(req.query.type, req.query.id, 0, 99, 'asc', function (err, jobs) {
                if (!err) {
                    res.json({ "code" : 0, "result" : jobs });
                } else {
                    res.json({ "code" : -1 });
                }
                    
                });
            } else {
                analytics.getQueueStats(function(err, result) {
                    if (!err) {
                        res.json({ "code" : 0, "result" : result });
                    } else {
                        res.json({ "code" : -1 });
                    }
                });
            }
        }
    });
}

function putQueueStats(req, res) {
    web.checkPermission("dashboard", req.gid, function(allowed) {
        if (!allowed) {
            res.status(403).json({ "code" : -1 });
        } else {
            var id = req.fields && req.fields.trash ? req.fields.trash : req.fields && req.fields.refresh ? req.fields.refresh : undefined;
            var type = req.fields && req.fields.type ? req.fields.type : undefined;
            var state = req.fields && req.fields.state ? req.fields.state : 'failed';
            if (id) {
                db.queueJobs.get(parseInt(id), function(errG, job) {
                    if (errG) return res.json({ "code" : -1, "error" : errG });
                    if (req.fields && req.fields.trash) {
                        job.remove(function (errR) {
                            if (errR) {
                                res.json({ "code" : -1 })
                            }else{
                                res.json({ "code" : 0 });
                            }
                            db.weblog.insert({
                                "ts": new Date().getTime(),
                                "ip": req.ip,
                                "id": common.escapeHtml(id),
                                "action": "queueJobAction",
                                "actionType": "remove",
                                "type": type,
                                "state": state,
                                "username": req.user,
                                "err": errR,
                                "code": errR ? -1 : 0
                            });
                        });
                    } else if (req.fields && req.fields.refresh) {
                        job.inactive(function (errR) {
                            if (errR){
                                res.json({ "code" : -1 })
                            }else{
                                res.json({ "code" : 0 });
                            }
                            db.weblog.insert({
                                "ts": new Date().getTime(),
                                "ip": req.ip,
                                "id": common.escapeHtml(id),
                                "action": "queueJobAction",
                                "actionType": "refresh",
                                "type": type,
                                "state": state,
                                "username": req.user,
                                "err": errR,
                                "code": errR ? -1 : 0
                            });
                        });
                    }
                });
            } else if (type && state && req.fields.bulkRefresh) {
                db.queueJobs.rangeByType(type, state, 0, 99, 'asc', function (err, jobs) {
                    if (err && !jobs) return res.json({ "code" : -1, "error" : err });
                    var tasks = new Array;
                    jobs.forEach(function(job) {
                        tasks.push(function (cb) {
                            job.inactive(function () { cb(undefined, job.id) });
                        });
                    });
                    async.series(tasks, function(err, result) {
                        db.weblog.insert({
                            "ts": new Date().getTime(),
                            "ip": req.ip,
                            "id": "all",
                            "action": "queueJobAction",
                            "actionType": "refreshBulk",
                            "type": type,
                            "state": state,
                            "username": req.user,
                            "err": err,
                            "code": err ? -1 : 0
                        });
                        res.json({ "code" : 0 });
                    });
                });
            } else if (type && state && req.fields.bulkDelete) {
                db.queueJobs.rangeByType(type, state, 0, 99, 'asc', function (err, jobs) {
                    if (err && !jobs) return res.json({ "code" : -1, "error" : err });
                    var tasks = new Array;
                    jobs.forEach(function(job) {
                        tasks.push(function (cb) {
                            job.remove(function () { cb(undefined, job.id) });
                        });
                    });
                    async.series(tasks, function(err, result) {
                        db.weblog.insert({
                            "ts": new Date().getTime(),
                            "ip": req.ip,
                            "id": "all",
                            "action": "queueJobAction",
                            "actionType": "removeBulk",
                            "type": type,
                            "state": state,
                            "username": req.user,
                            "err": err,
                            "code": err ? -1 : 0
                        });
                        res.json({ "code" : 0 });
                    });
                });
            } else {
                res.json({ "code" : -1, "error" : "empty params" });
            }
        }
    });
}

function getStatsCMS(req, res) {
    web.checkPermission("admin", req.gid, function(allowed) {
        var admin = allowed ? true : false;
        web.checkPermission("dashboard", req.gid, function(allowed) {
            if (!allowed) {
                res.status(403).json({ "code" : -1 });
            } else {
                var servers = config.DEV ? [ "riker", "checkov" ] : [ "kirk", "picard", "janeway" ];
                getStats(servers, function (err, result) {
                    if (!err && result) {
                        res.json({ "code" : 0, "result" : result, "admin": admin});
                    } else {
                        if (err) common.error("getStatsCMS", err);
                        res.json({ "code" : -1, "admin": admin});
                    }
                });
            }
        });
    });
}

function getStats(servers, callback) {
    var now = new Date();
    var start = new Date(now.getFullYear(), now.getMonth(), now.getDate() - 6);
    var end = new Date(now.getFullYear(), now.getMonth(), now.getDate(), now.getHours());
    var match = { "ts" : { "$gte" : start.getTime(), "$lt" : end.getTime() },  "$or" : [ ] }; 
    servers.forEach(function (server) {
        match["$or"].push({ "server" : new RegExp("^" + server, "i") });
    });
    var project = { "weight" : { "$multiply" : [ "$avg", "$count" ] }, "count" : 1, "maxtime" : "$max", "maxPath": "$maxPath", "sessions" : "$sessions",
        "date" : { "$dateToString" : { "format" : "%Y-%m-%d %H", "date" : { "$add" : [ new Date(0), "$ts" ] } } } };
    var aggArray = [ { "$match" : match },
            { "$project" : project },
            { "$group" : { "_id" : "$date", "count" : { $sum : "$count" },
                "weight" : { "$sum" : "$weight" }, "max" : { "$max" : "$maxtime" }, "sessions" : { "$max" : "$sessions"}, "paths": {"$push" : {"path": "$maxPath", "time": "$maxtime"}  }  } },
            { "$project" : {
                "avg" : { "$divide" : [ "$weight", "$count" ] },
                "max" : "$max",
                "paths" : "$paths",
                "sessions" : { "$cond" : [ { "$eq" : [ "$sessions", null ] }, 0, "$sessions" ] } } },
            {"$unwind": '$paths'},
            { "$sort" : {"paths.time": -1} },
            { "$group": {"_id": {"time": "$_id", "avg": "$avg", "max" : "$max", "sessions": "$sessions"}, "paths": {"$push": {"path": "$paths.path", "time": "$paths.time"}}}},
            { "$sort" : { "_id.time" : 1} }, { "$project" : { "_id" : 1, "paths" : { "$slice" : [ "$paths", 0, 100 ] } } } ];
        if (LOG) common.log("getStats", JSON.stringify(aggArray));
    db.apistats.aggregate(aggArray, { readPreference: db.readPreference.SECONDARY_PREFERRED }, callback);
}

function getEcomm(req, res) {
    web.checkPermission("dashboard", req.gid, function(allowed) {
        if (!allowed) {
            res.status(403).json({ "code" : -1 });
        } else {
            var link = req.query.link;
            if (link) {
                getEcommLink(link, function (reply) {
                    res.json({ "code" : 0, "result" : reply });
                });
            } else {
                db.cache_get("api_stats", "ecomm_dashboard", function(cache) {
                    if (cache) res.json({ "code" : 0, "result" : cache });
                    db.cache_ttl("api_stats", "ecomm_dashboard", function(ttl) {
                        if (!ttl || (ttl < 82800)) {
                            var options = { "timeout" : 60000 };
                            var cmd = "./scripts/internal/cache_ecomm_dashboard.js";
                            var cacheEcomm = spawn(cmd, options);
                            cacheEcomm.stdout.on("data", function(data) {
                                if (LOG) common.log("getEcomm", data.toString());
                            });
                            cacheEcomm.stderr.on("data", function(data) {
                                common.error("getEcomm", data.toString());
                            });
                            cacheEcomm.on("close", function(code) {
                                if (LOG) common.log("getEcomm", "done code=" + code);
                                if (!cache && !res.headersSent) {
                                    db.cache_get("api_stats", "ecomm_dashboard", function(cache) {
                                        res.json({ "code" : 0, "result" : cache });
                                    });
                                }
                            });
                        }
                    });
                });
            }
        }
    });
}

function getEcommLink(link, cb) {
        var params = { "qs" : JSON.parse(JSON.stringify(config.PAAS_CREDS)) };
        var path = "/" + link.split("?")[0];
        var paramStr = link.split("?")[1];
        if ( paramStr && (paramStr != "") ) paramStr.split("&").forEach(function (o) {
            var key = o.split("=")[0];
            var val = o.split("=")[1];
            if ( key != "" ) params.qs[key] = val;
        });
        paas.connect("get", path, params, function (err, result) {
            if (!err && result) {
//                if (result.code == 0) {
                if (result.orders) {
                    cb(result.orders);
                } else if (result.customers) {
                    cb(result.customers);
                } else {
                    cb(undefined);
                }
            } else {
                cb(undefined);
            }
        });
}
