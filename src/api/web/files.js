var async = require('async');
var fs = require('fs');
var md5 = require("MD5");
var mime = require('mime');

var config = require('../../../config');
var common = require('../../../lib/common');
var db = require('../../../lib/db_handler');
var randomManager = require('../../utils/randomManager');

var log = config.LOGSENABLED;

//exports

exports.uploadFile = uploadFile;
exports.getFile = getFile;

//functions

function uploadFile(req, res) {
    if (!req.files || !req.files.content || !req.files.content.path) {
        return res.json({code: -1, error: "Empty content, can no upload file"});
    }

    var files = req.files;
    var collectionType = req.params.collectionType;

    var fileId = md5(files.content.name + "_" + new Date().getTime() + randomManager.randomPredefinedString(10));
    var tmpPath = files.content.path;
    var mimeType = mime.lookup(tmpPath);

    if (log) common.log("FileApi", "setFile:  fileId=" + fileId + ", collectionType=" +
    collectionType + ", tmpPath=" + tmpPath + ", mime=" + mimeType);

    var sendResponse = function (err) {
        fs.unlink(tmpPath, function (unlinkErr) {

            // ignore error and return success response
            // it is not important to remove temp file

            if (err) {
                res.json({code: -1, error: err.message});
            } else {
                res.json({code: 0, fileId: fileId});
            }
        });
    };

    fs.readFile(tmpPath, function (err, data) {
        if (err) {
            return sendResponse(err);
        }

        if (collectionType === "documents") {
            var fileType = req.params.fileType;
            var serviceInstanceNumber = req.params.serviceInstanceNumber;
            if (!serviceInstanceNumber || !fileType) {
                return sendResponse(new Error("Mandatory params are missing (serviceInstanceNumber, fileType)"));
            }
            db.profile_doc_files_set({
                serviceInstanceNumber: serviceInstanceNumber,
                fileType: fileType,
                filename: fileId,
                mime: mimeType,
                file: data,
                ts: new Date().getTime()
            }, sendResponse);
        } else if (collectionType === "temp") {
            db.tempFilesInsert({
                filename: fileId,
                mime: mimeType,
                file: data,
                ts: new Date().getTime()
            }, sendResponse);
        } else if (collectionType === "resource") {
            db.resourceFilesInsert({
                filename: fileId,
                mime: mimeType,
                file: data,
                ts: new Date().getTime()
            }, sendResponse);
        } else {
            sendResponse(new Error("Unsupported file type " + collectionType));
        }
    });
}

function getFile(req, res) {
    var fileId = req.params.fileId;
    var collectionType = req.params.collectionType;

    if (!fileId || !collectionType) {
        return res.status(404).send('Not File Found: Invalid Params');
    }

    var collection;
    if (collectionType === "documents") {
        collection = db.profile_doc_files_get;
    } else if (collectionType === "temp") {
        collection = db.tempFilesGet;
    } else if (collectionType === "resource") {
        collection = db.resourceFilesGet;
    } else {
        return res.status(404).send('File Type "' + collectionType + '" Invalid');
    }

    collection(fileId, function (err, item) {
        if (item) {
            res.set('Content-Type', item.mime);
            res.send(item.file.buffer);
        } else {
            res.status(404).send('File "' + fileId + '" Not Found');
        }
    });
}
