var fs = require('fs');
var async = require('async');
var csvParse = require('csv-parse');
var config = require('../../../config');
var db = require('../../../lib/db_handler');
var common = require('../../../lib/common');
var BaseError = require('../../core/errors/baseError');
var sw = require('../../../lib/switch');
var inappManager = require(__core + '/manager/inapp/inappManager');

var LOG = config.LOGSENABLED;

module.exports = {
    init: function(authCheck, webs, usagePath) {

        var read = { "read" : 1, "write" : 0 };
        var write = { "read" : 1, "write" : 1 };

        // gentwo - put
        webs.put(usagePath + '/put/:type/:id', (req, res) => { authCheck("gentwo", write, req, res, put) });

        // gentwo - get
        webs.get(usagePath + '/get/:type', (req, res) => { authCheck("gentwo", read, req, res, get) });
    }
}

function get(req, res, allowed) {
    var process = function(callback) {
        callback(req.query, function(error, result) {
            if (result) result.write = allowed.write;
            var httpCode = 200;
            if ( error && error.status && (parseInt(error.status) == error.status) ) {
                httpCode = error.status;
            }
            if (!res.headersSent) res.status(httpCode).json(result);
        });
    }
    if (req.params.type == "finance") process(getFinance);
    else if (req.params.type == "cdr") process(getCDR);
    else if (req.params.type == "rates") process(getRates);
    else if (req.params.type == "active") process(getActive);
    else if (req.params.type == "online") process(getOnline);
    else if (req.params.type == "plans") process(getPlans);
    else if (req.params.type == "topup") process(getTopup);
    else if (req.params.type == "inapp") process(getInapp);
    else res.status(400).json({ "code" : -1, "description" : "unknown type" });
}

function put(req, res, allowed) {
    var start = new Date();
    var process = function(callback) {
        callback(req.params, req.query, req.fields, req.files, function(error, result) {
            if (result) result.write = allowed.write;
            var httpCode = 200;
            if ( error && error.status && (parseInt(error.status) == error.status) ) {
                httpCode = error.status;
            }
            if (!res.headersSent) res.status(httpCode).json(result);
            db.weblog.insert({
                "start_ts" : start,
                "ts" : new Date().getTime(),
                "ip" : req.ip,
                "type" : "gentwo " + req.params.type,
                "id" : req.params.id,
                "action" : req.method,
                "username" : req.user,
                "code" : (result ? result.code : -1),
                "query" : req.query,
                "fields" : req.fields,
                "files" : ((req.files && req.files.content) ? req.files.content.name : undefined)
            });
        });
    }
    if (req.params.type == "rates") process(uploadRates);
    else if (req.params.type == "plan") process(updatePlan);
    else if (req.params.type == "weight") process(toggleWeight);
    else if (req.params.type == "inapp") process(reverseInapp);
    else res.status(400).json({ "code" : -1, "description" : "unknown type" });
}

function getFinance(query, callback) {
    var monthNames = [
        "January", "February", "March", "April",
        "May", "June","July", "August",
        "September", "October", "November", "December"
    ];
    var month = query.month;
    var year = query.year;
    var today = (month && year) ?
        common.getDateTime(new Date(parseInt(year), parseInt(month) + 1, 0, 23, 59, 59, 999)) :
        common.getDateTime(new Date());
    var tasks = new Array;
    tasks.push(function (cb) {
        var q = "SELECT (m.total - IFNULL(a.total,0) - IFNULL(g.total,0)) total FROM ( " +
            " SELECT SUM(credit) total FROM msisdn) m, ( " +
            " SELECT SUM(user_credit) total FROM inapp_purchase WHERE DATE(ts_start)>DATE(" + db.escape(today) + ")) a, " +
            " (SELECT SUM(user_credit) total FROM android_inapp WHERE DATE(creationdate)>DATE(" + db.escape(today) + ")) g";
        db.query_noerr(q, function (rows) {
            cb(undefined, { "name" : "System Liability", "date" : common.getDateTime(today), "value" : rows[0].total });
        });
    });
    tasks.push(function (cb) {
        var q = "SELECT IF(SUM(user_credit) IS NULL,0,SUM(user_credit)) total, CONCAT(MONTHNAME(" + db.escape(today) + "), " +
            " ' ', YEAR(DATE(" + db.escape(today) + "))) monthyear FROM inapp_purchase WHERE " +
            " YEAR(ts_start)=YEAR(" + db.escape(today) + ") AND MONTH(ts_start)=MONTH(DATE(" + db.escape(today) + "))";
        db.query_noerr(q, function (rows) {
            cb(undefined, { "name" : "Apple Topup", "date" : rows[0].monthyear, "value" : rows[0].total });
        });
    });
    tasks.push(function (cb) {
        var q = "SELECT IF(SUM(actual_credit) IS NULL,0,SUM(actual_credit)) total, CONCAT(MONTHNAME(" + db.escape(today) + "), " +
            " ' ', YEAR(" + db.escape(today) + ")) monthyear FROM android_inapp WHERE voided=0 AND " +
            " YEAR(creationdate)=YEAR(" + db.escape(today) + ") AND MONTH(creationdate)=MONTH(" + db.escape(today) + ")";
        db.query_noerr(q, function (rows) {
            cb(undefined, { "name" : "Google Topup", "date" : rows[0].monthyear, "value" : rows[0].total });
        });
    });
    var today = new Date(today);
    var thisMonth = monthNames[today.getMonth()] + " " + today.getFullYear();
    var firstDay = new Date(today.getFullYear(), today.getMonth(), 1).getTime() / 1000;     // temp / 1000
    var lastDay = (new Date(today.getFullYear(), today.getMonth() + 1, 1).getTime()-1) / 1000;      // temp / 1000
    var agg = [
        { "$match" : { "start_time" : { "$gte" : firstDay, "$lt" : lastDay } } },
        { "$group" : { "_id" : "total" } }
    ];
    tasks.push(function (cb) {
        var agg_array = JSON.parse(JSON.stringify(agg));
        agg_array[1]["$group"].total = { "$sum" : "$ingress_cost" };
        db.cdrs.aggregate(agg_array, function (err, result) {
            var total = (result && result[0]) ? result[0].total : 0;
            cb(undefined, {
                "name" : "Charge of calls",
                "date" : thisMonth,
                "value" : Math.round(total * 1000000) / 1000000 });
        });
    });
    tasks.push(function (cb) {
        var agg_array = JSON.parse(JSON.stringify(agg));
        agg_array[1]["$group"].total = { "$sum" : "$egress_cost" };
        db.cdrs.aggregate(agg_array, function (err, result) {
            var total = (result && result[0]) ? result[0].total : 0;
            cb(undefined, {
                "name" : "Cost of calls",
                "date" : thisMonth,
                "value" : Math.round(total * 1000000) / 1000000 });
        });
    });

    // get usage in the future to adjust liability
    tasks.push(function (cb) {
        var agg_array = [
            { "$match" : { "start_time" : { "$gt" : lastDay } } },
            { "$group" : { "_id" : "total" } }
        ];
        agg_array[1]["$group"].total = { "$sum" : "$ingress_cost" };
        db.cdrs.aggregate(agg_array, function (err, result) {
            var total = (result && result[0]) ? result[0].total : 0;
            cb(undefined, { "future_usage" : Math.round(total * 1000000) / 1000000 });
        });
    });

    async.parallel(tasks, function(err, result) {
        if (!err && result) {
            callback(undefined, {
                "code" : 0,
                "month" : month,
                "year" : year,
                "list" : result });
        } else {
            callback(undefined, { "code" : -1 });
        }
    });
}

function getCDR(query, callback) {
    if (query.call_id) {
        db.siptrace.find({ "callid" : query.call_id })
            .sort({ "time_stamp" : 1, "time_us" : 1 })
            .toArray(function(err, result) {
            var list = new Array;
            result.forEach(function (item) {
                list.push({
                    "msg" : new Buffer(item.msg.buffer, 'base64').toString("ascii"),
                    "time_stamp" : item.time_stamp,
                    "fromip" : item.fromip,
                    "toip" : item.toip
                });
            });
            callback(undefined, { "code" : 0, "list" : list });
        });
    } else {
        var today = (query && query.date) ? new Date(query.date) : new Date();
        var first = new Date(today.getFullYear(),
                today.getMonth(),
                today.getDate()).getTime() - (8 * 60 * 60 * 1000);
        var last = new Date(today.getFullYear(),
                today.getMonth(),
                today.getDate()+1).getTime() - (8 * 60 * 60 * 1000);
        var match = { "start_time" : { "$gte" : first, "$lt" : last } };
        db.cdrs.find(match).sort({ "start_time" : -1 }).toArray(function (err, result) {
            if (!err && result) {
                callback(undefined, { "code" : 0, "list" : result });
            } else {
                callback(undefined, { "code" : -1 });
            }
        });
    }
}

function getRates(query, callback) {
    var tasks = new Array;
    tasks.push(function (cb) {
        var q = "";
        var lcr = false;
        if ( query.lcr == "true" ) {
            lcr = true;
            if ( query.prefix == "" ) {
                return callback(400, {
                    "code" : common.PARSE_ERROR,
                    "details" : "LCR Empty prefix",
                    "description" : "Prefix cannot be empty on LCR" });
            }
            var prefixes = "";
            for ( var i=query.prefix.length-1;i>0;i-- ) prefixes += db.escape(query.prefix.slice(0,-i)) + ",";
            prefixes += query.prefix;
            q = "SELECT l.*, IF(pw.prefix IS NOT NULL,1,0) weight FROM (SELECT s.*, (SELECT GROUP_CONCAT(gn.groupName) groupName FROM groupMember gm, groupName gn WHERE gn.groupID=gm.groupID AND s.prefix LIKE CONCAT(gm.prefix,'%') AND s.buyrate < gn.maxRate GROUP BY gm.prefix ORDER BY gm.prefix DESC LIMIT 1) gname FROM (SELECT (SELECT MAX(lastupdate) FROM call_rates WHERE providerID=lcr_t.providerID AND dialprefix=lcr_t.dp AND lastupdate<=NOW()) eff, lcr_t.dp prefix, lcr_t.providerID, c_r.buyrate, c_r.sellrate, c_r.destination, c_r.billstart, c_r.billincrement, lcr_t.name provider, c_r.status crstatus FROM (SELECT MAX(lcr_s.dialprefix) dp, lcr_s.providerID, lcr_s.name FROM (SELECT SUM(IF(cr.status='D',1,0)) del, cr.dialprefix, cr.providerID, cp.name FROM call_rates cr LEFT JOIN call_providers cp ON cr.providerID=cp.providerID LEFT JOIN prefix_weight pw ON cr.dialprefix=pw.prefix AND pw.providerID=cr.providerID, plans p, planProvider pp WHERE cr.dialprefix in (" + prefixes + ") AND cr.lastupdate<=NOW() AND p.planID=1 AND p.planID=pp.planID AND cp.providerID=pp.providerID GROUP BY cr.providerID, cr.dialprefix) lcr_s WHERE lcr_s.del=0 GROUP BY lcr_s.providerID) lcr_t LEFT JOIN call_rates c_r ON c_r.dialprefix=lcr_t.dp AND c_r.providerID=lcr_t.providerID WHERE DATE(c_r.lastupdate)<=DATE(NOW()) GROUP BY lcr_t.dp, lcr_t.providerID) s ORDER BY s.buyrate ASC) l LEFT JOIN prefix_weight pw ON l.prefix=pw.prefix AND l.providerID=pw.providerID ORDER BY weight DESC, l.buyrate ASC"
        } else {
            var destCond = "";
            var prefixCond = "";
            var providerCond = "";
            var groupCond1 = "";
            var groupCond2 = "";
            if ( query.dest && query.dest != "" ) destCond = " AND cr.destination LIKE " + db.escape(query.dest + "%");
            if ( query.prefix && query.prefix != "" ) prefixCond = " AND cr.dialprefix LIKE " + db.escape(query.prefix + "%");
            if ( query.provider && query.provider != "" ) providerCond = " AND cr.providerID IN (" + db.escape(query.provider).slice(0,-1).slice(1) + ")";
            if ( query.group && query.groups != "" ) {
                groupCond1 = " AND gm.groupID IN (" + db.escape(query.group).slice(0,-1).slice(1) + ")";
                groupCond2 = " WHERE l.gname != ''";
            }
            q = "SELECT l.*, IF(pw.prefix IS NOT NULL,1,0) weight FROM (SELECT cr.providerID, cr.dialprefix prefix, cr.destination, cr.buyrate, cr.sellrate, cr.billstart, cr.billincrement, cr.status crstatus, cr.lastupdate eff, cp.name provider, (SELECT GROUP_CONCAT(gn.groupName) groupName FROM groupMember gm, groupName gn WHERE gn.groupID=gm.groupID " + groupCond1 + " AND cr.dialprefix LIKE CONCAT(gm.prefix,'%') AND cr.buyrate < gn.maxRate GROUP BY gm.prefix ORDER BY gm.prefix DESC LIMIT 1) gname FROM call_rates cr, call_providers cp WHERE cr.providerID=cp.providerID " + prefixCond + " " + destCond + " " + providerCond + ") l LEFT JOIN prefix_weight pw ON l.prefix=pw.prefix AND l.providerID=pw.providerID " + groupCond2 + " LIMIT 1000";
        }
        if (LOG) common.log("ratesGet", q);
        db.query_noerr(q, function(rows) {
            cb(undefined, { "rates" : rows });
        });
    });
    tasks.push(function (cb) {
        db.query_noerr("SELECT groupID, groupName FROM groupName WHERE qty!=0 AND status='A'", function(rows) {
            cb(undefined, { "groups" : rows });
        });
    });
    tasks.push(function (cb) {
        db.query_noerr("SELECT providerID, name, host, prefix, rounding FROM call_providers", function(rows) {
            cb(undefined, { "providers" : rows });
        });
    });
    async.parallel(tasks, function (err, result) {
        if (err) return callback(undefined, { "code" : -1 });
        var reply = new Object;
        result.forEach(function (item) {
            if (item) {
                if (item.providers) reply.providers = item.providers;
                if (item.groups) reply.groups = item.groups;
                if (item.rates) reply.rates = item.rates;
            }
        });
        callback(undefined, { "code" : 0, "rates" : reply.rates, "providers" : reply.providers, "groups" : reply.groups });
    });
}

function getPlans(query, callback) {
    var q_plans = "SELECT p.planID, p.name, GROUP_CONCAT(pp.providerID) providerID FROM plans p LEFT JOIN planProvider pp " +
        " ON p.planID=pp.planID GROUP BY p.planID";
    var q_providers = "SELECT providerID, name, host, prefix, rounding FROM call_providers";
    db.query_noerr(q_plans, function(planRows) {
        db.query_noerr(q_providers, function(providerRows) {
            callback(undefined, { "code" : 0, "plans" : planRows, "providers" : providerRows });
        });
    });
}

function getTopup(query, callback) {
    var q_topup = "SELECT a.creationdate ts, 'google' type, a.number, a.orderID id, a.purchase_token, a.actual_credit amount, " +
        " a.user_credit credited, a.voided, g.sku FROM android_inapp a, google_purchase g WHERE a.payload = g.payload AND " +
        " a.creationdate > DATE_SUB(NOW(), INTERVAL 1 MONTH) UNION " +
        " SELECT ts_end ts, 'apple' type, number, transaction_id id, receipt purchase_token, user_credit amount, " +
        " user_credit credited, 0 voided, product_id sku FROM inapp_purchase WHERE " +
        " ts_end > DATE_SUB(NOW(), INTERVAL 1 MONTH) ORDER BY ts DESC";
    if (LOG) common.log("getTopup", q_topup);
    db.query_noerr(q_topup, function(rows) {
        callback(undefined, { "code" : 0, "list" : rows });
    });
}

function dash2Seq(dash) {
    var start = parseInt(dash.split("-")[0]);
    var end = parseInt(dash.split("-")[1]);
    var seq = new Array;
    for ( var i=start; i<=end; i++ ) {
        seq.push(i.toString());
    }
    return seq;
}

function idtPulse(destination) {
    var pulse = { "start" : 1, "increment" : 1 };
    if ( (destination.toUpperCase().indexOf("MEXICO") > -1) ||
            (destination.toUpperCase().indexOf("TONGA") > -1) ||
            (destination.toUpperCase().indexOf("NAURU") > -1) ||
            (destination.toUpperCase().indexOf("PAPUA NEW GUINEA") > -1) ||
            (destination.toUpperCase().indexOf("SURINAM") > -1) ||
            (destination.toUpperCase().indexOf("KIRIBATI") > -1) ||
            (destination.toUpperCase().indexOf("WESTERN SAMOA") > -1) ||
            (destination.toUpperCase().indexOf("NIUE") > -1) ||
            (destination.toUpperCase().indexOf("HAITI") > -1) ||
            (destination.toUpperCase().indexOf("NEW CALEDONIA") > -1) ||
            (destination.toUpperCase().indexOf("VANUATU") > -1) ) {
        pulse.start = 60;
        pulse.increment = 60;
    } else if ( destination.toUpperCase().indexOf("GAMBIA") > -1 ) {
        pulse.start = 60;
        pulse.increment = 1;
    } else if ( destination.toUpperCase().indexOf("USA") > -1 ) {
        pulse.start = 6;
        pulse.increment = 6;
    }
    return pulse;
}

function tmPulse(destination) {
    var pulse = { "start" : 1, "increment" : 1 };
    if ( (destination.toUpperCase().indexOf("MEXICO") > -1) ||
            (destination.toUpperCase().indexOf("TONGA") > -1) ||
            (destination.toUpperCase().indexOf("NAURU") > -1) ||
            (destination.toUpperCase().indexOf("PAPUA NEW GUINEA") > -1) ||
            (destination.toUpperCase().indexOf("SURINAM") > -1) ||
            (destination.toUpperCase().indexOf("KIRIBATI") > -1) ||
            (destination.toUpperCase().indexOf("WESTERN SAMOA") > -1) ||
            (destination.toUpperCase().indexOf("VANUATU") > -1) ) {
        pulse.start = 60;
        pulse.increment = 60;
    }
    return pulse;
}

function tataPulse(destination) {
    var pulse = { "start" : 1, "increment" : 1 };
    if ( (destination.toUpperCase().indexOf("MEXICO") > -1) ||
            (destination.toUpperCase().indexOf("TONGA") > -1) ||
            (destination.toUpperCase().indexOf("NAURU") > -1) ||
            (destination.toUpperCase().indexOf("PAPUA NEW GUINEA") > -1) ||
            (destination.toUpperCase().indexOf("SURINAM") > -1) ||
            (destination.toUpperCase().indexOf("SOLOMON ISLANDS") > -1) ||
            (destination.toUpperCase().indexOf("KIRIBATI") > -1) ||
            (destination.toUpperCase().indexOf("WESTERN SAMOA") > -1) ||
            (destination.toUpperCase().indexOf("VANUATU") > -1) ) {
        pulse.start = 60;
        pulse.increment = 60;
    } else if ( destination.toUpperCase().indexOf("GAMBIA") > -1 ) {
        pulse.start = 60;
        pulse.increment = 1;
    }
    return pulse;
}

function isDate(s) {
    if (typeof(s) != "string") return false;
    else if (isNaN(Date.parse(s))) return false;
    else if ( Date.parse(s) < 0 ) return false;
    else return true;
}

function voxbeamParse(id, data) {
    var sheet = "";
    data.forEach(function (row) {
        var destination = row[2];
        var buyrate = row[3];
        var effective = row[9] + " " + row[10];
        var prefix = row[0];
        var billstart = parseInt(row[4]);
        var billincrement = parseInt(row[5]);
        if (effective && isDate(effective)) {
            sheet += "(" +
                db.escape(id) + "," +
                db.escape(prefix) + "," +
                db.escape(destination) + "," +
                db.escape(buyrate) + "," +
                db.escape(billstart) + "," +
                db.escape(billincrement) + "," +
                db.escape(new Date(effective).toISOString()) + "," +
                db.escape("A") +
            "),";
        }
    });
    return sheet;
}

function idtParse(id, data) {
    var sheet = "";
    data.forEach(function (row) {
        var destination = row[0];
        var buyrate = row[1];
        var effective = row[3];
        var prefix = row[4];
        var billstart = idtPulse(destination).start;
        var billincrement = idtPulse(destination).increment;
        if (effective && isDate(effective)) {
            sheet += "(" +
                db.escape(id) + "," +
                db.escape(prefix) + "," +
                db.escape(destination) + "," +
                db.escape(buyrate) + "," +
                db.escape(billstart) + "," +
                db.escape(billincrement) + "," +
                db.escape(new Date(effective).toISOString()) + "," +
                db.escape("A") +
            "),";
        }
    });
    return sheet;
}

function tataParse(id, data) {
    var sheet = new Array;
    data.forEach(function (row) {
        var destination = row[0] + " - " + row[1];
        var countrycode = row[2];
        var areacodes = row[3].split(",");
        var pricing = (row[4] == "") ? "NULL" : parseFloat(row[4]) * 1.07;
        var effective = row[5];
        var billstart = tataPulse(destination).start;
        var billincrement = tataPulse(destination).increment;
        var stat = (pricing == "NULL") ? "D" : "A";
        if (effective && isDate(effective)) {
            areacodes.forEach(function (code) {
                sheet += "(" + db.escape(id) + "," + countrycode + "" + code.trim() + "," + db.escape(destination) + "," + pricing + "," + billstart + "," + billincrement + "," + db.escape(new Date(effective).toISOString()) + "," + db.escape(stat) + "),";
            });
        }
    });
    return sheet;
}

function tmParse(id, data) {
    var sheet = new Array;
    data.forEach(function (row) {
        var destination = row[0];
        var product = row[1];
        var areacode = row[2];
        var asr = row[3];
        var unknown = row[4];
        var cli = row[5];
        var unknown = row[6];
        var pricing = (row[7] == "") ? "NULL" : parseFloat(row[7]);
        var effective = row[8];
        var billstart = tmPulse(destination).start;
        var billincrement = tmPulse(destination).increment;
        var stat = (pricing == "NULL") ? "D" : "A";
        if ( product.toUpperCase() == "TMSG PREMIUM" ) {
            var areacodes = new Array;
            areacode.split(";").forEach(function (code) {
                if ( code.indexOf("-") > -1 ) {
                    areacodes = areacodes.concat(dash2Seq(code.trim()));
                } else {
                    areacodes.push(code.trim());
                }
            });
            areacodes.forEach(function (code) {
                sheet += "(" + db.escape(id) + "," + code + "," + db.escape(destination) + "," + pricing + "," + billstart + "," + billincrement + "," + db.escape(new Date(effective).toISOString()) + "," + db.escape(stat) + "),";
            });
        }
    });
    return sheet;
}

function toggleWeight(params, query, fields, files, callback) {
    var provider = params.id;
    var prefix = fields.prefix;
    if ( (parseInt(fields.prefix) != fields.prefix) ||
        (parseInt(params.id) != params.id) ) return callback(undefined, { "code" : -1 });
    var q = "";
    if (fields.enable == "true") { 
        q += "INSERT INTO prefix_weight (providerID, prefix) SELECT providerID, dialprefix FROM call_rates " +
            " WHERE providerID=" + db.escape(params.id) + " AND dialprefix LIKE CONCAT(" + db.escape(fields.prefix) + ", '%')";
    } else if (fields.enable == "false") { 
        q += "DELETE FROM prefix_weight WHERE " +
            " providerID=" + db.escape(params.id) + " AND prefix LIKE CONCAT(" + db.escape(fields.prefix) + ", '%')";
    } else return callback(undefined, { "code" : -1 });
    common.log("toggleWeight", q);
    db.query_noerr(q, function(rows) {
        if (rows) callback(undefined, { "code" : 0 });
        else callback(undefined, { "code" : -1 });
    });
}

function updatePlan(params, query, fields, files, callback) {
    if ( (parseInt(fields.providerID) != fields.providerID) ||
        (parseInt(params.id) != params.id) ) return callback(undefined, { "code" : -1 });
    var q = "";
    if (fields.enable == "true") { 
        q += "INSERT INTO planProvider (planID, providerID) VALUES " +
            " (" + parseInt(params.id) + "," + parseInt(fields.providerID) + ")";
    } else if (fields.enable == "false") { 
        q += "DELETE from planProvider WHERE planID=" + parseInt(params.id) +
            " AND providerID=" + parseInt(fields.providerID);
    } else return callback(undefined, { "code" : -1 });
    common.log("updatePlan", q);
    db.query_noerr(q, function(rows) {
        if (rows) callback(undefined, { "code" : 0 });
        else callback(undefined, { "code" : -1 });
    });
}

function uploadRates(params, query, fields, files, callback) {
    var providerID = params.id;
    var path = (files && files.content) ? files.content.path : undefined;
    var fileName = (files && files.content) ? files.content.name : undefined;
    var providerName = (fields) ? fields.name : "";
    var host = (fields) ? fields.host : "";
    var prefix = (fields) ? fields.prefix : "";
    var rounding = (fields) ? fields.rounding : 0;
    var q_cp = "UPDATE call_providers set " +
        " name=" + db.escape(providerName) + ", " +
        " host=" + db.escape(host) + ", " +
        " prefix=" + db.escape(prefix) + ", " +
        " rounding=" + db.escape(rounding) +
        " WHERE providerID=" + db.escape(providerID);
    db.query_noerr(q_cp, function(result) {
        if (path) {
            csvParse(fs.readFileSync(path, "utf8"), function(err, data) {
                var values = (providerID == "10") ? tmParse(providerID, data) :
                    (providerID == "7") ? tataParse(providerID, data) :
                    ((providerID == "2") || (providerID == "3")) ? idtParse(providerID, data) :
                    ((providerID == "4") || (providerID == "6")) ? voxbeamParse(providerID, data) : undefined;
                fs.unlink(path);
                if (!values) {
                    if (err) common.error("uploadRates", "Empty Ratesheet");
                    return callback(undefined, { "code" : -1, "description" : "Empty Ratesheet" });
                }
                var q_ins = "INSERT INTO call_rates (providerID, dialprefix, destination, buyrate, " +
                    " billstart, billincrement, lastupdate, status) VALUES " + values.slice(0,-1) +
                    " ON DUPLICATE KEY UPDATE buyrate=VALUES(buyrate)";
                db.query_noerr(q_ins, function(result) {
                    // do cleanup: remove old rates (more than 2 from latest)
                    var q_old = "DELETE cr FROM call_rates cr LEFT JOIN " +
                        " (SELECT COUNT(c.callrateID) tot, MAX(c.lastupdate) eff, c.dialprefix " +
                        " FROM call_rates c WHERE c.providerID=" + db.escape(providerID) +
                        " AND c.lastupdate<NOW() GROUP BY c.dialprefix) s ON cr.dialprefix=s.dialprefix " +
                        " WHERE s.tot>1 AND cr.lastupdate!=s.eff AND cr.providerID=" + db.escape(providerID);
                    db.query_noerr(q_old, function(result) { });
                    //remove "D" (deleted prefixes) if older than today
                    var q_del = "DELETE FROM call_rates WHERE providerID=" + db.escape(providerID) +
                        " AND dialprefix IN (SELECT dialprefix FROM call_rates WHERE providerID=" + db.escape(providerID) +
                        " AND buyrate IS NULL AND status='D' AND lastupdate<NOW())";
                    db.query_noerr(q_del, function(result) { });
                    // update prefix_weight ???
                    callback(undefined, { "code" : 0 });
                });
            });
        } else callback(undefined, { "code" : 0 });
    });
}

function getActive(query, callback) {
    db.cache_keys("switch", "*", function (keys) {
        var getServer = function (id) {
            switch (id) {
                case 32:
                    return "titan";
                    break;
                case 33:
                    return "voyager";
                    break;
                default:
                    return undefined;
            }
        }
        var tasks = new Array;
        keys.forEach(function (key) {
            if (typeof(key) == "string") tasks.push(function (cb) {
                db.cache_hget("switch", key, function (cache) {
                    var active = (cache && cache.cdr) ? common.safeParse(cache.cdr) : undefined;
                    if (active) cb(undefined, {
                        "orig_ani" : active.orig_ani,
                        "call_id" : active.call_id,
                        "orig_dst_number" : active.orig_dst_number,
                        "server" : getServer(active.core_id),
                        "start_time" : active.start_time });
                    else cb(undefined, {});
                });
            });
        });
        async.parallel(tasks, function (err, result) {
            var list = new Array;
            result.forEach(function (item) {
                if (item && item.orig_ani) list.push(item);
            });
            callback(undefined, { "code" : 0, "list" : list });
        });
    });
}

function getOnline(query, callback) {
    sw.connect("ul.dump", undefined, undefined, function(err, result) {
        callback(undefined, { "code" : 0, "err" : err, "list" : result });
    });
}

function getInapp(query, callback) {
    var type = query.type;
    var id = query.id;
    var token = query.token;
    var sku = query.sku;
    var autoreverse = false;
    if ((["google", "apple"]).indexOf(type) == -1) return callback(new BaseError("unknown type", 400));
    if (!token || !sku) return callback(new BaseError("empty sku or token", 400));
    inappManager[type].validate(id, token, sku, autoreverse, function(err, result) {
        callback(undefined, { "code" : 0, "err" : err, "list" : [ result ] });
    });
}

function reverseInapp(params, query, fields, files, callback) {
    var type =(fields) ? fields.type : undefined;
    var token = (fields) ? fields.token : undefined;
    if ((["google", "apple"]).indexOf(type) == -1) return callback(new BaseError("unknown type", 400));
    if (!token) return callback(new BaseError("empty sku or token", 400));
    inappManager[type].invalidate(token, function(err, result) {
        callback(err, { "code" : 0 });
    });
}
