var logsManager = require('../../core/manager/log/logsManager');
var common = require('../../../lib/common');
var db = require('../../../lib/db_handler');
var web = require('./web');
var async = require('async');

//exports

exports.getIccids = getIccids;
exports.putIccids = putIccids;

//functions

function getIccids(req, res) {
    web.checkPermission("customers", req.gid, function (allowed) {
        if (!allowed) {
            res.status(403).json({"code": -1});
        } else {
            db.iccidPool.find({}).toArray(function(err, result){
                if(err){
                    res.json({code: -1, error: err});
                }else{
                    res.json({code: 0, list: result});
                }
            });
        }
    });
}


function putIccids(req, res){
    var purpose = req.fields.purpose;
    var list = req.fields.list.match(/[0-9]{18}/g);
    var finalResult = [];
    async.each(list, function(item, callback){
        db.iccidPool.findOne({_id: item}, function(err, result){
            if(err){
                finalResult.push({iccid: item, status: "error occoured when searching for existing ids"});
                callback(null);
            }else if(result && result._id == item){
                finalResult.push({iccid: item, status: "Iccid already in database"});
                callback(null);
            }else{
                db.iccidPool.insertOne({_id: item, "status": "Available", "purpose": purpose}, function(err, result){
                    if(err){
                        finalResult.push({iccid: item, status: "Failed to enter new iccid to database"});
                        callback(null);
                    }else{
                        finalResult.push({iccid: item, status: "Successfully enter iccid"});
                        callback(null);
                    }
                });
            }
        });
    }, function(error){
        res.json({code: 0, list: finalResult});
    })
}
