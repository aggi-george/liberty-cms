var async = require('async');
var fs = require('fs');
var jsmediatags = require("jsmediatags");
var exec = require('child_process').exec;
var common = require('../../../lib/common');
var db = require('../../../lib/db_handler');
var config = require(__base + '/config');

module.exports = {
    init: function(authCheck, webs, usagePath) {

        var perms = { "read" : 1, "write" : 0 }; 

        // inbound - put
        webs.put(usagePath + '/tag/:type', (req, res) => { authCheck("customers", perms, req, res, put) });

        // inbound - get
        webs.get(usagePath + '/get/:type', (req, res) => { authCheck("customers", perms, req, res, get) });
    }
}

function get(req, res, allowed) {
    var process = function(callback) {
        callback(req.params, req.query, req.user, req.path, allowed, function(error, result, file) {
            if (file) return fs.createReadStream(file).pipe(res);
            if (result) result.write = allowed.write;
            var httpCode = 200;
            if ( error && error.status && (parseInt(error.status) == error.status) ) {
                httpCode = error.status;
            }
            if (!res.headersSent) res.status(httpCode).json(result);
        });
    }
    if (req.params.type == "recordings") process(getRecordings);
    else if (req.params.type == "suspend") process(getRequests);
    else if (req.params.type == "terminate") process(getRequests);
    else if (req.params.type == "all") process(getRequests);
    else res.status(400).json({ "code" : -1, "description" : "unknown type" });
}

function put(req, res, allowed) {
    var start = new Date();
    var process = function(callback) {
        callback(req.params, req.query, req.fields, req.user, allowed, function(error, result) {
            if (result) result.write = allowed.write;
            var httpCode = 200;
            if ( error && error.status && (parseInt(error.status) == error.status) ) {
                httpCode = error.status;
            }
            if (!res.headersSent) res.status(httpCode).json(result);
            db.weblog.insert({
                "start_ts" : start,
                "ts" : new Date().getTime(),
                "ip" : req.ip,
                "type" : "inbound " + req.params.type,
                "id" : req.params.id,
                "action" : req.method,
                "username" : req.user,
                "code" : (result ? result.code : -1),
                "query" : req.query,
                "fields" : req.fields
            });
        });
    }
    if (req.params.type == "recordings") process(tagRecordings);
    else res.status(400).json({ "code" : -1, "description" : "unknown type" });
}

function getMeta(list, dir, callback) {
    var tasks = new Array;
    if (list) list.forEach(function (item) {
        if (!item) {
            return;
        }

        (function (item) {
            var cbCalled = false;

            tasks.push(function (cb) {
                var meta = {"filename": item};
                jsmediatags.read(dir + "/" + item, {

                    onSuccess: function (tag) {
                        meta.src = tag.tags.artist;
                        meta.dst = tag.tags.album;
                        meta.cat = tag.tags.genre;
                        meta.date = parseInt(tag.tags.year);
                        meta.owner = (tag.tags.comment) ? tag.tags.comment.text : undefined;

                        fs.stat(dir + "/" + item, function (err, stats) {
                            if (!cbCalled) {
                                cbCalled = true;

                                // size / ( sampling *  channels * bitrate / 8 )
                                meta.duration = parseInt(stats.size / ( 8000 * 1 * 16 / 8 ));
                                cb(undefined, meta);
                            }
                        });
                    },

                    onError: function (error) {
                        if (!cbCalled) {
                            cbCalled = true;
                            cb(undefined, meta);
                        }
                    }
                });
            });
        }(item));
    });

    async.parallel(tasks, function (err, result) {
        result.sort(function (a, b) {
            if (a.date < b.date) return 1; else return -1;
        });
        callback(err, result);
    });
}

function getRecordings(params, query, user, path, allowed, callback) {
    var id = (query) ? query.id : undefined;
    var dir = config.DEV ? "/mnt/voice/staging/messages" : "/mnt/voice/messages";
    if (!user) return callback(undefined, {"code" : -1, "error" : "getMeta Failed"});
    if (!id) {
        exec("ls " + dir + "/*.wav -t | head -500 | xargs -n1 basename", function(error, stdout, stderr) {
            var result = stdout.split("\n");
            getMeta(result, dir, function (err, list) {
                callback(undefined, { "code": 0, "list": list, "path" : path });
            });
        });
    } else {
        tagOwner(user, dir, id, function(err, result) {
            if ( !err && result && (result == user) ) {
                callback(undefined, { "code" : 0 }, dir + "/" + id);
            } else if ( allowed.write > 1 ) {
                callback(undefined, { "code" : 0 }, dir + "/" + id);
            } else {
                callback(undefined, { "code" : -1, "error" : "getMeta Failed" });
            }
        });
    }
}

function tagOwner(user, dir, filename, callback) {
    db.cache_lock("cache", "recording_" + filename, user, 60000, function (lock) {
        if (!lock) {
            db.cache_get("cache", "recording_" + filename, function (value) {
                callback(undefined, value);
            });
        } else {
            jsmediatags.read(dir + "/" + filename, {
                onSuccess: function (tag) {
                    if (user == tag.tags.comment) {
                        callback(undefined, user);
                    } else if (!tag.tags.comment) {
                        // needed to reset file permissions
                        exec("id3v2 -D " + dir + "/" + filename,
                            function (error1, stdout1, stderr1) {
                                if (stderr1) common.error("ID3 Strip", JSON.stringify(stderr1));
                                exec("id3v2 -c owner:" + user +
                                    " -a " + tag.tags.artist +
                                    " -A " + tag.tags.album +
                                    " -g " + tag.tags.genre +
                                    " -y " + tag.tags.year +
                                    " " + dir + "/" + filename,
                                    function (error2, stdout2, stderr2) {
                                        if (!error2 && !stderr2) {
                                            callback(undefined, user);
                                        } else {
                                            common.error("ID3 Assign Owner", JSON.stringify(stderr2));
                                            db.cache_del("cache", "recording_" + filename);
                                            callback(new Error(-1));
                                        }
                                    });
                            });
                    } else {
                        callback(new Error(tag.tags.comment));
                    }
                }, onError: function (error) {
                    callback(new Error(-1));
                }
            });
        }
    });
}

function tagRecordings(params, query, fields, user, allowed, callback) {
    var type = params.type;
    var filename = fields ? fields.filename : "";
    if (!user || !filename) return callback(undefined, { "code" : -1 });
    var dir = config.DEV ? "/mnt/voice/staging/messages" : "/mnt/voice/messages";
    getMeta([filename], dir, function (err, list) {
        if (list && list[0]) {
            var reply = { "type" : type, "filename" : filename };
            if (!list[0].owner) {
                tagOwner(user, dir, filename, function (error, result) {
                    if (error && !result) {
                        if (error.message == "-1") {
                            reply.code = -1;
                        } else {
                            reply.code = 1;
                            reply.owner = error.message;
                        }
                    } else {
                        reply.code = 0;
                        reply.owner = result;
                    }
                    callback(undefined, reply);
                });
            } else if (list[0].owner == user) {
                reply.code = 0;
                reply.owner = list[0].owner;
                callback(undefined, reply);
            } else if (allowed.write > 1) {
                reply.code = 0;
                reply.owner = list[0].owner;
                callback(undefined, reply);
            } else if (list[0].owner != user) {
                reply.code = 1;
                reply.owner = list[0].owner;
                callback(undefined, reply);
            }
        } else {
            common.error("tagRecordings", "Cannot get file metadata " + filename);
            callback(undefined, { "code": -1 });
        }
    });
}

function getRequests(params, query, user, path, allowed, callback) {
    var type = params.type;
    var match = new Array;
    if ( (type == "all") || (type == "suspend") ) match.push({ "activity" : "sim_loss_suspension_request" });
    if ( (type == "all") || (type == "terminate") ) match.push({ "activity" : "termination_request_followup" });
    if (match.length == 0) return callback(undefined, { "code" : -1, "list": [], "type": type });
    db.notifications_webhook.find({
            "ts" : { "$gt" : (new Date().getTime() - (30 * 24 * 60 * 60 * 1000)) }, "$or" : match },{
            "_id" : 0,
            "ts" : 1,
            "number" : 1,
            "notification.ANI" : 1,
            "notification.DNIS" : 1,
            "notification.DOB" : 1,
            "notification.NRIC" : 1 }).toArray(function (err, result) {
        callback(undefined, { "code" : 0, "list": result, "type": type });
    });
}
