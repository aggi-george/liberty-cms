var async = require('async');
var fs = require('fs');
var dateformat = require('dateformat');

var config = require('../../../config');
var common = require('../../../lib/common');
var db = require('../../../lib/db_handler');
var ec = require('../../../lib/elitecore');
var web = require('./web');

var ecommManager = require('../../core/manager/ecomm/ecommManager');
var invoiceManager = require('../../core/manager/account/invoiceManager');
var transactionManager = require('../../core/manager/account/transactionManager');
var invoiceNotificationManager = require('../../core/manager/account/invoiceNotificationManager');
var syncQueueService = require(`${global.__queue}/sync`);
var logsManager = require('../../core/manager/log/logsManager');

var log = config.LOGSENABLED;

//exports

exports.getMonths = getMonths;
exports.getBills = getBills;
exports.getFile = getFile;
exports.getSyncLogs = getSyncLogs;
exports.getReport = getReport;
exports.syncInvoices = syncInvoices;
exports.syncUnpaidInvoices = syncUnpaidInvoices;
exports.syncInvoicesProgress = syncInvoicesProgress;
exports.updatePayments = updatePayments;
exports.runBatchPayment = runBatchPayment;
exports.sendBillsAll = sendBillsAll;
exports.sendBillSingle = sendBillSingle;
exports.sendReminderNonPayment = sendReminderNonPayment;
exports.getTransactions = getTransactions;
exports.recoverTransaction = recoverTransaction;
exports.loadTransactionStatus = loadTransactionStatus;

//functions

function getMonths(req, res) {
    web.checkPermission("bills", req.gid, function (allowed) {
        if (!allowed) {
            res.status(403).json({"code": -1});
        } else {
            var limit = parseInt(req.query.iDisplayLength);
            var offset = parseInt(req.query.iDisplayStart);
            var filter = req.query.sSearch;

            invoiceManager.loadMonths({
                limit: limit,
                offset: offset,
                filter: filter
            }, (err, result) => {
                if (err) {
                    return res.status(400).json({
                        code: -1,
                        error: err.message,
                        write: allowed.write
                    });
                }

                res.json({
                    code: 0,
                    write: allowed.write,
                    iTotalRecords: result.totalCount,
                    iTotalDisplayRecords: result.totalCount,
                    data: result.data
                });
            });
        }
    });
}

function getBills(req, res) {
    web.checkPermission("bills", req.gid, function (allowed) {
        if (!allowed) {
            res.status(403).json({"code": -1});
        } else {
            var limit = parseInt(req.query.iDisplayLength);
            var offset = parseInt(req.query.iDisplayStart);
            var filter = req.query.sSearch;
            var status = req.query.status;
            var type = req.query.type;
            var dependencyType = req.query.dependencyType;
            var paymentType = req.query.paymentType;
            var monthId = req.params.monthId ? parseInt(req.params.monthId) : undefined;

            invoiceManager.loadBills(monthId, {
                limit: limit,
                offset: offset,
                filter: filter,
                status: status,
                type: type,
                dependencyType: dependencyType,
                paymentType: paymentType
            }, (err, result) => {
                if (err) {
                    return res.status(400).json({
                        code: -1,
                        error: err.message,
                        write: allowed.write
                    });
                }

                res.json({
                    code: 0,
                    write: allowed.write,
                    iTotalRecords: result.totalCount,
                    iTotalDisplayRecords: result.totalCount,
                    data: result.data
                });
            });
        }
    });
}

function getFile(req, res) {
    web.checkPermission("customers", req.gid, function (allowed) {
        if (!allowed) {
            res.status(403).json({"code": -1});
        } else {
            var path = decodeURIComponent(req.query.path);
            fs.stat(path, (err, stat) => {
                if (err) {
                    return res.status(400).json({
                        code: -1,
                        error: err.message
                    });
                }

                if (!stat) {
                    return res.status(400).json({
                        code: -1,
                        error: "No File Found"
                    });
                }

                res.writeHead(200, {
                    'Content-Type': 'application/pdf',
                    'Content-Length': stat.size
                });

                var readStream = fs.createReadStream(path);
                readStream.pipe(res);
            });
        }
    });
}

function getSyncLogs(req, res) {
    web.checkPermission("bills", req.gid, function (allowed) {
        if (!allowed) {
            res.status(403).json({"code": -1});
        } else {
            logsManager.loadLogs("bill", "sync", 0, 5, undefined,
                undefined, undefined, undefined, (err, result) => {
                    if (err) {
                        return res.status(400).json({
                            "code": -1,
                            "error": err.message
                        });
                    }
                    res.json({
                        "code": 0,
                        "write": allowed.write,
                        "data": result.data
                    });
                })
        }
    });
}

function getReport(req, res) {
    web.checkPermission("bills", req.gid, function (allowed) {
        if (!allowed) {
            res.status(403).json({"code": -1});
        } else {
            var monthId = req.params.monthId ? parseInt(req.params.monthId) : undefined;

            invoiceManager.loadReport(monthId, {}, (err, result) => {
                if (err) {
                    return res.status(400).json({
                        code: -1,
                        error: err.message,
                        write: allowed.write
                    });
                }

                res.json({
                    code: 0,
                    data: result.data
                });
            });
        }
    });
}

function updatePayments(req, res) {
    web.checkPermission("bills", req.gid, function (allowed) {
        if (!allowed || !allowed.write) {
            res.status(403).json({"code": -1});
        } else {
            var initiator = "[CMS][" + req.user + "]";
            var file = req.fields.file;
            var overrideLock = req.fields.overrideLock;

            invoiceManager.syncPaymentsFromFile(file, "temp", {
                overrideLock: overrideLock,
                initiator: initiator
            }, (err) => {
                if (err) {
                    return res.status(400).json({
                        code: -1,
                        error: err.message
                    });
                }

                res.json({
                    code: 0,
                    message: "Payment statuses has been synced"
                });
            });
        }
    });
}

function runBatchPayment(req, res) {
    web.checkPermission("bills", req.gid, function (allowed) {
        if (!allowed || !allowed.write) {
            res.status(403).json({"code": -1});
        } else {
            var initiator = "[CMS][" + req.user + "]";
            var batchSize = req.fields.batchSize ? parseInt(req.fields.batchSize) : -1;
            var billingAccountNumber = req.fields.billingAccountNumber;
            var silent = req.fields.silent == "true";
            var successSync = req.fields.successSync == "true";
            var monthNumber = req.fields.monthNumber ? parseInt(req.fields.monthNumber): -1;
            var ignoreInvalidSync = req.fields.ignoreInvalidSync == "true";
            var overrideLock = req.fields.overrideLock == "true";
            var noPaymentAttempt = req.fields.noPaymentAttempt == "true";
            var retryUnfinished = req.fields.retryUnfinished == "true";
            var ignoreSuspension = req.fields.ignoreSuspension == "true";

            invoiceManager.runBatchPayment(billingAccountNumber, {
                batchSize: batchSize,
                silent: silent,
                successSync: successSync,
                monthNumber: monthNumber,
                overrideLock: overrideLock,
                noPaymentAttempt: noPaymentAttempt,
                ignoreInvalidSync: ignoreInvalidSync,
                retryUnfinished: retryUnfinished,
                ignoreSuspension: ignoreSuspension,
                initiator: initiator,
                platform: "CMS"
            }, (err) => {
                if (err) {
                    return res.status(400).json({
                        code: -1,
                        error: err.message
                    });
                }

                res.json({
                    code: 0,
                    message: "Payment batch request has been processed, payments were added to the queue"
                });
            });
        }
    });
}

function syncUnpaidInvoices(req, res) {
    web.checkPermission("bills", req.gid, function (allowed) {
        if (!allowed || !allowed.write) {
            res.status(403).json({"code": -1});
        } else {
            var initiator = "[CMS][" + req.user + "]";

            invoiceManager.syncUnpaidBills({initiator: initiator}, (err, result) => {
                if (err) {
                    return res.status(400).json({
                        code: -1,
                        error: err.message
                    });
                }

                res.json({
                    code: 0,
                    message: "Finished payment status check for " +
                    (result && result.localUnpaidInvoices ? result.localUnpaidInvoices.countUnpaid : -1) + ", update status for " +
                    (result && result.bssUnpaidInvoices ? result.bssUnpaidInvoices.changedStatusCount : -1) + " invoices"
                });
            });
        }
    });
}

function syncInvoices(req, res) {
    web.checkPermission("bills", req.gid, function (allowed) {
        if (!allowed || !allowed.write) {
            res.status(403).json({"code": -1});
        } else {
            var initiator = "[CMS][" + req.user + "]";
            var monthDate = req.fields.monthDate;
            var month = req.fields.month && req.fields.month.split('-').length == 2
                ? new Date(req.fields.month.split("-")[1], req.fields.month.split("-")[0], 1) : undefined;
            var overrideLock = req.fields.overrideLock == "true";
            var verifyContent = req.fields.verifyContent;

            if (!monthDate) {
                monthDate = month;
            }

            if (!monthDate || !new Date(monthDate).getTime()) {
                return res.status(400).json({
                    code: -1,
                    error: "Invalid month date"
                });
            }

            syncQueueService.run("SYNC_INVOICES", {
                monthDate: monthDate,
                options: {
                    verifyContent: verifyContent,
                    overrideLock: overrideLock,
                    initiator: initiator
                }
            }, {
                errorIfRunning: true,
                initiator: initiator
            }, (err, result) => {
                if (err) {
                    return res.status(400).json({
                        code: -1,
                        error: err.message
                    });
                }

                res.json({
                    code: 0,
                    message: "Sync of Invoices Will be Started Shortly."
                });
            });
        }
    });
}

function sendBillsAll(req, res) {
    web.checkPermission("bills", req.gid, function (allowed) {
        if (!allowed || !allowed.write) {
            res.status(403).json({"code": -1});
        } else {
            var initiator = "[CMS][" + req.user + "]";
            var monthId = req.fields.monthId ? parseInt(req.fields.monthId) : -1;
            var batchSize = req.fields.batchSize ? parseInt(req.fields.batchSize) : -1;
            var paymentType = req.fields.paymentType;
            var delay = req.fields.delay ? parseInt(req.fields.delay) : -1;
            var resendFailed = req.fields.resendFailed == "true";
            var overrideLock = req.fields.overrideLock == "true";
            var type = req.fields.type;

            if (!monthId || !batchSize) {
                return res.status(400).json({
                    code: -1,
                    error: "Invalid params"
                });
            }

            syncQueueService.run("SEND_ALL_PDF_FILES", {
                monthId: monthId,
                options: {
                    delay: delay,
                    batchSize: batchSize,
                    type: type,
                    paymentType: paymentType,
                    resendFailed: resendFailed,
                    overrideLock: overrideLock,
                    initiator: initiator
                }
            }, {
                errorIfRunning: true,
                initiator: initiator
            }, (err, result) => {
                if (err) {
                    return res.status(400).json({
                        code: -1,
                        error: err.message
                    });
                }

                res.json({
                    code: 0,
                    message: "PDF Bill Sending Will be Started Shortly."
                });
            });
        }
    });
}

function sendBillSingle(req, res) {
    web.checkPermission("bills", req.gid, function (allowed) {
        if (!allowed || !allowed.write) {
            res.status(403).json({"code": -1});
        } else {
            var initiator = "[CMS][" + req.user + "]";
            var invoiceId = req.fields.invoiceId;

            invoiceNotificationManager.sendSingleBill(invoiceId, {initiator: initiator}, (err) => {
                if (err) {
                    return res.status(400).json({
                        code: -1,
                        error: err.message
                    });
                }
                res.json({code: 0});
            });
        }
    });
}

function sendReminderNonPayment(req, res) {
    web.checkPermission("bills", req.gid, function (allowed) {
        if (!allowed || !allowed.write) {
            res.status(403).json({"code": -1});
        } else {
            var initiator = "[CMS][" + req.user + "]";
            var type = req.fields.type;
            var resendFailed = req.fields.resendFailed == "true";
            var overrideLock = req.fields.overrideLock == "true";
            var timeUnique = req.fields.timeUnique ? parseInt(req.fields.timeUnique) : -1;
            var batchSize = req.fields.batchSize ? parseInt(req.fields.batchSize) : -1;
            var delay = req.fields.delay ? parseInt(req.fields.delay) : -1;

            if (!type) {
                return res.status(400).json({
                    code: -1,
                    error: "Invalid params"
                });
            }

            syncQueueService.run("SEND_NON_PAYMENT_REMINDERS", {
                type: type,
                options: {
                    resendFailed: resendFailed,
                    overrideLock: overrideLock,
                    timeUnique: timeUnique,
                    batchSize: batchSize,
                    delay: delay,
                    initiator: initiator
                }
            }, {
                errorIfRunning: true,
                initiator: initiator
            }, (err, result) => {
                if (err) {
                    return res.status(400).json({
                        code: -1,
                        error: err.message
                    });
                }

                res.json({
                    code: 0,
                    message: "Non-Payment Reminders Sending Will be Started Shortly."
                });
            });
        }
    });
}

function syncInvoicesProgress(req, res) {
    web.checkPermission("bills", req.gid, function (allowed) {
        if (!allowed) {
            res.status(403).json({"code": -1});
        } else {
            invoiceManager.loadBillsSyncProgress((err, result) => {
                if (err) {
                    return res.status(400).json({
                        code: -1,
                        error: err.message,
                        write: allowed.write
                    });
                }

                res.json({
                    code: 0,
                    stepsName: result.stepsName,
                    stepsCount: result.stepsCount,
                    stepNumber: result.stepNumber,
                    stepProgress: result.stepProgress
                });
            });
        }
    });
}

function getTransactions(req, res) {
    web.checkPermission("bills", req.gid, function (allowed) {
        if (!allowed) {
            res.status(403).json({"code": -1});
        } else {
            var limit = parseInt(req.query.iDisplayLength);
            var offset = parseInt(req.query.iDisplayStart);
            var filter = req.query.sSearch;
            var status = req.query.status;
            var type = req.query.type;
            var completion = req.query.completion;
            var startDate = parseInt(req.query.startDate);
            var endDate = parseInt(req.query.endDate);

            transactionManager.loadTransactions({
                limit: limit,
                offset: offset,
                filter: filter,
                status: status,
                type: type,
                completion: completion,
                startDate: startDate,
                endDate: endDate
            }, (err, result) => {
                if (err) {
                    return res.status(400).json({
                        code: -1,
                        error: err.message,
                        write: allowed.write
                    });
                }

                res.json({
                    code: 0,
                    write: allowed.write,
                    iTotalRecords: result.totalCount,
                    iTotalDisplayRecords: result.totalCount,
                    data: result.data
                });
            });
        }
    });
}

function recoverTransaction(req, res) {
    web.checkPermission("bills", req.gid, function (allowed) {
        if (!allowed || !allowed.write) {
            res.status(403).json({"code": -1});
        } else {
            var initiator = "[CMS][" + req.user + "]";
            var id = req.fields.id ? parseInt(req.fields.id) : -1;
            var paymentFailedStrategy = req.fields.paymentFailedStrategy ? req.fields.paymentFailedStrategy : undefined;
            var paymentSucceedStrategy = req.fields.paymentSucceedStrategy ? req.fields.paymentSucceedStrategy : undefined;
            var paymentInProgressStrategy = req.fields.paymentInProgressStrategy ? req.fields.paymentInProgressStrategy : undefined;
            var skipActions = req.fields.skipActions == 'true';

            transactionManager.recoverTransaction(id,
                paymentFailedStrategy,
                paymentSucceedStrategy,
                paymentInProgressStrategy, {
                    skipActions,
                    initiator
            }, (err, result) => {
                if (err) {
                    return res.status(400).json({
                        code: -1,
                        error: err.message,
                        write: allowed.write
                    });
                }

                res.json({code: 0});
            });
        }
    });
}

function loadTransactionStatus(req, res) {
    web.checkPermission("bills", req.gid, function (allowed) {
        if (!allowed || !allowed.write) {
            res.status(403).json({"code": -1});
        } else {
            var initiator = "[CMS][" + req.user + "]";
            var orderRef = req.query.orderRef;
            var uuid = req.query.uuid;
            var billingAccountNumber = req.query.billingAccountNumber;

            ecommManager.checkStatus(billingAccountNumber, uuid, orderRef, (err, result) => {
                if (err) {
                    return res.status(400).json({
                        code: -1,
                        error: err.message,
                        write: allowed.write
                    });
                }

                res.json({code: 0, result});
            });
        }
    });
}
