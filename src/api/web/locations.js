var async = require('async');
var fs = require('fs');
var md5 = require("MD5");
var mime = require('mime');

var web = require('./web');
var config = require('../../../config');
var common = require('../../../lib/common');
var db = require('../../../lib/db_handler');
var locationManager = require('../../core/manager/promotions/locationManager');

var log = config.LOGSENABLED;

//exports

exports.getProviders = getProviders;
exports.putProvider = putProvider;
exports.getPoints = getPoints;
exports.getLogs = getLogs;
exports.getPromos = getPromos;
exports.putPoint = putPoint;
exports.putRedeemPromo = putRedeemPromo;
exports.deleteProvider = deleteProvider;
exports.deletePoint = deletePoint;
exports.deletePromo = deletePromo;
exports.getCustomersForProviders = getCustomersForProviders;
exports.getCustomersForGeoLocations = getCustomersForGeoLocations;
exports.getRedeemedCustomersForGeoLocations = getRedeemedCustomersForGeoLocations;
exports.getRedeemedCustomersForProviders = getRedeemedCustomersForProviders;

//functions

function getProviders(req, res) {
    web.checkPermission("marketing", req.gid, function (allowed) {
        if (!allowed) {
            res.status(403).json({"code": -1});
        } else {
            var limit = parseInt(req.query.iDisplayLength);
            var offset = parseInt(req.query.iDisplayStart);
            var filter = req.query.sSearch;

            locationManager.loadProviders({
                limit: limit,
                offset: offset,
                filter: filter
            }, (err, result) => {
                if (err) {
                    return res.status(400).json({
                        code: 0,
                        error: err.message
                    });
                }

                return res.json({
                    code: 0,
                    iTotalRecords: result.totalCount,
                    iTotalDisplayRecords: result.totalCount,
                    data: result.data
                });
            });
        }
    });
}

function getPoints(req, res) {
    web.checkPermission("marketing", req.gid, function (allowed) {
        if (!allowed) {
            res.status(403).json({"code": -1});
        } else {
            var providerId = !req.query.providerId || req.query.providerId == "ALL"
                ? undefined : parseInt(req.query.providerId);
            var limit = parseInt(req.query.iDisplayLength);
            var offset = parseInt(req.query.iDisplayStart);
            var filter = req.query.sSearch;

            locationManager.loadPoints({
                providerId: providerId,
                limit: limit,
                offset: offset,
                filter: filter
            }, (err, result) => {
                if (err) {
                    return res.status(400).json({
                        code: 0,
                        error: err.message
                    });
                }

                return res.json({
                    code: 0,
                    iTotalRecords: result.totalCount,
                    iTotalDisplayRecords: result.totalCount,
                    data: result.data
                });
            });
        }
    });
}

function getLogs(req, res) {
    web.checkPermission("marketing", req.gid, function (allowed) {
        if (!allowed) {
            res.status(403).json({"code": -1});
        } else {
            var pointId = !req.query.pointId || req.query.pointId == "ALL" ? undefined : parseInt(req.query.pointId);
            var providerId = !req.query.providerId || req.query.providerId == "ALL" ? undefined : parseInt(req.query.providerId);
            var limit = parseInt(req.query.iDisplayLength);
            var offset = parseInt(req.query.iDisplayStart);
            var filter = req.query.sSearch;

            locationManager.loadLogs({
                pointId: pointId,
                providerId: providerId,
                limit: limit,
                offset: offset,
                filter: filter
            }, (err, result) => {
                if (err) {
                    return res.status(400).json({
                        code: 0,
                        error: err.message
                    });
                }

                return res.json({
                    code: 0,
                    iTotalRecords: result.totalCount,
                    iTotalDisplayRecords: result.totalCount,
                    data: result.data
                });
            });
        }
    });
}

function getPromos(req, res) {
    web.checkPermission("marketing", req.gid, function (allowed) {
        if (!allowed) {
            res.status(403).json({"code": -1});
        } else {
            var providerId = !req.query.providerId || req.query.providerId == "ALL" ? undefined : parseInt(req.query.providerId);
            var limit = parseInt(req.query.iDisplayLength);
            var offset = parseInt(req.query.iDisplayStart);
            var filter = req.query.sSearch;

            locationManager.loadRedemptions({
                providerId: providerId,
                limit: limit,
                offset: offset,
                filter: filter
            }, (err, result) => {
                if (err) {
                    return res.status(400).json({
                        code: 0,
                        error: err.message
                    });
                }

                return res.json({
                    code: 0,
                    iTotalRecords: result.totalCount,
                    iTotalDisplayRecords: result.totalCount,
                    data: result.data
                });
            });
        }
    });
}

function putProvider(req, res) {
    web.checkPermission("marketing", req.gid, function (allowed) {
        if (!allowed) {
            res.status(403).json({"code": -1});
        } else {
            var id = req.fields.id;
            var name = req.fields.name;
            var enabled = req.fields.enabled == 'true';
            var activity = req.fields.activity;
            var redemptionCodeLength = req.fields.redemptionCodeLength ? parseInt(req.fields.redemptionCodeLength) : 0;
            var redemptionImageId = req.fields.redemptionImageId;
            var redemptionMessage = req.fields.redemptionMessage;
            var redemptionTitle = req.fields.redemptionTitle;
            var generateRedemptionCode = req.fields.generateRedemptionCode == 'true';
            var instruction = req.fields.instruction;
            var validUntil = req.fields.validUntil;
            var redeemedMessage = req.fields.redeemedMessage;

            locationManager.createUpdateProvider({
                id,
                name,
                enabled,
                activity,
                redemptionCodeLength,
                redemptionImageId,
                redemptionMessage,
                redemptionTitle,
                generateRedemptionCode,
                instruction,
                validUntil,
                redeemedMessage,
            }, (err, result) => {
                if (err) {
                    return res.status(400).json({
                        code: 0,
                        error: err.message
                    });
                }

                return res.json({code: 0});
            });
        }
    });
}

function putPoint(req, res) {
    web.checkPermission("marketing", req.gid, function (allowed) {
        if (!allowed) {
            res.status(403).json({"code": -1});
        } else {
            var id = req.fields.id;
            var providerId = req.fields.providerId;
            var label = req.fields.label;
            var enabled = req.fields.enabled == 'true';
            var latitude = req.fields.latitude ? parseFloat(req.fields.latitude) : 0;
            var longitude = req.fields.longitude ? parseFloat(req.fields.longitude) : 0;
            var radius = req.fields.radius ? parseFloat(req.fields.radius) : 0;
            var activity = req.fields.activity;
            var generateRedemptionCode = req.fields.generateRedemptionCode == 'true';
            var redemptionCode = req.fields.redemptionCode;

            locationManager.createUpdatePoint({
                id: id,
                providerId: providerId,
                label: label,
                enabled: enabled,
                latitude: latitude,
                longitude: longitude,
                radius: radius,
                activity: activity,
                generateRedemptionCode: generateRedemptionCode,
                redemptionCode: redemptionCode
            }, (err, result) => {
                if (err) {
                    return res.status(400).json({
                        code: 0,
                        error: err.message
                    });
                }

                return res.json({code: 0});
            });
        }
    });
}

function putRedeemPromo(req, res) {
    web.checkPermission("marketing", req.gid, function (allowed) {
        if (!allowed) {
            res.status(403).json({"code": -1});
        } else {
            var id = req.fields.id;
            var redemptionCode = req.fields.redemptionCode;

            locationManager.redeemPromo(id, redemptionCode, (err, result) => {
                if (err) {
                    return res.status(400).json({
                        code: 0,
                        error: err.message
                    });
                }

                return res.json({code: 0});
            });
        }
    });
}

function deleteProvider(req, res) {
    web.checkPermission("marketing", req.gid, function (allowed) {
        if (!allowed) {
            res.status(403).json({"code": -1});
        } else {
            var id = req.fields.id;
            locationManager.removeProvider(id, (err, result) => {
                if (err) {
                    return res.status(400).json({
                        code: 0,
                        error: err.message
                    });
                }

                return res.json({code: 0});
            });
        }
    });
}

function deletePoint(req, res) {
    web.checkPermission("marketing", req.gid, function (allowed) {
        if (!allowed) {
            res.status(403).json({"code": -1});
        } else {
            var id = req.fields.id;
            locationManager.removePoint(id, (err, result) => {
                if (err) {
                    return res.status(400).json({
                        code: 0,
                        error: err.message
                    });
                }

                return res.json({code: 0});
            });
        }
    });
}

function deletePromo(req, res) {
    web.checkPermission("marketing", req.gid, function (allowed) {
        if (!allowed) {
            res.status(403).json({"code": -1});
        } else {
            var id = req.fields.id;
            locationManager.removePromo(id, (err, result) => {
                if (err) {
                    return res.status(400).json({
                        code: 0,
                        error: err.message
                    });
                }

                return res.json({code: 0});
            });
        }
    });
}

function getCustomersForGeoLocations(req, res){
    web.checkPermission("marketing", req.gid, function (allowed) {
        if (!allowed) {
            res.status(403).json({"code": -1});
        } else {
            locationManager.getGeneratedPromosForPoints((err, result) => {
                if (err) {
                    return res.status(400).json({
                        code: -1,
                        error: err.message
                    });
                }

                return res.json({code: 0, list: result.list});
            });
        }
    });
}

function getRedeemedCustomersForGeoLocations(req, res){
    web.checkPermission("marketing", req.gid, function (allowed) {
        if (!allowed) {
            res.status(403).json({"code": -1});
        } else {
            locationManager.getRedeemedPromosForPoints((err, result) => {
                if (err) {
                    return res.status(400).json({
                        code: -1,
                        error: err.message
                    });
                }

                return res.json({code: 0, list: result.list});
            });
        }
    });
}

function getCustomersForProviders(req, res){
    web.checkPermission("marketing", req.gid, function (allowed) {
        if (!allowed) {
            res.status(403).json({"code": -1});
        } else {
            locationManager.getGeneratedPromosForProvider((err, result) => {
                if (err) {
                    return res.status(400).json({
                        code: -1,
                        error: err.message
                    });
                }

                return res.json({code: 0, list: result.list});
            });
        }
    });
}

function getRedeemedCustomersForProviders(req, res){
    web.checkPermission("marketing", req.gid, function (allowed) {
        if (!allowed) {
            res.status(403).json({"code": -1});
        } else {
            locationManager.getRedeemedPromosForProvider((err, result) => {
                if (err) {
                    return res.status(400).json({
                        code: -1,
                        error: err.message
                    });
                }

                return res.json({code: 0, list: result.list});
            });
        }
    });
}