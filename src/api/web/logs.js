var logsManager = require('../../core/manager/log/logsManager');
var common = require('../../../lib/common');
var config = require(__base  + '/config');
var web = require('./web');

const LOG = config.LOGSENABLED;

//exports

exports.loadLogs = loadLogs;

//functions

function loadLogs(req, res) {
    web.checkPermission("customers", req.gid, function (allowed) {
        if (!allowed) {
            res.status(403).json({"code": -1});
        } else {
            var category = req.params.category;
            var type = req.params.type;
            var length = parseInt(req.query.iDisplayLength);
            var offset = parseInt(req.query.iDisplayStart);
            var period = req.query.period ? parseInt(req.query.period) : -1;
            var searchItem = req.query.sSearch;
            var projections = req.query.projections ? JSON.parse(req.query.projections) : {};
            var noTotalCount = req.query.noTotalCount == 'true';
            if(Array.isArray(searchItem)){
                if(searchItem[0].length == 0){
                    filter = common.escapeHtml(searchItem[1]);
                }else{
                    filter = common.escapeHtml(searchItem[0]);
                }
            }else{
                var filter = common.escapeHtml(req.query.sSearch);
            }
            var searchType = common.escapeHtml(req.query.searchType);
            var matchType = common.escapeHtml(req.query.matchType);

            logsManager.loadLogs(category, type, offset, length, period, filter, searchType, matchType, function(err, result) {
                if (err) {
                    common.error("LogManagerApi Error occurred from logs", err);
                    return res.status(400).json({
                        "code": -1,
                        "error": err.message
                    });
                }

                if (LOG) common.log("LogManagerApi", "loaded");

                web.checkPermission("bills", req.gid, (allowed) => {
                    res.json({
                        "code": 0,
                        "write": allowed.write,
                        "data": result.data,
                        "iTotalRecords": result.totalCount,
                        "iTotalDisplayRecords": result.totalCount,
                        "access": {"downloadButton": allowed}
                    });
                })
            }, projections, noTotalCount);
        }
    });
}
