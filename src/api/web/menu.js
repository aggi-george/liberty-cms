var db = require(__lib + '/db_handler');
var common = require('../../../lib/common');

module.exports = {

    init: function(webs, usagePath) {
        webs.get(usagePath + '/get', function(req, res) {
            get(req.gid, function(err, result) {
                var list = (!err && result) ? result : [];
                var code = (!err && result) ? 0 : -1;
                res.json({ 'code' : code, 'list' : list });
            });
        });
    },

}

function get(gid, callback) {
    var list = new Array;
    var q = 'SELECT GROUP_CONCAT(DISTINCT p.page,":",p.put) pages FROM groupsPermissions gp, permissions p WHERE gp.permissionID=p.id ' +
        'AND gp.groupID=' + db.escape(gid) + ' AND (p.put=1 OR p.get=1)';

    db.query_err(q, function (err, rows) {
        if (err) {
            common.error('menu get', err.message);
            return callback(err);
        } else if (!rows || !rows[0] || !rows[0].pages) {
            return callback(undefined, []);
        }
        if (rows[0].pages.indexOf('dashboard') > -1) {
            list.push({
                'class': 'fa fa-dashboard',
                'span': 'Dashboard',
                'sub': [
                    {
                        'href': 'dashboard/dashboard.html',
                        'span': 'HUD'
                    },
                    {
                        'href': 'dashboard/dashboard_cms.html',
                        'span': 'CMS - Kirk'
                    },
                    {
                        'href': 'dashboard/dashboard_mobile.html',
                        'span': 'Mobile'
                    },
                    {
                        'href': 'dashboard/dashboard_elitecore.html',
                        'span': 'Elitecore'
                    },
                    {
                        'href': 'dashboard/dashboard_ecomm.html',
                        'span': 'ECommerce'
                    }]
            });

        }
        if (rows[0].pages.indexOf('customers') > -1) {
            list.push({
                'class': 'fa fa-users',
                'span': 'Customers',
                'sub': [
                    {
                        'href': 'customers/customers.html',
                        'span': 'Customers'
                    },
                    {
                        'href': 'customers/scheduler.html',
                        'span': 'Scheduler'
                    },
                    {
                        'href': 'customers/port_in_requests.html',
                        'span': 'PortIn Requests'
                    },
                    {
                        'href': 'customers/port_out_requests.html',
                        'span': 'PortOut Requests'
                    },
                    {
                        'href': 'customers/callin.html',
                        'span': 'Inbound Calls'
                    }]
            });
        }
        if (rows[0].pages.indexOf('notifications') > -1) {
            
            var notificationItem = {
                'class': 'fa fa-bullhorn',
                'span': 'Notifications',
                'sub': []
            };

            notificationItem.sub.push({
                'href': 'notifications/notifications.html',
                'span': 'Activity Triggers'
            });

            list.push(notificationItem);

        }
        if (rows[0].pages.indexOf('customers') > -1) {
            list.push({
                'class': 'fa fa-user-md',
                'span': 'Logs',
                'sub': [
                    {
                        'href': 'logs/logs_notification_webhook.html',
                        'span': 'External Notification'
                    },
                    {
                        'href': 'logs/logs_notification_all.html',
                        'span': 'All Notifications'
                    },
                    {
                        'href': 'logs/logs_notification_email.html',
                        'span': 'Email Notifications'
                    },
                    {
                        'href': 'logs/logs_notification_sms.html',
                        'span': 'SMS Notifications'
                    },
                    {
                        'href': 'logs/logs_notification_push.html',
                        'span': 'Push Notifications'
                    },
                    {
                        'href': 'logs/logs_account_status.html',
                        'span': 'Status Change'
                    },
                    {
                        'href': 'logs/logs_account_creation.html',
                        'span': 'Account Creation'
                    },
                    {
                        'href': 'logs/logs_pay_now.html',
                        'span': 'Invoice Payments'
                    },
                    {
                        'href': 'logs/logs_payments_creditcap.html',
                        'span': 'Credit Cap Payments'
                    },
                    {
                        'href': 'logs/logs_portin.html',
                        'span': 'Port-In'
                    },
                    {
                        'href': 'logs/logs_paas.html',
                        'span': 'Paas Transactions'
                    },
                    {
                        'href': 'logs/logs_data_aggregation.html',
                        'span': 'Bonus Aggregation'
                    },
                    {
                        'href': 'logs/logs_bill_sent.html',
                        'span': 'Sent Bill'
                    },
                    {
                        'href': 'logs/logs_internal_web.html',
                        'span': 'Users Actions'
                    },
                    {
                        'href': 'logs/logs_ec_errors.html',
                        'span': 'EliteCore Errors'
                    },
                    {
                        'href': 'logs/logs_addon.html',
                        'span': 'Addon Logs'
                    },
                    {
                        'href': 'logs/logs_promo_codes.html',
                        'span': 'Promo Codes'
                    }]
            });
        }
        if (rows[0].pages.indexOf('packages') > -1) {
            list.push({
                'class': 'fa fa-th',
                'span': 'Packages',
                'sub': [
                    {
                        'href': 'packages/packages.html',
                        'span': 'Hierarchy'
                    },
                    {
                        'href': 'packages/boost_logs.html',
                        'span': 'Boost Logs'
                    },
                    {
                        'href': 'packages/addon_logs.html',
                        'span': 'Addon Logs'
                    }]
            });
        }
        if (rows[0].pages.indexOf('gentwo') > -1) {
            list.push({
                'class': 'fa fa-phone',
                'span': 'Circles Talk',
                'sub': [
                    {
                        'href': 'ctalk/admin.html',
                        'span': 'Admin'
                    },
                    {
                        'href': 'ctalk/cdrs.html',
                        'span': 'CDRs'
                    },
                    {
                        'href': 'logs/logs_gentwo_feedback.html',
                        'span': 'Feedback'
                    }]
            });
        }
        if (rows[0].pages.indexOf('bills') > -1) {
            list.push({
                'class': 'fa fa-dollar',
                'span': 'Payments',
                'sub': [
                    {
                        'href': 'bills/transactions.html',
                        'span': 'Transactions'
                    },
                    {
                        'href': 'bills/invoices.html',
                        'span': 'Invoices'
                    },
                    {
                        'href': 'bills/bills_config.html',
                        'span': 'System Configuration'
                    }]
            });
        }
        if (rows[0].pages.indexOf('contents') > -1) {
            list.push({
                'class': 'fa fa-book',
                'span': 'Content Management',
                'sub': [
                    {
                        'href': 'contents/contents.html',
                        'span': 'Terms and Conditions'
                    },
                    {
                        'href': 'contents/contents_rates.html',
                        'span': 'Rates'
                    },
                    {
                        'href': 'contents/iccids.html',
                        'span': 'ICCIDS'
                    },
                    {
                        'href': 'contents/numbers.html',
                        'span': 'Numbers'
                    },
                    {
                        'href': 'contents/contents_faq.html',
                        'span': 'FAQ'
                    },
                    {
                        //								'href'	: 'contents_blog.html',
                        'href': '404.html',
                        'span': 'Blog'
                    }]
            });
        }

        if (rows[0].pages.indexOf('marketing') > -1) {
            list.push({
                'class': 'fa fa-comment-o',
                'span': 'Marketing',
                'sub': [
                    {
                        'href': 'marketing/raffle.html',
                        'span': 'Raffle'
                    },
                    {
                        'href': 'marketing/classifier.html',
                        'span': 'Customers Classes'
                    },
                    {
                        'href': 'marketing/promotions.html',
                        'span': 'Promotions'
                    },
                    {
                        'href': 'marketing/external_customers.html',
                        'span': 'External Customers'
                    },
                    {
                        'href': 'marketing/geolocations.html',
                        'span': 'Geolocations'
                    }]
            });
        }

        if (rows[0].pages.indexOf('cdr') > -1) {
            list.push({
                'class': 'fa fa-barcode',
                'span': 'CDRs',
                'sub': [
                    {
                        'href': 'cdrs/m1cdr.html',
                        'span': 'M1 TAPs'
                    },
                    {
                        'href': 'cdrs/eccdr.html',
                        'span': 'EC CDRs'
                    },
                    {
                        'href': 'cdrs/offlineeccdr.html',
                        'span': 'Offline EC CDRs'
                    },
                    {
                        'href' : 'cdrs/scpcdr.html',
                        'span' : 'SCP CDRs'
                    },
                    {
                        'href' : 'cdrs/reassurance.html',
                        'span' : 'Revenue Assurance'
                    },
                    /** NOT BEING USED
                    {
                        'href': 'ocserror.html',
                        'span': 'OCS Error Logs'
                    }
                    **/
                    ]
            });
        }

        if (rows[0].pages.indexOf('noc') > -1) {
            list.push({
                'class': 'fa fa-terminal',
                'span': 'NOC',
                'sub': [
                    {
                        'href': 'noc/auditlogs.html',
                        'span': 'Server Audit Logs'
                    },
                    {
                        'href': 'noc/noc.html',
                        'span': 'Control Buttons'
                    }
                ]
            });
        }

        if (rows[0].pages.indexOf('bint') > -1) {
            list.push({
                'class': 'fa fa-lightbulb-o',
                'span': 'Business Intelligence',
                'sub': [{
                    'href': 'bi/bi.html',
                    'span': 'New BI'
                    }
                ]
            });
        }

        if (rows[0].pages.indexOf('admin') > -1) {
            list.push({
                'class': 'fa fa-gears',
                'span': 'Administration',
                'sub': [
                    {
                        'href': 'admin/admin.html',
                        'span': 'Access Configuration'
                    },
                    {
                        'href': 'admin/config_map.html',
                        'span': 'System Configuration'
                    },
                    {
                        'href': 'admin/banned_emails.html',
                        'span': 'Banned Emails'
                    },
                    {
                        'href': 'admin/admin_logs.html',
                        'span': 'Access Logs'
                    }]
            });
        }
        callback(undefined, list);
    });
}
