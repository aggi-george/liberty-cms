var async = require('async');
const child_process = require('child_process');
const exec = child_process.exec;
const spawn = child_process.spawn;
var config = require('../../../config');
var db = require('../../../lib/db_handler');
var ec = require('../../../lib/elitecore');
var common = require('../../../lib/common');
var BaseError = require('../../core/errors/baseError');
var gantryManager = require(__core + '/manager/system/gantryManager');
var controlManager = require('../../core/manager/system/controlManager');
const ecSyncManager = require(`${__manager}/account/ecSyncManager`);


var LOG = config.LOGSENABLED;

let stressProgress = 0;
let bonusAnomalyStatus = { undersubList: [], oversubList: [] };
let bonusAnomalyChild;

module.exports = {
    init: function(authCheck, webs, usagePath) {

        var read = { "read" : 1, "write" : 0 };
        var write = { "read" : 1, "write" : 2 };

        // noc - put
        webs.put(usagePath + '/put/:type/:id', (req, res) => {
            if (req.params.type == "gantry") write.write = 1;
            else if (req.params.type == "sync") write.write = 1;
            else if (req.params.type == "ecStress") write.write = 3;
            authCheck("noc", write, req, res, put)
        });

        // noc - get
        webs.get(usagePath + '/get/:type', (req, res) => { authCheck("noc", read, req, res, get) });
    }
}

function get(req, res, allowed) {
    var process = function(callback) {
        callback(req.query, function(error, result) {
            if (result) result.write = allowed.write;
            var httpCode = 200;
            if ( error && error.status && (parseInt(error.status) == error.status) ) {
                httpCode = error.status;
            }
            if (!res.headersSent) res.status(httpCode).json(result);
        });
    }
    if (req.params.type == "auditLogs") process(auditLogs);
    else if (req.params.type == "ecStress") process(ecStressGet);
    else if (req.params.type == "bssStatus") process(bssStatus);
    else if (req.params.type == "gantryStatus") process(gantryStatus);
    else if (req.params.type == "serviceStatus") process(serviceStatus);
    else if (req.params.type == "bypassStats") process(bypassStats);
    else if (req.params.type == "env") process(env);
    else if (req.params.type == "bonusAnomaly") process(bonusAnomaly);
    else res.status(400).json({ "code" : -1, "description" : "unknown type" });
}

function put(req, res, allowed) {
    var start = new Date();
    var process = function(callback) {
        callback(req.params, req.query, req.fields, function(error, result) {
            if (result) result.write = allowed.write;
            var httpCode = 200;
            if ( error && error.status && (parseInt(error.status) == error.status) ) {
                httpCode = error.status;
            }
            if (!res.headersSent) res.status(httpCode).json(result);
            db.weblog.insert({
                "start_ts" : start,
                "ts" : new Date().getTime(),
                "ip" : req.ip,
                "type" : "gentwo " + req.params.type,
                "id" : req.params.id,
                "action" : req.method,
                "username" : req.user,
                "code" : ( result ? result.code : -1),
                "query" : req.query,
                "fields" : req.fields,
                "files" : ((req.files && req.files.content) ? req.files.content.name : undefined)
            });
        });
    }
    if (req.params.type == "ecStress") process(ecStressPut);
    else if (req.params.type == "bss") process(bss);
    else if (req.params.type == "control") process(control);
    else if (req.params.type == "gantry") process(gantry);
    else if (req.params.type == "syncBills") process(syncBills);
    else if (req.params.type == "sync") process(sync);
    else if (req.params.type == "bonusAnomaly") process(bonusAnomalyFix);
    else res.status(400).json({ "code" : -1, "description" : "unknown type" });
}

function env(params, callback) {
    callback(undefined, { "code" : 0, "staging" : config.DEV });
}

function auditLogs(params, callback) {
    var displayLength = parseInt(params.iDisplayLength);
    var displayStart = parseInt(params.iDisplayStart);
    var sortDir = (params.sSortDir_0 == "desc") ? 1 : -1;
    var sort = { "variables.date" : sortDir };
    var year = (params.year) ? parseInt(params.year) : undefined;
    var month = (params.month) ? parseInt(params.month) : undefined;
    var day = (params.day) ? parseInt(params.day) : undefined;
    var hour_start = (params.hour_start) ? parseInt(params.hour_start) :undefined;
    var hour_end = (params.hour_end) ? parseInt(params.hour_end) :undefined;
    var minute_start = (params.minute_start) ? parseInt(params.minute_start) :undefined;
    var minute_end = (params.minute_end) ? parseInt(params.minute_end) :undefined;
    var query = new Object;
    if (year && month) {
        var low = new Date(year, month-1, day, hour_start-8, minute_start).getTime();
        var high = new Date(year, month-1, day, hour_end-8, minute_end, 59, 999).getTime();
        query = {'variables.date' : { "$gte" : low, "$lt" : high }};
    }
    db.notifications_audit_logs.find(query).sort(sort).skip(displayStart).limit(displayLength).toArray(function (err, rows) {
        if (LOG) common.log("auditLogs", query);
        if (rows && rows[0]) {
            var list = new Array;
            rows.forEach(function (row) {
                var item = new Object;
                item.date = common.getDateTime(new Date(row.variables.date + 28800000));
                item.user = row.variables.user;
                item.ptyOwner = row.variables.ptyOwner;
                item.workingDir = row.variables.workingDir;
                item.hostname = row.variables.hostname;
                item.connDetail = row.variables.connDetail;
                item.command = row.variables.command;
                list.push(item);
            });
            db.notifications_audit_logs.find(query).count(function (err, count) {
                callback(undefined, { "code" : 0, "list" : list, "total" : count });
            });
        } else {
            callback(undefined, { "code" : -1});
        }
    });
}

function ecStressGet(query, callback) {
    callback(undefined, { "code" : 0, "percent" : stressProgress });
}

function ecStressPut(params, query, fields, callback) {
    var mobile = (parseInt(fields.mobile) >= 30000000) && (parseInt(fields.mobile) <= 99999999) ? fields.mobile : undefined;
    var perbatch = (parseInt(fields.perbatch) >= 2) && (parseInt(fields.perbatch) <= 30) ? parseInt(fields.perbatch) : undefined;
    var batches = (parseInt(fields.batches) >= 2) && (parseInt(fields.batches) <= 30) ? parseInt(fields.batches) : undefined;
    if (!mobile || !perbatch || !batches) return callback(undefined, { "code" : -1 });
    var total = perbatch * batches;
    var tasks = new Array;
    var tasksPush = function (idx) {
        tasks.push(function (cb) {
            ec.getCustomerDetailsNumber("65", mobile, true, function (err, cache) {
                stressProgress = stressProgress + ( 100 / (total + 1) );
                cb(undefined, idx);
            });
        });
    }
    for (var i=0; i<perbatch*batches; i++) tasksPush(i);
    stressProgress = 100 / (total + 1);
    callback(undefined, { "code" : 0 });
    async.parallelLimit(tasks, perbatch, function (err, result) {
        stressProgress = 0;
    });
}

function bssStatus(query, callback) {
    var bsses = [ "lwbss01", "lwbss02", "lwbss03", "lwbss04" ];
    var tasks = new Array;
    bsses.forEach(function(bss) {
        tasks.push(function(cb) {
            var resObj = { "bss" : bss, "server" : bss };
            controlManager.bssControl(bss, "show", undefined, function(error, result) {
                if (!error) {
                    resObj.status = { "allow" : [], "block" : [], "bills" : { "latest" : "", "count" : 0 } };
                    var lines = result && result.result ? result.result.split('\n') : [];
                    lines.forEach(function(line) {
                        var fields = line ? line.split(',') : [];
                        if (fields[0] == "bills") {
                            resObj.status.bills.latest = fields[1];
                            resObj.status.bills.count = fields[2];
                        } else {
                            var obj = { "from" : fields[1], "port" : fields[0] };
                            if (resObj.status[fields[2]]) resObj.status[fields[2]].push(obj);
                        }
                    });
                    if (result.syncBills) resObj.syncBills = result.syncBills;
                    cb(undefined, resObj);
                } else {
                    resObj.code = error.code;
                    resObj.status = error.message;
                    cb(undefined, resObj);
                }
            });
        });
    });
    async.parallel(tasks, function(err, result) {
        callback(err, { "code" : 0, "bss" : bsses, "list" : result });
    });
}

function gantryStatus(query, callback) {
    gantryManager.get(function(error, cache) {
        callback(error, {
            "code" : 0,
            "gantryStatus" : cache,
            "gantryTypes" : gantryManager.getTypes()
        });
    });
}

function serviceStatus(query, callback) {
    var production = [ "kirk", "picard", "janeway", "worf", "wesley" ];
    var staging = [ "riker", "chekov"];
    var pushTask = function(tasks, host, type) {
        tasks.push(function(cb) {
            var resObj = { "server" : host, "processes" : [] };
            resObj[type] = host;
            controlManager.serviceControl(host, "jlist", "", function(error, result) {
                if (!error) {
                    var arr = (result && result.result) ? common.safeParse(result.result) : [];
                    if (!Array.isArray(arr)) return cb(new BaseError("Parse Error", 400));
                    arr.forEach(function(obj) {
                        var item = new Object;
                        item.pid = obj.pid;
                        item.name = obj.name;
                        item.status = obj.pm2_env.status;
                        item.uptime = obj.pm2_env.pm_uptime;
                        resObj.processes.push(item);
                    });
                    cb(undefined, resObj);
                } else {
                    resObj.code = error.code;
                    resObj.status = error.message;
                    cb(undefined, resObj);
                }
            });
        });
    }
    var tasks = new Array;
    production.forEach(function(host) { pushTask(tasks, host, "production"); });
    staging.forEach(function(host) { pushTask(tasks, host, "staging"); });
    async.parallel(tasks, function(err, result) {
        callback(err, {
            "code" : 0,
            "production" : production,
            "staging" : staging,
            "list" : result
        });
    });
}

function bypassStats(query, callback) {
    var tasks = {
        "code" : function (cb) {
            cb(undefined, 0);
        },
        "mocGxTPS" : function (cb) {
            db.cache_get("diameter", "mocGxTPS", function(cache) { cb(undefined, (cache ? Math.round(parseInt(cache)/5*100)/100 : 0)) });
        },
        "mocGyTPS" : function (cb) {
            db.cache_get("diameter", "mocGyTPS", function(cache) { cb(undefined, (cache ? Math.round(parseInt(cache)/5*100)/100 : 0)) });
        },
        "rocGxTPS" : function (cb) {
            db.cache_get("diameter", "rocGxTPS", function(cache) { cb(undefined, (cache ? Math.round(parseInt(cache)/5*100)/100 : 0)) });
        },
        "rocGyTPS" : function (cb) {
            db.cache_get("diameter", "rocGyTPS", function(cache) { cb(undefined, (cache ? Math.round(parseInt(cache)/5*100)/100 : 0)) });
        },
        "mocGxSessions" : function (cb) {
            db.cache_keys("diameter", "mocGx_*", function(keys) { cb(undefined, (keys ? keys.length : 0)) });
        },
        "mocGySessions" : function (cb) {
            db.cache_keys("diameter", "mocGy_*", function(keys) { cb(undefined, (keys ? keys.length : 0)) });
        },
        "rocGxSessions" : function (cb) {
            db.cache_keys("diameter", "rocGx_*", function(keys) { cb(undefined, (keys ? keys.length : 0)) });
        },
        "rocGySessions" : function (cb) {
            db.cache_keys("diameter", "rocGy_*", function(keys) { cb(undefined, (keys ? keys.length : 0)) });
        },
        "mocGxErrors" : function (cb) {
            db.cache_get("diameter", "mocGxErrors", function(cache) { cb(undefined, (cache ? Math.round(parseInt(cache)/3600*100)/100 : 0)) });
        },
        "mocGyErrors" : function (cb) {
            db.cache_get("diameter", "mocGyErrors", function(cache) { cb(undefined, (cache ? Math.round(parseInt(cache)/3600*100)/100 : 0)) });
        },
        "mocGxWatchDog": function (cb) {
            db.cache_get("diameter", "mocGxWatchDog", function(cache) { cb(undefined, (cache ? cache : 0)) });
        },
        "mocGxInitial": function (cb) {
            db.cache_get("diameter", "mocGxInitialRequest", function(cache) { cb(undefined, (cache ? cache : 0)) });
        },
        "mocGxUpdate": function (cb) {
            db.cache_get("diameter", "mocGxUpdateRequest", function(cache) { cb(undefined, (cache ? cache : 0)) });
        },
        "mocGxTerminate": function (cb) {
            db.cache_get("diameter", "mocGxTerminateRequest", function(cache) { cb(undefined, (cache ? cache : 0)) });
        },
        "mocGxSuspended" : function (cb) {
            db.cache_get("diameter", "mocGxSuspendedCount", function(cache) { cb(undefined, (cache ? cache : 0)) });
        },
        "mocGxCDR" : function (cb) {
            db.cache_get("diameter", "mocGxCdrCount", function(cache) { cb(undefined, (cache ? cache : 0)) });
        },
        "mocGyWatchDog": function (cb) {
            db.cache_get("diameter", "mocGyWatchDog", function(cache) { cb(undefined, (cache ? cache : 0)) });
        },
        "mocGyInitial": function (cb) {
            db.cache_get("diameter", "mocGyInitialRequest", function(cache) { cb(undefined, (cache ? cache : 0)) });
        },
        "mocGyUpdate": function (cb) {
            db.cache_get("diameter", "mocGyUpdateRequest", function(cache) { cb(undefined, (cache ? cache : 0)) });
        },
        "mocGyTerminate": function (cb) {
            db.cache_get("diameter", "mocGyTerminateRequest", function(cache) { cb(undefined, (cache ? cache : 0)) });
        },
        "mocGySuspended" : function (cb) {
            db.cache_get("diameter", "mocGySuspendedCount", function(cache) { cb(undefined, (cache ? cache : 0)) });
        },
        "mocGyCDR" : function (cb) {
            db.cache_get("diameter", "mocGyCdrCount", function(cache) { cb(undefined, (cache ? cache : 0)) });
        },
        "rocGxErrors" : function (cb) {
            db.cache_get("diameter", "rocGxErrors", function(cache) { cb(undefined, (cache ? Math.round(parseInt(cache)/3600*100)/100 : 0)) });
        },
        "rocGxInitial": function (cb) {
            db.cache_get("diameter", "rocGxInitialRequest", function(cache) { cb(undefined, (cache ? cache : 0)) });
        },
        "rocGxUpdate": function (cb) {
            db.cache_get("diameter", "rocGxUpdateRequest", function(cache) { cb(undefined, (cache ? cache : 0)) });
        },
        "rocGxTerminate": function (cb) {
            db.cache_get("diameter", "rocGxTerminateRequest", function(cache) { cb(undefined, (cache ? cache : 0)) });
        },
        "rocGxSuspended" : function (cb) {
            db.cache_get("diameter", "rocGxSuspendedCount", function(cache) { cb(undefined, (cache ? cache : 0)) });
        },
        "rocGxCDR" : function (cb) {
            db.cache_get("diameter", "rocGxCdrCount", function(cache) { cb(undefined, (cache ? cache : 0)) });
        },
        "rocGyErrors" : function (cb) {
            db.cache_get("diameter", "rocGyErrors", function(cache) { cb(undefined, (cache ? Math.round(parseInt(cache)/3600*100)/100 : 0)) });
        },
        "rocGyInitial": function (cb) {
            db.cache_get("diameter", "rocGyInitialRequest", function(cache) { cb(undefined, (cache ? cache : 0)) });
        },
        "rocGyUpdate": function (cb) {
            db.cache_get("diameter", "rocGyUpdateRequest", function(cache) { cb(undefined, (cache ? cache : 0)) });
        },
        "rocGyTerminate": function (cb) {
            db.cache_get("diameter", "rocGyTerminateRequest", function(cache) { cb(undefined, (cache ? cache : 0)) });
        },
        "rocGySuspended" : function (cb) {
            db.cache_get("diameter", "rocGySuspendedCount", function(cache) { cb(undefined, (cache ? cache : 0)) });
        },
        "rocGyCDR" : function (cb) {
            db.cache_get("diameter", "rocGyCdrCount", function(cache) { cb(undefined, (cache ? cache : 0)) });
        },
        "rocGxWatchDog": function (cb) {
            db.cache_get("diameter", "rocGxWatchDog", function(cache) { cb(undefined, (cache ? cache : 0)) });
        },
        "rocGyWatchDog": function (cb) {
            db.cache_get("diameter", "rocGyWatchDog", function(cache) { cb(undefined, (cache ? cache : 0)) });
        }
    }
    async.parallel(tasks, function(e,r) {
        callback(e, r);
    });
}

function bss(params, query, fields, callback) {
    var action = fields.action;
    var server = fields.server;
    var args = fields.args;
    controlManager.bssControl(server, action, args, function(error, result) {
        var reply = { "code" : (error ? -1 : 0) };
        reply.error = error;
        reply.server = server;
        reply.result = result.result;
        callback(undefined, result);
    });
}

function control(params, query, fields, callback) {
    var action = fields.action;
    var server = fields.server;
    var args = fields.args;
    controlManager.serviceControl(server, action, args, function(error, result) {
        var reply = { "code" : (error ? -1 : 0) };
        reply.error = error;
        reply.server = server;
        reply.result = result.result;
        callback(undefined, reply);
    });
}

function gantry(params, query, fields, callback) {
    var action = fields.action;
    var server = fields.server;
    var args = fields.args;
    var throttle = fields.throttle;
    var allowedActions = gantryManager.allowedActions;
    if (allowedActions.indexOf(action) == -1) {
        return callback(new BaseError("Invalid Action", 400));
    }
    if ( !throttle && (action == "throttle") ) {
        return callback(undefined, { "code" : -1, "status" : "Empty Throttle Value" });
    }
    if ( !args && (action != "allow") ) {
        return callback(undefined, { "code" : -1, "status" : "Empty Reason" });
    }
    if (action == "block" || action == "throttle") {
        var timeout = 30 * 60;    // 30 minutes
        gantryManager.block(args, parseFloat(throttle), timeout, function(error, result) {
            callback(undefined, { "code" : 0 });
        });
    } else {
        gantryManager[action](function(error, result) {
            callback(undefined, { "code" : 0 });
        });
    }
}

function syncBills(params, query, fields, callback) {
    var server = fields ? fields.server : undefined;
    if (!server) return callback(400, { "code" : -1, "status" : "Server Empty" });
    var d = new Date();
    var billDate = fields && fields.check ? undefined : common.getDate(new Date(d.getFullYear(), d.getMonth(), 1));
    controlManager.bssControl(server, "syncBills", billDate, function(error, result) {
        if (error) return callback(undefined, { "code" : -1, "status" : error.message });
        var reply = { "code" : (error ? -1 : 0) };
        reply.error = error;
        reply.server = server;
        reply.result = result;
        callback(undefined, reply);
    });
}

function sync(params, query, fields, callback) {
    var id = params.id;
    var type = fields.type;
    ecSyncManager.elitecoreCustomerDetails((err, result) => {
        callback(err, { "code" : 0, "result" : result });
    });
}

function bonusAnomaly(params, callback) {
    if (params.statusCheck) return callback(undefined, { code: 0, status: bonusAnomalyStatus });
    if (bonusAnomalyChild) bonusAnomalyChild.kill();
    bonusAnomalyStatus = { undersubList: [], oversubList: [] };
    let argsArr = [];
    if (parseInt(params.fix)) argsArr.push(1); else argsArr.push(0);
    if (parseInt(params.future)) argsArr.push(1); else argsArr.push(0);
    bonusAnomalyChild = spawn('./scripts/internal/bonus_anomaly.js', argsArr);
    bonusAnomalyStatus.fix = parseInt(params.fix);
    bonusAnomalyStatus.future = parseInt(params.future);
    bonusAnomalyStatus.progress = 0;
    callback(undefined, { code: 0 });
    const outList = [ 'bucket', 'history', 'zero', 'unattached', 'oversub', 'undersub', 'time', 'undersubItem', 'oversubItem' ];
    bonusAnomalyChild.stdout.on("data", function (data) {
        const lines = data.toString().split(/(\r?\n)/g);
        lines.forEach(function (line) {
            const parsed = line.split('|');
            if (parsed[0] == 'progress') bonusAnomalyStatus.progress = parseInt(parsed[1]);
            else if (parsed[0] == 'undersubItem') bonusAnomalyStatus.undersubList.push(common.safeParse(parsed[1]));
            else if (parsed[0] == 'oversubItem') bonusAnomalyStatus.oversubList.push(common.safeParse(parsed[1]));
            else if (outList.indexOf(parsed[0]) > -1) bonusAnomalyStatus[parsed[0]] = parsed[1];
        });
    });
}

function bonusAnomalyFix(params, query, fields, callback) {
    params.fix = 1;
    bonusAnomaly(params, callback);
}
