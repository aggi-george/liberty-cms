var request = require('request');
var fs = require('fs');
var mime = require('mime');
var aws = require('aws-sdk');
var config = require('../../../config');
var common = require('../../../lib/common');
var db = require('../../../lib/db_handler');
var notificationSend = require('../../core/manager/notifications/send');
var web = require('./web');
var BaseError = require('../../core/errors/baseError');
var config = require('../../../config');

var logsEnabled = config.LOGSENABLED;

aws.config.update({ region : "ap-southeast-1", accessKeyId : config.AWS_KEY, secretAccessKey : config.AWS_SECRET });
var s3 = new aws.S3();

//exports

exports.getAll = getAll;
exports.getNotification = getNotification;
exports.sendNotification = sendNotification;
exports.updateNotification = updateNotification;
exports.updateTeam = updateTeam;
exports.updateGroup = updateGroup;
exports.updateTransport = updateTransport;
exports.addNew = addNew;
exports.deleteNotification = deleteNotification;
exports.getNotificationsLogs = getNotificationsLogs;
exports.uploadAsset = uploadAsset;
exports.deleteAsset = deleteAsset;
exports.uploadTemplate = uploadTemplate;
exports.deleteLog = deleteLog;
exports.updateRestriction = updateRestriction;

//functions

function getAll(req, res) {
	web.checkPermission("notifications", req.gid, function(allowed) {
		if (!allowed) {
			res.status(403).json({ "code" : -1 });
		} else {
			var q = "SELECT activity FROM notifications";
			db.query_noerr(q, function(rows) {
				if (rows && rows[0]) {
					var list = new Array;
					rows.forEach(function (row) {
						var item = new Object;
						item.activity = row.activity;
						list.push(item);
					});
					res.json({ "code" : 0, "list" : list, "write" : allowed.write });
				} else {
					res.json({ "code" : -1});
				}
			});
		}
	});
}

function sendNotification(req, res) {
	web.checkPermission("notifications", req.gid, function (allowed) {
		if (!allowed || !allowed.write) {
			res.status(403).json({"code": -1});
		} else {
			var name = req.fields.name;
			var number = req.fields.number;
			var email = req.fields.email;
			var type = req.fields.type;
			var activity = req.params.activity;
			var variables = JSON.parse(JSON.stringify(req.fields));
			delete variables.name;
			delete variables.email;
			delete variables.number;
			var send = function(cb) {
				var ecVar = JSON.parse(JSON.stringify(req.fields));
				ecVar.name = name;
				ecVar.number = number;
				ecVar.email = email;
				ecVar.activity = activity;
				if (typeof(ecVar.message) == 'undefined') ecVar.message = "TEST NOTIFICATION";
				delete ecVar.type;
				if (type && type == "elitecore") {
					notificationSend.elitecore(ecVar, cb);
				} else {
					notificationSend.internal(prefix, number, email, name, activity, variables, notificationKey, cb);
				}
                        }
			var prefix;
			var notificationKey;
			send(function(err, result) {
				var code = err ? -1 : 0;
				db.weblog.insert({
					"ts": new Date().getTime(),
					"ip": req.ip,
					"type": "manualNotificationTriggered",
					"id": common.escapeHtml(req.fields.number),
					"action": "manual notification triggered via riker",
					"username": req.user,
					"result": err ? "error occoured" : "success",
					"code": 0
				});
				res.json({
					"code": code,
					"number": req.params.number,
					"prefix": req.params.prefix,
					"write": allowed.write,
					"errorMessage": err,
					"body": result
				});
			});
		}
	});
}

function getAssets(id, callback) {
	var params = { "Bucket" : config.AWS_BUCKET,
			"Prefix" : "assets/" + id + "/",
			"Delimiter" : "/" };
	var count = 0;
	s3.listObjects(params, function(err, list) {
		if ( list && list.Contents && (list.Contents.length > 0) ) {
			var assets = new Array;
			list.Contents.forEach(function (item) {
				s3.headObject({ "Bucket" : config.AWS_BUCKET,
						"Key" : item.Key }, function (err, result) {
					count++;
					if (result && result.Metadata && result.Metadata.mime) {
						assets.push({ "link" : config.IMAGEHOST + "/" + item.Key,
								"mime" : result.Metadata.mime });
					}
					if ( count == list.Contents.length ) callback(assets);
				});
			});
		} else {
			callback([]);
		}
	});
}

function getNotification(req, res) {
	web.checkPermission("notifications", req.gid, function(allowed) {
		if (!allowed) {
			res.status(403).json({ "code" : -1 });
		} else {
			var activity = req.params.activity;
			var q = "SELECT s.id, s.name, SUM(s.value) value FROM (SELECT DISTINCT r.id, r.name, IF(n.activity=" + db.escape(activity) + ",nr.value,0) value FROM restrictions r LEFT JOIN notificationsRestrictions nr ON r.id=nr.restrictionID LEFT JOIN notifications n ON n.id=nr.notificationID) s GROUP BY s.id";
			db.query_noerr(q, function(rows) {
				var restrictions = [];
				if (rows && rows[0]) {
					restrictions = rows;
				}
				var q = "SELECT s.id, s.name, SUM(s.allowed) allowed FROM (SELECT DISTINCT t.id, t.name, IF(n.activity=" + db.escape(activity) + ",1,0) allowed FROM teams t LEFT JOIN teamsNotifications tn ON t.id=tn.teamID LEFT JOIN notifications n ON n.id=tn.notificationID) s GROUP BY s.id;";
				db.query_noerr(q, function(rows) {
					if (rows && rows[0]) {
						var teams = rows;
						var q = "SELECT s.id, s.name, SUM(s.enabled) enabled FROM (SELECT DISTINCT t.id, t.name, IF(n.activity=" + db.escape(activity) + ",1,0) enabled FROM transports t LEFT JOIN notificationsTransports nt ON t.id=nt.transportID LEFT JOIN notifications n ON n.id=nt.notificationID) s GROUP BY s.id ORDER BY s.name ASC";
						db.query_noerr(q, function(rows) {
							if (rows && rows[0]) {
								var transports = rows;
                                var q = "SELECT ng.*, IF(n.activity IS NULL, 0, 1) selected FROM notificationsGroup ng LEFT JOIN notifications n ON (ng.id=n.groupId && n.activity=" + db.escape(activity) + ") GROUP BY id";
                                db.query_noerr(q, function(rows) {
                                    if (rows && rows[0]) {
                                        var groups = rows;
                                        var q = "SELECT subject, plain, html, app, email_internal_1, subject_internal_1, html_internal_1 FROM notifications WHERE activity=" + db.escape(activity);
                                        db.query_noerr(q, function(rows) {
                                            var reply = {
                                                "code": 0,
                                                "result": {
                                                    "activity": activity,
                                                    "teams": teams,
                                                    "transports": transports,
                                                    "restrictions": restrictions,
                                                    "groups": groups
                                                },
                                                "write": allowed.write
                                            };
                                            if ( rows && rows[0] ) {
                                                reply.result.subject = rows[0].subject;
                                                reply.result.plain = rows[0].plain;
                                                reply.result.html = rows[0].html;
                                                reply.result.app = rows[0].app;
                                                reply.result.emailInternal1 = rows[0].email_internal_1;
                                                reply.result.subjectInternal1 = rows[0].subject_internal_1;
                                                reply.result.htmlInternal1 = rows[0].html_internal_1;
                                            } else {
                                                reply.result.subject = "";
                                                reply.result.plain = "";
                                                reply.result.html = "";
                                                reply.result.app = "";
                                                reply.result.emailInternal1 = "";
                                                reply.result.subjectInternal1 = "";
                                                reply.result.htmlInternal1 = "";
                                            }
                                            getAssets(activity, function (assets) {
                                                reply.result.assets = assets;
                                                res.json(reply);
                                            });
                                        });
                                    } else {
                                        res.json({ "code" : -1});
                                    }
                                });
                            } else {
                                res.json({ "code" : -1});
                            }
                        })
					} else {
						res.json({ "code" : -1});
					}
				});
			});
		}
	});
}

function updateTeam(req, res) {
	web.checkPermission("notifications", req.gid, function(allowed) {
		if (!allowed || !allowed.write) {
			res.status(403).json({ "code" : -1 });
		} else {
			var activity = req.params.activity;
			var onID = "";
			var offID = "";
			Object.keys(req.fields).forEach(function (key) {
				if ( req.fields[key] == "on" ) {
					onID = db.escape(key.substring(5));	// remove "team_"
				} else {
					offID = db.escape(key.substring(5));	// remove "team_"
				}
			});
			if (onID) {
				var q = "INSERT INTO teamsNotifications (notificationID,teamID) SELECT id," + onID + "	FROM notifications WHERE activity=" + db.escape(activity);
				if (logsEnabled) common.log("notifications updateTeam", q);
				db.query_noerr(q, function (rows) { });
				db.weblog.insert({	"ts" : new Date().getTime(),
							"ip" : req.ip,
							"type" : "updateTeamNotification",
							"id" : common.escapeHtml(activity),
							"action" : "INSERT " + common.escapeHtml(onID),
							"username" : req.user,
							"code" : 0
						});
			} else if (offID) {
				var q = "DELETE FROM teamsNotifications WHERE teamID=" + offID + " AND notificationID=(SELECT id FROM notifications WHERE activity=" + db.escape(activity) + ")";
				if (logsEnabled) common.log("notifications updateTeam", q);
				db.query_noerr(q, function (rows) { });
				db.weblog.insert({	"ts" : new Date().getTime(),
							"ip" : req.ip,
							"type" : "updateTeamNotification",
							"id" : common.escapeHtml(activity),
							"action" : "DELETE " + common.escapeHtml(offID),
							"username" : req.user,
							"code" : 0
						});
			}
			res.json({ "code" : 0, "activity" : activity, "write" : allowed.write });
		}
	});
}

function updateGroup(req, res) {
    web.checkPermission("notifications", req.gid, function (allowed) {
        if (!allowed || !allowed.write) {
            res.status(403).json({"code": -1});
        } else {
            var activity = req.params.activity;
            var groupId = req.fields.groupId ? parseInt(req.fields.groupId) : undefined;
            var q = "UPDATE notifications SET groupId=" + (groupId && groupId > 0 ? db.escape(groupId) : "NULL") +
                " WHERE activity=" + db.escape(activity);
            db.query_noerr(q, function (rows) {
            });
            db.weblog.insert({
                "ts": new Date().getTime(),
                "ip": req.ip,
                "type": "updateGroupNotification",
                "id": common.escapeHtml(activity),
                "action": "UPDATE",
                "username": req.user,
                "code": 0
            });
            res.json({"code": 0, "activity": activity, "write": allowed.write});
        }
    });
}

function updateTransport(req, res) {
	web.checkPermission("notifications", req.gid, function(allowed) {
		if (!allowed || !allowed.write) {
			res.status(403).json({ "code" : -1 });
		} else {
			var activity = req.params.activity;
			var onID = "";
			var offID = "";
			Object.keys(req.fields).forEach(function (key) {
				if ( req.fields[key] == "on" ) {
					onID = db.escape(key.substring(10));	 // remove "transport_"
				} else {
					offID = db.escape(key.substring(10));	 // remove "transport_"
				}
			});
			if (onID) {
				var q = "INSERT INTO notificationsTransports (notificationID,transportID) SELECT id," + onID + "  FROM notifications WHERE activity=" + db.escape(activity);
				if (logsEnabled) common.log("notifications updateTransport", q);
				db.query_noerr(q, function (rows) { });
				db.weblog.insert({	"ts" : new Date().getTime(),
							"ip" : req.ip,
							"type" : "updateTransport",
							"id" : common.escapeHtml(activity),
							"action" : "INSERT " + common.escapeHtml(onID),
							"username" : req.user,
							"code" : 0
						});
			} else if (offID) {
				var q = "DELETE FROM notificationsTransports WHERE transportID=" + offID + " AND notificationID=(SELECT id FROM notifications WHERE activity=" + db.escape(activity) + ")";
				if (logsEnabled) common.log("notifications updateTransport", q);
				db.query_noerr(q, function (rows) { });
				db.weblog.insert({	"ts" : new Date().getTime(),
							"ip" : req.ip,
							"type" : "updateTransport",
							"id" : common.escapeHtml(activity),
							"action" : "DELETE " + common.escapeHtml(offID),
							"username" : req.user,
							"code" : 0
						});
			}
			res.json({ "code" : 0, "activity" : activity, "write" : allowed.write });
		}
	});
}

function updateRestriction(req, res) {
	web.checkPermission("notifications", req.gid, function(allowed) {
		if (!allowed || !allowed.write) {
			return res.status(403).json({ "code" : -1 });
		} else {
			var activity = req.params.activity;
			var restrictionId;
			var restrictionValue;
			Object.keys(req.fields).forEach(function (key) {
				restrictionId = key;
				restrictionValue = req.fields[key];
			});
			updateRestrictionsInternal(activity, restrictionId, restrictionValue, function(err){
				if(err){
					return res.status(400).json({
						"code": -1,
						"status": -1,
						"error": err.message
					});
				}
				res.json({ "code" : 0, "activity" : activity, "write" : allowed.write });
			})
		}
	});
}

function updateRestrictionsInternal(activity, restrictionId, restrictionValue, callback){
	if(isNaN(restrictionValue)){
		return callback(new BaseError("Invalid restriction value", "RESTRICTION_VALUE_INVALID"));
	}else{
		var q = "SELECT id from notifications where activity = " + db.escape(activity);
		db.query_err(q, function(err, rows){
			if(err || !rows[0] || !rows[0].id){
				return callback(new BaseError("Notification id found", "NOTIFICATION_ID_NOT_FOUND"));
			}
			var notificationId = rows[0].id;
			if (restrictionValue.length > 0){
				var q = "DELETE FROM notificationsRestrictions WHERE notificationID = " + db.escape(notificationId) + " AND restrictionID = " +  db.escape(restrictionId);
				db.query_err(q, function(err, result){
					if(err){
						return callback(new BaseError("Error occoured when removing old restriction value", "FAILED_REMOVEING_OLD_RES_VAL"));
					}else{
						var q = "INSERT INTO notificationsRestrictions (notificationID, restrictionID, value) VALUES (" + db.escape(notificationId) + ", " + db.escape(restrictionId) + ", " + db.escape(restrictionValue) + ")";
						db.query_err(q, function(err, result){
							if(err){
								return callback(new BaseError("Error occoured when inserting new restriction value", "FAILED_INSERT_NEW_RES_VAL"));
							}
							return callback();
						})
					}
				});
			}else{
				var q = "DELETE FROM notificationsRestrictions WHERE notificationID = '" + notificationId + "' AND restrictionID = '" + restrictionId + "'";
				db.query_err(q, function(err, rows){
					if(err){
						return callback(new BaseError("Error occoured clearing restriction", "FAILED_INSERT_NEW_RES_VAL"));
					}
					return callback();
				});
			}
		});
	}
}

function updateNotification(req, res) {
	web.checkPermission("notifications", req.gid, function(allowed) {
		if (!allowed || !allowed.write) {
			res.status(403).json({ "code" : -1 });
		} else {
			var activity = req.params.activity;
			var subject = (req.fields.subject && req.fields.subject.replace(/ /g,"") == "") ? "" : req.fields.subject;		// prevent saving all spaces as input
			var plain = (req.fields.plain && req.fields.plain.replace(/ /g,"") == "") ? "" : req.fields.plain;
			var html = (req.fields.html && req.fields.html.replace(/ /g,"") == "") ? "" : req.fields.html;
			var app = (req.fields.app && req.fields.app.replace(/ /g,"") == "") ? "" : req.fields.app;
			var email_internal_1 = (req.fields.email_internal_1 && req.fields.email_internal_1.replace(/ /g,"") == "") ? "" : req.fields.email_internal_1;		// prevent saving all spaces as input
			var subject_internal_1 = (req.fields.subject_internal_1 && req.fields.subject_internal_1.replace(/ /g,"") == "") ? "" : req.fields.subject_internal_1;		// prevent saving all spaces as input
			var html_internal_1 = (req.fields.html_internal_1 && req.fields.html_internal_1.replace(/ /g,"") == "") ? "" : req.fields.html_internal_1;
			var columns = "";
			var values = "";
			var update = "";
			if (typeof subject !== 'undefined'){
				columns = "subject,";
				values = db.escape(subject) + ",";
				update = "subject=VALUES(subject),";
			}
			else if (typeof plain !== 'undefined'){
				columns = "plain,";
				values = db.escape(plain) + ",";
				update = "plain=VALUES(plain),";
			}
			else if (typeof html !== 'undefined'){
				columns = "html,";
				values = db.escape(html) + ",";
				update = "html=VALUES(html),";
			}
			else if (typeof app !== 'undefined'){
				columns = "app,";
				values = db.escape(app) + ",";
				update = "app=VALUES(app),";
			}
			else if (typeof email_internal_1 !== 'undefined'){
				columns = "email_internal_1,";
				values = db.escape(email_internal_1) + ",";
				update = "email_internal_1=VALUES(email_internal_1),";
			}
			else if (typeof subject_internal_1 !== 'undefined'){
				columns = "subject_internal_1,";
				values = db.escape(subject_internal_1) + ",";
				update = "subject_internal_1=VALUES(subject_internal_1),";
			}
			else if (typeof html_internal_1 !== 'undefined'){
				columns = "html_internal_1,";
				values = db.escape(html_internal_1) + ",";
				update = "html_internal_1=VALUES(html_internal_1),";
			}
			var q = "INSERT INTO notifications (activity," + columns.slice(0,-1) + ") VALUES (" + db.escape(activity) + "," + values.slice(0,-1) + ") ON DUPLICATE KEY UPDATE " + update.slice(0,-1);
			if (logsEnabled) common.log("updateNotification", q);
			db.query_noerr(q, function(rows) {
				res.json({ "code" : 0, "activity" : activity, "write" : allowed.write });
				db.weblog.insert({	"ts" : new Date().getTime(),
							"ip" : req.ip,
							"type" : "updateNotification",
							"id" : common.escapeHtml(activity),
							"action" : columns.slice(0,-1).replace(/['"{}]/g,'') + " VALUES " + values.slice(0,-1).replace(/['"{}]/g,''),	// avoid injection
							"username" : req.user,
							"code" : 0
						});

			});
		}
	});
}

function addNew(req, res) {
	web.checkPermission("notifications", req.gid, function(allowed) {
		if (!allowed || !allowed.write) {
			res.status(403).json({ "code" : -1 });
		} else {
			var activity = req.params.activity;
			var q = "INSERT IGNORE INTO notifications (activity) VALUES (" + db.escape(activity) + ")";
			db.query_noerr(q, function(rows) {
				res.json({ "code" : 0, "activity" : activity, "write" : allowed.write });
				db.weblog.insert({	"ts" : new Date().getTime(),
							"ip" : req.ip,
							"type" : "addNewNotification",
							"id" : common.escapeHtml(activity),
							"action" : "INSERT",
							"username" : req.user,
							"code" : 0
						});
			});
		}
	});
}

function deleteNotification(req, res) {
	web.checkPermission("notifications", req.gid, function(allowed) {
		if (!allowed || !allowed.write) {
			res.status(403).json({ "code" : -1 });
		} else {
			var activity = req.params.activity;
			var q = "DELETE FROM notifications WHERE activity=" + db.escape(activity);
			if (logsEnabled) common.log("notifications deleteNotification", q);
			db.query_noerr(q, function(rows) {
				res.json({ "code" : 0, "write" : allowed.write });
				db.weblog.insert({	"ts" : new Date().getTime(),
							"ip" : req.ip,
							"type" : "deleteNotification",
							"id" : common.escapeHtml(activity),
							"action" : "DELETE",
							"username" : req.user,
							"code" : 0
						});
			});
		}
	});
}

function getNotificationsLogs(req, res) {
	web.checkPermission("notifications", req.gid, function(allowed) {
		if (!allowed) {
			res.status(403).json({ "code" : -1 });
		} else {
			var today = new Date();
			// default 3 days
			var match = { "ts" : { "$gt" : new Date(today.getFullYear(),
								today.getMonth(),
								today.getDate()-3).getTime() } };
			var search = common.escapeHtml(req.query.search);
			if (req.query.search) {
				if ( search.length > 64 ) search = search.slice(0, 64);	// max 64 chars
				// if with search params, 3 months
				match.ts = { "$gt" : new Date(today.getFullYear(),
								today.getMonth()-3,
								today.getDate()).getTime() };
				match["$or"] = [
						{ "email" : new RegExp(search,"i") },
						{ "to" : new RegExp(search,"i") },
						{ "number" : new RegExp(search,"i") },
						{ "activity" : new RegExp(search,"i") },
						{ "status" : new RegExp(search,"i") },
					];
			}

			var logsContainer;
			if (req.query.type === "webhook") {
				logsContainer = db.notifications_logbookwebhook_get;
			} else if (req.query.type === "email") {
				logsContainer = db.notifications_logbookemails_get;
			} else if (req.query.type === "sms") {
				logsContainer = db.notifications_logbooksms_get;
			} else {
				logsContainer = db.notifications_logbook_get;
			}
			logsContainer(match, {}, {"_id" : -1}, function(item) { // 3 days
				if ( item ) res.json({ "code" : 0, "list" : item, "write" : allowed.write });
				else res.status(404).send('Not Found');
			});
		}
	});
}

function uploadAsset(req, res) {
	web.checkPermission("notifications", req.gid, function(allowed) {
		if (!allowed || !allowed.write) {
			res.status(403).json({ "code" : -1 });
		} else {
			var activity = req.params.activity;
			var path = req.files.content.path;
			var name = req.files.content.name;
			if (!path || !name || !activity) return res.json({ "code" : -1, "activity" : activity, "write" : allowed.write });
			var params = { "Bucket" : config.AWS_BUCKET,
					"Key" : "assets/" + activity + "/" + name,
					"Body" : fs.readFileSync(path) };
			params.Metadata = { "mime" : mime.lookup(path) };
			s3.upload(params, function(err, result) {
				res.json({ "code" : 0, "activity" : activity, "write" : allowed.write });
				db.weblog.insert({ "ts" : new Date().getTime(),
						"ip" : req.ip,
						"type" : "uploadAssetNotification",
						"id" : common.escapeHtml(activity),
						"action" : common.escapeHtml(name),
						"username" : req.user,
						"code" : 0
					});
				fs.stat(path, function(err,stats){
					if(!err){
						fs.unlink(path);
					}
				});
			});
		}
	});
}

function deleteAsset(req, res) {
	web.checkPermission("notifications", req.gid, function(allowed) {
		if (!allowed || !allowed.write) {
			res.status(403).json({ "code" : -1 });
		} else {
			var activity = req.params.activity;
			var files = req.fields.delete.split(",");
			var params = { "Bucket" : config.AWS_BUCKET };
			files.forEach(function (file) {
				params.Key = "assets/" + activity + "/" + file;
				s3.deleteObject(params, function (err, result) { });
			});
			res.json({ "code" : 0, "activity" : activity, "write" : allowed.write });
			db.weblog.insert({	"ts" : new Date().getTime(),
						"ip" : req.ip,
						"type" : "deleteAssetNotification",
						"id" : common.escapeHtml(activity),
						"action" : common.escapeHtml(files),
						"username" : req.user,
						"code" : 0
					});
		}
	});
}

function uploadTemplate(req, res) {
	web.checkPermission("notifications", req.gid, function(allowed) {
		if (!allowed || !allowed.write) {
			res.status(403).json({ "code" : -1 });
		} else {
			var path = req.files.content.path;
			var name = req.files.content.name;
			common.mv(path, "./res/notification/" + name);
			res.json({ "code" : 0, "write" : allowed.write });
			db.weblog.insert({ "ts" : new Date().getTime(),
					"ip" : req.ip,
					"type" : "uploadTemplate",
					"id" : common.escapeHtml(name),
					"action" : common.escapeHtml(name),
					"username" : req.user,
					"code" : 0
				});
			fs.stat(path, function(err,stats){
				if(!err){
					fs.unlink(path);
				}
			});
		}
	});
}

function deleteLog(req, res) {
	web.checkPermission("notifications", req.gid, function(allowed) {
		if (!allowed || !allowed.write) {
			res.status(403).json({ "code" : -1 });
		} else {
			var id = req.params.id;
			db.notifications_logbook.remove({ "_id" : db.objectID(id) }, function (err, result) {
				res.json( { "code" : 0 });
			});
		}
	});
}
