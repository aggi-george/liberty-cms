var logsManager = require('../../core/manager/log/logsManager');
var common = require('../../../lib/common');
var db = require('../../../lib/db_handler');
var web = require('./web');
var async = require('async');

//exports

exports.getNumbers = getNumbers;
exports.putNumbers = putNumbers;
exports.deleteNumber = deleteNumber;

//functions

function getNumbers(req, res) {
    web.checkPermission("customers", req.gid, function (allowed) {
        if (!allowed) {
            res.status(403).json({"code": -1});
        } else {
            db.numberPool.find({}).toArray(function(err, result){
                if(err){
                    res.json({code: -1, error: err});
                }else{
                    res.json({code: 0, list: result});
                }
            });
        }
    });
}


function putNumbers(req, res){
    var type = req.fields.type;
    var list = req.fields.list.match(/[0-9]{8}/g);
    var finalResult = [];
    async.each(list, function(item, callback){
        db.numberPool.findOne({_id: item}, function(err, result){
            if(err){
                finalResult.push({number: item, status: "error occoured when searching for existing ids"});
                callback(null);
            }else if(result && result._id == item){
                finalResult.push({number: item, status: "Number already in database"});
                callback(null);
            }else{
                db.numberPool.insertOne({_id: item, "type": type}, function(err, result){
                    if(err){
                        finalResult.push({number: item, status: "Failed to enter new number to database"});
                        callback(null);
                    }else{
                        finalResult.push({number: item, status: "Successfully enter number"});
                        db.weblog.insert({
                            "ts" : new Date().getTime(),
                            "ip" : req.ip,
                            "type" : "testingNumberPool",
                            "action" : "testingNumberAdded",
                            "username" : req.user,
                            "numberAdded": item
                        });
                        callback(null);
                    }
                });
            }
        });
    }, function(error){
        res.json({code: 0, list: finalResult});
    })
}

function deleteNumber(req, res){
    var number = req.fields.number;
    db.numberPool.remove({_id: number}, function(err, result){
        if(err){
            res.json({code: -1, error: err});
        }else{
            res.json({code: 0});
        }
    });
}
