var fs = require('fs');
var mime = require('mime');
var aws = require('aws-sdk');
var async = require('async');
var config = require('../../../config');
var common = require('../../../lib/common');
var db = require('../../../lib/db_handler');
var ec = require('../../../lib/elitecore');
var web = require('./web');

var logsEnabled = config.LOGSENABLED;

aws.config.update({ region : "ap-southeast-1", accessKeyId : config.AWS_KEY, secretAccessKey : config.AWS_SECRET });
var s3 = new aws.S3();

//exports

exports.businessHierarchy = businessHierarchy;
exports.getPackage = getPackage;
exports.updatePackage = updatePackage;
exports.updateTeam = updateTeam;
exports.uploadAsset = uploadAsset;
exports.deleteAsset = deleteAsset;
exports.getBoostsLogs = getBoostsLogs;
exports.getAddonLogs = getAddonLogs;

//functions

function businessHierarchy(req, res) {
	web.checkPermission("packages", req.gid, function(allowed) {
		if (!allowed) {
			res.status(403).json({ "code" : -1 });
		} else {
			res.json({ "code" : 0, "result" : ec.packages(), "write" : allowed.write});
		}
	});
}

function getAssets(id, callback) {
	var params = { "Bucket" : config.AWS_BUCKET,
			"Prefix" : "assets/" + id + "/",
			"Delimiter" : "/" };
	var count = 0;
	s3.listObjects(params, function(err, list) {
		if ( list && list.Contents && (list.Contents.length > 0) ) {
			var assets = new Array;
			list.Contents.forEach(function (item) {
				s3.headObject({ "Bucket" : config.AWS_BUCKET,
						"Key" : item.Key }, function (err, result) {
					count++;
					if (result && result.Metadata && result.Metadata.mime) {
						assets.push({ "link" : config.IMAGEHOST + "/" + item.Key,
								"mime" : result.Metadata.mime });
					}
					if ( count == list.Contents.length ) callback(assets);
				});
			});
		} else {
			callback([]);
		}
	});
}

function getPackage(req, res) {
	web.checkPermission("packages", req.gid, function(allowed) {
		if (!allowed) {
			res.status(403).json({ "code" : -1 });
		} else {
			var id = req.params.id;
			var result = { "cache" : ec.findPackage(ec.packages(), id) };	// deep copy
			var q = "SELECT * FROM packages WHERE product_id = " + db.escape(id);
			db.query_noerr(q, function(rows) {
				if (rows && rows[0]) {
					result.title = rows[0].title;
					result.app = rows[0].app;
					result.subtitle = rows[0].subtitle;
					result.description_short = rows[0].description_short;
					result.description_full = rows[0].description_full;
					result.disclaimer = rows[0].disclaimer;
					result.advanced_payment = rows[0].advanced_payment;
					result.free_package = rows[0].free_package ? rows[0].free_package : "";
					result.beta = rows[0].beta;
					result.beta_group = rows[0].beta_group;
					result.sub_effect = rows[0].sub_effect;
					result.unsub_effect = rows[0].unsub_effect;
					result.action = rows[0].action;
					result.order_id = rows[0].order_id;
					if ( rows[0].enabled == 0 ) {
						var q = "UPDATE packages SET enabled=1 WHERE product_id = " + db.escape(id);
						db.query_noerr(q, function (rows) { });
					}
				} else {
					result.title = "";
					result.app = "";
					result.subtitle = "";
					result.description_short = "";
					result.description_full = "";
					result.disclaimer = "";
					result.advanced_payment = 0;
					result.free_package = "";
					result.beta = 0;
					result.beta_group = "";
					result.sub_effect = 0;
					result.unsub_effect = 0;
					result.action = 0;
					result.order_id = 0;
					var q = "INSERT INTO packages (product_id) VALUES (" + db.escape(id) + ")";
					db.query_noerr(q, function (rows) { });
				}
				delete result.cache.title;
				delete result.cache.app;
				delete result.cache.subtitle;
				delete result.cache.description_short;
				delete result.cache.description_full;
				delete result.cache.disclaimer;
				delete result.cache.sub_effect;
				delete result.cache.unsub_effect;
				delete result.cache.action;
				delete result.cache.order_id;
				delete result.cache.enabled;
				delete result.cache.advanced_payment;
				delete result.cache.free_package;
				delete result.cache.beta;
				delete result.cache.beta_group;
				var q = "SELECT s.id, s.name, SUM(s.available) available FROM (SELECT DISTINCT t.id, t.name, IF(p.product_id=" + db.escape(id) + ",1,0) available FROM teams t LEFT JOIN  packagesTeams pt ON t.id=pt.teamID LEFT JOIN packages p ON pt.packageID=p.id) s GROUP BY s.id";
				db.query_noerr(q, function(rows) {
					result.teams = rows;
					result.effective = ec.effective();
					getAssets(id, function(assets) {
						result.assets = assets;
						res.json({ "code" : 0, "result" : result, "write" : allowed.write });
					});
				});
			});
		}
	});
}

function uploadAsset(req, res) {
	web.checkPermission("packages", req.gid, function(allowed) {
		if (!allowed || !allowed.write) {
			res.status(403).json({ "code" : -1 });
		} else {
			var id = req.params.id;
			var path = req.files.content.path;
			var name = req.files.content.name;
			if (!path || !name || !id) return res.json({ "code" : -1, "id" : id, "write" : allowed.write });
			var params = { "Bucket" : config.AWS_BUCKET,
					"Key" : "assets/" + id + "/" + name,
					"Body" : fs.readFileSync(path) };
			params.Metadata = { "mime" : mime.lookup(path) };
			s3.upload(params, function(err, result) {
				res.json({ "code" : 0, "id" : id, "write" : allowed.write });
				db.weblog.insert({ "ts" : new Date().getTime(),
					"ip" : req.ip, 
					"type" : "uploadAssetPackage",
					"id" : common.escapeHtml(id),
					"action" : common.escapeHtml(name),
					"username" : req.user,
					"code" : 0
				});
				fs.unlink(path);
			});	
		}
	});
}

function deleteAsset(req, res) {
	web.checkPermission("packages", req.gid, function(allowed) {
		if (!allowed || !allowed.write) {
			res.status(403).json({ "code" : -1 });
		} else {
			var id = req.params.id;
			var files = req.fields.delete.split(",");
			var params = { "Bucket" : config.AWS_BUCKET };
			files.forEach(function (file) {
				params.Key = "assets/" + id + "/" + file;
				s3.deleteObject(params, function (err, result) { });
			});
			res.json({ "code" : 0, "id" : id, "write" : allowed.write });
			db.weblog.insert({	"ts" : new Date().getTime(),
						"ip" : req.ip, 
						"type" : "deleteAssetPackage",
						"id" : common.escapeHtml(id),
						"action" : common.escapeHtml(files),
						"username" : req.user,
						"code" : 0
					});
		}
	});
}

function updateTeam(req, res) {
	web.checkPermission("packages", req.gid, function(allowed) {
		if (!allowed || !allowed.write) {
			res.status(403).json({ "code" : -1 });
		} else {
			var id = req.params.id;
			var onID = "";
			var offID = "";
			Object.keys(req.fields).forEach(function (key) {
				if ( req.fields[key] == "on" ) {
					onID = key.substring(5);	// remove "team_"
				} else {
					offID = key.substring(5);	// remove "team_"
				}
			});
			if (onID) {
				var q = "INSERT INTO packagesTeams (packageID,teamID) SELECT id," + db.escape(onID) + "  FROM packages WHERE product_id=" + db.escape(id);
				db.query_noerr(q, function (rows) { });
				db.weblog.insert({	"ts" : new Date().getTime(),
							"ip" : req.ip, 
							"type" : "updateTeamPackage",
							"id" : common.escapeHtml(id),
							"action" : "INSERT " + common.escapeHtml(onID),
							"username" : req.user,
							"code" : 0
						});
			} else if (offID) {
				var q = "DELETE FROM packagesTeams WHERE teamID=" + db.escape(offID) + " AND packageID=(SELECT id FROM packages WHERE product_id=" + db.escape(id) + ")";
				db.query_noerr(q, function (rows) { });
				db.weblog.insert({	"ts" : new Date().getTime(),
							"ip" : req.ip, 
							"type" : "updateTeamPackage",
							"id" : common.escapeHtml(id),
							"action" : "DELETE " + common.escapeHtml(offID),
							"username" : req.user,
							"code" : 0
						});
			}
			res.json({ "code" : 0, "id" : id, "write" : allowed.write });
		}
	});
}

function updatePackage(req, res) {
	web.checkPermission("packages", req.gid, function(allowed) {
		if (!allowed || !allowed.write) {
			res.status(403).json({ "code" : -1 });
		} else {
			var id = req.params.id
			var name = req.fields.name;
			var title = req.fields.title;
			var app = req.fields.app;
			var subtitle = req.fields.subtitle;
			var description_short = req.fields.description_short;
			var description_full = req.fields.description_full;
			var disclaimer = req.fields.disclaimer;
			var price = req.fields.price;
			var recurrent = req.fields.recurrent;
			var sub_effect = req.fields.sub_effect;
			var unsub_effect = req.fields.unsub_effect;
			var action = req.fields.action;
			var order_id = req.fields.order_id;
			var advanced_payment = req.fields.advanced_payment;
			var free_package = req.fields.free_package;
			var beta = req.fields.beta;
			var beta_group = req.fields.beta_group;
			var columns = "";
			var values = "";
			var update = "";
			if (typeof name !== 'undefined'){
				columns = "name,";
				values = db.escape(name) + ",";
				update = "name=VALUES(name),";
			}
			else if (typeof title !== 'undefined'){
				columns = "title,";
				values = db.escape(title) + ",";
				update = "title=VALUES(title),";
			}
			else if (typeof app !== 'undefined'){
				columns = "app,";
				values = db.escape(app) + ",";
				update = "app=VALUES(app),";
			}
			else if (typeof subtitle !== 'undefined'){
				columns = "subtitle,";
				values = db.escape(subtitle) + ",";
				update = "subtitle=VALUES(subtitle),";
			}
			else if (typeof description_short !== 'undefined'){
				columns = "description_short,";
				values = db.escape(description_short) + ",";
				update = "description_short=VALUES(description_short),";
			}
			else if (typeof description_full !== 'undefined'){
				columns = "description_full,";
				values = db.escape(description_full) + ",";
				update = "description_full=VALUES(description_full),";
			}
			else if (typeof disclaimer !== 'undefined'){
				columns = "disclaimer,";
				values = db.escape(disclaimer) + ",";
				update = "disclaimer=VALUES(disclaimer),";
			}
			else if (typeof sub_effect !== 'undefined'){
				columns = "sub_effect,";
				values = db.escape(sub_effect) + ",";
				update = "sub_effect=VALUES(sub_effect),";
			}
			else if (typeof unsub_effect !== 'undefined'){
				columns = "unsub_effect,";
				values = db.escape(unsub_effect) + ",";
				update = "unsub_effect=VALUES(unsub_effect),";
			}
			else if (typeof action !== 'undefined'){
				if ( action == "on" ) {
					action = 1;
				} else {
					action = 0;
				}
				columns = "action,";
				values = db.escape(action) + ",";
				update = "action=VALUES(action),";
			}
			else if (typeof beta !== 'undefined'){
				if ( beta == "on" ) {
					beta = 1;
				} else {
					beta = 0;
				}
				columns = "beta,";
				values = db.escape(beta) + ",";
				update = "beta=VALUES(beta),";
			}
			else if (typeof advanced_payment !== 'undefined'){
				if ( advanced_payment == "on" ) {
					advanced_payment = 1;
				} else {
					advanced_payment = 0;
				}
				columns = "advanced_payment,";
				values = db.escape(advanced_payment) + ",";
				update = "advanced_payment=VALUES(advanced_payment),";
			}
			else if (typeof free_package !== 'undefined'){
				columns = "free_package,";
				values = (free_package ? db.escape(free_package) : "NULL") + ",";
				update = "free_package=VALUES(free_package),";
			}
			else if (typeof beta_group !== 'undefined'){
				columns = "beta_group,";
				values = db.escape(beta_group) + ",";
				update = "beta_group=VALUES(beta_group),";
			}
			else if (typeof order_id !== 'undefined'){
				columns = "order_id,";
				values = db.escape(order_id) + ",";
				update = "order_id=VALUES(order_id),";
			}
			var q = "INSERT INTO packages (product_id," + columns.slice(0,-1) + ") VALUES (" + db.escape(id) + "," + values.slice(0,-1) + ") ON DUPLICATE KEY UPDATE " + update.slice(0,-1);
			if (logsEnabled) common.log("updatePackage", q);
			db.query_noerr(q, function(rows) {
				res.json({ "code" : 0, "id" : id, "write" : allowed.write });
				db.weblog.insert({	"ts" : new Date().getTime(),
							"ip" : req.ip, 
							"type" : "updatePackage",
							"id" : common.escapeHtml(id),
							"action" : columns.slice(0,-1).replace(/['"{}]/g,'') + " VALUES " + values.slice(0,-1).replace(/['"{}]/g,''),	// avoid injection
							"username" : req.user,
							"code" : 0
						});
			});
		}
	});
}

function getBoostsLogs(req, res) {
	web.checkPermission("packages", req.gid, function(allowed) {
		if (!allowed) {
			res.status(403).json({ "code" : -1 });
		} else {
			var search = common.escapeHtml(req.query.search);
			var match = { "ts" : { "$gt" : new Date().getTime() - 1000 * 60 * 60 * 24 * 3 } };
			if (req.query.search) {
				match["$or"] = [
						{ "number" : new RegExp(search,"i") },
						{ "result.response.order_ref" : new RegExp(search,"i") },
						{ "req.fields.id" : new RegExp(search,"i") }
					];
			}
			db.paas.find(match).sort({ "_id" : -1 }).limit(50).toArray(function(err, item) { // 3 days
				if ( item ) res.json({ "code" : 0, "list" : item, "write" : allowed.write });
				else res.status(404).send('Not Found');
			});
		}
	});
}

function getAddonLogs(req, res) {
	web.checkPermission("packages", req.gid, function(allowed) {
		if (!allowed) {
			res.status(403).json({ "code" : -1 });
		} else {
			var search = common.escapeHtml(req.query.search);
			var match = { "ts" : { "$gt" : new Date().getTime() - 1000 * 60 * 60 * 24 * 3 } };
			if (req.query.search) {
				match["$or"] = [
						{ "number" : new RegExp(search,"i") },
						{ "result.response.order_ref" : new RegExp(search,"i") },
						{ "req.fields.id" : new RegExp(search,"i") }
					];
			}
			db.addonlog.find(match).sort({ "_id" : -1 }).limit(50).toArray(function(err, item) { // 3 days
				if ( item ) res.json({ "code" : 0, "list" : item, "write" : allowed.write });
				else res.status(404).send('Not Found');
			});
		}
	});
}
