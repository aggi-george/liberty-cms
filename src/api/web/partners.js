var config = require('../../../config');
var common = require('../../../lib/common');
var db = require('../../../lib/db_handler');
var ec = require('../../../lib/elitecore');
var web = require('./web');
var partnerManager = require('../../core/manager/promotions/partnerManager');

var logsEnabled = config.LOGSENABLED;

//exports

exports.addPartnersCustomers = addPartnersCustomers;
exports.loadPartnersCustomers = loadPartnersCustomers;
exports.deletePartnersCustomers = deletePartnersCustomers;

//functions

function addPartnersCustomers(req, res) {
    web.checkPermission("marketing", req.gid, function (allowed) {
        if (!allowed || !allowed.write) {
            res.status(403).json({"code": -1});
        } else {
            var data = JSON.parse(req.fields.data);
            var partner = req.fields.partner;

            partnerManager.addCustomers(data, partner, function(err, result) {
                if (err) {
                    return res.status(400).json({
                        code: -1,
                        error: err.message,
                        write: allowed.write
                    });
                } else {
                    return res.json({
                        code: 0,
                        addedCount: result.addedCount,
                        skippedCount: result.skippedCount,
                        write: allowed.write
                    });
                }
            });
        }
    });
}

function loadPartnersCustomers(req, res) {
    web.checkPermission("marketing", req.gid, function (allowed) {
        if (!allowed || !allowed.write) {
            res.status(403).json({"code": -1});
        } else {
            var filter = common.escapeHtml(req.query.sSearch);
            var offset = parseInt(req.query.iDisplayStart);
            var length = parseInt(req.query.iDisplayLength);

            partnerManager.loadCustomers(function(err, result) {
                if (err) {
                    return res.status(400).json({
                        code: -1,
                        error: err.message,
                        write: allowed.write
                    });
                }

                res.json({
                    "code": 0,
                    "write": allowed.write,
                    "data": result.data,
                    "iTotalRecords": result.totalCount,
                    "iTotalDisplayRecords": result.totalCount
                });
            }, offset, length, filter);
        }
    });
}

function deletePartnersCustomers(req, res) {
    web.checkPermission("marketing", req.gid, function (allowed) {
        if (!allowed || !allowed.write) {
            res.status(403).json({"code": -1});
        } else {
            var id = req.params.id;
            partnerManager.deleteCustomer(id, function(err) {
                if (err) {
                    return res.status(400).json({
                        code: -1,
                        error: err.message,
                        write: allowed.write
                    });
                } else {
                    return res.json({
                        code: 0,
                        write: allowed.write
                    });
                }
            });
        }
    });
}