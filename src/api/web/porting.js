var config = require('../../../config');
var common = require('../../../lib/common');
var db = require('../../../lib/db_handler');
var ec = require('../../../lib/elitecore');
var web = require('./web');
var async = require('async');

var portInManager = require('../../core/manager/porting/portInManager');
var portOutManager = require('../../core/manager/porting/portOutManager');

var logsEnabled = config.LOGSENABLED;

//exports

exports.getPortedInReport = getPortedInReport;
exports.getPortedInReportAll = getPortedInReportAll;
exports.getPortedInRequests = getPortedInRequests;
exports.getPortedInRequestsWithWarnings = getPortedInRequestsWithWarnings;
exports.updatePortedInRequest = updatePortedInRequest;
exports.updatePortedInOwner = updatePortedInOwner;
exports.activatePortInRequest = activatePortInRequest;
exports.recoverReTryPortIn = recoverReTryPortIn;

exports.getPortedOutRequests = getPortedOutRequests;
exports.getPortedInStatus = getPortedInStatus;
exports.moveToCorrectStatus = moveToCorrectStatus;
// functions

function getPortedInStatus(req, res) {
    const { id } = req.query;

    web.checkPermission('customers', req.gid, (allowed) => {
        if (!allowed) {
            res.status(403).json({ code: -1 });
        } else {
            portInManager.getPortedInStatus(id)
                .then((data) => {
                    const { bssStatus, localStatus } = data;
                    res.json({
                        code: 0,
                        data: {
                            id,
                            bssStatus,
                            localStatus,
                        },
                    });
                })
                .catch((err) => {
                    common.log('Error Caught: ', err);
                    res.status(404).json({ code: -1 });
                });
        }
    });
}

function moveToCorrectStatus(req, res) {
    web.checkPermission('customers', req.gid, (allowed) => {
        if (!allowed || (allowed.write < 2)) {
            res.status(403).json({ code: -1 });
        } else {
            const { bssStatus, id } = req.fields;
            const initiator = `[CMS][${req.user}]`;

            portInManager.updateRequestStatus(id, bssStatus, false, false, (err, result) => {
                if (err) {
                    common.log('ERR while moving status =>> ', err);
                    return res.status(400).json({ code: -1, error: err.message, write: allowed.write });
                }
                common.log('Updated successfully');
                res.json({
                    code: 0
                });

                db.weblog.insert({
                    "ts": new Date().getTime(),
                    "ip": req.ip,
                    "id": common.escapeHtml(id),
                    "action": common.escapeHtml("status=" + bssStatus),
                    "username": req.user,
                    "code": 0
                });
            }, config.OPERATIONS_KEY, undefined, initiator, undefined);
        }
    });
}

function getPortedInRequests(req, res) {
    web.checkPermission('customers', req.gid, (allowed) => {
        if (!allowed) {
            res.status(403).json({ code: -1 });
        } else {
            getPortingRequests('portin', req.query, (err, result) => {
                if (err) {
                    return res.status(400).json({code: -1, error: err.message, write: allowed.write});
                }
                res.json({
                    "code": 0,
                    "write": allowed.write,
                    "data": result.data,
                    "iTotalRecords": result.totalCount,
                    "iTotalDisplayRecords": result.totalCount
                });
            });
        }
    });
}

function getPortedOutRequests(req, res) {
    web.checkPermission('customers', req.gid, (allowed) => {
        if (!allowed) {
            res.status(403).json({ code: -1 });
        } else {
            getPortingRequests('portout', req.query, (err, result) => {
                if (err) {
                    return res.status(400).json({code: -1, error: err.message, write: allowed.write});
                }
                res.json({
                    "code": 0,
                    "write": allowed.write,
                    "data": result.data,
                    "iTotalRecords": result.totalCount,
                    "iTotalDisplayRecords": result.totalCount
                });
            });
        }
    });
}

function getPortingRequests(type, query, callback) {
    var status = query.status;
    var startDate = query.startDate ? new Date(query.startDate) : undefined;
    var endDate = query.endDate ? new Date(query.endDate) : undefined;
    var uniqueType = query.uniqueType;
    var donorCode = query.donorCode;
    var source = query.source;

    var displayLength = parseInt(query.iDisplayLength);
    var displayStart = parseInt(query.iDisplayStart);
    var filter = query.sSearch;
    var sortColumn = parseInt(query.iSortCol_0);
    var sortDir = query.sSortDir_0;

    if (sortColumn == 2) {
        sortColumn = 'UPDATE_DATE';
    } else if (sortColumn == 3) {
        sortColumn = 'START_DATE';
    } else {
        sortColumn = 'ID';
    }

    if (type == 'portout') {
        portOutManager.loadRequest(undefined, {
            status: status,
            startDate: startDate,
            endDate: endDate,
            uniqueType: uniqueType,
            donorCode: donorCode,
            source: source
        }, callback, displayLength, displayStart, filter, sortColumn, sortDir, undefined, startDate, endDate);
    } else {
        portInManager.loadRequest(undefined, {
            status: status,
            startDate: startDate,
            endDate: endDate,
            uniqueType: uniqueType,
            donorCode: donorCode,
            source: source
        }, callback, displayLength, displayStart, filter, sortColumn, sortDir, undefined, startDate, endDate);
    }
}

function getPortedInReport(req, res) {
    web.checkPermission('customers', req.gid, (allowed) => {
        if (!allowed) {
            res.status(403).json({ code: -1 });
        } else {
            var startDate = req.query.startDate ? new Date(req.query.startDate) : undefined;
            var endDate = req.query.endDate ? new Date(req.query.endDate) : undefined;
            var uniqueType = req.query.uniqueType;
            var reportDonorCode = req.query.reportDonorCode;
            var reportSource = req.query.reportSource;
            var filterByIntention = req.query.teamType == "MARKETING";

            if (startDate) {
                startDate = new Date(startDate.getTime() - 24 * 60 * 60 * 1000);
            }

            portInManager.loadRequestReportByType({
                status: 'ALL',
                donorCode: reportDonorCode,
                source: reportSource,
                uniqueType: uniqueType,
                filterByIntention: filterByIntention
            }, startDate, endDate, function (err, result) {
                if (err) {
                    return res.status(400).json({code: -1, error: err.message, write: allowed.write});
                }

                res.json({
                    "code": 0,
                    "write": allowed.write,
                    "data": result.data,
                    "errors": result.errors,
                    "orderSources": result.orderSources,
                    "columnLabels": result.columnLabels,
                    "columnKeys": result.columnKeys,
                    "pastKeys": result.pastKeys,
                    "futureKeys": result.futureKeys,
                    "todaysKeys": result.todaysKeys
                });
            });
        }
    });
}

function getPortedInReportAll(req, res) {
    web.checkPermission('customers', req.gid, (allowed) => {
        if (!allowed) {
            res.status(403).json({ code: -1 });
        } else {
            var startDate = req.query.startDate ? new Date(req.query.startDate) : undefined;
            var endDate = req.query.endDate ? new Date(req.query.endDate) : undefined;
            var uniqueType = req.query.uniqueType;
            var filterByIntention = req.query.teamType == "MARKETING";

            let tasks = [];
            tasks.push((callback) => {
                portInManager.loadRequestReportByType({
                    status: "ALL",
                    uniqueType: uniqueType,
                    filterByIntention: filterByIntention
                }, startDate, endDate, (err, result) => {
                    callback(err, {type: "ALL", result: result});
                });
            });
            tasks.push((callback) => {
                portInManager.loadRequestReportByType({
                    status: "ALL",
                    donorCode: "001",
                    uniqueType: uniqueType,
                    filterByIntention: filterByIntention
                }, startDate, endDate, (err, result) => {
                    callback(err, {type: "M1", result: result});
                });
            });
            tasks.push((callback) => {
                portInManager.loadRequestReportByType({
                    status: "ALL",
                    donorCode: "002",
                    uniqueType: uniqueType,
                    filterByIntention: filterByIntention
                }, startDate, endDate, (err, result) => {
                    callback(err, {type: "STARHUB", result: result});
                });
            });
            tasks.push((callback) => {
                portInManager.loadRequestReportByType({
                    status: "ALL",
                    donorCode: "003",
                    uniqueType: uniqueType,
                    filterByIntention: filterByIntention
                }, startDate, endDate, (err, result) => {
                    callback(err, {type: "SINGTEL", result: result});
                });
            });
            tasks.push((callback) => {
                portInManager.loadRequestReportByType({
                    status: "ALL",
                    source: "ECOMM",
                    uniqueType: uniqueType,
                    filterByIntention: filterByIntention
                }, startDate, endDate, (err, result) => {
                    callback(err, {type: "ECOMM", result: result});
                });
            });
            tasks.push((callback) => {
                portInManager.loadRequestReportByType({
                    status: "ALL",
                    source: "SELFCARE",
                    uniqueType: uniqueType,
                    filterByIntention: filterByIntention
                }, startDate, endDate, (err, result) => {
                    callback(err, {type: "SELFCARE", result: result});
                });
            });

            async.parallel(tasks, (err, result) => {
                if (err) {
                    return res.status(400).json({code: -1, error: err.message, write: allowed.write});
                }

                var output = {
                    code: 0,
                    write: allowed.write,
                    data: []
                };

                if (result && result[0]) {
                    output.columnLabels = result[0].result.columnLabels;
                    output.columnKeys = result[0].result.columnKeys;
                    output.pastKeys = result[0].result.pastKeys;
                    output.futureKeys = result[0].result.futureKeys;
                    output.todaysKeys = result[0].result.todaysKeys;
                }

                if (result) {
                    result.forEach((item) => {
                        output.data.push({type: item.type, data: item.result.data,
                            errors: item.result.errors, orderSources: item.result.orderSources});
                    })
                }

                res.json(output);
            });
        }
    });
}

function getPortedInRequestsWithWarnings(req, res) {
    web.checkPermission('customers', req.gid, (allowed) => {
        if (!allowed) {
            res.status(403).json({ code: -1 });
        } else {
            var displayLength = parseInt(req.query.iDisplayLength);
            var displayStart = parseInt(req.query.iDisplayStart);

            var sortColumn = parseInt(req.query.iSortCol_0);
            var sortDir = req.query.sSortDir_0;

            if (sortColumn == 2) {
                sortColumn = 'UPDATE_DATE';
            } else if (sortColumn == 3) {
                sortColumn = 'START_DATE';
            } else if (sortColumn == 7) {
                sortColumn = 'WARNING_DATE';
            } else {
                sortColumn = 'ID';
            }

            portInManager.loadRequestWithWarnings((err, result) => {
                if (err) {
                    return res.status(400).json({code: -1, error: err.message, write: allowed.write});
                }
                res.json({
                    "code": 0,
                    "write": allowed.write,
                    "data": result.data,
                    "iTotalRecords": result.totalCount,
                    "iTotalDisplayRecords": result.totalCount
                });
            }, displayLength, displayStart, sortColumn, sortDir);
        }
    });
}

function updatePortedInRequest(req, res) {
    web.checkPermission('customers', req.gid, (allowed) => {
        if (!allowed || (allowed.write < 2)) {
            res.status(403).json({ code: -1 });
        } else {
            var id = req.fields.id;
            var status = req.fields.status;
            var reason = req.fields.reason;
            var description = req.fields.description;
            var initiator = "[CMS][" + req.user + "]";

            portInManager.updateRequestStatus(id, status, false, false, (err, result) => {
                if (err) {
                    return res.status(400).json({code: -1, error: err.message, write: allowed.write});
                }

                res.json({
                    "code": 0,
                    result,
                });

                db.weblog.insert({
                    "ts": new Date().getTime(),
                    "ip": req.ip,
                    "type": "updatePortedInRequest",
                    "id": common.escapeHtml(id),
                    "action": common.escapeHtml("status=" + status + ", reason=" + reason),
                    "username": req.user,
                    "code": 0
                });
            }, config.OPERATIONS_KEY, reason, initiator, description);
        }
    });
}

function updatePortedInOwner(req, res) {
    web.checkPermission('customers', req.gid, (allowed) => {
        if (!allowed || (allowed.write < 2)) {
            res.status(403).json({ code: -1 });
        } else {
            var fields = req.fields;
            var initiator = "[CMS][" + req.user + "]";
            portInManager.updateRequestOwner(fields.id, fields.ownerName, fields.ownerIdType,
                fields.ownerIdNumber, (err, result) => {
                    if (err) {
                        return res.status(400).json({
                            code: -1,
                            error: err.message
                        });
                    }

                    res.json({code: 0});
                }, undefined, initiator);

            db.weblog.insert({
                'ts': new Date().getTime(),
                ip: req.ip,
                type: 'updatePortedInOwner',
                'id': common.escapeHtml(fields.id),
                'action': common.escapeHtml('id=' + fields.id + ', ownerName='
                + fields.ownerName + ', id=' + fields.ownerIdNumber),
                'username': req.user,
                code: 0,
            });
        }
    });
}

function activatePortInRequest(req, res) {
    web.checkPermission('customers', req.gid, (allowed) => {
        if (!allowed || (allowed.write < 2)) {
            res.status(403).json({code: -1});
        } else {
            var fields = req.fields;
            var initiator = "[CMS][" + req.user + "]";

            portInManager.activateRequest(fields.service_instance_number,
                (fields.startDate ? new Date(fields.startDate) : undefined), (err, result) => {
                    if (err) {
                        return res.status(400).json({
                            code: -1,
                            error: err.message
                        });
                    }

                    res.json({code: 0});
                }, undefined, initiator);

            db.weblog.insert({
                'ts': new Date().getTime(),
                ip: req.ip,
                type: 'activatePortInRequest',
                'id': common.escapeHtml(fields.id),
                'action': common.escapeHtml('id=' + fields.id),
                'username': req.user,
                code: 0
            });
        }
    });
}

function recoverReTryPortIn(req, res) {
    web.checkPermission('customers', req.gid, (allowed) => {
        if (!allowed || (allowed.write < 2)) {
            res.status(403).json({ code: -1 });
        } else {
            var id = req.fields.id;
            var status = "IN_PROGRESS";
            var portInRecoverType = req.params.type.toUpperCase();
            var initiator = "[CMS][" + req.user + "]";

            portInManager.updateRequestStatus(id, 'IN_PROGRESS', false, false, (err, result) => {
                if (err) {
                    return res.status(400).json({code: -1, error: err.message, write: allowed.write});
                }

                res.json({
                    "code": 0
                });

                db.weblog.insert({
                    "ts": new Date().getTime(),
                    "ip": req.ip,
                    "type": "recoverReTryPortIn",
                    "id": common.escapeHtml(id),
                    "action": common.escapeHtml("status=" + status),
                    "username": req.user,
                    "code": 0
                });
            }, config.OPERATIONS_KEY, undefined, initiator, undefined, portInRecoverType);
        }
    });
}
