var config = require('../../../config');
var common = require('../../../lib/common');
var db = require('../../../lib/db_handler');
var web = require('./web');

var logsEnabled = config.LOGSENABLED;

//exports

exports.update = update;

//functions

function update(req, res) {
	var cookie = req.cookies.cmscookie;
	db.cache_get("session_cache", cookie, function(value) {
		if ( value ) {
			var name = value.split("|")[0];
			var type = req.params.type;
			if ( type == "password" ) {
				var password = req.fields.password;
				var user = name.split("@")[0];
				var domain = name.split("@")[1];
				var q = "UPDATE admin SET password=MD5(" + db.escape(user + ":" + domain + ":" + password) + ") WHERE email=" + db.escape(name);
				db.query_noerr(q, function (rows) {
					res.json({ "code" : 0 });
					db.cache_del("session_cache", cookie);
					db.weblog.insert({	"ts" : new Date().getTime(),
								"ip" : req.ip, 
								"type" : "updateProfile",
								"id" : common.escapeHtml(name),
								"action" : "password",
								"username" : req.user,
								"code" : 0
							});
				});
			} else {
				res.status(400).json({ "code" : -1 });
			}
		} else {
			res.status(403).json({ "code" : -1 });
		}
	});
}
