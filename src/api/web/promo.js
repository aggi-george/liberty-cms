var async = require('async');
var config = require('../../../config');
var common = require('../../../lib/common');
var db = require('../../../lib/db_handler');
var ec = require('../../../lib/elitecore');
var web = require('./web');

var bonusTypesService = require('../../../res/bonus/bonusTypesService');
var verificationManager = require('../../core/manager/promotions/verificationManager');
var bonusManager = require('../../core/manager/bonus/bonusManager');
var promotionsManager = require('../../core/manager/promotions/promotionsManager');

var logsEnabled = config.LOGSENABLED;

//exports

exports.getProducts = getProducts;
exports.getCodesLogs = getCodesLogs;
exports.getCodes = getCodes;
exports.addCode = addCode;
exports.addBonusHistory = addBonusHistory;
exports.deleteCode = deleteCode;
exports.copyCode = copyCode;
exports.deleteLog = deleteLog;
exports.getCategories = getCategories;
exports.addCategory = addCategory;
exports.addCategoryPage = addCategoryPage;
exports.deleteCategory = deleteCategory;

//functions

function getProducts(req, res) {
    web.checkPermission("marketing", req.gid, function (allowed) {
        if (!allowed) {
            res.status(403).json({"code": -1});
        } else {
            var bonuses = [];
            ec.packages().bonus.forEach(function (item) {
                if (item.id && item.app) bonuses.push(item);
            });

            var general_free = [];
            ec.packages().general.forEach(function (item) {
                if (item.id && item.title && item.price == 0) general_free.push(item);
            });
            res.json({
                "code": 0,
                "write": allowed.write,
                "valid_bonus_types": bonusTypesService.getBonusTypes(),
                "valid_partners": bonusTypesService.getPartnersTypes(),
                "valid_second_verification_types": verificationManager.getSupportedTypes(),
                "valid_bonuses": bonuses,
                "valid_free_general": general_free
            });
        }
    });
}

function getCodesLogs(req, res) {
    web.checkPermission("marketing", req.gid, function (allowed) {
        if (!allowed) {
            res.status(403).json({"code": -1});
        } else {
            var code = req.params.code;
            var categoryId = req.params.categoryId;
            var limit = parseInt(req.query.iDisplayLength);
            var offset = parseInt(req.query.iDisplayStart);
            var sortColumn = parseInt(req.query.iSortCol_0);
            var sortDir = req.query.sSortDir_0;
            var filter = req.query.sSearch;

            if (!sortDir) {
                sortDir = "desc";
            }

            sortColumn = "log.id"
            var fields = "log.id as id, log.code as code, log.product_id as productId, " +
                " log.months_count as monthsCount, log.free_addon_1 as freeAddon1," +
                " log.free_addon_months_1 as freeAddonMonths1, log.service_instance_number as serviceInstanceNumber, " +
                " log.category, log.status, log.order_reference_number as orderReferenceNumber, " +
                " log.waiver_cents as waiverCents, log.waiver_month as waiverMonths, log.bonus_data as bonusData, " +
                " log.bonus_data_type as bonusDataType, log.level as level, log.used_ts as usedDate";

            var where = "";

            if (code && code != 'all') {
                code = code.replace("%23", "#");
                where = "WHERE log.code=" + db.escape(code) +
                (filter ? " AND (log.service_instance_number LIKE " + db.escape("%" + filter + "%") +
                " OR log.order_reference_number LIKE " + db.escape("%" + filter + "%") + ")" : "");
            } else {
                where = "WHERE log.category=" + db.escape(categoryId) +
                (filter ? " AND (log.service_instance_number LIKE " + db.escape("%" + filter + "%") +
                " OR log.order_reference_number LIKE " + db.escape("%" + filter + "%") + ")" : "");
            }

            var q = "SELECT " + fields + " FROM promoCodesLogs log " +
                where + " ORDER BY " + sortColumn + " " + sortDir + " " +
                (limit > 0 ? " LIMIT " + db.escape(limit) : "") +
                (offset > 0 ? " OFFSET " + db.escape(offset) : "");

            db.query_noerr(q, function (rows) {

                var selectCount = "SELECT count(*) as count FROM promoCodesLogs log " + where;
                db.query_noerr(selectCount, function (countResult) {
                    var totalCount = countResult && countResult.length > 0
                        ? countResult[0].count : 0;

                    res.json({
                        "code": 0,
                        "write": allowed.write,
                        "iTotalRecords": totalCount,
                        "iTotalDisplayRecords": totalCount,
                        "data": rows
                    });
                });
            });
        }
    });
}


function getCodes(req, res) {
    web.checkPermission("marketing", req.gid, function (allowed) {
        if (!allowed) {
            res.status(403).json({"code": -1});
        } else {
            var categoryId = req.params.categoryId;
            var limit = parseInt(req.query.iDisplayLength);
            var offset = parseInt(req.query.iDisplayStart);
            var filter = req.query.sSearch;

            var fields = "promoCodes.id as id, promoCodes.category as category, promoCodes.code, promoCodes.max," +
                " promoCodes.waiver_cents as waiverCents, promoCodes.waiver_month as waiverMonths, " +
                " promoCodes.second_verification_type as secondVerificationType, promoCodes.extraBillDay as extraBillDay, " +
                " promoCodes.type as promoCodeType, promoCodes.device_discount_cents as deviceDiscountCents, " +
                " promoCodes.bonus_data as bonusData, promoCodes.bonus_data_type as bonusDataType," +
                " promoCodes.reg_discount_cents as regDiscountCents, promoCodes.sku_name_1 as skuName1, " +
                " promoCodes.sku_key_1 as skuKey1, promoCodes.sku_quantity_1 as skuQuantity1, " +
                " promoCodes.free_addon_1 as freeAddon1, promoCodes.free_addon_months_1 as freeAddonMonths1, " +
                " promoCodes.enabled, promoCodes.product_id as productId, promoCodes.months_count as monthsCount, " +
                " promoCodes.start, promoCodes.end, (SELECT count(*) FROM promoCodesLogs WHERE promoCodesLogs.code" +
                " = promoCodes.code) as usedCount";

            var where = "WHERE category=" + db.escape(parseInt(categoryId)) +
                (filter ? " AND (code LIKE " + db.escape("%" + filter + "%") +
                " OR code IN (SELECT DISTINCT code FROM promoCodesLogs WHERE " +
                " service_instance_number LIKE " + db.escape("%" + filter + "%") +
                " OR order_reference_number LIKE " + db.escape("%" + filter + "%") + "))" : "");

            var q = "SELECT " + fields + " FROM promoCodes " +
                " LEFT JOIN packages ON (packages.product_id=promoCodes.product_id) " +
                where + " GROUP BY code ORDER BY promoCodes.id DESC" +
                (limit > 0 ? " LIMIT " + db.escape(limit) : "") +
                (offset > 0 ? " OFFSET " + db.escape(offset) : "");

            db.query_err(q, function (err, rows) {
                if (err) {
                    return res.status(400).json({"code": -1, error: err.message});
                }

                var selectCount = "SELECT count(*) as count FROM promoCodes " + where;
                db.query_noerr(selectCount, function (countResult) {
                    var totalCount = countResult && countResult.length > 0
                        ? countResult[0].count : 0;

                    res.json({
                        "code": 0,
                        "webPartnersUrl": config.ECOMM_PARTNERS_URL,
                        "write": allowed.write,
                        "iTotalRecords": totalCount,
                        "iTotalDisplayRecords": totalCount,
                        "data": rows
                    });
                });
            });
        }
    });
}

function getCategories(req, res) {
    web.checkPermission("marketing", req.gid, function (allowed) {
        if (!allowed) {
            res.status(403).json({"code": -1});
        } else {
            var limit = parseInt(req.query.iDisplayLength);
            var offset = parseInt(req.query.iDisplayStart);
            var filter = req.query.sSearch;

            var selectQueryWhereSection = filter ? "WHERE name LIKE " + db.escape("%" + filter + "%") +
            " OR id IN (SELECT DISTINCT category FROM promoCodes WHERE code LIKE " + db.escape("%" + filter + "%") + ")" +
            " OR id IN (SELECT DISTINCT category FROM promoCodesLogs WHERE " +
            " service_instance_number LIKE " + db.escape("%" + filter + "%") +
            " OR order_reference_number LIKE " + db.escape("%" + filter + "%") + ")" : "";

            var q = "SELECT * FROM promoCodesCategories " +
                selectQueryWhereSection +
                " ORDER BY id DESC" +
                (limit > 0 ? " LIMIT " + db.escape(limit) : "") +
                (offset > 0 ? " OFFSET " + db.escape(offset) : "");

            db.query_err(q, function (err, rows) {
                if (err) {
                    return res.status(400).json({"code": -1, error: err.message});
                }

                var selectCount = "SELECT count(*) as count FROM promoCodesCategories " + selectQueryWhereSection;
                db.query_noerr(selectCount, function (countResult) {
                    var totalCount = countResult && countResult.length > 0
                        ? countResult[0].count : 0;

                    res.json({
                        "code": 0,
                        "webPartnersUrl": config.ECOMM_PARTNERS_URL,
                        "write": allowed.write,
                        "iTotalRecords": totalCount,
                        "iTotalDisplayRecords": totalCount,
                        "data": rows
                    });
                });
            });
        }
    });
}

function addCode(req, res) {
    web.checkPermission("marketing", req.gid, function (allowed) {
        if (!allowed || !allowed.write) {
            res.status(403).json({"code": -1});
        } else {
            var q = "SELECT start, end FROM promoCodesCategories WHERE id=" + db.escape(req.fields.category);
            db.query_noerr(q, function (rows) {
                if (rows && rows[0]) {
                    var productValue = req.fields.productId ? db.escape(req.fields.productId) : "NULL";
                    var maxValue = parseInt(req.fields.max) >= -1 ? parseInt(req.fields.max) : "0";
                    var monthCountValue = parseInt(req.fields.monthsCount) >= -1 ? parseInt(req.fields.monthsCount) : "0";
                    var waiverMonthsValue = parseInt(req.fields.waiverMonths) > 0 ? parseInt(req.fields.waiverMonths) : "0";
                    var waiverCentsValue = parseInt(req.fields.waiverCents) > 0 ? parseInt(req.fields.waiverCents) : "0";
                    var regDiscountCentsValue = parseInt(req.fields.regDiscountCents) > 0 ? parseInt(req.fields.regDiscountCents) : "0";
                    var deviceDiscountCentsValue = parseInt(req.fields.deviceDiscountCents) > 0 ? parseInt(req.fields.deviceDiscountCents) : "0";
                    var freeAddonMonths1Value = parseInt(req.fields.freeAddonMonths1) >= -1 ? parseInt(req.fields.freeAddonMonths1) : "0";
                    var type = req.fields.type;
                    var q;
                    if (req.fields.id) {
                        q = "UPDATE promoCodes SET code=" + db.escape(req.fields.code) +
                        ", product_id=" + productValue +
                        ", category=" + db.escape(parseInt(req.fields.category)) +
                        ", max=" + db.escape(maxValue) +
                        ", waiver_cents=" + db.escape(waiverCentsValue) +
                        ", waiver_month=" + db.escape(waiverMonthsValue) +
                        ", reg_discount_cents=" + db.escape(regDiscountCentsValue) +
                        ", device_discount_cents=" + db.escape(req.fields.deviceDiscountCents) +
                        ", months_count=" + db.escape(monthCountValue) +
                        ", sku_name_1=" + db.escape(req.fields.skuName1) +
                        ", sku_key_1=" + db.escape(req.fields.skuKey1) +
                        ", sku_quantity_1=" + db.escape(req.fields.skuQuantity1) +
                        ", free_addon_1=" + db.escape(req.fields.freeAddon1) +
                        ", free_addon_months_1=" + db.escape(freeAddonMonths1Value) +
                        ", extraBillDay=" + db.escape(parseInt(req.fields.extraBillDay) ? parseInt(req.fields.extraBillDay) : 0) +
                        ", second_verification_type=" + db.escape(req.fields.secondVerificationType) +
                        ", enabled=" + db.escape(req.fields.enabled === 'true' ? 1 : 0) +
                        ", bonus_data=" + (parseInt(req.fields.bonusData) > 0 ? db.escape(parseInt(req.fields.bonusData)) : "0") +
                        ", bonus_data_type=" + db.escape(req.fields.bonusDataType) +
                        ", start=" + db.escape(req.fields.start ? req.fields.start : "0000-00-00 00:00:00") +
                        ", end=" + db.escape(req.fields.end ? req.fields.end : "0000-00-00 00:00:00") +
                        ", type=" + db.escape(type ? type : "DEFAULT") +
                        " WHERE id=" + db.escape(parseInt(req.fields.id));
                    } else {
                        q = "INSERT INTO promoCodes (code, product_id, category, max, waiver_cents, waiver_month" +
                        ", reg_discount_cents, device_discount_cents, months_count, sku_name_1, sku_key_1, sku_quantity_1, free_addon_1" +
                        ", free_addon_months_1, extraBillDay, second_verification_type, enabled, bonus_data, bonus_data_type, type, start, end) " +
                        "VALUES (" + db.escape(req.fields.code) + "," + productValue + ","
                        + db.escape(parseInt(req.fields.category)) + ", "
                        + db.escape(maxValue) + ", "
                        + db.escape(waiverCentsValue) + ", "
                        + db.escape(waiverMonthsValue) + ", "
                        + db.escape(regDiscountCentsValue) + ", "
                        + db.escape(deviceDiscountCentsValue) + ", "
                        + db.escape(monthCountValue) + ", "
                        + db.escape(req.fields.skuName1) + ", "
                        + db.escape(req.fields.skuKey1) + ", "
                        + db.escape(req.fields.skuQuantity1) + ", "
                        + db.escape(req.fields.freeAddon1) + ", "
                        + db.escape(freeAddonMonths1Value) + ", "
                        + db.escape(parseInt(req.fields.extraBillDay) ? parseInt(req.fields.extraBillDay) : 0) + ", "
                        + db.escape(req.fields.secondVerificationType) + ", "
                        + db.escape(req.fields.enabled === 'true' ? 1 : 0) + ", "
                        + (parseInt(req.fields.bonusData) > 0 ? db.escape(parseInt(req.fields.bonusData)) : "0") + ", "
                        + db.escape(req.fields.bonusDataType) + ", "
                        + db.escape(type ? type : "DEFAULT") + ","
                        + db.escape(req.fields.start ? req.fields.start : "0000-00-00 00:00:00") + ","
                        + db.escape(req.fields.end ? req.fields.end : "0000-00-00 00:00:00") + ")";
                    }

                    db.query_err(q, function (err) {
                        if (err) {
                            res.status(400).json({code: -1, error: err.message, write: allowed.write});
                            return;
                        }

                        res.json({"code": 0, "write": allowed.write});
                    });
                } else {
                    res.status(400).json({"code": -1, error: "Category is not found", "write": allowed.write});
                }
            });
        }
    });
}

function addBonusHistory(req, res) {
    web.checkPermission("marketing", req.gid, function (allowed) {
        if (!allowed || !allowed.write) {
            res.status(403).json({"code": -1});
        } else {
            var prefix = req.fields.prefix;
            var number = req.fields.number;
            var bonusType = req.fields.bonus_type;
            var bonusSubType = req.fields.bonus_sub_type;
            var bonusKb = req.fields.bonus_kb;
            var displayUntilTs = req.fields.display_until_ts? new Date(req.fields.display_until_ts) : undefined;
            var serviceInstanceNumber = req.fields.service_instance_number;
            var initiator = "[CMS][" + req.user + "]";

            if (!serviceInstanceNumber) {
                return res.status(400).json({"code": -1, error: "No Service Instance provided", "write": allowed.write});
            }

            var metadata;
            if (bonusType == 'referral') {
                metadata = {
                    data1: initiator,
                    data2: "65",
                    data3: "12345678",
                    data4: "referral_referred",
                    data5: "LW99999999990",
                    data6: "LW99999999991",
                    data7: "G1111111G",
                    dataDate1: new Date()
                };
            }

            bonusManager.saveHistoryItem(prefix, number, bonusType, parseInt(bonusKb), serviceInstanceNumber,
                displayUntilTs, bonusSubType, metadata, initiator,
                config.OPERATIONS_KEY, function (err) {
                    if (err) {
                        return res.status(400).json({"code": -1, error: err.message, "write": allowed.write});
                    }
                    return res.json({"code": 0, "number": number, "prefix": prefix, "write": allowed.write});
                })
        }
    });
}

function addCategory(req, res) {
    web.checkPermission("marketing", req.gid, function (allowed) {
        if (!allowed || !allowed.write) {
            res.status(403).json({"code": -1});
        } else {
            var q;
            if (req.fields.id) {
                q = "UPDATE promoCodesCategories SET name=" + db.escape(req.fields.name) +
                ", type=" + db.escape(req.fields.type) +
                ", tag=" + db.escape(req.fields.tag) +
                ", partner=" + (req.fields.partner && req.fields.partner != null ? db.escape(req.fields.partner) : "NULL") +
                ", start=" + db.escape(req.fields.start) +
                ", end=" + db.escape(req.fields.end) +
                ", max=" + db.escape(parseInt(req.fields.max)) +
                ", max_level_2=" + db.escape(parseInt(req.fields.maxL2)) +
                ", max_level_3=" + db.escape(parseInt(req.fields.maxL3)) +
                ", multiple_codes_allowed=" + db.escape(parseInt(req.fields.multipleCodesAllowed)) +
                ", enabled=" + db.escape(req.fields.enabled === 'true' ? 1 : 0) +
                ", title=" + db.escape(req.fields.title) +
                ", message=" + db.escape(req.fields.message) +
                ", image1=" + db.escape(req.fields.image1) +
                ", enable_web_registration=" + db.escape(req.fields.webRegistration === 'true' ? 1 : 0) +
                ", public_name=" + db.escape(req.fields.publicName) +
                ", code_prefix=" + db.escape(req.fields.codePrefix) +
                ", code_valid_days=" + db.escape(req.fields.codeValidDays ? parseInt(req.fields.codeValidDays) : "0") +
                ", code_pattern_level_1=" + (req.fields.codePatternL1 ? db.escape(req.fields.codePatternL1) : "NULL") +
                ", code_pattern_level_2=" + (req.fields.codePatternL2 ? db.escape(req.fields.codePatternL2) : "NULL") +
                ", code_pattern_level_3=" + (req.fields.codePatternL3 ? db.escape(req.fields.codePatternL3) : "NULL") +
                ", customMNumber=" + (req.fields.customMNumber ? db.escape(req.fields.customMNumber) : "NULL") +
                ", bankName=" + (req.fields.bankName ? db.escape(req.fields.bankName) : "NULL") +
                " WHERE id=" + db.escape(parseInt(req.fields.id));
            } else {
                q = "INSERT INTO promoCodesCategories (name, type, tag, partner, start, end, max, max_level_2, " +
                "max_level_3, multiple_codes_allowed, enabled, title, message, image1, enable_web_registration, public_name," +
                "code_prefix, code_valid_days, code_pattern_level_1, code_pattern_level_2, code_pattern_level_3," +
                " customMNumber, bankName) VALUES (" +
                db.escape(req.fields.name) + "," + db.escape(req.fields.type) + "," + db.escape(req.fields.tag) + "," +
                (req.fields.partner && req.fields.partner != null ? db.escape(req.fields.partner) : "NULL" ) + "," +
                db.escape(req.fields.start) + "," + db.escape(req.fields.end) + "," +
                db.escape(parseInt(req.fields.max)) + "," + db.escape(parseInt(req.fields.maxL2)) + "," +
                db.escape(parseInt(req.fields.maxL3)) + "," + db.escape(parseInt(req.fields.multipleCodesAllowed)) + "," +
                db.escape(req.fields.enabled === 'true' ? 1 : 0) + "," +
                (req.fields.title ? db.escape(req.fields.title) : "''") + "," +
                (req.fields.message ? db.escape(req.fields.message) : "''") + "," +
                (req.fields.image1 ? db.escape(req.fields.image1) : "''") + "," +
                db.escape(req.fields.webRegistration === 'true' ? 1 : 0) + ", " +
                db.escape(req.fields.publicName) + ", " + db.escape(req.fields.codePrefix)+ ", " +
                db.escape(req.fields.codeValidDays ? parseInt(req.fields.codeValidDays) : "0") + ", " +
                (req.fields.codePatternL1 ? db.escape(req.fields.codePatternL1) : "NULL") + ", " +
                (req.fields.codePatternL2 ? db.escape(req.fields.codePatternL2) : "NULL") + ", " +
                (req.fields.codePatternL3 ? db.escape(req.fields.codePatternL3) : "NULL") + ", " +
                (req.fields.customMNumber ? db.escape(req.fields.customMNumber) : "NULL") + ", " +
                (req.fields.bankName ? db.escape(req.fields.bankName) : "NULL") + ")";
            }

            db.query_err(q, function (err, result) {
                if (err) {
                    res.status(400).json({code: -1, error: err.message, write: allowed.write});
                    return;
                }

                if (req.fields.id) {
                    var updateUniqueType = "UPDATE promoCodesLogs SET unique_in_category=" +
                        (parseInt(req.fields.multipleCodesAllowed) == 1 ? "NULL" : db.escape(1)) +
                        " WHERE category=" + db.escape(parseInt(req.fields.id));

                    db.query_err(updateUniqueType, function (err, result) {
                        if (err) {
                            res.status(400).json({code: -1, error: err.message, write: allowed.write});
                            return;
                        }
                        res.json({"code": 0, "write": allowed.write});
                    });
                } else {
                    res.json({"code": 0, "write": allowed.write});
                }
            });
        }
    });
}

function addCategoryPage(req, res) {
    web.checkPermission("marketing", req.gid, function (allowed) {
        if (!allowed || !allowed.write) {
            res.status(403).json({"code": -1});
        } else {
            var q;
            if (req.fields.id) {
                q = "UPDATE promoCodesCategories SET registration_url_path=" + db.escape(req.fields.pathUrl) +
                ", tac_url=" + db.escape(req.fields.tacUrl) +
                ", faq_url=" + db.escape(req.fields.faqUrl) +
                ", header_image_url=" + db.escape(req.fields.headerImageUrl) +
                ", footer_image_url=" + db.escape(req.fields.footerImageUrl) +
                ", background_image_url=" + db.escape(req.fields.backgroundImageUrl) +
                ", success_image_url=" + db.escape(req.fields.successImageUrl) +
                ", field_name=" + db.escape(req.fields.fieldName === 'true' ? 1 : 0) +
                ", field_phone=" + db.escape(req.fields.fieldPhone === 'true' ? 1 : 0) +
                ", field_email=" + db.escape(req.fields.fieldEmail === 'true' ? 1 : 0) +
                ", field_password=" + db.escape(req.fields.fieldPassword === 'true' ? 1 : 0) +
                ", field_nric=" + db.escape(req.fields.fieldNric === 'true' ? 1 : 0) +
                ", field_corporate_email=" + db.escape(req.fields.fieldCorporateEmail === 'true' ? 1 : 0) +
                ", unique_field=" + db.escape(req.fields.uniqueField) +
                ", registration_type=" + db.escape(req.fields.registrationType) +
                ", registration_button_text=" + db.escape(req.fields.registrationButtonText) +
                ", registration_button_text_logged_in=" + db.escape(req.fields.registrationButtonTextLoggedIn) +
                ", registration_oauth20_url=" + db.escape(req.fields.registrationOAuth20Url) +
                ", usage_activity=" + db.escape(req.fields.usageActivity) +
                ", welcome_activity=" + db.escape(req.fields.welcomeActivity) +
                ", email_extension=" + db.escape(req.fields.emailExtension) +
                ", generate_activity=" + db.escape(req.fields.generateActivity) +
                ", reminder_activity_1=" + db.escape(req.fields.reminderActivity1) +
                ", reminder_date_1=" + (req.fields.reminderDate1 ? db.escape(req.fields.reminderDate1) : "'0000-00-00 00:00:00'") +
                ", reminder_on_day_1=" + db.escape(req.fields.reminderOnDay1) +
                ", reminder_activity_2=" + db.escape(req.fields.reminderActivity2) +
                ", reminder_date_2=" + (req.fields.reminderDate2 ? db.escape(req.fields.reminderDate2) : "'0000-00-00 00:00:00'") +
                ", reminder_on_day_2=" + db.escape(req.fields.reminderOnDay2) +
                " WHERE id=" + db.escape(parseInt(req.fields.id));
            } else {
                res.status(403).json({"code": -1, error: "Category ID is missing"});
            }

            db.query_err(q, function (err, result) {
                if (err) {
                    res.status(400).json({code: -1, error: err.message, write: allowed.write});
                    return;
                }

                res.json({"code": 0, "write": allowed.write});
            });
        }
    });
}

function deleteCode(req, res) {
    web.checkPermission("marketing", req.gid, function (allowed) {
        if (!allowed || !allowed.write) {
            res.status(403).json({"code": -1});
        } else {
            var code = req.fields.code;
            var q = "DELETE FROM promoCodes WHERE code=" + db.escape(code);
            db.query_err(q, function (err) {
                if (err) {
                    return res.status(400).json({
                        code: -1,
                        error: err.message,
                        write: allowed.write
                    });
                }
                res.json({
                    "code": 0,
                    "write": allowed.write
                });
            });
        }
    });
}

function copyCode(req, res) {
    web.checkPermission("marketing", req.gid, function (allowed) {
        if (!allowed || !allowed.write) {
            res.status(403).json({"code": -1});
        } else {
            var categoryId = parseInt(req.fields.categoryId);
            var code = req.fields.code;
            var codeL1 = req.fields.codeL1;
            var codeL2 = req.fields.codeL2;
            var prefix = req.fields.prefix;
            var codeValidDays = req.fields.codeValidDays;
            var randomLength = parseInt(req.fields.randomLength);
            var count = parseInt(req.fields.count);

            var patterns = [];
            if (code) patterns.push(code);
            if (codeL1) patterns.push(codeL1);
            if (codeL2) patterns.push(codeL2);

            var tasks = [];
            for (var i = 0; i < count; i++) {
                tasks.push(function (callback) {
                    promotionsManager.generatePromoFromPattern(categoryId, codeValidDays, prefix, patterns,
                        randomLength, callback);
                });
            }

            async.parallelLimit(tasks, 1, function (err, result) {
                if (err) {
                    return res.status(400).json({
                        code: -1,
                        error: err.message,
                        write: allowed.write
                    });
                }

                res.json({
                    "code": 0,
                    "write": allowed.write
                });
            });
        }
    });
}

function deleteLog(req, res) {
    web.checkPermission("marketing", req.gid, function (allowed) {
        if (!allowed || !allowed.write) {
            res.status(403).json({"code": -1});
        } else {
            var id = req.fields.id;
            var q = "DELETE FROM promoCodesLogs WHERE id=" + db.escape(id);
            db.query_err(q, function (err) {
                if (err) {
                    return res.status(400).json({
                        code: -1,
                        error: err.message,
                        write: allowed.write
                    });
                }
                res.json({
                    "code": 0,
                    "write": allowed.write
                });
            });
        }
    });
}

function deleteCategory(req, res) {
    web.checkPermission("marketing", req.gid, function (allowed) {
        if (!allowed || !allowed.write) {
            res.status(403).json({"code": -1});
        } else {
            var category = req.fields.id;
            var q = "DELETE FROM promoCodes WHERE category=" + db.escape(category);
            db.query_err(q, function (err) {
                if (err) {
                    return res.status(400).json({
                        code: -1,
                        error: err.message,
                        write: allowed.write
                    });
                }

                var q = "DELETE FROM promoCodesCategories WHERE id=" + db.escape(category);
                db.query_err(q, function (err) {
                    if (err) {
                        return res.status(400).json({
                            code: -1,
                            error: err.message,
                            write: allowed.write
                        });
                    }
                    res.json({
                        "code": 0,
                        "write": allowed.write
                    });
                });
            });
        }
    });
}
