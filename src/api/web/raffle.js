var common = require('../../../lib/common');
var config = require('../../../config');
var spawn = require('child_process').spawn;

var LOG = config.LOGSENABLED;
var child;

//exports

exports.post = post;
exports.get = get;

var progress;
var topResult;

//functions
function post(req, res) {
    var len = req.fields && req.fields.len ? parseInt(req.fields.len) : 0;
    var distribute = req.fields && req.fields.distribute ? parseInt(req.fields.distribute) : 0;
    var count = req.fields && req.fields.count ? parseInt(req.fields.count) : 0;
    if (child) child.kill();
    child = spawn("./scripts/random_distribute.js", [ len, distribute, count ]);
    progress = 0;
    topResult = undefined;
    res.json({ "code" : 0 });
    child.stdout.on("data", function (data) {
        var lines = data.toString().split(/(\r?\n)/g);
        lines.forEach(function (line) {
        var parsed = common.safeParse(line);
        if (parsed.progress) progress = parsed.progress;
        else if (parsed.result) topResult = parsed.result;
        });
    });
}

function get(req, res) {
    var reply = topResult ? { "result" : topResult } : { "progress" : progress };
    reply.code = 0;
    res.json(reply);
}
