var config = require('../../../config');
var db = require('../../../lib/db_handler');
var web = require('./web');

var logsEnabled = config.LOGSENABLED;

//exports

exports.getAll = getAll;

//functions

function getAll(req, res) {
	web.checkPermission("marketing", req.gid, function(allowed) {
		if (!allowed) {
			res.status(403).json({ "code" : -1 });
		} else {
			var q = "SELECT name, email, phone, end_date, message FROM getsitecontrol";
			db.query_noerr(q, function(rows) {
				if (rows && rows[0]) {
					var list = new Array;
					rows.forEach(function (row) {
						var item = new Object;
						item.name = row.name;
						item.email = row.email;
						item.phone = row.phone;
						item.end_date = row.end_date;
						item.message = row.message;
						list.push(item);
					});
					res.json({ "code" : 0, "list" : list});
				} else {
					res.json({ "code" : -1});
				}
			});
		}
	});
}
