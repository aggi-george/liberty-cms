var analyticsManager = require('../../core/manager/analytics/analyticsManager');
var common = require('../../../lib/common');
var web = require('./web');

//exports

exports.loadStats = loadStats;

//functions

function loadStats(req, res) {
    web.checkPermission("customers", req.gid, function (allowed) {
        if (!allowed) {
            res.status(403).json({"code": -1});
        } else {
            var category = req.params.category;
            var start = req.query.start ? new Date(req.query.start) : new Date(new Date().getTime() - 24 * 60 * 60 * 1000);
            var end = req.query.end ? new Date(req.query.end) : new Date(new Date().getTime());
            var points = req.query.points ? parseInt(req.query.points) : 30;
            var host = req.query.host;
            var method = req.query.method;

            if (category === 'ec') {
                analyticsManager.loadEcStats(start, end, points, host, method, function (err, result) {
                    if (err) {
                        return res.status(400).json({
                            "code": -1,
                            "error": err.message
                        });
                    }

                    res.json({
                        code: 0,
                        write: allowed.write,
                        data: result
                    });
                });
            } else {
                res.status(400).json({"code": -1, error: "Category is not supported"});
            }
        }
    });
}
