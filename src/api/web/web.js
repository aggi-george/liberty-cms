var md5 = require('MD5');
var config = require('../../../config');
var common = require('../../../lib/common');
var db = require('../../../lib/db_handler');

var notifications = require('./notifications');
var sitecontrol = require('./sitecontrol');
var cdr = require('./cdr');
var bi = require('./bi');
var packages = require('./packages');
var customers = require('./customers');
var admin = require('./admin');
var contents = require('./contents');
var profile = require('./profile');
var dashboard = require('./dashboard');
var bill = require('./bill');
var configmap = require('./configmap');
var promo = require('./promo');
var analytics = require('./analytics');
var invoices = require('./invoices');
var partners = require('./partners');
var logs = require('./logs');
var stats = require('./stats');
var porting = require('./porting');
var raffle = require('./raffle');
var files = require('./files');
var locations = require('./locations');

var menu = require('./menu');
var inbound = require('./inbound');
var gentwo = require('./gentwo');
var noc = require('./noc');

var notificationsAPI = require('../internal/notifications');
var packagesAPI = require('../internal/packages');
var contentsAPI = require('./../internal/contentApi');
var iccids = require('./iccids');
var numbers = require('./numbers');

var postFieldsLimit = 50;
var putFieldsLimit = 50;
var deleteFieldsLimit = 50;
var logsEnabled = config.LOGSENABLED;

//exports

exports.init = init;
exports.checkSession = checkSession;
exports.checkPermission = checkPermission;
exports.checkApiKey = checkApiKey;
exports.checkApiKeyForTeams = checkApiKeyForTeams;


//functions

function init(baseApi, category, webs) {
    var path = baseApi + category;

    // packages - upload asset	-- special case, on top
    var usagePath = path + '/packages';
    webs.put(usagePath + '/upload/asset/:id', parsePutFiles);
    webs.put(usagePath + '/upload/asset/:id', packages.uploadAsset);

    // contents - upload		-- special case, on top
    var usagePath = path + '/contents';
    webs.put(usagePath + '/upload/:cat/:type', parsePutFiles);
    webs.put(usagePath + '/upload/:cat/:type', contents.upload);

    // notifications - upload asset	-- special case, on top
    var usagePath = path + '/notifications';
    webs.put(usagePath + '/upload/asset/:activity', parsePutFiles);
    webs.put(usagePath + '/upload/asset/:activity', notifications.uploadAsset);
    webs.put(usagePath + '/upload/template', parsePutFiles);
    webs.put(usagePath + '/upload/template', notifications.uploadTemplate);

    //Parse params
    webs.post(path + '/*', parsePost);
    webs.put(path + '/*', parsePutFiles);
    webs.delete(path + '/*', parseDelete);

    function authCheck(type, perms, req, res, callback) {    // perms = { read : x, write : x }
        checkPermission(type, req.gid, function (allowed) {
            if ( !allowed ||
                    (allowed.write < perms.write) ||
                    (allowed.read < perms.read) ) {
                res.status(403).json({"code": -1});
            } else {
                callback(req, res, allowed);
            }
        });
    }

    // menu module
    menu.init(webs, path + '/menu');

    // inbound module
    inbound.init(authCheck, webs, path + '/inbound');

    // gentwo module
    gentwo.init(authCheck, webs, path + '/gentwo');

    // noc module
    noc.init(authCheck, webs, path + '/noc');

    // web - get API
    var usagePath = path + '/api';
    webs.get(usagePath + '/get/:group', getAPI);

    // web - auth API
    var usagePath = path + '/auth';
    webs.post(usagePath + '/login', auth);

    // notifications - get all API
    var usagePath = path + '/notifications';
    webs.get(usagePath + '/get/notification/:activity', notifications.getNotification);

    // notifications - get all API
    webs.get(usagePath + '/get/all', notifications.getAll);

    // notifications - send notification
    webs.post(usagePath + '/test/send/:activity', notifications.sendNotification);

    // notifications - update notification
    webs.put(usagePath + '/update/notification/:activity', notifications.updateNotification);

    // notifications - update team details
    webs.put(usagePath + '/update/team/:activity', notifications.updateTeam);

    // notifications - update team details
    webs.put(usagePath + '/update/group/:activity', notifications.updateGroup);

    // notifications - update transport details
    webs.put(usagePath + '/update/transport/:activity', notifications.updateTransport);

    // notifications - update transport details
    webs.put(usagePath + '/update/restriction/:activity', notifications.updateRestriction);

    // notifications - submit new
    webs.put(usagePath + '/add/new/:activity', notifications.addNew);

    // notifications - delete notification
    webs.delete(usagePath + '/delete/notification/:activity', notifications.deleteNotification);

    // notifications - get notifications logs
    webs.get(usagePath + '/get/logs', notifications.getNotificationsLogs);

    // notifications - delete asset
    webs.delete(usagePath + '/delete/asset/:activity', notifications.deleteAsset);

    // notifications - delete log
    webs.delete(usagePath + '/delete/log/:id', notifications.deleteLog);

    var usagePath = path + '/iccids';
    //iccids
    webs.get(usagePath + '/get/iccids', iccids.getIccids);
    webs.put(usagePath + '/put/iccids', iccids.putIccids);

    var usagePath = path + '/numbers';
    //numbers
    webs.get(usagePath + '/get/numbers', numbers.getNumbers);
    webs.put(usagePath + '/put/numbers', numbers.putNumbers);
    webs.delete(usagePath + '/delete/number', numbers.deleteNumber);

    var usagePath = path + '/sitecontrol';
    webs.get(usagePath + '/get/sitecontrol', sitecontrol.getAll);

    var usagePath = path + '/cdr';
    webs.get(usagePath + '/get/:type', cdr.getCDR);

    var usagePath = path + '/bi';
    webs.get(usagePath + '/get/permissions', bi.getPermissions);
    webs.get(usagePath + '/get/allStats', bi.getAllStats);
    webs.get(usagePath + '/get/generateKpiValues', bi.generateKpiValuesFromApi);
    webs.get(usagePath + '/get/regenerate', bi.regenerate);
    webs.get(usagePath + '/get/rawQuery', bi.rawQuery);
    webs.get(usagePath + '/get/kpiDefinitions', bi.getKpiDefinitions);
    webs.get(usagePath + '/get/kpiGenerators', bi.getKpiGenerators);
    webs.get(usagePath + '/get/kpiGenKeys', bi.getKpiGenKeys);
    webs.get(usagePath + '/get/kpiViews', bi.getKpiViews);
    webs.get(usagePath + '/get/kpiViewsWithValues', bi.getKpiViewsWithValues);
    webs.get(usagePath + '/get/listKpiViewItems', bi.listKpiViewItems);
    webs.put(usagePath + '/put/kpiDefinition', bi.saveKpiDefinition);
    webs.put(usagePath + '/put/kpiGenerator', bi.saveKpiGenerator);
    webs.put(usagePath + '/put/kpiView', bi.saveKpiView);
    webs.put(usagePath + '/put/kpiToView', bi.savekpiToView);
    webs.put(usagePath + '/put/kpiValue', bi.manualAddKpiValue);
    webs.delete(usagePath + '/delete/kpiDefinition', bi.deleteKpiDefinition);
    webs.delete(usagePath + '/delete/kpiGenerator', bi.deleteKpiGenerator);
    webs.delete(usagePath + '/delete/kpiView', bi.deleteKpiView);
    webs.delete(usagePath + '/delete/kpiGenFromView', bi.deleteKpiGenFromView);

    // config - list
    var configPath = path + '/configmap';
    webs.get(configPath + '/list', configmap.getConfigMapList);

    // config - update
    webs.put(configPath + '/update', configmap.updateConfigItem);

    // packages - get business hierarchy API
    var usagePath = path + '/packages';
    webs.get(usagePath + '/business/hierarchy', packages.businessHierarchy);

    // packages - get package details
    webs.get(usagePath + '/get/package/:id', packages.getPackage);

    // packages - update package details
    webs.put(usagePath + '/update/package/:id', packages.updatePackage);

    // packages - update team details
    webs.put(usagePath + '/update/team/:id', packages.updateTeam);

    // packages - delete asset
    webs.delete(usagePath + '/delete/asset/:id', packages.deleteAsset);

    // packages - get boosts logs
    webs.get(usagePath + '/get/boosts/logs', packages.getBoostsLogs);

    // packages - get addon logs
    webs.get(usagePath + '/get/addon/logs', packages.getAddonLogs);

    // customers - get mobile
    var usagePath = path + '/customers';
    webs.get(usagePath + '/search/:type', customers.search);

    webs.get(usagePath + '/bills', customers.searchBills);

    webs.get(usagePath + '/doc/:id', customers.getDoc);

    webs.put(usagePath + '/consume/data/:account/:serviceInstance', customers.consumeData);

    webs.get(usagePath + '/link/update/cc/:account/:serviceInstance', customers.createUpdateCCLink);

    webs.get(usagePath + '/statusChangeSchedules/:serviceInstanceNumber', customers.getCustomerStatusSchedules);

    // customers - payment
    webs.put(usagePath + '/bill/payment/:account/:serviceInstance', customers.makePayment);

    // customers - update roaming 
    webs.put(usagePath + '/update/roaming/:account/:serviceInstance', customers.updateRoaming);

    // customers - update roaming cap
    webs.put(usagePath + '/update/cap/roaming/:account/:serviceInstance', customers.updateRoamingCap);

    // customers - addon subscribe
    webs.put(usagePath + '/update/addon/selfcare/:account/:serviceInstance', customers.updateAddon);

    // customers - delete general addon
    webs.delete(usagePath + '/update/addon/selfcare/:account/:serviceInstance', customers.updateAddon);

    // customers - add free boost
    webs.put(usagePath + '/update/addon/free/boost/:account/:serviceInstance', customers.updateFreeBoost);

    // customers - add loyalty bonus
    webs.put(usagePath + '/update/addon/add/bonus/loyalty/:account/:serviceInstance', customers.addLoyaltyBonus);

    // customers - add contract buster bonus
    webs.put(usagePath + '/update/addon/add/bonus/contractbuster/:account/:serviceInstance', customers.addContractBusterBonus);

    // customers - add circles care installation bonus
    webs.put(usagePath + '/update/addon/add/bonus/careinstallation/:account/:serviceInstance', customers.addCareinstallation);

    // customers - add pending bonuses
    webs.put(usagePath + '/update/addon/add/bonus/pending/:account/:serviceInstance', customers.addPendingBonuses);

    // customers - add circles care installation bonus
    webs.put(usagePath + '/update/addon/add/bonus/portin/:account/:serviceInstance', customers.addPortInBonus);

    // customers - add birthday bonus
    webs.put(usagePath + '/update/addon/add/bonus/birthday/:account/:serviceInstance', customers.addBirthdayBonus);

    // customers - add birthday bonus
    webs.put(usagePath + '/update/addon/add/bonus/winback/:account/:serviceInstance', customers.addWinbackBonus);

    // customers - use available bonus
    webs.put(usagePath + '/update/addon/use/bonus/:account/:serviceInstance', customers.useAvailableBonus);

    // customers - use re-subscribe addons
    webs.put(usagePath + '/update/addon/resubscribe/:account/:serviceInstance', customers.resubscribeAddons);

    // customers - use re-subscribe addons
    webs.get(usagePath + '/addon/exist/:prefix/:number/:productId', customers.addonExist);

    // customers - add autoboost
    webs.put(usagePath + '/update/addon/add/autoboost/:account/:serviceInstance', customers.addAutoBoost);

    // customers - update monkey
    webs.put(usagePath + '/update/monkey/:account/:serviceInstance', customers.updateMonkey);

    // customers - update monkey
    webs.put(usagePath + '/update/dob/:account', customers.updateDOB);

    // customers - delete apps via app manager
    webs.delete(usagePath + '/delete/app', customers.removeAppRegistration);

    // customers - recache
    webs.put(usagePath + '/update/cache/:account/:serviceInstance', customers.recache);

    // customers - recache
    webs.post(usagePath + '/pay200', customers.pay200);

    // customers - change status
    webs.put(usagePath + '/changestatus/:account/:serviceInstance', customers.changeStatus);

    // customers - change status
    webs.put(usagePath + '/validatestatus/:account/:serviceInstance', customers.validateStatus);

    // customers - repair
    webs.put(usagePath + '/repair/:account/:serviceInstance', customers.repair);

    // customers - confirm sim damage
    webs.put(usagePath + '/simDamageConfirm', customers.simDamageConfirm);

    // customers - port in operations
    webs.put(usagePath + '/portin/action/:account/:serviceInstance', customers.portInAction);

    // customers - port in operations
    webs.put(usagePath + '/portin/corporate', customers.corporatePortinAction);

    // customers - port in operations
    webs.put(usagePath + '/portin/report/upload', customers.portInReportUpload);

    // customers - port in operations
    webs.put(usagePath + '/portin/notification/:account/:serviceInstance', customers.portInNotification);

    // customers - get all schedules
    webs.get(usagePath + '/get/schedules/all', customers.getSchedules);

    // customers - add schedules
    webs.put(usagePath + '/add/schedules', customers.addSchedules);

    // customers - move schedules
    webs.post(usagePath + '/move/schedules/:id', customers.moveSchedules);

    // customers - delete schedules
    webs.delete(usagePath + '/delete/schedules/:id', customers.deleteSchedules);

    // customers - delete schedules
    webs.put(usagePath + '/execute/schedules/:id', customers.executeSchedules);

    // customers - number check
    webs.get(usagePath + '/get/number/availability/:prefix/:number', customers.checkNumberAvailable);

    // customers - number check
    webs.put(usagePath + '/update/number/release/:prefix/:number', customers.releaseNumber);

    // customers - update details
    webs.put(usagePath + '/update/details/:account/:serviceInstance', customers.updateDetails);

    // customers - update details
    webs.put(usagePath + '/update/profiles/settings/:account/:serviceInstance', customers.updateProfileSettings);

    // customers - cdr
    webs.get(usagePath + '/get/cdr/:prefix/:number', customers.getCDR);

    // customers - push
    webs.put(usagePath + '/apps/push', customers.testPush);

    // customers - add credit
    webs.put(usagePath + '/apps/gentwo/manage', customers.manageGentwoAPI);

    // customers - wave amount
    webs.put(usagePath + '/bill/add/credit/notes/:account/:serviceInstance', customers.addCreditNotes);

    // customers - code
    webs.put(usagePath + '/referral/use', customers.useReferralCode);

    // customers - code
    webs.put(usagePath + '/referrer/bonus/deactivate/', customers.deactivateReferrerBonuses);

    // customers - code
    webs.delete(usagePath + '/referral', customers.deleteReferralCode);

    // customers - delete bonus history
    webs.delete(usagePath + '/bonus/history', customers.deleteBonusHistory);

    // customers - add leaderboard golden ticket
    webs.put(usagePath + '/leaderboard/goldentickets/:account/:serviceInstance', customers.addGoldenTicket);

    // customers - delete leaderboard golden ticket
    webs.delete(usagePath + '/leaderboard/goldentickets', customers.deleteGoldenTicket);

    // customers - notification block
    webs.put(usagePath + '/notifications/block/:account/:serviceInstance', customers.blockNotif);

    // customers - notification override
    webs.put(usagePath + '/notifications/override/:account/:serviceInstance', customers.overrideNotif);

    // customers - send test sms
    webs.put(usagePath + '/notifications/test/sms/:prefix/:number', customers.smsTest);

    // customers - get available iccid
    webs.get(usagePath + '/get/available/iccid', customers.getFreeICCID);

    // customers - send ecomm notification
    webs.put(usagePath + '/ecomm/notification/:account/:serviceInstance', customers.ecommNotification);

    // admin - get users
    var usagePath = path + '/admin';
    webs.get(usagePath + '/get/users', admin.getUsersGroups);

    // admin - add User
    webs.put(usagePath + '/add/user/:group', admin.addUser);

    // admin - delete User
    webs.delete(usagePath + '/delete/user/:email', admin.deleteUser);

    // admin - update User
    webs.put(usagePath + '/update/user/:type/:name/:id', admin.updateUser);

    // admin - add Group
    webs.put(usagePath + '/add/group/:group', admin.addGroup);

    // admin - update Group Name
    webs.put(usagePath + '/update/group/name/:group/:name', admin.updateName);

    // admin - update Group Page
    webs.put(usagePath + '/update/group/page/:group/:page', admin.updatePage);

    // admin - update Group Perms
    webs.put(usagePath + '/update/group/perms/:group/:perm/:checked', admin.updateGroup);

    // admin - get access logs
    webs.get(usagePath + '/get/logs', admin.getAccessLogs);

    // admin - get banned email
    webs.get(usagePath + '/emails/banned/get', admin.getBannedEmails);

    // admin - remove banned email
    webs.delete(usagePath + '/emails/banned/:email', admin.deleteBannedEmail);

    // admin - add banned email
    webs.put(usagePath + '/emails/banned/:email', admin.addBannedEmail);

    var usagePath = path + '/promo';

    // promo - get products
    webs.get(usagePath + '/get/products', promo.getProducts);

    // promo - add history
    webs.put(usagePath + '/add/history/:account/:serviceInstanceNumber', promo.addBonusHistory);

    // promo - get categories
    webs.get(usagePath + '/get/categories', promo.getCategories);

    // promo - get codes
    webs.get(usagePath + '/get/codes/:categoryId', promo.getCodes);

    // promo - get code usage logs
    webs.get(usagePath + '/get/logs/:categoryId/:code', promo.getCodesLogs);

    // promo - add code
    webs.put(usagePath + '/add/code', promo.addCode);

    // promo - delete code
    webs.delete(usagePath + '/delete/code', promo.deleteCode);

    // promo - copy code
    webs.put(usagePath + '/copy/code', promo.copyCode);

    // promo - delete log
    webs.delete(usagePath + '/delete/log', promo.deleteLog);

    // promo - add category
    webs.put(usagePath + '/add/category', promo.addCategory);

    // promo - add category web page
    webs.put(usagePath + '/add/category/page', promo.addCategoryPage);

    // promo - delete category
    webs.delete(usagePath + '/delete/category', promo.deleteCategory);

    // partner - add external customers
    var partnersPath = path + '/partners';
    webs.put(partnersPath + '/customers/add', partners.addPartnersCustomers);

    // partner - load external customers
    webs.get(partnersPath + '/customers', partners.loadPartnersCustomers);

    // partner - delete external customers
    webs.delete(partnersPath + '/customers/delete/:id', partners.deletePartnersCustomers);

    // contents - get
    var usagePath = path + '/contents';
    webs.get(usagePath + '/get/:cat/:type', contents.get);

    // contents - submit new
    webs.put(usagePath + '/add/new/:cat', contents.addNew);

    // contents - delete faq
    webs.delete(usagePath + '/delete/faq/:id', contents.deleteQuestion);

    // contents - update faq
    webs.put(usagePath + '/update/faq/:type/:id', contents.updateFAQ);

    // profile - update
    var usagePath = path + '/profile';
    webs.put(usagePath + '/update/:type', profile.update);

    // dashboard - get
    var usagePath = path + '/dashboard';
    webs.get(usagePath + '/get/:type', dashboard.get);
    webs.put(usagePath + '/put/:type', dashboard.put);

    // bill - api
    var billPath = path + '/bill';

    // bill - broadcasting status
    webs.get(billPath + '/broadcasting/status', bill.getBillsBroadcastingStatus);

    // bill - archive
    webs.get(billPath + '/archive', bill.getBillsArchive);

    // bill - list
    webs.get(billPath + '/list', bill.getBillsList);

    // bill - broadcast all
    webs.post(billPath + '/send/all', bill.sendAllBills);

    // bill - send single
    webs.post(billPath + '/send/single', bill.sendSingleBill);

    // bill - logs
    webs.get(billPath + '/logs', bill.getBillLogs);

    // bill - file
    webs.get(billPath + '/file/:type/:archive/:pdf', bill.getBillFile);

    // bill - send single
    webs.post(billPath + '/send/single', bill.sendSingleBill);

    // bill - block accounts from displaying the last bill
    webs.put(billPath + '/last/blocked/add', bill.addBlockedBillingAccount);

    // bill - get blocked from displaying last bill accounts
    webs.get(billPath + '/last/blocked', bill.loadBlockedBillingAccount);

    // bill - clean blocked from displaying last bill accounts
    webs.delete(billPath + '/last/blocked', bill.cleanBlockedBillingAccount);

    /**
     * Logs
     */

    var logPath = path + '/logs';

    // logs - load any type of logs
    webs.get(logPath + '/:category/:type', logs.loadLogs);

    /**
     * Stats
     */

    var logPath = path + '/stats';

    // logs - load any type of logs
    webs.get(logPath + '/:category', stats.loadStats);

    /**
     * Port In
     */

    var portIn = path + '/portin';
    var portOut = path + '/portout';

    // port in/out - load requests
    webs.get(portIn + '/requests', porting.getPortedInRequests);
    webs.get(portOut + '/requests', porting.getPortedOutRequests);

    webs.get(portIn + '/verify/status', porting.getPortedInStatus);

    webs.put(portIn + '/move/status', porting.moveToCorrectStatus);

    // port in - load requests with warnings
    webs.get(portIn + '/requests/warnings/:status', porting.getPortedInRequestsWithWarnings);

    // port in - load requests with warnings
    webs.get(portIn + '/requests/report/statuses', porting.getPortedInReport);

    // port in - load requests with warnings
    webs.get(portIn + '/requests/report/all/statuses', porting.getPortedInReportAll);

    // port in - update request status
    webs.put(portIn + '/request/update/status', porting.updatePortedInRequest);

    // customers - port in operations
    webs.put(portIn + '/request/update/owner', porting.updatePortedInOwner);

    // customers - activate port in
    webs.put(portIn + '/request/activate', porting.activatePortInRequest);

    // customers - port in operations
    webs.put(portIn + '/request/recover/:type', porting.recoverReTryPortIn);

    // raffle
    var usagePath = path + '/raffle';
    webs.post(usagePath, raffle.post);
    webs.get(usagePath, raffle.get);

    /**
     * Analytics
     */

    var analyticsPath = path + '/analytics';

    // promo - all classes
    webs.get(analyticsPath + '/get/classes', analytics.getClasses);

    // promo - classes stats
    webs.get(analyticsPath + '/get/classes/stats/:type', analytics.getClassesStats);

    // promo - all customers
    webs.get(analyticsPath + '/get/customers/:classId', analytics.getCustomers);

    // promo - get notifications ids
    webs.get(analyticsPath + '/get/notification/ids/:query', analytics.getNotificationIds);

    // promo - send notifications
    webs.put(analyticsPath + '/classes/send/notification', analytics.classesSendNotification);

    // promo - send notifications
    webs.get(analyticsPath + '/classes/send/notification/progress', analytics.sendNotificationsProgress);

    // promo - resync classes
    webs.put(analyticsPath + '/classes/resync', analytics.classifyCustomers);

    // promo - resync classes
    webs.get(analyticsPath + '/classes/resync/progress', analytics.classifyCustomersProgress);

    /**
     * Invoices
     */

    var analyticsPath = path + '/invoices';

    // invoices - all month
    webs.get(analyticsPath + '/get/months', invoices.getMonths);

    // invoices - all bills
    webs.get(analyticsPath + '/get/invoices', invoices.getBills);

    // invoices - all bills
    webs.get(analyticsPath + '/get/invoices/:monthId', invoices.getBills);

    // invoices - all bills
    webs.get(analyticsPath + '/get/file', invoices.getFile);

    // invoices - all log status
    webs.get(analyticsPath + '/get/sync/logs', invoices.getSyncLogs);

    // invoices - all log status
    webs.get(analyticsPath + '/get/report', invoices.getReport);

    // invoices - all log status
    webs.get(analyticsPath + '/get/report/:monthId', invoices.getReport);

    // invoices - sync bills
    webs.put(analyticsPath + '/sync', invoices.syncInvoices);

    // invoices - sync unpaid bills
    webs.put(analyticsPath + '/unpaid/sync', invoices.syncUnpaidInvoices);

    // invoices - get sync bills progress
    webs.get(analyticsPath + '/sync/progress', invoices.syncInvoicesProgress);

    // invoices - sync bills payments
    webs.put(analyticsPath + '/payment/update', invoices.updatePayments);

    // invoices - send non-payment reminder
    webs.put(analyticsPath + '/payment/run/batch', invoices.runBatchPayment);

    // invoices - send all bills pdf
    webs.put(analyticsPath + '/send/bill/all', invoices.sendBillsAll);

    // invoices - send single bill pdf
    webs.put(analyticsPath + '/send/bill/single', invoices.sendBillSingle);

    // invoices - send non-payment reminder
    webs.put(analyticsPath + '/send/reminder/nonpayment', invoices.sendReminderNonPayment);


    var transactionPath = path + '/transactions';

    // transaction - load
    webs.get(transactionPath + '/get', invoices.getTransactions);

    // transaction - recover
    webs.put(transactionPath + '/recover', invoices.recoverTransaction);

    // transaction - recover
    webs.get(transactionPath + '/status', invoices.loadTransactionStatus);

    /**
     * Upload files
     */

    webs.put(path + '/file/:collectionType', files.uploadFile);

    webs.get(path + '/file/:collectionType/:fileId', files.getFile);

    /**
     * Geolocations
     */

    webs.get(path + '/locations/customersForProviders', locations.getCustomersForProviders);

    webs.get(path + '/locations/customersForGeoLocations', locations.getCustomersForGeoLocations);

    webs.get(path + '/locations/redeemedCustomersForProviders', locations.getRedeemedCustomersForProviders);
    
    webs.get(path + '/locations/redeemedCustomersForGeoLocations', locations.getRedeemedCustomersForGeoLocations);

    webs.get(path + '/locations/providers', locations.getProviders);

    webs.get(path + '/locations/points/:providerId', locations.getPoints);

    webs.get(path + '/locations/logs/:providerId/:pointId', locations.getLogs);

    webs.get(path + '/locations/promo/:providerId', locations.getPromos);

    webs.put(path + '/locations/provider', locations.putProvider);

    webs.put(path + '/locations/point', locations.putPoint);

    webs.put(path + '/locations/promo/redeem', locations.putRedeemPromo);

    webs.delete(path + '/locations/provider', locations.deleteProvider);

    webs.delete(path + '/locations/point', locations.deletePoint);

    webs.delete(path + '/locations/promo', locations.deletePromo);
}

function parsePost(req, res, next) {
    common.formParse(req, res, postFieldsLimit, function (fields) {
        req.fields = fields;
        next();
    });
}

function parsePut(req, res, next) {
    common.formParse(req, res, putFieldsLimit, function (fields) {
        req.fields = fields;
        next();
    });
}

function parseDelete(req, res, next) {
    common.formParse(req, res, deleteFieldsLimit, function (fields) {
        req.fields = fields;
        next();
    });
}

function parsePutFiles(req, res, next) {
    common.formParseFiles(req, res, putFieldsLimit, function (fields, files) {
        req.fields = fields;
        req.files = files;
        next();
    });
}

function auth(req, res) {
    var user = req.fields.user.split('@')[0];
    var realm = req.fields.user.split('@')[1];
    var q = "SELECT a.groupID, g.defaultPage, a.renew FROM admin a, groups g WHERE a.enabled=1 AND a.groupID=g.id AND a.email="
        + db.escape(req.fields.user) + " AND a.password=MD5(" + db.escape(user + ":" + realm + ":" + req.fields.secret) + ")";
    db.query_noerr(q, function (rows) {
        if (rows && rows[0]) {
            if (req.fields.update) {
                var q_update = "UPDATE admin SET password=MD5(" + db.escape(user + ":" + realm + ":" + req.fields.update) + "), renew=0 WHERE password=MD5(" + db.escape(user + ":" + realm + ":" + req.fields.secret) + ") AND email=" + db.escape(req.fields.user);
                db.query_noerr(q_update, function (rows) {
                });
            } else if (rows[0].renew) {
                res.json({
                    "code": 1,
                    "user": req.fields.user
                });
                db.weblog.insert({
                    "ts": new Date().getTime(),
                    "ip": req.ip,
                    "type": "login",
                    "username": common.escapeHtml(req.fields.user),
                    "code": 1
                });
                return false;
            }
            var session_key = md5(req.fields.key + ":" + new Date());
            db.cache_put("session_cache", session_key, req.fields.user + "|" + rows[0].groupID, 8 * 60 * 60 * 1000);	// 8 hours

            //Setting cookies
            let cookieExpiryDate = new Date();
            cookieExpiryDate.setTime(cookieExpiryDate.getTime() + (8 * 60 * 60 * 1000));

            let cookieOptions = {
                expires: cookieExpiryDate,
                secure:true,
                httpOnly:true
            }

            res.cookie("cmscookie", session_key, cookieOptions);
            res.cookie("cmsuser",req.fields.user, cookieOptions);
            res.cookie("cmsgid", rows[0].groupID, cookieOptions);

            res.json({
                "code": 0,
                "user": req.fields.user,
                "default": rows[0].defaultPage + ".html"
            });
            db.weblog.insert({
                "ts": new Date().getTime(),
                "ip": req.ip,
                "type": "login",
                "username": common.escapeHtml(req.fields.user),
                "code": 0
            });
        } else {
            res.json({"code": -1, "user": req.fields.user});
            db.weblog.insert({
                "ts": new Date().getTime(),
                "ip": req.ip,
                "type": "login",
                "username": common.escapeHtml(req.fields.user),
                "code": -1
            });
        }
    });
}

function getAPI(req, res) {
    var api = new Array;
    if (req.params.group == "packages") api = packagesAPI.getAPI();
    else if (req.params.group == "notifications") api = notificationsAPI.getAPI();
    else if (req.params.group == "contents") api = contentsAPI.getAPI();
    res.json({"code": 0, "list": api});
}

function checkSession(cookie, user, callback) {
    db.cache_get("session_cache", cookie, function (value) {
        if (value && value.indexOf(user) > -1) {
            callback(value.split("|")[1]);
        } else {
            callback(false);
        }
    });
}

function checkPermission(page, gid, callback) {
    var q = "SELECT p.page, p.get, p.put FROM groupsPermissions gp, permissions p WHERE p.id=gp.permissionID AND p.page LIKE " + db.escape(page + "%") + " AND gp.groupID=" + db.escape(gid) + " ORDER BY p.page DESC LIMIT 1";
    db.query_noerr(q, function (rows) {
        if (rows && rows[0] && rows[0].get) {
            var write = (rows[0].page.indexOf("|") > -1) ? rows[0].page.split("|").length - 1 : rows[0].put;
            callback({"read": rows[0].get, "write": write});
        } else callback(false);
    });
}

function checkApiKey(key, callback) {
    var q = "SELECT id FROM teams WHERE apiKey=" + db.escape(key);
	db.query_noerr(q, function(result) {
		if ( result && result[0] && ((result[0].id == 1) || (result[0].id == 2) || (result[0].id == 5)) ) {
            callback(null, true);
		} else {
			callback(null, false);
		}
	});
}

function checkApiKeyForTeams(key, teams, callback) {
    var teamsStr = teams.join(',');
    var q = "SELECT id FROM teams WHERE apiKey=" + db.escape(key) + " and id in (" + teamsStr + ")";
	db.query_noerr(q, function(result) {
		if ( result && result[0] && ((result[0].id == 1) || (result[0].id == 2) || (result[0].id == 5)) ) {
            callback(null, true);
		} else {
			callback(null, false);
		}
	});
}
