/*globals require */

require('../../../bin/global');

const express = require('express');
const bss = require('../index');
const apiHelper = require('./helper');
const app = express();

let bodyParser = require('body-parser');

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

// parse application/json
app.use(bodyParser.json());
app.use(apiHelper.codeChecker);
app.use(apiHelper.clientErrorHandler);
app.use(apiHelper.errorHandler);
app.use(apiHelper.checkGoToEliteCoreValue);

app.post("/bss",apiHelper.transformRequestPost);
app.post('/bss', (req, res) =>{
    if(!bss[req.query.method]){
        let err = new Error("Method is not found in BSS Index");
        return res.status(500).json({ error: err.message});
    }
    return bss[req.query.method](...req.apiArguements);
});

app.listen(8080, () => console.log('bss middleware server listening on 8080!'));
