/*globals exports, module,require*/

const mConfigGet = require('./method_config');
const bssConfig = require('../config');

class ApiHelper{
    constructor(){
        this.getArguments = this.getArguments.bind(this);
        this.transformRequestPost = this.transformRequestPost.bind(this);
    }

    checkGoToEliteCoreValue(req,res,next){
        try {
            bssConfig.gotoEliteCore = req.query.goToEliteCore !== "false";
            next();
        }catch(ex){
            res.status(500).json({msg:ex.message,stack:ex.stack});
        }
    }

    //Error handler
    errorHandler (err, req, res, next) {
        res.status(500);
        res.send({ error: err })
    }

    //This will check if Code exists as a query string in the request
    codeChecker(req,res,next){
        if(!req.query.method){
            res.status(404);
            res.send("method is missing");
            return;
        }
        if(!req.query.code) {
            res.status(401);
            res.send("Code is  missing");
            return;
        }
        if(req.query.code === 1234){
            next();
        }else{
            res.status(401);
            res.send("Code is incorrect");
        }

    }


    //client side error handler
    clientErrorHandler(err, req, res, next){
        if (req.xhr) {
            res.status(500).send({ error: 'Something failed!' })
        } else {
            next(err);
        }
    }



    //This will return the params required for a request.
    //Based on the methodConfigPost.js
    getArguments(req,res){
        let method = req.query.method;
        let args = [];
        let methodDefinition = mConfigGet[method];
        if(!methodDefinition) throw new Error("Method not found in the method config "+method);

        let paramsInConfig = methodDefinition.params;
        for(let p of paramsInConfig){
            if(p === "callback") args.push(this.apiCallback(req,res));
            else {
                let paramValue = req.body[p];
                if(paramValue !== "") {
                    if (!paramValue) throw new Error("parameter not found in request body " + p);
                }
                args.push(paramValue);

            }
        }
        return args;
    }

    //common callback for all the request.
    apiCallback(req,res){
        return (err,response)=>{
            if(err){
                return res.status(400).json({code: -1, status: err.status, error: err.message});
            }
            else return res.json({code: 0, result: response});
        };
    }

    //Used for post requests
    //Transform a get request by obtaining the request params and passing as arguments to the bss method
    transformRequestPost(req,res,next){
        try {
            req.apiArguements = this.getArguments(req,res);
           next();
        }catch(ex){
            res.status(500).json({msg:ex.message,stack:ex.stack});
        }
    }
}

let apiHelper = new ApiHelper();
module.exports = apiHelper;

