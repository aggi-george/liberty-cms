/*globals module */

module.exports = {

    //region test
    testBRM:{
        params:["callback"]
    },
    //endregion

    //region - Connector
    getProductOfferDetails:{
        params:["callback"]
    },
    getSpecificProdOfferDetails:{
        params:["args","callback"]
    },
    getBusinessHierarchyDetail:{
        params: ["callback"]
    },
    searchInventory:{
        // @param inventoryNumber - Inventory number - e.g. 1
        params: ["inventoryNumber", "callback"]
    },
    searchInventoryList:{
        // @param inventoryStatusId - Inventory status id - e.g. 'NBS01'
        // inventorySubTypeId - Inventory type id - e.g. 'NTP00016'
        params: ["inventoryStatusId", "inventorySubTypeId", "callback"]
    },
    listInventory:{
        // @param serviceInstanceNumber - Service instance number - e.g. 'LW100276'
        params: ["serviceInstanceNumber", "callback"]
    },
    searchAccount:{
        // @param number - mobile or MSISDN Number - e.g. 87420765
        // accountNumber - Account number - e.g. undefined
        // accountName - Account name - e.g. undefined
        // ServiceUserName - Service username - e.g. undefined
        params: ["number", "accountNumber", "accountName", "ServiceUserName", "callback"]
    },
    searchServiceInstance:{
        // @param number - mobile or MSISDN number - e.g. 87420765
        // accountNumber - Account number - e.g. undefined
        // accountName - Account name - e.g. undefined
        params: ["number", "accountNumber", "accountName", "callback"]
    },
    getAccountStatement:{
        // @param accountNumber - Account number - e.g. 'LW100274'
        // fromDate - From date - e.g. '2017-01-01'
        // toDate - To date - e.g. '2017-11-01'
        params: ["accountNumber", "fromDate", "toDate", "callback"]
    },
    searchBill: {
        // @param accountNumber - Account number - e.g. 'LW100274'
        // fromDate - From date - e.g. '2017-01-01'
        // toDate - To date - e.g. '2017-11-01'
        params: ["accountNumber", "fromDate", "toDate", "callback"]
    },
    getBillingAccountDetail: {
        // @param billingAccountNumber - Billing account number - e.g. 'LW100274'
        params: ["billingAccountNumber", "callback"]
    },
    getCustomerHierarchyDetails:{
        // @param accountNumber - Service account number - e.g. 'LW100275'
        params:["accountNumber", "callback"]
    },
    viewCustomer:{
        // @param serviceInstanceNumber - Service account number - e.g. 'LW100276'
        // fromDate : '2017-11-01"
        params:["serviceInstanceNumber","fromDate","callback"]
    },

    subscribeAddOnPackage:{
        params:["args", "callback"]
    },
    unSubscribeAddOnPackage:{
        params:["args", "callback"]
    },
    getFnFMembersDetails:{
        params:["args", "callback"]
    },
    addFnFMembers:{
        params:["args", "callback"]
    },
    removeFnFMembers:{
        params:["args", "callback"]
    },
    modifyPlanAttributes:{
        params:["args", "callback"]
    },
    doInventoryStatusOperation:{
        // @params inventoryNumber - Inventory number
        // operationAlias - Operation alias
        params: ["inventoryNumber", "operationAlias", "callback"]
    },
    repairInventoryRebundle:{
        params:["args", "callback"]
    },
    getInventoryDetail:{
        params:["args", "callback"]
    },
    unpairInventory:{
        params:["args", "callback"]
    },
    repairInventory:{
        params:["args", "callback"]
    },
    repairInventoryXml:{
        params:["args", "callback"]
    },
    removeInventory: {
        // @params serviceInstanceNumber - Service instance number
        // inventoryNumber - Inventory number
        params:["serviceInstanceNumber", "inventoryNumber", "callback"]
    },
    addInventory: {
        // @params effectDate - Effect date
        // inventoryNumber - Inventory number
        // serviceInstanceNumber - Service instance number
        params:["effectDate", "inventoryNumber", "serviceInstanceNumber", "callback"]
    },
    markInventoryAsDamaged: {
        // @params inventoryNumber - Inventory number
        // serviceInstanceNumber - Service instance number
        params:["inventoryNumber", "serviceInstanceNumber", "callback"]
    },
    createInventory: {
        // @params msisdn - MSISDN value
        // donorCode - Donor code
        // date - Date
        params:["msisdn", "donorCode", "date", "callback"]
    },
    updateInventoryAttributes: {
        // @params msisdn - MSISDN value
        // donorCode - Donor code
        // date - Date
        params: ["msisdn", "donorCode", "date", "callback"]
    },
    createAccount: {
        params:["args", "callback"]
    },
    changeServicePlan: {
        // @params billingAccountNumber - Billing account number
        // effectiveDate - Effective date
        // packageName - Package name
        // remarks - Remarks
        // serviceInstanceNumber - Service instance number
        params: ["billingAccountNumber", "effectiveDate", "packageName", "remarks", "serviceInstanceNumber", "callback"]
    },
    addDepositToCustomerBillingAccount: {
        // @params billingAccountNumber - Billing account number
        // reason - Reason
        // depositAmount - Deposit amount
        params: ["billingAccountNumber", "depositAmount", "reason", "callback"]
    },
    updateCustomerAccountData: {
        params:["args", "callback"]
    },
    updateBillingAccountData: {
        params:["args", "callback"]
    },
    updateServiceAccountData: {
        params:["args", "callback"]
    },
    updateBillingAccount: {
        params:["args", "callback"]
    },
    updateBillingAccount_ServiceOperation:{
        params:["args", "callback"]
    },
    updateServiceAccount: {
        params:["args", "callback"]
    },
    changeServiceInstanceStatus: {
        params:["args", "callback"]
    },
    searchCreditNote: {
        params:["args", "callback"]
    },
    makeDebitPaymentDataRequest: {
        // @params accountNumber - Account number
        // transactionAmount - Transaction amount
        // documentNumber - Document number
        // paymentDate - Payment date
        // channelAlias - Channel alias
        params: ["accountNumber", "transactionAmount", "documentNumber", "paymentDate", "channelAlias", "callback"]
    },
    makeAdvancePaymentDataRequest: {
        // @params accountNumber - Account number
        // transactionAmount - Transaction amount
        // paymentDate - Payment date
        // channelAlias - Channel alias
        // description - Description
        params: ["accountNumber", "transactionAmount", "paymentDate", "channelAlias", "description", "callback"]
    },
    searchPayment: {
        params:["args", "callback"]
    },
    createCreditNote: {
        params:["args", "callback"]
    },
    cancelPortInOrder: {
        params: ["MSISDN", "reasonCode", "callback"]
    },
    mnpNotification: {
        params: ["MSISDN", "callback"]
    },
    serviceProvisioningNotification: {
        params: ["externalRefId", "inventoryNumber", "callback"]
    },
    viewCustomerUsageInformation: {
        params: ["args", "callback"]
    },
    updateCustomerBucket: {
        params: ["args", "callback"]
    },

    //endregion

    //region - User
    loadUserInfo: {
        // @param accountNumber - Service account number - e.g. 'LW100275'
        params: ["keyType", "key", "callback", "fresh"]
    },
    loadUserInfoByInternalId: {
        // @param id - 'LW100276'
        // @param fresh : false
        // @param basic : false
        params: ["id", "callback", "fresh", "basic"]

    },
    loadUserInfoByCustomerAccountNumber: {
        // @param can - 'LW100273'
        // @param fresh : false
        // @param basic : false
        params: ["can", "callback", "fresh", "basic"]
    },
    loadUserInfoByBillingAccountNumber: {
        // @param ban - 'LW100274'
        // @param fresh : false
        // @param basic : false
        params: ["ban", "callback", "fresh", "basic"]
    },
    loadUserInfoByServiceAccountNumber: {
        // @param san - 'LW100275'
        // @param fresh : false
        // @param basic : false
        params: ["ban", "callback", "fresh", "basic"]
    },
    loadUserInfoByServiceInstanceNumber: {
        // @param sin - 'LW100275'
        // @param fresh : false
        // @param basic : false
        params: ["sin", "callback", "fresh", "basic"]
    },
    loadUserInfoByPhoneNumber: {
        // @param number - 87420765
        // @param fresh : false
        // @param basic : false
        params: ["number", "callback", "fresh", "basic"]

    },
    loadUserInfoByUserKey: {
        // @param userKey - d3ee3ef094574a1a015714207a72e85b
        // @param fresh : false
        // @param basic : false
        params: ["userKey", "callback", "fresh", "basic"]
    },
    loadAllUsersBaseInfo: {
        // @param options - ex: {serviceInstances: ["LW10027"] }
        params: ["options", "callback"]
    },
    loadActivePackages: {
        // @param serviceInstance - LW10026
        params: ["serviceInstance", "callback"]
    },
    loadCustomerWithPackage: {
        // @param serviceInstance - LW10026
        params: ["serviceInstance", "callback"]
    },
    getIDTypeByCode: {
        //@param code - ex: 1
        params: ["code"]
    },

    clearUserInfo: {
        // @param accountNumber - Service account number - e.g. 'LW100275'
        params: ["accountNumber", "callback"]
    },
    updateUserInfo: {
        // @param accountNumbe - Service account number - e.g. 'LW100275'
        // @param userKey- ex:
        params: ["accountNumber", "callback", "userKey"]
    },
    updateCustomerID: {
        // @param accountNumber
        // @param Type
        // @param Id
        params: ["accountNumber", "Type", "Id", "callback"]
    },
    updateOrderReferenceNumber: {
        // @param accountNumber
        // @param orn
        params: ["accountNumber", "orn", "callback"]
    },
    updateActivationDate: {
        // @param accountNumber
        // @param Datestring
        params: ["accountNumber", "Datestring", "callback"]
    },
    updateCustomerDOB: {
        // @param accountNumber
        // @param birthday
        params: ["accountNumber", "birthday", "callback"]
    },
    updateAllCustomerAccountsDetails: {
        // @param accountNumber
        // @param params
        // @param skipAccount
        // @param skipBilling
        // @param skipService
        params: ["accountNumber", "params", "callback", 'skipAccount', "skipBilling", "skipService"]
    },
    updateCustomerOrderReferenceNumber: {
        // @param accountNumber
        // @param orderReferenceNumber
        params: ["accountNumber", "orderReferenceNumber", "callback"]
    },
    //endregion

    //region - Pacakge

    loadPackagesForTeamApiKey:{

        // @param teamApiKey - Team API Key - e.g. 'sLZ7XNcxrN'
        params: ["teamApiKey", "callback"]

    },

    loadPackages:{
        // @param teamid - Team Id - e.g. 'PRD00440'
        params: ["teamId", "callback"]
    },

    //endregion

    //region - Usage

    loadCurrentCustomerUsage:{
        // @param serviceInstanceNumber - Service instance Number - e.g. 'LW100276'
        //@param fresh - false
        // @param basic -false
        params: ["serviceInstanceNumber", "callback","fresh","basic"]
    },

    loadRemainingDataForPackage:{

        // @param number - Service instance Number - e.g. 'LW100276'
        // @param packageId - Package Id ex: 'PRD00440'
        params: ["number", "packageId","callback"]

    },
    //endregioni

    //region - PortIn

    portInNumber:{

        // @param serviceInstanceNumber - Service instance Number - e.g. 'LW100276'
        // @param tempNumber - Temp Number
        // @param portInNumber - Portin Number
        // @param donor - Donor -
        // @param startTime - Start Time
        // @param externalRefId - External Ref Id
        // @param portInRecovery - PortIn Recovery
        params: ["serviceInstanceNumber", "tempNumber","portInNumber","donor","startTime","externalRefId","callback","portInRecovery"]
    },
    portInNumberImmediate:{

        // @param serviceInstanceNumber - Service instance Number - e.g. 'LW100276'
        // @param tempNumber - Temp Number
        // @param portInNumber - Portin Number
        // @param donor - Donor -
        // @param startTime - Start Time
        // @param externalRefId - External Ref Id
        // @param portInRecovery - PortIn Recovery
        params: ["serviceInstanceNumber", "tempNumber","portInNumber","donor","startTime","externalRefId","callback","portInRecovery"]

    },
    checkInventory:{
        // @param serviceInstanceNumber - Service instance Number - e.g. 'LW100276'
        // @param tempNumber - Temp Number

        params: ["serviceInstanceNumber", "portInNumber","callback"]
    },
    deactivatePortInNumber:{

        // @param serviceInstanceNumber - Service instance Number - e.g. 'LW100276'
        // @param portInNumber - PortIn Number

        params: ["serviceInstanceNumber", "portInNumber","callback"]
    },
    loadPortInFailureReason:{

        // @param portInNumber - PortIn  Number - e.g. 'LW100276'
        // @param start_ts - Temp Number

        params: ["portInNumber","start_ts","callback"]
    },
    loadPortInCompleationStatus:{

        // @param portInNumber - PortIn  Number - e.g. 'LW100276'
        // @param start_ts -

        params: ["portInNumber","start_ts","callback"]
    },
    getDonorCodeByNetworkName:{

        // @param donor -

        params: ["donor"]
    },
    getPortInStatus:{

        // @param portInNumber - PortIn  Number - e.g. 'LW100276'
        // @param status -

        params: ["portInNumber","status"]
    },
    //endregion

    //region - Billing
    loadBillInvoices:{

        // @param monthDate - 	"monthDate":"2017-10-01"

        params: ["monthDate","callback"]
    },
    loadUnpaidBillInvoices:{

        params: ["callback"]
    },
    loadCustomerUsage:{

        // @param serviceInstance - 	"serviceInstance": "LW0336031"

        params: ["serviceInstance","callback"]

    },
    loadBillInvoicesByIds:{

        // @param invoiceIds - 	"invoiceIds":["REG0000000085556", "REG0000000085248"]

        params: ["invoiceIds","callback"]

    },
    clearCustomerPaymentsCache:{
        // @param billingAccountNumber -

        params: ["billingAccountNumber","callback"]

    },
    loadCustomerPaymentsInfo:{

        // @param billingAccountNumber
        // @param fresh
        // @param basic

        params: ["billingAccountNumber","callback","fresh","basic"]
    },
    //endregion

};