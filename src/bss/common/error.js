/*globals exports, require, module */
const connectorError = require('../elitecore/connector/connector_error');
const soapconnectorError = require('../oracle/soap/soapconnector/connector_error');
const middlewareError = require('../middleware/middleware_error');
//const usageError = require('../usage/usage_error')
//const billError = require('../bill/bill_error');
//const portinError = require('../portin/portin_error');
//const userError = require('../user/user_error');

module.exports = {
    connector: connectorError,
    middleware :middlewareError,
    soapconnector:soapconnectorError
   // usage: usageError,
   // bill: billError,
   // portin: portinError,
   // user: userError
};