/*globals exports, require, module */
const middlewareLogger = require('../middleware/middleware_logger');
const connectorLogger = require('../elitecore/connector/connector_logger');
const soapconnectorLogger = require('../oracle/soap/soapconnector/connector_logger');
//const billLogger = require('../bill/bill_logger');
//const portinLogger = require('../portin/portin_logger');
//const userLogger = require('../user/user_logger');

module.exports = {
    middleware : middlewareLogger,
    connector: connectorLogger,
    soapconnector: soapconnectorLogger,
   // bill: billLogger,
   // portin: portinLogger,
   // user:userLogger
};