/* globals module,require */

const config = require(`${global.__base}/config`);



class BSSConfig{
    constructor(){
        ///merging config at the main level to this context.
        Object.assign(this,config);

        this.gotoEliteCore = true;

        this.testoracleconfig = 'config';

        this.INSTANCEID = '3c7aa341-e572-41de-95a3-902720c45b6d';

        this.reqQ = 'RequestQ-'+ this.INSTANCEID;
        this.resQ = 'ResponseQ-'+ this.INSTANCEID;



        this.QUrl = 'amqp://localhost'
        //this.QUrl = 'amqp://user:0dHA2NkOlvO1@23.102.226.197';

        this.resQArguments = {durable: true};
        this.resQArguments = {durable:true};

        this.brmsLength = 10;
        this.middleWareLength = 30;
        this.requestslength = 10;

        this.BRM1_HOST = '192.168.56.102';
        this.BRM1_SERVICE_1_PORT = '7001';

        this.gotoSoap = true;
    }
}

const bssConfig = new BSSConfig();
module.exports = bssConfig;