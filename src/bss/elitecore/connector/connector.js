/*global global, require, exports, module*/
const config = require(global.__base + '/config');
const bssCommonLogger = require('../../common/logger');
const bssCommonError = require('../../common/error');
const common = require(global.__lib + '/common');


const soap = require('soap');
const elitecoreUtils = require('../../elitecore/elitecoreUtils');

class Connector {
    constructor() {

        this.shiftedClients = this.shiftedClients.bind(this);
        this.init = this.init.bind(this);
        this.initWSDLClients = this.initWSDLClients.bind(this);
        this.enableRecurrentEvents = this.enableRecurrentEvents.bind(this);
        this.reInitWSDLClients = this.reInitWSDLClients.bind(this);
        this.calculateStats = this.calculateStats.bind(this);
        this.clearOutDatedTimeouts = this.clearOutDatedTimeouts.bind(this);
        this.loadBssMode = this.loadBssMode.bind(this);
        this.makeSoapCall = this.makeSoapCall.bind(this);
        this.internalSoapCall = this.internalSoapCall.bind(this);
        this.handleResult = this.handleResult.bind(this);
        this.handleErrorInSoapMethodExecution = this.handleErrorInSoapMethodExecution.bind(this);
        this.handleTimeOutErrorInSoapMethodExecution = this.handleTimeOutErrorInSoapMethodExecution.bind(this);
        this.handleErrorInvalidResponseInSoapMethodExecution = this.handleErrorInvalidResponseInSoapMethodExecution.bind(this);
        this.executeSoapCall = this.executeSoapCall.bind(this);
        this.getEndpoint = this.getEndpoint.bind(this);
        this.shiftedClients = this.shiftedClients.bind(this);

        //region instance variables
        this.initInProgress = false;
        this.initDone = false;
        this.initCallbacks = [];
        this.reInitDelay = 10 * 60 * 1000;
        this.statsDelay = 60 * 1000;
        this.quarantineTimeoutsLimit = 150;
        this.quarantineDelay = 5 * 60 * 1000;

        this.countRequests = 0;
        this.countTimeouts = 0;
        this.countErrors = 0;


        //endregion

        //region EndPoints

        this.apiProductManagerAPIWebService = 'ProductManagerAPIWebService';
        this.apiProductManagerAPIWebService_v3 = 'ProductManagerAPIWebService_v3';
        this.apiCrestelInventoryOperation = 'crestelinventory/InventoryWebService/CrestelInventoryOperationsService/InventoryOperation';
        this.apiCrestelCAAMInventoryOperationsService = 'crestelcaam/ServiceOperationsWebService/CrestelCAAM/CrestelCAAMInventoryOperationsService';
        this.apiCrestelCAAMServiceOperations = 'crestelcaam/ServiceOperationsWebService/CrestelCAAMServiceOperationsService/CrestelCAAMServiceOperations';
        this.apiCrestelCAAMCustomerAccount2Operations = 'crestelcaam/CustomerOperationsWebService/CrestelCAAMCustomerAccountOperations2Service/CrestelCAAMCustomerAccount2Operations';
        this.apiCrestelCAAMAccountOperation = 'crestelcaam/AccountOperationsWebService/CrestelCAAMAccountOperationService/CrestelCAAMAccountOperation';
        this.apiCrestelCAAMAccount2Operation = 'crestelcaam/AccountOperationsWebService/CrestelCAAMAccount2OperationService/CrestelCAAMAccount2Operation';
        this.apiCrestelBECommon = 'crestelBECommon/billcommon/CrestelBECommonService/CrestelBECommon';
        this.apiCrestelPaymentManager = 'crestelpayment/service/CrestelPaymentManagerService/CrestelPaymentManager';
        this.apiIntegrationWebService = 'Integrations/IntegrationWebService';
        this.apiLWWSServices = 'LWWS/services';

        //endregion

        //region instance objects, arrays

        this.bssOfflineMode = false;

        this.disabledBss = [];
        this.statistics = {
            hosts: {},
            listeners: []
        };
        this.hostNames = {
            '10.20.3.11': 'LWEPC01',
            '10.20.3.12': 'LWEPC02',
            '10.20.3.13': 'LWBSS01',
            '10.20.3.14': 'LWBSS02',
            '10.20.3.16': 'LWBSS03',
            '10.20.3.21': 'LWBSS04',
            '10.20.3.18': 'LWTES01'
        };
        this.defaultConfig = {
            bssHosts: [config.BSS1_HOST, config.BSS2_HOST, config.BSS3_HOST, config.BSS4_HOST],
            bssPort1: config.BSS_SERVICE_1_PORT,
            bssPort2: config.BSS_SERVICE_2_PORT,
            bssPort3: config.BSS_SERVICE_3_PORT,
            ocsHosts: [config.OCS1_HOST, config.OCS2_HOST],
            ocsPort: config.OCS_PORT,
            fqdn: config.MYFQDN
        };
        this.initParams = [
            {hosts: this.defaultConfig.bssHosts, port: this.defaultConfig.bssPort1, path:this.apiProductManagerAPIWebService, ignoredNamespaces: true},
            {hosts: this.defaultConfig.bssHosts, port: this.defaultConfig.bssPort1, path: this.apiProductManagerAPIWebService_v3, ignoredNamespaces: true},
            {hosts: this.defaultConfig.bssHosts, port: this.defaultConfig.bssPort2, path: this.apiCrestelInventoryOperation, ignoredNamespaces: true},
            {hosts: this.defaultConfig.bssHosts, port: this.defaultConfig.bssPort2, path: this.apiCrestelCAAMInventoryOperationsService, ignoredNamespaces: true},
            {hosts: this.defaultConfig.bssHosts, port: this.defaultConfig.bssPort2, path: this.apiCrestelCAAMServiceOperations, ignoredNamespaces: true},
            {hosts: this.defaultConfig.bssHosts, port: this.defaultConfig.bssPort2, path: this.apiCrestelCAAMCustomerAccount2Operations, ignoredNamespaces: true},
            {hosts: this.defaultConfig.bssHosts, port: this.defaultConfig.bssPort2, path: this.apiCrestelCAAMAccountOperation, ignoredNamespaces: true},
            {hosts: this.defaultConfig.bssHosts, port: this.defaultConfig.bssPort2, path: this.apiCrestelCAAMAccount2Operation, ignoredNamespaces: true},
            {hosts: this.defaultConfig.bssHosts, port: this.defaultConfig.bssPort2, path: this.apiCrestelBECommon, ignoredNamespaces: true},
            {hosts: this.defaultConfig.bssHosts, port: this.defaultConfig.bssPort2, path: this.apiCrestelPaymentManager, ignoredNamespaces: true},
            {
                hosts: this.defaultConfig.bssHosts, port: this.defaultConfig.bssPort3, path: this.apiLWWSServices, ignoredNamespaces: true, overrideRootElement: true,
                targetNSAlias: "ws"
            },
            {hosts: this.defaultConfig.ocsHosts, port: this.defaultConfig.ocsPort, path: this.apiIntegrationWebService, ignoredNamespaces: true}
        ];

        this.clients = {};

        //endregion
    }


//region Init, Re-Init, recurring events, stats
    init(callback) {

        if (callback)
            this.initCallbacks.push(callback);

        if (this.checkIfNotRequiredToInit){
            if (callback) callback(this.clients);
            return;
        }


        this.initInProgress = true;

        let loadedCount = 0;

        let initCallBack = (err) => {

            loadedCount++;

            if (err) bssCommonLogger.connector.initFailed(err);
            bssCommonLogger.connector.loadedCount(loadedCount, this.initParams.length);

            if (loadedCount === this.initParams.length) {
                let callbacks = Object.assign([], this.initCallbacks);

                this.initDone = true;
                this.initInProgress = false;
                this.initCallbacks = [];

                this.enableRecurrentEvents();

                callbacks.forEach((callback) => {
                    callback(this.clients);
                });
            }
        };

        this.initParams.forEach((item) => {
            //Parameters - hosts, port, path, ignoredNamespaces,overrideRootElement,targetNSAlias, callback
            Object.assign(item, {callback: initCallBack});
            this.initWSDLClients(item);
        });
    }

    //Parameters - hosts, port, path, ignoredNamespaces,overrideRootElement,targetNSAlias, callback
    initWSDLClients(parameters) {
        let {hosts, ignoredNamespaces, overrideRootElement} = parameters;

        this.checkParamValidity(parameters);

        let options = this.getOptions(ignoredNamespaces, overrideRootElement);

        let hostsInitCount = 0;

        hosts.forEach((host) => {
            //Parameters - hosts, port, path, ignoredNamespaces,overrideRootElement,targetNSAlias,host,options, callback
            let initHost = (parameters) => {
                let {options, port, host, path, targetNSAlias} = parameters;
                let url = common.getUrl(host, port, path);

                bssCommonLogger.connector.initClientMethodInitiated(url);

                //Parameters - hosts, port, path, ignoredNamespaces,overrideRootElement,targetNSAlias,host,options,url,client, callback
                let handleInitClientCallbackResult = (err, parameters) => {
                    let {url, client, host, hosts, port, path, needIgnoredNamespaces, needOverrideRootElement, targetNSAlias, callback} = parameters;
                    hostsInitCount++;

                    let clientEndPoint = this.getEndpoint(path);

                    if (err) {
                        bssCommonLogger.connector.initClientCreateFailed(url, err);
                        clientEndPoint.errors[url] = {
                            err: err,
                            url: url,
                            info: {
                                hosts: [host],
                                port: port,
                                path: path,
                                ignoredNamespaces: needIgnoredNamespaces,
                                overrideRootElement: needOverrideRootElement,
                                targetNSAlias: targetNSAlias
                            }
                        };
                    } else {
                        bssCommonLogger.connector.loadedSoapClient(url);
                        delete clientEndPoint.errors[url];
                        clientEndPoint.clients.push({
                            client: client,
                            host: host,
                            port: port,
                            url: url
                        });
                    }


                    let pendingCallbacks = Object.assign([], clientEndPoint.pendingCallbacks);
                    clientEndPoint.pendingCallbacks = [];
                    pendingCallbacks.forEach(pendingCallback => {
                        pendingCallback(clientEndPoint);
                    });

                    if (hostsInitCount === hosts.length) {
                        if (callback) callback(undefined, clientEndPoint);
                    }
                };

                let initHostCallback = (err, client) => {

                    //Parameters - hosts, port, path, ignoredNamespaces,overrideRootElement,targetNSAlias,host,options,url,client, callback
                    Object.assign(parameters, {
                        url: url,
                        client: client
                    });

                    if (err)
                        return handleInitClientCallbackResult(err, parameters);

                    // workaround to define namespace for function name

                    if (targetNSAlias) {
                        client._invoke_overriden = client._invoke;
                        client._invoke = (method, args, location, callback, options, extraHeaders) => {
                            method.input.targetNSAlias = targetNSAlias;
                            client._invoke_overriden(method, args, location, callback, options, extraHeaders);
                        }
                    }
                    handleInitClientCallbackResult(undefined, parameters);
                };

                soap.createClient(url, options, initHostCallback);

            };
            //Parameters - hosts, port, path, ignoredNamespaces,overrideRootElement,targetNSAlias,host,options, callback
            Object.assign(parameters, {host, options});
            initHost(parameters);
        });
    }

    enableRecurrentEvents() {
        setInterval(() => {
            this.reInitWSDLClients();
        }, this.reInitDelay);

        setInterval(() => {
            this.calculateStats();
        }, this.statsDelay);
    };

    reInitWSDLClients() {
        Object.keys(this.clients).forEach((path) => {
            let client = this.clients[path];

            Object.keys(client.errors).forEach((url) => {
                let error = client.errors[url];
                let item = error.info;
                bssCommonLogger.connector.reInitOccured(url);
                this.initWSDLClients(item);
            });
        });
    }

    calculateStats() {
        let aggregated = {};
        let allErrors = [];
        let allTimeouts = [];


        Object.keys(this.statistics.hosts).forEach(host => {
            let hostStats = this.statistics.hosts[host];
            let lastRequests = hostStats.lastRequests;
            let lastErrors = hostStats.lastErrors;
            let lastTimeouts = hostStats.lastTimeouts;

            hostStats.lastRequests = [];
            hostStats.lastErrors = [];
            hostStats.lastTimeouts = [];

            lastErrors.forEach(error => {
                error.ip = error.host;
                error.host = this.hostNames[error.host];
                error.status = 'ERROR';
                allErrors.push(error);
            });

            lastTimeouts.forEach((timeout) => {
                timeout.ip = timeout.host;
                timeout.host = this.hostNames[timeout.host];
                timeout.status = 'TIMEOUT';
                allTimeouts.push(timeout);
            });

            lastRequests.forEach((request) => {
                let key = request.method + '_' + host;
                let aggregatedItem = aggregated[key];
                let hostName = this.hostNames[request.host];
                if (!hostName) hostName = request.host;

                if (!aggregatedItem) {
                    aggregatedItem = {
                        items: [],
                        executionTime: 0,
                        requestsCount: 0,
                        errorsCount: 0,
                        timeoutsCount: 0,
                        host: hostName,
                        method: request.method
                    };
                    aggregated[key] = aggregatedItem;
                }
                aggregatedItem.items.push(request);
            });
        });

        let requestsResult = [];
        let allRequestsTotalTime = 0;
        let allRequestsStats = {
            executionTime: 0,
            requestsCount: 0,
            errorsCount: 0,
            timeoutsCount: 0
        };

        Object.keys(aggregated).forEach((key) => {
            let aggregatedItem = aggregated[key];
            let totalTime = 0;
            let maxTime = 0;

            aggregatedItem.items.forEach((item) => {
                allRequestsTotalTime += item.executionTime;
                totalTime += item.executionTime;
                aggregatedItem.requestsCount++;
                allRequestsStats.requestsCount++;

                if (maxTime < item.executionTime) {
                    maxTime = item.executionTime;
                }

                if (item.hasError) {
                    aggregatedItem.errorsCount++;
                    allRequestsStats.errorsCount++;
                }

                if (item.hasTimeout) {
                    aggregatedItem.timeoutsCount++;
                    allRequestsStats.timeoutsCount++;
                }
            });

            aggregatedItem.executionMaxTime = maxTime;
            aggregatedItem.executionTime = aggregatedItem.requestsCount > 0
                ? parseInt(totalTime / aggregatedItem.requestsCount) : 0;

            delete aggregatedItem.items;
            requestsResult.push(aggregatedItem);
        });

        allRequestsStats.executionTime = allRequestsStats.requestsCount > 0
            ? parseInt(allRequestsTotalTime / allRequestsStats.requestsCount) : 0;
        //if (log) common.log('EliteCoreConnector', 'statistics: ' + JSON.stringify(allRequestsStats));

        let resultAverage = [];
        let mapAverage = {};
        let config = this.defaultConfig;

        requestsResult.forEach((item) => {
            let aggregated = mapAverage[item.host];

            if (!aggregated) {
                aggregated = {
                    host: item.host,
                    fqdn: config.fqdn ? config.fqdn : 'undefined',
                    requests: []
                };

                mapAverage[item.host] = aggregated;
                resultAverage.push(aggregated);
            }

            delete item.host;
            aggregated.requests.push(item);
        });

        this.statistics.listeners.forEach((listener) => {
            listener.callback(resultAverage, allRequestsStats, allErrors, allTimeouts);
        });
    }

    clearOutDatedTimeouts(stats) {
        let currentTimeMs = new Date().getTime();
        if (stats.activeTimeouts.length > 0) {
            stats.activeTimeouts = stats.activeTimeouts.filter((item) => {
                return item.ts + this.quarantineDelay > currentTimeMs;
            });
        }
    }

//endregion

//region Call, Trigger, Fire

    loadBssMode(callback) {
        if (!callback) callback = () => {
        };
        callback(undefined, {offline: this.bssOfflineMode});
    };

    //Params - path, method, parameters, options, callback
    makeSoapCall(path, method, parameters, options, callback) {


        this.loadBssMode((err, result) => {
            if (err) return callback(err);
            if (result && result.offline) return callback(bssCommonError.connector.bssIsOff(params));

            this.init((wsdlClients) => {
                // Params : wsdlEndpoint, method, parameters, options, callback
                let params = {wsdlEndpoint: wsdlClients[path], method, parameters, options, callback};
                this.internalSoapCall(params);
            });
        });
    };

    // Params : wsdlEndpoint, method, parameters, options, callback
    internalSoapCall(params) {
        let {wsdlEndpoint,  options, callback} = params;

        callback = callback || (() => {
        });

        if (this.isEndPointValidityFailed(params)) return callback(bssCommonError.connector.noSoapClientsFound(params));

        let num = this.countRequests++;

        this.countRequests = (Number.MAX_VALUE <= this.countRequests + 1) ? this.countRequests : 0;
        const soapTimeout = (options && options.timeout) ? options.timeout : 30000;

        let shiftedClients = this.shiftedClients(wsdlEndpoint.clients, num);

        // Params : wsdlEndpoint, method, parameters, options, callback, soapTimeout, num, soapClients, pos
        Object.assign(params, {soapTimeout, num, soapClients: shiftedClients, pos: 0});

        this.executeSoapCall(params);
    }

    // Params : wsdlEndpoint, method, parameters, options, callback, soapTimeout, num, soapClients, pos, err, reponse, soapClient, executionTime
    handleResult (params) {
        let {method, callback, soapClient, err, executionTime, response} = params;

        let currentTimeMs = new Date().getTime();

        if (response) elitecoreUtils.replace$Keys(response);
        if (!soapClient) return callback(err, response);

        let stats = this.statistics.hosts[soapClient.host];
        if (!stats) {
            stats = {
                host: soapClient.host,
                activeTimeouts: [],
                lastTimeouts: [],
                lastErrors: [],
                lastRequests: [],
                errorsCount: 0,
                timeoutsCount: 0,
                quarantineEnd: 0
            };
            this.statistics.hosts[soapClient.host] = stats;
        }

        stats.quarantineEnd = (stats.quarantineEnd < currentTimeMs) ? 0 : stats.quarantineEnd;

        let responseCode = (response && response.return ? response.return.responseCode : undefined) || 'NONE';
        let responseMessage = response && response.return ? response.return.responseMessage : undefined;

        let invalidResponse = this.checkResponseValidity(response, responseCode, responseMessage);

        let timeoutError = err && (err.code === 'ETIMEDOUT' || err.code === 'ESOCKETTIMEDOUT');


        if (timeoutError || (err || invalidResponse)) {
            Object.assign(params, {stats, currentTimeMs, responseCode, responseMessage});
            //params : wsdlEndpoint, method, parameters, options, callback, soapTimeout, num, soapClients, pos, err, reponse, soapClient, executionTime, stats,currentTimeMs, responseCode, responseMessage
        }
        if (timeoutError) {
            this.handleTimeOutErrorInSoapMethodExecution(params);
        } else if (err || invalidResponse) {
            this.handleErrorInvalidResponseInSoapMethodExecution(params);
        }

        stats.lastRequests.push({
            hasError: !!((err && !timeoutError) || invalidResponse),
            hasTimeout: !!(err && timeoutError),
            error: err ? err.message : undefined,
            ts: currentTimeMs,
            executionTime: executionTime,
            host: soapClient.host,
            port: soapClient.port,
            method: method
        });

        callback(err, response);
    };

    // Params : wsdlEndpoint, method, parameters, options, callback, soapTimeout, num, soapClients, pos,err,result,executionTime,soapClient
    handleErrorInSoapMethodExecution(params) {
        let {pos, err} = params;

        bssCommonLogger.connector.handleErrorInSoapMethodExecution(params);


        // do not continue in case of TIMEOUT, EliteCore may actually
        // finish operation successfully but don't respond

        if (err.code === 'ETIMEDOUT' || err.code === 'ESOCKETTIMEDOUT') {
            return this.handleResult(params);
        } else {
            Object.assign(params, {pos: pos + 1});
            return this.executeSoapCall(params);
        }
    };

    //params : wsdlEndpoint, method, parameters, options, callback, soapTimeout, num, soapClients, pos, err, reponse, soapClient, executionTime, stats,currentTimeMs, responseCode, responseMessage
    handleTimeOutErrorInSoapMethodExecution(params){
        let {wsdlEndpoint, method, stats, parameters, err, currentTimeMs, soapClient, num} = params;

        // clear outdated timeouts
        this.clearOutDatedTimeouts(stats);

        this.countTimeouts++;

        stats.timeoutsCount++;
        stats.lastTimeouts.push({
            ts: currentTimeMs,
            code: err.code,
            host: soapClient.host,
            port: soapClient.port,
            path: wsdlEndpoint.path,
            method: method,
            parameters: parameters
        });

        stats.activeTimeouts.push({
            ts: currentTimeMs,
            host: soapClient.host,
            port: soapClient.port,
            method: method
        });

        if (stats.activeTimeouts.length > this.quarantineTimeoutsLimit && stats.quarantineEnd < currentTimeMs) {
            stats.quarantineEnd = currentTimeMs + this.quarantineDelay;
        }
        bssCommonLogger.connector.handleTimeOutErrorInSoapMethodExecution({num, stats, soapClient, currentTimeMs});
    };

    //params : wsdlEndpoint, method, parameters, options, callback, soapTimeout, num, soapClients, pos, err, reponse, soapClient, executionTime, stats,currentTimeMs, responseCode, responseMessage
    handleErrorInvalidResponseInSoapMethodExecution(params) {
        let {wsdlEndpoint, method, stats, parameters, err, currentTimeMs, soapClient, num,response, responseCode, responseMessage} = params;
        this.countErrors++;

        let errorMessage = err ? err.message : (!response.return ? `Empty response` : `Error (responseCode=${responseCode})`);

        stats.errorsCount++;
        stats.lastErrors.push({
            ts: currentTimeMs,
            host: soapClient.host,
            port: soapClient.port,
            responseCode: responseCode,
            responseMessage: responseMessage,
            err: errorMessage,
            path: wsdlEndpoint.path,
            method: method,
            parameters: parameters,
            response: response
        });
        bssCommonLogger.connector.handleErrorInvalidResponseInSoapMethodExecution({num, stats, soapClient, errorMessage});
    };

    // Params : wsdlEndpoint, method, parameters, options, callback, soapTimeout, num, soapClients, pos
    executeSoapCall(params) {

        let {wsdlEndpoint, method, parameters, soapTimeout, num, soapClients, pos} = params;

        if (pos >= soapClients.length) {
            let error = bssCommonError.connector.failedWithAllSoapClients(soapClients,method);
            let soapClient = soapClients.length > 0 ? soapClients[soapClients.length - 1] : undefined;

            // Params : wsdlEndpoint, method, parameters, options, callback, soapTimeout, num, soapClients, pos, err, reponse, soapClient, executionTime
            Object.assign(params, {err: error, response: undefined, soapClient: soapClient, executionTime: -1});
            return this.handleResult(params);
        }

        let soapClient = soapClients[pos];
        let startTimeMs = new Date().getTime();

        bssCommonLogger.connector.soapCallStarted(params, soapClient);

        if (!soapClient.client[method]) {
            let error = bssCommonError.connector.methodNotProvidedByEndPoint(method, wsdlEndpoint);
            // Params : wsdlEndpoint, method, parameters, options, callback, soapTimeout, num, soapClients, pos, err, reponse, soapClient, executionTime
            Object.assign(params, {err: error, response: undefined, soapClient: soapClient, executionTime: 0});
            return this.handleResult(params);
        }

        soapClient.client[method](parameters, (err, result) => {
            let executionTime = new Date().getTime() - startTimeMs;

            if (err) {
                // Params : wsdlEndpoint, method, parameters, options, callback, soapTimeout, num, soapClients, pos,err,result,executionTime,soapClient
                Object.assign(params, {err, response: result, executionTime, soapClient});
                return this.handleErrorInSoapMethodExecution(params);
            }

            bssCommonLogger.connector.soapCallFinished({num, soapClient, method, executionTime});
            // Params : wsdlEndpoint, method, parameters, options, callback, soapTimeout, num, soapClients, pos, err, reponse, soapClient, executionTime
            Object.assign(params, {
                err: undefined,
                response: result,
                soapClient: soapClient,
                executionTime: executionTime
            });
            this.handleResult(params);
        }, {timeout: soapTimeout});
    }

//endregion

//region helpers, other functions, validators


    getOptions(needIgnoredNamespaces, needOverrideRootElement) {
        let options = {};
        if (needIgnoredNamespaces) {
            options.ignoredNamespaces = {
                namespaces: ['targetNamespace', 'typedNamespace', 'xs'],
                override: true
            }
        }

        if (needOverrideRootElement) {
            options.overrideRootElement = {
                namespace: "tns"
            }
        }
        return options;
    }

    get checkIfNotRequiredToInit() {
        if (this.initInProgress) {
            bssCommonLogger.connector.initializationInProgress();
            return true;
        }
        if (this.initDone) {
            bssCommonLogger.connector.initializationAlreadyDone();
            return true;
        }
        return false;
    }

    checkParamValidity(parameters) {

        let {hosts, port, path, callback} = parameters;

        let valid = true;

        [hosts, port, path].forEach(param => {
            if (!param) {
                valid = false;
            }
        });

        if (!valid && callback) {
            bssCommonLogger.connector.paramInvalid(parameters);
            callback(new Error('Params are invalid'));
        }
    }

    getEndpoint(path) {
        if (!this.clients[path]) {
            this.clients[path] = {
                path: path,
                clients: [],
                errors: {},
                pendingCallbacks: []
            };
        }
        return this.clients[path];
    };

    //Params -  wsdlEndpoint, method, parameters, options, callback
    isEndPointValidityFailed(params) {
        let {wsdlEndpoint} = params;
        return (!wsdlEndpoint || wsdlEndpoint.clients.length === 0)
    }

    checkResponseValidity(response, responseCode, responseMessage) {
        return response
            && (!response.return || (responseCode !== 'NONE'
                && responseCode !== '1'
                && responseCode !== '0'
                && responseCode !== '-1'
                && responseCode !== '-50'
                && responseCode !== '-1001002'
                && responseCode !== '-10010133'
                && responseCode !== '-2002205'
                && responseCode !== '-2002500'
                && responseCode !== '-1003028003'
                && responseMessage !== 'SUCCESS'));
    }

    shiftedClients(soapClients, num) {

        let currentTimeMs = new Date().getTime();
        let clients = [];

        for (let i = 0; i < soapClients.length; i++) {
            let client = soapClients[i];
            let stats = this.statistics[client.host];

            // check if host is in quarantine
            if ((!stats || currentTimeMs > stats.quarantineEnd)
                && (!this.disabledBss || this.disabledBss.indexOf(client.host) === -1)) {
                clients.push(client);
            }
        }

        let offset = num % clients.length;
        for (let i = 0; i < offset; i++) {
            clients.push(clients.shift());
        }

        bssCommonLogger.connector.internalSoapCallDetails(clients, this.disabledBss, num);
        if (clients.length === 0) bssCommonLogger.connector.shiftedClientsClientsLengthZero(clients, num, soapClients);

        return clients;
    }

//endregion


}


let connector = new Connector();
module.exports = connector;