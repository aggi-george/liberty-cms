/*globals module */
class ConnectorError{
    constructor(){

    }
    noSoapClientsFound(params){
        let {method} = params;
        return new Error(`No SOAP clients are found for the endpoint, method=${method}`);
    }
    bssIsOff(params){
        let {method} = params;
        return new Error(`BSS is off, SOAP calls are not allowed, method=${method}`);
    }
    failedWithAllSoapClients(soapClients,method){
        return new Error(`Failed with all available SOAP clients, method=${method}, clients count=${soapClients.length}`);
    }

    methodNotProvidedByEndPoint(method, wsdlEndpoint){
        return new Error(`Method ${method} is not provided by end point ${wsdlEndpoint.path}`)
    }

}

let connectorError = new ConnectorError();
module.exports = connectorError;