/*global require, exports, module*/

const common = require(global.__lib + '/common');
const bssConfig = require('../../config');

class ConnectorLogger {
    constructor() {
        this.category = 'EliteCoreConnector';
        this.logEnabled = bssConfig.EC_LOGSENABLED;
    }

    initializationInProgress() {
        if (!this.logEnabled) return;
        common.log(this.category, `init: initialisation is in progress`);
    }

    initializationAlreadyDone() {
        if (!this.logEnabled) return;
        common.log(this.category, `init: initialisation is already done`);
    }

    initClientMethodInitiated(url) {
        if (!this.logEnabled) return;
        common.log(this.category, `initWSDLClients: load soap client, url=${url}`);
    }

    initClientCreateFailed(url, err) {
        if (!this.logEnabled) return;
        let error = err || {};
        common.error(this.category, `initWSDLClients: failed to create soap client, url=${url}, error=${error.message}`);
    }

    loadedSoapClient(url) {
        if (!this.logEnabled) return;
        common.log(this.category, `initWSDLClients: loaded soap client, url=${url}`);
    }

    initFailed(err) {
        if (!this.logEnabled) return;
        err = err || {};
        common.error(this.category, `init: failed to init an group, error=${err.message}`);
    }

    loadedCount(loadedCount, paramsLength) {
        if (!this.logEnabled) return;
        common.log(this.category, `init: loaded count=${loadedCount}, totalCount=${paramsLength}`);
    }

    reInitOccured(url) {
        if (!this.logEnabled) return;
        common.log(this.category, `reInitWSDLClients: an initialization error is found, need to re-init, url=${url}`);
    }

    paramInvalid(parameters) {
        if (!this.logEnabled) return;
        let {hosts, port, path} = parameters;
        common.error(this.category, `initWSDLClients: invalid params, hosts=${JSON.stringify(hosts)}, port=${port}, path${path}`);
    }
    soapCallStarted(params,soapClient){
        let {method, parameters, soapTimeout, num} = params;
        if (!this.logEnabled) return;
        common.log(this.category, `internalSoapCall[${num}]: soap call started, host=${soapClient.host}, port=${soapClient.port}, timeout=${soapTimeout}, method=${method}, params=${JSON.stringify(parameters)}, url=${soapClient.url}`);
    }

    handleErrorInSoapMethodExecution(parameters){

        if (!this.logEnabled) return;

        let {err,result,num,soapClient,method,executionTime} = parameters;
        
        if (err.message === 'undefined: undefined' && result && result.statusCode) {
            err.message = `CODE=${result.statusCode}, BODY=${result.body}`;
        }

        common.error(this.category, `internalSoapCall[${num}]: failed to make a soap call, message=${err.message}, code=${err.code}, body=${err.body && err.body.length > 600 ? err.body.substring(0, 600) + '.....' : ''}, host=${soapClient.host}, port=${soapClient.port}, method=${method}, executionTime=${executionTime}`);
    }

    soapCallFinished(parameters){
        if (!this.logEnabled) return;
        let {num,soapClient,method,executionTime} = parameters;
        common.log(this.category, `internalSoapCall[${num}]: soap call finished, host=${soapClient.host}, port=${soapClient.port}, method=${method}, executionTime=${executionTime}`);
    }

    handleTimeOutErrorInSoapMethodExecution(params){
        if (!this.logEnabled) return;
        let {num,stats,soapClient,currentTimeMs,response} = params;
        common.log(this.category, `internalSoapCall[${num}]: saved ETIMEDOUT log, host=${soapClient.host}, port=${soapClient.port}, timeoutsCount=${stats.activeTimeouts.length}, quarantineTime=${stats.quarantineEnd > 0 ? ((stats.quarantineEnd - currentTimeMs) / 1000) + 's' : 'NONE'}`);
    }

    handleErrorInvalidResponseInSoapMethodExecution(params){
        if (!this.logEnabled) return;
        let {num,stats,soapClient,errorMessage,response} = params;
        let responseString = response ? JSON.stringify(response) : '';
        responseString = responseString.length > 150 ? responseString.substring(0, 149) : responseString;
        common.log(this.category, `internalSoapCall[${num}]: saved ERROR log, host=${soapClient.host}, port=${soapClient.port}, errorMessage=${errorMessage}, response=${responseString}, errorsCount=${stats.lastErrors.length}`);
    }

    internalSoapCallDetails(clients,disabledBss,num){
        if (!this.logEnabled) return;
        let hosts = [];
        clients.forEach(function (item) {
            hosts.push(item.host);
        });

        common.log(this.category, `internalSoapCall[${num}]: selected hosts=${JSON.stringify(hosts)}, disabled hosts=${JSON.stringify(disabledBss)}`);
    }

    shiftedClientsClientsLengthZero(clients, num, soapClients){
        if (!this.logEnabled) return;

        let randomlySelectedClient = soapClients[num % soapClients.length];
        clients.push(randomlySelectedClient);
        common.error(this.category, `internalSoapCall[${num}]: all provided hosts are in quarantine, select any, clients=${randomlySelectedClient.host}`);
    }
}

let connectorLogger = new ConnectorLogger();
module.exports = connectorLogger;