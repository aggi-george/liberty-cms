/* globals require,module*/
const connector = require('./connector');
const dateformat = require('dateformat');

class SoapClient {
    constructor() {

    }

//region General

    loadBssMode(callback){
        connector.loadBssMode(callback);
    }

    init(callback) {
        connector.init(callback);
    };

    setDisabledBss(bssList)  {
        connector.disabledBss = bssList;
    };

    setOfflineMode(offline)  {
        connector.bssOfflineMode = offline;
    };

    getCountTimeouts() {
        return connector.countTimeouts;
    };

    getCountRequests() {
        return connector.countRequests;
    };

    getCountErrors() {
        return connector.countErrors;
    };

    addStatsUpdateListener(l) {
        const key = new Date().getTime() + "_" + Math.random();

        connector.statistics.listeners.push({
            key: key,
            callback: l
        });

        return key;
    };

    removeStatsUpdateListener (key) {
        for (let i = 0; i < connector.statistics.listeners.length; i++) {
            const obj = connector.statistics.listeners[i];
            if (obj.key === key) {
                connector.statistics.listeners.splice(i, 1);
                return true;
            }
        }
        return false;
    };

//endregion

//region Product

    getProductOfferDetails (callback) {
        connector.makeSoapCall(connector.apiProductManagerAPIWebService, "getProductOfferDetails", {}, undefined, callback);
    };

    getSpecificProdOfferDetails (args, callback) {
        connector.makeSoapCall(connector.apiProductManagerAPIWebService, "getSpecificProdOfferDetails", {arg0: {productIds: args}}, undefined, callback);
    };

//endregion

//region Business Heirarchy, Package

    getBusinessHierarchyDetail (callback) {
        connector.makeSoapCall(connector.apiProductManagerAPIWebService_v3, "getBusinessHierarchyDetail", {arg0: {}}, undefined, callback);
    };
    getCustomerHierarchyDetails (accountNumber, callback) {
        connector.makeSoapCall(connector.apiCrestelCAAMCustomerAccount2Operations, "getCustomerHierarchyDetails",
            {arguments: {accountNumber: accountNumber}}, undefined, callback);
    };
    subscribeAddOnPackage (args, callback) {
        connector.makeSoapCall(connector.apiCrestelCAAMServiceOperations, "subscribeAddOnPackage", {arguments: args}, undefined, callback);
    };
    unSubscribeAddOnPackage (args, callback) {
        connector.makeSoapCall(connector.apiCrestelCAAMServiceOperations, "unSubscribeAddOnPackage", {arguments: args}, undefined, callback);
    };
    getFnFMembersDetails (args, callback) {
        connector.makeSoapCall(connector.apiCrestelCAAMServiceOperations, "getFnFMembersDetails", {arguments: args}, undefined, callback);
    };
    addFnFMembers (args, callback) {
        connector.makeSoapCall(connector.apiCrestelCAAMServiceOperations, "addFnFMembers", {arguments: args}, undefined, callback);
    };
    removeFnFMembers (args, callback) {
        connector.makeSoapCall(connector.apiCrestelCAAMServiceOperations, "removeFnFMembers", {arguments: args}, undefined, callback);
    };
    modifyPlanAttributes (args, callback) {
        connector.makeSoapCall(connector.apiCrestelCAAMServiceOperations, "modifyPlanAttributes", {arguments: args}, undefined, callback);
    };
//endregion

//region Inventory

    searchInventory (inventoryNumber, callback) {
        connector.makeSoapCall(connector.apiCrestelInventoryOperation, "searchInventory",
            {arguments: {inventoryNumber: inventoryNumber}}, undefined, callback);
    };
    searchInventoryList (inventoryStatusId, inventorySubTypeId, callback) {
        connector.makeSoapCall(connector.apiCrestelInventoryOperation, "searchInventory",
            {arguments: {inventoryStatusId: inventoryStatusId, inventorySubTypeId: inventorySubTypeId, ownerId: "Operator"}}, undefined, callback);
    };
    doInventoryStatusOperation (inventoryNumber, operationAlias, callback) {
        connector.makeSoapCall(connector.apiCrestelInventoryOperation, "doInventoryStatusOperation", {
            arguments: {
                inventoryNumber: inventoryNumber,
                operationAlias: operationAlias,
                referenceNumber: "N/A"
            }
        }, undefined, callback);
    };
    repairInventoryRebundle (args, callback) {
        connector.makeSoapCall(connector.apiCrestelInventoryOperation, "repairInventory", {arg0: args}, undefined, callback);
    };
    getInventoryDetail (args, callback) {
        connector.makeSoapCall(connector.apiCrestelInventoryOperation, "getInventoryDetail", {arguments: args}, undefined, callback);
    };
    listInventory (serviceInstanceNumber, callback) {
        connector.makeSoapCall(connector.apiCrestelCAAMInventoryOperationsService, "listInventory", {
            arguments: {
                serviceInstanceNumber: serviceInstanceNumber
            }
        }, undefined, callback);
    };
    unpairInventory (args, callback) {
        connector.makeSoapCall(connector.apiCrestelCAAMInventoryOperationsService, "unpairInventory", {arguments: args}, undefined, callback);
    };
    repairInventory (args, callback) {
        connector.makeSoapCall(connector.apiCrestelCAAMInventoryOperationsService, "repairInventory", {arguments: args}, undefined, callback);
    };
    repairInventoryXml (args, callback) {
        connector.makeSoapCall(connector.apiCrestelCAAMInventoryOperationsService, "repairInventory", {arguments: args}, undefined, callback);
    };
    removeInventory (serviceInstanceNumber, inventoryNumber, callback) {
        connector.makeSoapCall(connector.apiCrestelCAAMInventoryOperationsService, "removeInventory", {
            arguments: {
                serviceInstanceNumber: serviceInstanceNumber,
                inventoryNumber: inventoryNumber
            }
        }, undefined, callback);
    };
    addInventory (effectDate, inventoryNumber, serviceInstanceNumber, callback) {
        let ecDate = dateformat(new Date(effectDate ? effectDate : new Date()), "yyyy-mm-dd");
        connector.makeSoapCall(connector.apiCrestelCAAMInventoryOperationsService, "addInventory", {
            arguments: {
                effectDate: ecDate,
                inventoryNumber: inventoryNumber,
                inventorySubtype: "MSISDN",
                isManualInventory: "N",
                packageId: "PRD00401",    // base plan hard coded for now
                serviceInstanceNumber: serviceInstanceNumber
            }
        }, undefined, callback);
    };
    markInventoryAsDamaged (inventoryNumber, serviceInstanceNumber, callback) {
        connector.makeSoapCall(connector.apiCrestelCAAMInventoryOperationsService, "markInventoryAsDamaged", {
            arguments: {
                inventoryNumber: inventoryNumber,
                serviceInstanceNumber: serviceInstanceNumber
            }
        }, undefined, callback);
    };
    createInventory (msisdn, donorCode, date, callback) {
        let ecDate = dateformat(new Date(date ? date : new Date()), "dd-mm-yyyy");
        connector.makeSoapCall(connector.apiLWWSServices, "createInventory", {
            arg0: {
                arguments: {
                    inventoryGroup: "NGP00003",
                    inventorySubGroup: "NTP00007",
                    warehouseName: "WMAS00000001",
                    batchNumber: "NBT000000265",
                    staffName: "LWUser",
                    MSISDN: msisdn,
                    inventoryAttributes: [{
                        key: "PARAM2",
                        value: donorCode
                    }, {
                        key: "PARAM3",
                        value: "Manual"
                    }, {
                        key: "PARAM4",
                        value: ecDate
                    }]
                }
            }
        }, undefined, callback);
    };
    updateInventoryAttributes (msisdn, donorCode, date, callback) {
        let ecDate = dateformat(new Date(date ? date : new Date()), "dd-mm-yyyy");
        connector.makeSoapCall(connector.apiLWWSServices, "updateInventoryAttributes", {
            arg0: {
                arguments: {
                    staffName: "LWUser",
                    MSISDN: msisdn,
                    inventoryAttributes: [{
                        key: "PARAM2",
                        value: donorCode
                    }, {
                        key: "PARAM4",
                        value: ecDate
                    }]
                }
            }
        }, undefined, callback);
    };
//endregion

//region Accosnt, Service, Instance

    createAccount (args, callback) {
        connector.makeSoapCall(connector.apiCrestelCAAMCustomerAccount2Operations, "createCustomer", {arguments: args}, {timeout: 60000}, callback);
    };
    changeServicePlan (billingAccountNumber, effectiveDate, packageName, remarks, serviceInstanceNumber, callback) {
        connector.makeSoapCall(connector.apiCrestelCAAMServiceOperations, "changeServicePlan", {
            arguments: {
                billingAccountNumber: billingAccountNumber,
                effectiveDate: effectiveDate,
                packageName: packageName,
                remarks: remarks,
                serviceInstanceNumber: serviceInstanceNumber
            }
        }, undefined, callback);
    };
    searchAccount (number, accountNumber, accountName, serviceUserName, callback) {
        connector.makeSoapCall(connector.apiCrestelCAAMCustomerAccount2Operations, "searchAccount", {
            arguments: {
                mobileOrMSISDNNumber: number,
                accountNumber: accountNumber,
                accountName: accountName,
                serviceUserName: serviceUserName
            }
        }, undefined, callback);
    };
    searchServiceInstance (number, accountNumber, accountName, callback) {
        connector.makeSoapCall(connector.apiCrestelCAAMCustomerAccount2Operations, "searchAccount", {
            arguments: {
                mobileOrMSISDNNumber: number,
                accountNumber: accountNumber,
                accountName: accountName
            }
        }, undefined, callback);
    };
    getAccountStatement (accountNumber, fromDate, toDate, callback) {
        connector.makeSoapCall(connector.apiCrestelCAAMAccountOperation, "getAccountStatement", {
            arguments: {
                accountNumber: accountNumber,
                fromDate: fromDate,
                toDate: toDate
            }
        }, undefined, callback);
    };
    addDepositToCustomerBillingAccount (billingAccountNumber, depositAmount, reason, callback) {
        connector.makeSoapCall(connector.apiCrestelCAAMAccountOperation, "addDepositToCustomerBillingAccount", {
            arguments: {
                "$xml": "<staffName>LWUser</staffName>" +
                "<accountNumber>" + billingAccountNumber + "</accountNumber>" +
                "<depositAmount>" + depositAmount + "</depositAmount>" +
                "<depositName>" + reason + "</depositName>" +
                "<depositType>" + reason + "</depositType>" +
                "<reason>" + reason + "</reason>"
            }
        }, undefined, callback);
    };
    getBillingAccountDetail (billingAccountNumber, callback) {
        connector.makeSoapCall(connector.apiCrestelCAAMAccountOperation, "getBillingAccountDetail", {
            arguments: {
                accountNumber: billingAccountNumber
            }
        }, undefined, callback);
    };
    updateCustomerAccountData (args, callback) {
        connector.makeSoapCall(connector.apiCrestelCAAMCustomerAccount2Operations, "updateCustomerAccountData", {arguments: args}, undefined, callback);
    };
    updateBillingAccountData (args, callback) {
        connector.makeSoapCall(connector.apiCrestelCAAMCustomerAccount2Operations, "updateBillingAccount", {arguments: args}, undefined, callback);
    };
    updateServiceAccountData (args, callback) {
        connector.makeSoapCall(connector.apiCrestelCAAMCustomerAccount2Operations, "updateServiceAccount", {arguments: args}, undefined, callback);
    };
    updateBillingAccount (args, callback) {
        connector.makeSoapCall(connector.apiCrestelCAAMAccount2Operation, "updateBillingAccount", {arguments: args}, undefined, callback);
    };

    updateServiceAccount (args, callback) {
        connector.makeSoapCall(connector.apiCrestelCAAMAccount2Operation, "updateServiceAccount", {arguments: args}, undefined, callback);
    };
    changeServiceInstanceStatus (args, callback) {
        connector.makeSoapCall(connector.apiCrestelCAAMServiceOperations, "changeServiceInstanceStatus", {arguments: args}, undefined, callback);
    };
//endregion

//region Bill, Payments, Deposits

    searchBill (accountNumber, fromDate, toDate, callback) {
        connector.makeSoapCall(connector.apiCrestelBECommon, "searchBill", {
            arguments: {
                accountNumber: accountNumber,
                fromDate: fromDate,
                toDate: toDate
            }
        }, undefined, callback);
    };
    searchCreditNote (args, callback) {
        connector.makeSoapCall(connector.apiCrestelPaymentManager, "searchCreditNote", {arguments: args}, undefined, callback);
    };
    makeDebitPaymentDataRequest (accountNumber, transactionAmount, documentNumber, paymentDate,channelAlias, callback) {
        connector.makeSoapCall(connector.apiCrestelPaymentManager, "makeDebitPayment", {
            makeDebitPaymentDataRequest: {
                accountCurrencyAlias: "SGD",
                accountList: {
                    "$xml": "<accountName>" + accountNumber + "</accountName>" +
                    "<accountNumber>" + accountNumber + "</accountNumber>" +
                    "<transactionAmount>" + transactionAmount + "</transactionAmount>" +
                    "<adjustmentList><documentNumber>" + documentNumber + "</documentNumber></adjustmentList>"
                },
                description: "Debit Payment",
                documentAlias: "DEBIT_PAYMENT",
                paymentCurrencyAlias: "SGD",
                paymentDate: paymentDate,
                receivableChannelAlias: channelAlias,
                receivableChannelName: channelAlias,
                OnlineMethod: {
                    "$xml": "<paymentMode>" + "ONLINE" + "</paymentMode>" +
                    "<instituteBranchName>" + "singapore" + "</instituteBranchName>" +
                    "<instituteMasterId>" + "1" + "</instituteMasterId>" +
                    "<instituteMasterName>" + "LW_Test_Payment_Institute" + "</instituteMasterName>"
                },
                staffCurrencyAlias: "SGD",
                staffId: "S000103",
                staffName: "LWUser"
            }
        }, undefined, callback);
    };
    makeAdvancePaymentDataRequest (accountNumber, transactionAmount, paymentDate,
                                             channelAlias, description, callback) {
        connector.makeSoapCall(connector.apiCrestelPaymentManager, "makeAdvancePayment", {
            makeAdvancePaymentDataRequest: {
                accountCurrencyAlias: "SGD",
                accountList: {
                    "$xml": "<accountName>" + accountNumber + "</accountName>" +
                    "<accountNumber>" + accountNumber + "</accountNumber>" +
                    "<transactionAmount>" + transactionAmount + "</transactionAmount>"
                },
                description: description ? description : "Payment",
                documentAlias: "ADVANCE_PAYMENT",
                paymentCurrencyAlias: "SGD",
                paymentDate: paymentDate,
                receivableChannelAlias: channelAlias,
                receivableChannelName: channelAlias,
                OnlineMethod: {
                    "$xml": "<paymentMode>" + "ONLINE" + "</paymentMode>" +
                    "<instituteBranchName>" + "singapore" + "</instituteBranchName>" +
                    "<instituteMasterId>" + "1" + "</instituteMasterId>" +
                    "<instituteMasterName>" + "LW_Test_Payment_Institute" + "</instituteMasterName>"
                },
                staffCurrencyAlias: "SGD",
                staffId: "S000103",
                staffName: "LWUser"
            }
        }, undefined, callback);
    };
    searchPayment (args, callback) {
        connector.makeSoapCall(connector.apiCrestelPaymentManager, "searchPayment", {searchPaymentData: args}, undefined, callback);
    };
    createCreditNote (args, callback) {
        connector.makeSoapCall(connector.apiCrestelPaymentManager, "createCreditNote", {createCreditNoteDataRequest: args}, undefined, callback);
    };


//endregion

//region Other, Customer, Porting, Notifications

    /**
     * Reason codes
     * 1 - Cancellation due to customer request.
     * 2 - Cancellation due to RNO request.
     * 3 - Cancellation due to overdue of Approval Window.
     */
    cancelPortInOrder (MSISDN, reasonCode, callback) {
        connector.makeSoapCall(connector.apiLWWSServices, "CancelPortInOrder", {
            arg0: {
                MSISDN: MSISDN,
                reasonCode: reasonCode,
                reasonText: "N/A"
            }
        }, undefined, callback);
    };
    /**
     *  Fake success port in M1 -> EC notifications
     */
    mnpNotification (MSISDN, callback) {
        connector.makeSoapCall(connector.apiLWWSServices, "MNPNotification", {
            arg0: {
                arguments: {
                    MSISDN: MSISDN,
                    notificationType: "PortIn_Success",
                    staffName: "M1User",
                    statusCode: "33",
                    statusMessage: "N/A"
                }
            }
        }, undefined, callback);
    };
    serviceProvisioningNotification (externalRefId, inventoryNumber, callback) {
        if (!inventoryNumber) inventoryNumber = "0";
        connector.makeSoapCall(connector.apiLWWSServices, "ServiceProvisioningNotification", {
            arg0: {
                arguments: {
                    externalRefId: externalRefId,
                    inventoryNumber: inventoryNumber,
                    inventoryStatus: "N/A",
                    reason: "N/A",
                    staffName: "M1User",
                    status: "0"
                }, params: {
                    param: {
                        key: "Notification_Type",
                        value: "HLRCreateSubscriber_Success"
                    }
                }
            }
        }, undefined, callback);
    };
    viewCustomer (serviceInstanceNumber, fromDate, callback) {
        connector.makeSoapCall(connector.apiIntegrationWebService, "viewCustomer", {
            arg1: {
                subscriberIdentifier: serviceInstanceNumber,
                fromDate: fromDate
            }
        }, undefined, callback);
    };
    viewCustomerUsageInformation (args, callback) {
        connector.makeSoapCall(connector.apiIntegrationWebService, "viewCustomerUsageInformation", {arg0: '2', arg1: args}, undefined, callback);
    };
    updateCustomerBucket (args, callback) {
        connector.makeSoapCall(connector.apiIntegrationWebService, "updateCustomer", {arg0: 4, arg1 : args}, undefined, callback);
    };
//endregion

    ///GET PROMISE METHOD GOES HERE

}

const soapClient = new SoapClient();
module.exports = soapClient;