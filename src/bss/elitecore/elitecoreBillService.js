var async = require('async');
var request = require('request');
var common = require(global.__lib + '/common');
var db = require(global.__lib + '/db_handler');
var config = require(global.__base + '/config');

var elitecoreConnector = require('./elitecoreConnector');
var elitecoreUtils = require('./elitecoreUtils');
var elitecoreUserService = require('./elitecoreUserService');

var log = config.EC_LOGSENABLED;
var loadFromDB = !config.EC_STAGING;
var cacheTime = 10 * 60 * 1000;

var ocsSchema = (config.EC_STAGING) ? "crestelocstestlwp" : "crestelocslwp";
var beSchema = (config.EC_STAGING) ? "crestelbetestlwp" : "crestelbelwp";
var paymentSchema = (config.EC_STAGING) ? "crestelpaymenttestlwp" : "crestelpaymentlwp";

module.exports = {

    /**
     *
     */
    loadBillInvoices: function (monthDate, callback) {
        if (!monthDate) {
            if (callback) callback(new Error("Invalid params"));
            return;
        }
        loadBillInvoices(undefined, monthDate, false, callback);
    },

    /**
     *
     */
    loadUnpaidBillInvoices: function (callback) {
        loadBillInvoices(undefined, undefined, true, callback);
    },

    /**
     *
     */
    loadCustomerUsage: function (serviceInstance, callback) {
        loadCustomerUsage(serviceInstance, callback);
    },

    /**
     *
     */
    loadBillInvoicesByIds: function (invoiceIds, callback) {
        if (!invoiceIds || !invoiceIds[0]) {
            if (callback) callback(new Error("Invalid params"));
            return;
        }
        loadBillInvoices(invoiceIds, undefined, false, callback);
    },

    /**
     *
     */
    clearCustomerPaymentsCache: function (billingAccountNumber) {
        var cacheKey = "ban_" + billingAccountNumber + "_payment_info";
        db.cache_del("account_cache", cacheKey);
        db.cache_del("account_cache_micro", cacheKey);
    },

    /**
     *
     */
    loadCustomerPaymentsInfo: function (billingAccountNumber, callback, fresh, basic) {
        elitecoreConnector.loadBssMode(function (err, result) {
            if ((result && result.offline) || (!config.DEV && basic && loadFromDB)) {
                loadBackupCustomerPaymentsInfo(billingAccountNumber, callback, fresh);
            } else {
                loadBssCustomerPaymentsInfo(billingAccountNumber, callback, fresh);
            }
        });
    }
}

function loadBackupCustomerPaymentsInfo(billingAccountNumber, callback, fresh) {
    if (!billingAccountNumber) {
        if (callback) callback(new Error("Invalid params"));
        return;
    }

    var cacheKey = "ban_" + billingAccountNumber + "_payment_info";
    var loadFresh = () => {

        elitecoreUserService.loadUserInfoByBillingAccountNumber(billingAccountNumber, (err, cache) => {
            if (err) {
                return callback(err);
            }
            if (!cache) {
                return callback(new Error("Customer not found"));
            }

            var currentDate = new Date();
            var currentCycle = elitecoreUtils.calculateBillCycle(cache.billing_cycle, undefined, currentDate);
            var currentCycleStartDate = currentCycle.startDateUTC;
            var activationDate = new Date(cache.serviceInstanceCreationDate);

            var fromCycleDateStr = currentCycleStartDate && currentCycleStartDate.getTime() > 0
                ? currentCycleStartDate.toISOString().split('T')[0] : undefined;
            var fromActivationDateStr = activationDate && activationDate.getTime() > 0
                ? activationDate.toISOString().split('T')[0] : undefined;
            var currentDateStr = currentDate && currentDate.getTime() > 0
                ? currentDate.toISOString().split('T')[0] : undefined;

            var tasks = [];
            tasks.push(function (callback) {
                var query = "SELECT ca_number, ba_number, si_number, emailid, name, debitdocumentnumber, billdate, " +
                    " status, totalamount, leftamount, billtype, dateofbirth FROM " + ocsSchema + ".LWP_Billview " +
                    " WHERE ba_number=" + db.escape(billingAccountNumber);

                db.oracle.query(query, function (err, result) {
                    if (err) {
                        return callback(err);
                    }

                    var bills = [];
                    result.rows.forEach((item) => {
                        bills.push({
                            amount: item[8] + item[9],
                            billDate: item[6],
                            billNumber: item[5],
                            billStatus: item[7],
                            documentType: 'REGULAR_INVOICE',
                            unpaidAmount: item[9]
                        });
                    });

                    return callback(undefined, {billsResult: {objList: bills}});
                });
            });
            tasks.push(function (callback) {
                var query = "select a.TOTALAMOUNT, a.CURRENCYNAME, 'CREDIT' DocumentCategory, " +
                    "a.CREDITDOCUMENTTYPENAME, a.CREDITDOCUMENTNUMBER,a.CREATEDATE from " + paymentSchema + ".TBLTCREDITDOCUMENT a " +
                    "where accountnumber = " + db.escape(billingAccountNumber) +
                    " UNION select a.TOTALAMOUNT,a.CURRENCYALIAS, 'DEBIT' DocumentCategory, " +
                    "'Regular Invoice' CREDITDOCUMENTTYPENAME, a.DEBITDOCUMENTNUMBER,a.CREATEDATE " +
                    "from " + beSchema + ".tbltdebitdocument a where accountnumber = " + db.escape(billingAccountNumber);

                db.oracle.query(query, function (err, result) {
                    if (err) {
                        return callback(err);
                    }

                    var statementsResult = {
                        creditDocumentVO: [],
                        debitDocumentVO: []
                    }
                    var statementsCurrentCycleResult = {
                        creditDocumentVO: [],
                        debitDocumentVO: []
                    }

                    result.rows.forEach((item) => {
                        var document = {
                            amount: item[0],
                            currency: item[1],
                            documentCategory: item[2],
                            documentType: item[3],
                            documentnumber: item[4],
                            postDate: item[5]
                        }

                        var currentCycle = new Date(document.postDate).getTime() > currentCycleStartDate.getTime();
                        if (document.documentCategory == 'CREDIT') {
                            statementsResult.creditDocumentVO.push(document);
                            if (currentCycle) {
                                statementsCurrentCycleResult.creditDocumentVO.push(document);
                            }
                        } else if (document.documentCategory == 'DEBIT') {
                            statementsResult.debitDocumentVO.push(document);
                            if (currentCycle) {
                                statementsCurrentCycleResult.creditDocumentVO.push(document);
                            }
                        }
                    });

                    return callback(undefined, {
                        statementsResult: statementsResult,
                        statementsCurrentCycleResult: statementsCurrentCycleResult
                    });
                });
            });

            async.parallelLimit(tasks, 3, function (err, asyncResult) {
                if (err) {
                    return callback(err);
                }

                var billsResult;
                var statementsResult;
                var statementsCurrentCycleResult;
                asyncResult.forEach(function (item) {
                    if (item.billsResult) billsResult = item.billsResult;
                    if (item.statementsResult) statementsResult = item.statementsResult;
                    if (item.statementsCurrentCycleResult) statementsCurrentCycleResult = item.statementsCurrentCycleResult;
                });

                var result = {
                    bills: billsResult,
                    statements: {
                        all: statementsResult,
                        currentCycle: statementsCurrentCycleResult
                    },
                    backupData: true,
                    cachedAt: new Date().toISOString()
                }

                db.cache_put("account_cache_micro", cacheKey, result, cacheTime);
                callback(undefined, result);
            });
        });
    }

    if (fresh) {
        return loadFresh();
    }

    db.cache_get("account_cache_micro", cacheKey, function (value) {
        if (!value) return loadFresh();
        callback(undefined, value);
    });
}

function loadBssCustomerPaymentsInfo(billingAccountNumber, callback, fresh) {
    if (!billingAccountNumber) {
        if (callback) callback(new Error("Invalid params"));
        return;
    }

    var cacheKey = "ban_" + billingAccountNumber + "_payment_info";
    var loadFresh = () => {

        elitecoreUserService.loadUserInfoByBillingAccountNumber(billingAccountNumber, (err, cache) => {
            if (err) {
                return callback(err);
            }
            if (!cache) {
                return callback(new Error("Customer not found"));
            }

            var currentDate = new Date();
            var currentCycle = elitecoreUtils.calculateBillCycle(cache.billing_cycle, undefined, currentDate);
            var currentCycleStartDate = currentCycle.startDateUTC;
            var activationDate = new Date(cache.serviceInstanceCreationDate);

            var fromCycleDateStr = currentCycleStartDate && currentCycleStartDate.getTime() > 0
                ? currentCycleStartDate.toISOString() : undefined;
            var fromActivationDateStr = activationDate && activationDate.getTime() > 0
                ? activationDate.toISOString() : undefined;
            var currentDateStr = currentDate && currentDate.getTime() > 0
                ? currentDate.toISOString() : undefined;

            var tasks = [];
            tasks.push(function (callback) {
                elitecoreConnector.searchBill(billingAccountNumber, fromActivationDateStr, currentDateStr, (err, result) => {
                    if (err) {
                        return callback(err);
                    }
                    if (!result || !result.return) {
                        return callback(new Error("Invalid 'getAccountStatement' BSS response for BAN " + billingAccountNumber));
                    }
                    return callback(undefined, {billsResult: result.return});
                });
            });
            tasks.push(function (callback) {
                elitecoreConnector.getAccountStatement(billingAccountNumber, fromActivationDateStr, currentDateStr, (err, result) => {
                    if (err) {
                        return callback(err);
                    }
                    if (!result || !result.return) {
                        return callback(new Error("Invalid 'getAccountStatement' BSS response for BAN " + billingAccountNumber));
                    }
                    return callback(undefined, {statementsResult: result.return});
                });
            });
            tasks.push(function (callback) {
                elitecoreConnector.getAccountStatement(billingAccountNumber, fromCycleDateStr, currentDateStr, (err, result) => {
                    if (err) {
                        return callback(err);
                    }
                    if (!result || !result.return) {
                        return callback(new Error("Invalid 'getAccountStatement' BSS response for BAN " + billingAccountNumber));
                    }
                    return callback(undefined, {statementsCurrentCycleResult: result.return});
                });
            });

            async.parallelLimit(tasks, 3, function (err, asyncResult) {
                if (err) {
                    return callback(err);
                }

                var billsResult;
                var statementsResult;
                var statementsCurrentCycleResult;
                asyncResult.forEach(function (item) {
                    if (item.billsResult) billsResult = item.billsResult;
                    if (item.statementsResult) statementsResult = item.statementsResult;
                    if (item.statementsCurrentCycleResult) statementsCurrentCycleResult = item.statementsCurrentCycleResult;
                });

                var result = {
                    bills: billsResult,
                    statements: {
                        all: statementsResult,
                        currentCycle: statementsCurrentCycleResult
                    },
                    backupData: false,
                    cachedAt: new Date().toISOString()
                }

                db.cache_put("account_cache", cacheKey, result, cacheTime);
                callback(undefined, result);
            });
        });
    }

    if (fresh) {
        return loadFresh();
    }

    db.cache_get("account_cache", cacheKey, function (value) {
        if (!value) return loadFresh();
        callback(undefined, value);
    });
}

function loadBillInvoices(invoiceIds, monthDate, unpaidOnly, callback) {
    var invoiceWhereIn = "";
    if (invoiceIds) {
        invoiceIds.forEach((item) => {
            if (invoiceWhereIn) invoiceWhereIn += ',';
            invoiceWhereIn += db.escape(item);
        });
    }

    var query = "SELECT ca_number, ba_number, si_number, emailid, name, debitdocumentnumber, billdate, " +
        " status, totalamount, leftamount, billtype, dateofbirth FROM " + ocsSchema + ".LWP_Billview WHERE 1=1" +
        (monthDate ? " AND billdate=TO_TIMESTAMP(" + db.escape(monthDate) + ", 'YYYY-MM-DD HH24:MI:SS.xff')" : "") +
        (unpaidOnly ? " AND (status='PARTIALLY_PAID' OR status='UNPAID')" : "") +
        (invoiceWhereIn ? " AND debitdocumentnumber IN (" + invoiceWhereIn + ")" : "");

    var startTime = new Date().getTime();
    //console.log(query);
    db.oracle.query(query, function (err, results) {
        var time = new Date().getTime() - startTime;

        if (err) {
            common.error("ElitecoreBillService", "loadBillInvoices: failed to load bills data " +
            "from DB, time=" + time + ", error=" + err.message);
            if (callback) callback(err);
            return;
        }

        var rows;
        if (results.rows) {
            rows = results.rows;
        } else {
            rows = results;
        }

        var bills = [];

        if (rows) {
            if (rows.forEach) {
                common.log("ElitecoreBillService", "loadBillInvoices: loaded bills " +
                "from DB, time=" + time + ", count=" + rows.length);

                rows.forEach((item) => {
                    var billType = item[10];
                    var billPwd = getPdfPassword(item[11]);

                    // bill can have 2 types: PAID, UNPAID, NEGATIVE
                    // 1. all bills with status PARTIALLY_PAID bills in BSS are considered as UNPAID in CMS
                    // 2. all bills with status PAID and type NEGATIVE in BSS are considered NEGATIVE in CMS
                    // ** PARTIALLY_PAID status have bills with advanced payments

                    bills.push({
                        customerAccountNumber: item[0],
                        billingAccountNumber: item[1],
                        serviceInstanceNumber: item[2],
                        email: item[3],
                        name: item[4],
                        invoiceId: item[5],
                        date: item[6],
                        type: billType == "NEGATIVE" && "PAID" ? "NEGATIVE" : (item[7] == "PARTIALLY_PAID"
                            ? "UNPAID" : (item[7] ? item[7] : "UNKNOWN")),
                        totalAmount: item[8],
                        leftAmount: item[9],
                        billPwd: billPwd
                    });
                });
            } else {
                common.error("ElitecoreBillService", "loadBaseAllWithoutCaching: result is not an array="
                + JSON.stringify(rows));
            }
        }

        if (callback) {
            callback(undefined, bills);
        }
    });
}

function loadCustomerUsage(serviceInstance, callback) {
    var query = "SELECT sum(accountedcost) FROM (SELECT " +
        " (CASE WHEN transtype!=2 THEN accountedcost ELSE (0-accountedcost) END) as accountedcost FROM " +
        " (SELECT a.transtype, nvl(a.accountedcost,0) accountedcost " +
        " FROM " + ocsSchema + ".tblcdrpostpaid a, " + ocsSchema + ".ab_hierarchy b" +
        " WHERE b.si_number = " + db.escape(serviceInstance) +
        " AND a.debitedaccountid = si_id" +
        " AND a.accountedcost > 0" +
        " AND processdate >= TRUNC(SYSDATE, 'MM')" +
        " AND processdate <= LAST_DAY(TRUNC (SYSDATE,'MM'))+1))";

    var startTime = new Date().getTime();
    db.oracle.query(query, function (err, results) {
        var time = new Date().getTime() - startTime;

        if (err) {
            common.error("ElitecoreUserService", "loadCustomerUsage: failed to load usage " +
            "from DB, time=" + time + ", error=" + err.message);
            if (callback) callback(err);
            return;
        }

        if (!results || !results.rows || !results.rows[0]) {
            common.error("ElitecoreUserService", "loadCustomerUsage: failed to find usage " +
            "from DB, time=" + time + ", error='Empty response'");
            if (callback) callback(new Error("Empty response"));
            return;
        }

        return callback(undefined, {amountCents: results.rows[0][0] > 0 ? parseInt(results.rows[0][0]) : 0});
    });
}

function getPdfPassword(birthday) {
    if (!birthday) {
        return undefined;
    }
    if (birthday.toISOString) {
        birthday = birthday.toISOString();
    }
    if (!birthday.split) {
        return undefined;
    }

    if (birthday) {
        var dobParts = birthday.split("T")[0].split("-");
        var pdfPassword = dobParts.length == 3 ? (dobParts[2] + dobParts[1] + dobParts[0]) : "";
        return pdfPassword;
    } else {
        return "";
    }
}
