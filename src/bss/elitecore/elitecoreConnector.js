var soap = require('soap');
var async = require('async');
var dateformat = require('dateformat');
var common = require(global.__lib + '/common');
var config = require(global.__base + '/config');
var elitecoreUtils = require('./elitecoreUtils');
const configManager = require(`${global.__manager}/config/configManager`);

var log = config.EC_LOGSENABLED;
var initDone = false;
var initInProgress = false;
var initCallbacks = [];

var reInitDelay = 10 * 60 * 1000;
var statsDelay = 1 * 60 * 1000;
var quarantineTimeoutsLimit = 150;
var quarantineDelay = 5 * 60 * 1000;
var countRequests = 0;
var countTimeouts = 0;
var countErrors = 0;

var wsdlClients = {};
var statistics = {
    hosts: {},
    listeners: []
};

var apiProductManagerAPIWebService
    = "ProductManagerAPIWebService";
var apiProductManagerAPIWebService_v3
    = "ProductManagerAPIWebService_v3";
var apiCrestelInventoryOperation
    = "crestelinventory/InventoryWebService/CrestelInventoryOperationsService/InventoryOperation";
var apiCrestelCAAMInventoryOperationsService
    = "crestelcaam/ServiceOperationsWebService/CrestelCAAM/CrestelCAAMInventoryOperationsService";
var apiCrestelCAAMServiceOperations
    = "crestelcaam/ServiceOperationsWebService/CrestelCAAMServiceOperationsService/CrestelCAAMServiceOperations";
var apiCrestelCAAMCustomerAccount2Operations
    = "crestelcaam/CustomerOperationsWebService/CrestelCAAMCustomerAccountOperations2Service/CrestelCAAMCustomerAccount2Operations";
var apiCrestelCAAMAccountOperation
    = "crestelcaam/AccountOperationsWebService/CrestelCAAMAccountOperationService/CrestelCAAMAccountOperation";
var apiCrestelCAAMAccount2Operation
    = "crestelcaam/AccountOperationsWebService/CrestelCAAMAccount2OperationService/CrestelCAAMAccount2Operation";
var apiCrestelBECommon
    = "crestelBECommon/billcommon/CrestelBECommonService/CrestelBECommon";
var apiCrestelPaymentManager
    = "crestelpayment/service/CrestelPaymentManagerService/CrestelPaymentManager";
var apiIntegrationWebService
    = "Integrations/IntegrationWebService";
var apiLWWSServices
    = "LWWS/services";

var disabledBss = [];
var bssOfflineMode = false;

module.exports = {

    setDisabledBss: (bssList) => {
        disabledBss = bssList;
    },

    setOfflineMode: (offline) => {
        bssOfflineMode = offline;
    },

    getCountTimeouts: function () {
        return countTimeouts;
    },

    getCountRequests: function () {
        return countRequests;
    },

    getCountErrors: function () {
        return countErrors;
    },

    /**
     *
     */
    getDefaultConfig: function () {
        return {
            bssHosts: [config.BSS1_HOST, config.BSS2_HOST, config.BSS3_HOST, config.BSS4_HOST],
            bssPort1: config.BSS_SERVICE_1_PORT,
            bssPort2: config.BSS_SERVICE_2_PORT,
            bssPort3: config.BSS_SERVICE_3_PORT,
            ocsHosts: [config.OCS1_HOST, config.OCS2_HOST],
            ocsPort: config.OCS_PORT,
            fqdn: config.MYFQDN
        }
    },

    getEndpoint: function (path) {
        if (!wsdlClients[path]) {
            wsdlClients[path] = {
                path: path,
                clients: [],
                errors: {},
                pendingCallbacks: []
            };
        }
        return wsdlClients[path];
    },

    /**
     *
     */
    addStatsUpdateListener: function (l) {
        var key = new Date().getTime() + "_" + Math.random();

        statistics.listeners.push({
            key: key,
            callback: l
        });

        return key;
    },

    /**
     *
     */
    removeStatsUpdateListener: function (key) {
        for (var i = 0; i < statistics.listeners.length; i++) {
            var obj = statistics.listeners[i];
            if (obj.key === key) {
                statistics.listeners.splice(i, 1);
                return true;
            }
        }
        return false;
    },

    /**
     *
     */
    loadBssMode: function (callback) {
        if (!callback) callback = () => {
        }

        var bssOff = bssOfflineMode ? true : false;
        callback(undefined, {offline: bssOff});
    },

    /**
     *
     */
    init: function (callback) {
        if (initInProgress) {
            if (log) common.log("EliteCoreConnector", "init: initialisation is in progress");
            if (callback) initCallbacks.push(callback);
            return;
        } else if (initDone) {
            if (callback) callback(wsdlClients);
            return;
        }

        initInProgress = true;
        initCallbacks.push(callback);

        var config = module.exports.getDefaultConfig();
        var initParams = [
            {hosts: config.bssHosts, port: config.bssPort1, path: apiProductManagerAPIWebService, ignoredNamespaces: true},
            {hosts: config.bssHosts, port: config.bssPort1, path: apiProductManagerAPIWebService_v3, ignoredNamespaces: true},
            {hosts: config.bssHosts, port: config.bssPort2, path: apiCrestelInventoryOperation, ignoredNamespaces: true},
            {hosts: config.bssHosts, port: config.bssPort2, path: apiCrestelCAAMInventoryOperationsService, ignoredNamespaces: true},
            {hosts: config.bssHosts, port: config.bssPort2, path: apiCrestelCAAMServiceOperations, ignoredNamespaces: true},
            {hosts: config.bssHosts, port: config.bssPort2, path: apiCrestelCAAMCustomerAccount2Operations, ignoredNamespaces: true},
            {hosts: config.bssHosts, port: config.bssPort2, path: apiCrestelCAAMAccountOperation, ignoredNamespaces: true},
            {hosts: config.bssHosts, port: config.bssPort2, path: apiCrestelCAAMAccount2Operation, ignoredNamespaces: true},
            {hosts: config.bssHosts, port: config.bssPort2, path: apiCrestelBECommon, ignoredNamespaces: true},
            {hosts: config.bssHosts, port: config.bssPort2, path: apiCrestelPaymentManager, ignoredNamespaces: true},
            {
                hosts: config.bssHosts, port: config.bssPort3, path: apiLWWSServices, ignoredNamespaces: true, overrideRootElement: true,
                targetNSAlias: "ws"
            },
            {hosts: config.ocsHosts, port: config.ocsPort, path: apiIntegrationWebService, ignoredNamespaces: true}
        ];

        var enableRecurrentEvents = function () {
            setInterval(function () {
                reInitWSDLClients();
            }, reInitDelay);

            setInterval(function () {
                calculateStats();
            }, statsDelay);
        }

        var loadedCount = 0;
        var handleResult = function (err) {
            loadedCount++;

            if (err) common.error("EliteCoreConnector", "init: failed to init an group, error=" + err.message);
            if (log) common.log("EliteCoreConnector", "init: loaded count=" + loadedCount + ", totalCount=" + initParams.length);

            if (loadedCount == initParams.length) {
                var callbacks = initCallbacks;

                initDone = true;
                initInProgress = false;
                initCallbacks = [];

                enableRecurrentEvents();
                callbacks.forEach(function (callback) {
                    callback(wsdlClients);
                });
            }
        }

        initParams.forEach(function (item) {
            initWSDLClients(item.hosts, item.port, item.path, item.ignoredNamespaces,
                item.overrideRootElement, item.targetNSAlias, handleResult);
        });
    },

    /**
     *
     */
    makeSoapCall: function (path, method, parameters, options, callback) {
        module.exports.loadBssMode(function (err, result) {
            if (err) return callback(err);
            if (result && result.offline) return callback(new Error("BSS is off, SOAP calls are not allowed, method=" + method));

            module.exports.init(function (wsdlClients) {
                internalSoapCall(wsdlClients[path], method, parameters, options, callback);
            });
        });
    },

    /**
     *
     */
    getProductOfferDetails: function (callback) {
        module.exports.makeSoapCall(apiProductManagerAPIWebService, "getProductOfferDetails", {}, undefined, callback);
    },

    /**
     *
     */
    getSpecificProdOfferDetails: function (args, callback) {
        module.exports.makeSoapCall(apiProductManagerAPIWebService, "getSpecificProdOfferDetails", {arg0: {productIds: args}}, undefined, callback);
    },

    /**
     *
     */
    getBusinessHierarchyDetail: function (callback) {
        module.exports.makeSoapCall(apiProductManagerAPIWebService_v3, "getBusinessHierarchyDetail", {arg0: {}}, undefined, callback);
    },

    /**
     *
     */
    searchInventory: function (inventoryNumber, callback) {
        module.exports.makeSoapCall(apiCrestelInventoryOperation, "searchInventory",
            {arguments: {inventoryNumber: inventoryNumber}}, undefined, callback);
    },

    /**
     *
     */
    searchInventoryList: function (inventoryStatusId, inventorySubTypeId, callback) {
        module.exports.makeSoapCall(apiCrestelInventoryOperation, "searchInventory",
            {arguments: {inventoryStatusId: inventoryStatusId, inventorySubTypeId: inventorySubTypeId, ownerId: "Operator"}}, undefined, callback);
    },

    /**
     *
     */
    doInventoryStatusOperation: function (inventoryNumber, operationAlias, callback) {
        module.exports.makeSoapCall(apiCrestelInventoryOperation, "doInventoryStatusOperation", {
            arguments: {
                inventoryNumber: inventoryNumber,
                operationAlias: operationAlias,
                referenceNumber: "N/A"
            }
        }, undefined, callback);
    },

    /**
     *
     */
    repairInventoryRebundle: function (args, callback) {
        module.exports.makeSoapCall(apiCrestelInventoryOperation, "repairInventory", {arg0: args}, undefined, callback);
    },

    /**
     *
     */
    getInventoryDetail: function (args, callback) {
        module.exports.makeSoapCall(apiCrestelInventoryOperation, "getInventoryDetail", {arguments: args}, undefined, callback);
    },

    /**
     *
     */
    listInventory: function (serviceInstanceNumber, callback) {
        module.exports.makeSoapCall(apiCrestelCAAMInventoryOperationsService, "listInventory", {
            arguments: {
                serviceInstanceNumber: serviceInstanceNumber
            }
        }, undefined, callback);
    },

    /**
     *
     */
    unpairInventory: function (args, callback) {
        module.exports.makeSoapCall(apiCrestelCAAMInventoryOperationsService, "unpairInventory", {arguments: args}, undefined, callback);
    },

    /**
     *
     */
    repairInventory: function (args, callback) {
        module.exports.makeSoapCall(apiCrestelCAAMInventoryOperationsService, "repairInventory", {arguments: args}, undefined, callback);
    },

    /**
     *
     */
    repairInventoryXml: function (args, callback) {
        module.exports.makeSoapCall(apiCrestelCAAMInventoryOperationsService, "repairInventory", {arguments: args}, undefined, callback);
    },

    /**
     *
     */
    removeInventory: function (serviceInstanceNumber, inventoryNumber, callback) {
        module.exports.makeSoapCall(apiCrestelCAAMInventoryOperationsService, "removeInventory", {
            arguments: {
                serviceInstanceNumber: serviceInstanceNumber,
                inventoryNumber: inventoryNumber
            }
        }, undefined, callback);
    },

    /**
     *
     */
    addInventory: function (effectDate, inventoryNumber, serviceInstanceNumber, callback) {
        var ecDate = dateformat(new Date(effectDate ? effectDate : new Date()), "yyyy-mm-dd");
        module.exports.makeSoapCall(apiCrestelCAAMInventoryOperationsService, "addInventory", {
            arguments: {
                effectDate: ecDate,
                inventoryNumber: inventoryNumber,
                inventorySubtype: "MSISDN",
                isManualInventory: "N",
                packageId: "PRD00401",    // base plan hard coded for now
                serviceInstanceNumber: serviceInstanceNumber
            }
        }, undefined, callback);
    },

    /**
     *
     */
    markInventoryAsDamaged: function (inventoryNumber, serviceInstanceNumber, callback) {
        module.exports.makeSoapCall(apiCrestelCAAMInventoryOperationsService, "markInventoryAsDamaged", {
            arguments: {
                inventoryNumber: inventoryNumber,
                serviceInstanceNumber: serviceInstanceNumber
            }
        }, undefined, callback);
    },

    /**
     *
     */
    createAccount: function (args, callback) {
        module.exports.makeSoapCall(apiCrestelCAAMCustomerAccount2Operations, "createCustomer", {arguments: args}, {timeout: 60000}, callback);
    },

    /**
     *
     */
    changeServicePlan: function (billingAccountNumber, effectiveDate, packageName, remarks, serviceInstanceNumber, callback) {
        module.exports.makeSoapCall(apiCrestelCAAMServiceOperations, "changeServicePlan", {
            arguments: {
                billingAccountNumber: billingAccountNumber,
                effectiveDate: effectiveDate,
                packageName: packageName,
                remarks: remarks,
                serviceInstanceNumber: serviceInstanceNumber
            }
        }, undefined, callback);
    },

    /**
     *
     */
    searchAccount: function (number, accountNumber, accountName, serviceUserName, callback) {
        module.exports.makeSoapCall(apiCrestelCAAMCustomerAccount2Operations, "searchAccount", {
            arguments: {
                mobileOrMSISDNNumber: number,
                accountNumber: accountNumber,
                accountName: accountName,
                serviceUserName: serviceUserName
            }
        }, undefined, callback);
    },

    /**
     *
     */
    searchServiceInstance: function (number, accountNumber, accountName, callback) {
        module.exports.makeSoapCall(apiCrestelCAAMCustomerAccount2Operations, "searchAccount", {
            arguments: {
                mobileOrMSISDNNumber: number,
                accountNumber: accountNumber,
                accountName: accountName
            }
        }, undefined, callback);
    },

    /**
     *
     */
    getAccountStatement: function (accountNumber, fromDate, toDate, callback) {
        module.exports.makeSoapCall(apiCrestelCAAMAccountOperation, "getAccountStatement", {
            arguments: {
                accountNumber: accountNumber,
                fromDate: fromDate,
                toDate: toDate
            }
        }, undefined, callback);
    },

    /**
     *
     */
    searchBill: function (accountNumber, fromDate, toDate, callback) {
        module.exports.makeSoapCall(apiCrestelBECommon, "searchBill", {
            arguments: {
                accountNumber: accountNumber,
                fromDate: fromDate,
                toDate: toDate
            }
        }, undefined, callback);
    },

    /**
     *
     */
    searchCreditNote: function (args, callback) {
        module.exports.makeSoapCall(apiCrestelPaymentManager, "searchCreditNote", {arguments: args}, undefined, callback);
    },

    /**
     *
     */
    getBillingAccountDetail: function (billingAccountNumber, callback) {
        module.exports.makeSoapCall(apiCrestelCAAMAccountOperation, "getBillingAccountDetail", {
            arguments: {
                accountNumber: billingAccountNumber
            }
        }, undefined, callback);
    },

    /**
     *
     */
    addDepositToCustomerBillingAccount: function (billingAccountNumber, depositAmount, reason, callback) {
        module.exports.makeSoapCall(apiCrestelCAAMAccountOperation, "addDepositToCustomerBillingAccount", {
            arguments: {
                "$xml": "<staffName>LWUser</staffName>" +
                "<accountNumber>" + billingAccountNumber + "</accountNumber>" +
                "<depositAmount>" + depositAmount + "</depositAmount>" +
                "<depositName>" + reason + "</depositName>" +
                "<depositType>" + reason + "</depositType>" +
                "<reason>" + reason + "</reason>"
            }
        }, undefined, callback);
    },

    /**
     *
     */
    makeDebitPaymentDataRequest: function (accountNumber, transactionAmount, documentNumber, paymentDate,
                                           channelAlias, callback) {
        module.exports.makeSoapCall(apiCrestelPaymentManager, "makeDebitPayment", {
            makeDebitPaymentDataRequest: {
                accountCurrencyAlias: "SGD",
                accountList: {
                    "$xml": "<accountName>" + accountNumber + "</accountName>" +
                    "<accountNumber>" + accountNumber + "</accountNumber>" +
                    "<transactionAmount>" + transactionAmount + "</transactionAmount>" +
                    "<adjustmentList><documentNumber>" + documentNumber + "</documentNumber></adjustmentList>"
                },
                description: "Debit Payment",
                documentAlias: "DEBIT_PAYMENT",
                paymentCurrencyAlias: "SGD",
                paymentDate: paymentDate,
                receivableChannelAlias: channelAlias,
                receivableChannelName: channelAlias,
                OnlineMethod: {
                    "$xml": "<paymentMode>" + "ONLINE" + "</paymentMode>" +
                    "<instituteBranchName>" + "singapore" + "</instituteBranchName>" +
                    "<instituteMasterId>" + "1" + "</instituteMasterId>" +
                    "<instituteMasterName>" + "LW_Test_Payment_Institute" + "</instituteMasterName>"
                },
                staffCurrencyAlias: "SGD",
                staffId: "S000103",
                staffName: "LWUser"
            }
        }, undefined, callback);
    },

    /**
     *
     */
    makeAdvancePaymentDataRequest: function (accountNumber, transactionAmount, paymentDate,
                                           channelAlias, description, callback) {
        module.exports.makeSoapCall(apiCrestelPaymentManager, "makeAdvancePayment", {
            makeAdvancePaymentDataRequest: {
                accountCurrencyAlias: "SGD",
                accountList: {
                    "$xml": "<accountName>" + accountNumber + "</accountName>" +
                    "<accountNumber>" + accountNumber + "</accountNumber>" +
                    "<transactionAmount>" + transactionAmount + "</transactionAmount>"
                },
                description: description ? description : "Payment",
                documentAlias: "ADVANCE_PAYMENT",
                paymentCurrencyAlias: "SGD",
                paymentDate: paymentDate,
                receivableChannelAlias: channelAlias,
                receivableChannelName: channelAlias,
                OnlineMethod: {
                    "$xml": "<paymentMode>" + "ONLINE" + "</paymentMode>" +
                    "<instituteBranchName>" + "singapore" + "</instituteBranchName>" +
                    "<instituteMasterId>" + "1" + "</instituteMasterId>" +
                    "<instituteMasterName>" + "LW_Test_Payment_Institute" + "</instituteMasterName>"
                },
                staffCurrencyAlias: "SGD",
                staffId: "S000103",
                staffName: "LWUser"
            }
        }, undefined, callback);
    },

    /**
     *
     */
    updateCustomerAccountData: function (args, callback) {
        module.exports.makeSoapCall(apiCrestelCAAMCustomerAccount2Operations, "updateCustomerAccountData", {arguments: args}, undefined, callback);
    },

    /**
     *
     */
    updateBillingAccountData: function (args, callback) {
        module.exports.makeSoapCall(apiCrestelCAAMCustomerAccount2Operations, "updateBillingAccount", {arguments: args}, undefined, callback);
    },

    /**
     *
     */
    updateServiceAccountData: function (args, callback) {
        module.exports.makeSoapCall(apiCrestelCAAMCustomerAccount2Operations, "updateServiceAccount", {arguments: args}, undefined, callback);
    },

    /**
     *
     */
    getCustomerHierarchyDetails: function (accountNumber, callback) {
        module.exports.makeSoapCall(apiCrestelCAAMCustomerAccount2Operations, "getCustomerHierarchyDetails",
            {arguments: {accountNumber: accountNumber}}, undefined, callback);
    },

    /**
     *
     */
    updateBillingAccount: function (args, callback) {
        module.exports.makeSoapCall(apiCrestelCAAMAccount2Operation, "updateBillingAccount", {arguments: args}, undefined, callback);
    },

    /**
     *
     */
    updateServiceAccount: function (args, callback) {
        module.exports.makeSoapCall(apiCrestelCAAMAccount2Operation, "updateServiceAccount", {arguments: args}, undefined, callback);
    },

    /**
     *
     */
    subscribeAddOnPackage: function (args, callback) {
        module.exports.makeSoapCall(apiCrestelCAAMServiceOperations, "subscribeAddOnPackage", {arguments: args}, undefined, callback);
    },

    /**
     *
     */
    unSubscribeAddOnPackage: function (args, callback) {
        module.exports.makeSoapCall(apiCrestelCAAMServiceOperations, "unSubscribeAddOnPackage", {arguments: args}, undefined, callback);
    },

    /**
     *
     */
    getFnFMembersDetails: function (args, callback) {
        module.exports.makeSoapCall(apiCrestelCAAMServiceOperations, "getFnFMembersDetails", {arguments: args}, undefined, callback);
    },

    /**
     *
     */
    addFnFMembers: function (args, callback) {
        module.exports.makeSoapCall(apiCrestelCAAMServiceOperations, "addFnFMembers", {arguments: args}, undefined, callback);
    },

    /**
     *
     */
    removeFnFMembers: function (args, callback) {
        module.exports.makeSoapCall(apiCrestelCAAMServiceOperations, "removeFnFMembers", {arguments: args}, undefined, callback);
    },

    /**
     *
     */
    searchPayment: function (args, callback) {
        module.exports.makeSoapCall(apiCrestelPaymentManager, "searchPayment", {searchPaymentData: args}, undefined, callback);
    },

    /**
     *
     */
    createCreditNote: function (args, callback) {
        module.exports.makeSoapCall(apiCrestelPaymentManager, "createCreditNote", {createCreditNoteDataRequest: args}, undefined, callback);
    },

    /**
     *
     */
    modifyPlanAttributes: function (args, callback) {
        module.exports.makeSoapCall(apiCrestelCAAMServiceOperations, "modifyPlanAttributes", {arguments: args}, undefined, callback);
    },

    /**
     *
     */
    changeServiceInstanceStatus: function (args, callback) {
        module.exports.makeSoapCall(apiCrestelCAAMServiceOperations, "changeServiceInstanceStatus", {arguments: args}, undefined, callback);
    },

    /**
     * Reason codes
     * 1 - Cancellation due to customer request.
     * 2 - Cancellation due to RNO request.
     * 3 - Cancellation due to overdue of Approval Window.
     */
    cancelPortInOrder: function (MSISDN, reasonCode, callback) {
        module.exports.makeSoapCall(apiLWWSServices, "CancelPortInOrder", {
            arg0: {
                MSISDN: MSISDN,
                reasonCode: reasonCode,
                reasonText: "N/A"
            }
        }, undefined, callback);
    },

    /**
     *  Fake success port in M1 -> EC notifications
     */
    mnpNotification: function (MSISDN, callback) {
        module.exports.makeSoapCall(apiLWWSServices, "MNPNotification", {
            arg0: {
                arguments: {
                    MSISDN: MSISDN,
                    notificationType: "PortIn_Success",
                    staffName: "M1User",
                    statusCode: "33",
                    statusMessage: "N/A"
                }
            }
        }, undefined, callback);
    },

    /**
     *
     */
    serviceProvisioningNotification: function (externalRefId, inventoryNumber, callback) {
        if (!inventoryNumber) inventoryNumber = "0";
        module.exports.makeSoapCall(apiLWWSServices, "ServiceProvisioningNotification", {
            arg0: {
                arguments: {
                    externalRefId: externalRefId,
                    inventoryNumber: inventoryNumber,
                    inventoryStatus: "N/A",
                    reason: "N/A",
                    staffName: "M1User",
                    status: "0"
                }, params: {
                    param: {
                        key: "Notification_Type",
                        value: "HLRCreateSubscriber_Success"
                    }
                }
            }
        }, undefined, callback);
    },

    /**
     *
     */
    viewCustomer: function (serviceInstanceNumber, fromDate, callback) {
        module.exports.makeSoapCall(apiIntegrationWebService, "viewCustomer", {
            arg1: {
                subscriberIdentifier: serviceInstanceNumber,
                fromDate: fromDate
            }
        }, undefined, callback);
    },

    /**
     *
     */
    viewCustomerUsageInformation: function (args, callback) {
        module.exports.makeSoapCall(apiIntegrationWebService, "viewCustomerUsageInformation", {arg0: '2', arg1: args}, undefined, callback);
    },

    /**
     *
     */
    createInventory: function (msisdn, donorCode, date, callback) {
        var ecDate = dateformat(new Date(date ? date : new Date()), "dd-mm-yyyy");
        module.exports.makeSoapCall(apiLWWSServices, "createInventory", {
            arg0: {
                arguments: {
                    inventoryGroup: "NGP00003",
                    inventorySubGroup: "NTP00007",
                    warehouseName: "WMAS00000001",
                    batchNumber: "NBT000000265",
                    staffName: "LWUser",
                    MSISDN: msisdn,
                    inventoryAttributes: [{
                        key: "PARAM2",
                        value: donorCode
                    }, {
                        key: "PARAM3",
                        value: "Manual"
                    }, {
                        key: "PARAM4",
                        value: ecDate
                    }]
                }
            }
        }, undefined, callback);
    },

    /**
     *
     */
    updateInventoryAttributes: function (msisdn, donorCode, date, callback) {
        var ecDate = dateformat(new Date(date ? date : new Date()), "dd-mm-yyyy");
        module.exports.makeSoapCall(apiLWWSServices, "updateInventoryAttributes", {
            arg0: {
                arguments: {
                    staffName: "LWUser",
                    MSISDN: msisdn,
                    inventoryAttributes: [{
                        key: "PARAM2",
                        value: donorCode
                    }, {
                        key: "PARAM4",
                        value: ecDate
                    }]
                }
            }
        }, undefined, callback);
    },

    /**
     *
     */
    updateCustomerBucket: function (args, callback) {
        module.exports.makeSoapCall(apiIntegrationWebService, "updateCustomer", {arg0: 4, arg1 : args}, undefined, callback);
    },

    /**
     *
     */
    getPromise: (func, params) => {
        return new Promise((resolve, reject) => {
            var funcName = "unknown";
            var callback = (err, result) => {
                if (err) {
                    return reject(err);
                }

                var returnError = (message, code, status, obj) => {
                    var err = new Error(message);
                    err.raw = result;
                    err.status = status;
                    err.code = code;
                    err.obj = obj;
                    err.params = params;
                    err.func = funcName;
                    return reject(err);
                }

                if (!result || !result.return) {
                    return returnError("Result is empty");
                }

                var handleResponse = (responseCode, expectedResponse, invalid) => {
                    if (responseCode != expectedResponse.code) {
                        return false;
                    }

                    var expectedObj = result.return;
                    delete expectedObj.responseCode;


                    for (var j = 0; expectedResponse.keys && j < expectedResponse.keys.length; j++) {

                        if (!expectedObj) {
                            return returnError("Response expected object is not found", responseCode);
                        }

                        var key = expectedResponse.keys[j];
                        if (key.lastIndexOf("$") == 0) {
                            if (key === "$array0") {
                                expectedObj = expectedObj[0];
                            } else {
                                return returnError("System command " + key + " is not supported", responseCode);
                            }
                        } else {
                            expectedObj = expectedObj[key];
                        }
                    }

                    if (!expectedObj) {
                        returnError("Response expected object is empty", responseCode);
                        return true;
                    }

                    // replace keys starting from $ to escaped $ (\\$)
                    elitecoreUtils.replace$Keys(expectedObj);

                    if (invalid) {
                        var message = expectedResponse.message;
                        var status = expectedResponse.status;

                        if (!message) {
                            message = "Response code " + responseCode
                            + " is invalid (" + JSON.stringify(expectedObj) + ")";
                        }

                        var expectedObjKeys = Object.keys(expectedObj);
                        expectedObjKeys.forEach(function (key) {
                            message = message.replace("%" + key + "%", expectedObj[key]);
                        })

                        returnError(message, responseCode, status, expectedObj);
                        return true;
                    } else {
                        resolve({code: responseCode, obj: expectedObj});
                        return true;
                    }
                }

                var responseCode = result.return.responseCode;
                var codeFound = false;
                for (var i = 0; !codeFound && params.validResponses && i < params.validResponses.length; i++) {
                    codeFound = handleResponse(responseCode, params.validResponses[i], false);
                }
                for (var i = 0; !codeFound && params.invalidResponses && i < params.invalidResponses.length; i++) {
                    codeFound = handleResponse(responseCode, params.invalidResponses[i], true);
                }
                if (!codeFound) {
                    returnError("Response contain unsupported or " +
                    "invalid error code " + responseCode, responseCode);
                }
            }

            if (func == module.exports.searchInventory) {
                funcName = "searchInventory";
                func(params.inventoryNumber, callback);
            } else if (func == module.exports.createInventory) {
                funcName = "createInventory";
                func(params.msisdn, params.donorCode, params.date, callback);
            } else if (func == module.exports.updateInventoryAttributes) {
                funcName = "updateInventoryAttributes";
                func(params.msisdn, params.donorCode, params.date, callback);
            } else if (func == module.exports.doInventoryStatusOperation) {
                funcName = "doInventoryStatusOperation";
                func(params.inventoryNumber, params.operationAlias, callback);
            } else if (func == module.exports.markInventoryAsDamaged) {
                funcName = "markInventoryAsDamaged";
                func(params.inventoryNumber, params.serviceInstanceNumber, callback);
            } else if (func == module.exports.searchAccount) {
                funcName = "searchAccount";
                func(params.number, params.accountNumber, params.accountName, params.serviceUserName, callback);
            } else if (func == module.exports.addInventory) {
                funcName = "addInventory";
                func(params.effectDate, params.inventoryNumber, params.serviceInstanceNumber, callback);
            } else if (func == module.exports.removeInventory) {
                funcName = "removeInventory";
                func(params.serviceInstanceNumber, params.inventoryNumber, callback);
            } else if (func == module.exports.listInventory) {
                funcName = "listInventory";
                func(params.serviceInstanceNumber, callback);
            } else if (func == module.exports.serviceProvisioningNotification) {
                funcName = "serviceProvisioningNotification";
                func(params.externalRefId, params.inventoryNumber, callback);
            } else if (func == module.exports.mnpNotification) {
                funcName = "mnpNotification";
                func(params.MSISDN, callback);
            } else if (func == module.exports.makeDebitPaymentDataRequest) {
                funcName = "makeDebitPaymentDataRequest";
                func(params.accountNumber, params.transactionAmount,
                    params.documentNumber, params.paymentDate, params.channelAlias, callback);
            } else if (func == module.exports.makeAdvancePaymentDataRequest) {
                funcName = "makeAdvancePaymentDataRequest";
                func(params.accountNumber, params.transactionAmount,
                    params.paymentDate, params.channelAlias, params.description, callback);
            } else {
                reject(new Error("Not supported promise method"));
            }
        })
    },

    refreshBssSetting: (callback) => {
        if (!callback) callback = () => {
        }

        configManager.loadConfigMapForGroup("elitecore", function (err, items) {
            if (err) {
                return callback(err);
            }

            var offlineMode = false;
            var disabledBss = [];

            if (items) items.forEach((item) => {
                if (item.key == 'elitecore_api' && item.options) {
                    offlineMode = (process.env.ROLE == 'mobile'
                        ? item.options.ec_mobile_bss_api_disabled
                        : item.options.ec_cms_bss_api_disabled);
                }
                if (item.key == 'elitecore_bss_pool' && item.options) {

                    var options = item.options;
                    var currentDate = new Date();
                    if (config.BSS1_HOST && options.bss1_disabled_until
                        && new Date(options.bss1_disabled_until).getTime() > currentDate.getTime()) {
                        disabledBss.push(config.BSS1_HOST);
                    }
                    if (config.BSS2_HOST && options.bss2_disabled_until
                        && new Date(options.bss2_disabled_until).getTime() > currentDate.getTime()) {
                        disabledBss.push(config.BSS2_HOST);
                    }
                    if (config.BSS3_HOST && options.bss3_disabled_until
                        && new Date(options.bss3_disabled_until).getTime() > currentDate.getTime()) {
                        disabledBss.push(config.BSS3_HOST);
                    }
                    if (config.BSS4_HOST && options.bss4_disabled_until
                        && new Date(options.bss4_disabled_until).getTime() > currentDate.getTime()) {
                        disabledBss.push(config.BSS4_HOST);
                    }
                    if (config.DEV && config.BSS1_HOST && options.bssstaging_disabled_until
                        && new Date(options.bssstaging_disabled_until).getTime() > currentDate.getTime()) {
                        disabledBss.push(config.BSS1_HOST);
                    }
                }
            });

            module.exports.setOfflineMode(offlineMode);
            module.exports.setDisabledBss(disabledBss);
            return callback();
        });
    }


}

function promiseResponse() {

}

function reInitWSDLClients() {
    Object.keys(wsdlClients).forEach(function (path) {
        var wsdlClient = wsdlClients[path];

        Object.keys(wsdlClient.errors).forEach(function (url) {
            var error = wsdlClient.errors[url];
            var item = error.info;

            if (log) common.log("EliteCoreConnector", "reInitWSDLClients: an initialization error is found, " +
            "need to re-init, url=" + url);

            initWSDLClients(item.hosts, item.port, item.path,
                item.ignoredNamespaces, item.overrideRootElement);
        });
    });
}

function calculateStats() {
    var aggregated = {};
    var allErrors = [];
    var allTimeouts = [];

    var hostNames = {
        "10.20.3.11": "LWEPC01",
        "10.20.3.12": "LWEPC02",
        "10.20.3.13": "LWBSS01",
        "10.20.3.14": "LWBSS02",
        "10.20.3.16": "LWBSS03",
        "10.20.3.21": "LWBSS04",
        "10.20.3.18": "LWTES01"
    }

    var currentTimeMs = new Date().getTime();
    Object.keys(statistics.hosts).forEach(function (host) {
        var hostStats = statistics.hosts[host];
        var lastRequests = hostStats.lastRequests;
        var lastErrors = hostStats.lastErrors;
        var lastTimeouts = hostStats.lastTimeouts;
        hostStats.lastRequests = [];
        hostStats.lastErrors = [];
        hostStats.lastTimeouts = [];

        lastErrors.forEach(function (error) {
            error.ip = error.host;
            error.host = hostNames[error.host];
            error.status = "ERROR";
            allErrors.push(error);
        });

        lastTimeouts.forEach(function (timeout) {
            timeout.ip = timeout.host;
            timeout.host = hostNames[timeout.host];
            timeout.status = "TIMEOUT";
            allTimeouts.push(timeout);
        });

        lastRequests.forEach(function (request) {
            var key = request.method + "_" + host;
            var aggregatedItem = aggregated[key];
            var hostName = hostNames[request.host];
            if (!hostName) hostName = request.host;

            if (!aggregatedItem) {
                aggregatedItem = {
                    items: [],
                    executionTime: 0,
                    requestsCount: 0,
                    errorsCount: 0,
                    timeoutsCount: 0,
                    host: hostName,
                    method: request.method
                }
                aggregated[key] = aggregatedItem;
            }

            aggregatedItem.items.push(request);
        });
    });

    var requestsResult = [];
    var allRequestsTotalTime = 0;
    var allRequestsStats = {
        executionTime: 0,
        requestsCount: 0,
        errorsCount: 0,
        timeoutsCount: 0
    }

    Object.keys(aggregated).forEach(function (key) {
        var aggregatedItem = aggregated[key];
        var totalTime = 0;
        var maxTime = 0;

        aggregatedItem.items.forEach(function (item) {
            allRequestsTotalTime += item.executionTime;
            totalTime += item.executionTime;
            aggregatedItem.requestsCount++;
            allRequestsStats.requestsCount++;

            if (maxTime < item.executionTime) {
                maxTime = item.executionTime;
            }

            if (item.hasError) {
                aggregatedItem.errorsCount++;
                allRequestsStats.errorsCount++;
            }

            if (item.hasTimeout) {
                aggregatedItem.timeoutsCount++;
                allRequestsStats.timeoutsCount++;
            }
        });

        aggregatedItem.executionMaxTime = maxTime;
        aggregatedItem.executionTime = aggregatedItem.requestsCount > 0
            ? parseInt(totalTime / aggregatedItem.requestsCount) : 0;

        delete aggregatedItem.items;
        requestsResult.push(aggregatedItem);
    });

    allRequestsStats.executionTime = allRequestsStats.requestsCount > 0
        ? parseInt(allRequestsTotalTime / allRequestsStats.requestsCount) : 0;
    //if (log) common.log("EliteCoreConnector", "statistics: " + JSON.stringify(allRequestsStats));

    var resultAverage = [];
    var mapAverage = {};
    var config = module.exports.getDefaultConfig();

    requestsResult.forEach(function (item) {
        var aggregated = mapAverage[item.host];

        if (!aggregated) {
            aggregated = {
                host: item.host,
                fqdn: config.fqdn ? config.fqdn : "undefined",
                requests: []
            }

            mapAverage[item.host] = aggregated;
            resultAverage.push(aggregated);
        }

        delete item.host;
        aggregated.requests.push(item);
    });

    statistics.listeners.forEach(function (listener) {
        listener.callback(resultAverage, allRequestsStats, allErrors, allTimeouts);
    });
}

function initWSDLClients(hosts, port, path, needIgnoredNamespaces, needOverrideRootElement, targetNSAlias, callback) {
    if (!hosts || !port || !path) {
        common.error("EliteCoreConnector", "initWSDLClients: invalid params, hosts=" + JSON.stringify(hosts)
        + ", port=" + port + ", path" + path);
        if (callback) callback(new Error("Params are invalid"));
    }

    var options = {};
    if (needIgnoredNamespaces) {
        options.ignoredNamespaces = {
            namespaces: ['targetNamespace', 'typedNamespace', 'xs'],
            override: true
        }
    }

    if (needOverrideRootElement) {
        options.overrideRootElement = {
            namespace: "tns"
        }
    }

    var loadedCount = 0;
    var handleResult = function (err, host, url, client) {
        loadedCount++;

        var wsdlClient = module.exports.getEndpoint(path);
        if (err) {
            wsdlClient.errors[url] = {
                err: err,
                url: url,
                info: {
                    hosts: [host],
                    port: port,
                    path: path,
                    ignoredNamespaces: needIgnoredNamespaces,
                    overrideRootElement: needOverrideRootElement,
                    targetNSAlias: targetNSAlias
                }
            };
        } else {
            delete wsdlClient.errors[url];
            wsdlClient.clients.push({
                client: client,
                host: host,
                port: port,
                url: url
            });
        }

        var pendingCallbacks = wsdlClient.pendingCallbacks;
        wsdlClient.pendingCallbacks = [];
        pendingCallbacks.forEach(function (pendingCallback) {
            pendingCallback(wsdlClient);
        });

        if (loadedCount == hosts.length) {
            if (callback) callback(undefined, wsdlClient);
        }
    }

    var initClient = function (host) {
        var url = 'http://' + host + ':' + port + '/' + path + '?wsdl';
        if (log) common.log("EliteCoreConnector", "initWSDLClients: load soap client, url=" + url);

        soap.createClient(url, options, function (err, client) {
            if (err) {
                common.error("EliteCoreConnector", "initWSDLClients: failed to create soap client, " +
                "url=" + url + ", error=" + err.message);

                handleResult(err, host, url);
                return;
            }

            // workaround to define namespace for function name

            if (targetNSAlias) {
                client._invoke_overriden = client._invoke;
                client._invoke = function (method, args, location, callback, options, extraHeaders) {
                    method.input.targetNSAlias = targetNSAlias;
                    client._invoke_overriden(method, args, location, callback, options, extraHeaders);
                }
            }

            if (log) common.log("EliteCoreConnector", "initWSDLClients: loaded soap client, url=" + url);
            handleResult(undefined, host, url, client);
        });
    }

    hosts.forEach(function (host) {
        initClient(host);
    });
}

function clearOutDatedTimeouts(stats) {
    var currentTimeMs = new Date().getTime();
    if (stats.activeTimeouts.length > 0) {
        stats.activeTimeouts = stats.activeTimeouts.filter(function (item) {
            return item.ts + quarantineDelay > currentTimeMs;
        });
    }
}

function internalSoapCall(wsdlEndpoint, method, parameters, options, callback) {


    if (!callback) callback = function () {
    };

    if (!wsdlEndpoint || wsdlEndpoint.clients.length == 0) {
        return callback(new Error("No SOAP clients are found for the endpoint, method=" + method));
    }

    var num = countRequests++;
    if (Number.MAX_VALUE <= countRequests + 1) {
        countRequests = 0;
    }

    const soapTimeout = options && options.timeout ? options.timeout : 30000;

    var handleResult = function (err, response, soapClient, executionTime) {
        if (response) {
            elitecoreUtils.replace$Keys(response);
        }

        if (!soapClient) {
            return callback(err, response);
        }

        var stats = statistics.hosts[soapClient.host];
        if (!stats) {
            stats = {
                host: soapClient.host,
                activeTimeouts: [],
                lastTimeouts: [],
                lastErrors: [],
                lastRequests: [],
                errorsCount: 0,
                timeoutsCount: 0,
                quarantineEnd: 0
            }
            statistics.hosts[soapClient.host] = stats;
        }

        if (stats.quarantineEnd < currentTimeMs) {
            stats.quarantineEnd = 0;
        }

        var responseCode = response && response.return ? response.return.responseCode : undefined;
        var responseMessage = response && response.return ? response.return.responseMessage : undefined;

        if (!responseCode) {
            responseCode = "NONE";
        }

        var invalidResponse = response
            && (!response.return || (responseCode !== "NONE"
            && responseCode !== "1"
            && responseCode !== "0"
            && responseCode !== "-1"
            && responseCode !== "-50"
            && responseCode !== "-1001002"
            && responseCode !== "-10010133"
            && responseCode !== "-2002205"
            && responseCode !== "-2002500"
            && responseCode !== "-1003028003"
            && responseMessage != "SUCCESS"));

        var timeoutError = err && (err.code === "ETIMEDOUT" || err.code === "ESOCKETTIMEDOUT");
        var currentTimeMs = new Date().getTime();
        if (timeoutError) {

            // clear outdated timeouts
            clearOutDatedTimeouts(stats);

            countTimeouts++;

            stats.timeoutsCount++;
            stats.lastTimeouts.push({
                ts: currentTimeMs,
                code: err.code,
                host: soapClient.host,
                port: soapClient.port,
                path: wsdlEndpoint.path,
                method: method,
                parameters: parameters
            });

            stats.activeTimeouts.push({
                ts: currentTimeMs,
                host: soapClient.host,
                port: soapClient.port,
                method: method
            });

            if (stats.activeTimeouts.length > quarantineTimeoutsLimit && stats.quarantineEnd < currentTimeMs) {
                stats.quarantineEnd = currentTimeMs + quarantineDelay;
            }

            if (log) common.log("EliteCoreConnector", "internalSoapCall[" + num + "]: saved ETIMEDOUT log, host=" + soapClient.host
            + ", port=" + soapClient.port + ", timeoutsCount=" + stats.activeTimeouts.length
            + ", quarantineTime=" + (stats.quarantineEnd > 0 ? ((stats.quarantineEnd - currentTimeMs) / 1000) + "s" : "NONE"));
        } else if (err || invalidResponse) {

            countErrors++;

            var errorMessage = err ? err.message : (!response.return ? "Empty response"
                : "Error (responseCode=" + responseCode + ")");

            stats.errorsCount++;
            stats.lastErrors.push({
                ts: currentTimeMs,
                host: soapClient.host,
                port: soapClient.port,
                responseCode: responseCode,
                responseMessage: responseMessage,
                err: errorMessage,
                path: wsdlEndpoint.path,
                method: method,
                parameters: parameters,
                response: response
            });

            var responseString = response ? JSON.stringify(response) : "";
            responseString = responseString.length > 150 ? responseString.substring(0, 149) : responseString;

            if (log) common.log("EliteCoreConnector", "internalSoapCall[" + num + "]: saved ERROR log, host=" + soapClient.host
            + ", port=" + soapClient.port + ", errorMessage=" + errorMessage + ", response=" + responseString
            + ", errorsCount=" + stats.lastErrors.length);
        }

        stats.lastRequests.push({
            hasError: ((err && !timeoutError) || invalidResponse) ? true : false,
            hasTimeout: (err && timeoutError) ? true : false,
            error: err ? err.message : undefined,
            ts: currentTimeMs,
            executionTime: executionTime,
            host: soapClient.host,
            port: soapClient.port,
            method: method
        });

        callback(err, response);
    }

    var shiftedClients = function (soapClients) {

        var currentTimeMs = new Date().getTime();
        var clients = [];

        for (var i = 0; i < soapClients.length; i++) {
            var client = soapClients[i];
            var stats = statistics[client.host];

            // check if host is in quarantine
            if ((!stats || currentTimeMs > stats.quarantineEnd)
                && (!disabledBss || disabledBss.indexOf(client.host) == -1)) {
                clients.push(client);
            }
        }

        var offset = num % clients.length;
        for (var i = 0; i < offset; i++) {
            clients.push(clients.shift());
        }

        if (log) {
            var hosts = [];
            clients.forEach(function (item) {
                hosts.push(item.host);
            });

            common.log("EliteCoreConnector", "internalSoapCall[" + num + "]: selected hosts=" +
            JSON.stringify(hosts) + ", disabled hosts=" + JSON.stringify(disabledBss));
        }

        if (clients.length == 0) {
            var randomlySelectedClient = soapClients[num % soapClients.length];
            clients.push(randomlySelectedClient);

            common.error("EliteCoreConnector", "internalSoapCall[" + num + "]: all provided hosts " +
            "are in quarantine, select any, clients=" + randomlySelectedClient.host);
        }

        return clients;
    }

    var executeSoapCall = function (soapClients, pos) {
        if (pos >= soapClients.length) {
            return handleResult(new Error("Failed with all available SOAP clients, method=" + method
                + ", clients count=" + soapClients.length), undefined,
                soapClients.length > 0 ? soapClients[soapClients.length - 1] : undefined, -1);
        }

        var soapClient = soapClients[pos];
        var startTimeMs = new Date().getTime();

        if (log) common.log("EliteCoreConnector", "internalSoapCall[" + num + "]: soap call started, host=" + soapClient.host
        + ", port=" + soapClient.port + ", timeout=" + soapTimeout + ", method=" + method + ", params=" + JSON.stringify(parameters) + ", url=" + soapClient.url);

        if (!soapClient.client[method]) {
            return handleResult(new Error("Method " + method + " is not provided by end point " + wsdlEndpoint.path), undefined,
                soapClient, 0);
        }

        soapClient.client[method](parameters, function (err, result) {
            var executionTime = new Date().getTime() - startTimeMs;

            if (err) {

                if (err.message == "undefined: undefined" && result && result.statusCode) {
                    err.message = "CODE=" + result.statusCode + ", BODY=" + result.body;
                }

                common.error("EliteCoreConnector", "internalSoapCall[" + num + "]: failed to make a soap call, "
                + "message=" + err.message + ", code=" + err.code + ", body="
                + (err.body && err.body.length > 600 ? err.body.substring(0, 600) + "....." : "")
                + ", host=" + soapClient.host
                + ", port=" + soapClient.port + ", method=" + method
                + ", executionTime=" + executionTime);

                // do not continue in case of TIMEOUT, EliteCore may actually
                // finish operation successfully but don't respond

                if (err.code === "ETIMEDOUT" || err.code === "ESOCKETTIMEDOUT") {
                    return handleResult(err, undefined, soapClient, executionTime);
                } else {
                    return executeSoapCall(soapClients, pos + 1);
                }
            }

            if (log) common.log("EliteCoreConnector", "internalSoapCall[" + num + "]: soap call finished, host=" + soapClient.host
            + ", port=" + soapClient.port + ", method=" + method + ", executionTime=" + executionTime);
            handleResult(undefined, result, soapClient, executionTime);

        }, {timeout: soapTimeout});
    }

    executeSoapCall(shiftedClients(wsdlEndpoint.clients), 0);
}

module.exports.refreshBssSetting();
setInterval(() => {
    module.exports.refreshBssSetting();
}, 5000);
