var async = require('async');
var request = require('request');
var common = require(global.__lib + '/common');
var db = require(global.__lib + '/db_handler');
var config = require(global.__base + '/config');

var log = config.EC_LOGSENABLED;
var initDone = false;
var initInProgress = false;
var initCallbacks = [];

var reInitHierarchyDelay = 60 * 60 * 1000;
var reInitDetailsDelay = 5 * 60 * 1000;
var apiKey;

var packages = {
    base: [],
    boost: [],
    bonus: [],
    general: []
};

module.exports = {

    /**
     *
     */
    getDefaultConfig: function () {
        return {
            packagesKey: config.HIERARCHYSOURCE_CACHE_KEY,
            apiKey: config.MOBILE_APP_KEY,
            apiHost: config.CMSHOST,
            apiPort: config.CMSPORT
        }
    },

    setAPIKey: function (teamApiKey) {
        apiKey = teamApiKey;
    },

    /**
     *
     */
    loadHierarchy: function (callback) {
        module.exports.init(function (hierarchy) {
            callback(undefined, hierarchy);
        });
    },

    /**
     *
     */
    init: function (callback) {
        if (initInProgress) {
            if (log) common.log("EliteCoreConnector", "init: initialisation is in progress");
            if (callback) initCallbacks.push(callback);
            return;
        } else if (initDone) {
            if (callback) callback(packages);
            return;
        }

        initInProgress = true;
        initCallbacks.push(callback);

        var enableRecurrentEvents = function () {

            // reload complete hierarchy from cache

            setInterval(function () {
                var config = module.exports.getDefaultConfig();
                reLoadHierarchyFromCache(apiKey, config.packagesKey, function (err, hierarchy) {
                    if (err) return;
                    packages = hierarchy;
                })
            }, reInitHierarchyDelay);

            // reload hierarchy content need to sync with database
            // when pulling data directly from elitecore

            setInterval(function () {
                reLoadHierarchyContent(apiKey, packages, function (err, hierarchy) {
                    if (err) return;
                    packages = hierarchy;
                })
            }, reInitDetailsDelay);
        }

        var config = module.exports.getDefaultConfig();
        reLoadHierarchyFromCache(apiKey, config.packagesKey, function (err, hierarchy) {
            if (err) {
                // abort! we cannot continue without this
                common.error("EliteCoreConnector", "init: abort, cannot continue without hierarchy, error=" + err.message);
                process.kill(process.pid, 'SIGINT');
            }

            packages = hierarchy;
            enableRecurrentEvents();

            var callbacks = initCallbacks;
            initDone = true;
            initInProgress = false;
            initCallbacks = [];

            if (log) common.log("EliteCoreConnector", "init: initialisation finished");
            callbacks.forEach(function (callback) {
                callback(packages);
            });
        });
    },

    /**
     *
     */
    loadPackagesForTeamApiKey: function (teamApiKey, callback) {
        if (!teamApiKey) {
            module.exports.loadPackages(undefined, (err, rows) => {
                if (callback) callback(err, rows);
            });
        } else {
            var q = "SELECT id FROM teams WHERE apiKey=" + db.escape(teamApiKey);
            db.sql.query(q, function (err, rows) {
                if (err) {
                    return callback(err);
                }
                if (!rows || !rows[0] || !rows[0].id) {
                    return callback(new Error("Platform config is not found"));
                }

                var id = rows[0].id;
                if (log) common.log("EliteCoreConnector", "loadPackagesForTeamApiKey: teamApiKey=" + teamApiKey + ", id=" + id);

                module.exports.loadPackages(id, (err, rows) => {
                    if (callback) callback(err, rows);
                });
            });
        }
    },

    /**
     *
     */
    loadPackages: function (teamId, callback) {
        var query;
        if (teamId && (teamId > 0)) {
            var escTeamId = db.escape(teamId);
            query = "SELECT p.product_id, p.order_id, p.title, p.app, p.subtitle, p.description_short, p.description_full, "
            + "p.disclaimer, p.sub_effect, p.unsub_effect, p.beta, p.beta_group, p.advanced_payment, p.free_package, p.action, "
            + "SUM(IF(pt.teamID=" + escTeamId + ", 1, 0)) enabled "
            + "FROM packages p LEFT JOIN packagesTeams pt ON pt.packageID=p.id WHERE p.enabled=1 GROUP BY p.product_id";
        } else {
            query = "SELECT p.product_id, p.order_id, p.title, p.app, p.subtitle, p.description_short, p.description_full, "
            + "p.disclaimer, p.sub_effect, p.unsub_effect, p.beta, p.beta_group, p.advanced_payment, p.free_package, p.action, "
            + "1 enabled FROM packages p WHERE p.enabled=1";
        }

        db.sql.query(query, function (err, rows) {
            if (callback) callback(err, rows);
        });
    }
}

function reLoadHierarchyFromCache(apiKey, packagesKey, callback) {
    if (log) common.log("EliteCoreConnector", "reLoadHierarchyFromCache: load hierarchy, packagesKey=" + packagesKey);
    db.statusEvent.on("ready", function () {
        db.statusEvent.removeListener("ready", function () {});
        db.cache.get("account_cache", packagesKey, function (hierarchy) {
            if (!hierarchy || !hierarchy.base || !hierarchy.boost || !hierarchy.bonus || !hierarchy.general) {
                common.error("EliteCoreConnector", "reLoadHierarchyFromCache: failed to load hierarchy, abort the execution");
                if (callback) callback(new Error("Can not load hierarchy"));
            }

            reLoadHierarchyContent(apiKey, hierarchy, function (err, hierarchy) {
                if (err) {
                    common.error("EliteCoreConnector", "reLoadHierarchyFromCache: failed to load hierarchy details, abort the execution");
                    if (callback) callback(new Error("Can not load hierarchy products details, (" + err.message + ")"));
                }

                callback(undefined, hierarchy);
            });
        });
    });
}

function reLoadHierarchyContent(apiKey, hierarchy, callback) {
    if (log) common.log("EliteCoreConnector", "reLoadHierarchyContent");
    module.exports.loadPackagesForTeamApiKey(apiKey, function (err, rows) {
        if (err) {
            if (callback) callback(err);
            return;
        }

        var updateCategory = function (packages, item) {
            packages.forEach(function (product) {
                if (product && item && (product.id == item.product_id)) {

                    // old format
                    product.order_id = item.order_id;
                    product.sub_effect = item.sub_effect;
                    product.unsub_effect = item.unsub_effect;
                    product.description_short = item.description_short;
                    product.description_full = item.description_full;
                    product.advanced_payment = item.advanced_payment;
                    product.free_package = item.free_package ? item.free_package : "";

                    // new format
                    product.app = item.app;
                    product.title = item.title;
                    product.subtitle = item.subtitle;
                    product.descriptionShort = item.description_short;
                    product.descriptionFull = item.description_full;
                    product.disclaimer = item.disclaimer;
                    product.orderId = item.order_id;
                    product.action = item.action;
                    product.assets = item.assets;
                    product.enabled = item.enabled;
                    product.beta = item.beta;
                    product.betaGroup = item.beta_group;
                    product.advancedPayment = item.advanced_payment;
                    product.freePackage = item.free_package ? item.free_package : "";
                }
            });
        }

        rows.forEach(function (item) {

            hierarchy.base.forEach((subBase) => {
                if (subBase.data) updateCategory(subBase.data, item);
                if (subBase.sms) updateCategory(subBase.sms, item);
                if (subBase.voice) updateCategory(subBase.voice, item);
            })

            updateCategory(hierarchy.base, item);
            updateCategory(hierarchy.boost, item);
            updateCategory(hierarchy.bonus, item);
            updateCategory(hierarchy.general, item);
        });

        callback(undefined, hierarchy);
    });
}

