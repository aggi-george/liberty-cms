/*globals module,exports */


const elitecoreConnector = require('./elitecoreConnector');
const elitecoreBill = require('./elitecoreBillService');
const elitecorePortIn = require('./elitecorePortInService');
const elitecorePackages = require('./elitecorePackagesService');
const elitecoreUsage = require('./elitecoreUsageService');
const elitecoreUser = require('./elitecoreUserService');

/**
 * This is the entry point for eliteCore. From here it is routed to the methods in side elitecore
 */

class EliteCore{
    constructor(){

    }

    //region - General

    /**
     *
     * @param callback - callback handler
     */
    init(callback) {
        elitecoreConnector.init(callback);
    }
    
    /**
     * load BSS mode
     * @param callback
     */
    loadBssMode(callback) {
        elitecoreConnector.loadBssMode(callback);
    }

    /**
     * Set disabled BSS
     * @param bssList
     */
    setDisabledBss(bssList) {
        elitecoreConnector.setDisabledBss(bssList);
    }

    /**
     * Set offline mode
     * @param offline
     */
    setOfflineMode(offline) {
        elitecoreConnector.setOfflineMode(offline);
    }

    /**
     * Get count timeouts function
     */
    getCountTimeouts() {
        elitecoreConnector.getCountTimeouts();
    }

    /**
     * Get count requests function
     */
    getCountRequests() {
        elitecoreConnector.getCountRequests();
    }

    /**
     * Get count errors function
     */
    getCountErrors() {
        elitecoreConnector.getCountErrors();
    }

    /**
     * Add stats update listener
     * @param callback
     */
    addStatsUpdateListener(callback) {
        elitecoreConnector.addStatsUpdateListener(callback);
    }

    /**
     * Remove stats update listener
     * @param key
     */
    removeStatsUpdateListener(key) {
        elitecoreConnector.removeStatsUpdateListener(key);
    }

    //endregion

    //region - connector

    /**
     * To get Customer Hierarchy Details by account number
     * @param accountNumber - Service account number - e.g. 'LW100275'
     * @param callback - callback handler
     */
    getCustomerHierarchyDetails(accountNumber,callback){
        elitecoreConnector.getCustomerHierarchyDetails(accountNumber,callback);
    }

    /**
     * Get all the product offer details
     * @param callback - callback handler
     */
    getProductOfferDetails(callback) {
        elitecoreConnector.getProductOfferDetails(callback);
    }

    /**
     * Get the specific product offer details by the  product Id
     * @param args - args means productId - e.g. 'PRD00192'
     * @param callback - callback handler
     */
    getSpecificProdOfferDetails(args, callback) {
        elitecoreConnector.getSpecificProdOfferDetails(args, callback);
    }

    /**
     * Get the business hierarchy details
     * @param callback - callback handler
     */
    getBusinessHierarchyDetail(callback) {
        elitecoreConnector.getBusinessHierarchyDetail(callback);
    }

    /**
     * Get the inventory details by the inventory number
     * @param inventoryNumber - Inventory number - e.g. 1
     * @param callback - callback handler
     */
    searchInventory(inventoryNumber, callback) {
        elitecoreConnector.searchInventory(inventoryNumber, callback);
    }

    /**
     * Get the list of inventories by the inventory status id, subtype id, owner id (default - 'operator')
     * @param inventoryStatusId - Inventory status Id - e.g. 'NBS01'
     * @param inventorySubTypeId - Inventory Subtype Id - e.g. 'NTP00016'
     * @param callback - callback handler
     */
    searchInventoryList(inventoryStatusId, inventorySubTypeId, callback) {
        elitecoreConnector.searchInventoryList(inventoryStatusId, inventorySubTypeId, callback);
    }

    /**
     * Do the inventory operation by the inventory number, operationAlias and reference number(Default  - N/A)
     * @param inventoryNumber - Inventory Number
     * @param operationAlias - Operation Alias
     * @param callback - callback handler
     */
    doInventoryStatusOperation(inventoryNumber, operationAlias, callback) {
        elitecoreConnector.doInventoryStatusOperation(inventoryNumber, operationAlias, callback);
    }

    /**
     * repair inventory bundle
     * @param args
     * @param callback - callback handler
     */
    repairInventoryRebundle(args, callback) {
        elitecoreConnector.repairInventoryRebundle(args, callback);
    }

    /**
     * Get inventory details by the arguments
     * @param args
     * @param callback - callback handler
     */
    getInventoryDetail(args, callback) {
        elitecoreConnector.getInventoryDetail(args, callback);
    }

    /**
     * Get inventory list by the sevice instance number
     * @param serviceInstanceNumber - Service instance number - e.g. 'LW100276'
     * @param callback - callback handler
     */
    listInventory(serviceInstanceNumber, callback) {
        elitecoreConnector.listInventory(serviceInstanceNumber, callback);
    }

    /**
     * Unpair inventory by arguments
     * @param args
     * @param callback - callback handler
     */
    unpairInventory(args, callback) {
        elitecoreConnector.unpairInventory(args, callback);
    }

    /**
     * Repair inventory by arguments
     * @param args
     * @param callback - callback handler
     */
    repairInventory(args, callback) {
        elitecoreConnector.repairInventory(args, callback);
    }

    /**
     * repair inventory xml by arguments
     * @param args
     * @param callback - callback handler
     */
    repairInventoryXml(args, callback) {
        elitecoreConnector.repairInventoryXml(args, callback);
    }

    /**
     * Remove inventory by service instance number and inventory number
     * @param serviceInstanceNumber - Service instance number
     * @param inventoryNumber - Inventory number
     * @param callback - callback handler
     */
    removeInventory(serviceInstanceNumber, inventoryNumber, callback) {
        elitecoreConnector.removeInventory(serviceInstanceNumber, inventoryNumber, callback);
    }

    /**
     * Add inventory by effective date, inventory number, subtype, Ismanualinventory, packageid and service instance number
     * @param effectDate - Effect date
     * @param inventoryNumber - Inventory number
     * @param serviceInstanceNumber - Service Instance number
     * @param callback - callback handler
     */
    addInventory(effectDate, inventoryNumber, serviceInstanceNumber, callback) {
        elitecoreConnector.addInventory(effectDate, inventoryNumber, serviceInstanceNumber, callback);
    }

    /**
     * set inventory as damaged by inventory number and serviceInstanceNumber
     * @param inventoryNumber - Inventory number
     * @param serviceInstanceNumber - Service instance number
     * @param callback - callback handler
     */
    markInventoryAsDamaged(inventoryNumber, serviceInstanceNumber, callback) {
        elitecoreConnector.markInventoryAsDamaged(inventoryNumber, serviceInstanceNumber, callback);
    }

    /**
     * Create Account
     * @param args
     * @param callback - callback handler
     */
    createAccount(args, callback) {
        elitecoreConnector.createAccount(args, callback);
    }

    /**
     * Change service plan by account number, effective date, pacakage name, remarks and service instance number
     * @param billingAccountNumber - Billing account number
     * @param effectiveDate - Effective date
     * @param packageName - Package name
     * @param remarks - Remarks
     * @param serviceInstanceNumber - Service instance number
     * @param callback - callback handler
     */
    changeServicePlan(billingAccountNumber, effectiveDate, packageName, remarks, serviceInstanceNumber, callback) {
        elitecoreConnector.changeServicePlan(billingAccountNumber, effectiveDate, packageName, remarks, serviceInstanceNumber, callback);
    }

    /**
     * search account by mobile or MSISDN Number, account number, service username and account name
     * @param number - Number
     * @param accountNumber - Account number
     * @param accountName - Account name
     * @param serviceUserName - Service username
     * @param callback - callback handler
     */
    searchAccount(number, accountNumber, accountName, serviceUserName, callback) {
        elitecoreConnector.searchAccount(number, accountNumber, accountName, serviceUserName, callback);
    }

    /**
     * search service instance by mobile or MSISDN Number, account number and account name
     * @param number - Number - e.g. '87420765'
     * @param accountNumber - Account number - e.g. undefined
     * @param accountName - Account Name - e.g. undefined
     * @param callback - callback handler
     */
    searchServiceInstance(number, accountNumber, accountName, callback) {
        elitecoreConnector.searchServiceInstance(number, accountNumber, accountName, callback);
    }

    /**
     * Get account statement by account number, from and to date
     * @param accountNumber - Account number - e.g. 'LW100274'
     * @param fromDate - From date - e.g. '2017-01-01'
     * @param toDate - To date - e.g. '2017-11-01'
     * @param callback - callback handler
     */
    getAccountStatement(accountNumber, fromDate, toDate, callback) {
        elitecoreConnector.getAccountStatement(accountNumber, fromDate, toDate, callback);
    }

    /**
     * Search bill by accout number, from and to date
     * @param accountNumber - Account number - e.g. 'LW100274'
     * @param fromDate - From date - e.g. '2017-01-01'
     * @param toDate - To date -  e.g. '2017-02-01'
     * @param callback - callback handler
     */
    searchBill(accountNumber, fromDate, toDate, callback) {
        elitecoreConnector.searchBill(accountNumber, fromDate, toDate, callback);
    }

    /**
     * Search credit note by arguments
     * @param args
     * @param callback - callback handler
     */
    searchCreditNote(args, callback) {
        elitecoreConnector.searchCreditNote(args, callback);
    }

    /**
     * Get billing account details by billing account number
     * @param billingAccountNumber - Billing account number - e.g. 'LW100274'
     * @param callback - callback handler
     */
    getBillingAccountDetail(billingAccountNumber, callback) {
        elitecoreConnector.getBillingAccountDetail(billingAccountNumber, callback);
    }

    /**
     * Add deposit to customer billing account by billing account number, deposit account and reason
     * @param billingAccountNumber - Billing account number
     * @param depositAmount - Deposit amount
     * @param reason - Reason
     * @param callback - callback handler
     */
    addDepositToCustomerBillingAccount(billingAccountNumber, depositAmount, reason, callback) {
        elitecoreConnector.addDepositToCustomerBillingAccount(billingAccountNumber, depositAmount, reason, callback);
    }

    /**
     * Make debit payment data by makeDebitPaymentDataRequest
     * @param accountNumber - Account number
     * @param transactionAmount - Transaction amount
     * @param documentNumber - Document number
     * @param paymentDate - Payment date
     * @param channelAlias - Channel alias
     * @param callback - callback handler
     */
    makeDebitPaymentDataRequest(accountNumber, transactionAmount, documentNumber, paymentDate,
                                           channelAlias, callback) {
        elitecoreConnector.makeDebitPaymentDataRequest(accountNumber, transactionAmount, documentNumber, paymentDate,
            channelAlias, callback);
    }

    /**
     * Make advance payment by makeAdvancePaymentDataRequest object
     * @param accountNumber - Account number
     * @param transactionAmount - Transaction amount
     * @param paymentDate - Payment date
     * @param channelAlias - Channel alias
     * @param description - Description
     * @param callback - callback handler
     */
    makeAdvancePaymentDataRequest(accountNumber, transactionAmount, paymentDate,
                                             channelAlias, description, callback) {
        elitecoreConnector.makeAdvancePaymentDataRequest(accountNumber, transactionAmount, paymentDate,
            channelAlias, description, callback);
    }

    /**
     * Update customer account data by arguments
     * @param args
     * @param callback - callback handler
     */
    updateCustomerAccountData(args, callback) {
        elitecoreConnector.updateCustomerAccountData(args, callback);
    }

    /**
     * To Update billing account data
     * @param args
     * @param callback - callback handler
     */
    updateBillingAccountData(args, callback) {
        elitecoreConnector.updateBillingAccountData(args, callback);
    }

    /**
     * To Update service account data
     * @param args
     * @param callback - callback handler
     */
    updateServiceAccountData(args, callback) {
        elitecoreConnector.updateServiceAccountData(args, callback);
    }

    /**
     * To update billing account
     * @param args
     * @param callback - callback handler
     */
    updateBillingAccount(args, callback) {
        elitecoreConnector.updateBillingAccount(args, callback);
    }



    /**
     * To Update service account
     * @param args
     * @param callback - callback handler
     */
    updateServiceAccount(args, callback) {
        elitecoreConnector.updateServiceAccount(args, callback);
    }

    /**
     * To subscribe add on package
     * @param args
     * @param callback - callback handler
     */
    subscribeAddOnPackage(args, callback) {
        elitecoreConnector.subscribeAddOnPackage(args, callback);
    }

    /**
     * To un-subscribe add on package
     * @param args
     * @param callback - callback handler
     */
    unSubscribeAddOnPackage(args, callback) {
        elitecoreConnector.unSubscribeAddOnPackage(args, callback);
    }

    /**
     * To get FnF member details
     * @param args
     * @param callback - callback handler
     */
    getFnFMembersDetails(args, callback) {
        elitecoreConnector.getFnFMembersDetails(args, callback);
    }

    /**
     * To add FnF members
     * @param args
     * @param callback - callback handler
     */
    addFnFMembers(args, callback) {
        elitecoreConnector.addFnFMembers(args, callback);
    }

    /**
     * To remove FnF members
     * @param args
     * @param callback - callback handler
     */
    removeFnFMembers(args, callback) {
        elitecoreConnector.removeFnFMembers(args, callback);
    }

    /**
     * To search payments
     * @param args
     * @param callback - callback handler
     */
    searchPayment(args, callback) {
        elitecoreConnector.searchPayment(args, callback);
    }

    /**
     * To create Credit note
     * @param args
     * @param callback - callback handler
     */
    createCreditNote(args, callback) {
        elitecoreConnector.createCreditNote(args, callback);
    }

    /**
     * To modify plan attributes
     * @param args
     * @param callback - callback handler
     */
    modifyPlanAttributes(args, callback) {
        elitecoreConnector.modifyPlanAttributes(args, callback);
    }

    /**
     * To change service instance status
     * @param args
     * @param callback - callback handler
     */
    changeServiceInstanceStatus(args, callback) {
        elitecoreConnector.changeServiceInstanceStatus(args, callback);
    }

    /**
     * To Cancel PortIn order
     * @param MSISDN
     * @param reasonCode
     * @param callback - callback handler
     */
    cancelPortInOrder(MSISDN, reasonCode, callback) {
        elitecoreConnector.cancelPortInOrder(MSISDN, reasonCode, callback);
    }

    /**
     * For MNP Notification
     * @param MSISDN
     * @param callback - callback handler
     */
    mnpNotification(MSISDN, callback) {
        elitecoreConnector.mnpNotification(MSISDN, callback);
    }

    /**
     * For Service provisioning notification
     * @param externalRefId
     * @param inventoryNumber
     * @param callback - callback handler
     */
    serviceProvisioningNotification(externalRefId, inventoryNumber, callback) {
        elitecoreConnector.serviceProvisioningNotification(externalRefId, inventoryNumber, callback);
    }

    /**
     * To Views customer
     * @param serviceInstanceNumber - e.g. 'LW100276'
     * @param fromDate - e.g. '2017-11-01'
     * @param callback - callback handler
     */
    viewCustomer(serviceInstanceNumber, fromDate, callback) {
        elitecoreConnector.viewCustomer(serviceInstanceNumber, fromDate, callback);
    }

    /**
     * To view customer usage information
     * @param args
     * @param callback - callback handler
     */
    viewCustomerUsageInformation(args, callback) {
        elitecoreConnector.viewCustomerUsageInformation(args, callback);
    }

    /**
     * To create Inventory
     * @param msisdn
     * @param donorCode
     * @param date
     * @param callback - callback handler
     */
    createInventory(msisdn, donorCode, date, callback) {
        elitecoreConnector.createInventory(msisdn, donorCode, date, callback);
    }

    /**
     * To update inventory attributes
     * @param msisdn
     * @param donorCode
     * @param date
     * @param callback - callback handler
     */
    updateInventoryAttributes(msisdn, donorCode, date, callback) {
        elitecoreConnector.updateInventoryAttributes(msisdn, donorCode, date, callback);
    }

    /**
     * To update customer bucket
     * @param args
     * @param callback - callback handler
     */
    updateCustomerBucket(args, callback) {
        elitecoreConnector.updateCustomerBucket(args, callback);
    }

    //endregion

    //region - elitecorebillservice

    /**
     * To load all Bill Invoices
     * @param monthDate - e.g. '2017-10-01'
     * @param callback - callback handler
     */
    loadBillInvoices(monthDate, callback) {
        elitecoreBill.loadBillInvoices(monthDate, callback);
    }

    /**
     * To load all unpaid bill invoices
     * @param callback - callback handler
     */
    loadUnpaidBillInvoices(callback) {
        elitecoreBill.loadUnpaidBillInvoices(callback);
    }

    /**
     * To load Customer Usage
     * @param serviceInstance - Service instance - e.g. 'LW0336031'
     * @param callback - callback handler
     */
    loadCustomerUsage(serviceInstance, callback) {
        elitecoreBill.loadCustomerUsage(serviceInstance, callback);
    }

    /**
     * To load bill invoices by Ids
     * @param invoiceIds - Invoice Ids - e.g. ["REG0000000085556", "REG0000000085248"]
     * @param callback - callback handler
     */
    loadBillInvoicesByIds(invoiceIds, callback) {
        elitecoreBill.loadBillInvoicesByIds(invoiceIds, callback);
    }

    /**
     * To clear customer payments cache
     * @param billingAccountNumber - Billing account number
     */
    clearCustomerPaymentsCache(billingAccountNumber) {
        elitecoreBill.clearCustomerPaymentsCache(billingAccountNumber);
    }

    /**
     * Callback service for various inner services
     * @param billingAccountNumber - e.g. 'LW100274'
     * @param callback - callback handler
     * @param fresh - e.g. false
     * @param basic - e.g. false
     */
    loadCustomerPaymentsInfo(billingAccountNumber, callback, fresh, basic) {
        elitecoreBill.loadCustomerPaymentsInfo(billingAccountNumber, callback, fresh, basic);
    }

    //endregion

    //region - elitecorePortInService

    /**
     *
     * @param serviceInstanceNumber
     * @param tempNumber
     * @param portInNumber
     * @param donor
     * @param externalRefId
     * @param callback - callback handler
     * @param portInRecovery
     */
    portInNumberImmediate(serviceInstanceNumber, tempNumber, portInNumber,
                                     donor, externalRefId, callback, portInRecovery) {
        elitecorePortIn.portInNumberImmediate(serviceInstanceNumber, tempNumber, portInNumber,
            donor, externalRefId, callback, portInRecovery);
    }

    /**
     *
     * @param serviceInstanceNumber
     * @param tempNumber
     * @param portInNumber
     * @param donor
     * @param startTime
     * @param externalRefId
     * @param callback - callback handler
     * @param portInRecovery
     */
    portInNumber(serviceInstanceNumber, tempNumber, portInNumber, donor, startTime, externalRefId,
                            callback, portInRecovery) {
        elitecorePortIn.portInNumber(serviceInstanceNumber, tempNumber, portInNumber, donor, startTime, externalRefId,
            callback, portInRecovery);
    }

    /**
     *
     * @param serviceInstanceNumber
     * @param portInNumber
     * @param callback - callback handler
     */
    deactivatePortInNumber(serviceInstanceNumber, portInNumber, callback) {
        elitecorePortIn.deactivatePortInNumber(serviceInstanceNumber, portInNumber, callback);
    }

    /**
     *
     * @param portInNumber
     * @param start_ts
     * @param callback - callback handler
     */
    loadPortInFailureReason(portInNumber, start_ts, callback) {
        elitecorePortIn.loadPortInFailureReason(portInNumber, start_ts, callback);
    }

    /**
     *
     * @param portInNumber
     * @param callback - callback handler
     */
    loadPortInCompleationStatus(portInNumber, callback) {
        elitecorePortIn.loadPortInCompleationStatus(portInNumber, callback);
    }

    /**
     *
     * @param donor
     */
    getDonorCodeByNetworkName(donor) {
        elitecorePortIn.getDonorCodeByNetworkName(donor);
    }

    /**
     *
     * @param portInNumber
     * @param status
     */
    getPortInStatus(portInNumber, status) {
        elitecorePortIn.getPortInStatus(portInNumber, status);
    }

    //endregion

    //region - elitecorePackageService

    /**
     *
     * @param teamApiKey - Team API Key - e.g. 'sLZ7XNcxrN'
     */
    setAPIKey(teamApiKey) {
        elitecorePackages.setAPIKey(teamApiKey);
    }

    /**
     *
     * @param callback - callback handler
     */
    loadHierarchy(callback) {
        elitecorePackages.loadHierarchy(callback);
    }

    /**
     *
     * @param teamApiKey - Team Api key e.g. 'sLZ7XNcxrN'
     * @param callback - callback handler
     */
    loadPackagesForTeamApiKey(teamApiKey, callback) {
        elitecorePackages.loadPackagesForTeamApiKey(teamApiKey, callback);
    }

    /**
     *
     * @param teamId - Team Id ex: 'PRD00440'
     * @param callback - callback handler
     */
    loadPackages(teamId, callback) {
        elitecorePackages.loadPackages(teamId, callback);
    }

    //endregion

    //region - elitecoreUsageService

    /**
     * Get the usage data and counter balance data  from DB call or view customer method in connector
     * @param serviceInstanceNumber - Service instance number - e.g. 'LW100276'
     * @param callback - callback handler
     * @param fresh - e.g. false
     * @param basic - e.g. false
     */
    loadCurrentCustomerUsage(serviceInstanceNumber, callback, fresh, basic) {
        elitecoreUsage.loadCurrentCustomerUsage(serviceInstanceNumber, callback, fresh, basic);
    }

    /**
     * Get remaining data for pacakge  by service Instance number
     * @param number - Number
     * @param packageId - Package Id
     * @param callback - callback handler
     */
    loadRemainingDataForPackage(number, packageId, callback) {
        elitecoreUsage.loadRemainingDataForPackage(number, packageId, callback);
    }

    //endregion

    //region - elitecoreUserService

    /**
     * Clear user information from db cache
     * @param accountNumber
     * @param callback - callback handler
     */
    clearUserInfo(accountNumber, callback) {
        elitecoreUser.clearUserInfo(accountNumber, callback);
    }

    /**
     * Update User Information
     * @param accountNumber - Account number - e.g. 'LW100273'
     * @param callback - callback handler
     * @param userKey - User key - e.g. undefined
     */
    updateUserInfo(accountNumber, callback, userKey) {
        elitecoreUser.updateUserInfo(accountNumber, callback, userKey);
    }

    /**
     * Load user information
     * @param keyType - Key type - e.g. 'sin_'
     * @param key - Key value - e.g. 'LW100276'
     * @param callback - callback handler
     * @param fresh - Fresh boolean - e.g. false
     */
    loadUserInfo(keyType, key, callback, fresh) {
        elitecoreUser.loadUserInfo(keyType, key, callback, fresh);
    }

    /**
     * Update the user info in cache
     * @param id - Id - e.g. 'LW100276'
     * @param callback - callback handler
     * @param fresh - Fresh boolean - e.g. false
     * @param basic - Basic boolean - e.g. false
     */
    loadUserInfoByInternalId(id, callback, fresh, basic) {
        elitecoreUser.loadUserInfoByInternalId(id, callback, fresh, basic);
    }

    /**
     * Load User information by CAN
     * @param can - Customer Account Number - e.g. 'LW100273'
     * @param callback - callback handler
     * @param fresh - Fresh boolean - e.g. false
     * @param basic - Basic boolean - e.g. false
     */
    loadUserInfoByCustomerAccountNumber(can, callback, fresh, basic) {
        elitecoreUser.loadUserInfoByCustomerAccountNumber(can, callback, fresh, basic);
    }

    /**
     * Load User information by BAN
     * @param ban - Business Account Number - e.g. 'LW100274'
     * @param callback - callback handler
     * @param fresh - Fresh boolean - e.g. false
     * @param basic - Basic boolean - e.g. false
     */
    loadUserInfoByBillingAccountNumber(ban, callback, fresh, basic) {
        elitecoreUser.loadUserInfoByBillingAccountNumber(ban, callback, fresh, basic);
    }

    /**
     * Load User information by Service Account Number
     * @param san - Service Account Number - e.g. 'LW100275'
     * @param callback - callback handler
     * @param fresh - Fresh boolean
     * @param basic - Basic boolean
     */
    loadUserInfoByServiceAccountNumber(san, callback, fresh, basic) {
        elitecoreUser.loadUserInfoByServiceAccountNumber(san, callback, fresh, basic);
    }

    /**
     * Load User Information by SIN
     * @param sin - Service Instance Number - e.g. 'LW100276'
     * @param callback - callback handler
     * @param fresh - Fresh boolean - e.g. false
     * @param basic - Basic boolean - e.g. false
     */
    loadUserInfoByServiceInstanceNumber(sin, callback, fresh, basic) {
        elitecoreUser.loadUserInfoByServiceInstanceNumber(sin, callback, fresh, basic);
    }

    /**
     * Load User Information by Phone number
     * @param number - Phone number - e.g. '87420765'
     * @param callback - callback handler
     * @param fresh - Fresh boolean - e.g. false
     * @param basic - Basic boolean - e.g. false
     */
    loadUserInfoByPhoneNumber(number, callback, fresh, basic)  {
        elitecoreUser.loadUserInfoByPhoneNumber(number, callback, fresh, basic);
    }

    /**
     * Load User Information by User key
     * @param userKey - User key for Native app (Mobile app) - e.g. 'd3ee3ef094574a1a015714207a72e85b'
     * @param callback - callback handler
     * @param fresh - Fresh boolean - e.g. false
     * @param basic - Basic boolean - e.g. false
     */
    loadUserInfoByUserKey(userKey, callback, fresh, basic) {
        elitecoreUser.loadUserInfoByUserKey(userKey, callback, fresh, basic);
    }

    /**
     * Load all users basic information
     * @param options - Options like Service Instance number - e.g. '{serviceInstances:["LW100276"]}'
     * @param callback - callback handler
     */
    loadAllUsersBaseInfo(options, callback) {
        elitecoreUser.loadAllUsersBaseInfo(options, callback);
    }

    /**
     * Load all active packages
     * @param serviceInstance - Service Instance - e.g. 'LW100276'
     * @param callback - callback handler
     */
    loadActivePackages(serviceInstance, callback) {
        elitecoreUser.loadActivePackages(serviceInstance, callback);
    }

    /**
     * Load customer data with packages
     * @param serviceInstance - Service Instance - e.g. 'CirclesOne'
     * @param callback - callback handler
     */
    loadCustomerWithPackage(serviceInstance, callback) {
        elitecoreUser.loadCustomerWithPackage(serviceInstance, callback);
    }

    /**
     * Update customer Id
     * @param accountNumber - Customer Account Number
     * @param type - Type
     * @param id - ID
     * @param callback - callback handler
     */
    updateCustomerID(accountNumber, type, id, callback) {
        elitecoreUser.updateCustomerID(accountNumber, type, id, callback);
    }

    /**
     * Update account by order reference number
     * @param accountNumber - Customer Account Number
     * @param orn - Order Reference Number
     * @param callback - callback handler
     */
    updateOrderReferenceNumber(accountNumber, orn, callback) {
        elitecoreUser.updateOrderReferenceNumber(accountNumber, orn, callback);
    }

    /**
     * Update activation date
     * @param accountNumber - Customer Account Number
     * @param datestring - Date value
     * @param callback - callback handler
     */
    updateActivationDate(accountNumber, datestring, callback) {
        elitecoreUser.updateActivationDate(accountNumber, datestring, callback);
    }

    /**
     * Update customer Date of Birth
     * @param accountNumber - Customer Account Number
     * @param birthday - Birthday
     * @param callback - callback handler
     */
    updateCustomerDOB(accountNumber, birthday, callback) {
        elitecoreUser.updateCustomerDOB(accountNumber, birthday, callback);
    }

    /**
     * Update all details (Billing, Servce and account) of the customer by account nubmer
     * @param accountNumber - Account number
     * @param params - Params
     * @param callback - callback handler
     * @param skipAccount - Skip Account
     * @param skipBilling - Skip billing
     * @param skipService - Skip Service
     */
    updateAllCustomerAccountsDetails(accountNumber, params, callback, skipAccount, skipBilling, skipService) {
        elitecoreUser.updateAllCustomerAccountsDetails(accountNumber, params, callback, skipAccount, skipBilling, skipService);
    }

    /**
     * Update customer order by account numer and reference number
     * @param accountNumber - Customer Account Number
     * @param orderReferenceNumber - Order Reference Number
     * @param callback - callback handler
     */
    updateCustomerOrderReferenceNumber(accountNumber, orderReferenceNumber, callback) {
        elitecoreUser.updateCustomerOrderReferenceNumber(accountNumber, orderReferenceNumber, callback);
    }

    /**
     * Get ID Type  by code
     * @param code
     */
    getIDTypeByCode(code) {
        elitecoreUser.getIDTypeByCode(code);
    }

    //endregion
}
const eliteCore = new EliteCore();

module.exports = eliteCore;