/*globals require, module */

// This is the entry point for any request to the middle ware..... here it is decided to go with elitecore or oraclemw

const validations = require('./validations/index');

const bssCommonError = require('./common/error');
const bssCommonLogger = require('./common/logger');
const bssConfig = require('./config');

const elitecoremw = require('./elitecore/index');

const middleWare = require('./middleware/index');

class BSS {
    constructor() {

    }

    localCatch(reason) {
        bssCommonLogger.middleware.localCatch(reason);
    }

    get gotoEliteCore() {
        return bssConfig.gotoEliteCore;
    }

    //region TEST

    testBRM(callback){
        middleWare.testBRM({callback});
    }

    //endregion

    //region - General

    /**
     * Init method to load initialization
     * @param callback - callback handler
     */
    init(callback) {

        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.init(callback);

        //This request is related to billing, now will pass this to billing handler in the middleware
        middleWare.init({callback});

    }

    /**
     * To set API Key
     * @param teamApiKey - Team Api key ex: 'sLZ7XNcxrN'
     */
    setAPIKey(teamApiKey) {

        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.setAPIKey(teamApiKey);

        //This request is related to billing, now will pass this to billing handler in the middleware
        middleWare.setAPIKey({teamApiKey});

    }

    /**
     *
     * @param bssList - bss list ex:
     */
    setDisabledBss(bssList) {

        //The following line will be here untill elitecore is ered. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.setDisabledBss(bssList);

        middleWare.setDisabledBss({bssList});
    }

    /**
     *
     * @param offline - offline ex:
     */
    setOfflineMode(offline) {
        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.setOfflineMode(offline);

        middleWare.setOfflineMode({offline});

    }

    /**
     *
     */
    getCountTimeouts() {
        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.getCountTimeouts();

        middleWare.getCountTimeouts();
    }

    /**
     *
     */
    getCountRequests() {
        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.getCountRequests();

        middleWare.getCountRequests();
    }

    /**
     *
     */
    getCountErrors() {
        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.getCountErrors();

        middleWare.getCountErrors();
    }

    /**
     *
     * @param callback - callback handler
     */
    addStatsUpdateListener(callback) {
        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.addStatsUpdateListener(callback);

        middleWare.addStatsUpdateListener({callback});
    }

    /**
     *
     * @param key -
     */
    removeStatsUpdateListener(key) {
        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.removeStatsUpdateListener(key);

        middleWare.removeStatsUpdateListener({key});
    }

    /**
     *
     * @param callback - callback handler
     */
    loadBssMode(callback) {
        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.loadBssMode(callback);

        middleWare.loadBssMode({callback});
    }

    //endregion

    //region - User, Account, Notifications


    /**
     * Create Account
     * @param args
     * @param callback - callback handler
     */
    createAccount(args, callback) {
        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.createAccount(args, callback);

        //this request is related to customer, now will pass this to customer handler in the middleware
        middleWare.createAccount({args, callback});
    }

    /**
     * search account by mobile or MSISDN Number, account number, service username and account name
     * @param number - Number
     * @param accountNumber - Account number
     * @param accountName - Account name
     * @param serviceUserName - Service username
     * @param callback - callback handler
     */
    searchAccount(number, accountNumber, accountName, serviceUserName, callback) {

        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.searchAccount(number, accountNumber, accountName, serviceUserName, callback);

        //this request is related to customer, now will pass this to customer handler in the middleware
        middleWare.searchAccount({number, accountNumber, accountName, serviceUserName, callback});
    }

    /**
     * search service instance by mobile or MSISDN Number, account number and account name
     * @param number - Number - e.g. '87420765'
     * @param accountNumber - Account number - e.g. undefined
     * @param accountName - Account Name - e.g. undefined
     * @param callback - callback handler
     */
    searchServiceInstance(number, accountNumber, accountName, callback) {
        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.searchServiceInstance(number, accountNumber, accountName, callback);

        //this request is related to customer, now will pass this to customer handler in the middleware
        middleWare.searchServiceInstance({number, accountNumber, accountName, callback});
    }

    /**
     * To Views customer
     * @param serviceInstanceNumber - e.g. 'LW100276'
     * @param fromDate - e.g. '2017-11-01'
     * @param callback - callback handler
     */
    viewCustomer(serviceInstanceNumber, fromDate, callback) {

        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.viewCustomer(serviceInstanceNumber, fromDate, callback);


        //this request is related to customer, now will pass this to customer handler in the middleware
        middleWare.viewCustomer({serviceInstanceNumber, fromDate, callback});
    }

    /**
     * Get account statement by account number, from and to date
     * @param accountNumber - Account number - e.g. 'LW100274'
     * @param fromDate - From date - e.g. '2017-01-01'
     * @param toDate - To date - e.g. '2017-11-01'
     * @param callback - callback handler
     */
    getAccountStatement(accountNumber, fromDate, toDate, callback) {

        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.getAccountStatement(accountNumber, fromDate, toDate, callback);

        //this request is related to customer, now will pass this to customer handler in the middleware
        middleWare.getAccountStatement({accountNumber, fromDate, toDate, callback});
    }

    /**
     * To get Customer Hierarchy Details by account number
     * @param accountNumber - Service account number - e.g. 'LW100275'
     * @param callback - callback handler
     */
    getCustomerHierarchyDetails(accountNumber, callback) {
        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if (this.gotoEliteCore) return elitecoremw.getCustomerHierarchyDetails(accountNumber, callback);

        //this request is related to customer, now will pass this to customer handler in the middleware
        middleWare.getCustomerHierarchyDetails({accountNumber, callback});
    }

    /**
     * Get the business hierarchy details
     * @param callback - callback handler
     */
    getBusinessHierarchyDetail(callback) {

        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.getBusinessHierarchyDetail(callback);

        //this request is related to customer, now will pass this to customer handler in the middleware
        middleWare.getBusinessHierarchyDetail({callback});
    }

    /**
     * Get billing account details by billing account number
     * @param billingAccountNumber - Billing account number - e.g. 'LW100274'
     * @param callback - callback handler
     */
    getBillingAccountDetail(billingAccountNumber, callback) {

        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.getBillingAccountDetail(billingAccountNumber, callback);

        //this request is related to customer, now will pass this to customer handler in the middleware
        middleWare.getBillingAccountDetail({args, callback});
    }

    /**
     * Get ID Type  by code
     * @param code
     */
    getIDTypeByCode(code) {

        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.getIDTypeByCode(code);


        //this request is related to user, now will pass this to customer handler in the middleware
        middleWare.getIDTypeByCode({code});
    }

    /**
     * To change service instance status
     * @param args
     * @param callback - callback handler
     */
    changeServiceInstanceStatus(args, callback) {

        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.changeServiceInstanceStatus(args, callback);


        //this request is related to customer, now will pass this to customer handler in the middleware
        middleWare.changeServiceInstanceStatus({args, callback});
    }

    /**
     * For MNP Notification
     * @param MSISDN
     * @param callback - callback handler
     */
    mnpNotification(MSISDN, callback) {

        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.mnpNotification(MSISDN, callback);


        //this request is related to customer, now will pass this to customer handler in the middleware
        middleWare.mnpNotification({MSISDN, callback});

    }

    /**
     * For Service provisioning notification
     * @param externalRefId
     * @param inventoryNumber
     * @param callback - callback handler
     */
    serviceProvisioningNotification(externalRefId, inventoryNumber, callback) {

        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.serviceProvisioningNotification(externalRefId, inventoryNumber, callback);


        //this request is related to customer, now will pass this to customer handler in the middleware
        middleWare.serviceProvisioningNotification({externalRefId, inventoryNumber, callback});
    }

    /**
     * Update User Information
     * @param accountNumber - Account number - e.g. 'LW100273'
     * @param callback - callback handler
     * @param userKey - User key - e.g. undefined
     */
    updateUserInfo(accountNumber, callback, userKey) {

        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.updateUserInfo(accountNumber, callback, userKey);


        //this request is related to customer, now will pass this to customer handler in the middleware
        middleWare.updateUserInfo({accountNumber, callback, userKey});
    }

    /**
     * Update customer Id
     * @param accountNumber - Customer Account Number
     * @param type - Type
     * @param id - ID
     * @param callback - callback handler
     */
    updateCustomerID(accountNumber, type, id, callback) {

        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.updateCustomerID(accountNumber, type, id, callback);


        //this request is related to user, now will pass this to customer handler in the middleware
        middleWare.updateCustomerID({accountNumber, type, id, callback});
    }

    /**
     * Update customer account data by arguments
     * @param args
     * @param callback - callback handler
     */
    updateCustomerAccountData(args, callback) {
        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.updateCustomerAccountData(args, callback);

        //this request is related to customer, now will pass this to customer handler in the middleware
        middleWare.updateCustomerAccountData({args, callback});
    }

    /**
     * To Update billing account data
     * @param args
     * @param callback - callback handler
     */
    updateBillingAccountData(args, callback) {

        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.updateBillingAccountData(args, callback);

        //this request is related to customer, now will pass this to customer handler in the middleware
        middleWare.updateBillingAccountData({args, callback});

    }

    /**
     * To update billing account
     * @param args -
     * Sample working Input for updating address

        {
            "accountNumber": "LW100274",
            "addressDetails":
            {
             "addressOne": "2113, Henderson road, #6-10",
             "addressTwo": "Henderson road;6;10;2113",
             "zipPostalCode": 510123
            },
            "accountProfiles": {
                  "strcustom1": "20140916001-03112017-1509679237631", //$recurrent_token,
                  "strcustom2": "20140916001", //$sub_mid,
                  "strcustom3": "None", //$bank, customer bank name
                  "strcustom4": "visa", //$paytype,
                  "strcustom5": "1111", //$cc_last_4_digit,
                  "strcustom11": "dbs", //$card_bank_name
              }
        }

     * @param callback - callback handler
     */
    updateBillingAccount(args, callback) {

        validations.updateBillingAccount(args).then(result=>{
            //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
            if(this.gotoEliteCore) return elitecoremw.updateBillingAccount(args, callback);

            //this request is related to customer, now will pass this to customer handler in the middleware
            middleWare.updateBillingAccount({args, callback});
        },callback);
    }


    /**
     * To Update service account
     * @param args
     * @param callback - callback handler
     */
    updateServiceAccount(args, callback) {

        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.updateServiceAccount(args, callback);

        //this request is related to customer, now will pass this to customer handler in the middleware
        middleWare.updateServiceAccount({args, callback});

    }

    /**
     * To Update service account data
     * @param args
     * @param callback - callback handler
     */
    updateServiceAccountData(args, callback) {

        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.updateServiceAccountData(args, callback);

        //this request is related to customer, now will pass this to customer handler in the middleware
        middleWare.updateServiceAccountData({args, callback});
    }

    /**
     * To update customer bucket
     * @param args
     * @param callback - callback handler
     */
    updateCustomerBucket(args, callback) {

        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.updateCustomerBucket(args,callback);


        //this request is related to customer, now will pass this to customer handler in the middleware
        middleWare.updateCustomerBucket({args, callback});

    }

    /**
     * Update account by order reference number
     * @param accountNumber - Customer Account Number
     * @param orn - Order Reference Number
     * @param callback - callback handler
     */
    updateOrderReferenceNumber(accountNumber, orn, callback) {

        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.updateOrderReferenceNumber(accountNumber, orn, callback);


        //this request is related to user, now will pass this to customer handler in the middleware
        middleWare.updateOrderReferenceNumber({accountNumber, orn, callback});
    }

    /**
     * Update activation date
     * @param accountNumber - Customer Account Number
     * @param datestring - Date value
     * @param callback - callback handler
     */
    updateActivationDate(accountNumber, datestring, callback) {

        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.updateActivationDate(accountNumber, datestring, callback);


        //this request is related to user, now will pass this to customer handler in the middleware
        middleWare.updateActivationDate({accountNumber, datestring, callback});
    }

    /**
     * Update customer Date of Birth
     * @param accountNumber - Customer Account Number
     * @param birthday - Birthday
     * @param callback - callback handler
     */
    updateCustomerDOB(accountNumber, birthday, callback) {

        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.updateCustomerDOB(accountNumber, birthday, callback);


        //this request is related to user, now will pass this to customer handler in the middleware
        middleWare.updateCustomerDOB({accountNumber, birthday, callback});
    }

    /**
     * Update all details (Billing, Servce and account) of the customer by account nubmer
     * @param accountNumber - Account number
     * @param params - Params
     * @param callback - callback handler
     * @param skipAccount - Skip Account
     * @param skipBilling - Skip billing
     * @param skipService - Skip Service
     */
    updateAllCustomerAccountsDetails(accountNumber, params, callback, skipAccount, skipBilling, skipService) {

        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.updateAllCustomerAccountsDetails(accountNumber, params, callback, skipAccount, skipBilling, skipService);


        //this request is related to user, now will pass this to customer handler in the middleware
        middleWare.updateAllCustomerAccountsDetails({
            accountNumber,
            params,
            callback,
            skipAccount,
            skipBilling,
            skipService
        });
    }

    /**
     * Update customer order by account numer and reference number
     * @param accountNumber - Customer Account Number
     * @param orderReferenceNumber - Order Reference Number
     * @param callback - callback handler
     */
    updateCustomerOrderReferenceNumber(accountNumber, orderReferenceNumber, callback) {

        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.updateCustomerOrderReferenceNumber(accountNumber, orderReferenceNumber, callback);


        //this request is related to user, now will pass this to customer handler in the middleware
        middleWare.updateCustomerOrderReferenceNumber({accountNumber, orderReferenceNumber, callback});
    }

    /**
     * Load user information
     * @param keyType - Key type - e.g. 'sin_'
     * @param key - Key value - e.g. 'LW100276'
     * @param callback - callback handler
     * @param fresh - Fresh boolean - e.g. false
     */
    loadUserInfo(keyType, key, callback, fresh) {

        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.loadUserInfo(keyType, key, callback, fresh);


        //this request is related to user, now will pass this to customer handler in the middleware
        middleWare.loadUserInfo({keyType, key, callback, fresh});
    }

    /**
     * Update the user info in cache
     * @param id - Id - e.g. 'LW100276'
     * @param callback - callback handler
     * @param fresh - Fresh boolean - e.g. false
     * @param basic - Basic boolean - e.g. false
     */
    loadUserInfoByInternalId(id, callback, fresh, basic) {

        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.loadUserInfoByInternalId(id, callback, fresh, basic);


        //this request is related to user, now will pass this to customer handler in the middleware
        middleWare.loadUserInfoByInternalId({id, callback, fresh, basic});
    }

    /**
     * Load User information by CAN
     * @param can - Customer Account Number - e.g. 'LW100273'
     * @param callback - callback handler
     * @param fresh - Fresh boolean - e.g. false
     * @param basic - Basic boolean - e.g. false
     */
    loadUserInfoByCustomerAccountNumber(can, callback, fresh, basic) {
        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.loadUserInfoByCustomerAccountNumber(can, callback, fresh, basic);


        //this request is related to user, now will pass this to customer handler in the middleware
        middleWare.loadUserInfoByCustomerAccountNumber({can, callback, fresh, basic});

    }

    /**
     * Load User information by BAN
     * @param ban - Business Account Number - e.g. 'LW100274'
     * @param callback - callback handler
     * @param fresh - Fresh boolean - e.g. false
     * @param basic - Basic boolean - e.g. false
     */
    loadUserInfoByBillingAccountNumber(ban, callback, fresh, basic) {
        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.loadUserInfoByBillingAccountNumber(ban, callback, fresh, basic);


        //this request is related to user, now will pass this to customer handler in the middleware
        middleWare.loadUserInfoByBillingAccountNumber({ban, callback, fresh, basic});
    }

    /**
     * Load User information by Service Account Number
     * @param san - Service Account number ex : 'LW100275'
     * @param callback - callback handler
     * @param fresh - ex:false
     * @param basic - ex:false
     */
    loadUserInfoByServiceAccountNumber(san, callback, fresh, basic) {

        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.loadUserInfoByServiceAccountNumber(san, callback, fresh, basic);


        //this request is related to user, now will pass this to customer handler in the middleware
        middleWare.loadUserInfoByServiceAccountNumber({san, callback, fresh, basic});
    }

    /**
     * Load User Information by SIN
     * @param sin - Service Instance number ex : 'LW100276'
     * @param callback - callback handler
     * @param fresh - ex:false
     * @param basic - ex:false
     */
    loadUserInfoByServiceInstanceNumber(sin, callback, fresh, basic) {

        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.loadUserInfoByServiceInstanceNumber(sin, callback, fresh, basic);


        //this request is related to user, now will pass this to customer handler in the middleware
        middleWare.loadUserInfoByServiceInstanceNumber({sin, callback, fresh, basic});
    }

    /**
     * Load User Information by Phone number
     * @param number - Phone number - e.g. '87420765'
     * @param callback - callback handler
     * @param fresh - Fresh boolean - e.g. false
     * @param basic - Basic boolean - e.g. false
     */
    loadUserInfoByPhoneNumber(number, callback, fresh, basic) {

        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.loadUserInfoByPhoneNumber(number, callback, fresh, basic);


        //this request is related to user, now will pass this to customer handler in the middleware
        middleWare.loadUserInfoByPhoneNumber({number, callback, fresh, basic});
    }

    /**
     * Load User Information by User key
     * @param userKey - User key for Native app (Mobile app) - e.g. 'd3ee3ef094574a1a015714207a72e85b'
     * @param callback - callback handler
     * @param fresh - Fresh boolean - e.g. false
     * @param basic - Basic boolean - e.g. false
     */
    loadUserInfoByUserKey(userKey, callback, fresh, basic) {

        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.loadUserInfoByUserKey(userKey, callback, fresh, basic);


        //this request is related to user, now will pass this to customer handler in the middleware
        middleWare.loadUserInfoByUserKey({userKey, callback, fresh, basic});
    }

    /**
     * Load all users basic information
     * @param options - Options like Service Instance number - e.g. '{serviceInstances:["LW100276"]}'
     * @param callback - callback handler
     */
    loadAllUsersBaseInfo(options, callback) {

        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.loadAllUsersBaseInfo(options, callback);


        //this request is related to user, now will pass this to customer handler in the middleware
        middleWare.loadAllUsersBaseInfo({options, callback});
    }

    /**
     * Load all active packages
     * @param serviceInstance - Service Instance - e.g. 'LW100276'
     * @param callback - callback handler
     */
    loadActivePackages(serviceInstance, callback) {

        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.loadActivePackages(serviceInstance, callback);


        //this request is related to user, now will pass this to customer handler in the middleware
        middleWare.loadActivePackages({serviceInstance, callback});
    }

    /**
     * Load customer data with packages
     * @param serviceInstance - Service Instance - e.g. 'CirclesOne'
     * @param callback - callback handler
     */
    loadCustomerWithPackage(serviceInstance, callback) {

        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.loadCustomerWithPackage(serviceInstance, callback);


        //this request is related to user, now will pass this to customer handler in the middleware
        middleWare.loadCustomerWithPackage({serviceInstance, callback});
    }

    /**
     * Clear user information from db cache
     * @param accountNumber - Customer account number ex : 'LW100273'
     * @param callback - callback handler
     */
    clearUserInfo(accountNumber, callback) {

        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.clearUserInfo(accountNumber,callback);

        //this request is related to user, now will pass this to customer handler in the middleware
        middleWare.clearUserInfo({accountNumber, callback});
    }

    //endregion

    //region - Billing, Inventory

    /**
     * To load all Bill Invoices
     * @param monthDate - e.g. '2017-10-01'
     * @param callback - callback handler
     */
    loadBillInvoices(monthDate, callback) {
        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.loadBillInvoices(monthDate, callback);


        //this request is related to billing, now will pass this to billing handler in the middleware
        middleWare.loadBillInvoices({monthDate, callback});
    }

    /**
     * To load all unpaid bill invoices
     * @param callback - callback handler
     */
    loadUnpaidBillInvoices(callback) {

        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.loadUnpaidBillInvoices(callback);

        //This request is related to billing, now will pass this to billing handler in the middleware
        middleWare.loadUnpaidBillInvoices({callback})
    }

    /**
     * To load bill invoices by Ids
     * @param invoiceIds - Invoice Ids - e.g. ["REG0000000085556", "REG0000000085248"]
     * @param callback - callback handler
     */
    loadBillInvoicesByIds(invoiceIds, callback) {
        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.loadBillInvoicesByIds(invoiceIds, callback);

        //This request is related to billing, now will pass this to billing handler in the middleware
        middleWare.loadBillInvoicesByIds({invoiceIds, callback});
    }

    /**
     * To clear customer payments cache
     * @param billingAccountNumber - Billing account number
     */
    clearCustomerPaymentsCache(billingAccountNumber) {

        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.clearCustomerPaymentsCache(billingAccountNumber);

        //This request is related to billing, now will pass this to billing handler in the middleware
        middleWare.clearCustomerPaymentsCache({billingAccountNumber});
    }

    /**
     * Callback service for various inner services
     * @param billingAccountNumber - e.g. 'LW100274'
     * @param callback - callback handler
     * @param fresh - e.g. false
     * @param basic - e.g. false
     */
    loadCustomerPaymentsInfo(billingAccountNumber, callback, fresh, basic) {

        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.loadCustomerPaymentsInfo(billingAccountNumber, callback, fresh, basic);

        //This request is related to billing, now will pass this to billing handler in the middleware
        middleWare.loadCustomerPaymentsInfo({billingAccountNumber, callback, fresh, basic});
    }

    /**
     * Search bill by accout number, from and to date
     * @param accountNumber - Account number - e.g. 'LW100274'
     * @param fromDate - From date - e.g. '2017-01-01'
     * @param toDate - To date -  e.g. '2017-02-01'
     * @param callback - callback handler
     */
    searchBill(accountNumber, fromDate, toDate, callback) {

        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.searchBill(accountNumber, fromDate, toDate, callback);

        //this request is related to customer, now will pass this to customer handler in the middleware
        middleWare.searchBill({accountNumber, fromDate, toDate, callback});
    }

    /**
     * Search credit note by arguments
     * @param args
     * @param callback - callback handler
     */
    searchCreditNote(args, callback) {
        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.searchCreditNote(args, callback);

        //this request is related to customer, now will pass this to customer handler in the middleware
        middleWare.searchCreditNote({args, callback});
    }

    /**
     * Add deposit to customer billing account by billing account number, deposit account and reason
     * @param billingAccountNumber - Billing account number
     * @param depositAmount - Deposit amount
     * @param reason - Reason
     * @param callback - callback handler
     */
    addDepositToCustomerBillingAccount(billingAccountNumber, depositAmount, reason, callback) {

        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.addDepositToCustomerBillingAccount(billingAccountNumber, depositAmount, reason, callback);

        //this request is related to customer, now will pass this to customer handler in the middleware
        middleWare.addDepositToCustomerBillingAccount({billingAccountNumber, depositAmount, reason, callback});
    }

    /**
     * Make debit payment data by makeDebitPaymentDataRequest
     * @param accountNumber - Account number
     * @param transactionAmount - Transaction amount
     * @param documentNumber - Document number
     * @param paymentDate - Payment date
     * @param channelAlias - Channel alias
     * @param callback - callback handler
     */
    makeDebitPaymentDataRequest(accountNumber, transactionAmount, documentNumber, paymentDate, channelAlias, callback) {
        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.makeDebitPaymentDataRequest(accountNumber, transactionAmount, documentNumber, paymentDate,channelAlias, callback);

        //this request is related to customer, now will pass this to customer handler in the middleware
        middleWare.makeDebitPaymentDataRequest({
            accountNumber,
            transactionAmount,
            documentNumber,
            paymentDate,
            channelAlias,
            callback
        });
    }

    /**
     * Make advance payment by makeAdvancePaymentDataRequest object
     * @param accountNumber - Account number
     * @param transactionAmount - Transaction amount
     * @param paymentDate - Payment date
     * @param channelAlias - Channel alias
     * @param description - Description
     * @param callback - callback handler
     */
    makeAdvancePaymentDataRequest(accountNumber, transactionAmount, paymentDate, channelAlias, description, callback) {

        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.makeAdvancePaymentDataRequest(accountNumber, transactionAmount, paymentDate,channelAlias, description, callback);

        //this request is related to customer, now will pass this to customer handler in the middleware
        middleWare.makeAdvancePaymentDataRequest({
            accountNumber,
            transactionAmount,
            paymentDate,
            channelAlias,
            description,
            callback
        });
    }

    /**
     * To search payments
     * @param args
     * @param callback - callback handler
     */
    searchPayment(args, callback) {

        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.searchPayment(args, callback);


        //this request is related to customer, now will pass this to customer handler in the middleware
        middleWare.searchPayment({args, callback});
    }

    /**
     * To create Credit note
     * @param args
     * @param callback - callback handler
     */
    createCreditNote(args, callback) {

        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.createCreditNote(args, callback);


        //this request is related to customer, now will pass this to customer handler in the middleware
        middleWare.createCreditNote({args, callback});

    }

    /**
     * To create Inventory
     * @param msisdn
     * @param donorCode
     * @param date
     * @param callback - callback handler
     */
    createInventory(msisdn, donorCode, date, callback) {

        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.createInventory(msisdn, donorCode, date, callback);


        //this request is related to customer, now will pass this to customer handler in the middleware
        middleWare.createInventory({msisdn, donorCode, date, callback});
    }

    /**
     * Get inventory details by the arguments
     * @param args
     * @param callback - callback handler
     */
    getInventoryDetail(args, callback) {

        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.getInventoryDetail(args, callback);

        //this request is related to customer, now will pass this to customer handler in the middleware
        middleWare.getInventoryDetail({args, callback});

    }

    /**
     * Add inventory by effective date, inventory number, subtype, Ismanualinventory, packageid and service instance number
     * @param effectDate - Effect date
     * @param inventoryNumber - Inventory number
     * @param serviceInstanceNumber - Service Instance number
     * @param callback - callback handler
     */
    addInventory(effectDate, inventoryNumber, serviceInstanceNumber, callback) {


        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.addInventory(effectDate, inventoryNumber, serviceInstanceNumber, callback);

        //this request is related to customer, now will pass this to customer handler in the middleware
        middleWare.addInventory({effectDate, inventoryNumber, serviceInstanceNumber, callback});
    }

    /**
     * Get inventory list by the sevice instance number
     * @param serviceInstanceNumber - Service instance number - e.g. 'LW100276'
     * @param callback - callback handler
     */
    listInventory(serviceInstanceNumber, callback) {

        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.listInventory(serviceInstanceNumber, callback);

        //this request is related to customer, now will pass this to customer handler in the middleware
        middleWare.listInventory({serviceInstanceNumber, callback});
    }

    /**
     * Unpair inventory by arguments
     * @param args
     * @param callback - callback handler
     */
    unpairInventory(args, callback) {
        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.unpairInventory(args, callback);

        //this request is related to customer, now will pass this to customer handler in the middleware
        middleWare.unpairInventory({args, callback});
    }

    /**
     * Repair inventory by arguments
     * @param args
     * @param callback - callback handler
     */
    repairInventory(args, callback) {

        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.repairInventory(args, callback);

        //this request is related to customer, now will pass this to customer handler in the middleware
        middleWare.repairInventory({args, callback});
    }

    /**
     * repair inventory xml by arguments
     * @param args
     * @param callback - callback handler
     */
    repairInventoryXml(args, callback) {

        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.repairInventoryXml(args, callback);

        //this request is related to customer, now will pass this to customer handler in the middleware
        middleWare.repairInventoryXml({args, callback});
    }

    /**
     * Remove inventory by service instance number and inventory number
     * @param serviceInstanceNumber - Service instance number
     * @param inventoryNumber - Inventory number
     * @param callback - callback handler
     */
    removeInventory(serviceInstanceNumber, inventoryNumber, callback) {

        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.removeInventory(serviceInstanceNumber, inventoryNumber, callback);

        //this request is related to customer, now will pass this to customer handler in the middleware
        middleWare.removeInventory({serviceInstanceNumber, inventoryNumber, callback});

    }

    /**
     * To update inventory attributes
     * @param msisdn
     * @param donorCode
     * @param date
     * @param callback - callback handler
     */
    updateInventoryAttributes(msisdn, donorCode, date, callback) {

        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.updateInventoryAttributes(msisdn, donorCode, date, callback);


        //this request is related to customer, now will pass this to customer handler in the middleware
        middleWare.updateInventoryAttributes({msisdn, donorCode, date, callback});
    }

    /**
     * Get the inventory details by the inventory number
     * @param inventoryNumber - Inventory number - e.g. 1
     * @param callback - callback handler
     */
    searchInventory(inventoryNumber, callback) {

        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.searchInventory(inventoryNumber, callback);

        //this request is related to customer, now will pass this to customer handler in the middleware
        middleWare.searchInventory({inventoryNumber, callback});

    }

    /**
     * Get the list of inventories by the inventory status id, subtype id, owner id (default - 'operator')
     * @param inventoryStatusId - Inventory status Id - e.g. 'NBS01'
     * @param inventorySubTypeId - Inventory Subtype Id - e.g. 'NTP00016'
     * @param callback - callback handler
     */
    searchInventoryList(inventoryStatusId, inventorySubTypeId, callback) {

        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.searchInventoryList(inventoryStatusId, inventorySubTypeId, callback);

        //this request is related to customer, now will pass this to customer handler in the middleware
        middleWare.searchInventoryList({inventoryStatusId, inventorySubTypeId, callback});

    }

    /**
     * Do the inventory operation by the inventory number, operationAlias and reference number(Default  - N/A)
     * @param inventoryNumber - Inventory Number
     * @param operationAlias - Operation Alias
     * @param callback - callback handler
     */
    doInventoryStatusOperation(inventoryNumber, operationAlias, callback) {

        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.doInventoryStatusOperation(inventoryNumber, operationAlias, callback);

        //this request is related to customer, now will pass this to customer handler in the middleware
        middleWare.doInventoryStatusOperation({inventoryNumber, operationAlias, callback});
    }

    /**
     * repair inventory bundle
     * @param args
     * @param callback - callback handler
     */
    repairInventoryRebundle(args, callback) {

        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.repairInventoryRebundle(args, callback);

        //this request is related to customer, now will pass this to customer handler in the middleware
        middleWare.repairInventoryRebundle({args, callback});

    }

    /**
     * Mark inventory as damaged by inventory number and serviceInstanceNumber
     * @param inventoryNumber - Inventory number
     * @param serviceInstanceNumber - Service instance number
     * @param callback - callback handler
     */
    markInventoryAsDamaged(inventoryNumber, serviceInstanceNumber, callback) {

        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.markInventoryAsDamaged(inventoryNumber, serviceInstanceNumber, callback);

        //this request is related to customer, now will pass this to customer handler in the middleware
        middleWare.markInventoryAsDamaged({inventoryNumber, serviceInstanceNumber, callback});
    }

    //endregion

    //region - Package, Product

    /**
     * To load hierarchy
     * @param callback - callback handler
     */
    loadHierarchy(callback) {

        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.loadHierarchy(callback);

        //This request is related to billing, now will pass this to billing handler in the middleware
        middleWare.loadHierarchy({callback});

    }

    /**
     * To load Packages
     * @param teamId - Team Id ex: 'PRD00440'
     * @param callback - callback handler
     */
    loadPackages(teamId, callback) {

        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.loadPackages(teamId, callback);

        //This request is related to billing, now will pass this to billing handler in the middleware
        middleWare.loadPackages({teamId, callback});

    }

    /**
     * To load packages fro team API key
     * @param teamApiKey -Team Api key ex: 'sLZ7XNcxrN'
     * @param callback - callback handler
     */
    loadPackagesForTeamApiKey(teamApiKey, callback) {

        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.loadPackagesForTeamApiKey(teamApiKey, callback);

        //This request is related to billing, now will pass this to billing handler in the middleware
        middleWare.loadPackagesForTeamApiKey({teamApiKey, callback});

    }

    /**
     * Get all the product offer details
     * @param callback - callback handler
     */
    getProductOfferDetails(callback) {

        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.getProductOfferDetails(callback);

        //this request is related to customer, now will pass this to customer handler in the middleware
        middleWare.getProductOfferDetails(callback);

    }

    /**
     * Get the specific product offer details by the  product Id
     * @param args - args means productId - e.g. 'PRD00192'
     * @param callback - callback handler
     */
    getSpecificProdOfferDetails(args, callback) {
        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.getSpecificProdOfferDetails(args, callback);

        //this request is related to customer, now will pass this to customer handler in the middleware
        middleWare.getSpecificProdOfferDetails({args, callback});
    }

    /**
     * To modify plan attributes
     * @param args
     * @param callback - callback handler
     */
    modifyPlanAttributes(args, callback) {
        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.modifyPlanAttributes(args, callback);


        //this request is related to customer, now will pass this to customer handler in the middleware
        middleWare.modifyPlanAttributes({args, callback});
    }

    /**
     * Change service plan by account number, effective date, pacakage name, remarks and service instance number
     * @param billingAccountNumber - Billing account number
     * @param effectiveDate - Effective date
     * @param packageName - Package name
     * @param remarks - Remarks
     * @param serviceInstanceNumber - Service instance number
     * @param callback - callback handler
     */
    changeServicePlan(billingAccountNumber, effectiveDate, packageName, remarks, serviceInstanceNumber, callback) {

        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.changeServicePlan(billingAccountNumber, effectiveDate, packageName, remarks, serviceInstanceNumber, callback);

        //this request is related to customer, now will pass this to customer handler in the middleware
        middleWare.changeServicePlan({
            billingAccountNumber,
            effectiveDate,
            packageName,
            remarks,
            serviceInstanceNumber,
            callback
        });
    }

    /**
     * To subscribe add on package
     * @param args
     * {
          "addOnPlans": [{"packageId": $package}, ...],
          "serviceInstanceNumber": $service_instance_number
        }
     * @param callback - callback handler
     */
    subscribeAddOnPackage(args, callback) {

        validations.subscribeAddOnPackage(args).then(result=> {

            //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
            if(this.gotoEliteCore) return elitecoremw.subscribeAddOnPackage(args, callback);

            //this request is related to customer, now will pass this to customer handler in the middleware
            middleWare.subscribeAddOnPackage({args, callback});
        },callback);
    }

    /**
     * To un-subscribe add on package
     * @param args
     * @param callback - callback handler
     */
    unSubscribeAddOnPackage(args, callback) {
        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.unSubscribeAddOnPackage(args, callback);

        //this request is related to customer, now will pass this to customer handler in the middleware
        middleWare.unSubscribeAddOnPackage({args, callback});
    }

    /**
     * To get FnF member details
     * @param args
     * @param callback - callback handler
     */
    getFnFMembersDetails(args, callback) {

        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.getFnFMembersDetails(args, callback);

        //this request is related to customer, now will pass this to customer handler in the middleware
        middleWare.getFnFMembersDetails({args, callback});
    }

    /**
     * To add FnF members
     * @param args
     * @param callback - callback handler
     */
    addFnFMembers(args, callback) {

        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.addFnFMembers(args, callback);

        //this request is related to customer, now will pass this to customer handler in the middleware
        middleWare.addFnFMembers({args, callback});
    }

    /**
     * To remove FnF members
     * @param args
     * @param callback - callback handler
     */
    removeFnFMembers(args, callback) {

        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.removeFnFMembers(args, callback);


        //this request is related to customer, now will pass this to customer handler in the middleware
        middleWare.removeFnFMembers({args, callback});
    }

    //endregion

    //region - Usage

    /**
     * To load Customer Usage
     * @param serviceInstance - Service instance - e.g. 'LW0336031'
     * @param callback - callback handler
     */
    loadCustomerUsage(serviceInstance, callback) {

        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.loadCustomerUsage(serviceInstance,callback);

        //This request is related to billing, now will pass this to billing handler in the middleware
        middleWare.loadCustomerUsage({serviceInstance, callback});
    }

    /**
     * Get the usage data and counter balance data  from DB call or view customer method in connector
     * @param serviceInstanceNumber - Service instance number - e.g. 'LW100276'
     * @param callback - callback handler
     * @param fresh - e.g. false
     * @param basic - e.g. false
     */
    loadCurrentCustomerUsage(serviceInstanceNumber, callback, fresh, basic) {

        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.loadCurrentCustomerUsage(serviceInstanceNumber, callback, fresh, basic);

        //This request is related to billing, now will pass this to billing handler in the middleware
        middleWare.loadCurrentCustomerUsage({serviceInstanceNumber, callback, fresh, basic});

    }

    /**
     * Get remaining data for pacakge  by service Instance number
     * @param number - Number
     * @param packageId - Package Id
     * @param callback - callback handler
     */
    loadRemainingDataForPackage(number, packageId, callback) {

        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.loadRemainingDataForPackage(number, packageId, callback);

        //This request is related to billing, now will pass this to billing handler in the middleware
        middleWare.loadRemainingDataForPackage({number, packageId, callback});

    }

    /**
     * To view customer usage information
     * @param args
     * @param callback - callback handler
     */
    viewCustomerUsageInformation(args, callback) {
        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.viewCustomerUsageInformation(args, callback);


        //this request is related to customer, now will pass this to customer handler in the middleware
        middleWare.viewCustomerUsageInformation({args, callback});
    }

    //endregion

    //region - PortIn

    /**
     * PortIn Number
     * @param serviceInstanceNumber - Service account number ex : 'LW100275'
     * @param tempNumber - Temp number ex : 'LW100275'
     * @param portInNumber - Port In number ex : 'LW100275'
     * @param donor - Donor ex : 'LW100275'
     * @param startTime - ex:
     * @param externalRefId  - ex:
     * @param callback - callback handler
     * @param portInRecovery - ex:
     */
    portInNumber(serviceInstanceNumber, tempNumber, portInNumber, donor, startTime, externalRefId,
                 callback, portInRecovery) {

        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.portInNumber(serviceInstanceNumber, tempNumber, portInNumber, donor, startTime, externalRefId,
            callback, portInRecovery);


        //this request is related to customer, now will pass this to customer handler in the middleware
        middleWare.portInNumber({
            serviceInstanceNumber, tempNumber, portInNumber, donor, startTime, externalRefId,
            callback, portInRecovery
        });

    }


    /**
     * PortIn Number Immediate
     * @param serviceInstanceNumber - Service account number ex : 'LW100275'
     * @param tempNumber - Temp number ex :
     * @param portInNumber - Port In number ex :
     * @param donor - Service account number ex : 'LW100275'
     * @param externalRefId - ex:
     * @param callback - callback handler
     * @param portInRecovery - ex:
     */
    portInNumberImmediate(serviceInstanceNumber, tempNumber, portInNumber,
                          donor, externalRefId, callback, portInRecovery) {

        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.portInNumberImmediate(serviceInstanceNumber, tempNumber, portInNumber,
            donor, externalRefId, callback, portInRecovery);


        //this request is related to customer, now will pass this to customer handler in the middleware
        middleWare.portInNumberImmediate({
            serviceInstanceNumber, tempNumber, portInNumber,
            donor, externalRefId, callback, portInRecovery
        });

    }

    /**
     * Get Status of PortIn
     * @param portInNumber
     * @param status
     */
    getPortInStatus(portInNumber, status) {

        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.getPortInStatus(portInNumber,status);


        //this request is related to customer, now will pass this to customer handler in the middleware
        middleWare.getPortInStatus({portInNumber, status});

    }

    /**
     * Get Donor Code by Network Name
     * @param donor
     */
    getDonorCodeByNetworkName(donor) {

        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.getDonorCodeByNetworkName(donor);


        //this request is related to customer, now will pass this to customer handler in the middleware
        middleWare.getDonorCodeByNetworkName({donor});

    }

    /**
     * Get Reason for PortIn failure
     * @param portInNumber
     * @param start_ts
     * @param callback - callback handler
     */
    loadPortInFailureReason(portInNumber, start_ts, callback) {

        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.loadPortInFailureReason(portInNumber, start_ts, callback);


        //this request is related to customer, now will pass this to customer handler in the middleware
        middleWare.loadPortInFailureReason({portInNumber, start_ts, callback});

    }

    /**
     * Get status of portin completion
     * @param portInNumber
     * @param callback - callback handler
     */
    loadPortInCompleationStatus(portInNumber, callback) {

        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.loadPortInCompleationStatus(portInNumber,callback);


        //this request is related to customer, now will pass this to customer handler in the middleware
        middleWare.loadPortInCompleationStatus({portInNumber, callback});

    }

    /**
     * Deactivate portIn Number
     * @param serviceInstanceNumber - Service account number ex : 'LW100275'
     * @param portInNumber
     * @param callback - callback handler
     */
    deactivatePortInNumber(serviceInstanceNumber, portInNumber, callback) {

        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.deactivatePortInNumber(serviceInstanceNumber, portInNumber, callback);


        //this request is related to customer, now will pass this to customer handler in the middleware
        middleWare.deactivatePortInNumber({serviceInstanceNumber, portInNumber, callback});

    }

    /**
     * To Cancel PortIn order
     * @param MSISDN
     * @param reasonCode
     * @param callback - callback handler
     */
    cancelPortInOrder(MSISDN, reasonCode, callback) {
        //The following line will be here untill elitecore is used. Once elitecore is removed this line can be deleted.
        if(this.gotoEliteCore) return elitecoremw.cancelPortInOrder(MSISDN, reasonCode, callback);


        //this request is related to customer, now will pass this to customer handler in the middleware
        middleWare.cancelPortInOrder({MSISDN, reasonCode, callback});
    }

    //endregion

}

let bss = new BSS();
module.exports = bss;