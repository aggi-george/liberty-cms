/*globals require, module */

//This is related only to Billing

const Promise = require('bluebird');
const oraclemw = require('../../oracle/index');

class Billing{
    constructor(){

    }

    /**
     * To load all Bill Invoices
     * params- {monthDate, callback}
     */
    loadBillInvoices(params){
        let {monthDate, callback} = params;

        //from here we hop to the actual oracle mw

        oraclemw.loadBillInvoices({monthDate}).then(result=>{
            callback(null,result);
        },callback);
    }

    /**
     * To load all unpaid bill invoices
     * params - {callback}
     */
    loadUnpaidBillInvoices(params){
        let {callback} = params;

        oraclemw.loadUnpaidBillInvoices().then(result=>{
            callback(null,result);
        },callback);

    }

    /**
     * To load bill invoices by Ids
     * params - {invoiceIds, callback}
     */
    loadBillInvoicesByIds (params){
        let {invoiceIds, callback}=params;

        oraclemw.loadBillInvoicesByIds({invoiceIds}).then(result=>{
            callback(null,result);
        },callback);
    }

    /**
     * To clear customer payments cache
     * @param params.- {billingAccountNumber}
     */
    clearCustomerPaymentsCache (params){
        let {billingAccountNumber}=params;

        oraclemw.clearCustomerPaymentsCache({billingAccountNumber}).then(result=>{
            callback(null,result);
        },callback);
    }

    /**
     * Callback service for various inner services
     * @param params.- {billingAccountNumber, callback, fresh, basic}
     */
    loadCustomerPaymentsInfo(params){
        let {billingAccountNumber, callback, fresh, basic}=params;

        oraclemw.loadCustomerPaymentsInfo({billingAccountNumber, fresh, basic}).then(result=>{
            callback(null,result);
        },callback);
    }

    /**
     * Search bill by accout number, from and to date
     * @param params.- {accountNumber, fromDate, toDate, callback}
     */
    searchBill(params){
        let {accountNumber, fromDate, toDate, callback} = params;

        oraclemw.searchBill({accountNumber, fromDate, toDate}).then(result=>{
            callback(null,result);
        },callback);
    }

    /**
     * Search credit note by arguments
     * @param params.- {args, callback}
     */
    searchCreditNote(params){
        let {args, callback} = params;

        oraclemw.searchCreditNote(args).then(result=>{
            callback(null,result);
        },callback);
    }

    /**
     * Add deposit to customer billing account by billing account number, deposit account and reason
     * @param params.- {billingAccountNumber, depositAmount, reason, callback}
     */
    addDepositToCustomerBillingAccount(params){
        let {billingAccountNumber, depositAmount, reason, callback} = params;

        oraclemw.addDepositToCustomerBillingAccount({billingAccountNumber, depositAmount, reason}).then(result=>{
            callback(null,result);
        },callback);
    }

    /**
     * Make debit payment data by makeDebitPaymentDataRequest
     * @param params.- {accountNumber, transactionAmount, documentNumber, paymentDate,
     *                                      channelAlias, callback}
     */
    makeDebitPaymentDataRequest(params){
        let {accountNumber, transactionAmount, documentNumber, paymentDate, channelAlias, callback} = params;

        oraclemw.makeDebitPaymentDataRequest({accountNumber, transactionAmount, documentNumber, paymentDate,
            channelAlias}).then(result=>{
            callback(null,result);
        },callback);
    }

    /**
     * Make advance payment by makeAdvancePaymentDataRequest object
     * @param params.- {accountNumber, transactionAmount, paymentDate, channelAlias, description, callback}
     */
    makeAdvancePaymentDataRequest(params){
        let {accountNumber, transactionAmount, paymentDate,channelAlias, description, callback} = params;

        oraclemw.makeAdvancePaymentDataRequest({accountNumber, transactionAmount, paymentDate,channelAlias, description}).then(result=>{
            callback(null,result);
        },callback);
    }

    /**
     * To search payments
     * @param params.- {args, callback}
     */
    searchPayment(params){
        let {args, callback} = params;

        oraclemw.searchPayment(args).then(result=>{
            callback(null,result);
        },callback);
    }

    /**
     * To create Credit note
     * @param params.- {args, callback}
     */
    createCreditNote(params){
        let {args, callback} = params;

        oraclemw.createCreditNote(args).then(result=>{
            callback(null,result);
        },callback);
    }

    /**
     * To create Inventory
     * @param params.- {msisdn, donorCode, date, callback}
     */
    createInventory(params){
        let {msisdn, donorCode, date, callback} = params;

        oraclemw.createInventory({msisdn, donorCode, date}).then(result=>{
            callback(null,result);
        },callback);

    }

    /**
     * Get inventory details by the arguments
     * @param params.- {args, callback}
     */
    getInventoryDetail(params){
        let {args, callback} = params;

        oraclemw.getInventoryDetail({args}).then(result=>{
            callback(null,result);
        },callback);
    }

    /**
     * Add inventory by effective date, inventory number, subtype, Ismanualinventory, packageid and service instance number
     * @param params.- {effectDate, inventoryNumber, serviceInstanceNumber, callback}
     */
    addInventory(params){
        let {effectDate, inventoryNumber, serviceInstanceNumber, callback} = params;

        oraclemw.addInventory({effectDate, inventoryNumber, serviceInstanceNumber}).then(result=>{
            callback(null,result);
        },callback);
    }

    /**
     * Get inventory list by the sevice instance number
     * @param params.- {serviceInstanceNumber, callback}
     */
    listInventory(params){
        let {serviceInstanceNumber, callback} = params;

        oraclemw.listInventory({serviceInstanceNumber}).then(result=>{
            callback(null,result);
        },callback);
    }

    /**
     * Unpair inventory by arguments
     * @param params.- {args, callback}
     */
    unpairInventory(params){
        let {args, callback} = params;

        oraclemw.unpairInventory({args}).then(result=>{
            callback(null,result);
        },callback);

    }

    /**
     * Repair inventory by arguments
     * @param params.- {args, callback}
     */
    repairInventory(params){
        let {args, callback} = params;

        oraclemw.repairInventory({args}).then(result=>{
            callback(null,result);
        },callback);
    }

    /**
     * repair inventory xml by arguments
     * @param params.- {args, callback}
     */
    repairInventoryXml(params){
        let {args, callback} = params;

        oraclemw.repairInventoryXml({args}).then(result=>{
            callback(null,result);
        },callback);
    }

    /**
     * Remove inventory by service instance number and inventory number
     * @param params.- {serviceInstanceNumber, inventoryNumber, callback}
     */
    removeInventory(params){
        let {serviceInstanceNumber, inventoryNumber, callback} = params;

        oraclemw.removeInventory({serviceInstanceNumber, inventoryNumber}).then(result=>{
            callback(null,result);
        },callback);
    }

    /**
     * To update inventory attributes
     * @param params.- {msisdn, donorCode, date, callback}
     */
    updateInventoryAttributes(params){
        let {msisdn, donorCode, date, callback} = params;

        oraclemw.updateInventoryAttributes({msisdn, donorCode, date}).then(result=>{
            callback(null,result);
        },callback);
    }

    /**
     * Get the inventory details by the inventory number
     * @param params.- {inventoryNumber, callback}
     */
    searchInventory(params){
        let {inventoryNumber, callback} = params;

        oraclemw.searchInventory({inventoryNumber}).then(result=>{
            callback(null,result);
        },callback);
    }

    /**
     * Get the list of inventories by the inventory status id, subtype id, owner id (default - 'operator')
     * @param params.- {inventoryStatusId, inventorySubTypeId, callback}
     */
    searchInventoryList(params){
        let {inventoryStatusId, inventorySubTypeId, callback} = params;

        oraclemw.searchInventoryList({inventoryStatusId, inventorySubTypeId}).then(result=>{
            callback(null,result);
        },callback);
    }

    /**
     * Do the inventory operation by the inventory number, operationAlias and reference number(Default  - N/A)
     * @param params.- {inventoryNumber, operationAlias, callback}
     */
    doInventoryStatusOperation(params){
        let {inventoryNumber, operationAlias, callback} = params;

        oraclemw.doInventoryStatusOperation({inventoryNumber, operationAlias}).then(result=>{
            callback(null,result);
        },callback);

    }

    /**
     * repair inventory bundle
     * params- {args, callback}
     */
    repairInventoryRebundle(params){
        let {args, callback} = params;

        oraclemw.repairInventoryRebundle({args}).then(result=>{
            callback(null,result);
        },callback);

    }

    /**
     * Mark inventory as damaged by inventory number and serviceInstanceNumber
     * @param params.- {inventoryNumber, serviceInstanceNumber, callback}
     */
    markInventoryAsDamaged(params){
        let {inventoryNumber, serviceInstanceNumber, callback} = params;

        oraclemw.markInventoryAsDamaged({inventoryNumber, serviceInstanceNumber}).then(result=>{
            callback(null,result);
        },callback);
    }
}

let billing = new Billing();
module.exports = billing;