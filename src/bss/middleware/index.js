/*globals require */

const userService = require('./user/index');
const billingService = require('./billing/index');
const packageService = require('./package/index');
const portinService = require('./portin/index');
const usageService = require('./usage/index');
const commonError = require('../common/error');
const testService = require('./test/index');


class MiddleWare{
    constructor(){

    }

    /**
     * Create Account
     * @param params - {args, callback}
     */
    createAccount(params){
        userService.createAccount(params);
    }

    /**
     * search account by mobile or MSISDN Number, account number, service username and account name
     * @param params - {number, accountNumber, accountName, serviceUserName, callback}
     */
    searchAccount(params){
       userService.searchAccount(params);
    }

    /**
     * search service instance by mobile or MSISDN Number, account number and account name
     * @param params - {number, accountNumber, accountName, callback}
     */
    searchServiceInstance(params) {
        userService.searchServiceInstance(params);
    }

    /**
     * To Views customer
     * @param params - {serviceInstanceNumber, fromDate, callback}
     */
    viewCustomer(params) {
        userService.viewCustomer(params);
    }

    /**
     * Get account statement by account number, from and to date
     * @param params - {accountNumber, fromDate, toDate, callback}
     */
    getAccountStatement(params) {
        userService.getAccountStatement(params);
    }

    /**
     * To get Customer Hierarchy Details by account number
     * @param params - {accountNumber, callback}
     */
    getCustomerHierarchyDetails(params) {
        userService.getCustomerHierarchyDetails(params);
    }

    /**
     * Get the business hierarchy details
     * @param params - {callback}
     */
    getBusinessHierarchyDetail(params) {
        userService.getBusinessHierarchyDetail(params);
    }

    /**
     * To get billing account detail
     * @param params - {args, callback}
     */
    getBillingAccountDetail(params) {
        userService.getBillingAccountDetail(params);
    }

    /**
     * Get ID Type  by code
     * @param params - {code}
     */
    getIDTypeByCode(params) {
        userService.getIDTypeByCode(params)
    }

    /**
     * To change service instance status
     * @param params - {args, callback}
     */
    changeServiceInstanceStatus(params) {
        userService.changeServiceInstanceStatus(params);
    }

    /**
     * For MNP Notification
     * @param params - {MSISDN, callback}
     */
    mnpNotification(params) {
        userService.mnpNotification(params);
    }

    /**
     * For Service provisioning notification
     * @param params - {externalRefId, inventoryNumber, callback}
     */
    serviceProvisioningNotification(params) {
        userService.serviceProvisioningNotification(params);
    }

    /**
     * Update User Information
     * @param params - {accountNumber, callback, userKey}
     */
    updateUserInfo(params) {
        userService.updateUserInfo(params);
    }

    /**
     * Update customer Id
     * @param params - {accountNumber, type, id, callback}
     */
    updateCustomerID(params) {
        userService.updateCustomerID(params);
    }

    /**
     * Update customer account data by arguments
     * @param params - {args, callback}
     */
    updateCustomerAccountData(params) {
        userService.updateCustomerAccountData(params);
    }

    /**
     * To Update billing account data
     * @param params - {args, callback}
     */
    updateBillingAccountData(params) {
        userService.updateBillingAccountData(params);
    }

    /**
     * To Update billing account data
     * @param params - {args, callback}
     */
    updateBillingAccountData(params) {
        userService.updateBillingAccountData(params);
    }


    /**
     * To update billing account
     * @param params - {args, callback}
     */
    updateBillingAccount(params) {
        userService.updateBillingAccount(params);
    }


    /**
     * To Update service account data
     * @param params - {args, callback}
     */
    updateServiceAccountData(params) {
        userService.updateServiceAccountData(params)
    }

    /**
     * To update customer bucket
     * @param params - {args, callback}
     */
    updateCustomerBucket(params) {
        userService.updateCustomerBucket(params);
    }

    /**
     * Update account by order reference number
     * @param params - {accountNumber, orn, callback}
     */
    updateOrderReferenceNumber(params) {
        userService.updateOrderReferenceNumber(params);
    }

    /**
     * Update activation date
     * @param params - {accountNumber, datestring, callback}
     */
    updateActivationDate(params) {
        userService.updateActivationDate(params);
    }

    /**
     * Update customer Date of Birth
     * @param params - {accountNumber, birthday, callback}
     */
    updateCustomerDOB(params) {
        userService.updateCustomerDOB(params);
    }

    /**
     * Update all details (Billing, Servce and account) of the customer by account nubmer
     * @param params - {accountNumber, params, callback, skipAccount, skipBilling, skipService}
     */
    updateAllCustomerAccountsDetails(params) {
        userService.updateAllCustomerAccountsDetails(params);
    }

    /**
     * Update customer order by account numer and reference number
     * @param params - {accountNumber, orderReferenceNumber, callback}
     */
    updateCustomerOrderReferenceNumber(params) {
        userService.updateCustomerOrderReferenceNumber(params);
    }

    /**
     * Load user information
     * @param params - {keyType, key, callback, fresh}
     */
    loadUserInfo(params) {
        userService.loadUserInfo(params);
    }

    /**
     * Update the user info by internal id
     * @param params - {id, callback, fresh, basic}
     */
    loadUserInfoByInternalId(params) {
        userService.loadUserInfoByInternalId(params);
    }

    /**
     * Load User information by Customer Account Number
     * @param params - {can, callback, fresh, basic}
     */
    loadUserInfoByCustomerAccountNumber(params) {
        userService.loadUserInfoByCustomerAccountNumber(params);
    }

    /**
     * Load User information by Billing Account Number
     * @param params - {ban, callback, fresh, basic}
     */
    loadUserInfoByBillingAccountNumber(params) {
        userService.loadUserInfoByBillingAccountNumber(params);
    }

    /**
     * Load User information by Service Account Number
     * @param params - {san, callback, fresh, basic}
     */
    loadUserInfoByServiceAccountNumber(params) {
        userService.loadUserInfoByServiceAccountNumber(params);
    }

    /**
     * Load User Information by Service Instance Number
     * @param params - {sin, callback, fresh, basic}
     */
    loadUserInfoByServiceInstanceNumber(params) {
        userService.loadUserInfoByServiceInstanceNumber(params);
    }

    /**
     * Load User Information by Phone number
     * @param params - {number, callback, fresh, basic}
     */
    loadUserInfoByPhoneNumber(params) {
        userService.loadUserInfoByPhoneNumber(params);
    }

    /**
     * Load User Information by User key
     * @param params - {userKey, callback, fresh, basic}
     */
    loadUserInfoByUserKey(params) {
        userService.loadUserInfoByUserKey(params);
    }

    /**
     * Load all users base information
     * @param params - {options, callback}
     */
    loadAllUsersBaseInfo(params) {
        userService.loadAllUsersBaseInfo(params);
    }

    /**
     * Load all active packages
     * @param params - {serviceInstance, callback}
     */
    loadActivePackages(params) {
        userService.loadActivePackages(params);
    }

    /**
     * Load customer data with packages
     * @param params - {serviceInstance, callback}
     */
    loadCustomerWithPackage(params) {
        userService.loadCustomerWithPackage(params);
    }

    /**
     * Clear user information from db cache
     * @param params - {accountNumber, callback}
     */
    clearUserInfo(params) {
        userService.clearUserInfo(params);
    }


    /**
     * To load all Bill Invoices
     * @param params - {monthDate, callback}
     */
    loadBillInvoices(params) {
        billingService.loadBillInvoices(params);
    }

    /**
     * To load all unpaid bill invoices
     * @param params - {callback}
     */
    loadUnpaidBillInvoices(params) {
        billingService.loadUnpaidBillInvoices(params);
    }

    /**
     * To load bill invoices by Ids
     * @param params - {invoiceIds, callback}
     */
    loadBillInvoicesByIds(params) {
        billingService.loadBillInvoicesByIds(params);
    }

    /**
     * To clear customer payments cache
     * @param params - {billingAccountNumber}
     */
    clearCustomerPaymentsCache(params) {
        billingService.clearCustomerPaymentsCache(params);
    }

    /**
     * Callback service for various inner services
     * @param params - {billingAccountNumber, callback, fresh, basic}
     */
    loadCustomerPaymentsInfo(params) {
        billingService.loadCustomerPaymentsInfo(params);
    }

    /**
     * Search bill by accout number, from and to date
     * @param params - {accountNumber, fromDate, toDate, callback}
     */
    searchBill(params) {
        billingService.searchBill(params);
    }

    /**
     * Search credit note by arguments
     * @param params - {args, callback}
     */
    searchCreditNote(params) {
        billingService.searchCreditNote(params);
    }

    /**
     * Add deposit to customer billing account by billing account number, deposit account and reason
     * @param params - {billingAccountNumber, depositAmount, reason, callback}
     */
    addDepositToCustomerBillingAccount(params) {
        billingService.addDepositToCustomerBillingAccount(params);
    }

    /**
     * Make debit payment data by makeDebitPaymentDataRequest
     * @param params - {accountNumber, transactionAmount, documentNumber, paymentDate, channelAlias, callback}
     */
    makeDebitPaymentDataRequest(params) {
        billingService.makeDebitPaymentDataRequest(params);
    }

    /**
     * Make advance payment by makeAdvancePaymentDataRequest object
     * @param params - {accountNumber, transactionAmount, paymentDate, channelAlias, description, callback}
     */
    makeAdvancePaymentDataRequest(params) {
        billingService.makeAdvancePaymentDataRequest(params)
    }

    /**
     * To search payments
     * @param params - {args, callback}
     */
    searchPayment(params) {
        billingService.searchPayment(params);
    }

    /**
     * To create Credit note
     * @param params - {args, callback}
     */
    createCreditNote(params) {
        billingService.createCreditNote(params);
    }

    /**
     * To create Inventory
     * @param params - {msisdn, donorCode, date, callback}
     */
    createInventory(params) {
        billingService.createInventory(params);
    }

    /**
     * Get inventory details by the arguments
     * @param params - {args, callback}
     */
    getInventoryDetail(params) {
        billingService.getInventoryDetail(params);
    }

    /**
     * Add inventory by effective date, inventory number, subtype, Ismanualinventory, packageid and service instance number
     * @param params - {effectDate, inventoryNumber, serviceInstanceNumber, callback}
     */
    addInventory(params) {
        billingService.addInventory(params);
    }

    /**
     * Get inventory list by the sevice instance number
     * @param params - {serviceInstanceNumber, callback}
     */
    listInventory(params) {
        billingService.listInventory(params);
    }

    /**
     * Unpair inventory by arguments
     * @param params - {args, callback}
     */
    unpairInventory(params) {
        billingService.unpairInventory(params);
    }

    /**
     * Repair inventory by arguments
     * @param params -{args, callback}
     */
    repairInventory(params) {
        billingService.repairInventory(params);
    }

    /**
     * repair inventory xml by arguments
     * @param params - {args, callback}
     */
    repairInventoryXml(params) {
        billingService.repairInventoryXml(params);
    }

    /**
     * Remove inventory by service instance number and inventory number
     * @param params - {serviceInstanceNumber, inventoryNumber, callback}
     */
    removeInventory(params) {
        billingService.removeInventory(params);
    }

    /**
     * To update inventory attributes
     * @param params - {msisdn, donorCode, date, callback}
     */
    updateInventoryAttributes(params) {
        billingService.updateInventoryAttributes(params);
    }

    /**
     * Get the inventory details by the inventory number
     * @param params - {inventoryNumber, callback}
     */
    searchInventory(params) {
        billingService.searchInventory(params);
    }

    /**
     * Get the list of inventories by the inventory status id, subtype id, owner id (default - 'operator')
     * @param params - {inventoryStatusId, inventorySubTypeId, callback}
     */
    searchInventoryList(params) {
        billingService.searchInventoryList(params);
    }

    /**
     * Do the inventory operation by the inventory number, operationAlias and reference number(Default  - N/A)
     * @param params -- {inventoryNumber, operationAlias, callback}
     */
    doInventoryStatusOperation(params) {
        billingService.doInventoryStatusOperation(params);
    }

    /**
     * repair inventory bundle
     * @param params - {args, callback}
     */
    repairInventoryRebundle(params) {
        billingService.repairInventoryRebundle(params);
    }

    /**
     * Mark inventory as damaged by inventory number and serviceInstanceNumber
     * @param params - {inventoryNumber, serviceInstanceNumber, callback}
     */
    markInventoryAsDamaged(params) {
        billingService.markInventoryAsDamaged(params);
    }

    /**
     * To load hierarchy
     * @param params - {callback}
     */
    loadHierarchy(params) {
        packageService.loadHierarchy(params);
    }

    /**
     * To load Packages
     * @param params - {teamId, callback}
     */
    loadPackages(params) {
        packageService.loadPackages(params);
    }

    /**
     * To load packages fro team API key
     * @param params - {teamApiKey, callback}
     */
    loadPackagesForTeamApiKey(params) {
        packageService.loadPackagesForTeamApiKey(params);
    }

    /**
     * Get all the product offer details
     * @param params - {callback}
     */
    getProductOfferDetails(params) {
        packageService.getProductOfferDetails(params);
    }

    /**
     * Get the specific product offer details by the  product Id
     * @param params - {args, callback}
     */
    getSpecificProdOfferDetails(params) {
        packageService.getSpecificProdOfferDetails(params);
    }

    /**
     * To modify plan attributes
     * @param params - {args, callback}
     */
    modifyPlanAttributes(params) {
        packageService.modifyPlanAttributes(params);
    }

    /**
     * Change service plan by account number, effective date, pacakage name, remarks and service instance number
     * @param params - {billingAccountNumber, effectiveDate, packageName, remarks, serviceInstanceNumber, callback}
     */
    changeServicePlan(params) {
        packageService.changeServicePlan(params);
    }

    /**
     * To subscribe add on package
     * @param params - {args, callback}
     */
    subscribeAddOnPackage(params) {
        packageService.subscribeAddOnPackage(params);
    }

    /**
     * To un-subscribe add on package
     * @param params - {args, callback}
     */
    unSubscribeAddOnPackage(params) {
        packageService.unSubscribeAddOnPackage(params);
    }

    /**
     * To get FnF member details
     * @param params - {args, callback}
     */
    getFnFMembersDetails(params) {
        packageService.getFnFMembersDetails(params);
    }

    /**
     * To add FnF members
     * @param params - {args, callback}
     */
    addFnFMembers(params) {
        packageService.addFnFMembers(params);
    }

    /**
     * To remove FnF members
     * @param params - {args, callback}
     */
    removeFnFMembers(params) {
        packageService.removeFnFMembers(params);
    }


    /**
     * To load Customer Usage
     * @param params - {serviceInstance, callback}
     */
    loadCustomerUsage(params) {
        usageService.loadCustomerUsage(params);
    }

    /**
     * Get the usage data and counter balance data  from DB call or view customer method in connector
     * @param params - {serviceInstanceNumber, callback, fresh, basic}
     */
    loadCurrentCustomerUsage(params) {
        usageService.loadCurrentCustomerUsage(params);
    }

    /**
     * Get remaining data for pacakge  by service Instance number
     * @param params - {number, packageId, callback}
     */
    loadRemainingDataForPackage(params) {
        usageService.loadRemainingDataForPackage(params);
    }

    /**
     * To view customer usage information
     * @param params - {args, callback}
     */
    viewCustomerUsageInformation(params) {
        usageService.viewCustomerUsageInformation(params);
    }


    /**
     * Set PortIn Number immediate
     * @param params - {serviceInstanceNumber, tempNumber, portInNumber, donor, startTime, externalRefId, callback, portInRecovery}
     */
    portInNumber(params) {
        portinService.portInNumber(params);
    }

    /**
     * To Cancel PortIn order
     * @param params - {serviceInstanceNumber, tempNumber, portInNumber, donor, externalRefId, callback, portInRecovery}
     */
    portInNumberImmediate(params) {
        portinService.portInNumberImmediate(params);
    }

    /**
     * Get PortIn Status by  PortIn Number
     * @param params - {portInNumber, status}
     */
    getPortInStatus(params) {
        portinService.getPortInStatus(params);
    }

    /**
     * Get Donor Code by Network Name
     * @param params - {donor}
     */
    getDonorCodeByNetworkName(params) {
        portinService.getDonorCodeByNetworkName(params);
    }

    /**
     * Load Portin Failure reason by portin Number and start_ts
     * @param params - {portInNumber, start_ts, callback}
     */
    loadPortInFailureReason(params) {
        portinService.loadPortInFailureReason(params);
    }

    /**
     * Load Portin Completion Status by PortIn Number
     * @param params - {portInNumber, callback}
     */
    loadPortInCompleationStatus(params) {
        portinService.loadPortInCompleationStatus(params);
    }

    /**
     * Deactive Portin Number
     * @param params - {serviceInstanceNumber, portInNumber, callback}
     */
    deactivatePortInNumber(params) {
        portinService.deactivatePortInNumber(params);
    }

    /**
     * To Cancel PortIn order
     * @param params - {MSISDN, reasonCode, callback}
     */
    cancelPortInOrder(params) {
        portinService.cancelPortInOrder(params);
    }

    //region TEST




    testBRM(params) {
        testService.testBRM(params);
    }

    //endregion


    //region - General

    /**
     * Init method to load initialization
     * @param params - {callback}
     */
    init(params) {
        commonError.middleware.notImplemented();
    }

    /**
     * To set API Key
     * @param params - {teamApiKey}
     */
    setAPIKey(params) {
        packageService.setAPIKey(params);
    }


    /**
     *
     * @param params - {bssList}
     */
    setDisabledBss(params){
        commonError.middleware.notImplemented();
    }

    /**
     *
     * @param params - {offline}
     */
    setOfflineMode(params){
        commonError.middleware.notImplemented();
    }

    /**
     *
     */
    getCountTimeouts(){
        commonError.middleware.notImplemented();
    }

    /**
     *
     */
    getCountRequests() {
        commonError.middleware.notImplemented();
    }

    /**
     *
     * @param bssList - ex:
     */
    getCountErrors() {
        commonError.middleware.notImplemented();
    }

    /**
     * @param params - {callback}
     */
    addStatsUpdateListener(params) {
        commonError.middleware.notImplemented();
    }

    /**
     *
     * @param params - {key}
     */
    removeStatsUpdateListener(params) {
        commonError.middleware.notImplemented();
    }

    /**
     *@params params - {callback}
     */
    loadBssMode(params) {
        commonError.middleware.notImplemented();
    }

    //endregion
}

const middleware = new MiddleWare();
module.exports = middleware;