/*globals module */
class MiddlewareError{
    constructor(){

    }

    /**
     * Fired if key is invalid
     * @param params - keyType, key, callback, fresh
     * @returns {Error}
     */
    invalidKeyForUserInfo(params){
        let {keyType} = params;
        return new Error(`Key is empty, keyType=${keyType}`);
    }

    notImplemented(){
        return new Error("Not Implemented in middleware");
    }
}

let middlewareError = new MiddlewareError();
module.exports = middlewareError;