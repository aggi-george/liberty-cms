/*global require, exports, module*/

const common = require(global.__lib + '/common');
const config = require(`${global.__base}/config`);

const LOG = config.EC_LOGSENABLED;

class MiddlewareLogger {
    constructor() {
        this.category = 'Middleware';
    }

    localCatch(reason){
        if (!LOG) return;
        common.log( this.category, JSON.stringify(reason));
    }

    updateProductHierarchy(updateProductHierarchyCategory, productId){
        if (!LOG) return;
        common.log(updateProductHierarchyCategory, productId);
    }

}

let middlewareLogger = new MiddlewareLogger();
module.exports = middlewareLogger;