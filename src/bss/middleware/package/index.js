/*globals require, module */

//This is related only to Pacakge

const Promise = require('bluebird');
const oraclemw = require('../../oracle/index');
let apiKey;

class Package {
    constructor() {

    }

    /**
     * To get default configuration
     */
    getDefaultConfig() {
       oraclemw.getDefaultConfig().then(result=>{
          return result;
        },console.error);
    }

    /**
     * To set API Key
     * @param teamApiKey
     */
    setAPIKey(params) {
       let{teamApiKey} = params;


    }

    /**
     * Init method to load initialization
     * @param params.- {callback}
     */
    init(params) {
        let {callback} = params;

        oraclemw.init().then(result=>{
            callback(null,result);
        },callback);
    }

    /**
     * To load hierarchy
     * @param params.- {callback}
     */
    loadHierarchy(params) {
        let {callback} = params;

        oraclemw.loadHierarchy().then(result=>{
            callback(null,result);
        },callback);
    }

    /**
     * To load Packages
     * @param params.- {teamId, callback}
     */
    loadPackages(params) {
        let {teamId, callback} = params;

        oraclemw.loadPackages({teamId}).then(result=>{
            callback(null,result);
        },callback);
    }

    /**
     * To load packages fro team API key
     * @param params.- {teamApiKey, callback}
     */
    loadPackagesForTeamApiKey(params) {
        let {teamApiKey, callback} = params;

        oraclemw.loadPackagesForTeamApiKey({teamApiKey}).then(result=>{
            callback(null,result);
        },callback);
    }

    /**
     * Get all the product offer details
     * @param params- {callback}
     */
    getProductOfferDetails(params){
        let {callback} = params;

        oraclemw.getProductOfferDetails().then(result => {
            callback(null, result);
       },callback);
    }

    /**
     * Get the specific product offer details by the  product Id
     * @param params- {args, callback}
     */
    getSpecificProdOfferDetails(params){
        let {args, callback} = params;

        oraclemw.getSpecificProdOfferDetails(args).then(result => {
            callback(null, result);
       },callback);
    }

    /**
     * To modify plan attributes
     * @param params.- args, callback
     */
    modifyPlanAttributes(params) {
        let {args, callback} = params;

        oraclemw.modifyPlanAttributes(args).then(result=>{
            callback(null,result);
        },callback);
    }

    /**
     * Change service plan by account number, effective date, pacakage name, remarks and service instance number
     * @param params.- {billingAccountNumber, effectiveDate, packageName, remarks, serviceInstanceNumber, callback}
     */
    changeServicePlan(params){

        let {billingAccountNumber, effectiveDate, packageName, remarks, serviceInstanceNumber, callback} = params;

        oraclemw.changeServicePlan({billingAccountNumber, effectiveDate, packageName, remarks, serviceInstanceNumber}).then(result => {
             callback(null, result);
        },callback);
    }

    /**
     * To subscribe add on package
     * @param params.- {teamId, callback}
     */
    subscribeAddOnPackage(params){
        let {args, callback} = params;

        oraclemw.subscribeAddOnPackage(args).then(result => {
            callback(null, result);
       },callback);
    }

    /**
     * To un-subscribe add on package
     * @param params.- {teamId, callback}
     */
    unSubscribeAddOnPackage(params){
        let {args, callback} = params;

        oraclemw.unSubscribeAddOnPackage(args).then(result => {
            callback(null, result);
       },callback);
    }

    /**
     * To get FnF member details
     * @param params- args, callback
     */
    getFnFMembersDetails(params){

        let {args, callback} = params;

        oraclemw.getFnFMembersDetails(args).then(result => {
            callback(null, result);
       },callback);
    }

    /**
     * To add FnF members
     * @param params.- args, callback
     */
    addFnFMembers(params){
        let {args, callback} = params;

        oraclemw.addFnFMembers(args).then(result => {
            callback(null, result);
       },callback);
    }

    /**
     * To remove FnF members
     * @param params.- args, callback
     */
    removeFnFMembers(params) {
        let {args, callback} = params;

        oraclemw.removeFnFMembers(args).then(result => {
            callback(null, result);
       },callback);
    }


}

let pkg = new Package();
module.exports = pkg;