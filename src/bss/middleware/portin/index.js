/*globals require, module */

//This is related only to PortIn

const Promise = require('bluebird');
const oraclemw = require('../../oracle/index');

class Portin{
    constructor(){

    }

    /**
     * Set PortIn Number immediate
     * @param params.- {serviceInstanceNumber, tempNumber, portInNumber, donor, startTime, externalRefId,
                 callback, portInRecovery}
     */
    portInNumber(params){

        let{serviceInstanceNumber, tempNumber, portInNumber, donor, startTime, externalRefId,
            callback, portInRecovery}=params;

        oraclemw.portInNumber({serviceInstanceNumber, tempNumber, portInNumber, donor, startTime, externalRefId,
            portInRecovery}).then(result=>{
            callback(null,result);
        },callback);


    }

    /**
     * To Cancel PortIn order
     * @param params.- {serviceInstanceNumber, tempNumber, portInNumber,
                          donor, externalRefId, callback, portInRecovery}
     */
    portInNumberImmediate(params){
      let{serviceInstanceNumber, tempNumber, portInNumber,
          donor, externalRefId, callback, portInRecovery}=params;

        oraclemw.portInNumberImmediate({serviceInstanceNumber, tempNumber, portInNumber,
          donor, externalRefId, portInRecovery}).then(result=>{
            callback(null,result);
        },callback);
    }

    /**
     * Get PortIn Status by  PortIn Number
     * @param params.- {portInNumber, status}
     */
    getPortInStatus(params){

        let {portInNumber, status} = params;

        oraclemw.getPortInStatus({portInNumber, status}).then(result=>{
            callback(null,result);
        },callback);

    }

    /**
     * Get Donor Code by Network Name
     * @param params.- {donor}
     */
    getDonorCodeByNetworkName(params){

        let{donor} = params;

        oraclemw.getDonorCodeByNetworkName({donor}).then(result=>{
            callback(null,result);
        },callback);
    }

    /**
     * Load Portin Failure reason by portin Number and start_ts
     * @param params.- {portInNumber, start_ts, callback}
     */
    loadPortInFailureReason(params){

        let{portInNumber, start_ts, callback} = params;

        oraclemw.loadPortInFailureReason({portInNumber, start_ts}).then(result=>{
            callback(null,result);
        },callback);
    }

    /**
     * Load Portin Completion Status by PortIn Number
     * @param params.- {portInNumber, callback}
     */
    loadPortInCompleationStatus(params){

        let{portInNumber, callback} = params;

        oraclemw.loadPortInCompleationStatus({portInNumber}).then(result=>{
            callback(null,result);
        },callback);

    }

    /**
     * Deactive Portin Number
     * @param params.- {serviceInstanceNumber, portInNumber, callback}
     */
    deactivatePortInNumber(params){

        let{serviceInstanceNumber, portInNumber, callback}=params;

        oraclemw.deactivatePortInNumber({serviceInstanceNumber, portInNumber}).then(result=>{
           callback(null,result);
       },callback);
    }

    /**
     * To Cancel PortIn order
     * @param params- {MSISDN, reasonCode, callback}
     */
    cancelPortInOrder(params){

        let {MSISDN, reasonCode, callback}=params;

        oraclemw.cancelPortInOrder({MSISDN, reasonCode}).then(result=>{
          callback(null,result);
      },callback);

    }

}

let portin = new Portin();
module.exports = portin;