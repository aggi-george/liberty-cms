/*globals require, module */

//This is related only to User

const Promise = require('bluebird');
const oraclemw = require('../../oracle/index');

class Test {
    constructor() {

    }


    /**
     * Test
     * @param params.- {callback}
     */
    testBRM(params) {
        let {callback} = params;

        //from here we hop to the actual oracle mw

        oraclemw.testBRM().then(result => {
            callback(null, result);
        }, callback);
    }

}

let test = new Test();
module.exports = test;