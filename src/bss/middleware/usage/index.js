/*globals require, module */

//This is related only to Usage

const Promise = require('bluebird');
const oraclemw = require('../../oracle/index');

class Usage {
    constructor() {

    }

    /**
     * To load Customer Usage
     * @param params.- {serviceInstance, callback}
     */
    loadCustomerUsage(params) {
        let {serviceInstance, callback} = params;

        oraclemw.loadCustomerUsage({serviceInstance}).then(result => {
            callback(null, result);
        }, callback);
    }

    /**
     * Get the usage data and counter balance data  from DB call or view customer method in connector
     * @param params.- {serviceInstanceNumber, callback, fresh, basic}
     */
    loadCurrentCustomerUsage(params) {
        let {serviceInstanceNumber, callback, fresh, basic} = params;

        oraclemw.loadCurrentCustomerUsage({serviceInstanceNumber, fresh, basic}).then(result => {
            callback(null, result);
        }, callback);
    }

    /**
     * Get remaining data for pacakge  by service Instance number
     * @param params.- {number, packageId, callback}
     */
    loadRemainingDataForPackage(params) {

        let {number, packageId, callback} = params;

        oraclemw.loadRemainingDataForPackage({number, packageId}).then(result => {
            callback(null, result);
        }, callback);
    }

    /**
     * To view customer usage information
     * @param params.- {args, callback}
     */
    viewCustomerUsageInformation(params) {

        let {args, callback} = params;
        oraclemw.viewCustomerUsageInformation({args}).then(result => {
            callback(null, result);
        }, callback);
    }

}

let usage = new Usage();
module.exports = usage;