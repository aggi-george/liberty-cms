/*globals require, module */

//This is related only to User

const Promise = require('bluebird');
const oraclemw = require('../../oracle/index');

class User {
    constructor() {

    }

    /**
     * Create Account
     * @param params.- {args, callback}
     */
    createAccount(params) {
        let {args, callback} = params;

        oraclemw.createAccount({args}).then(result => {
            callback(null, result);
        }, callback);
    }

    /**
     * search account by mobile or MSISDN Number, account number, service username and account name
     * @param params.- {number, accountNumber, accountName, serviceUserName, callback}
     */
    searchAccount(params) {
        let {number, accountNumber, accountName, serviceUserName, callback} = params;

        oraclemw.searchAccount({number, accountNumber, accountName, serviceUserName}).then(result => {
            callback(null, result);
        }, callback);
    }

    /**
     * search service instance by mobile or MSISDN Number, account number and account name
     * @param params.- {number, accountNumber, accountName, callback}
     */
    searchServiceInstance(params) {

        let {number, accountNumber, accountName, callback} = params;

        oraclemw.searchServiceInstance({number, accountNumber, accountName}).then(result => {
            callback(null, result);
        }, callback);

    }

    /**
     * To Views customer
     * @param params.- {serviceInstanceNumber, fromDate, callback}
     */
    viewCustomer(params) {
        let {serviceInstanceNumber, fromDate, callback} = params;

        //from here we hop to the actual oracle mw

        oraclemw.viewCustomer({serviceInstanceNumber, fromDate}).then(result => {
            callback(null, result);
        }, callback);
    }

    /**
     * Get account statement by account number, from and to date
     * @param params.- {accountNumber, fromDate, toDate, callback}
     */
    getAccountStatement(params) {

        let {accountNumber, fromDate, toDate, callback} = params;

        oraclemw.getAccountStatement({accountNumber, fromDate, toDate}).then(result => {
            callback(null, result);
        }, callback);
    }

    /**
     * To get Customer Hierarchy Details by account number
     * @param params.- {accountNumber, callback}
     */
    getCustomerHierarchyDetails(params) {
        let {accountNumber, callback} = params;

        //from here we hop to the actual oracle mw

        oraclemw.getCustomerHierarchyDetails({accountNumber}).then(result => {
            callback(null, result);
        }, callback).catch(callback);
    }

    /**
     * Get the business hierarchy details
     * @param params- {callback}
     */
    getBusinessHierarchyDetail(params) {

        let {callback} = params;

        oraclemw.getBusinessHierarchyDetail().then(result => {
            callback(null, result);
        }, callback);
    }

    /**
     *
     * @param params.- {billingAccountNumber, callback}
     */
    getBillingAccountDetail(params) {

        let {billingAccountNumber, callback} = params;

        oraclemw.getBillingAccountDetail({billingAccountNumber}).then(result => {
            callback(null, result);
        }, callback);
    }

    /**
     * Get ID Type  by code
     * @param params.- {code}
     */
    getIDTypeByCode(params) {
        let {code} = params;

        //from here we hop to the actual oracle mw

        oraclemw.getIDTypeByCode({code}).then(result => {
            callback(null, result);
        }, callback);
    }

    /**
     * To change service instance status
     * @param params.- {args, callback}
     */
    changeServiceInstanceStatus() {


        let {args, callback} = params;

        oraclemw.changeServiceInstanceStatus({args}).then(result => {
            callback(null, result);
        }, callback);
    }

    /**
     * For MNP Notification
     * @param params.- {MSISDN, callback}
     */
    mnpNotification(params) {

        let {MSISDN, callback} = params;

        oraclemw.mnpNotification({MSISDN}).then(result => {
            callback(null, result);
        }, callback);
    }

    /**
     * For Service provisioning notification
     * @param params.- {externalRefId, inventoryNumber, callback}
     */
    serviceProvisioningNotification(params) {
        let {externalRefId, inventoryNumber, callback} = params;

        oraclemw.serviceProvisioningNotification({externalRefId, inventoryNumber}).then(result => {
            callback(null, result);
        }, callback);
    }

     /**
     * Update User Information
     * @param params.- {accountNumber, callback, userKey}
     */
    updateUserInfo(params) {
        let {accountNumber, callback, userKey} = params;

        //from here we hop to the actual oracle mw

        oraclemw.updateUserInfo({accountNumber, userKey}).then(result => {
            callback(null, result);
        }, callback);
    }

    /**
     * Update customer Id
     * @param params.- {accountNumber, type, id, callback}
     */
    updateCustomerID(params) {
        let {accountNumber, type, id, callback} = params;

        //from here we hop to the actual oracle mw

        oraclemw.updateCustomerID({accountNumber, type, id}).then(result => {
            callback(null, result);
        }, callback);
    }

    /**
     * Get billing account details by billing account number
     * Update customer account data by arguments
     * @param params.- {args, callback}
     */
    updateCustomerAccountData(params) {
        let {args, callback} = params;

        oraclemw.updateCustomerAccountData({args}).then(result => {
            callback(null, result);
        }, callback);

    }

    /**
     * To Update billing account data
     * @param params.- {args, callback}
     */
    updateBillingAccountData(params) {
        let {args, callback} = params;

        oraclemw.updateBillingAccountData({args}).then(result => {
            callback(null, result);
        }, callback);

    }

    /**
     *  To update billing account
     * @param params.- {args, callback}
     */
    updateBillingAccount(params) {
        let {args, callback} = params;

        oraclemw.updateBillingAccount({args}).then(result => {
            callback(null, result);
        }, callback);
    }

    /**
     * To Update service account data
     * @param params.- {args, callback}
     */
    updateServiceAccountData(params) {

        let {args, callback} = params;

        oraclemw.updateServiceAccountData({args}).then(result => {
            callback(null, result);
        }, callback);
    }

    /**
     * To update customer bucket
     * @param params.- { args, callback}
     */
    updateCustomerBucket(params) {
        let {args, callback} = params;

        //from here we hop to the actual oracle mw

        oraclemw.updateCustomerBucket({args}).then(result => {
            callback(null, result);
        }, callback);
    }

    /**
     * Update account by order reference number
     * @param params.- {accountNumber, orn, callback}
     */
    updateOrderReferenceNumber(params) {
        let {accountNumber, orn, callback} = params;

        //from here we hop to the actual oracle mw

        oraclemw.updateOrderReferenceNumber({accountNumber, orn}).then(result => {
            callback(null, result);
        }, callback);
    }

    /**
     * Update activation date
     * @param params.- {accountNumber, datestring, callback}
     */
    updateActivationDate(params) {
        let {accountNumber, datestring, callback} = params;

        //from here we hop to the actual oracle mw

        oraclemw.updateActivationDate({accountNumber, datestring}).then(result => {
            callback(null, result);
        }, callback);
    }

    /**
     * Update customer Date of Birth
     * @param params.- {accountNumber, birthday, callback}
     */
    updateCustomerDOB(params) {
        let {accountNumber, birthday, callback} = params;

        //from here we hop to the actual oracle mw

        oraclemw.updateCustomerDOB({accountNumber, birthday}).then(result => {
            callback(null, result);
        }, callback);
    }

    /**
     * Update all details (Billing, Servce and account) of the customer by account nubmer
     * @param parameters.- {accountNumber, params, callback, skipAccount, skipBilling, skipService}
     */
    updateAllCustomerAccountsDetails(parameters) {
        let {accountNumber, params, callback, skipAccount, skipBilling, skipService} = parameters;

        //from here we hop to the actual oracle mw

        oraclemw.updateAllCustomerAccountsDetails({
            accountNumber,
            params,
            skipAccount,
            skipBilling,
            skipService
        }).then(result => {
            callback(null, result);
        }, callback);
    }

    /**
     * Update customer order by account numer and reference number
     * @param params.- {accountNumber, orderReferenceNumber, callback}
     */
    updateCustomerOrderReferenceNumber(params) {
        let {accountNumber, orderReferenceNumber, callback} = params;

        //from here we hop to the actual oracle mw

        oraclemw.updateCustomerOrderReferenceNumber({accountNumber, orderReferenceNumber}).then(result => {
            callback(null, result);
        }, callback);
    }

    /**
     * Load user information
     * @param params.- {keyType, key, callback, fresh}
     */
    loadUserInfo(params) {
        let {keyType, key, callback, fresh} = params;

        //from here we hop to the actual oracle mw

        oraclemw.loadUserInfo({keyType, key, fresh}).then(result => {
            callback(null, result);
        }, callback);
    }

    /**
     * Update the user info by internal id
     * @param params.- {id, callback, fresh, basic}
     */
    loadUserInfoByInternalId(params) {
        let {id, callback, fresh, basic} = params;

        //from here we hop to the actual oracle mw

        oraclemw.loadUserInfoByInternalId({id, fresh, basic}).then(result => {
            callback(null, result);
        }, callback);
    }

    /**
     * Load User information by CAN
     * @param params.- {can, callback, fresh, basic}
     */
    loadUserInfoByCustomerAccountNumber(params) {
        let {can, callback, fresh, basic} = params;

        //from here we hop to the actual oracle mw

        oraclemw.loadUserInfoByCustomerAccountNumber({can, fresh, basic}).then(result => {
            callback(null, result);
        }, callback);
    }

    /**
     * Load User information by BAN
     * @param params.- {ban, callback, fresh, basic}
     */
    loadUserInfoByBillingAccountNumber(params) {
        let {ban, callback, fresh, basic} = params;

        //from here we hop to the actual oracle mw

        oraclemw.loadUserInfoByBillingAccountNumber({ban, fresh, basic}).then(result => {
            callback(null, result);
        }, callback);
    }

    /**
     * Load User information by Service Account Number
     * @param params.- {san, callback, fresh, basic}
     */
    loadUserInfoByServiceAccountNumber(params) {
        let {san, callback, fresh, basic} = params;

        //from here we hop to the actual oracle mw

        oraclemw.loadUserInfoByServiceAccountNumber({san, fresh, basic}).then(result => {
            callback(null, result);
        }, callback);
    }

    /**
     * Load User Information by SIN
     * @param params.- {sin, callback, fresh, basic}
     */
    loadUserInfoByServiceInstanceNumber(params) {
        let {sin, callback, fresh, basic} = params;

        //from here we hop to the actual oracle mw

        oraclemw.loadUserInfoByServiceInstanceNumber({sin, fresh, basic}).then(result => {
            callback(null, result);
        }, callback);
    }

    /**
     * Load User Information by Phone number
     * @param params.- {number, callback, fresh, basic}
     */
    loadUserInfoByPhoneNumber(params) {
        let {number, callback, fresh, basic} = params;

        //from here we hop to the actual oracle mw

        oraclemw.loadUserInfoByPhoneNumber({number, fresh, basic}).then(result => {
            callback(null, result);
        }, callback);
    }

    /**
     * Load User Information by User key
     * @param params.- {userKey, callback, fresh, basic}
     */
    loadUserInfoByUserKey(params) {
        let {userKey, callback, fresh, basic} = params;

        //from here we hop to the actual oracle mw

        oraclemw.loadUserInfoByUserKey({userKey, fresh, basic}).then(result => {
            callback(null, result);
        }, callback);
    }

    /**
     * Load all users base information
     * @param params.- {options, callback}
     */
    loadAllUsersBaseInfo(params) {
        let {options, callback} = params;

        //from here we hop to the actual oracle mw

        oraclemw.loadAllUsersBaseInfo({options}).then(result => {
            callback(null, result);
        }, callback);
    }

    /**
     * Load all active packages
     * @param params.- {serviceInstance, callback}
     */
    loadActivePackages(params) {
        let {serviceInstance, callback} = params;

        //from here we hop to the actual oracle mw

        oraclemw.loadActivePackages({serviceInstance}).then(result => {
            callback(null, result);
        }, callback);
    }

    /**
     * Load customer data with packages
     * @param params.- {serviceInstance, callback}
     */
    loadCustomerWithPackage(params) {
        let {serviceInstance, callback} = params;

        //from here we hop to the actual oracle mw

        oraclemw.loadCustomerWithPackage({serviceInstance}).then(result => {
            callback(null, result);
        }, callback);
    }

    /**
     * Clear user information from db cache
     * @param params.- {accountNumber, callback}
     */
    clearUserInfo(params) {
        let {accountNumber, callback} = params;

        //from here we hop to the actual oracle mw

        oraclemw.clearUserInfo({accountNumber}).then(result => {
            callback(null, result);
        }, callback);
    }

}

let user = new User();
module.exports = user;