/*globals module,require, Buffer */
const Promise = require('bluebird');
const bssConfig = require('../config');
const qConnector = require('./queue/qconnector/index');
const soap = require('./soap/index');
const soapConnector = require('./soap/soapconnector/index');

/**
 * This is the entry point for Oracle MW. from here it is decided to go with soapconnector, qconnector or any other connector.
 */

class OracleMW {

    constructor() {

    }

    get gotoSoap(){
        return bssConfig.gotoSoap;
    }

    //region test

    testBRM() {
        //this is returning a promise that fires the request to the queue.
        //return qConnector.request("createAccount", arg);
        return soapConnector.testBRM();
    }


    //endregion


    //region - User, Account, Notifications

    /**
     * Create Account
     * @param params - {args}
     * @returns {Promise}
     */
    createAccount(params) {
        let{args} = params;
        //this is returning a promise that fires the request to the queue.
        if(this.gotoSoap){
            return soap.createAccount(args);
        }
        return qConnector.request("createAccount", args);
    }

    /**
     * search account by mobile or MSISDN Number, account number, service username and account name
     * @param params - {number, accountNumber, accountName, serviceUserName}
     * @returns {Promise}
     */
    searchAccount(params) {
        //this is returning a promise that fires the request to the queue.
        return qConnector.request("searchAccount", params);
    }

    /**
     * search service instance by mobile or MSISDN Number, account number and account name
     * @param params - {number, accountNumber, accountName}
     * @returns {Promise}
     */
    searchServiceInstance(params) {
        //this is returning a promise that fires the request to the queue.
        return qConnector.request("searchServiceInstance", params);

    }

    /**
     * To Views customer
     * @param params - {serviceInstanceNumber, fromDate}
     * @returns {Promise}
     */
    viewCustomer(params) {
        //this is returning a promise that fires the request to the queue.
        return qConnector.request("viewCustomer", params);
    }

    /**
     * Get account statement by account number, from and to date
     * @param params - {accountNumber, fromDate, toDate}
     * @returns {Promise}
     */
    getAccountStatement(params) {
        //this is returning a promise that fires the request to the queue.
        return qConnector.request("getAccountStatement", params);
    }

    /**
     * To get Customer Hierarchy Details by account number
     * @param params - {accountNumber}
     * @returns {Promise}
     */
    getCustomerHierarchyDetails(params) {
        //this is returning a promise that fires the request to the queue.
        return qConnector.request("getCustomerHierarchyDetails", params);

        //this is returning a promise that fires the request to the soapConnector.
        /* return new Promise((resolve,reject)=>{
             let callback = this.getPConnectorCallback(resolve,reject);
             let {accountNumber} = args;
             return soapConnector.getCustomerHierarchyDetails(accountNumber,callback);
         })*/
    }

    /**
     * Get the business hierarchy details
     * @returns {Promise}
     */
    getBusinessHierarchyDetail() {
        //this is returning a promise that fires the request to the queue.
        return qConnector.request("getBusinessHierarchyDetail", {});
    }

    /**
     * Get Account Details
     * @param params - {billingAccountNumber}
     * @returns {Promise}
     */
    getBillingAccountDetail(params) {
        //this is returning a promise that fires the request to the queue.
        return qConnector.request("getBillingAccountDetail", params);
    }

    /**
     * Get ID Type  by code
     * @param params - {code}
     * @returns {Promise}
     */
    getIDTypeByCode(params) {
        //this is returning a promise that fires the request to the queue.
        return qConnector.request("getIDTypeByCode", params);
    }

    /**
     * To change service instance status
     * @param params - {args}
     * @returns {Promise}
     */
    changeServiceInstanceStatus(params) {
        let {args}=params;
        //this is returning a promise that fires the request to the queue.
        return qConnector.request("changeServiceInstanceStatus", args);
    }

    /**
     * For MNP Notification
     * @param params - {MSISDN}
     * @returns {Promise}
     */
    mnpNotification(params) {
        //this is returning a promise that fires the request to the queue.
        return qConnector.request("mnpNotification", params);
    }

    /**
     * For Service provisioning notification
     * @param params - {externalRefId, inventoryNumber}
     * @returns {Promise}
     */
    serviceProvisioningNotification(params) {
        //this is returning a promise that fires the request to the queue.
        return qConnector.request("serviceProvisioningNotification", params);
    }

    /**
     * Update User Information
     * @param params - {accountNumber, userKey}
     * @returns {Promise}
     */
    updateUserInfo(params) {
        //this is returning a promise that fires the request to the queue.
        return qConnector.request("updateUserInfo", params);
    }

    /**
     * Update customer Id
     * @param params - {accountNumber, type, id}
     * @returns {Promise}
     */
    updateCustomerID(params) {
        //this is returning a promise that fires the request to the queue.
        return qConnector.request("updateCustomerID", params);
    }

    /**
     * Update customer account data by params
     * @param params - {args}
     * @returns {Promise}
     */
    updateCustomerAccountData(params) {
        let {args} = params;
        //this is returning a promise that fires the request to the queue.
        return qConnector.request("updateCustomerAccountData", args);

    }

    /**
     * To Update billing account data
     * @param params - {args}
     * @returns {Promise}
     */
    updateBillingAccountData(params) {
        let {args}=params;
        //this is returning a promise that fires the request to the queue.
        return qConnector.request("updateBillingAccountData", args);
    }

    /**
     *  To update billing account
     * @param params - {args}
     * @returns {Promise}
     */
    updateBillingAccount(params) {
        let {args}=params;

        //this is returning a promise that fires the request to the queue.
        return qConnector.request("updateBillingAccount", args);
    }

    /**
     *  To update billing account
     * @param arguments - {args}
     * @returns {Promise}
     */
    updateBillingAccount_ServiceOperation(params) {

        let {args}=params;
        //this is returning a promise that fires the request to the queue.
        return qConnector.request("updateBillingAccount_ServiceOperation", args);
    }

    /**
     * To Update service account
     * @param params - {args}
     * @returns {Promise}
     */
    updateServiceAccount(params) {

        let {args}=params;
        //this is returning a promise that fires the request to the queue.
        return qConnector.request("updateServiceAccount", args);
    }

    /**
     * To Update service account data
     * @param params - {args}
     * @returns {Promise}
     */
    updateServiceAccountData(params) {
        let {args}=params;

        //this is returning a promise that fires the request to the queue.
        return qConnector.request("updateServiceAccountData", args);
    }

    /**
     * To update customer bucket
     * @param params - {args}
     * @returns {Promise}
     */
    updateCustomerBucket(params) {
        let {args}=params;
        //this is returning a promise that fires the request to the queue.
        return qConnector.request("updateCustomerBucket", args);
    }

    /**
     * Update account by order reference number
     * @param params - {accountNumber, orn}
     * @returns {Promise}
     */
    updateOrderReferenceNumber(params) {
        //this is returning a promise that fires the request to the queue.
        return qConnector.request("updateOrderReferenceNumber", params);
    }

    /**
     * Update activation date
     * @param params - {accountNumber, datestring}
     */
    updateActivationDate(params) {
        //this is returning a promise that fires the request to the queue.
        return qConnector.request("updateActivationDate", params);
    }

    /**
     * Update customer Date of Birth
     * @param params - {accountNumber, birthday}
     * @returns {Promise}
     */
    updateCustomerDOB(params) {
        //this is returning a promise that fires the request to the queue.
        return qConnector.request("updateCustomerDOB", params);
    }

    /**
     * Update all details (Billing, Servce and account) of the customer by account nubmer
     * @param params - {accountNumber, params, skipAccount, skipBilling, skipService}
     * @returns {Promise}
     */
    updateAllCustomerAccountsDetails(params) {
        //this is returning a promise that fires the request to the queue.
        return qConnector.request("updateAllCustomerAccountsDetails", params);
    }

    /**
     * Update customer order by account numer and reference number
     * @param params - {accountNumber, orderReferenceNumber}
     * @returns {Promise}
     */
    updateCustomerOrderReferenceNumber(params) {
        //this is returning a promise that fires the request to the queue.
        return qConnector.request("updateCustomerOrderReferenceNumber", params);
    }

    /**
     * Load user information
     * @param params - {keyType, key, fresh}
     * @returns {Promise}
     */
    loadUserInfo(params) {
        //this is returning a promise that fires the request to the queue.
        return qConnector.request("loadUserInfo", params);
    }

    /**
     * Update the user info by internal id
     * @param params - {id, fresh, basic}
     * @returns {Promise}
     */
    loadUserInfoByInternalId(params) {
        //this is returning a promise that fires the request to the queue.
        return qConnector.request("loadUserInfoByInternalId", params);
    }

    /**
     * Load User information by CAN
     * @param params - {can, fresh, basic}
     * @returns {Promise}
     */
    loadUserInfoByCustomerAccountNumber(params) {
        //this is returning a promise that fires the request to the queue.
        return qConnector.request("loadUserInfoByCustomerAccountNumber", params);
    }

    /**
     * Load User information by BAN
     * @param params - {ban, fresh, basic}
     * @returns {Promise}
     */
    loadUserInfoByBillingAccountNumber(params) {
        //this is returning a promise that fires the request to the queue.
        return qConnector.request("loadUserInfoByBillingAccountNumber", params);
    }

    /**
     * Load User information by Service Account Number
     * @param params - {san, fresh, basic}
     * @returns {Promise}
     */
    loadUserInfoByServiceAccountNumber(params) {
        //this is returning a promise that fires the request to the queue.
        return qConnector.request("loadUserInfoByServiceAccountNumber", params);
    }

    /**
     * Load User Information by SIN
     * @param params - {sin, fresh, basic}
     * @returns {Promise}
     */
    loadUserInfoByServiceInstanceNumber(params) {
        //this is returning a promise that fires the request to the queue.
        return qConnector.request("loadUserInfoByServiceInstanceNumber", params);
    }

    /**
     * Load User Information by Phone number
     * @param params - {number, fresh, basic}
     * @returns {Promise}
     */
    loadUserInfoByPhoneNumber(params) {
        //this is returning a promise that fires the request to the queue.
        return qConnector.request("loadUserInfoByPhoneNumber", params);
    }

    /**
     * Load User Information by User key
     * @param params - {userKey, fresh, basic}
     * @returns {Promise}
     */
    loadUserInfoByUserKey(params) {
        //this is returning a promise that fires the request to the queue.
        return qConnector.request("loadUserInfoByUserKey", params);
    }

    /**
     * Load all users base information
     * @param params - {options}
     * @returns {Promise}
     */
    loadAllUsersBaseInfo(args) {
        //this is returning a promise that fires the request to the queue.
        return qConnector.request("loadAllUsersBaseInfo", params);
    }

    /**
     * Load all active packages
     * @param params - {serviceInstance}
     * @returns {Promise}
     */
    loadActivePackages(params) {
        //this is returning a promise that fires the request to the queue.
        return qConnector.request("loadActivePackages", params);
    }

    /**
     * Load customer data with packages
     * @param params - {serviceInstance}
     * @returns {Promise}
     */
    loadCustomerWithPackage(params) {
        //this is returning a promise that fires the request to the queue.
        return qConnector.request("loadCustomerWithPackage", params);
    }

    /**
     * Clear user information from db cache
     * @param params - {accountNumber}
     * @returns {Promise}
     */
    clearUserInfo(params) {
        //this is returning a promise that fires the request to the queue.
        return qConnector.request("clearUserInfo", params);
    }


    //endregion

    //region - Billing, Inventory

    /**
     * To load all Bill Invoices
     * @params - {monthDate}
     * @returns {Promise}
     */
    loadBillInvoices(params){
        return qConnector.request("loadBillInvoices", params);
    }

    /**
     * To load all unpaid bill invoices
     * @returns {Promise}
     */
    loadUnpaidBillInvoices(){
        return qConnector.request("loadUnpaidBillInvoices", {});

    }

    /**
     * To load bill invoices by Ids
     * @params - {invoiceIds,}
     * @returns {Promise}
     */
    loadBillInvoicesByIds (params){

        return qConnector.request("loadBillInvoicesByIds", params);
    }

    /**
     * To clear customer payments cache
     * @params - {billingAccountNumber}
     * @returns {Promise}
     */
    clearCustomerPaymentsCache (params){
        return qConnector.request("clearCustomerPaymentsCache", params);
    }

    /**
     * Callback service for various inner services
     * @params - {billingAccountNumber, fresh, basic}
     * @returns {Promise}
     */
    loadCustomerPaymentsInfo(params){
        return qConnector.request("loadCustomerPaymentsInfo", params);
    }

    /**
     * Search bill by accout number, from and to date
     * @params - {accountNumber, fromDate, toDate}
     * @returns {Promise}
     */
    searchBill(params){
        return qConnector.request("searchBill", params);
    }

    /**
     * Add deposit to customer billing account by billing account number, deposit account and reason
     * @params - {billingAccountNumber, depositAmount, reason}
     * @returns {Promise}
     */
    addDepositToCustomerBillingAccount(params){
        return qConnector.request("addDepositToCustomerBillingAccount", params);
    }

    /**
     * Make debit payment data by makeDebitPaymentDataRequest
     * @params - {accountNumber, transactionAmount, documentNumber, paymentDate,
     * channelAlias}
     * @returns {Promise}
     */
    makeDebitPaymentDataRequest(params){
        return qConnector.request("makeDebitPaymentDataRequest", params);
    }

    /**
     * Make advance payment by makeAdvancePaymentDataRequest object
     * @params - {accountNumber, transactionAmount, paymentDate, channelAlias, description}
     * @returns {Promise}
     */
    makeAdvancePaymentDataRequest(params){
        return qConnector.request("makeAdvancePaymentDataRequest", params);
    }

    /**
     * To search payments
     * @params - {args}
     * @returns {Promise}
     */
    searchPayment(params){
        let {args}=params;
        return qConnector.request("searchPayment", args);
    }

    /**
     * Search credit note by params
     * @params - {args}
     * @returns {Promise}
     */
    searchCreditNote(params){
        let {args}=params;
        return qConnector.request("searchCreditNote", args);
    }

    /**
     * To create Credit note
     * @params - {args}
     * @returns {Promise}
     */
    createCreditNote(params){
        let {args}=params;
        return qConnector.request("createCreditNote", args);
    }

    /**
     * To create Inventory
     * @param args.- {msisdn, donorCode, date}
     * @returns {Promise}
     */
    createInventory(params){
        return qConnector.request("createInventory", params);
    }

    /**
     * Get inventory details by the params
     * @args - {args}
     * @returns {Promise}
     */
    getInventoryDetail(params){
        let {args}=params;
        return qConnector.request("getInventoryDetail", args);
    }

    /**
     * Add inventory by effective date, inventory number, subtype, Ismanualinventory, packageid and service instance number
     * @params - {effectDate, inventoryNumber, serviceInstanceNumber}
     * @returns {Promise}
     */

  addInventory(params){
        return qConnector.request("addInventory", params);
    }

    /**
     * Get inventory list by the sevice instance number
     * @params - {serviceInstanceNumber}
     * @returns {Promise}
     */
    listInventory(params){
        return qConnector.request("listInventory", params);
    }

    /**
     * Unpair inventory by params
     * @params - {args}
     * @returns {Promise}
     */
    unpairInventory(params){
        let {args}=params;
        return qConnector.request("unpairInventory", args);
    }

    /**
     * Repair inventory by params
     * @params - {args}
     * @returns {Promise}
     */
    repairInventory(params){
        let {args}=params;
        return qConnector.request("repairInventory", args);
    }

    /**
     * repair inventory xml by params
     * @params - {args}
     * @returns {Promise}
     */
    repairInventoryXml(params){
        let {args}=params;
        return qConnector.request("repairInventoryXml", args);
    }

    /**
     * Remove inventory by service instance number and inventory number
     * @params - {serviceInstanceNumber, inventoryNumber}
     * @returns {Promise}
     */
    removeInventory(params){
        return qConnector.request("removeInventory", params);
    }

    /**
     * To update inventory attributes
     * @params - {msisdn, donorCode, date}
     * @returns {Promise}
     */
    updateInventoryAttributes(params){
        return qConnector.request("updateInventoryAttributes", params);
    }

    /**
     * Get the inventory details by the inventory number
     * @params - {inventoryNumber}
     * @returns {Promise}
     */
    searchInventory(params){
        return qConnector.request("searchInventory", params);
    }

    /**
     * Get the list of inventories by the inventory status id, subtype id, owner id (default - 'operator')
     * @params - {inventoryStatusId, inventorySubTypeId}
     * @returns {Promise}
     */
    searchInventoryList(params){
        return qConnector.request("searchInventoryList", params);
    }

    /**
     * Do the inventory operation by the inventory number, operationAlias and reference number(Default  - N/A)
     * @params - {inventoryNumber, operationAlias}
     * @returns {Promise}
     */
    doInventoryStatusOperation(params){
        return qConnector.request("doInventoryStatusOperation", params);
    }

    /**
     * repair inventory bundle
     * @params - {args}
     * @returns {Promise}
     */
    repairInventoryRebundle(params){
        let {args}=params;
        return qConnector.request("repairInventoryRebundle", args);
    }

    /**
     * Mark inventory as damaged by inventory number and serviceInstanceNumber
     * @params - {inventoryNumber, serviceInstanceNumber}
     * @returns {Promise}
     */
    markInventoryAsDamaged(params){
        return qConnector.request("markInventoryAsDamaged", params);
    }
    //endregion

    //region - Package, Product

    /**
     * To get default configuration
     */
    getDefaultConfig(){

    }

    /**
     * To set API Key
     * @param teamApiKey
     */
    setAPIKey(args){

    }

    /**
     * Init method to load initialization
     * @returns {Promise}
     */
    init() {
        return qConnector.request("init", {});

    }

    /**
     * To load hierarchy
     * @returns {Promise}
     */
    loadHierarchy() {
        return qConnector.request("loadHierarchy", {});
    }

    /**
     * To load Packages
     * @params - {teamId}
     * @returns {Promise}
     */
    loadPackages(params) {
        return qConnector.request("loadPackages", params);
    }

    /**
     * To load packages fro team API key
     * @params - {teamApiKey}
     * @returns {Promise}
     */
    loadPackagesForTeamApiKey(params) {
        return qConnector.request("loadPackagesForTeamApiKey", params);
    }

    /**
     * Get all the product offer details
     * @returns {Promise}
     */
    getProductOfferDetails(){
        return qConnector.request("getProductOfferDetails", {});
    }

    /**
     * Get the specific product offer details by the  product Id
     * @params - {args}
     * @returns {Promise}
     */
    getSpecificProdOfferDetails(params){
        let {args}=params;
        return qConnector.request("getSpecificProdOfferDetails", args);
    }

    /**
     * To modify plan attributes
     * @params - {args}
     * @returns {Promise}
     */
    modifyPlanAttributes(params) {
        let {args}=params;
        return qConnector.request("modifyPlanAttributes", params);
    }

    /**
     * Change service plan by account number, effective date, pacakage name, remarks and service instance number
     * @params - {billingAccountNumber, effectiveDate, packageName, remarks, serviceInstanceNumber}
     * @returns {Promise}
     */
    changeServicePlan(params){
        return qConnector.request("changeServicePlan", params);
    }

    /**
     * To subscribe add on package
     * @params - {args}
     * @returns {Promise}
     */
    subscribeAddOnPackage(params){
        let {args}=params;
        return qConnector.request("subscribeAddOnPackage", args);
    }

    /**
     * To un-subscribe add on package
     * @params - {teamId}
     * @returns {Promise}
     */
    unSubscribeAddOnPackage(params){
        return qConnector.request("unSubscribeAddOnPackage", params);
    }

    /**
     * To get FnF member details
     * @params - {args}
     * @returns {Promise}
     */
    getFnFMembersDetails(params){
        let {args}=params;
        return qConnector.request("getFnFMembersDetails", args);
    }

    /**
     * To add FnF members
     * @params - {args}
     * @returns {Promise}
     */
    addFnFMembers(params){
        let {args}=params;
        return qConnector.request("addFnFMembers", args);
    }

    /**
     * To remove FnF members
     * @params - {args}
     * @returns {Promise}
     */
    removeFnFMembers(params) {
        let {args}=params;
        return qConnector.request("removeFnFMembers", args);
    }




    //endregion

    //region- Usage

    /**
     * To load Customer Usage
     * @param params - {serviceInstance}
     * @returns {Promise}
     */
    loadCustomerUsage(params) {
        //this is returning a promise that fires the request to the queue.
        return qConnector.request("loadCustomerUsage", params);

    }

    /**
     * Get the usage data and counter balance data  from DB call or view customer method in connector
     * @param params - {serviceInstanceNumber, fresh, basic}
     * @returns {Promise}
     */
    loadCurrentCustomerUsage(params) {
        //this is returning a promise that fires the request to the queue.
        return qConnector.request("loadCurrentCustomerUsage", params);

    }

    /**
     * Get remaining data for pacakge  by service Instance number
     * @param params - {number, packageId}
     * @returns {Promise}
     */
    loadRemainingDataForPackage(params) {
        //this is returning a promise that fires the request to the queue.
        return qConnector.request("loadRemainingDataForPackage", params);

    }

    /**
     * To view customer usage information
     * @param params - {args}
     */
    viewCustomerUsageInformation(params) {
        let {args}=params;
        //this is returning a promise that fires the request to the queue.
        return qConnector.request("viewCustomerUsageInformation", args);

    }


    //endregion

    //region - PortIn

    /**
     * Set PortIn Number immediate
     * @param params - {serviceInstanceNumber, tempNumber, portInNumber, donor, startTime, externalRefId,portInRecovery}
     * @returns {Promise}
     */
    portInNumber(params) {
        //this is returning a promise that fires the request to the queue.
        return qConnector.request("portInNumber", params);

    }

    /**
     * To Cancel PortIn order
     * @param params - {serviceInstanceNumber, tempNumber, portInNumber,donor, externalRefId, portInRecovery}
     * @returns {Promise}
     */
    portInNumberImmediate(params) {
               //this is returning a promise that fires the request to the queue.
        return qConnector.request("portInNumberImmediate", params);

    }

    /**
     * Get PortIn Status by  PortIn Number
     * @param params - {portInNumber, status}
     * @returns {Promise}
     */
    getPortInStatus(params) {
        //this is returning a promise that fires the request to the queue.
        return qConnector.request("getPortInStatus", params);

    }

    /**
     * Get Donor Code by Network Name
     * @param params - {donor}
     * @returns {Promise}
     */
    getDonorCodeByNetworkName(params) {
        //this is returning a promise that fires the request to the queue.
        return qConnector.request("getDonorCodeByNetworkName", params);
    }

    /**
     * Load Portin Failure reason by portin Number and start_ts
     * @param params - {portInNumber, start_ts}
     * @returns {Promise}
     */
    loadPortInFailureReason(params) {
        //this is returning a promise that fires the request to the queue.
        return qConnector.request("loadPortInFailureReason", params);

    }

    /**
     * Load Portin Completion Status by PortIn Number
     * @param params - {portInNumber}
     * @returns {Promise}
     */
    loadPortInCompleationStatus(params){
        //this is returning a promise that fires the request to the queue.
        return qConnector.request("loadPortInCompleationStatus", params);

    }

    /**
     * Deactive Portin Number
     * @param params - {serviceInstanceNumber, portInNumber}
     * @returns {Promise}
     */
    deactivatePortInNumber(params) {
        //this is returning a promise that fires the request to the queue.
        return qConnector.request("deactivatePortInNumber", params);

    }

    /**
     * To Cancel PortIn order
     * @param params - {MSISDN, reasonCode}
     * @returns {Promise}
     */
    cancelPortInOrder(params) {
        //this is returning a promise that fires the request to the queue.
        return qConnector.request("cancelPortInOrder", params);

    }

    //endregion
}

let oracleMW = new OracleMW();
module.exports = oracleMW;