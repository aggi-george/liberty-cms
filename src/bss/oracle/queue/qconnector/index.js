/*globals module,require, Buffer */
const Promise = require('bluebird');
const uuid = require('uuid/v1');;
const EventEmitter = require('events');
const bssConfig = require('../../../config');
const qManager = require('amqp-connection-manager');

/**
 * This is the entry point for Oracle BRM. From here it is routed to the methods in side elitecore
 */
let constructorCounter = 0;
let createConnectionCounter= 0;
let requestCounter= 0;
let onceCounter = 0;
let emitterCounter= 0;
class QConnector extends EventEmitter{

    constructor(){
        super();

        this.createConnection = this.createConnection.bind(this);
        this.request = this.request.bind(this);


        this.channels = [];
        for(var i = 1;i<=bssConfig.middleWareLength;i++){
            this.channels.push(this.createConnection());
        }
        this.channelCounter = 0;
        //console.log("constructorCounter",++constructorCounter);
    }

    createConnection(){
       //console.log("createConnectionCounter", ++createConnectionCounter);
        // Create a new connection manager
        let connection = this.connect();

        // Ask the connection manager for a ChannelWrapper.  Specify a setup function to run every time we reconnect
        // to the broker.
        let channelWrapper = connection.createChannel({
            json: true,
            setup : (channel)=> {
                Promise.all([
                    channel.assertQueue(bssConfig.reqQ,bssConfig.resQArguments),
                    channel.assertQueue(bssConfig.resQ,bssConfig.resQArguments),
                    channel.consume(bssConfig.resQ,handleResponse)
                ]);
            },
        });
        let handleResponse = (msg)=>{
            channelWrapper.ack(msg);
            console.log("emitterCount",++emitterCounter);
            this.emit(msg.properties.correlationId,msg);
        }
        return channelWrapper;
    }



    connect() {
        return qManager.connect([bssConfig.QUrl]);
    }

    request(method,reqParams){
      //  console.log("requestCounter",++requestCounter);
        var num = this.channelCounter % this.channels.length;
        let channel = this.channels[num];

        this.channelCounter++;
        if(this.channelCounter > this.channels.length) this.channelCounter = 0;

        let request = {method, reqParams};
        let correlationId = uuid();
        let replyTo= bssConfig.resQ;

        // Send some messages to the queue.  If we're not currently connected, these will be queued up in memory
        // until we connect.  Note that `sendToQueue()` and `publish()` return a Promise which is fulfilled or rejected
        // when the message is actually sent (or not sent.)

        return new Promise((resolve,reject)=>{
            if(!channel._channel) reject(new Error("RequestQ Channel is undefined"));
            var failed = function(err) {
                console.log("Message was rejected...  Boo!");
                reject(err);
            };
            request.startTime = new Date().getTime();
            channel.sendToQueue(bssConfig.reqQ, request, {correlationId, replyTo})
                .then(()=> {
                    //console.log(this.resChannelWrapper.eventNames());
                    //Listner to get notified when we get the response from the responseq
                    this.once(correlationId,(response)=>{
                        console.log("event reciever counter",++onceCounter);
                        var data = JSON.parse(Buffer.from(response.content).toString());
                        resolve(data);
                    });
                },failed)
                .catch(failed);
        });

    }
}

let qConnector = new QConnector();
module.exports = qConnector;