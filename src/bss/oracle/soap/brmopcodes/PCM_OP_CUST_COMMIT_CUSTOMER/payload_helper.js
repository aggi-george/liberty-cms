class PayloadHelper{
    constructor(){
      this.rootElement = 'PCM_OP_CUST_COMMIT_CUSTOMER_inputFlist';
        this.enum = {
            COMMON:{
                CURRENCY: {
                    USD: 840,
                    SGD: 702
                },
                //USED FOR PRODUCTS, DISCOUNTS STATUS
                STATUS: {
                    NOT_SET: 0,
                    ACTIVE: 1,
                    INACTIVE: 2,
                    CANCELLED: 3
                },
                STATUSES:{
                    STATUS:{
                        PIN_STATUS_ACTIVE : 10100,
                        PIN_STATUS_INACTIVE : 10102,
                        PIN_STATUS_CLOSED : 10103
                    }
                },
                LOCALE:{
                    USA : 'en_US',
                    SG : 'en_SG',
                    INDONESIA:'id_ID'
                },
            },
            ACCTINFO: {
                BUSINESS_TYPE: {
                    PIN_BUSINESS_TYPE_CONSUMER: 1,
                    PIN_BUSINESS_TYPE_BUSINESS: 2
                },
                ACCOUNT_TYPE: {
                    PIN_ACCOUNT_TYPE_STANDARD: 1,
                    PIN_ACCOUNT_TYPE_BRAND: 2,
                    PIN_ACCOUNT_TYPE_SUBSCRIPTION: 3
                },
                ACTG_TYPE: {
                    PIN_ACTG_TYPE_OPEN_ITEMS: 1,
                    PIN_ACTG_TYPE_BALANCE_FORWARD: 2
                },
            },
            BAL_INFO: {
                RULES: {
                    PIN_CONSUMPTION_RULE_UNDEFINED: 0,
                    PIN_CONSUMPTION_RULE_EST: 1, //Earliest start time
                    PIN_CONSUMPTION_RULE_LST: 2, //Last start time
                    PIN_CONSUMPTION_RULE_EET: 3, //Earliest expiration time
                    PIN_CONSUMPTION_RULE_LET: 4, //Last expiration time
                    PIN_CONSUMPTION_RULE_ESTLET: 5,
                    PIN_CONSUMPTION_RULE_ESTEET: 6,
                    PIN_CONSUMPTION_RULE_LSTEET: 7,
                    PIN_CONSUMPTION_RULE_LSTLET: 8,
                    PIN_CONSUMPTION_RULE_EETEST: 9,
                    PIN_CONSUMPTION_RULE_EETLST: 10,
                    PIN_CONSUMPTION_RULE_LETEST: 11,
                    PIN_CONSUMPTION_RULE_LETLST: 12
                }
            },
            PAYINFO:{
                PAY_TYPE:{
                    PIN_PAY_TYPE_INVOICE:10001,
                    PIN_PAY_TYPE_CC:10003,
                    PIN_PAY_TYPE_DD:10005,
                    PIN_PAY_TYPE_SUBORD:10007 // indicates charge rollup to parent
                },
                INV_TYPE:{
                    PIN_INV_TYPE_DETAIL:0x01,
                    PIN_INV_TYPE_SUMMARY:0x02,
                    PIN_INV_TYPE_REGULAR:0x04,
                    PIN_INV_TYPE_SUBORDINATE:0x08,
                    PIN_INV_TYPE_PARENT:0x10,
                    PIN_INV_TYPE_PARENT_WITH_SUBORDS:0x18,
                    PIN_INV_TYPE_HIERARCHY:0x20,
                    PIN_INV_TYPE_TRIAL_INVOICE:0x40,
                },
                INHERITED_INFO:{
                    INV_INFO:{
                        DELIVERY_PREFER:{
                            PIN_INV_EMAIL_DELIVERY:0,
                            PIN_INV_USP_DELIVERY:1,
                            PIN_INV_FAX_DELIVERY:3
                        }
                    },
                    DD_INFO:{
                        TYPE:{
                            PIN_ACCOUNT_TYPE_CHECKING:1,
                            PIN_ACCOUNT_TYPE_SAVINGS:2,
                            PIN_ACCOUNT_TYPE_CORPORATE:3
                        }
                    }
                }
            },
            BILLINFO:{
                BILLING_STATUS:{
                    PIN_BILL_ACTIVE:0,
                    PIN_BILL_INACTIVE:1,
                    PIN_BILL_ACCOUNTING_ONLY:2
                },
                STATUS:{
                    PIN_STATUS_ACTIVE:10100,
                    PIN_STATUS_INACTIVE:10102,
                    PIN_STATUS_CLOSED:10103
                }
            },
            NAME_INFO:{
                PHONES:{
                    PIN_PHONE_TYPE_UNKNOWN:0,
                    PIN_PHONE_TYPE_HOME:1,
                    PIN_PHONE_TYPE_WORK:2,
                    PIN_PHONE_TYPE_FAX:3,
                    PIN_PHONE_TYPE_PAGER:4,
                    PIN_PHONE_TYPE_PORTABLE:5,
                    PIN_PHONE_TYPE_POP:6,
                    PIN_PHONE_TYPE_SUPPORT:7,
                }
            },
            SERVICES:{
                PASSWD_STATUS:{
                    PIN_SERV_PASSWDSTATUS_NORMAL:0,
                    PIN_SERV_PASSWDSTATUS_TEMPORARY:1,
                    PIN_SERV_PASSWDSTATUS_EXPIRES:2,
                    PIN_SERV_PASSWDSTATUS_INVALID:3
                },
                DEAL_INFO:{
                    DEALS:{
                        TYPE:{
                            PIN_PRICE_DEAL_OPTIONAL:0,
                            PIN_PRICE_DEAL_REQUIRED:1
                        }
                    }
                }
            }
        };
       
        this.constants={
            'ACCTINFO': {
                '$': { //$ indicates that the json inside are attributes
                  'elem': '0' 
                },
                'BAL_INFO': {
                  '$': {
                    'elem': '0'
                  }
                },
                'BUSINESS_TYPE': '', //see enum
                'CURRENCY': '', //see enum
                'POID': '0.0.0.1 /account -1 0' //###### please explain the structure //this is how it is passed to convey that this is new
              },
              'BAL_INFO': {
                '$': {
                  'elem': '0'
                },
                'BILLINFO': {
                  '$': {
                    'elem': '0'
                  }
                },
                'LIMIT': {
                  '$': {
                    'elem': '' //here the currency enum value the limit is specified goes 
                  },
                  'CREDIT_LIMIT': '' //value is blank in the sample
                },
                'NAME': '',
                'POID': '0.0.0.1 /balance_group -1 0'
              },
              'BILLINFO': {
                '$': {
                  'elem': '0'
                },
                'BAL_INFO': {
                  '$': {
                    'elem': '0'
                  }
                },
                'BILLINFO_ID': '',
                'PAYINFO': {
                  '$': {
                    'elem': '0'
                  }
                },
                'PAY_TYPE': '',
                'POID': '0.0.0.1 /billinfo -1 0'
              },
              'CODE': '37448857-e8fb-4881-a538-d6b96fbb2e2b', //##########    What is CODE          
              'CREATED_T': '',
              'DEAL_OBJ': '0.0.0.0 / 0 0',
              'DESCR': '', //########## 
              'FLAGS': '0', //Plan level flags. This may be used to pass plan customization flag PIN_PLAN_FLG_ON_DEMAND to force on demand billing.
              'LOCALES': {
                '$': {
                  'elem': '1'
                },
                'LOCALE': ''
              },
              'MOD_T': '', //########## 
              'NAME': '', //########## 
              'NAMEINFO': {
                '$': {
                  'elem': '1'
                },
                'ADDRESS': '',
                'CITY': '',
                'CONTACT_TYPE': 'Account holder', //########## 
                'COUNTRY': '',
                'EMAIL_ADDR': '',
                'FIRST_NAME': '',
                'LAST_NAME': '',
                'MIDDLE_NAME': '',
                'SALUTATION': '',
                'STATE': '',
                'ZIP': ''
              },
              'PAYINFO': {
                '$': {
                  'elem': '0'
                },
                'FLAGS': '1',
                'INHERITED_INFO': { //##########  what is the inherited info for other PayTypes?
                  'INV_INFO': {
                    '$': {
                      'elem': '0'
                    },
                    'ADDRESS': '',
                    'CITY': '',
                    'COUNTRY': '',
                    'DELIVERY_DESCR': '',
                    'DELIVERY_PREFER': '',
                    'EMAIL_ADDR': '',
                    'INV_TERMS': '',
                    'NAME': '',
                    'STATE': '',
                    'ZIP': ''
                  }
                },
                'INV_TYPE': '', //enum
                'NAME': '',
                'PAY_TYPE': '',
                'POID': '0.0.0.1 /payinfo/invoice -1 0'
              },
              'POID': '', //0.0.0.1 /plan 2897244 0', the poid of the pln has to be passed.
              'READ_ACCESS': 'B', //########## what is this. Not provided in documentation
              'SERVICES': {
                '$': {
                  'elem': '0'
                },
                'BAL_INFO': {
                  '$': {
                    'elem': '0'
                  }
                },
                'BAL_INFO_INDEX': '0',
                'DEALS': {
                  '$': {
                    'elem': '0'
                  },
                  'ACCOUNT_OBJ': '0.0.0.1 /account 1 0',
                  'CODE': '',
                  'CREATED_T': '', //#### this is the deal created date from prodcut heirarchy??
                  'DEAL_OBJ': '', // '0.0.0.1 /deal 568019 4',
                  'DESCR': '',
                  'END_T': '0', //if this is zero the deal will not expire
                  'FLAGS': '0',
                  'MOD_T': '',
                  'NAME': '',
                  'PERMITTED': '', //'service/telco/gsm/telephony',
                  'PRODUCTS': {
                    '$': {
                      'elem': '0'
                    },
                    'CYCLE_DISCOUNT': '0',
                    'CYCLE_END_DETAILS': '2', //####### Not provided in documentation
                    'CYCLE_END_T': '0',
                    'CYCLE_START_DETAILS': '1', //####### Not provided in documentation
                    'CYCLE_START_T': '0',
                    'DESCR': '',
                    'NAME': '',
                    'OWN_MAX': '', //####### Not provided in documentation
                    'OWN_MIN': '', //####### Not provided in documentation
                    'PRODUCT_OBJ': '', //'0.0.0.1 /product 2899548 0', 
                    'PURCHASE_DISCOUNT': '0', 
                    'PURCHASE_END_DETAILS': '2', //####### Not provided in documentation
                    'PURCHASE_END_T': '0', 
                    'PURCHASE_START_DETAILS': '1', //####### Not provided in documentation
                    'PURCHASE_START_T': '0',
                    'QUANTITY': '1',
                    'STATUS': '1', 
                    'STATUS_FLAGS': '0', //Please explain
                    'USAGE_DISCOUNT': '0',
                    'USAGE_END_DETAILS': '2', //####### Not provided in documentation
                    'USAGE_END_T': '0',
                    'USAGE_START_DETAILS': '1', //####### Not provided in documentation
                    'USAGE_START_T': '0'

                    //#### what is the difference between CYCLE / PURCHASE / USAGE
                  },
                  'READ_ACCESS': 'B', //####### Not provided in documentation
                  'START_T': '0', 
                  'WRITE_ACCESS': 'S' //####### Not provided in documentation
                },
                'DEAL_INFO': {
                  'DESCR': '',
                  'END_T': '0',  //If end time is zero, valid period does not expire. 
                  'FLAGS': '0', //Flag to indicate on demand bill at the deal level and has value PIN_DEAL_FLG_BILL_ON_DEMAND for on demand billing. Present only if a customized deal is to be purchased.
                  'NAME': 'Sample_MRC_BUNDLE',
                  'POID': '', //'0.0.0.1 /deal 568019 4',
                  'PRODUCTS': {
                    '$': {
                      'elem': '0'
                    },
                    'CYCLE_DISCOUNT': '0',
                    'CYCLE_END_DETAILS': '2',
                    'CYCLE_END_OFFSET': '0',
                    'CYCLE_END_T': '0',
                    'CYCLE_END_UNIT': '0',
                    'CYCLE_START_DETAILS': '1',
                    'CYCLE_START_OFFSET': '0',
                    'CYCLE_START_T': '0',
                    'CYCLE_START_UNIT': '0',
                    'DESCR': 'Test 100 free seconds',
                    'PLAN_OBJ': '',//'0.0.0.1 /plan 2897244 0',
                    'PRODUCT_OBJ':'',// '0.0.0.1 /product 2899548 0',
                    'PURCHASE_DISCOUNT': '0',
                    'PURCHASE_END_DETAILS': '2',
                    'PURCHASE_END_OFFSET': '0',
                    'PURCHASE_END_T': '0',
                    'PURCHASE_END_UNIT': '0',
                    'PURCHASE_START_DETAILS': '1',
                    'PURCHASE_START_OFFSET': '0',
                    'PURCHASE_START_T': '0',
                    'PURCHASE_START_UNIT': '0',
                    'QUANTITY': '1',
                    'STATUS': '1',
                    'STATUS_FLAGS': '0',
                    'USAGE_DISCOUNT': '0',
                    'USAGE_END_DETAILS': '2',
                    'USAGE_END_OFFSET': '0',
                    'USAGE_END_T': '0',
                    'USAGE_END_UNIT': '0',
                    'USAGE_START_DETAILS': '1',
                    'USAGE_START_OFFSET': '0',
                    'USAGE_START_T': '0',
                    'USAGE_START_UNIT': '0'
                  },
                  'START_T': '0'
                },
                'DEVICES': {
                  '$': {
                    'elem': '0'
                  },
                  'DEVICE_OBJ': '', // '0.0.0.1 /device/num 57814 1',
                  'FLAGS': '1'
                },
                'INHERITED_INFO': {
                  'TELCO_INFO': {
                    'PRIMARY_NUMBER': '1'
                  }
                },
                'LOGIN': '',
                'PASSWD_CLEAR': '',
                'SERVICE_CODES': {
                  '$': {
                    'elem': '0'
                  },
                  'STATE_ID': '0'
                },
                'SERVICE_ID': '/service/telco/gsm/telephony-cc4dab83-8cf7-41e9-9da1-80998d79db52',
                'SERVICE_OBJ': '0.0.0.1 /service/telco/gsm/telephony -1 0',
                'SUBSCRIPTION_INDEX': '0',
                'SUBSCRIPTION_OBJ': ''
              },
              'WRITE_ACCESS': 'S'
        }
    }

}

let payloadHelper = new PayloadHelper();
module.exports = payloadHelper;