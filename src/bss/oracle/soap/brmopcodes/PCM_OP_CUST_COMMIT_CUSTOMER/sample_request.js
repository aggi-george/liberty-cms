module.exports = {
    'PCM_OP_CUST_COMMIT_CUSTOMER_inputFlist': {
        'ACCTINFO': {
            '$': {
                'elem': '0'
            },
            'BAL_INFO': {
                '$': {
                    'elem': '0'
                }
            },
            'BUSINESS_TYPE': '1',
            'CURRENCY': '840',
            'POID': '0.0.0.1 /account -1 0'
        },
        'BAL_INFO': {
            '$': {
                'elem': '0'
            },
            'BILLINFO': {
                '$': {
                    'elem': '0'
                }
            },
            'LIMIT': {
                '$': {
                    'elem': '840'
                },
                'CREDIT_LIMIT': ''
            },
            'NAME': 'Balance Group<Account>',
            'POID': '0.0.0.1 /balance_group -1 0'
        },
        'BILLINFO': {
            '$': {
                'elem': '0'
            },
            'BAL_INFO': {
                '$': {
                    'elem': '0'
                }
            },
            'BILLINFO_ID': 'Bill Unit(1)',
            'PAYINFO': {
                '$': {
                    'elem': '0'
                }
            },
            'PAY_TYPE': '10001',
            'POID': '0.0.0.1 /billinfo -1 0'
        },
        'CODE': '37448857-e8fb-4881-a538-d6b96fbb2e2b',
        'CREATED_T': '1525115373',
        'DEAL_OBJ': '0.0.0.0 / 0 0',
        'DESCR': 'Sample_MRC',
        'FLAGS': '0',
        'LOCALES': {
            '$': {
                'elem': '1'
            },
            'LOCALE': 'en_US'
        },
        'MOD_T': '1525115373',
        'NAME': 'Sample_MRC',
        'NAMEINFO': {
            '$': {
                'elem': '1'
            },
            'ADDRESS': 'Test_LW',
            'CITY': 'Test_LW',
            'CONTACT_TYPE': 'Account holder',
            'COUNTRY': 'IN',
            'EMAIL_ADDR': 'Test_LW',
            'FIRST_NAME': 'Test_LW',
            'LAST_NAME': 'Test_LW',
            'MIDDLE_NAME': 'Test_LW',
            'SALUTATION': 'Mr',
            'STATE': 'Test_LW',
            'ZIP': 'Test_LW'
        },
        'PAYINFO': {
            '$': {
                'elem': '0'
            },
            'FLAGS': '1',
            'INHERITED_INFO': {
                'INV_INFO': {
                    '$': {
                        'elem': '0'
                    },
                    'ADDRESS': 'Test_LW',
                    'CITY': 'Test_LW',
                    'COUNTRY': 'IN',
                    'DELIVERY_DESCR': 'Test_LW',
                    'DELIVERY_PREFER': '0',
                    'EMAIL_ADDR': '',
                    'INV_TERMS': '0',
                    'NAME': 'Test_LW Test_LW Test_LW',
                    'STATE': 'Test_LW',
                    'ZIP': 'Test_LW'
                }
            },
            'INV_TYPE': '257',
            'NAME': 'Invoice1',
            'PAY_TYPE': '10001',
            'POID': '0.0.0.1 /payinfo/invoice -1 0'
        },
        'POID': '0.0.0.1 /plan 2891837 0',
        'READ_ACCESS': 'B',
        'SERVICES': {
            '$': {
                'elem': '0'
            },
            'BAL_INFO': {
                '$': {
                    'elem': '0'
                }
            },
            'BAL_INFO_INDEX': '0',
            'DEALS': {
                '$': {
                    'elem': '0'
                },
                'ACCOUNT_OBJ': '0.0.0.1 /account 1 0',
                'CODE': 'b4909266-529f-435e-867a-4af98ddf1164',
                'CREATED_T': '1444819566',
                'DEAL_OBJ': '0.0.0.1 /deal 568019 4',
                'DESCR': 'Test_NCR_Proration',
                'END_T': '0',
                'FLAGS': '0',
                'MOD_T': '1525115373',
                'NAME': 'Sample_MRC_BUNDLE',
                'PERMITTED': '/service/telco/gsm/telephony',
                'PRODUCTS': {
                    '$': {
                        'elem': '0'
                    },
                    'CYCLE_DISCOUNT': '0',
                    'CYCLE_END_DETAILS': '2',
                    'CYCLE_END_T': '0',
                    'CYCLE_START_DETAILS': '1',
                    'CYCLE_START_T': '0',
                    'DESCR': 'Test 100 free seconds',
                    'NAME': 'Test_Free_MRC',
                    'OWN_MAX': '',
                    'OWN_MIN': '',
                    'PRODUCT_OBJ': '0.0.0.1 /product 1869581 0',
                    'PURCHASE_DISCOUNT': '0',
                    'PURCHASE_END_DETAILS': '2',
                    'PURCHASE_END_T': '0',
                    'PURCHASE_START_DETAILS': '1',
                    'PURCHASE_START_T': '0',
                    'QUANTITY': '1',
                    'STATUS': '1',
                    'STATUS_FLAGS': '0',
                    'USAGE_DISCOUNT': '0',
                    'USAGE_END_DETAILS': '2',
                    'USAGE_END_T': '0',
                    'USAGE_START_DETAILS': '1',
                    'USAGE_START_T': '0'
                },
                'READ_ACCESS': 'B',
                'START_T': '0',
                'WRITE_ACCESS': 'S'
            },
            'DEAL_INFO': {
                'DESCR': 'Test_NCR_Proration',
                'END_T': '0',
                'FLAGS': '0',
                'NAME': 'Sample_MRC_BUNDLE',
                'POID': '0.0.0.1 /deal 2028144 4',
                'PRODUCTS': {
                    '$': {
                        'elem': '0'
                    },
                    'CYCLE_DISCOUNT': '0',
                    'CYCLE_END_DETAILS': '2',
                    'CYCLE_END_OFFSET': '0',
                    'CYCLE_END_T': '0',
                    'CYCLE_END_UNIT': '0',
                    'CYCLE_START_DETAILS': '1',
                    'CYCLE_START_OFFSET': '0',
                    'CYCLE_START_T': '0',
                    'CYCLE_START_UNIT': '0',
                    'DESCR': 'Test 100 free seconds',
                    'PLAN_OBJ': '0.0.0.1 /plan 2028144 0',
                    'PRODUCT_OBJ': '0.0.0.1 /product 1869581 0',
                    'PURCHASE_DISCOUNT': '0',
                    'PURCHASE_END_DETAILS': '2',
                    'PURCHASE_END_OFFSET': '0',
                    'PURCHASE_END_T': '0',
                    'PURCHASE_END_UNIT': '0',
                    'PURCHASE_START_DETAILS': '1',
                    'PURCHASE_START_OFFSET': '0',
                    'PURCHASE_START_T': '0',
                    'PURCHASE_START_UNIT': '0',
                    'QUANTITY': '1',
                    'STATUS': '1',
                    'STATUS_FLAGS': '0',
                    'USAGE_DISCOUNT': '0',
                    'USAGE_END_DETAILS': '2',
                    'USAGE_END_OFFSET': '0',
                    'USAGE_END_T': '0',
                    'USAGE_END_UNIT': '0',
                    'USAGE_START_DETAILS': '1',
                    'USAGE_START_OFFSET': '0',
                    'USAGE_START_T': '0',
                    'USAGE_START_UNIT': '0'
                },
                'START_T': '0'
            },
            'DEVICES': {
                '$': {
                    'elem': '0'
                },
                'DEVICE_OBJ': '0.0.0.1 /device/num 57814 1',
                'FLAGS': '1'
            },
            'INHERITED_INFO': {
                'TELCO_INFO': {
                    'PRIMARY_NUMBER': '1'
                }
            },
            'LOGIN': 'telco_57814',
            'PASSWD_CLEAR': 'XXXX',
            'SERVICE_CODES': {
                '$': {
                    'elem': '0'
                },
                'STATE_ID': '0'
            },
            'SERVICE_ID': '/service/telco/gsm/telephony-cc4dab83-8cf7-41e9-9da1-80998d79db52',
            'SERVICE_OBJ': '0.0.0.1 /service/telco/gsm/telephony -1 0',
            'SUBSCRIPTION_INDEX': '0',
            'SUBSCRIPTION_OBJ': ''
        },
        'WRITE_ACCESS': 'S'
    }
}