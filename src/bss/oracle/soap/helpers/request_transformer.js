/*globals require, module */
const resTransformer = require('./response_transformer');
const toXML = require('xmljson').to_xml;
const uuid = require('uuid/v1');
/**
 * This class is used mainly to transform request from upstream systems into the structure compatible to BRM
 */
class RequestTransformer {
    constructor() {

    }


    /**
     * returns a new callback function that transforms the result by passing the result to the action that is passed as an param
     * @param callback -> original callback
     * @param resTransformer -> this is a function that is triggered on the result and new result is used on the orignal callback
     * @returns {function(*=, *=)} -> new callback
     */
    getTransformedCallback(callback,resTransformer){
        return (err,result)=>{
            if(err) return callback(err);
            if(resTransformer) {
               resTransformer(result).then(res=>{
                    result = res;
                    callback(null,res);
                }, err=>{
                    callback(err);
                });
            }
            else callback(null,result);
        }
    }

    /**
     * Create Account
     * @param params - {args, callback}
     */
    createAccount(params) {
        let {args,callback} = params;

        //Now time to define new callback that will transform the response and trigger the original callback
        //changing the callback is usefull when you want to do something with the result.

        callback = this.getTransformedCallback(callback,resTransformer.createAccount);

        
        let requestMain = require('../brmopcodes/PCM_OP_CUST_COMMIT_CUSTOMER/sample_request');
        let requestWorking = requestMain.PCM_OP_CUST_COMMIT_CUSTOMER_inputFlist;
        let payloadHelper = require('../brmopcodes/PCM_OP_CUST_COMMIT_CUSTOMER/payload_helper');

        let personalInfo = args.personalInformation;
        let billingInfo = personalInfo.billingAddress;

        //Name info using billing and personal info
      //  requestWorking.LOCALES.LOCALE = payloadHelper.enum.COMMON.LOCALE[args.location]; //##### en_us not working
        requestWorking.NAMEINFO.ADDRESS = billingInfo.unit + ' ' + billingInfo.floor + ' ' + billingInfo.buildingNo + ' ' + billingInfo.street;
        requestWorking.NAMEINFO.CITY=billingInfo.city;
        requestWorking.NAMEINFO.CONTACT_TYPE='Account holder';
        requestWorking.NAMEINFO.COUNTRY=billingInfo.country;
        requestWorking.NAMEINFO.EMAIL_ADDR=personalInfo.email;
        requestWorking.NAMEINFO.FIRST_NAME=personalInfo.firstName;
        requestWorking.NAMEINFO.LAST_NAME=personalInfo.lastName;
        requestWorking.NAMEINFO.MIDDLE_NAME='', //###### 
        requestWorking.NAMEINFO.SALUTATION='Mr', //###### 
        requestWorking.NAMEINFO.STATE=billingInfo.state;
        requestWorking.NAMEINFO.ZIP=billingInfo.zip;

        /*TODO
        //use payloadhelper.constants instead of sample request
        //add deals
        //add deal info
        //add products
        //add discounts
        //add invoice type
        //add credit card information
        //add advance payments
        //add account information

         */

        //handle currency
       // requestWorking.ACCTINFO.CURRENCY = payloadHelper.enum.COMMON.CURRENCY.SGD;
       // requestWorking.BAL_INFO.LIMIT.$.elem = payloadHelper.enum.COMMON.CURRENCY.SGD;

        //handle service Id

        let id = uuid();
      //  requestWorking.SERVICES.SERVICE_ID = `/service/telco/gsm/telephony-${id}`;
      //  requestWorking.SERVICES.SERVICE_OBJ = `0.0.0.1 /service/telco/gsm/telephony-${id} 0`

        //device info
        requestWorking.SERVICES.DEVICES.DEVICE_OBJ = `0.0.0.1 /device/num ${args.deviceNumber} 1`; //since this is important I am passing this in the payload for now

        //change login
        requestWorking.SERVICES.LOGIN = `telco${args.deviceNumber}`;

        //convert the requestmain to xml and then to string
        return new Promise((resolve,reject)=>{
            toXML(JSON.stringify(requestMain), function (error, xml) {
                if(error) return reject(error);

                let xmlString = xml.toString();

                xmlString = xmlString
                    .replace(`<data><${payloadHelper.rootElement}>`,`<${payloadHelper.rootElement}>`)
                    .replace(`</${payloadHelper.rootElement}></data>`,`</${payloadHelper.rootElement}>`);

                console.log(xmlString);

                args = {
                    'flags':1,
                    'PCM_OP_CUST_COMMIT_CUSTOMER_request' :xmlString
                };

                resolve({args,callback});
            });
        });
    }
}

let requestTransformer = new RequestTransformer();
module.exports = requestTransformer;