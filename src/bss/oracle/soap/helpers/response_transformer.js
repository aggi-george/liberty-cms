const toJson = require('xmljson').to_json;
/**
 * This class is used mainly to transform request from upstream systems into the structure compatible to BRM
 */
class ResponseTransformer {
    constructor() {

    }

    /**
     * Create Account
     * @param params - {args, callback}
     */
    createAccount(result) {
        //assuming that we transformed the result from BRM
        xmlResponseString = result.pcmOpCustCommitCustomerReturn["\\$value"];
        //convert the xmlResponseString to json
        return new Promise((resolve,reject)=>{
            toJson(xmlResponseString, function (error, data) {
                if(error) return reject(error);
                resolve(data);
            });
        });
    }
}

let responseTransformer = new ResponseTransformer();
module.exports = responseTransformer;