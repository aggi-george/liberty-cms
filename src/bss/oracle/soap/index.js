/*globals module */
const Promise=require('bluebird');
const soapConnector = require('./soapconnector/index');
const requestTransformer = require('./helpers/request_transformer');

class Soap {
    constructor() {
        this.createAccount = this.createAccount.bind(this);
    }
    createAccount(args){
        return new Promise((resolve,reject)=>{
            requestTransformer.createAccount({args,callback:resolve}).then(({args,callback})=>{
                soapConnector.createAccount(args).then(callback,reject);
            },reject);
        });
    }
}

let soap = new Soap();
module.exports = soap;