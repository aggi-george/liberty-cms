/* globals require,module*/
const connector = require('./connector');
const dateformat = require('dateformat');

class SoapClient {
    constructor() {
        this.getPromiseCallback = this.getPromiseCallback.bind(this);
        this.testBRM = this.testBRM.bind(this);
        this.loadBssMode = this.loadBssMode.bind(this);
        this.init = this.init.bind(this);
        this.getProductOfferDetails = this.getProductOfferDetails.bind(this);
        this.getSpecificProdOfferDetails = this.getSpecificProdOfferDetails.bind(this);
        this.getBusinessHierarchyDetail = this.getBusinessHierarchyDetail.bind(this);
        this.getCustomerHierarchyDetails = this.getCustomerHierarchyDetails.bind(this);
        this.subscribeAddOnPackage = this.subscribeAddOnPackage.bind(this);
        this.unSubscribeAddOnPackage = this.unSubscribeAddOnPackage.bind(this);
        this.getFnFMembersDetails = this.getFnFMembersDetails.bind(this);
        this.addFnFMembers = this.addFnFMembers.bind(this);
        this.removeFnFMembers = this.removeFnFMembers.bind(this);
        this.modifyPlanAttributes = this.modifyPlanAttributes.bind(this);
        this.searchInventory = this.searchInventory.bind(this);
        this.searchInventoryList = this.searchInventoryList.bind(this);
        this.doInventoryStatusOperation = this.doInventoryStatusOperation.bind(this);
        this.repairInventoryRebundle = this.repairInventoryRebundle.bind(this);
        this.getInventoryDetail = this.getInventoryDetail.bind(this);
        this.listInventory = this.listInventory.bind(this);
        this.unpairInventory = this.unpairInventory.bind(this);
        this.repairInventory = this.repairInventory.bind(this);
        this.repairInventoryXml = this.repairInventoryXml.bind(this);
        this.removeInventory = this.removeInventory.bind(this);
        this.addInventory = this.addInventory.bind(this);
        this.markInventoryAsDamaged = this.markInventoryAsDamaged.bind(this);
        this.createInventory = this.createInventory.bind(this);
        this.updateInventoryAttributes = this.updateInventoryAttributes.bind(this);
        this.createAccount = this.createAccount.bind(this);
        this.changeServicePlan = this.changeServicePlan.bind(this);
        this.searchAccount = this.searchAccount.bind(this);
        this.searchServiceInstance = this.searchServiceInstance.bind(this);
        this.getAccountStatement = this.getAccountStatement.bind(this);
        this.addDepositToCustomerBillingAccount = this.addDepositToCustomerBillingAccount.bind(this);
        this.getBillingAccountDetail = this.getBillingAccountDetail.bind(this);
        this.updateCustomerAccountData = this.updateCustomerAccountData.bind(this);
        this.updateBillingAccountData = this.updateBillingAccountData.bind(this);
        this.updateServiceAccountData = this.updateServiceAccountData.bind(this);
        this.updateBillingAccount = this.updateBillingAccount.bind(this);
        this.updateServiceAccount = this.updateServiceAccount.bind(this);
        this.changeServiceInstanceStatus = this.changeServiceInstanceStatus.bind(this);
        this.searchBill = this.searchBill.bind(this);
        this.searchCreditNote = this.searchCreditNote.bind(this);
        this.makeDebitPaymentDataRequest = this.makeDebitPaymentDataRequest.bind(this);
        this.makeAdvancePaymentDataRequest = this.makeAdvancePaymentDataRequest.bind(this);
        this.searchPayment = this.searchPayment.bind(this);
        this.createCreditNote = this.createCreditNote.bind(this);
        this.cancelPortInOrder = this.cancelPortInOrder.bind(this);
        this.mnpNotification = this.mnpNotification.bind(this);
        this.serviceProvisioningNotification = this.serviceProvisioningNotification.bind(this);
        this.viewCustomer = this.viewCustomer.bind(this);
        this.viewCustomerUsageInformation = this.viewCustomerUsageInformation.bind(this);
        this.updateCustomerBucket = this.updateCustomerBucket.bind(this);

    }

    // Common callback
    getPromiseCallback(resolve, reject) {
        return (err, result) => {
            if (err) return reject(err);
            resolve(result);
        }
    }

    //region Test

    testBRM() {
        let xml = "&lt;PCM_OP_READ_OBJ_inputFlist&gt;&lt;POID&gt;0.0.0.1 /account 3177663 0&lt;/POID&gt;&lt;/PCM_OP_READ_OBJ_inputFlist&gt;";

        return new Promise((resolve, reject) => {
            let callback = this.getPromiseCallback(resolve, reject);
            var parameter = {
                "flags":1,
                "PCM_OP_READ_OBJ_request" : { "$xml" : xml }
            };
            connector.makeSoapCall(connector.BRMReadServices, "pcmOpReadObj",parameter, {escapeXML:false}, callback);
        });
    };

    //endregion


//region General

    loadBssMode() {
        return new Promise((resolve, reject) => {
            let callback = this.getPromiseCallback(resolve, reject);
            connector.loadBssMode(callback);
        });
    }

    init() {
        return new Promise((resolve, reject) => {
            let callback = this.getPromiseCallback(resolve, reject);
            connector.init(callback);
        });
    };

    setDisabledBss(bssList) {
        connector.disabledBss = bssList;
    };

    setOfflineMode(offline) {
        connector.bssOfflineMode = offline;
    };

    getCountTimeouts() {
        return connector.countTimeouts;
    };

    getCountRequests() {
        return connector.countRequests;
    };

    getCountErrors() {
        return connector.countErrors;
    };

    addStatsUpdateListener(l) {
        const key = new Date().getTime() + "_" + Math.random();

        connector.statistics.listeners.push({
            key: key,
            callback: l
        });

        return key;
    };

    removeStatsUpdateListener(key) {
        for (let i = 0; i < connector.statistics.listeners.length; i++) {
            const obj = connector.statistics.listeners[i];
            if (obj.key === key) {
                connector.statistics.listeners.splice(i, 1);
                return true;
            }
        }
        return false;
    };

//endregion

//region Product

    /**
     * Get all the product offer details
     * @returns {Promise}
     */
    getProductOfferDetails() {
        return new Promise((resolve, reject) => {
            let callback = this.getPromiseCallback(resolve, reject);
            connector.makeSoapCall(connector.apiProductManagerAPIWebService, "getProductOfferDetails", {}, undefined, callback);
        });
    };

    /**
     * Get the specific product offer details by the  product Id
     * @param args - {args}
     * @returns {Promise}
     */
    getSpecificProdOfferDetails(args) {
        return new Promise((resolve, reject) => {
            let callback = this.getPromiseCallback(resolve, reject);
            connector.makeSoapCall(connector.apiProductManagerAPIWebService, "getSpecificProdOfferDetails", {arg0: {productIds: args}}, undefined, callback);
        });
    };

//endregion

//region Business Heirarchy, Package

    /**
     * Get the business hierarchy details
     * @returns {Promise}
     */
    getBusinessHierarchyDetail() {
        return new Promise((resolve, reject) => {
            let callback = this.getPromiseCallback(resolve, reject);
            connector.makeSoapCall(connector.apiProductManagerAPIWebService_v3, "getBusinessHierarchyDetail", {arg0: {}}, undefined, callback);
        });
    };

    /**
     * To get Customer Hierarchy Details by account number
     * @param args - {accountNumber}
     * @returns {Promise}
     */

    getCustomerHierarchyDetails(args) {
        let {accountNumber}=args;
        return new Promise((resolve, reject) => {
            let callback = this.getPromiseCallback(resolve, reject);
            connector.makeSoapCall(connector.apiCrestelCAAMCustomerAccount2Operations, "getCustomerHierarchyDetails",
                {arguments: {accountNumber: accountNumber}}, undefined, callback);
        });
    };

    /**
     * To subscribe add on package
     * @args - {teamId}
     * @returns {Promise}
     */
    subscribeAddOnPackage(args) {
        return new Promise((resolve, reject) => {
            let callback = this.getPromiseCallback(resolve, reject);
            connector.makeSoapCall(connector.apiCrestelCAAMServiceOperations, "subscribeAddOnPackage", {arguments: args}, undefined, callback);
        });
    };

    /**
     * To un-subscribe add on package
     * @param - {args}
     * @returns {Promise}
     */
    unSubscribeAddOnPackage(args) {
        return new Promise((resolve, reject) => {
            let callback = this.getPromiseCallback(resolve, reject);
            connector.makeSoapCall(connector.apiCrestelCAAMServiceOperations, "unSubscribeAddOnPackage", {arguments: args}, undefined, callback);
        });
    };

    /**
     * To get FnF member details
     * @param - {args}
     * @returns {Promise}
     */
    getFnFMembersDetails(args) {
        return new Promise((resolve, reject) => {
            let callback = this.getPromiseCallback(resolve, reject);
            connector.makeSoapCall(connector.apiCrestelCAAMServiceOperations, "getFnFMembersDetails", {arguments: args}, undefined, callback);
        });
    };

    /**
     * To add FnF members
     * @param- {args}
     * @returns {Promise}
     */
    addFnFMembers(args) {
        return new Promise((resolve, reject) => {
            let callback = this.getPromiseCallback(resolve, reject);
            connector.makeSoapCall(connector.apiCrestelCAAMServiceOperations, "addFnFMembers", {arguments: args}, undefined, callback);
        });
    };

    /**
     * To remove FnF members
     * @param - {args}
     * @returns {Promise}
     */
    removeFnFMembers(args) {
        return new Promise((resolve, reject) => {
            let callback = this.getPromiseCallback(resolve, reject);
            connector.makeSoapCall(connector.apiCrestelCAAMServiceOperations, "removeFnFMembers", {arguments: args}, undefined, callback);
        });
    };

    /**
     * To modify plan attributes
     * @param - {args}
     * @returns {Promise}
     */
    modifyPlanAttributes(args) {
        return new Promise((resolve, reject) => {
            let callback = this.getPromiseCallback(resolve, reject);
            connector.makeSoapCall(connector.apiCrestelCAAMServiceOperations, "modifyPlanAttributes", {arguments: args}, undefined, callback);
        });
    };

//endregion

//region Inventory

    /**
     * Get the inventory details by the inventory number
    * @args - {inventoryNumber}
    * @returns {Promise}
    */
    searchInventory(args) {

        let {inventoryNumber}=args;
        return new Promise((resolve, reject) => {
            let callback = this.getPromiseCallback(resolve, reject);
            connector.makeSoapCall(connector.apiCrestelInventoryOperation, "searchInventory",
                {arguments: {inventoryNumber: inventoryNumber}}, undefined, callback);
        });
    };

    /**
     * Get the list of inventories by the inventory status id, subtype id, owner id (default - 'operator')
     * @param args - {inventoryStatusId, inventorySubTypeId}
     * @returns {Promise}
     */
    searchInventoryList(args) {
        let {inventoryStatusId, inventorySubTypeId}=args;
        return new Promise((resolve, reject) => {
            let callback = this.getPromiseCallback(resolve, reject);
            connector.makeSoapCall(connector.apiCrestelInventoryOperation, "searchInventory",
                {
                    arguments: {
                        inventoryStatusId: inventoryStatusId,
                        inventorySubTypeId: inventorySubTypeId,
                        ownerId: "Operator"
                    }
                }, undefined, callback);
        });

    };

    /**
     * Do the inventory operation by the inventory number, operationAlias and reference number(Default  - N/A)
     * @param args - {inventoryNumber, operationAlias}
     * @returns {Promise}
     */
    doInventoryStatusOperation(args) {
        let {inventoryNumber, operationAlias}=args;
        return new Promise((resolve, reject) => {
            let callback = this.getPromiseCallback(resolve, reject);
            connector.makeSoapCall(connector.apiCrestelInventoryOperation, "doInventoryStatusOperation", {
                arguments: {
                    inventoryNumber: inventoryNumber,
                    operationAlias: operationAlias,
                    referenceNumber: "N/A"
                }
            }, undefined, callback);
        });
    };

    /**
     * repair inventory bundle
     * @param - {args}
     * @returns {Promise}
     */
    repairInventoryRebundle(args) {
        return new Promise((resolve, reject) => {
            let callback = this.getPromiseCallback(resolve, reject);
            connector.makeSoapCall(connector.apiCrestelInventoryOperation, "repairInventory", {arg0: args}, undefined, callback);
        });
    };

    /**
     * Get inventory details by the arguments
     * @param - {args}
     * @returns {Promise}
     */
    getInventoryDetail(args) {
        return new Promise((resolve, reject) => {
            let callback = this.getPromiseCallback(resolve, reject);
            connector.makeSoapCall(connector.apiCrestelInventoryOperation, "getInventoryDetail", {arguments: args}, undefined, callback);
        });
    };

    /**
     * Get inventory list by the sevice instance number
     * @param args- {serviceInstanceNumber}
     * @returns {Promise}
     */
    listInventory(args) {
        let {serviceInstanceNumber}=args;
        return new Promise((resolve, reject) => {
            let callback = this.getPromiseCallback(resolve, reject);
            connector.makeSoapCall(connector.apiCrestelCAAMInventoryOperationsService, "listInventory", {
                arguments: {
                    serviceInstanceNumber: serviceInstanceNumber
                }
            }, undefined, callback);
        });
    };

    /**
     * Unpair inventory by arguments
     * @param - {args}
     * @returns {Promise}
     */
    unpairInventory(args) {
        return new Promise((resolve, reject) => {
            let callback = this.getPromiseCallback(resolve, reject);
            connector.makeSoapCall(connector.apiCrestelCAAMInventoryOperationsService, "unpairInventory", {arguments: args}, undefined, callback);
        });
    };

    /**
     * Repair inventory by arguments
     * @param - {args}
     * @returns {Promise}
     */
    repairInventory(args) {
        return new Promise((resolve, reject) => {
            let callback = this.getPromiseCallback(resolve, reject);
            connector.makeSoapCall(connector.apiCrestelCAAMInventoryOperationsService, "repairInventory", {arguments: args}, undefined, callback);
        });
    };

    /**
     * repair inventory xml by arguments
     * @param args - {args}
     * @returns {Promise}
     */
    repairInventoryXml(args) {
        return new Promise((resolve, reject) => {
            let callback = this.getPromiseCallback(resolve, reject);
            connector.makeSoapCall(connector.apiCrestelCAAMInventoryOperationsService, "repairInventory", {arguments: args}, undefined, callback);
        });
    };

    /**
     * Remove inventory by service instance number and inventory number
     * @param args - {serviceInstanceNumber, inventoryNumber}
     * @returns {Promise}
     */
    removeInventory(args) {
        let {serviceInstanceNumber, inventoryNumber}=args;
        return new Promise((resolve, reject) => {
            let callback = this.getPromiseCallback(resolve, reject);
            connector.makeSoapCall(connector.apiCrestelCAAMInventoryOperationsService, "removeInventory", {
                arguments: {
                    serviceInstanceNumber: serviceInstanceNumber,
                    inventoryNumber: inventoryNumber
                }
            }, undefined, callback);
        });
    };

    /**
     * Add inventory by effective date, inventory number, subtype, Ismanualinventory, packageid and service instance number
     * @param args - {effectDate, inventoryNumber, serviceInstanceNumber}
     * @returns {Promise}
     */
    addInventory(args) {
        let {effectDate, inventoryNumber, serviceInstanceNumber}=args;
        let ecDate = dateformat(new Date(effectDate ? effectDate : new Date()), "yyyy-mm-dd");
        return new Promise((resolve, reject) => {
            let callback = this.getPromiseCallback(resolve, reject);
            connector.makeSoapCall(connector.apiCrestelCAAMInventoryOperationsService, "addInventory", {
                arguments: {
                    effectDate: ecDate,
                    inventoryNumber: inventoryNumber,
                    inventorySubtype: "MSISDN",
                    isManualInventory: "N",
                    packageId: "PRD00401",    // base plan hard coded for now
                    serviceInstanceNumber: serviceInstanceNumber
                }
            }, undefined, callback);
        });
    };

    /**
     * Mark inventory as damaged by inventory number and serviceInstanceNumber
     * @param args - {inventoryNumber, serviceInstanceNumber}
     * @returns {Promise}
     */
    markInventoryAsDamaged(args) {
        let {inventoryNumber, serviceInstanceNumber}=args;
        return new Promise((resolve, reject) => {
            let callback = this.getPromiseCallback(resolve, reject);
            connector.makeSoapCall(connector.apiCrestelCAAMInventoryOperationsService, "markInventoryAsDamaged", {
                arguments: {
                    inventoryNumber: inventoryNumber,
                    serviceInstanceNumber: serviceInstanceNumber
                }
            }, undefined, callback);
        });
    };

    /**
     * To create Inventory
     * @param args- {msisdn, donorCode, date}
     * @returns {Promise}
     */
    createInventory(args) {
        let {msisdn, donorCode, date}=args;
        let ecDate = dateformat(new Date(date ? date : new Date()), "dd-mm-yyyy");
        return new Promise((resolve, reject) => {
            let callback = this.getPromiseCallback(resolve, reject);
            connector.makeSoapCall(connector.apiLWWSServices, "createInventory", {
                arg0: {
                    arguments: {
                        inventoryGroup: "NGP00003",
                        inventorySubGroup: "NTP00007",
                        warehouseName: "WMAS00000001",
                        batchNumber: "NBT000000265",
                        staffName: "LWUser",
                        MSISDN: msisdn,
                        inventoryAttributes: [{
                            key: "PARAM2",
                            value: donorCode
                        }, {
                            key: "PARAM3",
                            value: "Manual"
                        }, {
                            key: "PARAM4",
                            value: ecDate
                        }]
                    }
                }
            }, undefined, callback);
        });
    };

    /**
     * To update inventory attributes
     * @param args - {msisdn, donorCode, date}
     * @returns {Promise}
     */
    updateInventoryAttributes(args) {
        let {msisdn, donorCode, date}=args;
        let ecDate = dateformat(new Date(date ? date : new Date()), "dd-mm-yyyy");
        return new Promise((resolve, reject) => {
            let callback = this.getPromiseCallback(resolve, reject);
            connector.makeSoapCall(connector.apiLWWSServices, "updateInventoryAttributes", {
                arg0: {
                    arguments: {
                        staffName: "LWUser",
                        MSISDN: msisdn,
                        inventoryAttributes: [{
                            key: "PARAM2",
                            value: donorCode
                        }, {
                            key: "PARAM4",
                            value: ecDate
                        }]
                    }
                }
            }, undefined, callback);
        });
    };

//endregion

//region Account, Service, Instance

    /**
     * Create Account
     * @param args - {args}
     * @returns {Promise}
     */
    createAccount(args) {
        return new Promise((resolve, reject) => {
            let callback = this.getPromiseCallback(resolve, reject);
            connector.makeSoapCall(connector.BRMCustServices, "pcmOpCustCommitCustomer", args, {timeout: 60000}, callback);
        });
    };

    /**
     * Change service plan by account number, effective date, pacakage name, remarks and service instance number
     * @param args - {billingAccountNumber, effectiveDate, packageName, remarks, serviceInstanceNumber}
     * @returns {Promise}
     */
    changeServicePlan(args) {
        let {billingAccountNumber, effectiveDate, packageName, remarks, serviceInstanceNumber}=args;
        return new Promise((resolve, reject) => {
            let callback = this.getPromiseCallback(resolve, reject);
            connector.makeSoapCall(connector.apiCrestelCAAMServiceOperations, "changeServicePlan", {
                arguments: {
                    billingAccountNumber: billingAccountNumber,
                    effectiveDate: effectiveDate,
                    packageName: packageName,
                    remarks: remarks,
                    serviceInstanceNumber: serviceInstanceNumber
                }
            }, undefined, callback);
        });
    };

    /**
     * search account by mobile or MSISDN Number, account number, service username and account name
     * @param args - {number, accountNumber, accountName, serviceUserName}
     * @returns {Promise}
     */
    searchAccount(args) {
        let {number, accountNumber, accountName, serviceUserName} = args;
        return new Promise((resolve, reject) => {
            let callback = this.getPromiseCallback(resolve, reject);
            connector.makeSoapCall(connector.apiCrestelCAAMCustomerAccount2Operations, "searchAccount", {
                arguments: {
                    mobileOrMSISDNNumber: number,
                    accountNumber: accountNumber,
                    accountName: accountName,
                    serviceUserName: serviceUserName
                }
            }, undefined, callback);
        });
    };

    /**
     * search service instance by mobile or MSISDN Number, account number and account name
     * @param args - {number, accountNumber, accountName}
     * @returns {Promise}
     */
    searchServiceInstance(args) {
        let {number, accountNumber, accountName}=args;
        return new Promise((resolve, reject) => {
            let callback = this.getPromiseCallback(resolve, reject);
            connector.makeSoapCall(connector.apiCrestelCAAMCustomerAccount2Operations, "searchAccount", {
                arguments: {
                    mobileOrMSISDNNumber: number,
                    accountNumber: accountNumber,
                    accountName: accountName
                }
            }, undefined, callback);
        });
    };

    /**
     * Get account statement by account number, from and to date
     * @param args - {accountNumber, fromDate, toDate}
     * @returns {Promise}
     */
    getAccountStatement(args) {
        let {accountNumber, fromDate, toDate} = args;
        return new Promise((resolve, reject) => {
            let callback = this.getPromiseCallback(resolve, reject);
            connector.makeSoapCall(connector.apiCrestelCAAMAccountOperation, "getAccountStatement", {
                arguments: {
                    accountNumber: accountNumber,
                    fromDate: fromDate,
                    toDate: toDate
                }
            }, undefined, callback);
        });
    };

    /**
     * Add deposit to customer billing account by billing account number, deposit account and reason
     * @param args - {billingAccountNumber, depositAmount, reason}
     * @returns {Promise}
     */
    addDepositToCustomerBillingAccount(args) {
        let {billingAccountNumber, depositAmount, reason}=args;
        return new Promise((resolve, reject) => {
            let callback = this.getPromiseCallback(resolve, reject);
            connector.makeSoapCall(connector.apiCrestelCAAMAccountOperation, "addDepositToCustomerBillingAccount", {
                arguments: {
                    "$xml": "<staffName>LWUser</staffName>" +
                    "<accountNumber>" + billingAccountNumber + "</accountNumber>" +
                    "<depositAmount>" + depositAmount + "</depositAmount>" +
                    "<depositName>" + reason + "</depositName>" +
                    "<depositType>" + reason + "</depositType>" +
                    "<reason>" + reason + "</reason>"
                }
            }, undefined, callback);
        });
    };

    /**
     * Get Account Details
     * @param args - {billingAccountNumber}
     * @returns {Promise}
     */
    getBillingAccountDetail(args) {
        let {billingAccountNumber}=args;
        return new Promise((resolve, reject) => {
            let callback = this.getPromiseCallback(resolve, reject);
            connector.makeSoapCall(connector.apiCrestelCAAMAccountOperation, "getBillingAccountDetail", {
                arguments: {
                    accountNumber: billingAccountNumber
                }
            }, undefined, callback);
        });
    };

    /**
     * Update customer account data by arguments
     * @param args - {args}
     * @returns {Promise}
     */
    updateCustomerAccountData(args) {
        return new Promise((resolve, reject) => {
            let callback = this.getPromiseCallback(resolve, reject);
            connector.makeSoapCall(connector.apiCrestelCAAMCustomerAccount2Operations, "updateCustomerAccountData", {arguments: args}, undefined, callback);
        });
    };

    /**
     * To Update billing account data
     * @param argguments - {args}
     * @returns {Promise}
     */

    updateBillingAccountData(args) {
        return new Promise((resolve, reject) => {
            let callback = this.getPromiseCallback(resolve, reject);
            connector.makeSoapCall(connector.apiCrestelCAAMCustomerAccount2Operations, "updateBillingAccount", {arguments: args}, undefined, callback);
        });
    };

    /**
     * To Update service account data
     * @param arguments - {args}
     * @returns {Promise}
     */
    updateServiceAccountData(args) {
        return new Promise((resolve, reject) => {
            let callback = this.getPromiseCallback(resolve, reject);
            connector.makeSoapCall(connector.apiCrestelCAAMCustomerAccount2Operations, "updateServiceAccount", {arguments: args}, undefined, callback);
        });
    };

    /**
     * To Update billing account data
     * @param arguments - {args}
     * @returns {Promise}
     */
    updateBillingAccount(args) {
        return new Promise((resolve, reject) => {
            let callback = this.getPromiseCallback(resolve, reject);
            connector.makeSoapCall(connector.apiCrestelCAAMAccount2Operation, "updateBillingAccount", {arguments: args}, undefined, callback);
        });
    };

    /**
     * To Update service account data
     * @param arguments - {args}
     * @returns {Promise}
     */
    updateServiceAccount(args) {
        return new Promise((resolve, reject) => {
            let callback = this.getPromiseCallback(resolve, reject);
            connector.makeSoapCall(connector.apiCrestelCAAMAccount2Operation, "updateServiceAccount", {arguments: args}, undefined, callback);
        });
    };

    /**
     * To change service instance status
     * @param arguments - {args}
     * @returns {Promise}
     */
    changeServiceInstanceStatus(args) {
        return new Promise((resolve, reject) => {
            let callback = this.getPromiseCallback(resolve, reject);
            connector.makeSoapCall(connector.apiCrestelCAAMServiceOperations, "changeServiceInstanceStatus", {arguments: args}, undefined, callback);
        });
    };

//endregion

//region Bill, Payments, Deposits

    /**
     * Search bill by accout number, from and to date
     * @param args - {accountNumber, fromDate, toDate}
     * @returns {Promise}
     */
    searchBill(args) {
        let {accountNumber, fromDate, toDate}=args;
        return new Promise((resolve, reject) => {
            let callback = this.getPromiseCallback(resolve, reject);
            connector.makeSoapCall(connector.apiCrestelBECommon, "searchBill", {
                arguments: {
                    accountNumber: accountNumber,
                    fromDate: fromDate,
                    toDate: toDate
                }
            }, undefined, callback);
        });
    };

    /**
     * Search credit note by arguments
     * @param arguments - {args}
     * @returns {Promise}
     */
    searchCreditNote(args) {
        return new Promise((resolve, reject) => {
            let callback = this.getPromiseCallback(resolve, reject);
            connector.makeSoapCall(connector.apiCrestelPaymentManager, "searchCreditNote", {arguments: args}, undefined, callback);
        });
    };

    /**
     * Make debit payment data by makeDebitPaymentDataRequest
     * @parma args- {accountNumber, transactionAmount, documentNumber, paymentDate, channelAlias}
     * @returns {Promise}
     */
    makeDebitPaymentDataRequest(args) {
        let {accountNumber, transactionAmount, documentNumber, paymentDate, channelAlias}=args;
        return new Promise((resolve, reject) => {
            let callback = this.getPromiseCallback(resolve, reject);
            connector.makeSoapCall(connector.apiCrestelPaymentManager, "makeDebitPayment", {
                makeDebitPaymentDataRequest: {
                    accountCurrencyAlias: "SGD",
                    accountList: {
                        "$xml": "<accountName>" + accountNumber + "</accountName>" +
                        "<accountNumber>" + accountNumber + "</accountNumber>" +
                        "<transactionAmount>" + transactionAmount + "</transactionAmount>" +
                        "<adjustmentList><documentNumber>" + documentNumber + "</documentNumber></adjustmentList>"
                    },
                    description: "Debit Payment",
                    documentAlias: "DEBIT_PAYMENT",
                    paymentCurrencyAlias: "SGD",
                    paymentDate: paymentDate,
                    receivableChannelAlias: channelAlias,
                    receivableChannelName: channelAlias,
                    OnlineMethod: {
                        "$xml": "<paymentMode>" + "ONLINE" + "</paymentMode>" +
                        "<instituteBranchName>" + "singapore" + "</instituteBranchName>" +
                        "<instituteMasterId>" + "1" + "</instituteMasterId>" +
                        "<instituteMasterName>" + "LW_Test_Payment_Institute" + "</instituteMasterName>"
                    },
                    staffCurrencyAlias: "SGD",
                    staffId: "S000103",
                    staffName: "LWUser"
                }
            }, undefined, callback);
        });
    };

    /**
     * Make advance payment by makeAdvancePaymentDataRequest object
     * @param args - {accountNumber, transactionAmount, paymentDate, channelAlias, description}
     * @returns {Promise}
     */
    makeAdvancePaymentDataRequest(args) {

        let {accountNumber, transactionAmount, paymentDate,channelAlias, description}=args;
        return new Promise((resolve, reject) => {
            let callback = this.getPromiseCallback(resolve, reject);
            connector.makeSoapCall(connector.apiCrestelPaymentManager, "makeAdvancePayment", {
                makeAdvancePaymentDataRequest: {
                    accountCurrencyAlias: "SGD",
                    accountList: {
                        "$xml": "<accountName>" + accountNumber + "</accountName>" +
                        "<accountNumber>" + accountNumber + "</accountNumber>" +
                        "<transactionAmount>" + transactionAmount + "</transactionAmount>"
                    },
                    description: description ? description : "Payment",
                    documentAlias: "ADVANCE_PAYMENT",
                    paymentCurrencyAlias: "SGD",
                    paymentDate: paymentDate,
                    receivableChannelAlias: channelAlias,
                    receivableChannelName: channelAlias,
                    OnlineMethod: {
                        "$xml": "<paymentMode>" + "ONLINE" + "</paymentMode>" +
                        "<instituteBranchName>" + "singapore" + "</instituteBranchName>" +
                        "<instituteMasterId>" + "1" + "</instituteMasterId>" +
                        "<instituteMasterName>" + "LW_Test_Payment_Institute" + "</instituteMasterName>"
                    },
                    staffCurrencyAlias: "SGD",
                    staffId: "S000103",
                    staffName: "LWUser"
                }
            }, undefined, callback);
        });
    };

    /**
     * To search payments
     * @param arguments - {args}
     * @returns {Promise}
     */
    searchPayment(args) {
        return new Promise((resolve, reject) => {
            let callback = this.getPromiseCallback(resolve, reject);
            connector.makeSoapCall(connector.apiCrestelPaymentManager, "searchPayment", {searchPaymentData: args}, undefined, callback);
        });
    };

    /**
     * To create Credit note
     * @param arguments - {args}
     * @returns {Promise}
     */
    createCreditNote(args) {
        return new Promise((resolve, reject) => {
            let callback = this.getPromiseCallback(resolve, reject);
            connector.makeSoapCall(connector.apiCrestelPaymentManager, "createCreditNote", {createCreditNoteDataRequest: args}, undefined, callback);
        });
    };


//endregion

//region Other, Customer, Porting, Notifications

    /**
     * Reason codes
     * 1 - Cancellation due to customer request.
     * 2 - Cancellation due to RNO request.
     * 3 - Cancellation due to overdue of Approval Window.
     */
    /**
     * To Cancel PortIn order
     * @param args - {MSISDN, reasonCode}
     * @returns {Promise}
     */
    cancelPortInOrder(args) {
        let {MSISDN, reasonCode}=args;
        return new Promise((resolve, reject) => {
            let callback = this.getPromiseCallback(resolve, reject);
            connector.makeSoapCall(connector.apiLWWSServices, "CancelPortInOrder", {
                arg0: {
                    MSISDN: MSISDN,
                    reasonCode: reasonCode,
                    reasonText: "N/A"
                }
            }, undefined, callback);
        });
    };

    /**
     *  Fake success port in M1 -> EC notifications
     */
    /**
     * For MNP Notification
     * @param args - {MSISDN}
     * @returns {Promise}
     */
    mnpNotification(args) {

        let {MSISDN}=args;
        return new Promise((resolve, reject) => {
            let callback = this.getPromiseCallback(resolve, reject);
            connector.makeSoapCall(connector.apiLWWSServices, "MNPNotification", {
                arg0: {
                    arguments: {
                        MSISDN: MSISDN,
                        notificationType: "PortIn_Success",
                        staffName: "M1User",
                        statusCode: "33",
                        statusMessage: "N/A"
                    }
                }
            }, undefined, callback);
        });
    };

    /**
     * For Service provisioning notification
     * @param args - {externalRefId, inventoryNumber}
     * @returns {Promise}
     */
    serviceProvisioningNotification(args) {

        let {externalRefId, inventoryNumber}=args;

        if (!inventoryNumber) inventoryNumber = "0";
        return new Promise((resolve, reject) => {
            let callback = this.getPromiseCallback(resolve, reject);
            connector.makeSoapCall(connector.apiLWWSServices, "ServiceProvisioningNotification", {
                arg0: {
                    arguments: {
                        externalRefId: externalRefId,
                        inventoryNumber: inventoryNumber,
                        inventoryStatus: "N/A",
                        reason: "N/A",
                        staffName: "M1User",
                        status: "0"
                    }, params: {
                        param: {
                            key: "Notification_Type",
                            value: "HLRCreateSubscriber_Success"
                        }
                    }
                }
            }, undefined, callback);
        });
    };

    /**
     * To view customer usage information
     * @param args - {args}
     */
    viewCustomer(args) {
        let {serviceInstanceNumber, fromDate}=args;
        return new Promise((resolve, reject) => {
            let callback = this.getPromiseCallback(resolve, reject);
            connector.makeSoapCall(connector.apiIntegrationWebService, "viewCustomer", {
                arg1: {
                    subscriberIdentifier: serviceInstanceNumber,
                    fromDate: fromDate
                }
            }, undefined, callback);
        });
    };

    /**
     * To view customer usage information
     * @param arguments - {args}
     */
    viewCustomerUsageInformation(args) {
        return new Promise((resolve, reject) => {
            let callback = this.getPromiseCallback(resolve, reject);
            connector.makeSoapCall(connector.apiIntegrationWebService, "viewCustomerUsageInformation", {
                arg0: '2',
                arg1: args
            }, undefined, callback);
        });
    };

    /**
     * To update customer bucket
     * @param arguments - {args}
     * @returns {Promise}
     */
    updateCustomerBucket(args) {
        return new Promise((resolve, reject) => {
            let callback = this.getPromiseCallback(resolve, reject);
            connector.makeSoapCall(connector.apiIntegrationWebService, "updateCustomer", {
                arg0: 4,
                arg1: args
            }, undefined, callback);
        });
    };

//endregion

    ///GET PROMISE METHOD GOES HERE

}

const soapClient = new SoapClient();
module.exports = soapClient;