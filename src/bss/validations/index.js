/*globals require, module */

//This is mainly used to validate the payload
const validator = require('./validator');

class BSSValidator {
    constructor() {

    }

    /**
     * Here we will be validating the args provided for updateBillingAccount
     * @param args
     *
        {
            "accountNumber": "LW100274",
            "addressDetails":
            {
             "addressOne": "2113, Henderson road, #6-10",
             "addressTwo": "Henderson road;6;10;2113",
             "zipPostalCode": 510123
            },
            "accountProfiles": {
                  "strcustom1": "20140916001-03112017-1509679237631", //$recurrent_token,
                  "strcustom2": "20140916001", //$sub_mid,
                  "strcustom3": "None", //$bank, customer bank name
                  "strcustom4": "visa", //$paytype,
                  "strcustom5": "1111", //$cc_last_4_digit,
                  "strcustom11": "dbs", //$card_bank_name
              }
        }
     * @returns {Promise}
     */
    updateBillingAccount(args){
        let validations = [];

        validations.push({
            key:'addressDetails and accountProfiles',
            action:validator.is_atleast_one_is_not_undefined,
            params:[args.addressDetails,args.accountProfiles]
        });

        validations.push({
            key:'accountNumber',
            action:validator.is_not_empty,
            params:[args.accountNumber]
        });

        return this.validateAllPromise(validations);
    }

    /**
     * Here we will be validatingh the args provided for subscribeaAddOnPackage
     * @param args
        {
    /*     "addOnPlans": [{"packageId": $package}, ...], */
     /*    "serviceInstanceNumber": $service_instance_number
       }
     * @returns {Promise}
     */
    subscribeAddOnPackage(args){
        let validations = [];

        validations.push({
            key:'serviceInstanceNumber',
            action:validator.is_not_empty,
            params:[args.serviceInstanceNumber]
        });

        validations.push({
            key:'addOnPlans',
            action:validator.is_having_min_length,
            params:[args.addOnPlans,1]
        });

        args.addOnPlans.forEach((item,index,array)=>{
            validations.push({
                key:`packageId - ${index+1} of addOnPlans`,
                action:validator.is_not_empty,
                params:[item.packageId]
            });
        });

        return this.validateAllPromise(validations);
    }

    /**
     * this function will return a promise which will resolve true if all the validations are passed or
     * reject an error if any validation is failed
     * @param validations
     * @returns {Promise}
     */
    validateAllPromise(validations){
        return new Promise((resolve,reject)=>{
            try{
                for(let key in validations){
                    let item = validations[key];
                    let validationResult = item.action(...item.params);
                    if( validationResult!== true){
                        let message = `Validation '${validator.getActionName(item.action)}' failed for '${item.key}'`;
                        return reject(new Error(message));
                    }
                }
                resolve(true);
            }catch(ex){
                reject(ex);
            }
        });
    }
}

let bss_validator = new BSSValidator();
module.exports = bss_validator;