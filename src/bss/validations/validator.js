/*globals require, module */

//This is mainly used to validate the payload
class Validator {
    constructor() {
        this.is_not_empty = this.is_not_empty.bind(this);
        this.is_atleast_one_is_not_undefined = this.is_atleast_one_is_not_undefined.bind(this);
        this.is_having_min_length = this.is_having_min_length.bind(this);
        this.is_not_exceeding_max_length = this.is_not_exceeding_max_length.bind(this);
        this.is_having_length = this.is_having_length.bind(this);
        this.is_number = this.is_number.bind(this);

        this.getActionName = (action)=>{
            return action.name.replace("bound ","");
        };

        this.getLength = (p)=>{
            if(p.constructor.name === "Array"){
                return p.length;
            }
            if(p.constructor.name === "Object"){
                return Object.keys(p).length;
            }
            let x = p+'';
            return x.length;
        }
    }

    /**
     * to check atleast one of the arguments you pass is not undefined
     * @returns {boolean}
     */
    is_atleast_one_is_not_undefined(){
        let allArgumentsAreUndefined = true;
        for (let i = 0; i < arguments.length; i++) {
            if(arguments[i]){
                allArgumentsAreUndefined = false;
                break;
            }
        }
        return !allArgumentsAreUndefined;
    }

    /**
     * to check if the parameter value is not empty
     * @param p
     * @returns {boolean}
     */
    is_not_empty(p){
        let isNotEmpty = false;
        if(p !== undefined){
            if(p !== null){
                let x = p+'';
                if(x.length > 0) isNotEmpty = true;
            }
        }
        return isNotEmpty;
    }

    /**
     * to check if the value is having min length.
     * @param p
     * @param length
     * @returns {boolean}
     */
    is_having_min_length(p,length){
        if(length === undefined) length = 0;
        let currentLength = this.getLength(p);
        return currentLength >= length;
    }

    /**
     * to check if the value having length that is not exceeding the maxlength
     * @param p
     * @param maxLength
     * @returns {boolean}
     */
    is_not_exceeding_max_length(p,maxLength){
        if(maxLength === undefined) maxLength = 0;
        let currentLength = this.getLength(p);
        return currentLength <= maxLength;
    }

    /**
     * to check if the value is having the same length as passed in the second argument
     * @param p
     * @param length
     * @returns {boolean}
     */

    is_having_length(p,length){
        if(length === undefined) length = 0;
        let currentLength = this.getLength(p);
        return currentLength === length;
    }

    /**
     * to check if the value is a number
     * @param p
     * @returns {boolean}
     */
    is_number(p){
        if(this.is_not_empty(p)) {
            return !isNaN(p);
        }
        return false;
    }

}

let validator = new Validator();
module.exports = validator;