var config = require('../../config');
var CIRCLES_ONE_ADDON_SUBSCRIPTION_LIST_PRODUCTION = [
    'PRD00316', // Plus 4G Speed Max
    'PRD00317', // Plus Plan Change
    'PRD00315', // Plus Roaming
    'PRD00440', // Whatsapp_Unlimited
    'PRD00841'  // IMESSAGE_ADDON
];
var CIRCLES_ONE_ADDON_SUBSCRIPTION_LIST_DEV = [
    'PRD00316', // Plus 4G Speed Max
    'PRD00317', // Plus Plan Change
    'PRD00315', // Plus Roaming
    'PRD00440', // Whatsapp_Unlimited
    'PRD00756'  // IMESSAGE_ADDON
];

exports.CHARGES = {
    PAY200_ADDON_PRICE: 1.33
};

exports.TIMEOUTS = {
    WHATSAPP_PENALTY_LOCK_TIMEOUT : config.DEV ? 1000 : 1800000
}

exports.THRESHOLDS = {
    ECTIMEOUTS : [ 0, 100, 200, 300, 400, 500, 600, 700, 800, 900 ],    // always 10 items and start with 0
    ECTIMEOUTSWINDOW : 3 * 60000,    // always > 2 * ECTIMEOUTSINTERVAL
    ECTIMEOUTSINTERVAL : 60000,
}

exports.NOTIFICATION_RESEND_WINDOW = 120000;

exports.NOTIFICATION_RESEND_BLOCK_BLACKLIST = [
    'email_changed_new',
    'email_changed_old',
    'cap_roaming_data',
    'autoboost_added',
    'boost_added',
    'roaming_voice_sms_data_on','roaming_voice_sms_on',
    'address_details_changed',
    'app_credit_card_change',
    'activate_app',
    'plus_added_immediate',
    'plus_removed_immediate',
    'selfcare_calls_changed',
    'selfcare_data_changed',
    'selfcare_sms_changed',
    'failure_alert_common',
    'crash_alert_common',
    'usage_consumption_data',
    'generic_report',
    'customer_activation_report',
    'delivery_info',
    'payment_unsuccessful',
    'freeboost_added',
    'plus_removed_next_cycle',
    'plus_added_immediate',
    'plus_added_next_cycle',
    'status_changed',
    'bill_resend',
    'autoboost_payment_success',
    'bill_payment_success_after_failure',
    'forgot_password_email',
    'pay200_failure',
    'portin_otp'
];

exports.ICCID_REPLACEMENT_INVENTORY_THRESHOLD = 200;
exports.SCHEDULER_LONG_RUNNING_ACTIONS = ['report', 'biRun'];

exports.ICCID_REPLACEMENT_INVENTORY_ALERT_START_THRESHOLD = 200;

exports.SCHEDULER_PENDING_HOUR_GAP = 2;
exports.SCHEDULER_PICKED_UP_HOUR_GAP = 2;

exports.TEMPLATE_NAME_VARIABLES = ['name', 'joined_user_name', 'street_building_name', 'last_name', 'customer_name', 'first_name', 'addonname'];
exports.SCHEDULER_PENDING_FROM = 1501084800000;
exports.SCHEDULER_NOT_PICKED_UP_FROM = 1501084800000;

exports.MAX_PAGE_ZENDESK = 6;
exports.DFF_ADDON_NAME = 'Data Freeflow';
exports.DATA_UNTHROTTLE_ADDON_NAME = 'DATA_UNTHROTTLE';
exports.PM2_BLACKLIST_MODULES = ['pm2-logrotate'];
exports.CUSTOMER_CACHE_TEST_NUMBER = config.DEV ? '87420412' : '87421330';

exports.CIRCLES_SWITCH_REMINDER_TIMESTAMP_21_DAYS_LEFT = '2017-12-09 03:00:00';
exports.CIRCLES_SWITCH_REMINDER_TIMESTAMP_1_DAYS_LEFT = '2017-12-31 03:00:00';
exports.CIRCLES_SWITCH_ADDON_WHITELIST = config.DEV ? ['PRD00445', 'PRD00316', 'PRD00753', 'PRD00315', 'PRD00589'] : ['PRD00445', 'PRD00316', 'PRD00800', 'PRD00650', 'PRD00649'];
exports.CIRCLES_SWITCH_REMINDER_MAX_DATE = '2017-12-14 03:00:00';
exports.RESTRICTED_ADDONS_FOR_UNLIMITED_DATA_USERS = config.DEV ? ['PRD00678', 'PRD00753'] : ['PRD00800', 'PRD00746'];
exports.DFF_ADDON_ID = config.DEV ? 'PRD00607' : 'PRD00711';
exports.PAID_2020_ADDON_ID = config.DEV ? 'PRD00678' : 'PRD00746';
exports.FREE_2020_ADDON_ID = config.DEV ? 'PRD00753' : 'PRD00800';
exports.CIRCLES_SWITCH_PLAN_ID = config.DEV ? 'PRD00773' : 'PRD00850';
exports.OVERLAPPING_RESTRICTEDADDON_IDS = config.DEV ? ['PRD00678', 'PRD00753', 'PRD00607'] : ['PRD00800', 'PRD00746', 'PRD00711'];
exports.VOICEMAIL = { numbers: ['6587421330', '6588086868'] };
exports.TERMINATION_WINBACK_PROMO_TAG = 'TERMINATION_WINBACK_2020_PROMO';
exports.CIRCLES_ONE_ADDON_SUBSCRIPTION_LIST = config.DEV ? CIRCLES_ONE_ADDON_SUBSCRIPTION_LIST_DEV : CIRCLES_ONE_ADDON_SUBSCRIPTION_LIST_PRODUCTION;
exports.CIRCLES_SWITCH_BLOCKED_NOTIFICATIONS = ['autoboost_10_added',
'autoboost_20_warning_billing',
'autoboost_25_warning_billing',
'autoboost_30_added',
'autoboost_50_added',
'autoboost_added',
'autoboost_added_last',
'autoboost_disabled',
'autoboost_payment_failure',
'autoboost_payment_success',
'autoboost_warning',
'bill_amount_error',
'bill_credit_notice',
'bill_failure_reactivation',
'bill_failure_suspension',
'bill_failure_suspension_day_before',
'bill_failure_termination',
'bill_new_format',
'bill_payment_failure',
'bill_payment_failure_first_bill',
'bill_payment_failure_first_bill_no_reason',
'bill_payment_failure_no_reason',
'bill_payment_failure_reminder_1',
'bill_payment_failure_reminder_1_no_reason',
'bill_payment_failure_reminder_2',
'bill_payment_failure_reminder_2_no_reason',
'bill_payment_failure_reminder_strong',
'bill_payment_failure_reminder_strong_no_reason',
'bill_payment_late',
'bill_payment_late_reminder_1',
'bill_payment_late_reminder_2',
'bill_payment_partially',
'bill_payment_success',
'bill_payment_success_after_failure',
'bill_payment_success_first_bill',
'bill_resend',
'bill_upcoming',
'autoboost_10_added',
'autoboost_20_warning_billing',
'autoboost_25_warning_billing',
'autoboost_30_added',
'autoboost_50_added',
'autoboost_added',
'autoboost_added_last',
'autoboost_disabled',
'autoboost_payment_failure',
'autoboost_payment_success',
'autoboost_warning',
'auto_approve_order_failed',
'bill_amount_error',
'bill_credit_notice',
'bill_failure_reactivation',
'bill_failure_suspension',
'bill_failure_suspension_day_before',
'bill_failure_termination',
'bill_new_format',
'bill_payment_failure',
'bill_payment_failure_first_bill',
'bill_payment_failure_first_bill_no_reason',
'bill_payment_failure_no_reason',
'bill_payment_failure_reminder_1',
'bill_payment_failure_reminder_1_no_reason',
'bill_payment_failure_reminder_2',
'bill_payment_failure_reminder_2_no_reason',
'bill_payment_failure_reminder_strong',
'bill_payment_failure_reminder_strong_no_reason',
'bill_payment_late',
'bill_payment_late_reminder_1',
'bill_payment_late_reminder_2',
'bill_payment_partially',
'bill_payment_success',
'bill_payment_success_after_failure',
'bill_payment_success_first_bill',
'bill_resend',
'bill_upcoming'
];

exports.BOOSTS_IMAGE_URLS = {
    'Boost 1GB': 'https://s3-ap-southeast-1.amazonaws.com/kirk.circles.asia/assets/PRD00828/boost_1gb.png',
    'Boost 2GB': 'https://s3-ap-southeast-1.amazonaws.com/kirk.circles.asia/assets/PRD00839/boost_2gb.png',
    'Boost 500': 'https://s3-ap-southeast-1.amazonaws.com/kirk.circles.asia/assets/PRD00403/boost_500mb.png'
}

exports.MOBILE_ACTIONS = {
    ACTION_TYPE_TYPE_BOOST: 'BOOST',
    ACTION_TYPE_TYPE_PLAN: 'PLAN',
    ACTION_TYPE_TYPE_PLUS: 'PLUS',
    JOB_TYPE_BOOST: 'QUEUE_ACTION_BOOST',
    JOB_TYPE_PLUS_MODIFICATION: 'QUEUE_ACTION_PLUS_MODIFICATION',
    JOB_TYPE_PLAN_MODIFICATION: 'QUEUE_ACTION_PLAN_MODIFICATION',
}
