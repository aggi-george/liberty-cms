'use strict';

module.exports = function BaseError(message, status, metadata) {
    Error.captureStackTrace(this, this.constructor);
    this.name = this.constructor.name;
    this.message = message;
    this.status = status;
    this.metadata = metadata ? metadata : {};
};

require('util').inherits(module.exports, Error);