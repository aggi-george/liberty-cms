var request = require('request');
var async = require('async');

var config = require('../../../../config');
var common = require('../../../../lib/common');
var db = require('../../../../lib/db_handler');
var ec = require('../../../../lib/elitecore');
var elitecoreBillService = require('../../../../lib/manager/ec/elitecoreBillService');
var elitecoreConnector = require('../../../../lib/manager/ec/elitecoreConnector');
var paas = require('../../../../lib/paas');

var elitecoreUserService = require('../../../../lib/manager/ec/elitecoreUserService');
var elitecorePackagesService = require('../../../../lib/manager/ec/elitecorePackagesService');
var elitecoreUtils = require('../../../../lib/manager/ec/elitecoreUtils');
var resourceManager = require('../../../../res/resourceManager.js');
var ecommManager = require('../ecomm/ecommManager');
var transactionManager = require('../account/transactionManager');

const actions = require('./actions');
var billManager = require('./billManager');
var invoiceManager = require('./invoiceManager');
var notificationSend = require('../notifications/send');
var profileManager = require('../profile/profileManager');
var portInManager = require('../porting/portInManager');
var referralManager = require('../promotions/referralManager');
var packageManager = require('../packages/packageManager');
var promotionsManager = require('../promotions/promotionsManager');
var schedulerManager = require('../scheduler/schedulerManager');
var betaUsersManager = require('../beta/betaUsersManager');

const Bluebird = require('bluebird');
const moment = require('moment');
var BaseError = require('../../errors/baseError');

var hlrManager = require('../hlr/hlrManager');

var LOG = config.LOGSENABLED;
var SG_PREFIX = "65";

var NUMBER_TYPES = {
    GENERAL: "N",
    GOLDEN: "G",
    SILVER: "S",
    PLATINUM: "P"
};

var CURRENCY = {
    SG: "SGD"
}

var DONOR_TYPES = {
    M1: "001",
    STARHUB: "002",
    SINGTEL: "003"
}

var IC_TYPES = {
    NRIC: "1",
    FIN: "4"
};

var ACCOUNT_TYPES = {
    GENERAL: "CirclesOne",
    TRIAL: "CirclesTrial",
    DEFAULT: "CirclesOne",
    CIRCLES_SWITCH: "CirclesSwitch"
};

var REGISTRATION_TYPES = {
    8: "Registration 8",
    10: "Registration 10",
    18: "Registration 18",
    28: "Registration 28",
    38: "Registration 38"
};

var INVENTORY_STATUS_MAPPING = {
    'GENERATED': 'NBS09',
    'BLOCK': 'NBS10',
    'AVAILABLE': 'NBS01',
    'RESERVED': 'NBS02',
    'IN-USE': 'NBS03',
    'RELEASED': 'NBS04',
    'DAMAGED': 'NBS05',
    'SCRAPPED': 'NBS06',
    'VOID': 'NBS07',
    'PAIRED': 'NBS08'
}

var INVENTORY_TYPE_MAPPING = {
    'MOBILE_HANDSET': 'NTP00008',
    'IMSI': 'NTP00005',
    'SIM': 'NTP00006',
    'MSISDN': 'NTP00007',
    'CAF': 'NTP00001',
    'MODEM': 'NTP00002',
    'DONGLE': 'NTP00003',
    'IP_SERIES': 'NTP00004'
}

module.exports = {

    /**
     *
     */
    createAccount: function (location, planName, accountInformation, orderInformation, paymentGateway,
                             personalInformation, advancedPayments, callback) {

        var handleResponse = function (err, result) {
            if (err) common.error("AccountManager", "createAccount: error=" + err.message);

            var obj = {
                type: "CREATE_ACCOUNT",
                status: (err ? (err.status ? err.status : "ERROR") : "OK"),
                errorMessage: (err ? err.message : undefined),
                params: {
                    id: location,
                    accountInformation: accountInformation,
                    orderInformation: orderInformation,
                    paymentGateway: paymentGateway,
                    personalInformation: personalInformation
                },
                result: result,
                number: accountInformation.number ? accountInformation.number : accountInformation.temporaryNumber,
                portinNumber: accountInformation.portingNumber,
                customerAccountNumber: (result ? result.customerAccountId : undefined),
                serviceInstanceNumber: (result ? result.serviceInstanceId : undefined),
                fqdn: config.MYFQDN,
                ts: new Date().getTime()
            };

            db.notifications_account.insert(obj, function (errLog) {
                if (errLog) common.error("AccountManager", "createAccount: save log error, error=" + errLog.message);
                if (callback) callback(err, result);
            });
        }

        if (!accountInformation || !orderInformation || !paymentGateway || !personalInformation) {
            return handleResponse(new BaseError("Some mandatory objects (accountInformation, " +
            "orderInformation, paymentGateway, personalInformation) are missing", "INVALID_PARAMS"));
        }

        if (location !== "SG") {
            return handleResponse(new BaseError("Location " + location + " is not supported (SG only)", "INVALID_PARAMS"));
        }

        var args = {
            billingAccountVO: {
                addressDetails: {},
                contactDetails: {},
                accountProfiles: {},
                billCycleName: "Default",
                billDeliveryModeName: "E-Mail",
                billFormatType: "Summary",
                currencyName: CURRENCY[location]
            },
            customerAccountVO: {
                addressDetails: {},
                contactDetails: {},
                accountProfiles: {
                    strcustom4: "Auto_Topup"
                },
                billingAreaName: "Default",
                categoryName: "Default"
            },
            serviceAccountVO: {
                addressDetails: {},
                contactDetails: {},
                accountProfiles: {}
            },
            serviceInstanceVO: {
                packageName: "CirclesRegistration",
                internalDemographicRequestVOs: {
                    category: "NUMBER_CATEGORY"
                }
            }
        }

        // save payment gateway

        if (paymentGateway.provider === "wirecard") {
            if (!paymentGateway.token || !paymentGateway.mid || !paymentGateway.bank
                || !paymentGateway.payType || !paymentGateway.lastDigits) {
                return handleResponse(new BaseError("Some mandatory Wirecard params (token, mid, bank, payType, lastDigits) are missing",
                    "ERROR_INVALID_PARAMS"));
            }
            args.billingAccountVO.accountProfiles.strcustom1 = paymentGateway.token;
            args.billingAccountVO.accountProfiles.strcustom2 = paymentGateway.mid;
            args.billingAccountVO.accountProfiles.strcustom3 = paymentGateway.bank;
            args.billingAccountVO.accountProfiles.strcustom4 = paymentGateway.payType;
            args.billingAccountVO.accountProfiles.strcustom5 = paymentGateway.lastDigits;
            args.billingAccountVO.accountProfiles.strcustom11 = paymentGateway.cardBankName;

        } else {
            return handleResponse(new BaseError("Payment Gateway is not supported", "ERROR_INVALID_PAYMENT_GATEWAY"));
        }

        // save order information

        if (!orderInformation.no) {
            return handleResponse(new BaseError("Order no. is missing", "ERROR_INVALID_PARAMS"));
        }

        args.serviceAccountVO.accountProfiles = {
            strcustom1: orderInformation.referralCode,
            strcustom7: orderInformation.no
        }

        var checkoutDate;
        if (orderInformation.checkoutDate && new Date(orderInformation.checkoutDate).getTime() > 0) {
            checkoutDate = new Date(orderInformation.checkoutDate);
            var checkoutDateString = common.getLocalTime(checkoutDate).toISOString();
            args.customerAccountVO.accountProfiles.datecustom6 = checkoutDateString;
        }

        // save personal information (address)

        var bAddress = personalInformation.billingAddress;
        if (!bAddress || !bAddress.street || !bAddress.city || !bAddress.country || !bAddress.zip) {
            return handleResponse(new BaseError("Some mandatory billing address params (street, city, country, zip) are missing",
                "ERROR_INVALID_PARAMS"));
        }

        var formattedUnitFloor = "";
        if (bAddress.floor && bAddress.unit) {
            formattedUnitFloor = "#" + bAddress.floor + "-" + bAddress.unit;
        } else if (bAddress.floor) {
            formattedUnitFloor = "Floor " + bAddress.floor;
        } else if (bAddress.unit) {
            formattedUnitFloor = "Unit " + bAddress.unit;
        }

        var formattedAddress = (bAddress.buildingNo ? bAddress.buildingNo + ", " : "")
            + bAddress.street + (formattedUnitFloor ? " " + formattedUnitFloor : "");
        var formattedAddressInternal = bAddress.street + ";" + (bAddress.floor ? bAddress.floor : "") + ";"
            + (bAddress.unit ? bAddress.unit : "") + ";" + (bAddress.buildingNo ? bAddress.buildingNo : "");

        var addressDetails = {
            addressOne: formattedAddress,
            addressTwo: formattedAddressInternal,
            city: bAddress.city,
            country: bAddress.country,
            state: bAddress.state,
            zipPostalCode: bAddress.zip
        }
        args.billingAccountVO.addressDetails = addressDetails;
        args.customerAccountVO.addressDetails = addressDetails;
        args.serviceAccountVO.addressDetails = addressDetails;

        // save personal information (name)

        args.billingAccountVO.firstName = personalInformation.firstName;
        args.billingAccountVO.lastName = personalInformation.lastName;
        args.customerAccountVO.firstName = personalInformation.firstName;
        args.customerAccountVO.lastName = personalInformation.lastName;
        args.serviceAccountVO.firstName = personalInformation.firstName;
        args.serviceAccountVO.lastName = personalInformation.lastName;

        // save personal information (IC)

        if (!personalInformation.ic || !personalInformation.icType) {
            return handleResponse(new BaseError("Some IC params (ic, icType) are missing", "ERROR_INVALID_PARAMS"));
        } else if (!IC_TYPES[personalInformation.icType]) {
            return handleResponse(new BaseError("IC type is not supported " + JSON.stringify(IC_TYPES), "ERROR_INVALID_PARAMS"));
        }
        args.customerAccountVO.accountProfiles.strcustom1 = IC_TYPES[personalInformation.icType];
        args.customerAccountVO.accountProfiles.strcustom2 = personalInformation.ic;

        // save personal information (dob)

        if (!personalInformation.dateOfBirth) {
            return handleResponse(new BaseError("Date of birth is missing", "ERROR_INVALID_PARAMS"));
        }
        args.customerAccountVO.accountProfiles.datecustom1 = personalInformation.dateOfBirth;

        // save personal information (email)

        if (!personalInformation.email) {
            return handleResponse(new BaseError("Email is missing", "ERROR_INVALID_PARAMS"));
        }
        var contactDetails = {
            emailId: personalInformation.email
        };
        args.billingAccountVO.contactDetails = contactDetails;
        args.customerAccountVO.contactDetails = contactDetails;
        args.serviceAccountVO.contactDetails = contactDetails;

        // save account information

        if (!NUMBER_TYPES[accountInformation.category]) {
            return handleResponse(new BaseError("Number category is not supported "
            + JSON.stringify(NUMBER_TYPES), "ERROR_INVALID_NUMBER_TYPE"));
        }
        args.serviceInstanceVO.internalDemographicRequestVOs.strValue = NUMBER_TYPES[accountInformation.category];

        if (accountInformation.portin) {

            if (!accountInformation.temporaryNumber || !accountInformation.portingNumber
                || !accountInformation.portingDonor || (!accountInformation.portingDate && planName != 'CIRCLES_SWITCH')) {
                return handleResponse(new BaseError("Some mandatory account fields (temporaryNumber, portingNumber, " +
                "portingDonor, portingDate) are missing", "ERROR_INVALID_PARAMS"));
            } else if (!DONOR_TYPES[accountInformation.portingDonor]) {
                return handleResponse(new BaseError("Donor is not supported "
                + JSON.stringify(DONOR_TYPES), "ERROR_INVALID_DONOR_NAME"));
            }

            args.customerAccountVO.accountProfiles.strcustom3 = "Normal";
            args.customerAccountVO.accountProfiles.strcustom6 = "not-applicable";
            args.serviceInstanceVO.inventoryRequestVO = {
                inventoryNSI: accountInformation.temporaryNumber,
                inventoryNumber: accountInformation.temporaryNumber,
                isManualInventory: "N",
                inventorySubtype: "MSISDN",
                isResourceInventory: "Y"
            }
        } else {
            if (planName == 'CIRCLES_SWITCH') {
                return handleResponse(new BaseError("CirclesSwitch plan requires port-in number",
                    "ERROR_INVALID_PARAMS"));
            }
            if (!accountInformation.number) {
                return handleResponse(new BaseError("Some mandatory account fields (number) are missing",
                    "ERROR_INVALID_PARAMS"));
            }

            args.customerAccountVO.accountProfiles.strcustom3 = "Normal";
            args.customerAccountVO.accountProfiles.strcustom6 = "not-applicable";
            args.serviceInstanceVO.inventoryRequestVO = {
                inventoryNSI: accountInformation.number,
                inventoryNumber: accountInformation.number,
                isManualInventory: "N",
                inventorySubtype: "MSISDN",
                isResourceInventory: "Y"
            }
        }

        //console.log(JSON.stringify(args, null, 3));
        ec.createAccount(args, function (err, result) {
            if (err) {
                return handleResponse(new BaseError(err.message, "ERROR_BSS"));
            }

            //result = {
            //    "return": {
            //        "responseCode": "0",
            //        "accountNumber": "LW210357",
            //        "billingAccountNumber": "LW210358",
            //        "serviceAccountNumber": "LW210359",
            //        "serviceInstanceNumber": "LW210360",
            //        "userName": "LW210360"
            //    }
            //}

            if (!result || !result.return) {
                return handleResponse(new BaseError("Empty BSS response", "ERROR_BSS_INVALID_RESPONSE"));
            }

            if (result.return.responseCode !== "0") {
                return handleResponse(new BaseError(result.return.responseMessage + " (" + result.return.responseCode + ")",
                    result.return.responseCode === "-101110048" ? "ERROR_BSS_INVENTORY_NUMBER_TAKEN" : "ERROR_BSS_UNKNOWN"));
            }

            var billingAccountNumber = result.return.billingAccountNumber;
            var accountResponse = {
                customerAccountId: result.return.accountNumber,
                billingAccountId: result.return.billingAccountNumber,
                serviceAccountId: result.return.serviceAccountNumber,
                serviceInstanceId: result.return.serviceInstanceNumber
            };

            var createPortInRequest = (callback) => {

                // date has format which is supported by EC, however,
                // it is invalid and can not be parsed properly but is invalid

                var startDate;
                var validDate;

                if (accountInformation.portingDate) {
                    var chunks = accountInformation.portingDate.split('-');
                    if (chunks.length == 3) {
                        var formattedDate = chunks[2] + '-' + chunks[1] + '-' + chunks[0];
                        startDate = new Date(new Date(formattedDate).setHours(0, 30, 0, 0));
                    }

                    if (!startDate || !startDate.getTime()) {
                        var errorMessage = "Failed to create a port-in request, invalid date, " + accountInformation.portingDate;
                        accountResponse.portin.error = errorMessage;
                        accountResponse.portin.status = "ERROR_INVALID_PORT_IN_DATE_FORMAT";
                        return callback();
                    }

                    var validDates = portInManager.getAvailablePortInDates(SG_PREFIX, startDate);
                    if (!validDates || !validDates[0] || !validDates[0].date) {
                        var errorMessage = "No valid port-in date found, " + accountInformation.portingDate;
                        accountResponse.portin.error = errorMessage;
                        accountResponse.portin.status = "ERROR_NO_AVAILABLE_DATE";
                        return callback();
                    }

                    validDate = new Date(validDates[0].date);
                }

                var prefix = SG_PREFIX;
                var tempNumber = accountInformation.temporaryNumber;
                var portNumber = accountInformation.portingNumber;
                var donorCode = DONOR_TYPES[accountInformation.portingDonor];
                var platform = "ECOMM";
                var inactivePortIn = planName == 'CIRCLES_SWITCH';

                portInManager.putRequest(prefix, tempNumber, prefix, portNumber, donorCode, validDate, platform, {
                    initiator: "[ECOMM][INTERNAL]",
                    orderReferenceNumber: orderInformation.no,
                    checkoutDate: checkoutDate,
                    inactivePortIn: inactivePortIn,
                    planName: planName
                }, (err, result) => {
                    accountResponse.portin = {
                        error: err ? err.message : undefined,
                        status: err ? (err.status ? err.status : "ERROR") : (result.requestId ? "CREATED" : "ERROR"),
                        requestId: result ? result.requestId : undefined,
                        startDate: {
                            original: startDate ? startDate.toISOString() : undefined,
                            valid: validDate ? validDate.toISOString() : undefined
                        }
                    };

                    return callback();
                });
            }

            var saveAdvancedPayments = (paymentDetails, callback) => {
                if (!paymentDetails || !paymentDetails.amount) {
                    accountResponse.advancedPayments.push({
                        params: paymentDetails,
                        error: "Invalid Params",
                        status: "ERROR_INVALID_PARAMS"
                    });
                    return callback();
                }

                elitecoreConnector.getPromise(elitecoreConnector.makeAdvancePaymentDataRequest, {
                    accountNumber: billingAccountNumber,
                    transactionAmount: paymentDetails.amount * 100,
                    paymentDate: new Date().toISOString().split('T')[0],
                    channelAlias: 'AccountManager',
                    description: paymentDetails.description,
                    validResponses: [
                        {keys: ['receivableAccountData', '$array0']}
                    ]
                }).then((result) => {
                    accountResponse.advancedPayments.push({
                        params: paymentDetails,
                        result: result
                    });
                    callback();
                }).catch((err) => {
                    accountResponse.advancedPayments.push({
                        params: paymentDetails,
                        error: err.message,
                        status: err.status
                    });
                    callback();
                });
            }

            // timeout is required for ec to finish customer creation, in some cases without
            // timeout customer can not be found which may result in an inability to create a new port-in request

            setTimeout(() => {

                var tasks = [];

                // add advanced payments posting tasks
                accountResponse.advancedPayments = [];
                if (advancedPayments && advancedPayments.length > 0) {
                    advancedPayments.forEach((item) => {
                        tasks.push({type: "ADVANCED_PAYMENT", params: item});
                    });
                }

                // add port-in request creation task
                accountResponse.portin = {};
                if (accountInformation.portin) {
                    accountResponse.portin.status = "IN_PROGRESS";
                    tasks.push({type: "CREATE_PORT_IN_REQUEST"});
                } else {
                    accountResponse.portin.status = "NONE";
                }

                async.mapSeries(tasks, (task, callback) => {
                    if (task.type == "CREATE_PORT_IN_REQUEST") {
                        createPortInRequest(callback);
                    } else if (task.type == "ADVANCED_PAYMENT") {
                        saveAdvancedPayments(task.params, callback);
                    } else {
                        callback();
                    }
                }, () => {

                    // no need to handle any error, transaction should be completed,
                    // errors should be parsed by eComm, in case of any errors we have
                    // to send an internal notification and correct failed operations manually

                    handleResponse(undefined, accountResponse);
                });

            }, 2000);
        });
    },

    /**
     *
     */
    activateAccountPlan: function (location, planName, serviceInstanceId, serviceInstanceInformation, registrationPayment, callback) {
        var cachedNumber;
        var cachedAccount;

        var pendingPromoErr;
        var emptyBonusErr;
        var iMessageAddonErr;
        var free2020AddonErr;
        var callNumberAddonErr;

        var handleResponse = function (err, result) {
            if (!result) result = {};
            if (err) common.error("AccountManager", "activateAccountPlan: error=" + err.message);
            var status = (err ? (err.status ? err.status : "ERROR") : "OK");

            var pendingPromoStatus = (pendingPromoErr ? (pendingPromoErr.status
                ? pendingPromoErr.status : "ERROR_PENDING_PROMO") : "OK");
            if (pendingPromoStatus != "OK") {
                common.error("AccountManager", "activateAccountPlan: failed to get promo, error=" + pendingPromoErr.message);
                status += ", " + pendingPromoStatus;
                result.pendingPromoStatus = pendingPromoStatus;
            }

            var emptyBonusStatus = (emptyBonusErr ? (emptyBonusErr.status
                ? emptyBonusErr.status : "ERROR_EMPTY_BONUS") : "OK");
            if (emptyBonusStatus != "OK") {
                common.error("AccountManager", "activateAccountPlan: failed to get empty bonus, error=" + emptyBonusErr.message);
                status += ", " + emptyBonusStatus;
                result.emptyBonusStatus = emptyBonusStatus;
            }

            var iMessageAddonStatus = (iMessageAddonErr ? (iMessageAddonErr.status
                ? iMessageAddonErr.status : "ERROR_IMESSAGE_ADDON") : "OK");
            if (iMessageAddonStatus != "OK") {
                common.error("AccountManager", "activateAccountPlan: failed to get iMessage addons, error=" + iMessageAddonErr.message);
                status += ", " + iMessageAddonStatus;
                result.iMessageAddonStatus = iMessageAddonStatus;
            }

            var callNumberAddonStatus = (callNumberAddonErr ? (callNumberAddonErr.status
                ? callNumberAddonErr.status : "ERROR_CALLER_NUMBER_ADDON") : "OK");
            if (callNumberAddonStatus != "OK") {
                common.error("AccountManager", "activateAccountPlan: failed to get caller number addons, error=" + callNumberAddonErr.message);
                status += ", " + callNumberAddonStatus;
                result.callNumberAddonStatus = callNumberAddonStatus;
            }

            if(serviceInstanceInformation.type === 'CIRCLES_SWITCH'){
                portInManager.loadActiveRequestByServiceInstanceNumber(serviceInstanceId, null, (err, portInData) => {
                    let portinNumber = (portInData && portInData.porting_number) ? portInData.porting_number : null;
                    schedulerManager.scheduleCirclesSwitchNotifications(serviceInstanceId, portinNumber, (err, result) => {
                        if (err) {
                            common.error('AccountManager', 'Fail to schedule : ' + err);
                        }
                    });
                });
            }
            
            var free2020AddonStatus = (free2020AddonErr ? (free2020AddonErr.status
                ? free2020AddonErr.status : "ERROR_FREE2020_ADDON") : "OK");
            if (free2020AddonStatus != "OK") {
                common.error("AccountManager", "activateAccountPlan: failed to get Free 2020 addons, error=" + free2020AddonErr.message);
                status += ", " + free2020AddonStatus;
                result.free2020AddonStatus = free2020AddonStatus;
            }

            var obj = {
                type: "ACTIVATE_ACCOUNT",
                status: status,
                errorMessage: (err ? err.message : undefined),
                params: {
                    location: location,
                    serviceInstanceId: serviceInstanceId,
                    serviceInstanceInformation: serviceInstanceInformation,
                    registrationPayment: registrationPayment
                },
                result: result,
                number: cachedNumber,
                customerAccountNumber: cachedAccount,
                serviceInstanceNumber: serviceInstanceId,
                fqdn: config.MYFQDN,
                ts: new Date().getTime()
            };
            

            db.notifications_account.insert(obj, function (errLog) {
                if (errLog) common.error("AccountManager", "createAccount: save log error, error=" + errLog.message);
                if (callback) callback(err, result);
            });

        }

        if (location !== "SG") {
            return handleResponse(new BaseError("Location " + location + " is not supported (SG only)",
                "ERROR_INVALID_PARAMS"));
        }
        if (!serviceInstanceId || !serviceInstanceInformation) {
            return handleResponse(new BaseError("Some mandatory params are missing", "ERROR_INVALID_PARAMS"));
        }

        var components = serviceInstanceInformation.components;
        var addons = serviceInstanceInformation.addons;

        var regPayment = registrationPayment + "";
        if (regPayment != "0" && !REGISTRATION_TYPES[regPayment]) {
            return handleResponse(new BaseError("Registration price is not supported, supported: " + JSON.stringify(REGISTRATION_TYPES),
                "ERROR_INVALID_REGISTRATION_PAYMENT"));
        }

        elitecorePackagesService.loadHierarchy(function (err, hierarchy) {
            if (err) {
                return handleResponse(new BaseError("Failed to load packages: " + err.message, "ERROR_BSS"));
            }

            var registrationProduct;
            if (regPayment != "0") {
                registrationProduct = elitecoreUtils.findPackageByName(hierarchy, REGISTRATION_TYPES[regPayment]);
                if (!registrationProduct) {
                    return handleResponse(new BaseError("Registration product " + REGISTRATION_TYPES[regPayment] + " is not found",
                        "ERROR_INVALID_REGISTRATION_PAYMENT"));
                }
            }

            elitecoreUserService.loadUserInfoByServiceInstanceNumber(serviceInstanceId, function (err, cache) {
                if (err) {
                    return handleResponse(new BaseError(err.message + " (" + serviceInstanceId + ")"), "ERROR_BSS");
                }
                if (!cache) {
                    return handleResponse(new BaseError("Customer " + serviceInstanceId + " is not found"), "ERROR_BSS");
                }

                cachedNumber = cache.number;
                cachedAccount = cache.account;

                if (!cache.serviceInstanceBasePlan) {
                    return handleResponse(new BaseError("Customer " + serviceInstanceId + " has invalid service plan"), "ERROR_BSS");
                }
                if (cache.serviceInstanceBasePlan.name != "CirclesRegistration") {
                    return handleResponse(new BaseError("Customer " + serviceInstanceId + " has already been activated, current plan is '"
                    + cache.serviceInstanceBasePlan.name + "'", "ERROR_ALREADY_ACTIVATED"));
                }

                promotionsManager.loadPendingProducts(addons, cache.orderReferenceNumber, "DEFAULT", (pendingProductsErr, pendingPromoResult) => {
                    if (pendingProductsErr) {
                        pendingPromoErr = new BaseError(pendingProductsErr.message, "ERROR_EMPTY_BONUS_NOT_FOUND");
                        // do not stop customer activation, it is more important to complete activation
                    }

                    // replace addons ids with addon ids from loadPendingProducts result
                    // that is required when a customer has used a promo code which provides
                    // free plus forever or for a limited amount of time

                    var addons = pendingPromoResult.addons;
                    var addonsMetadata = pendingPromoResult.metadata;

                    var callNumberAddon = ec.findPackageName(ec.packages(), "Plus Caller Number Display");
                    if (!callNumberAddon) {
                        callNumberAddonErr = new BaseError("Caller Number Display has not been found",
                            "ERROR_CALLER_NUMBER_DISPLAY_ADDON_NOT_FOUND");
                        // do not stop customer activation, it is more important to complete activation
                    } else {
                        addons.push(callNumberAddon.id);
                    }

                    if (planName == 'CIRCLES_SWITCH') {
                        var free2020 = ec.findPackageName(ec.packages(), "Free Plus 2020");
                        if (!free2020) {
                            free2020AddonErr = new BaseError("Free Plus 2020 has not been found", "ERROR_FREE2020_ADDON_NOT_FOUND");
                            // do not stop customer activation, it is more important to complete activation
                        } else {
                            addons.push(free2020.id);
                        }
                    } else {
                        var iMessageAddon = ec.findPackageName(ec.packages(), "IMESSAGE_ADDON");
                        if (!iMessageAddon) {
                            iMessageAddonErr = new BaseError("IMESSAGE_ADDON has not been found", "ERROR_IMESSAGE_ADDON_NOT_FOUND");
                            // do not stop customer activation, it is more important to complete activation
                        } else {
                            addons.push(iMessageAddon.id);
                        }

                        // subscribe 0MB forever bonus, that bonus to be
                        // used to recover customer bonus data
                        var emptyBonus = ec.findPackageName(ec.packages(), "Bonus 0MB R");
                        if (!emptyBonus) {
                            emptyBonusErr = new BaseError("Bonus 0MB R has not been found", "ERROR_EMPTY_BONUS_NOT_FOUND");
                            // do not stop customer activation, it is more important to complete activation
                        } else {
                            addons.push(emptyBonus.id);
                        }
                    }

                    // activation date should be in SGT
                    var activationDate = new Date(new Date().getTime() + 8 * 60 * 60 * 1000).toISOString().split('.')[0];
                    var accountType = planName ? planName : serviceInstanceInformation.type;
                    var packageName = ACCOUNT_TYPES[accountType];

                    if (!packageName) {
                        return handleResponse(new BaseError("Account type " + (planName ? planName : accountType) +
                            " is not supported " + JSON.stringify(ACCOUNT_TYPES), "ERROR_INVALID_ACCOUNT_TYPE"));
                    }

                    ec.changeServicePlan({
                        billingAccountNumber: cache.billingAccountNumber,
                        effectiveDate: activationDate,
                        packageName: packageName,
                        remarks: "Customer Request",
                        serviceInstanceNumber: serviceInstanceId
                    }, function (err, result) {
                        if (err) {
                            return handleResponse(new BaseError("Failed to switch plan: " + err.message, "ERROR_BSS"));
                        }
                        if (!result || !result.return) {
                            return handleResponse(new BaseError("Failed to switch plan: empty response", "ERROR_BSS_INVALID_RESPONSE"));
                        }
                        if (result.return.responseCode !== "0") {
                            return handleResponse(new BaseError("Failed to switch plan: " + result.return.responseMessage
                                + " (" + result.return.responseCode + ")",
                                "ERROR_BSS_UNKNOWN"));
                        }

                        elitecoreUserService.clearUserInfo(cache.account);
                        hlrManager.set(hlrManager.roamingOff, cache.prefix, cache.number, true, function () {});
                        var tasks = [];
                        var addAddon = function (id, recurrent, addonMetadata) {
                            if (!id) return;

                            tasks.push(function (callback) {
                                var ecPackage = ec.findPackage(ec.packages(), id);
                                if (!ecPackage) {
                                    return callback(undefined, {
                                        subscribed: false,
                                        status: "ERROR",
                                        error: "Package is not found",
                                        packageId: id
                                    });
                                }

                                var overrideEffect = "0";
                                var overrideRecurrent = recurrent ? "true" : "false";
                                var options = {
                                    paymentsDisabled: true
                                };

                                if (addonMetadata) {
                                    var freeAddonMonths = addonMetadata.freeAddonMonths;
                                    options.activeMonths = freeAddonMonths;
                                    options.notification = packageManager.getNotificationActivities("FREE_ON_ACTIVATION", {
                                        paramsReminder: {code: addonMetadata.code}
                                    });
                                }

                                options.allowMultiple = true;
                                packageManager.addonUpdate(cache.prefix, cache.number, "PUT", id, undefined, true, (err, result) => {
                                    if (err) {
                                        return callback(undefined, {
                                            subscribed: false,
                                            status: "ERROR",
                                            error: err.message,
                                            packageId: id
                                        });
                                    }

                                    if (result.length > 0) result = result[0];
                                    result.subscribed = true;
                                    result.status = "OK";
                                    result.packageId = id;
                                    return callback(undefined, result);

                                }, overrideEffect, overrideRecurrent, options);
                            });
                        }

                        if (registrationProduct) {
                            addAddon(registrationProduct.id, false);
                        }
                        if (components) {
                            addAddon(components.data, true);
                            addAddon(components.voice, true);
                            addAddon(components.sms, true);
                        }

                        if (addons && addons.length > 0) {
                            var addedAddonIds = [];
                            addons.forEach(function (item) {
                                if (typeof item == 'object') {
                                    var addonId = item.id;
                                    var count = (item.count && parseInt(item.count) > 0) ? parseInt(item.count) : 1;

                                    // check for duplicates, ignore if has already been added
                                    if (addedAddonIds.indexOf(addonId) == -1) {
                                        addedAddonIds.push(addonId);
                                        for (var i = 0; i < count; i++) {
                                            addAddon(addonId, true, addonsMetadata[addonId]);
                                        }
                                    }
                                } else {
                                    var addonId = item;

                                    // check for duplicates, ignore if has already been added
                                    if (addedAddonIds.indexOf(addonId) == -1) {
                                        addedAddonIds.push(addonId);
                                        addAddon(addonId, true, addonsMetadata[addonId]);
                                    }
                                }
                            });
                        }

                        tasks.push(function (callback) {
                            setTimeout(() => {

                                // added 2 sec delay due to some EC DB lock, requested by Ritesh

                                var date = new Date(common.msOffsetSG(new Date().getTime())).toISOString();
                                elitecoreUserService.updateActivationDate(cache.serviceAccountNumber, date, (err, result)=> {
                                    if (err) {
                                        return callback(undefined, {
                                            operation: "JOIN_DATE_UPDATE",
                                            status: "ERROR",
                                            error: err.message
                                        });
                                    }

                                    return callback(undefined, {
                                        operation: "JOIN_DATE_UPDATE",
                                        status: "OK",
                                        result: result
                                    });
                                });
                            }, 2000);
                        });

                        async.parallelLimit(tasks, 1, function (error, result) {
                            if (err) {
                                return handleResponse(new BaseError("Failed to subscribe some addons: " + err.message,
                                    "ERROR_BSS"));
                            }
                            elitecoreUserService.clearUserInfo(cache.account);
                            return handleResponse(undefined, {
                                activated: true,
                                pendingPromoResult: pendingPromoResult,
                                addons: result
                            });
                        });
                    });
                });
            }, true);
        });
    },

    /**
     *
     */
    scheduleTermination: function (serviceInstanceNumber, effective, initiator, executionKey, callback) {
        if (!serviceInstanceNumber) {
            return callback(new BaseError("Invalid params", "INVALID_PARAMS"));
        }

        if (!callback) {
            callback = function () {
            };
        }

        ec.getCustomerDetailsServiceInstance(serviceInstanceNumber, false, function (err, cache) {
            if (err || !cache) {
                err = new BaseError("User is not found in EC", "USER_NOT_FOUND_EC");
                return callback(err);
            }

            var prefix = cache.prefix;
            var number = cache.number;

            if (effective <= new Date().getTime()) {
                var newStatus = "Terminated";
                var remarks = "Immediate";
                var reason;
                actions.changeStatus(serviceInstanceNumber, newStatus, remarks, reason, initiator, executionKey,
                    callback);
            } else {
                db.scheduler.insert({
                    start: effective,
                    prefix: prefix,
                    number: number,
                    serviceInstanceNumber: serviceInstanceNumber,
                    action: "terminate",
                    activity: "termination_effective",
                    allDay: false,
                    trigger: initiator,
                    parallelLimitOne: true,
                    title: "Terminate " + common.escapeHtml(serviceInstanceNumber),
                    createdDate: new Date().getTime()
                }, function (err, result) {
                    notificationSend.internal(prefix, number, undefined, undefined,
                        "termination_notice", undefined, executionKey);
                    if (err) callback(new BaseError("Scheduler Error", "INSERT_ERROR"));
                    else callback();
                });
            }
        });
    },

    /**
     *
     */
    scheduleTemporarySuspension: function (serviceInstanceNumber, effective, months, perMonthAmount, initiator, executionKey, callback) {
        if (!serviceInstanceNumber || !months) {
            callback(new BaseError("Invalid params", "INVALID_PARAMS"));
        }

        if (!callback) {
            callback = function () {
            };
        }

        ec.getCustomerDetailsServiceInstance(serviceInstanceNumber, false, function (err, cache) {
            if (err || !cache) {
                err = new BaseError("User is not found in EC", "USER_NOT_FOUND_EC");
                return callback(err);
            }

            var prefix = cache.prefix;
            var number = cache.number;

            if (perMonthAmount == "5") {
                var productName = months % 2 == 0 ? "Monthly Suspension " + months / 2 : "Monthly Suspension " + months * 5 + " Dollars";
            } else {
                var productName = "Monthly Suspension " + months;
            }

            var product = ec.findPackageName(ec.packages(), productName);
            var productId = (product) ? product.id : undefined;
            var price = product.price;

            if (!productId) {
                var err = new BaseError("Product " + productName + " is not found is not found in EC", "PRODUCT_NOT_FOUND");
                return callback(err);
            }

            var description = "Suspend " + months + " month(s)";
            transactionManager.performPayment(cache.billingAccountNumber, {
                type: transactionManager.TYPE_SUSPENSION_PAYMENT,
                amount: price,
                internalId: productId,
                metadata: {
                    actions: [
                        transactionManager.createAdvancedPaymentAction(),
                        transactionManager.createAddonSubAction(description, productId)
                    ]
                }
            }, (status, transaction, callback) => {
                if (LOG) common.log(`AccountManager, scheduleTemporarySuspension: transaction status 
                changed, billingAccountNumber= ${cache.billingAccountNumber}, status= ${status}`);
                callback();
            }, (err, result) => {
                if (err || (result && result.validPaymentError)) {
                    let error = err ? err : result.validPaymentError;
                    const errNotification = {
                        teamID: 5,
                        teamName: 'Operations',
                        activity: 'suspension_request_failed',
                        prefix: cache.prefix,
                        number: cache.number,
                        name: cache.billingFullName,
                        email: cache.email,
                        billingAccountNumber: cache.billingAccountNumber,
                        serviceInstanceNumber: cache.serviceInstanceNumber,
                        failure_reason: resourceManager
                        .getErrorValuesForCS(error).description ? resourceManager
                        .getErrorValuesForCS(error).description : 'Operation failed, Please try again'
                    }
                    notificationSend.deliver(errNotification, null, (err, result) => {
                        if (err) {
                            common.error('AccountManager:TempSuspension Error ', err);
                        }
                    }, cache);
                    return callback(resourceManager.getErrorValuesForCS(error));
                }
                var eff = new Date(effective);
                
                var future = new Date(eff.getFullYear(),
                    eff.getMonth() + parseInt(months, 10),
                    eff.getDate(),
                    eff.getHours(),
                    eff.getMinutes(),
                    eff.getSeconds());

                var firstDay = new Date(future.getFullYear(), future.getMonth(), 1);
                var firstDayOfNextMonth = new Date(future.getFullYear(), future.getMonth() + 1, 1);
                var activate;
                if (future.getTime() - firstDay.getTime() > firstDayOfNextMonth.getTime() - future.getTime()) {
                    activate = (firstDayOfNextMonth.getTime() - 8 * 60 * 60 * 1000) + 1000;
                } else {
                    activate = (firstDay.getTime() - 8 * 60 * 60 * 1000) + 1000;
                }

                db.scheduler.insertMany([{
                    start: effective,
                    prefix: common.escapeHtml(prefix),
                    number: common.escapeHtml(number),
                    serviceInstanceNumber: cache.serviceInstanceNumber,
                    action: "suspend",
                    activity: "suspension_effective",
                    email: cache.email,
                    allDay: false,
                    name: cache.billingFullName,
                    trigger: initiator,
                    parallelLimitOne: true,
                    title: "Suspend " + common.escapeHtml(number),
                    "suspension_ending": activate,
                    createdDate: new Date().getTime()
                }, {
                    start: activate,
                    prefix: common.escapeHtml(prefix),
                    number: common.escapeHtml(number),
                    serviceInstanceNumber: cache.serviceInstanceNumber,
                    action: "activate",
                    activity: "suspension_over",
                    name: cache.billingFullName,
                    email: cache.email,
                    allDay: false,
                    trigger: initiator,
                    parallelLimitOne: true,
                    title: "Activate " + common.escapeHtml(number),
                    suspension_period: months + " month(s)",
                    createdDate: new Date().getTime()
                },{
                    start: parseInt(moment(eff).subtract(2, 'days').format('x')),
                    prefix: common.escapeHtml(prefix),
                    number: common.escapeHtml(number),
                    serviceInstanceNumber: cache.serviceInstanceNumber,
                    action: "notification",
                    activity: "suspension_notice",
                    email: cache.email,
                    allDay: false,
                    trigger: initiator,
                    parallelLimitOne: true,
                    title: "Suspend " + common.escapeHtml(number),
                    suspension_ending: activate,
                    suspension_amount_paid: `$${price}`,
                    suspension_starting: moment(effective).format('Do MMMM YYYY'),
                    suspension_ending: moment(activate).format('Do MMMM YYYY'),
                    createdDate: new Date().getTime()
                }, {
                    start: parseInt(moment(activate).subtract(2, 'days').format('x')),
                    prefix: common.escapeHtml(prefix),
                    number: common.escapeHtml(number),
                    serviceInstanceNumber: cache.serviceInstanceNumber,
                    action: "notification",
                    activity: "suspension_over_notice",
                    email: cache.email,
                    month_after_suspension: moment(activate).add(1, 'months').format('MMMM'),
                    allDay: false,
                    trigger: initiator,
                    parallelLimitOne: true,
                    title: "Activate " + common.escapeHtml(number),
                    suspension_period: months + " month(s)",
                    createdDate: new Date().getTime()
                }], (err, result) => {
                    if (err) {
                        return callback(err);
                    }
                    const activity = 'suspension_request_ approved';
                    const notification = {
                        teamID: 5,
                        teamName: "Operations",
                        activity: "suspension_request_approved",
                        prefix: cache.prefix,
                        number: cache.number,
                        name: cache.billingFullName,
                        email: cache.email,
                        billingAccountNumber: cache.billingAccountNumber,
                        serviceInstanceNumber: cache.serviceInstanceNumber,
                        suspensionStatus: 'SCHEDULED',
                        suspension_amount_paid: `$${price}`,
                        suspension_starting: moment(effective).format('Do MMMM YYYY'),
                        suspension_ending: moment(activate).format('Do MMMM YYYY')
                    };

                    
                    notificationSend.deliver(notification, undefined, (err, notifRes) => {
                        if (err) {
                            common.error('AccountManager.scheduleTemporySuspension Error ', err);
                        }else {
                            common.log('AccountManager.scheduleTemporySuspension: notification success', notifRes);
                        }
                    }, cache);
                    return callback(null, result);
                });
            });
        });
    },

    /**
     *
     */
    handleSchedulerEvent: function (tEvent, executionKey, callback) {
        if (!callback) {
            callback = function () {
            };
        }

        var prefix = (tEvent.prefix) ? tEvent.prefix : SG_PREFIX;
        var number = tEvent.number;
        var serviceInstanceNumber = tEvent.serviceInstanceNumber;

        var newStatus;
        if (tEvent.action == "suspend") {
            newStatus = "Suspended";
        } else if (tEvent.action == "terminate") {
            newStatus = "Terminated";
        } else if (tEvent.action == "activate") {
            newStatus = "Active";
        }

        var remarks;
        if (tEvent.trigger) {
            remarks = "Scheduled " + tEvent.trigger
        } else if (tEvent.activity === "termination_portout") {
            remarks = "Scheduled portout";
        } else {
            remarks = "Scheduled";
        }

        
        var reason;
        if (tEvent.activity === "suspension_effective") {
            reason = "SUSPENDED_DUE_TO_SUSPENSION_EFFECTIVE";
        } else if (tEvent.activity === "suspension_over") {
            reason = "ACTIVE_DUE_TO_SUSPENSION_OVER";
        } else if (tEvent.activity === "suspension_manual") {
            reason = "SUSPENDED_MANUAL";
        } else if (tEvent.activity === "termination_portout") {
            reason = "OTHERS";
        }

        var suspensionEndDate = tEvent.suspension_ending ? new Date(tEvent.suspension_ending) : undefined;
        var suspensionMonthCount = tEvent.suspension_period;
        var initiator = "[SCHEDULER][" + tEvent.trigger + "]";

        var cacheHandler = (err, cache) => {
            if (err) {
                return callback(new BaseError(err, "INVALID_SIN"));
            }
            if (!cache) {
                return callback(new BaseError("Customer not found", "ERROR_CUSTOMER_NOT_FOUND"));
            }

            actions.changeStatus(cache.serviceInstanceNumber, newStatus, remarks, reason, initiator, executionKey,
                (err, result) => {
                    if (err) {
                        if (tEvent.action == "terminate" && cache.number) {
                            notificationSend.internal(cache.prefix, cache.number, undefined, undefined,
                                "termination_failure", undefined, executionKey);
                            
                                return callback(new BaseError(err, "TERMINATE_ERROR"));    
                        }
                        return callback(err);
                    }

                    if (tEvent.action == "terminate" && cache.number) {
                        ec.getAccountNumber(cache.prefix, cache.number, function (errAcct, accountNumber) {

                            // sometimes termination fails even if BSS API response has successful status,
                            // to verify that termination has finished properly we need to try to fetch
                            // a customer's details by number and since terminated accounts don't have any number
                            // we not supposed to get any info

                            if (!errAcct && accountNumber) {
                                notificationSend.internal(cache.prefix, cache.number, undefined, undefined,
                                    "termination_failure", undefined, executionKey);
                            }
                        });
                    }

                    if (tEvent.action === 'activate' && tEvent.activity === "suspension_over") {
                        ecommManager.updateSuspensionState('SUSPENSION_ENDED', serviceInstanceNumber)
                            .then(updateRes => {
                                common.log("AccountManager.handleSchedulerEvent: updateSuspensionState", updateRes)
                            })
                            .catch(error => {
                                common.error("AccountManager.handleSchedulerEvent: updateSuspensionState: Error", error);
                            });
   
                    }

                    if (tEvent.activity === 'suspension_effective' && tEvent.action === 'suspend') {
                        const saveSetting = Bluebird.promisify(profileManager.saveSetting);
                        return ecommManager.updateSuspensionState('SUSPENDED', serviceInstanceNumber)
                            .then(updateRes => {
                                if (LOG) common.log("AccountManager.updateSuspensionState", updateRes)
                                return saveSetting(prefix, number, 'selfcare', 'voluntary_suspension',
                                    'Integer', 1, undefined);
                            })
                            .then(result => {
                                if (LOG) common.log(`Successfully saved voluntary_suspension
                                    profile setting`, result);
                            })
                            .catch(error => {
                                common.error("AccountManager.updateSuspensionState: Error", error);
                            })
                    }
                    return callback(err, result);
                }, suspensionEndDate, suspensionMonthCount);
        }

        if (!serviceInstanceNumber) {
            ec.getCustomerDetailsNumber(prefix, number, false, cacheHandler);
        } else {
            ec.getCustomerDetailsServiceInstance(serviceInstanceNumber, false, cacheHandler);
        }
    },

    getInventoryStatusFromEC: function(number) {
        const searchInventoryPromise = Bluebird.promisify(ec.searchInventory);

        return portInManager.loadActiveRequestByPortinNumber(number)
            .then(rows => {
                if (rows && rows.length && rows[0].status) {
                    return {
                        type: 'Portin',
                        status: rows[0].status
                    }
                }
                return portInManager.loadActivePortoutRequest(number)
                    .then(rows => {
                        if (rows && rows.length && rows[0].status) {
                            return {
                                type: 'Portout',
                                status: rows[0].status
                            } 
                        }
                        return searchInventoryPromise({"inventoryNumber": number})
                            .then(result => {
                                if (result && result.return && result.return.inventoryDetails &&
                                    result.return.inventoryDetails[0]) {
                                        return {
                                            type: 'Bss',
                                            status: result.return.inventoryDetails[0].inventoryStatus
                                        }
                                    }
                                    return {
                                        type: 'Bss',
                                        status: 'N/A'
                                    }
                            });
                    });
            });
    },
    loadInventory: (status, type, options, callback) => {
        if (!callback) callback = () => {
        }

        var inventoryStatusId = INVENTORY_STATUS_MAPPING[status];
        var inventorySubTypeId = INVENTORY_TYPE_MAPPING[type];

        if (!inventoryStatusId || !inventorySubTypeId) {
            return callback(new BaseError("Invalid params, status " + status + " or/and type " + type + " are not supported",
                "ERROR_INVALID_PARAMS"))
        }

        elitecoreConnector.searchInventoryList(inventoryStatusId, inventorySubTypeId, (err, result) => {
            if (err) {
                return callback(err);
            }

            if (!result || !result.return) {
                return callback(new BaseError("Empty result", "ERROR_EMPTY_RESULT"))
            }
            if (result.return.responseCode != 0) {
                return callback(new BaseError("Invalid response code " + result.result.responseCode, "ERROR_EMPTY_RESULT"))
            }

            var inventories = [];
            var count = 0;

            if (result.return.inventoryDetails) result.return.inventoryDetails.forEach((item) => {
                var inventoryNumber = item.inventoryNumber ? item.inventoryNumber : "";
                var inventoryType = "GENERAL";
                var inventoryCategory = "";
                var inventoryCharge = "";

                if (item.inventoryAttributes) item.inventoryAttributes.forEach((item) => {
                    if (item.category == 'Charge') {
                        if (item.value && item.value['\\$value']) {
                            inventoryCharge = item.value['\\$value'];
                        }
                    }
                    if (item.category == 'NumberCategory') {
                        if (item.value && item.value['\\$value']) {
                            inventoryCategory = item.value['\\$value'];
                        }
                    }
                });

                if (options.inventoryCategory && options.inventoryCategory != inventoryCategory) {
                    return;
                }
                if (options.inventoryCategory == 'NONE' && inventoryCategory) {
                    return;
                }
                if (options.inventoryType && options.inventoryType != inventoryType) {
                    return;
                }
                if (options.inventoryNumberLength > 0 && (!inventoryNumber || inventoryNumber.length != options.inventoryNumberLength)) {
                    return;
                }
                if (options.offset > 0 && count < options.offset) {
                    count++;
                    return;
                }
                if (options.limit > 0 && inventories.length >= options.limit) {
                    count++;
                    return;
                }

                count++;
                inventories.push({
                    inventoryNumber,
                    inventoryType,
                    inventoryCategory,
                    inventoryCharge
                });

                if (count % 1000 == 0) {
                    if (LOG) common.log("AccountManager", "loadInventory: loaded " + count + " inventories");
                }
            })

            if (LOG) common.log("AccountManager", "loadInventory: totalCount=" + count + ", loadedCount=" + inventories.length);
            callback(undefined, inventories);
        })
    }
}


