const async = require('async');
const config = require(`${global.__base}/config`);
const common = require(`${global.__lib}/common`);
const db = require(`${global.__lib}/db_handler`);
const ec = require(`${global.__lib}/elitecore`);
const BaseError = require(`${global.__core}/errors/baseError`);
const ecommManager = require(`${global.__manager}/ecomm/ecommManager`);
const notificationSend = require(`${global.__manager}/notifications/send`);
const profileManager = require(`${global.__manager}/profile/profileManager`);
const referralManager = require(`${global.__manager}/promotions/referralManager`);
const betaUsersManager = require(`${global.__manager}/beta/betaUsersManager`);
const hlrManager = require(`${global.__manager}/hlr/hlrManager`);

const LOG = config.LOGSENABLED;

class Actions {

    /**
     *
     */
    static activateAccount(serviceInstanceNumber, reason, initiator, executionKey, callback, options) {
        Actions.changeStatus(serviceInstanceNumber, 'ACTIVE', initiator, reason, initiator, executionKey, (err, result) => {
            if (callback) callback(err, result);
        }, undefined, undefined, options);
    }

    /**
     *
     */
    static suspendAccount(serviceInstanceNumber, reason, initiator, executionKey, callback, options) {
        Actions.changeStatus(serviceInstanceNumber, 'SUSPENDED', initiator, reason, initiator, executionKey, (err, result) => {
            if (callback) callback(err, result);
        }, undefined, undefined, options);
    }

    /**
     *
     */
    static changeStatus(serviceInstanceNumber, status, remarks, reason,
        initiator, executionKey, callback, suspensionEndDate, suspensionMonthCount, options) {
        if (!callback) callback = () => {};
        if (!options) options = {};

        if (!serviceInstanceNumber || !status || !remarks) {
            return callback(new BaseError('Invalid params', 'ERROR_INVALID_PARAMS'));
        }

        status = status ? status.toUpperCase() : '';
        reason = reason ? reason.toUpperCase() : '';

        var handleResult = (err, result, cache) => {

            if (result) {
                result.suspensionEndDate = suspensionEndDate;
                result.suspensionMonthCount = suspensionMonthCount;
            }

            // trying to identify if a referral bonus has been unsubscribed

            var referrerNumber;
            var referralBonus;

            if (status === 'TERMINATED') {
                referrerNumber = 'NONE';
                referralBonus = 'NONE';

                if (result && result.asyncResult && result.asyncResult.length > 0) {
                    result.asyncResult.forEach((item) => {

                        if (item.deactivateReferrerBonusResult) {
                            var deactivate = item.deactivateReferrerBonusResult;

                            if (deactivate.result && deactivate.result.length > 0) {
                                var info = deactivate.result[0];
                                referrerNumber = info.number;
                                referralBonus = info.error ? 'ERROR' : 'REMOVED';
                            }
                        }
                    });
                }
            }

            var obj = {
                status: err ? 'ERROR' : 'OK',
                error: err ? err.message : undefined,
                serviceInstanceNumber: serviceInstanceNumber,
                billingAccountNumber: cache ? cache.billingAccountNumber : undefined,
                customerAccountNumber: cache ? cache.customerAccountNumber : undefined,
                prefix: cache ? cache.prefix : undefined,
                number: cache ? cache.number : undefined,
                type: status,
                reason: reason,
                options: options,
                initiator: initiator,
                referrerNumber: referrerNumber,
                referralBonus: referralBonus,
                result: result,
                ts: new Date().getTime()
            };

            if (LOG) common.log('AccountManager', `termination report, obj=${JSON.stringify(obj)}`);
            db.notifications_status_change.insert(obj, () => {
                if (callback) callback(err, result);
            });
        };

        ec.getCustomerDetailsServiceInstance(serviceInstanceNumber, false, (err, cache) => {
            if (err || !cache) {
                err = new BaseError('User is not found in EC', 'ERROR_CUSTOMER_NOT_FOUND');
                return handleResult(err);
            }

            var prefix = cache.prefix;
            var number = cache.number;
            var args = {
                serviceInstanceNumber: cache.serviceInstanceNumber,
                remarks: remarks
            };

            // Convert local status to EC status
            // Terminated -> Suspended (Suspend Permanent), also requires reasonAlias=HLR_TERMINATION
            // Suspended  -> Suspended (Suspend Temporary), also requires reasonAlias=HLR_TERMINATION
            // Active     -> Active

            if (status == 'TERMINATED') {
                args.newServiceStatus = 'Suspended';
                args.reasonAlias = 'HLR_TERMINATION';
                args.subStatus = 'Suspend Permanent';
            } else if (status == 'SUSPENDED') {
                args.newServiceStatus = 'Suspended';
                args.reasonAlias = 'HLR_TERMINATION';
                args.subStatus = 'Suspend Temporary';
            } else if (status == 'ACTIVE') {
                args.newServiceStatus = 'Active';
                args.reasonAlias = 'REACTIVATE_OTHER';
            } else {
                return callback(new BaseError(`Status ${status} is not supported`, 'ERROR_STATUS_INVALID'));
            }

            var tasks = [];
            if (status == 'TERMINATED') {
                tasks.push((cb) => {
                    ecommManager.notifyTerminated(cache.prefix, cache.number, (err, result) => {
                        cb(undefined, {error: err ? err.message : undefined, notifyTerminatedResult: result});
                    });
                });
                tasks.push((cb) => {
                    referralManager.deactivateReferrerBonus(cache.prefix, cache.number, executionKey, (err, result) => {
                        cb(undefined, {error: err ? err.message : undefined, deactivateReferrerBonusResult: result});
                    });
                });
                tasks.push((cb) => {
                    var tomorrow = new Date(new Date().getTime() + 1 * 24 * 60 * 60 * 1000);
                    var params = {start: {'$gt': tomorrow.getTime()}, serviceInstanceNumber: cache.serviceInstanceNumber};
                    db.scheduler.remove(params, (err) => {
                        cb(undefined, {
                            error: err ? err.message : undefined,
                            cleanSchedulerResult: err ? 'FAILED' : 'OK'
                        });
                    });
                });
            }

            async.parallelLimit(tasks, 5, (asyncErr, asyncResult) => {
                if (asyncErr) {
                    common.error('AccountManager', `changeStatus: failed to complete all related to status change operations, error=${asyncErr.message}`);
                    // do nothing, asyncErr is ignored, continue with status change
                }
                common.log(`STATUS_CHANGE_SERIES_TEST, SI NUM : ${args.serviceInstanceNumber}`, `START_TIME: ${new Date().getTime()}`);
                ec.changeServiceInstanceStatus(args, (err, result) => {
                    common.log(`STATUS_CHANGE_SERIES_TEST, SI NUM : ${args.serviceInstanceNumber}`, `END_TIME: ${new Date().getTime()}`);
                    if (err) {
                        return handleResult(err, {
                            statusChanged: false,
                            asyncResult: asyncResult,
                            changeStatusArgs: args,
                            changeStatusResult: result
                        }, cache);
                    }

                    if (status == 'SUSPENDED') {
                        betaUsersManager.isBetaCustomer(number, 'qa_internal', (err, qa) => {
                            // m1 is not ready for this var hlrID = qa ? hlrManager.suspension : hlrManager.roamingOff;
                            var hlrID = qa ? hlrManager.roamingOff : hlrManager.roamingOff;
                            hlrManager.set(hlrID, prefix, number, true, () => {
                                // do nothing
                            });
                        });
                    } else if (status == 'ACTIVE') {
                        betaUsersManager.isBetaCustomer(number, 'qa_internal', (err, qa) => {
                            if (qa) hlrManager.set(hlrManager.reconnection, prefix, number, true, () => {
                                // do nothing
                            });
                        });
                    }

                    // re-cache customer account
                    ec.getCustomerDetailsServiceInstance(serviceInstanceNumber, true, () => {
                        // do nothing
                    });

                    var operationStatus = !result || !result.return ? -1 : result.return.responseCode;
                    if (operationStatus != 0) {
                        return handleResult(new BaseError(`Failed to change status, error code ${operationStatus}`,
                            'ERROR_STATUS_CHANGE_FAILED'), {
                            statusChanged: false,
                            asyncResult: asyncResult,
                            changeStatusArgs: args,
                            changeStatusResult: result
                        }, cache);
                    }

                    handleResult(undefined, {
                        statusChanged: true,
                        asyncResult: asyncResult,
                        changeStatusArgs: args,
                        changeStatusResult: result
                    }, cache);

                    if (status !== 'TERMINATED') {
                        ec.setCache(cache.number, cache.prefix, undefined, cache.account, () => {
                            // do nothing
                        });
                    }

                    if (status === 'ACTIVE') {
                        profileManager.saveSetting(prefix, number, 'selfcare', 'voluntary_suspension',
                            'Integer', 0, executionKey, () => {
                                // do nothing
                            });
                    }

                    var activity;
                    var variables = {};
                    if (status === 'TERMINATED') {
                        activity = 'termination_effective';
                    } else if (status === 'SUSPENDED') {
                        if (reason === 'SUSPENDED_DUE_TO_LOST_SIM') {
                            activity = 'sim_loss_suspension';
                        } else if (reason === 'SUSPENDED_DUE_TO_NON_PAYMENT') {
                            activity = 'monthly_bill_failure_suspension';
                        } else if (reason === 'SUSPENDED_DUE_TO_SUSPENSION_EFFECTIVE') {
                            activity = 'suspension_effective';
                            variables.suspension_ending = common.getDate(suspensionEndDate);
                        } else if (reason === 'SUSPENDED_MANUAL') {
                            activity = 'suspension_manual';
                        } else if (reason === 'SUSPENDED_DUE_TO_BILL_PAYMENT_FAILURE') {
                            activity = 'bill_failure_suspension';
                        } else if (reason === 'SILENT_ACTION') {
                            // do nothing
                        } else {
                            // do nothing
                        }
                    } else if (status === 'ACTIVE') {
                        if (reason === 'ACTIVE_DUE_TO_SUSPENSION_OVER') {
                            activity = 'suspension_over';
                            variables.suspension_period = suspensionMonthCount;
                        } else if (reason === 'SILENT_ACTION') {
                            activity = 'suspension_over_silent';
                        } else if (reason === 'ACTIVE_DUE_TO_BILL_PAYMENT') {
                            activity = 'suspension_over_pay_now';
                        } else {
                            activity = 'suspension_over_manual';
                        }
                    }

                    if (options.notificationVariables) {
                        Object.keys(options.notificationVariables).forEach((key) => {
                            variables[key] = options.notificationVariables[key];
                        });
                    }

                    if (activity) {
                        notificationSend.internal(cache.prefix, cache.number, cache.email,
                            cache.first_name, activity, variables, executionKey);
                    }
                });
            });
        });
    }

}

module.exports = Actions;
