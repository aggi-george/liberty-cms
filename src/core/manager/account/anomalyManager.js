

const config = require('../../../../config');
const common = require('../../../../lib/common');
const async = require('async');
const db = require('../../../../lib/db_handler');
const notificationSend = require('../../manager/notifications/send');
const Bluebird = require('bluebird');

const LOG = config.LOGSENABLED;
const systemSchema = (config.DEV) ? "crestelsystemtestlwp" : "crestelsystemlwp";

class AnomalyManager {
  static getCustomerProvisioningReport(data, callback) {
    const { numbers } = data;
    const isSendEmail = typeof data.sendEmail !== 'undefined' ? data.sendEmail : true;
    const obj = {
      style: {
        table: 'border-collapse:collapse',
        th: 'border:1px solid black',
        td: 'border:1px solid black' },
      head: ['Temp #', 'Ported #', 'Error'],
      body: [] };
    const tasks = [];

    numbers.forEach((number) => {
      if (number) {
        AnomalyManager.pushTasks(tasks, number);
      }
    });
    async.parallelLimit(tasks, 10, (err, result) => {
      if (err) {
        callback(err, undefined);
      }
      result.forEach((item) => {
        if (item && (item.result !== 'OK')) {
          obj.body.push([item.number, item.ported, item.result]);
        }
      });

      if (obj.body.length && isSendEmail) {
        if (LOG) common.log('AnomalyManager getCustomerProvisioningReport', 'Going to send email notification');
        AnomalyManager.sendNotification(obj, data)
          .then((output) => {
            if (LOG) common.log('AnomalyManager getCustomerProvisioningReport', `Notification response: ${output}`);
            const { notificationId } = output;
            callback(undefined, { result, notificationId });
          });
      } else {
        if (LOG) common.log('AnomalyManager getCustomerProvisioningReport', 'Email is suppressed');
        callback(undefined, { result });
      }
    });
  }

  static pushTasks(tasks, number) {
    tasks.push((cb) => {
      AnomalyManager.getPortedNumber(number)
        .then(AnomalyManager.getTempNumber.bind(AnomalyManager))
        .then(({ queryPromise, ported }) => {
          queryPromise
            .then((queryResult) => {
              const rows = queryResult && queryResult.rows ? queryResult.rows : [];
              if (!rows || (!rows.length)) {
                                return cb(undefined, { number, ported: ported || 'N/A', result: 'FAIL - empty' });
                            }

              const remarks = rows.map(o => o[1].split(' ')[0]);
              const fieldnames = rows.map(o => o[2]);
              const successIdx = fieldnames.indexOf('HLRCreateSubscriber_Success');
              const requestIdx = remarks.indexOf('HLR_CREATE_SUBSCRIBER_REQUEST');
              const result = (successIdx > -1) && (successIdx < requestIdx) ? 'OK' :
                (successIdx == -1) && (requestIdx > -1) ? 'FAIL - missing success' : 'FAIL - unknown';
              return cb(undefined, { number, ported, result });
            });
        })
        .catch((err) => {
          if (LOG) common.log('AnomalyManager getCustomerProvisioningReport', `Inside checkPushTask() Error => ${err}`);
          return cb(undefined, { number, ported: 'N/A', result: 'FAIL - empty' });
        });
    });
  }

  static getPortedNumber(number) {
    const query = `SELECT number FROM customerPortIn WHERE temp_number=${db.escape(number)}`;
    /* eslint-disable no-new */
    return new Bluebird((resolve, reject) => {
      db.query_err(query, (err, rows) => {
        const ported = (!err && rows && rows[0] && rows[0].number) ? rows[0].number : '';
        resolve({ ported: ported, number: number });
      });
    });
    /* eslint-enable no-new */
  }

  static getTempNumber(result) {
    const resultObj = {};
    const { ported, number } = result;
    resultObj.ported = ported;
    const dbPromise = Bluebird.promisify(db.oracle.query);
    const query = `SELECT auditdate, remark, fieldname FROM ${systemSchema}.tbltsystemaudit 
      WHERE remark LIKE '%${number}%' AND (remark LIKE 'HLR_CREATE_SUBSCRIBER_REQUEST%' OR remark 
      LIKE 'SERVICE_PROVISION_NOTIFICATION_REQUEST%') AND auditdate > (TRUNC(SYSDATE) - 45) 
      ORDER BY auditdate DESC`;
    resultObj.queryPromise = dbPromise(query);
    return Bluebird.resolve(resultObj);
  }

  static sendNotification(obj, data) {
    const courier = data.courier ? data.courier : '';
    const variables = {
      activity: 'generic_report',
      teamID: 5,
      reportSubject: 'Provisioning Report',
      labelA: `Customer Provisioning - ${courier}`,
      tableA: obj,
      labelB: '',
      tableB: '',
      labelC: '',
      tableC: '' };

    const notifyPromise = Bluebird.promisify(notificationSend.deliver);
    return notifyPromise(variables, null);
  }
}

module.exports = AnomalyManager;
