var async = require('async');
var dateformat = require('dateformat');

const config = require(`${global.__base}/config`);
const common = require(`${global.__lib}/common`);
const db = require(`${global.__lib}/db_handler`);

var ec = require('../../../../lib/elitecore');
var elitecoreConnector = require('../../../../lib/manager/ec/elitecoreConnector');
var elitecoreUserService = require('../../../../lib/manager/ec/elitecoreUserService');
var elitecoreBillService = require('../../../../lib/manager/ec/elitecoreBillService');
var configManager = require('../config/configManager');
var invoiceManager = require('./invoiceManager');
var betaUsersManager = require('../beta/betaUsersManager');
var BaseError = require('../../errors/baseError');

var LOG = config.LOGSENABLED;
var creditCapMinPaymentAmountCents = 200 * 100;

module.exports = {

    /**
     *
     */
    loadOutstandingAmount: (serviceInstanceNumber, callback) => {
        if (!callback) callback = () => {
        }

        elitecoreUserService.loadUserInfoByServiceInstanceNumber(serviceInstanceNumber, (err, cache) => {
            if (err) {
                return callback(err);
            }
            if (!cache) {
                return callback(new BaseError("Customer not found", "ERROR_CUSTOMER_NOT_FOUND"));
            }

            elitecoreBillService.loadCustomerUsage(cache.serviceInstanceNumber, (err, result) => {
                if (err) {
                    return callback(err);
                }

                var amountSpent = result && result.amountCents ? (result.amountCents / 100) : 0;
                module.exports.loadAdvancedPayments(cache.billingAccountNumber, {
                    filterAmountCents: [creditCapMinPaymentAmountCents, creditCapMinPaymentAmountCents * 2]
                }, (err, result) => {
                    if (err) {
                        return callback(err);
                    }

                    var amountPaid = result && result.paymentsCents ? (result.paymentsCents / 100) : 0;
                    var outstandingAmount = amountSpent - amountPaid;
                    return callback(undefined, {amount: outstandingAmount});
                });
            });
        });
    },

    /**
     *
     *
     */
    loadAdvancedPayments: function (billingAccountNumber, options, callback) {
        if (!options) options = {};
        if (!callback) callback = () => {
        };

        elitecoreUserService.loadUserInfoByBillingAccountNumber(billingAccountNumber, (err, cache) => {
            if (err) {
                return callback(err);
            }
            if (!cache) {
                return callback(new BaseError("Customer not found", "ERROR_CUSTOMER_NOT_FOUND"));
            }

            var currentDate = new Date();
            var billingCycle = cache.billing_cycle;
            var currentCycle = ec.nextBill(billingCycle, undefined, currentDate);
            var firstDay = currentCycle.startDateUTC;

            elitecoreConnector.getAccountStatement(billingAccountNumber,
                firstDay.toISOString(), currentDate.toISOString(), (err, result) => {
                    if (err) {
                        return callback(err);
                    }
                    if (!result || !result.return) {
                        return callback(new BaseError("Invalid BSS response", "ERROR_BSS_RESPONSE_INVALID"));
                    }

                    var credits = result.return.creditDocumentVO;
                    var processedResult = getCreditPaymentsAmount(credits, options.filterAmountCents);
                    callback(err, processedResult);
                });
        });
    },


    /**
     *
     *
     */
    addDepositPayment: function (accountNumber, amount, callback) {
        if (!callback)  callback = function () {
        };

        if (!accountNumber && !amount) {
            return callback(new BaseError("Invalid params (accountNumber=" + accountNumber +
            ", amountCents=" + amount + ")", "INVALID_PARAMS"));
        }

        ec.getCustomerDetailsAccount(accountNumber, false, function (err, cache) {
            if (err) {
                return callback(err);
            }

            if (!cache) {
                return callback(new BaseError("Customer account " + accountNumber + " has not been found", "INVALID_PARAMS"));
            }

            elitecoreConnector.addDepositToCustomerBillingAccount(cache.billingAccountNumber,
                amount * 100, "Security Deposit", (err, result) => {
                    if (err) {
                        return callback(err);
                    }

                    if (!result || !result.return || result.return.responseCode != 0) {
                        return callback(new Error("Failed to post a deposit payment, code=" + result.return.responseCode));
                    }

                    elitecoreConnector.getAccountStatement(cache.billingAccountNumber,
                        common.getDate(new Date(cache.serviceInstanceCreationDate)),
                        common.getDate(new Date(common.msOffsetSG())), function (err, result) {
                            if (err) {
                                return callback(err);
                            }

                            var found;
                            var totalDeposit = 0;
                            if (result && result.return && result.return.debitDocumentVO) {
                                result.return.debitDocumentVO.some((item) => {
                                    if (item.documentType == "Deposit" && parseInt(item.amount) == amount * 100) {
                                        found = item;
                                    }
                                });
                                result.return.debitDocumentVO.forEach((item) => {
                                    if (item.documentType == "Deposit") {
                                        var depositAmount = parseInt(item.amount);
                                        if (depositAmount) {
                                            totalDeposit += depositAmount;
                                        }
                                    }
                                });
                            }

                            if (!found) {
                                return callback(new Error("No debit document with amount " + amount * 100 + "found"));
                            }

                            //callback(undefined, found);

                            var date = new Date(found.postDate);
                            if (!date.getTime()) date = new Date();
                            var textDate = date.toISOString().split('T')[0];

                            var documentNumber = found.documentnumber;
                            elitecoreConnector.makeDebitPaymentDataRequest(cache.billingAccountNumber, found.amount,
                                found.documentnumber, textDate, "BillManager", function (err, result) {
                                    if (err) {
                                        return callback(err);
                                    }

                                    if (!result
                                        || !result.return
                                        || !result.return.receivableAccountData
                                        || !result.return.receivableAccountData[0]
                                        || !result.return.receivableAccountData[0].receivableData
                                        || !result.return.receivableAccountData[0].receivableData[0]
                                        || !result.return.receivableAccountData[0].receivableData[0].creditdocumentnumber) {
                                        return callback(new Error("Invalid response, can not find credit document"));
                                    }

                                    var creditDocumentNumber = result.return.receivableAccountData[0]
                                        .receivableData[0].creditdocumentnumber;

                                    callback(err, {
                                        depositDocumentId: documentNumber,
                                        creditDocumentNumber: creditDocumentNumber,
                                        depositAmountTotal: (totalDeposit / 100),
                                        depositAmountAdded: amount
                                    });
                                });
                        });
                });
        });
    },

    /**
     *
     *
     */
    loadOpenBillsHistoryByAccount: function (accountNumber, callback) {
        module.exports.loadBillHistoryByAccount(accountNumber, function (err, result) {
            if (err) {
                return callback(err);
            }

            var openBills = [];
            if (result) {
                if (result.bills) result.bills.forEach(function (item) {
                    if (item.type === 'Regular Invoice') {
                        if (item.payNow && item.payNow.value > 0) {
                            openBills.push(item);
                        }
                    }
                });
                if (result.pending && result.pending.bill && result.pending.bill.payNow
                    && result.pending.bill.payNow.value > 0) {
                    openBills.push(result.pending.bill);
                }
            }

            callback(undefined, openBills);
        });
    },

    /**
     *
     *
     */
    loadOpenBillsHistory: function (prefix, number, callback) {
        module.exports.loadBillHistory(prefix, number, function (err, result) {
            if (err) {
                return callback(err);
            }

            var openBills = [];
            if (result) {
                if (result.bills) result.bills.forEach(function (item) {
                    if (item.type === 'Regular Invoice') {
                        if (item.payNow && item.payNow.value > 0) {
                            openBills.push(item);
                        }
                    }
                });
                if (result.pending && result.pending.bill && result.pending.bill.payNow
                    && result.pending.bill.payNow.value > 0) {
                    openBills.push(result.pending.bill);
                }
            }

            callback(undefined, openBills);
        });
    },

    /**
     *
     *
     */
    loadBillHistoryByAccount: function (accountNumber, callback) {
        if (!callback)  callback = function () {
        };

        if (!accountNumber) {
            return callback(new BaseError("Invalid params", "INVALID_PARAMS"));
        }

        if (LOG) common.log("BillManager", "loadBillHistoryByAccount: accountNumber=" + accountNumber);
        ec.getCustomerDetailsAccount(accountNumber, false, function (err, cache) {
            if (!cache) {
                return callback(new BaseError("Customer account " + accountNumber + " has not been found", "INVALID_PARAMS"));
            }

            module.exports.loadPayments(cache.billingAccountNumber, function (err, payments) {
                callback(err, payments);
            });
        });
    },

    /**
     *
     *
     */
    loadBillHistory: function (prefix, number, callback) {
        if (!callback) callback = function () {
        };

        if (!prefix || !number) {
            return callback(new BaseError("Invalid params", "INVALID_PARAMS"));
        }

        if (LOG) common.log("BillManager", "loadBillHistory: prefix=" + prefix + ", number=" + number);
        ec.getCustomerDetailsNumber(prefix, number, false, function (err, cache) {
            if (!cache) {
                return callback(new BaseError("Customer not found params", "INVALID_PARAMS"));
            }

            module.exports.loadPayments(cache.billingAccountNumber, function (err, payments) {
                callback(err, payments);
            });
        });
    },

    /**
     *
     *
     */
    loadBillRelatedInfo: function (billingAccountNumber, callback) {
        if (!callback) callback = () => {
        }
        if (!billingAccountNumber) {
            return callback(new Error("Invalid params"), reply);
        }

        elitecoreUserService.loadUserInfoByBillingAccountNumber(billingAccountNumber, (err, cache) => {
            if (err) {
                return callback(err);
            }
            if (!cache) {
                return callback(new BaseError("Customer not found", "ERROR_CUSTOMER_NOT_FOUND"));
            }

            var tasks = [];
            tasks.push(function (callback) {
                const transactionsQuery = `SELECT
                    CONCAT(DATE(createdAt + INTERVAL 8 HOUR) - INTERVAL 1 DAY,'T16:00:00.000Z') createdAt,
                    CONCAT(DATE(completedAt + INTERVAL 8 HOUR) - INTERVAL 1 DAY,'T16:00:00.000Z') completedAt,
                    type, amount, internalId FROM customerTransaction WHERE
                    billingAccountNumber=${db.escape(billingAccountNumber)} AND status='OK'`;
                db.query_err(transactionsQuery, (err, result) => {
                    if (err) {
                        return callback(err);
                    }
                    return callback(undefined, { transactions: result });
                });
            });
            tasks.push(function (callback) {
                betaUsersManager.isBetaCustomer(billingAccountNumber, 'bills_dr_only', function (err, backupOnly) {
                    elitecoreBillService.loadCustomerPaymentsInfo(billingAccountNumber, (err, result) => {
                        if (err) {
                            return callback(err);
                        }
                        return callback(undefined, {paymentsInfoResult: result});
                    }, false, backupOnly);
                });
            });
            tasks.push(function (callback) {
                elitecoreBillService.loadCustomerUsage(cache.serviceInstanceNumber, (err, result) => {
                    if (err) {
                        return callback(err);
                    }
                    var usageAmountCents = result && result.amountCents ? result.amountCents : 0;
                    return callback(undefined, {usageAmountCents: usageAmountCents});
                });
            });
            tasks.push(function (callback) {
                module.exports.checkLastBillViewBlocked(billingAccountNumber, function (err, result) {
                    if (err) {
                        return callback(err);
                    }
                    if (result && result.blocked) {
                        return callback(undefined, {lastBillBlocked: true});
                    }

                    invoiceManager.loadCurrentMonth((err, month) => {
                        if (err) {
                            return callback(err);
                        }
                        if (!month) {
                            return callback(undefined, {lastBillBlocked: true});
                        }

                        invoiceManager.loadBills(month.id, {billingAccountNumbers: [billingAccountNumber]}, (err, invoice) => {
                            if (err) {
                                return callback(err);
                            }
                            if (!invoice || !invoice.data || !invoice.data[0] || invoice.data[0].sendStatus != "OK") {
                                return callback(undefined, {lastBillBlocked: true});
                            }

                            callback(undefined, {lastBillBlocked: false});
                        });
                    });
                });
            });

            async.parallelLimit(tasks, 10, function (err, asyncResult) {
                if (err) {
                    return callback(err);
                }

                var billsResult;
                var accountsResult;
                var accountsCurrentCycleResult;

                var usageAmountCents;
                var lastBillBlocked = false;
                var backupData;
                var cachedAt;
                var transactions;

                asyncResult.forEach(function (item) {
                    if (item.paymentsInfoResult) {
                        backupData = item.paymentsInfoResult.backupData;
                        cachedAt = item.paymentsInfoResult.cachedAt;
                        billsResult = item.paymentsInfoResult.bills;
                        if (item.paymentsInfoResult.statements) {
                            accountsResult = item.paymentsInfoResult.statements.all;
                            accountsCurrentCycleResult = item.paymentsInfoResult.currentCycle;
                        }
                    }
                    if (item.lastBillBlocked) lastBillBlocked = item.lastBillBlocked;
                    if (item.usageAmountCents) usageAmountCents = item.usageAmountCents;
                    if (item.transactions) transactions = item.transactions;
                });

                callback(undefined, {
                    usageAmountCents: usageAmountCents,
                    accountsResult: accountsResult,
                    billsResult: billsResult,
                    lastBillBlocked: lastBillBlocked,
                    backupData: backupData,
                    cachedAt: cachedAt,
                    transactions: transactions
                });
            });
        });
    },

    /**
     *
     *
     */
    loadPayments: function (billingAccountNumber, callback) {
        if (!callback) callback = () => {
        }
        if (!billingAccountNumber) {
            return callback(new Error("Invalid params"));
        }

        if (LOG) common.log("BillManager", "loadPayments: billingAccountNumber=" + billingAccountNumber);
        elitecoreUserService.loadUserInfoByBillingAccountNumber(billingAccountNumber, (err, cache) => {
            if (err) {
                return callback(err);
            }
            if (!cache) {
                return callback(new BaseError("Customer not found", "ERROR_CUSTOMER_NOT_FOUND"));
            }

            var creationDate = new Date(cache.serviceInstanceCreationDate);
            var billingCycle = cache.billing_cycle;
            var currentDate = new Date();
            var currentCycle = ec.nextBill(billingCycle, undefined, currentDate);
            var currentCycleStart = currentCycle.startDateUTC;

            module.exports.loadBillRelatedInfo(billingAccountNumber, function (err, result) {
                if (err) {
                    return callback(err);
                }
                if (!result || !result.accountsResult || !result.billsResult) {
                    return callback(new BaseError("Invalid customer bills / credits / payments response", "ERROR_INVALID_BILLS_INFO"));
                }

                var backupData = result.backupData;
                var cachedAt = result.cachedAt;
                var creditDocumentVO = result.accountsResult.creditDocumentVO;
                var debitDocumentVO = result.accountsResult.debitDocumentVO;

                var capUsageCents = result.usageAmountCents;
                var capPaymentsResult = getCreditPaymentsAmount(creditDocumentVO,
                    [creditCapMinPaymentAmountCents, creditCapMinPaymentAmountCents * 2], currentCycleStart);

                var capPaymentsCents = capPaymentsResult.paymentsCents;
                var capOutstandingCents = capUsageCents - capPaymentsCents;
                var capPaymentsCountRequired = parseInt(capOutstandingCents / creditCapMinPaymentAmountCents);

                var refunds = [];
                var cancellations = [];
                var debitCreditHistory = [];

                var credit = [];
                if (creditDocumentVO) {
                    creditDocumentVO.forEach(function (item) {
                        item.postDateObj = parsePostDate(item.postDate);
                        item.amountInt = parseInt(item.amount);
                    });
                    creditDocumentVO = result.accountsResult.creditDocumentVO.sort(function (a, b) {
                        return a.postDateObj.getTime() < b.postDateObj.getTime() ? -1 : 1;
                    });
                    creditDocumentVO.forEach(function (item) {
                        debitCreditHistory.push({
                            postDate: parsePostDate(item.postDate),
                            amount: parseInt(item.amount),
                            type: "Credit",
                            documentNumber: item.documentnumber,
                            documentType: item.documentType
                        });

                        if (item.documentType === "Cancel Payment") {
                            cancellations.push(item);


                        } else if (item.documentType === "Refund advance Charge") {
                            refunds.push(item)
                        } else {
                            credit.push(item);
                        }
                    });

                    // since cancellation happens always after payment it is save to assume
                    // that we can remove similar payment from array with credits
                    cancellations.forEach((item) => {
                        for (var i = credit.length - 1; i >= 0; i--) {
                            if ( (item.amountInt + credit[i].amountInt == 0) && (item.postDateObj.getTime() >= credit[i].postDateObj.getTime()) ) {
                                credit.splice(i, 1);
                                break;
                            }
                        }
                    });
                }

                var debit = [];
                if (debitDocumentVO) {
                    debitDocumentVO.forEach(function (item) {
                        item.postDateObj = parsePostDate(item.postDate);
                        item.amountInt = parseInt(item.amount);
                    });
                    debitDocumentVO = result.accountsResult.debitDocumentVO.sort(function (a, b) {
                        return a.postDateObj.getTime() < b.postDateObj.getTime() ? -1 : 1;
                    });

                    debitDocumentVO.forEach(function (item) {
                        debitCreditHistory.push({
                            postDate: parsePostDate(item.postDate),
                            amount: parseInt(item.amount),
                            type: "Debit",
                            documentNumber: item.documentnumber,
                            documentType: item.documentType
                        });

                        if (item.amount > 0) {
                            debit.push(item);
                        }
                    });
                }

                debitCreditHistory = debitCreditHistory.sort(function (a, b) {
                    return a.postDate.getTime() < b.postDate.getTime() ? 1 : -1;
                });

                var billsBssMap = {};
                if (result.billsResult.objList) result.billsResult.objList.forEach(function (item) {
                    if (item.billStatus === "FULLY_PAID" || item.billStatus === 'PAID') item.unpaidAmount = 0;
                    billsBssMap[item.billNumber] = item;
                });

                var reply = {
                    bills: [],
                    debit_credit_history: {list: debitCreditHistory},
                    instant_charges: {list: []},
                    refunds: {},
                    creditCap: {
                        paymentRequired: capPaymentsCountRequired > 0,
                        paymentAmount: {
                            prefix: "$",
                            value: creditCapMinPaymentAmountCents / 100,
                            postfix: ""
                        },
                        outstandingAmount: {
                            prefix: "$",
                            value: parseInt(capOutstandingCents) / 100,
                            postfix: ""
                        }
                    }
                };

                var totalPevCycle = 0;
                var totalCurrentCycle = 0;
                var advancePayments = [];
                var billPaymentsMap = {};

                credit.forEach(function (credit) {
                    var postDate = parsePostDate(credit.postDate);
                    var amount = parseInt(credit.amount) / 100;

                    if (credit.documentType === "Advance Payment" || credit.documentType === "Credit note") {
                        advancePayments.push({
                            postDate: postDate,
                            amount: amount
                        })
                    }

                    // "Debit Payment" is payment for a bill, it always applies for previous month
                    // so we consider "Debit Payment" as prev month payment all the time

                    if (credit.documentType === "Debit Payment") {
                        billPaymentsMap[(parseInt(credit.amount) / 100) + ""] = credit;
                    }

                    if ((postDate.getTime() < currentCycle.startDateUTC.getTime() && debit.length > 0)
                        || credit.documentType === "Debit Payment"
                        || credit.documentType === "Credit note") {
                        totalPevCycle += amount;
                    } else {
                        var label;
                        if (credit.documentType === "Cancel Payment") {
                            label = "Cancellation";
                        } else if (credit.documentType === "Refund advance Charge") {
                            label = "Refund";
                        } else {
                            var priceOptionKey = "" + parseInt(credit.amount);

                            var predefinedPaymentLabel = config.PAYMENT_OPTIONS
                                ? config.PAYMENT_OPTIONS[priceOptionKey]
                                : undefined;
                            if (result.transactions) result.transactions.every((trans) => {
                                // this is also guess work but should cover most cases
                                if (!trans.matched && trans.type == 'BOOST_PAYMENT' &&
                                    trans.amount == credit.amountInt &&
                                    (trans.createdAt == credit.postDateObj.toISOString() ||
                                    trans.completedAt == credit.postDateObj.toISOString())) {
                                    const product = ec.findPackage(ec.packages(), trans.internalId)
                                    if (product)
                                        predefinedPaymentLabel = `Payment for ${product.name}`;
                                    trans.matched = credit.documentnumber;
                                    return false;
                                } else return true;
                            });

                            if (predefinedPaymentLabel) {
                                label = predefinedPaymentLabel;
                            } else {
                                label = "Payment";
                            }
                        }

                        totalCurrentCycle += amount;
                        reply.instant_charges.list.push({
                            "id": credit.documentnumber,
                            "label": label,
                            "paid": {
                                "prefix": "$",
                                "value": amount,
                                "postfix": credit.currency
                            },
                            "date": postDate.toISOString()
                        });
                    }
                });

                var hasInvoiceForLastCycles;
                var pendingBillForLastCycles;
                var debitNotes = [];

                debit.forEach(function (bill) {
                    var price = parseInt(bill.amount) / 100;
                    var post = parsePostDate(bill.postDate);

                    // post is in UTC but nextBill requires SGT time
                    var receivedInCycle = ec.nextBill(billingCycle, new Date(post.getTime() + 8 * 60 * 60 * 1000));
                    var billEnd = new Date(receivedInCycle.startDate.getTime() - 1000 * 60 * 60 * 24);
                    var billEndUTC = new Date(receivedInCycle.startDateUTC.getTime() - 1000);
                    var appliedInCycle = ec.nextBill(billingCycle, billEnd);
                    var billStart = appliedInCycle.startDate;
                    var billStartUTC = appliedInCycle.startDateUTC;

                    // for labels use SGT time
                    var label = parseBillLabel(billStart, billEnd);

                    var paid = 0;
                    var adjustments = [];

                    if (bill.documentType === "Regular Invoice") {
                        refunds.forEach(function (item) {
                            if (item.postDateObj.getTime() == post.getTime()) {
                                var adjustment = {
                                    "id": item.documentnumber,
                                    "type": item.documentType,
                                    "price": {
                                        "prefix": "$",
                                        "value": parseInt(item.amount) / 100,
                                        "postfix": bill.currency
                                    },
                                    "date": post.toISOString(),
                                    "view": false
                                };

                                price -= adjustment.price.value;
                                adjustments.push(adjustment);
                            }
                        });
                    }

                    if (price <= totalPevCycle) {
                        totalPevCycle -= price;
                        paid = price;
                    } else {
                        paid = totalPevCycle;
                        totalPevCycle = 0;
                    }

                    var invoice = {
                        id: bill.documentnumber,
                        type: bill.documentType,
                        show_price: true,
                        bill_start: billStart.toISOString(),
                        bill_start_utc: billStartUTC.toISOString(),
                        bill_end: billEnd.toISOString(),
                        bill_end_utc: billEndUTC.toISOString(),
                        price: {
                            prefix: "$",
                            value: price,
                            postfix: bill.currency
                        },
                        correctedPrice: {
                            prefix: "$",
                            value: price,
                            postfix: bill.currency
                        },
                        paid: {
                            prefix: "$",
                            value: Math.round(paid * 100) / 100,
                            postfix: bill.currency
                        },
                        advancePayments: [],
                        date: post.toISOString(),
                        label: label,
                        view: false,
                        backupData: backupData,
                        cachedAt: cachedAt
                    };

                    // all debit notes should be aggregated into one bill as adjustments
                    // if prevInvoice does not exist it means debit were created in current
                    // billing cycle and should not be shown to the customer

                    if (bill.documentType === "Regular Invoice") {
                        if (!invoice.adjustedFrom) {
                            invoice.adjustedFrom = JSON.parse(JSON.stringify(invoice.price));
                        }
                        if (!invoice.adjustedTo) {
                            invoice.adjustedTo = JSON.parse(JSON.stringify(invoice.price));
                        }

                        invoice.debitNotes = debitNotes;
                        invoice.debitNotes.forEach(function (item) {
                            invoice.adjustedTo.value += item.price.value
                            invoice.price.value += item.price.value;
                            invoice.paid.value += item.paid.value;
                        });

                        invoice.adjustments = adjustments;
                        invoice.adjustments.forEach(function (item) {
                            invoice.adjustedFrom.value += item.price.value;

                            // price was already corrected above
                            // during calculation of invoce amount
                        });

                        invoice.adjustedTo.value = Math.round(invoice.adjustedTo.value * 100) / 100;
                        invoice.price.value = Math.round(invoice.price.value * 100) / 100;
                        invoice.paid.value = Math.round(invoice.paid.value * 100) / 100;

                        advancePayments.forEach(function (payment) {
                            if (new Date(invoice.bill_start_utc).getTime() <= payment.postDate.getTime()
                                && payment.postDate.getTime() < new Date(invoice.bill_end_utc).getTime()) {
                                invoice.correctedPrice.value -= payment.amount;
                                invoice.advancePayments.push(payment);
                            }
                        });

                        debitNotes = [];

                        if (billsBssMap[invoice.id]) {
                            var bill = billsBssMap[invoice.id];
                            invoice.price.value = bill.amount / 100;
                            invoice.paid.value = (bill.amount - bill.unpaidAmount) / 100;

                            invoice.payNow = {
                                prefix: "$",
                                value: bill.unpaidAmount / 100,
                                postfix: invoice.paid.postfix
                            }
                        }

                        var isCurrentCycleInvoice = post.getTime() >= currentCycle.startDateUTC.getTime();
                        if (isCurrentCycleInvoice && result.lastBillBlocked) {
                            pendingBillForLastCycles = invoice;
                        } else {
                            if (isCurrentCycleInvoice) {
                                hasInvoiceForLastCycles = true;
                            }
                            reply.bills.push(invoice);
                        }
                    } else {
                        debitNotes.push(invoice);
                    }
                });

                // show "Bill processing" if no invoice is found for the first 10 days
                // after 5 days user should see outstanding if not paid

                var now = new Date();
                var recentlyActivated = creationDate.getUTCFullYear() == now.getUTCFullYear()
                    && creationDate.getUTCMonth() == now.getUTCMonth();

                if (!hasInvoiceForLastCycles && !recentlyActivated) {

                    // for labels use SGT time
                    var prevBillEnd = new Date(currentCycle.startDate.getTime() - 1000 * 60 * 60 * 24);
                    var prevCycle = ec.nextBill(billingCycle, prevBillEnd);
                    var label = parseBillLabel(prevCycle.startDate, prevBillEnd);

                    if (pendingBillForLastCycles) {
                        pendingBillForLastCycles.type = "Pending Invoice";
                    }

                    reply.pending = {
                        "type": "Pending Invoice",
                        "bill": pendingBillForLastCycles,
                        "price": {},
                        "correctedPrice": {},
                        "paid": {},
                        "advancedPayments": {
                            "prefix": "$",
                            "value": Math.round(totalPevCycle * 100) / 100,
                            "postfix": "SGD"
                        },
                        "date": currentCycle.startDateUTC,
                        "label": label,
                        "view": false
                    };
                } else {

                    // in case if we have some "Advanced Payment" or "Debit Payment" which are not included in any bill
                    // and made in a prev month we consider it as an overpayment and aggregate all payments to a singe item

                    if (totalPevCycle > 0) {
                        // for labels use SGT time
                        reply.refunds.label = "Included into " + dateformat(Date.parse(currentCycle.endDate), "dS mmmm") + " bill";
                        reply.refunds.paid = {
                            "prefix": "$",
                            "value": Math.round(totalPevCycle * 100) / 100,
                            "postfix": "SGD"
                        };
                    }
                }

                // for labels use SGT time
                var currentCycleLastDate = new Date(currentCycle.endDate - 1000 * 60 * 60 * 24);
                reply.instant_charges.label = parseBillLabel(currentCycle.startDate, currentCycleLastDate);
                reply.instant_charges.paid = {
                    "prefix": "$",
                    "value": Math.round(totalCurrentCycle * 100) / 100,
                    "postfix": "SGD"
                };

                if (reply.bills) {
                    reply.bills.sort(function (a, b) {
                        return new Date(b.date).getTime() - new Date(a.date).getTime();
                    });
                }

                reply.bssBills = result && result.billsResult ? result.billsResult.objList : undefined;
                callback(false, reply);
            });
        });
    },

    /**
     *
     *
     */
    addBlockedBillingAccount: function (data, callback) {
        var pushTask = function (tasks, billingAccountNumber) {
            tasks.push(function (callback) {
                insertBillingAccount(billingAccountNumber, function (err, result) {
                    callback(undefined, result)
                });
            });
        }

        var insertBillingAccount = function (billingAccountNumber, callback) {
            var insertQuery = "INSERT INTO billHidden (billing_account_number) VALUES ("
                + db.escape(billingAccountNumber.toUpperCase()) + ")";

            if (LOG) common.log("BillManager", "addCustomers: insertQuery=" + insertQuery);
            db.query_err(insertQuery, function (err, result) {
                if (err) {
                    if (LOG) common.error("BillManager", "addCustomers: can not save customer, " + err.code);
                    var alreadyAdded = (err.code === 'ER_DUP_ENTRY') ? 'NRIC already saved' : 'Database error';
                    callback(alreadyAdded ? undefined : err, {billingAccountNumber: billingAccountNumber, added: false});
                } else {
                    return callback(undefined, {billingAccountNumber: billingAccountNumber, added: true});
                }
            });
        }

        var tasks = [];
        data.forEach(function (item) {
            pushTask(tasks, item.billing_account_number);
        });

        async.parallelLimit(tasks, 1, function (err, result) {
            if (err) {
                return callback(err);
            }

            var addedCount = 0;
            var skippedCount = 0;
            if (result) {
                result.forEach(function (item) {
                    if (item.added) {
                        addedCount++;
                    } else {
                        skippedCount++;
                    }
                });
            }

            callback(undefined, {
                addedCount: addedCount,
                skippedCount: skippedCount
            });
        });
    },

    /**
     *
     *
     */
    loadBlockedBillingAccount: function (callback) {
        var insertQuery = "SELECT id, billing_account_number FROM billHidden ORDER BY id DESC";
        db.query_err(insertQuery, function (err, result) {
            if (err) {
                if (LOG) common.error("BillManager", "loadBlockedBillingAccount: can not load");
                return callback(err);
            }

            var accounts = [];
            result.forEach(function (item) {
                accounts.push({
                    id: item.id,
                    billing_account_number: item.billing_account_number
                });
            })

            return callback(undefined, {list: accounts});
        });
    },

    /**
     *
     *
     */
    cleanBlockedBillingAccount: function (callback) {
        var query = "DELETE FROM billHidden";
        db.query_err(query, function (err, result) {
            if (err) {
                if (LOG) common.error("BillManager", "cleanBlockedBillingAccount: can not load");
                return callback(err);
            }

            return callback(undefined, {deletedCount: result.affectedRows});
        });
    },

    /**
     *
     *
     */
    checkLastBillViewBlocked: function (billingAccount, callback) {
        var selectQuery = "SELECT * FROM billHidden WHERE " + "billing_account_number=" + db.escape(billingAccount);
        db.query_err(selectQuery, function (err, result) {
            if (err) {
                if (LOG) common.error("BillManager", "checkLastBillViewBlocked: can not load");
                return callback(err);
            }

            return callback(undefined, {blocked: result.length > 0});
        });
    },

    /**
     * this will give the monthNumber for a billingAccountNumber from MySQL
     * @param billingccount - LW..
     * @param forMonths - how many months he is loyal for - you pass 2 if you want customer who are loyal for two months only.
     * @param callback
     */
    isCustomerLoyal : function(billingAccount,forMonths, callback) {
        const selectQuery = `SELECT monthNumber FROM billFiles WHERE billingAccountNumber = ${db.escape(billingAccount)} ORDER BY monthNumber DESC LIMIT 1`;
        db.query_err(selectQuery, function (err, rows) {
            if (err) {
                if (LOG) common.error("BillManager", "isCustomerLoyal: can not load");
                return callback(err);
            }
            let found = false;
            if(rows[0]){
                if(rows[0].monthNumber === forMonths){
                    found = true;
                }
            }
            return callback(undefined,found);
        });
    }
}

function getCreditPaymentsAmount(credits, filterAmountCents, startDate) {
    var paymentsCents = 0;
    var payments = [];

    if (!filterAmountCents) {
        filterAmountCents = [];
    } else if (!Array.isArray(filterAmountCents)) {
        filterAmountCents = [filterAmountCents];
    }

    if (credits) credits.forEach(function (item) {
        var postDateObj = parsePostDate(item.postDate);
        if (!startDate || postDateObj.getTime() > startDate.getTime()) {

            var amount = parseInt(item.amount);
            if ((item.documentType == "Advance Payment" || item.documentType == "Cancel Payment")
                && (filterAmountCents.indexOf(amount) >= 0 || filterAmountCents.indexOf(amount * -1) >= 0)) {
                paymentsCents += parseInt(item.amount);
                payments.push(item);
            }
        }
    });

    return {paymentsCents: paymentsCents, payments: payments};
}

// shame on me for that function
function parsePostDate(postDate) {
    var date = postDate && postDate.getTime ? postDate : new Date(postDate.replace("SGT", "UTC+08:00"));
    return date;
}

function parseBillLabel(billStart, billEnd) {
    var billDayStart = billStart.getDate();
    var billDayEnd = billEnd.getDate();
    var billMonthFirstDay = dateformat(Date.parse(billStart), "mmmm");
    var billMonthLastDay = dateformat(Date.parse(billEnd), "mmmm");
    var billYearLastDay = billEnd.getFullYear();

    var label;
    if (billMonthFirstDay === billMonthLastDay) {
        label = billDayStart + "-"
        + billDayEnd + " " + billMonthLastDay + " " + billYearLastDay;
    } else {
        label = billDayStart + " " + billMonthFirstDay + " - "
        + billDayEnd + " " + billMonthLastDay + " " + billYearLastDay;
    }
    return label;
}
