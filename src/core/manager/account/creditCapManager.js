var config = require('../../../../config');
var common = require('../../../../lib/common');
var db = require('../../../../lib/db_handler');
var ec = require('../../../../lib/elitecore');

var log = config.LOGSENABLED;

var ROAMING_CAP_ADDONS = {
    "100": "NONE",
    "300": "PRD00433",
    "600": "PRD00434"
}

module.exports = {

    /**
     *
     */
    updateRoamingCap: function (prefix, number, limit, callback) {
        if (log) common.log("creditCapManager", "updateRoamingCap: prefix=" + prefix + ", number="
        + number + ", limit=" + limit);

        var newCapProductId = ROAMING_CAP_ADDONS[limit + ""];
        if (!newCapProductId) {
            common.error("creditCapManager", "updateRoamingCap: limit is not supported, limit=" + limit);
            return callback(new Error("Roaming cap limit " + limit
            + " is not supported data not found"));
        }

        ec.getCustomerDetailsNumber(prefix, number, false, function (err, cache) {
            if (err) {
                common.error("creditCapManager", "updateRoamingCap: load customer info error, err=" + err.message);
                return callback(err);
            }

            if (!cache || !cache.serviceInstanceRoaming) {
                common.error("creditCapManager", "updateRoamingCap: customer not found, number=" + number);
                return callback(new Error("Customer data not found or invalid, prefix=" + prefix
                + ", number=" + number));
            }

            if (cache.serviceInstanceRoaming.limit === limit) {
                if (log) common.log("creditCapManager", "updateRoamingCap: limit is already " + limit + ", no update required");
                return callback(undefined, {
                    subscribed: false, limit: limit,
                    reason: "Limit is already the same"
                });
            }

            var subscribeNewRoamingCap = function () {
                if (newCapProductId === "NONE") {
                    // reload customer info
                    return ec.getCustomerDetailsNumber(prefix, number, true, function () {
                        callback(undefined, {
                            subscribed: false, limit: limit,
                            reason: "No addon is required for that limit"
                        });
                    });
                }

                var addon = [{
                    product_id: newCapProductId,
                    recurrent: true,
                    effect: 0
                }];

                ec.addonSubscribe(cache.number, cache.serviceInstanceNumber, addon,
                    cache.billing_cycle, function (err, result) {
                        if (err) {
                            if (log) common.log("creditCapManager", "updateRoamingCap: subscription error, error=" + err.message);
                            return callback(err, result);
                        }

                        // reload customer info
                        ec.getCustomerDetailsNumber(prefix, number, true, function () {
                            return callback(undefined, {subscribed: true, limit: limit, result: result});
                        });
                    });
            }

            var unsubscribeOldAddon = function (existingAddon) {
                if (!existingAddon) {
                    return subscribeNewRoamingCap();
                }

                var addon = [{
                    product_id: existingAddon.id,
                    packageHistoryId: existingAddon.packageHistoryId,
                    effect: 0
                }];

                ec.addonUnsubscribe(number, cache.serviceInstanceNumber, addon,
                    cache.billingCycleStartDay, function (err, result) {
                        if (err) {
                            common.error("creditCapManager", "updateRoamingCap: unsubscription error, error=" + err.message);
                            return callback(err, result);
                        }
                        subscribeNewRoamingCap();
                    });
            }

            unsubscribeOldAddon(cache.serviceInstanceRoaming.addon);
        });
    }
}