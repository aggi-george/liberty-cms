var request = require('request');
var async = require('async');

var config = require('../../../../config');
var common = require('../../../../lib/common');
var db = require('../../../../lib/db_handler');

var BaseError = require('../../errors/baseError');
var elitecoreUserService = require('../../../../lib/manager/ec/elitecoreUserService');

var log = config.LOGSENABLED;

module.exports = {

    /**
     *
     */
    updateAccountDetails: function (accountNumber, params, callback) {
        if (!callback) callback = () => {
        }

        if (!accountNumber) {
            return callback(new BaseError("Some mandatory params are missing", "INVALID_PARAMS"));
        }

        var updated = [];
        Promise.resolve().then(() => {
            return new Promise((resolve, reject) => {
                if (!params.name) {
                    return resolve();
                }

                if (log) common.log("DetailsManager", "updateAccountDetails:updateCustomerName: starting...");
                elitecoreUserService.updateAllCustomerAccountsDetails(accountNumber, {name: params.name}, (err) => {
                    if (err) {
                        common.error("DetailsManager", "updateAccountDetails:updateCustomerName: failed, error=" + err.message);
                        return reject(err);
                    }

                    updated.push("CUSTOMER_NAME");
                    if (log) common.log("DetailsManager", "updateAccountDetails:updateCustomerName: updated");
                    resolve();
                });
            });
        }).then(() => {
            return new Promise((resolve, reject) => {
                if (!params.email) {
                    return resolve();
                }

                if (log) common.log("DetailsManager", "updateAccountDetails:updateCustomerEmail: starting...");
                elitecoreUserService.updateAllCustomerAccountsDetails(accountNumber, {email: params.email}, (err) => {
                    if (err) {
                        common.error("DetailsManager", "updateAccountDetails:updateCustomerEmail: failed, error=" + err.message);
                        return reject(err);
                    }

                    updated.push("CUSTOMER_EMAIL");
                    if (log) common.log("DetailsManager", "updateAccountDetails:updateCustomerEmail: updated");
                    resolve();
                });
            });
        }).then(() => {
            return new Promise((resolve, reject) => {
                if (!params.birthday) {
                    return resolve();
                }

                if (log) common.log("DetailsManager", "updateAccountDetails:updateCustomerDOB: starting...");
                elitecoreUserService.updateCustomerDOB(accountNumber, params.birthday, (err) => {
                    if (err) {
                        common.error("DetailsManager", "updateAccountDetails:updateCustomerDOB: failed, error=" + err.message);
                        return reject(err);
                    }

                    updated.push("CUSTOMER_DOB");
                    if (log) common.log("DetailsManager", "updateAccountDetails:updateCustomerDOB: updated");
                    resolve();
                });
            });
        }).then(() => {
            return new Promise((resolve, reject) => {
                if (!params.idType || !params.idNumber) {
                    return resolve();
                }

                if (log) common.log("DetailsManager", "updateAccountDetails:updateCustomerID: starting...");
                elitecoreUserService.updateCustomerID(accountNumber, params.idType, params.idNumber, (err) => {
                    if (err) {
                        common.error("DetailsManager", "updateAccountDetails:updateCustomerID: failed, error=" + err.message);
                        return reject(err);
                    }

                    updated.push("CUSTOMER_ID");
                    if (log) common.log("DetailsManager", "updateAccountDetails:updateCustomerID: updated");
                    resolve();
                });
            });
        }).then(() => {
            return new Promise((resolve, reject) => {
                if (!params.orderReferenceNumber) {
                    return resolve();
                }

                if (log) common.log("DetailsManager", "updateAccountDetails:updateOrderReferenceNumber: starting...");
                elitecoreUserService.updateCustomerOrderReferenceNumber(accountNumber, params.orderReferenceNumber, (err) => {
                    if (err) {
                        common.error("DetailsManager", "updateAccountDetails:updateOrderReferenceNumber: " +
                        "failed, error=" + err.message);
                        return reject(err);
                    }

                    updated.push("CUSTOMER_ORDER_REFERENCE_NUMBER");
                    if (log) common.log("DetailsManager", "updateAccountDetails:updateOrderReferenceNumber: updated");
                    resolve();
                });
            });
        }).then(() => {
            return new Promise((resolve, reject) => {
                if (!params.portedStatus) {
                    return resolve();
                }

                if (log) common.log("DetailsManager", "updateAccountDetails:updatePortInStatus: starting...");
                elitecoreUserService.updateCustomerAccountData({
                    accountNumber: accountNumber,
                    accountProfiles: {strcustom3: params.portedStatus}
                }, (err) => {
                    if (err) {
                        common.error("DetailsManager", "updateAccountDetails:updatePortInStatus: " +
                        "failed, error=" + err.message);
                        return reject(err);
                    }

                    updated.push("CUSTOMER_PORT_IN_STATUS");
                    if (log) common.log("DetailsManager", "updateAccountDetails:updatePortInStatus: updated");
                    resolve();
                });
            });
        }).then(() => {
            callback(undefined, updated);
        }).catch(function (err) {
            common.error("DetailsManager", "updateAccountDetails: error=" + (err ? err.message : "Unknown error")
            + ", status=" + err.status);

            callback(err);
        });
    }
}


