const config = require(`${global.__base}/config`);
const common = require(`${global.__lib}/common`);
const db = require(`${global.__lib}/db_handler`);

const LOG = config.LOGSENABLED;

class DeviceManager{
	constructor(){
		this.isTheDeviceOld = this.isTheDeviceOld.bind(this);
	};
	 /**
     *
     * @param prefix - 65
     * @param number - 81152030
     * @param ageInYears - max age of device you consider as old..for ex : 2
     * @param callback
     */
    isTheDeviceOld(prefix, number,ageInYears, callback){
        const selectQuery = `SELECT EXISTS(select a.number from app a join device d on a.id = d.app_id join device_models dm on d.user_agent LIKE CONCAT('%', dm.user_agent_identifier ,'%') where dm.start_date <= DATE_SUB(NOW(),INTERVAL ${ageInYears} YEAR) and a.number = CONCAT('${prefix}','${number}')) exist;`;
        db.query_err(selectQuery, function (err, rows) {
            if (err) {
                if (LOG) common.error("deviceManager", "isTheDeviceOld: can not load");
                return callback(err);
            }
            let found = false;
            if(rows[0]){
                if(rows[0].exist === 1){
                    found = true;
                }
            }
            return callback(undefined,found);
        });
    };
}
let deviceManager = new DeviceManager();
module.exports = deviceManager;
