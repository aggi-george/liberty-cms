const bluebird = require('bluebird');
const spawn = require('child_process').spawn;
const common = require(`${__lib}/common`);

class ecSyncManager {

    static elitecoreCustomerDetails(callback) {
        const accounts = new bluebird.Promise((resolve, reject) => {
            let timer = setTimeout(() => {
                reject(new Error('Accounts timeout'));
            }, 120000);
            let obj = { accountsTotal: 0, accountsDone: 0 };
            const aScript = spawn(`${process.cwd()}/scripts/internal/sync_ec_accounts.js`);
            aScript.stderr.on('data', function(data) {
                common.error('ecSync Accounts', data.toString());
            });
            aScript.stdout.on('data', function(data) {
                var lines = data ? data.toString().split('\n') : [];
                lines.forEach(function(line) {
                    var items = line ? line.split(',') : [];
                    if ( items && (items[0] == 'accounts total') ) obj.accountsTotal = parseInt(items[1]);
                    if ( items && (items[0] == 'accounts done') ) obj.accountsDone = parseInt(items[1]);
                });
            });
            aScript.on('close', function(code) {
                clearTimeout(timer);
                resolve(obj);
            });
        });
        const numbers = new bluebird.Promise((resolve, reject) => {
            let timer = setTimeout(() => {
                reject(new Error('Numbers timeout'));
            }, 120000);
            let obj = { numbersTotal: 0, numbersDone: 0}; // ignore this for now
            const nScript = spawn(`${process.cwd()}/scripts/internal/sync_ec_numbers.js`);
            nScript.stderr.on('data', function(data) {
                common.error('ecSync Numbers', data.toString());
            });
            nScript.stdout.on('data', function(data) {
                var lines = data ? data.toString().split('\n') : [];
                lines.forEach(function(line) {
                    var items = line ? line.split(',') : [];
                    if ( items && (items[0] == 'numbers total') ) obj.numbersTotal = parseInt(items[1]);
                    if ( items && (items[0] == 'numbers done') ) obj.numbersDone = parseInt(items[1]);
                });
            });
            nScript.on('close', function(code) {
                clearTimeout(timer);
                resolve(obj);
            });
        });
        const portouts = new bluebird.Promise((resolve, reject) => {
            let timer = setTimeout(() => {
                reject(new Error('Portouts timeout'));
            }, 120000);
            const oScript = spawn(`${process.cwd()}/scripts/internal/sync_ec_portouts.js`);
            oScript.stderr.on('data', function(data) {
                common.error('ecSync Portouts', data.toString());
            });
            oScript.on('close', function(code) {
                clearTimeout(timer);
                resolve({ portouts: 'ok' });
            });
        });
        bluebird.all([ accounts, numbers, portouts ]).then(result => {
            callback(undefined, result);
        }, err => {
            callback(err);
        });
    }

}

module.exports = ecSyncManager;
