var async = require('async');
var fs = require('fs');
var extract = require('pdf-text-extract');
var crypto = require("crypto");

var config = require('../../../../config');
var common = require('../../../../lib/common');
var db = require('../../../../lib/db_handler');

var elitecoreConnector = require('../../../../lib/manager/ec/elitecoreConnector');
var elitecoreBillService = require('../../../../lib/manager/ec/elitecoreBillService');
var elitecoreUserService = require('../../../../lib/manager/ec/elitecoreUserService');
var fileManager = require("../file/fileManager");

var BaseError = require('../../errors/baseError');

var rootFolderPath = __dirname + '/../../../../temp_bills';
var log = config.LOGSENABLED;
var pdfRootFolder = config.BILLS_PATH;
var operationKey = "billFilesSyncSending";
var operationSyncProgressKey = "billFilesSyncSendingProgress";

var months = ['January', 'February', 'March', 'April', 'May', 'June',
    'July', 'August', 'September', 'October', 'November', 'December'];

module.exports = {

    /**
     *
     *
     */
    getMonthName: function (date) {
        var montDate = new Date(date);
        if (montDate.getTime() > 0) {
            var paymentMonth = common.getLocalTime(montDate).getMonth();
            if (paymentMonth == 0) {
                paymentMonth = 12;
            }
            var monthName = months[paymentMonth - 1];
            return monthName ? monthName : date + "";
        } else {
            return date + "";
        }
    },

    /**
     *
     *
     */
    loadBillsSyncProgress: function (callback) {
        db.cache.get("cache", operationSyncProgressKey, (cache) => {
            callback(undefined, cache ? cache : {});
        });
    },

    /**
     *
     *
     */
    loadCurrentMonth: function (callback) {
        if (!callback) callback = () => {
        }

        var now = common.getLocalTime();
        var monthDate = new Date(now.getFullYear(), now.getMonth(), 1, 0, 0, 0);

        module.exports.loadMonths({
            monthDate: monthDate
        }, (err, result) => {
            if (err) {
                return callback(err);
            }

            callback(undefined, result && result.data ? result.data[0] : undefined);
        })
    },

    /**
     *
     *
     */
    loadMonths: function (options, callback) {
        if (!callback) callback = () => {
        }
        if (!options) options = {};

        var fields = "id, monthDate, monthName, " +
            " (SELECT count(*) FROM billFiles WHERE monthId=billMonths.id) as invoicesCount," +
            " (SELECT count(*) FROM billFiles WHERE monthId=billMonths.id AND sendStatus='OK') as sentCount," +
            " (SELECT count(*) FROM billFiles WHERE monthId=billMonths.id AND sendStatus='SENDING') as sendingCount," +
            " (SELECT count(*) FROM billFiles WHERE monthId=billMonths.id AND sendStatus like 'ERROR%') as sendErrorsCount," +
            " (SELECT count(*) FROM billFiles WHERE monthId=billMonths.id AND syncStatus like 'ERROR%') as syncErrorsCount," +
            " (SELECT count(*) FROM billFiles WHERE monthId=billMonths.id AND (paymentStatus like 'ERROR%' " +
            " OR paymentStatus like 'PF%')) as paymentErrorsCount," +
            " (SELECT count(*) FROM billFiles WHERE monthId=billMonths.id AND contentStatus like 'ERROR%') as contentErrorsCount";

        var where = "1=1 " +
            (options.filter ? " AND (monthName LIKE " + db.escape("%" + options.filter + "%") + " OR " +
            " id IN (SELECT DISTINCT monthId FROM billFiles WHERE invoiceId LIKE " + db.escape("%" + options.filter + "%") + " OR " +
            " billingAccountNumber LIKE " + db.escape("%" + options.filter + "%") + "))" : "") +
            (options.monthDate ? " AND monthDate=" + db.escape(options.monthDate) : "");

        var q = "SELECT " + fields + " FROM billMonths WHERE " + where + " ORDER BY monthDate DESC" +
            (options.limit > 0 ? " LIMIT " + db.escape(options.limit) : "") +
            (options.offset > 0 ? " OFFSET " + db.escape(options.offset) : "");

        db.query_err(q, function (err, rows) {
            if (err) {
                common.error("InvoiceManager", "loadMonths: failed to load months, error=" + err.message);
                return callback(err);
            }

            var selectCount = "SELECT count(*) as count FROM billMonths WHERE " + where;
            db.query_err(selectCount, function (err, countResult) {
                if (err) {
                    common.error("InvoiceManager", "loadMonths: failed to load months count, error=" + err.message);
                    return callback(err);
                }

                var totalCount = countResult && countResult.length > 0
                    ? countResult[0].count : 0;

                callback(undefined, {
                    totalCount: totalCount,
                    data: rows
                });
            });
        });
    },

    /**
     *
     *
     */
    loadBill: function (invoiceId, callback) {
        if (!callback) callback = () => {
        }
        if (!invoiceId) {
            return callback(new BaseError("Invalid Params", "ERROR_INVALID_PARAMS"));
        }

        module.exports.loadBills(undefined, {invoiceIds: [invoiceId]}, (err, result) => {
            if (err) {
                common.error("InvoiceManager", "loadBill: failed to a bill, error=" + err.message);
                return callback(err);
            }
            var invoice = result && result.data ? result.data[0] : undefined;
            callback(undefined, invoice);
        });
    },

    /**
     *
     *
     */
    loadBillsCount: function (billingAccountNumbers, options, callback) {
        if (!callback) callback = () => {
        }
        var startDate = new Date();
        if (!billingAccountNumbers || billingAccountNumbers.constructor !== Array ||
             billingAccountNumbers.length <1) {
                callback(undefined, {
                    time: (new Date().getTime() - startDate.getTime()),
                    data: []
                });
        }

        var where = "1=1 ";
        var inStatement = ""
        billingAccountNumbers.forEach((item) => {
            if (inStatement.length > 0) inStatement += ',';
            inStatement += db.escape(item);
        })
        where += " AND type <>'NEGATIVE' AND billingAccountNumber IN (" + inStatement + ")";
        groupBy = " GROUP BY billingAccountNumber";
        var selectCount = "SELECT billingAccountNumber, count(1) as count FROM billFiles WHERE " + where + groupBy;
        db.query_err(selectCount, function (err, result) {
            if (err) {
                common.error("InvoiceManager", "loadBillsCount: failed to load bills count, error=" + err.message);
                return callback(err);
            }

            callback(undefined, {
                time: (new Date().getTime() - startDate.getTime()),
                data: result
            });
        });
    },

    /**
     *
     *
     */
    loadBills: function (monthId, options, callback) {
        if (!callback) callback = () => {
        }
        if (!options) options = {};
        var startDate = new Date();

        var fields = "billFiles.id, monthId, billFiles.profileId, billFiles.billingAccountNumber, billFiles.invoiceId, billFiles.type, " +
            "syncedName, syncedEmail, syncedNumber, syncedPwd, sendStatus, syncStatus, contentStatus, billFiles.paymentStatus, sentCount, " +
            "billFiles.createdAt, billFiles.sentAt, filePath, billFiles.paymentAt, billFiles.totalAmount, billFiles.leftAmount, " +
            "otherUnpaid, otherPaid, notificationActivity, billMonths.monthDate as monthDate, billMonths.monthName as monthName, " +
            "regenerated, batchStatus, monthNumber, transactionId, customerTransaction.uuid as transactionUUID, " +
            "customerTransaction.status as transactionStatus, customerTransaction.state as transactionState, " +
            "customerTransaction.externalTransactionId";

        var where = "1=1 " +
            (monthId ? " AND monthId=" + db.escape(monthId) : "") +
            (options.filter ? " AND (" +
            " billFiles.billingAccountNumber LIKE " + db.escape("%" + options.filter + "%") + " OR " +
            " billFiles.invoiceId LIKE " + db.escape("%" + options.filter + "%") + " OR " +
            " billFiles.type LIKE " + db.escape("%" + options.filter + "%") + " OR " +
            " billFiles.syncedName LIKE " + db.escape("%" + options.filter + "%") + " OR " +
            " billFiles.syncedEmail LIKE " + db.escape("%" + options.filter + "%") + " OR " +
            " billFiles.syncedNumber LIKE " + db.escape("%" + options.filter + "%") + " OR " +
            " billFiles.sendStatus LIKE " + db.escape("%" + options.filter + "%") + " OR " +
            " billFiles.batchStatus LIKE " + db.escape("%" + options.filter + "%") + " OR " +
            " billFiles.contentStatus LIKE " + db.escape("%" + options.filter + "%") + " OR " +
            " billFiles.syncStatus LIKE " + db.escape("%" + options.filter + "%") + " OR " +
            " billFiles.paymentStatus LIKE " + db.escape("%" + options.filter + "%") + ")" : "");

        if (options.type && options.type != "ANY") {
            if (options.type == "OPEN") {
                where += " AND (billFiles.type='UNPAID')";
            } else if (options.type == "CLOSED") {
                where += " AND (billFiles.type='PAID' OR billFiles.type='NEGATIVE')";
            } else if (options.type == "PARTIALLY_PAID_NEW") {
                where += " AND billFiles.type='PAID' AND billFiles.otherUnpaid IS NOT NULL";
            } else if (options.type == "PARTIALLY_PAID_OLD") {
                where += " AND billFiles.type='UNPAID' AND billFiles.otherPaid IS NOT NULL";
            } else {
                where += " AND billFiles.type=" + db.escape(options.type);
            }
        }

        if (options.dependencyType == "OTHER_PAID") {
            where += " AND billFiles.otherPaid IS NOT NULL";
        } else if (options.dependencyType == "OTHER_UNPAID") {
            where += " AND billFiles.otherUnpaid IS NOT NULL";
        }

        if (options.hasPaymentDate) {
            where += " AND billFiles.paymentAt<>'0000-00-00 00:00:00'";
        } else {
            if (options.paymentType == 'PAYMENT_PROCESSED') {
                where += " AND billFiles.paymentAt<>'0000-00-00 00:00:00'";
            } else if (options.paymentType == 'PAYMENT_NOT_PROCESSED') {
                where += " AND billFiles.paymentAt='0000-00-00 00:00:00'";
            }
        }

        if (options.monthDateLt && options.monthDateLt.getTime()) {
            where += " AND billMonths.monthDate<" + db.escape(options.monthDateLt);
        } else if (options.monthDateGt && options.monthDateGt.getTime()) {
            where += " AND billMonths.monthDate>=" + db.escape(options.monthDateLt);
        }

        if (options.paymentAtLt && options.paymentAtLt.getTime()) {
            where += " AND billFiles.paymentAt<" + db.escape(options.paymentAtLt);
        } else if (options.paymentAtGt && options.paymentAtGt.getTime()) {
            where += " AND billFiles.paymentAt>=" + db.escape(options.paymentAtGt);
        }

        if (options.monthIdNot) {
            where += " AND billFiles.monthId<>" + db.escape(options.monthIdNot);
        }

        if (options.billingAccountNumbers) {
            var inStatement = ""
            options.billingAccountNumbers.forEach((item) => {
                if (inStatement.length > 0) inStatement += ',';
                inStatement += db.escape(item);
            })
            where += " AND billFiles.billingAccountNumber IN (" + inStatement + ")";
        }

        if (options.invoiceIds && options.invoiceIds.length) {
            var inStatement = ""
            options.invoiceIds.forEach((item) => {
                if (inStatement.length > 0) inStatement += ',';
                inStatement += db.escape(item);
            })
            where += " AND billFiles.invoiceId IN (" + inStatement + ")";
        }

        if (options.status) {
            if (options.status == "SYNC_NO_FILE_FOUND") {
                where += " AND billFiles.syncStatus IN ('ERROR_FILE_NOT_FOUND')";
            } else if (options.status == "SYNC_NO_NUMBER_FOUND") {
                where += " AND billFiles.syncStatus IN ('ERROR_BSS_EMPTY_NUMBER')";
            } else if (options.status == "SYNC_INVALID_DATA") {
                where += " AND billFiles.syncStatus IN ('ERROR_BSS_INCONSISTENT_NAME'," +
                "'ERROR_BSS_INCONSISTENT_EMAIL','ERROR_BSS_DETAILS_NOT_FOUND')";
            } else if (options.status == "SYNC_MULTIPLE_FILES") {
                where += " AND billFiles.syncStatus = 'ERROR_MULTIPLE_FILES'";
            } else if (options.status == "SYNC_MULTIPLE_INVOICES") {
                where += " AND billFiles.syncStatus = 'ERROR_MULTIPLE_INVOICES'";
            } else if (options.status == "SYNC_MULTIPLE_REGENERATED") {
                where += " AND billFiles.syncStatus = 'ERROR_MULTIPLE_REGENERATED'";
            } else if (options.status == "SYNC_ANY_ERROR") {
                where += " AND billFiles.syncStatus LIKE 'ERROR%'";
            } else if (options.status == "SEND_SENT") {
                where += " AND billFiles.sendStatus='OK' AND billFiles.type IN ('PAID', 'UNPAID')";
            } else if (options.status == "SEND_SENDING") {
                where += " AND billFiles.sendStatus='SENDING' AND billFiles.type IN ('PAID', 'UNPAID')";
            }  else if (options.status == "SEND_NOT_SENT") {
                where += " AND billFiles.sendStatus='NOT_SENT' AND billFiles.type IN ('PAID', 'UNPAID')";
            }else if (options.status == "SEND_NEED_TO_SEND") {
                where += " AND billFiles.type IN ('PAID', 'UNPAID') AND sendStatus = 'NOT_SENT' " +
                "AND syncStatus = 'OK' AND (contentStatus = 'OK' OR contentStatus = 'UNKNOWN')";
            } else if (options.status == "SEND_NEED_TO_SEND_AND_FAILED") {
                where += " AND billFiles.type IN ('PAID', 'UNPAID') AND (sendStatus = 'NOT_SENT' OR sendStatus LIKE 'ERROR%') " +
                "AND syncStatus = 'OK' AND (contentStatus = 'OK' OR contentStatus = 'UNKNOWN')";
            } else if (options.status == "SEND_INVALID_TO_SEND") {
                where += " AND billFiles.sendStatus <> 'OK' AND sendStatus <> 'SENDING' " +
                "AND (syncStatus <> 'OK' OR (contentStatus <> 'OK' AND contentStatus <> 'UNKNOWN'))";
            } else if (options.status == "SEND_ANY_ERROR") {
                where += " AND billFiles.sendStatus LIKE 'ERROR%'";
            } else if (options.status == "CONTENT_ANY_ERROR") {
                where += " AND billFiles.contentStatus LIKE 'ERROR%'";
            } else if (options.status == "CONTENT_UNKNOWN") {
                where += " AND billFiles.contentStatus='UNKNOWN'";
            } else if (options.status == "CONTENT_TIMEOUT_ERROR") {
                where += " AND billFiles.contentStatus='ERROR_TIMEOUT'";
            } else if (options.status == "PAYMENT_ANY_ERROR") {
                where += " AND (billFiles.paymentStatus LIKE 'ERROR%' OR billFiles.paymentStatus LIKE 'PF_%')";
            } else if (options.status == "PAYMENT_UNKNOWN") {
                where += " AND billFiles.paymentStatus='UNKNOWN'";
            } else if (options.status == "PAYMENT_OK") {
                where += " AND billFiles.paymentStatus='OK'";
            } else if (options.status == "TRANSACTION_NOT_FINISHED") {
                where += " AND (billFiles.type='UNPAID' AND billFiles.batchStatus IS NOT NULL AND billFiles.batchStatus<>'OK')";
            } else if (options.status == "FILE_EMPTY_PATH") {
                where += " AND (billFiles.filePath IS NULL OR billFiles.filePath='' AND type IN ('PAID', 'UNPAID'))";
            }
        }

        var q = "SELECT " + fields + " FROM billFiles " +
            "LEFT OUTER JOIN billMonths ON (billMonths.id = billFiles.monthId) " +
            "LEFT OUTER JOIN customerTransaction ON (customerTransaction.id = billFiles.transactionId) " +
            "WHERE " + where + " ORDER BY billFiles.billingAccountNumber DESC, billFiles.id DESC " +
            (options.limit > 0 ? " LIMIT " + db.escape(options.limit) : "") +
            (options.offset > 0 ? " OFFSET " + db.escape(options.offset) : "");

        db.query_err(q, function (err, rows) {
            if (err) {
                common.error("InvoiceManager", "loadBills: failed to load bills, error=" + err.message);
                return callback(err);
            }

            if (options.noLengthCheck) {
                return callback(undefined, {
                    time: (new Date().getTime() - startDate.getTime()),
                    totalCount: -1,
                    data: rows
                });
            }

            var selectCount = "SELECT count(*) as count FROM billFiles WHERE " + where;
            db.query_err(selectCount, function (err, countResult) {
                if (err) {
                    common.error("InvoiceManager", "loadBills: failed to load bills count, error=" + err.message);
                    return callback(err);
                }

                var totalCount = countResult && countResult.length > 0
                    ? countResult[0].count : 0;

                callback(undefined, {
                    time: (new Date().getTime() - startDate.getTime()),
                    totalCount: totalCount,
                    data: rows
                });
            });
        });
    },

    /**
     *
     *
     */
    loadReport: function (monthId, options, callback) {
        if (!callback) callback = () => {
        }
        if (!options) options = {};

        var query = "SELECT " +
            "COUNT(IF(type='PAID',1,null)) as TYPE_PAID, " +
            "COUNT(IF(type='NEGATIVE',1,null)) as TYPE_NEGATIVE, " +
            "COUNT(IF(type='UNPAID',1,null)) as TYPE_UNPAID, " +
            "COUNT(IF(sendStatus='OK',1,null)) as SEND_STATUS_SENT, " +
            "COUNT(IF(sendStatus='NOT_SENT',1,null)) as SEND_STATUS_NOT_SENT, " +
            "COUNT(IF(sendStatus='SENDING',1,null)) as SEND_STATUS_SENDING, " +
            "COUNT(IF(sendStatus LIKE 'ERROR%',1,null)) as SEND_STATUS_ERROR, " +
            "COUNT(IF(paymentStatus='PF_NOT_HONORED',1,null)) as PAYMENT_ERROR_PF_NOT_HONORED, " +
            "COUNT(IF(paymentStatus='PF_LOST',1,null)) as PAYMENT_ERROR_PF_LOST, " +
            "COUNT(IF(paymentStatus='PF_STOLEN',1,null)) as PAYMENT_ERROR_PF_STOLEN, " +
            "COUNT(IF(paymentStatus='PF_INSUFFICIENT_FUND',1,null)) as PAYMENT_ERROR_PF_INSUFFICIENT_FUND, " +
            "COUNT(IF(paymentStatus='PF_CARD_EXPIRED',1,null)) as PAYMENT_ERROR_PF_CARD_EXPIRED, " +
            "COUNT(IF(paymentStatus='PF_INVALID_CARD_INFORMATION',1,null)) as PAYMENT_ERROR_PF_INVALID_CARD_INFORMATION, " +
            "COUNT(IF(paymentStatus='PF_OTHER',1,null)) as PAYMENT_ERROR_PF_OTHER, " +
            "COUNT(*) as totalCount FROM billFiles WHERE 1=1 " +
            (monthId ? " AND monthId=" + db.escape(monthId) : "");

        db.query_err(query, function (err, rows) {
            if (err) {
                common.error("InvoiceManager", "loadReport: failed to load report, error=" + err.message);
                return callback(err);
            }

            callback(undefined, {
                data: rows[0] ? rows[0] : undefined
            });
        });
    },

    /**
     *
     *
     */
    syncPaymentsFromFile: function (fileId, type, options, callback) {
        if (!callback) callback = () => {
        }

        if (!fileId || !type) {
            return callback(new BaseError("Month date is missing", "ERROR_INVALID_PARAMS"))
        }

        var stepNumber = 0;
        var logId = -1;
        var lockCacheKey = operationKey;
        var progressCacheKey = operationSyncProgressKey;

        var progressCallback = (stepName, newStep, progress, end) => {
            if (newStep) {
                stepNumber++;
                if (log) common.log("InvoiceManager", "syncPaymentsFromFile->(" + stepNumber + ")" + stepName);
            }
            saveProgressIntoCache(progressCacheKey, stepName, stepNumber, progress, end);
        }

        Promise.resolve({metadata: {}}).then((info) => {
            return getPromiseLogCreation("SYNC_PAYMENTS_FILE", info, options, progressCallback, (id) => {
                logId = id;
            });
        }).then((info) => {
            return getPromiseLock(info, options, lockCacheKey, progressCallback);
        }).then((info) => {
            return new Promise((resolve, reject) => {
                progressCallback("LOAD_PAYMENT_STATUSES", true, 0);
                loadPaymentsResultsFromXLS(fileId, type, (err, result) => {
                    if (err) {
                        common.error("InvoiceManager", "syncPaymentsFromFile->lock: failed to load " +
                        "statuses from payment file, err=" + err.message);
                        return reject(err);
                    }

                    if (log) common.log("InvoiceManager", "syncPaymentsFromFile->loadPaymentsResultsFromXLS: success count=" +
                    result.success.length + ", failure count=" + result.failure.length);

                    info.paymentInfo = result;
                    resolve(info);
                });
            });
        }).then((info) => {
            return new Promise((resolve, reject) => {
                progressCallback("UPDATE_INVOICES", true, 0);
                var paymentInfo = info.paymentInfo;

                var tasks = [];
                var tasksFinishedCount = 0;

                var updatePaymentStatus = (payment, callback) => {

                    if (payment.status == "OK") {

                        // 'OK' paymentStatus is also being updated from payment file,
                        // need to make sure that payment file is uplaoded to BSS earlier than in CMS,
                        // or else there could be some inconsistency due to sync delay in DR database

                        // in case if we see SUCCESS payment in payment gateway, aka Wirecard,
                        // we mark paymentStatus with OK, before sending files we need to check
                        // type and paymentStatus consistency, it is not allowed to send pdf if
                        // invoice has "paymentStatus" OK and "type" UNPAID
                        // it means DR database is in inconsitent state

                        var query = "UPDATE billFiles SET" +
                            " type=" + db.escape('PAID') + ", " +
                            " paymentStatus=" + db.escape(payment.status) + ", " +
                            " paymentAt=" + db.escape(payment.time) +
                            " WHERE invoiceId=" + db.escape(payment.invoiceId);
                    } else {

                        // 'ERROR' status will be ignored if invoice is alsready marked as
                        // PAID / NEGATIVE in BSS database

                        var query = "UPDATE billFiles SET" +
                            " paymentStatus=" + db.escape(payment.status) + ", " +
                            " paymentAt=" + db.escape(payment.time) +
                            " WHERE invoiceId=" + db.escape(payment.invoiceId) +
                            " AND type<>'PAID' AND type<>'NEGATIVE'";
                    }

                    db.query_err(query, function (err, result) {
                        if (err) {
                            return callback(err);
                        }

                        notifyProgress(tasks.length, ++tasksFinishedCount, "UPDATE_INVOICES", progressCallback)
                        callback(undefined, {status: result.affectedRows == 0 ? "IGNORED" : "UPDATED"});
                    });
                }

                paymentInfo.success.forEach((item) => {
                    (function (payment) {
                        tasks.push((callback) => {
                            updatePaymentStatus(payment, callback);
                        });
                    }(item));
                });
                paymentInfo.failure.forEach((item) => {
                    (function (payment) {
                        tasks.push((callback) => {
                            updatePaymentStatus(payment, callback);
                        });
                    }(item));
                });

                async.parallelLimit(tasks, 50, (err, result) => {
                    if (err) {
                        return reject(err);
                    }

                    var entriesIgnoredCount = 0;
                    var entriesUpdatedCount = 0;

                    result.forEach((item) => {
                        if (item.status == "IGNORED") {
                            entriesIgnoredCount++;
                        } else {
                            entriesUpdatedCount++;
                        }
                    });

                    if (log) common.log("InvoiceManager", "syncPaymentsFromFile->insertEntries: updated count=" +
                    entriesUpdatedCount + ", ignored count=" + entriesIgnoredCount);

                    info.metadata.update = {
                        entriesIgnoredCount: entriesIgnoredCount,
                        entriesUpdatedCount: entriesUpdatedCount
                    };
                    resolve(info);
                });
            });
        }).then((info) => {
            return getPromiseUpdateBillsDependencies(info, options, progressCallback);
        }).then((info) => {
            processOperationResult(logId, undefined, info, options, lockCacheKey, progressCallback, callback);
        }).catch(function (err) {
            processOperationResult(logId, err, undefined, options, lockCacheKey, progressCallback, callback);
        });
    },

    /**
     *
     *
     */
    runBatchPayment: function (billingAccountNumber, options, callback) {
        if (!callback) callback = () => {
        }
        if (!options) options = {};

        if (billingAccountNumber && options.batchSize > 1) {
            return callback(new BaseError("Invalid input, single payment has invalid batch size not equal to 1", "ERROR_INVALID_PARAMS"));
        }
        if (!billingAccountNumber && !options.batchSize) {
            return callback(new BaseError("Invalid batch size <= 0", "ERROR_INVALID_PARAMS"));
        }
        var applyMonthFilter = false;
        if (options.monthNumber) {
            var allowedMonthNumbers = [-1, 1, 2, 3]
            if (allowedMonthNumbers.indexOf(options.monthNumber) < 0) {
                return callback(new BaseError("Invalid month number " + options.monthNumber,
                    "ERROR_INVALID_PARAMS"));
            }

            if (options.monthNumber > 0) {
                applyMonthFilter = true;
            }
        }

        var logId = -1;
        var stepNumber = 0;
        var lockCacheKey = operationKey;
        var progressCacheKey = operationSyncProgressKey;

        options.batch = true;
        options.reportId = crypto.randomBytes(16).toString("hex");
        options.billingAccountNumber = billingAccountNumber;

        var progressCallback = (stepName, newStep, progress, end) => {
            if (newStep) {
                stepNumber++;
                if (log) common.log("InvoiceManager", "runBatchPayment->(" + stepNumber + ")" + stepName);
            }
            saveProgressIntoCache(progressCacheKey, stepName, stepNumber, progress, end);
        }

        Promise.resolve({metadata: {}}).then((info) => {
            return getPromiseLogCreation("BATCH_PAYMENT", info, options, progressCallback, (id) => {
                logId = id;
            });
        }).then((info) => {
            return getPromiseLock(info, options, lockCacheKey, progressCallback);
        }).then((info) => {
            return new Promise((resolve, reject) => {
                module.exports.loadCurrentMonth((err, currentMonth) => {
                    if (err) {
                        return reject(err);
                    }

                    if (!currentMonth) {
                        return reject(new BaseError("Current month invoices has not been synchronized, " +
                        "payments are disabled to avoid inconsistency", "ERROR_INVOICES_NOT_SYNCED"));
                    }

                    info.currentMonth = currentMonth;
                    info.currentMonthId = currentMonth.id;
                    return resolve(info);
                })
            });
        }).then((info) => {
            return new Promise((resolve, reject) => {
                progressCallback("LOAD_UNPAID_LOCAL", true, 0);
                var monthNumberFilter = "";

                switch(options.monthNumber) {
                    case 1:
                        monthNumberFilter = 'monthNumber=1';
                        break;
                    case 2:
                        monthNumberFilter = 'monthNumber=2';
                        break;
                    case 3:
                        monthNumberFilter = 'monthNumber>2';
                        break;
                };
                var query = "SELECT GROUP_CONCAT(CONCAT_WS('-', invoiceId, leftAmount)) as invoicesInfo, " +
                    " count(*) as count, billingAccountNumber FROM billFiles WHERE type='UNPAID' " +
                    (billingAccountNumber ? " AND billingAccountNumber=" + db.escape(billingAccountNumber) : "") +
                    (options.successSync ? " AND billingAccountNumber IN (SELECT billingAccountNumber" +
                    " FROM billFiles WHERE monthId=" + db.escape(info.currentMonthId) + " AND type='UNPAID' AND syncStatus='OK')" : "") +
                    (options.noPaymentAttempt ? " AND billingAccountNumber IN (SELECT billingAccountNumber" +
                    " FROM billFiles WHERE monthId=" + db.escape(info.currentMonthId) + " AND type='UNPAID' AND paymentAt='0000-00-00 00:00:00')" : "") +
                    (applyMonthFilter ? " AND billingAccountNumber IN (SELECT billingAccountNumber" +
                    " FROM billFiles WHERE type='UNPAID' AND " + monthNumberFilter + ")" : "") +
                    " GROUP BY billingAccountNumber" + (options.batchSize > 0 ? " LIMIT " + options.batchSize : "");

                db.query_err(query, function (err, rows) {
                    if (err) {
                        common.error("InvoiceManager", "runBatchPayment: failed to load bills, error=" + err.message);
                        return reject(err);
                    }

                    if (!rows || !rows[0]) {
                        common.error("InvoiceManager", "runBatchPayment: no unpaid bills");
                        return reject(new BaseError("No unpaid bills are found", "ERROR_NO_UNPAID_BILLS_FOUND"));
                    }

                    var batch = [];
                    rows.forEach((item) => {
                        var invoicesInfo = [];
                        item.invoicesInfo.split(',').forEach((item) => {
                            var info = item.split('-');
                            var invoiceId = info[0];
                            var amountCents = parseInt(info[1]);

                            invoicesInfo.push({
                                invoiceId: invoiceId,
                                amount: amountCents / 100
                            });
                        });

                        batch.push({
                            billingAccountNumber: item.billingAccountNumber,
                            invoicesInfo: invoicesInfo
                        });
                    });

                    info.paymentBatch = batch;
                    info.metadata.paymentBatchSize = batch.length;

                    if (log) common.log("InvoiceManager", "runBatchPayment->fetchedPayments: count=" + batch.length);
                    resolve(info);
                });
            });
        }).then((info) => {
            return new Promise((resolve, reject) => {
                progressCallback("ADDING_BATCH_TO_QUEUE", true, 0);

                var paymentManager = require('./paymentManager');
                var tasks = convertBatchedToTasks(info.paymentBatch, (payment, callback) => {
                    paymentManager.queueBillPaymentByBA(payment.billingAccountNumber, payment.invoicesInfo, options, (err) => {
                        if (err) {
                            return callback(err);
                        }
                        callback();
                    });
                });

                async.parallelLimit(tasks, 10, (err, result) => {
                    if (err) {
                        return reject(err);
                    }

                    var jobsCount = result.length;
                    info.metadata.queue = {
                        jobsCount: jobsCount
                    };

                    if (log) common.log("InvoiceManager", "runBatchPayment->addedQueueJobs: count=" + jobsCount);
                    resolve(info);
                });
            });
        }).then((info) => {
            processOperationResult(logId, undefined, info, options, lockCacheKey, progressCallback, callback);
        }).catch(function (err) {
            processOperationResult(logId, err, undefined, options, lockCacheKey, progressCallback, callback);
        });
    },

    /**
     *
     *
     */
    syncUnpaidBills: function (options, callback) {
        if (!callback) callback = () => {
        }
        if (!options) options = {};

        var logId = -1;
        var stepNumber = 0;
        var lockCacheKey = operationKey;
        var progressCacheKey = operationSyncProgressKey;

        var progressCallback = (stepName, newStep, progress, end) => {
            if (newStep) {
                stepNumber++;
                if (log) common.log("InvoiceManager", "syncUnpaidBills->(" + stepNumber + ")" + stepName);
            }
            saveProgressIntoCache(progressCacheKey, stepName, stepNumber, progress, end);
        }

        Promise.resolve({metadata: {}}).then((info) => {
            return getPromiseLogCreation("SYNC_UNPAID_BILLS", info, options, progressCallback, (id) => {
                logId = id;
            });
        }).then((info) => {
            return getPromiseLock(info, options, lockCacheKey, progressCallback);
        }).then((info) => {
            return getPromiseSyncUnpaidBills(info, options, progressCallback);
        }).then((info) => {
            return getPromiseUpdateBillsDependencies(info, options, progressCallback);
        }).then((info) => {
            processOperationResult(logId, undefined, info, options, lockCacheKey, progressCallback, callback);
        }).catch(function (err) {
            processOperationResult(logId, err, undefined, options, lockCacheKey, progressCallback, callback);
        });
    },

    /**
     *
     *
     */
    syncBills: function (monthDate, options, callback) {
        if (!callback) callback = () => {
        }
        if (!options) options = {};

        if (!monthDate || !monthDate.getTime()) {
            return callback(new BaseError("Month date is missing", "ERROR_INVALID_PARAMS"))
        }

        var logId = -1;
        var stepNumber = 0;
        var lockCacheKey = operationKey;
        var progressCacheKey = operationSyncProgressKey;

        var progressCallback = (stepName, newStep, progress, end) => {
            if (newStep) {
                stepNumber++;
                if (log) common.log("InvoiceManager", "syncBills->(" + stepNumber + ")" + stepName);
            }
            saveProgressIntoCache(progressCacheKey, stepName, stepNumber, progress, end);
        }

        Promise.resolve({metadata: {}}).then((info) => {
            return getPromiseLogCreation("SYNC_INVOICES", info, options, progressCallback, (id) => {
                logId = id;
            });
        }).then((info) => {
            return getPromiseLock(info, options, lockCacheKey, progressCallback);
        }).then((billsInfo) => {
            return new Promise((resolve, reject) => {
                progressCallback("LOAD_INVOICES", true, 0);

                // load all existing bills for a month
                // function can load up to 100,000 entries at once without any issues,
                // improvements will be required after crossing that number of customers

                elitecoreBillService.loadBillInvoices(monthDate, (err, bills) => {
                    if (err) {
                        return reject(err);
                    }
                    if (!bills || !bills[0]) {
                        return reject(new BaseError("Response is empty, no invoices are found", "ERROR_INVALID_PARAMS"));
                    }

                    var uniqueInvoices = [];
                    var uniqueInvoicesMap = {};
                    var regeneratedInvoiceIds = [];
                    var duplicatedInvoiceIds = [];
                    var processedInvoiceIds = [];

                    var invoicesCount = 0;
                    var duplicatesCount = 0;
                    var regeneratedCount = 0;
                    var paidCount = 0;
                    var unpaidCount = 0;
                    var negativeCount = 0;

                    progressCallback("LOAD_INVOICES", false, 50);

                    bills.forEach((item) => {
                        if (item.name) {
                            item.name = item.name.replace(/\s\s+/g, ' ').trim().toUpperCase();
                        }

                        var foundInvoice = uniqueInvoicesMap[item.billingAccountNumber];
                        if (!foundInvoice) {
                            invoicesCount++;
                            processedInvoiceIds.push(item.invoiceId);
                            uniqueInvoicesMap[item.billingAccountNumber] = item;
                        } else if (processedInvoiceIds.indexOf(item.invoiceId) >= 0) {
                            // found same invoice, saving into map
                            item.syncStatus = "ERROR_MULTIPLE_INVOICES";

                            duplicatesCount++;
                            duplicatedInvoiceIds.push(item.billingAccountNumber + "_" + item.invoiceId);
                            uniqueInvoicesMap[item.billingAccountNumber] = item;
                        } else {

                            regeneratedCount++;
                            regeneratedInvoiceIds.push(item.billingAccountNumber + "_" + item.invoiceId);
                            if (regeneratedInvoiceIds.indexOf(foundInvoice.billingAccountNumber + "_" + foundInvoice.invoiceId) == -1) {
                                regeneratedCount++;
                                regeneratedInvoiceIds.push(foundInvoice.billingAccountNumber + "_" + foundInvoice.invoiceId);
                            }

                            if (foundInvoice.invoiceId && item.invoiceId
                                && parseInt(item.invoiceId.substring(3)) > parseInt(foundInvoice.invoiceId.substring(3))) {
                                // found latest invoice, saving into map
                                item.syncStatus = "ERROR_MULTIPLE_REGENERATED";
                                uniqueInvoicesMap[item.billingAccountNumber] = item;
                            } else {
                                // skip, last invoice is already presented
                            }
                        }
                    });

                    Object.keys(uniqueInvoicesMap).forEach((item) => {
                        var invoice = uniqueInvoicesMap[item];
                        if (invoice.type == "PAID") paidCount++;
                        if (invoice.type == "UNPAID") unpaidCount++;
                        if (invoice.type == "NEGATIVE") negativeCount++;
                        uniqueInvoices.push(invoice);
                    });

                    if (log) common.log("InvoiceManager", "syncBills->loadInvoices: unique=" +
                    invoicesCount + ", duplicates=" + duplicatesCount + ", paid=" + paidCount + ", unpaid=" +
                    unpaidCount + ", negative=" + negativeCount + ", regenerated=" + regeneratedCount);

                    billsInfo.bills = uniqueInvoices;
                    billsInfo.metadata.invoices = {
                        invoicesCount: invoicesCount,
                        paidCount: paidCount,
                        unpaidCount: unpaidCount,
                        negativeCount: negativeCount,
                        duplicatesCount: duplicatesCount,
                        regeneratedCount: regeneratedCount,
                        duplicatedInvoiceIds: duplicatedInvoiceIds,
                        regeneratedInvoiceIds: regeneratedInvoiceIds
                    };

                    resolve(billsInfo);
                })
            });
        }).then((billsInfo) => {
            return new Promise((resolve, reject) => {
                progressCallback("LOAD_MONTH", true, 0);

                // create new month "group" or retrieve existing

                var now = common.getLocalTime();
                var currentMonthDate = new Date(now.getFullYear(), now.getMonth(), 1, 0, 0, 0);
                var isPastMonth = currentMonthDate.getTime() != monthDate.getTime();

                var query = "SELECT id FROM billMonths WHERE monthDate=" + db.escape(monthDate);
                db.query_err(query, function (err, results) {
                    if (err) {
                        return reject(err);
                    }
                    if (!results) {
                        return reject(new BaseError("SQL response is invalid", "ERROR_SQL_RESPONSE"));
                    }

                    if (results[0]) {
                        if (log) common.log("InvoiceManager", "syncBills->loadMonth: load existing month, " +
                        "date=" + monthDate.toISOString() + ", isPastMonth=" + isPastMonth);

                        billsInfo.isPastMonth = isPastMonth;
                        billsInfo.metadata.month = {
                            id: results[0].id,
                            date: monthDate,
                            isPastMonth: isPastMonth,
                            newCreated: false
                        }
                        return resolve(billsInfo);
                    }

                    var paymentMonth = monthDate.getMonth();
                    if (paymentMonth == 0) {
                        paymentMonth = 12;
                    }
                    var monthName = months[paymentMonth - 1];

                    var query = "INSERT INTO billMonths (monthDate, monthName) VALUES (" + db.escape(monthDate)
                        + "," + db.escape(monthName) + ")";
                    db.query_err(query, function (err, results) {
                        if (err) {
                            return reject(err);
                        }
                        if (!results) {
                            return reject(new BaseError("SQL response is invalid", "ERROR_SQL_RESPONSE"));
                        }

                        if (log) common.log("InvoiceManager", "syncBills->loadMonth: create new month, date=" +
                        monthDate.toISOString() + ", name=" + monthName + ", isPastMonth=" + isPastMonth);

                        billsInfo.isPastMonth = isPastMonth;
                        billsInfo.metadata.month = {
                            id: results.insertId,
                            date: monthDate,
                            name: monthName,
                            isPastMonth: isPastMonth,
                            newCreated: true
                        }

                        return resolve(billsInfo);
                    });
                });
            });
        }).then((billsInfo) => {
            return new Promise((resolve, reject) => {
                progressCallback("LOAD_PROFILES", true, 0);
                var tasksFinishedCount = 0;

                var batches = getBatches(100, billsInfo.bills);
                var tasks = convertBatchedToTasks(batches, (bills, callback) => {
                    var serviceInstanceIds = "";
                    bills.forEach((item) => {
                        if (serviceInstanceIds) serviceInstanceIds += ",";
                        serviceInstanceIds += db.escape(item.serviceInstanceNumber);
                    });

                    var query = "SELECT id, service_instance_number FROM customerProfile " +
                        "WHERE service_instance_number IN (" + serviceInstanceIds + ")";

                    db.query_err(query, function (err, results) {
                        if (err) {
                            return callback(err);
                        }
                        if (!results) {
                            return callback(new BaseError("SQL response is invalid", "ERROR_SQL_RESPONSE"));
                        }

                        var profileIdsMap = {};
                        results.forEach((item) => {
                            profileIdsMap[item.service_instance_number] = item.id;
                        });

                        bills.forEach((item) => {
                            if (profileIdsMap[item.serviceInstanceNumber]) {
                                item.profileId = profileIdsMap[item.serviceInstanceNumber];
                            } else {
                                // profile is not very important for bill sending
                                // no need to consider profile
                            }
                        });

                        notifyProgress(tasks.length, ++tasksFinishedCount, "LOAD_PROFILES", progressCallback);
                        callback();
                    });
                });

                async.parallelLimit(tasks, 10, (err, result) => {
                    if (err) {
                        return reject(err);
                    }

                    var profilesFoundCount = 0;
                    var profilesNotFoundCount = 0;

                    billsInfo.bills.forEach((item) => {
                        if (item.profileId) {
                            profilesFoundCount++;
                        } else {
                            profilesNotFoundCount++;
                        }
                    });

                    billsInfo.metadata.profiles = {
                        foundCount: profilesFoundCount,
                        notFoundCount: profilesNotFoundCount
                    };

                    if (log) common.log("InvoiceManager", "syncBills->loadProfiles: loaded count=" +
                    profilesFoundCount + ", not found count=" + profilesNotFoundCount);
                    resolve(billsInfo);
                });
            });
        }).then((billsInfo) => {
            return new Promise((resolve, reject) => {
                progressCallback("LOAD_BSS_DETAILS", true, 0);
                var tasksFinishedCount = 0;

                var batches = getBatches(400, billsInfo.bills);
                var tasks = convertBatchedToTasks(batches, (bills, callback) => {
                    var serviceInstanceIds = [];
                    bills.forEach((item) => {
                        serviceInstanceIds.push(item.serviceInstanceNumber);
                    });

                    elitecoreUserService.loadAllUsersBaseInfo({serviceInstances: serviceInstanceIds}, (err, results) => {
                        if (err) {
                            return callback(err);
                        }
                        if (!results) {
                            return callback(new BaseError("SQL response is invalid", "ERROR_SQL_RESPONSE"));
                        }

                        var profileIdsMap = {};
                        results.forEach((item) => {
                            profileIdsMap[item.serviceInstanceNumber] = item;
                        });

                        bills.forEach((item) => {

                            if (profileIdsMap[item.serviceInstanceNumber]) {
                                var customerCache = profileIdsMap[item.serviceInstanceNumber];
                                item.bssDetailsFound = true;
                                item.syncedEmail = customerCache.email;
                                item.syncedName = customerCache.billingFullName
                                    ? customerCache.billingFullName.replace(/\s\s+/g, ' ').trim().toUpperCase() : undefined;

                                if (customerCache.number) {
                                    item.syncedNumber = customerCache.number;
                                } else if (customerCache.serviceInstanceInventories
                                    && customerCache.serviceInstanceInventories[0]) {

                                    // some terminated accounts may not have primary phone number
                                    // but such account should still have an old number which can be
                                    // found in the list of inventories

                                    item.syncedNumber = customerCache.serviceInstanceInventories[0].number;
                                }

                                if (item.syncedName != item.name) {
                                    item.syncStatus = "ERROR_BSS_INCONSISTENT_NAME";
                                } else if (item.syncedEmail != item.email) {
                                    item.syncStatus = "ERROR_BSS_INCONSISTENT_EMAIL";
                                } else if (!item.syncedNumber) {
                                    item.syncStatus = "ERROR_BSS_EMPTY_NUMBER";
                                }
                            } else {
                                item.syncStatus = "ERROR_BSS_DETAILS_NOT_FOUND";
                            }
                        });

                        notifyProgress(tasks.length, ++tasksFinishedCount, "LOAD_BSS_DETAILS", progressCallback);
                        callback();
                    });
                });

                async.parallelLimit(tasks, 1, (err, result) => {
                    if (err) {
                        return reject(err);
                    }

                    var bssDetailsFoundCount = 0;
                    var bssDetailsNotFoundCount = 0;

                    billsInfo.bills.forEach((item) => {
                        if (item.bssDetailsFound) {
                            bssDetailsFoundCount++;
                        } else {
                            bssDetailsNotFoundCount++;
                        }
                    });

                    billsInfo.metadata.bssDetails = {
                        foundCount: bssDetailsFoundCount,
                        notFoundCount: bssDetailsNotFoundCount
                    };

                    if (log) common.log("InvoiceManager", "syncBills->loadBssDetails: loaded count=" +
                    bssDetailsFoundCount + ", not found count=" + bssDetailsNotFoundCount);
                    resolve(billsInfo);
                });
            });
        }).then((billsInfo) => {
            return new Promise((resolve, reject) => {
                progressCallback("LOAD_INVOICE_FILES", true, 0);

                loadInvoicesForMonth(monthDate, billsInfo.bills.length, (err, pathMap) => {
                    if (err) {
                        return reject(err);
                    }

                    var billsFoundCount = 0;
                    var billsNotFoundCount = 0;
                    var billsMultipleCount = 0;
                    var billsNegativePaid = 0;
                    var billsNegativeUnpaid = 0;
                    var multipleFiles = [];

                    billsInfo.bills.forEach((item) => {
                        var billInfo = pathMap[item.invoiceId];

                        if (item.type == "NEGATIVE") {
                            // NEGATIVE invoices should be ignored
                            // even if files are generated
                            item.filePath = "";
                        } else if (!billInfo || !billInfo.path) {
                            billsNotFoundCount++;
                            if (!billsInfo.isPastMonth) {
                                item.syncStatus = "ERROR_FILE_NOT_FOUND";
                            } else {
                                // ignore issues for past months
                                // file is not required to sync invoice
                            }
                            item.filePath = "";
                        } else if (billInfo.count > 1) {
                            billsMultipleCount++;

                            if (!billsInfo.isPastMonth) {
                                item.syncStatus = "ERROR_MULTIPLE_FILES";
                            } else {
                                // ignore issues for past months
                                // file is not required to sync invoice
                            }

                            item.filePath = "";
                            multipleFiles.push({
                                invoiceId: item.invoiceId,
                                files: billInfo.paths
                            });
                        } else {
                            billsFoundCount++;
                            item.filePath = billInfo.path;
                        }
                    });

                    billsInfo.metadata.files = {
                        foundCount: billsFoundCount,
                        notFoundCount: billsNotFoundCount,
                        multipleCount: billsMultipleCount,
                        billsNegativePaid: billsNegativePaid,
                        billsNegativeUnpaid: billsNegativeUnpaid,
                        multipleFiles: multipleFiles
                    };

                    if (log) common.log("InvoiceManager", "syncBills->loadFiles: loaded count=" + billsFoundCount +
                    ", not found count=" + billsNotFoundCount + ", multiple files count=" + billsMultipleCount);

                    resolve(billsInfo);
                }, (progress) => {
                    progressCallback("LOAD_INVOICE_FILES", false, progress);
                });
            });
        }).then((billsInfo) => {
            return new Promise((resolve, reject) => {
                progressCallback("LOAD_FILES_CONTENT", true, 0);

                if (!options || !options.verifyContent || options.verifyContent == "NONE") {
                    return resolve(billsInfo);
                }

                var runContentAnalysis = (bill, callback) => {
                    new Promise(
                        (resolve, reject) => {
                            var replied;
                            var timeout;

                            var updateStatus = (status) => {
                                if (!replied) {
                                    replied = true;
                                    if (timeout) {
                                        clearTimeout(timeout);
                                    }

                                    bill.contentStatus = status;
                                    resolve();
                                }
                            }

                            if (!bill.filePath) {
                                return updateStatus("UNKNOWN");
                            }

                            var timeout = setTimeout(function () {
                                updateStatus("ERROR_TIMEOUT");
                            }, 20000);

                            checkFilesContent(rootFolderPath, [
                                {key: "NAME", value: bill.name, page: 0},
                                {key: "INVOICE_ID", value: bill.invoiceId, page: 0}
                            ], bill.filePath, bill.billPwd, (err, result) => {
                                if (err) common.log("InvoiceManager", "checkFilesContent: error=" + err.message);
                                return updateStatus(err ? (err.status ? err.status : "ERROR") : "OK");
                            });

                        }).then((billsInfo) => {
                            callback();
                        }).catch(function (err) {
                            callback();
                        })
                }

                var tasks = [];
                var tasksFinishedCount = 0;

                var createTasksToValidateContent = (invoicesForAnalysis) => {

                    billsInfo.bills.forEach((item) => {
                        if (invoicesForAnalysis && invoicesForAnalysis.indexOf(item.invoiceId) == -1) {
                            item.contentStatusSkipped = true;
                            return;
                        }

                        (function (bill) {
                            tasks.push((callback) => {
                                runContentAnalysis(bill, (err, result) => {
                                    notifyProgress(tasks.length, ++tasksFinishedCount, "LOAD_FILES_CONTENT", progressCallback)
                                    callback(err, result)
                                });
                            });
                        }(item));
                    });

                    async.parallelLimit(tasks, 10, (err, result) => {
                        if (err) {
                            return reject(err);
                        }

                        var skippedFilesCount = 0;
                        var notFoundFilesCount = 0;
                        var validFilesCount = 0;
                        var invalidFilesCount = 0;

                        billsInfo.bills.forEach((item) => {
                            if (item.contentStatus == "OK") {
                                validFilesCount++;
                            } else if (item.contentStatus == "UNKNOWN") {
                                notFoundFilesCount++;
                            } else if (item.contentStatusSkipped) {
                                skippedFilesCount++;
                                item.contentStatus = "UNKNOWN";
                            } else {
                                invalidFilesCount++;
                            }
                        })

                        if (log) common.log("InvoiceManager", "syncBills->checkContent: valid count=" +
                        validFilesCount + ", invalid count=" + invalidFilesCount + ", skipped count=" + skippedFilesCount +
                        ", not found files count=" + notFoundFilesCount + ", checked count=" + tasksFinishedCount);

                        billsInfo.metadata.content = {
                            validCount: validFilesCount,
                            invalidCount: invalidFilesCount,
                            skippedFilesCount: skippedFilesCount,
                            notFoundFilesCount: notFoundFilesCount
                        };

                        resolve(billsInfo);
                    });
                }

                createAndClearFolder(rootFolderPath, (err, result) => {
                    if (err) {
                        return reject(err);
                    }

                    if (result && result.countRemoved > 0) {
                        if (log) common.log("InvoiceManager", "syncBills->checkContent: removed old " +
                        "temp files, count=" + result.countRemoved);
                    }

                    if (options.verifyContent == "UNKNOWN_ONLY"
                        || options.verifyContent == "FAILED_ONLY") {

                        var query = "SELECT invoiceId FROM billFiles WHERE monthId=" +
                            db.escape(billsInfo.metadata.month.id);

                        if (options.verifyContent == "UNKNOWN_ONLY") {
                            query += " AND contentStatus='UNKNOWN'";
                        } else if (options.verifyContent == "FAILED_ONLY") {
                            query += " AND contentStatus LIKE 'ERROR%'";
                        }

                        db.query_err(query, function (err, results) {
                            if (err) {
                                return reject(err);
                            }
                            if (!results) {
                                return reject(new BaseError("SQL response is invalid", "ERROR_SQL_RESPONSE"));
                            }

                            var invoices = [];
                            results.forEach((item) => {
                                invoices.push(item.invoiceId);
                            });

                            createTasksToValidateContent(invoices);
                        });
                    } else {
                        createTasksToValidateContent();
                    }
                });
            });
        }).then((billsInfo) => {
            return new Promise((resolve, reject) => {
                progressCallback("SAVE_RESULTS", true, 0);

                var tasks = [];
                var tasksFinishedCount = 0;
                var monthId = billsInfo.metadata.month.id;

                var saveBill = (monthId, bill, callback) => {
                    var profileId = (bill.profileId ? db.escape(bill.profileId) : "NULL");

                    var defaultPaymentStatus = bill.type == "PAID" || bill.type == "NEGATIVE" ? "OK" : "UNKNOWN";
                    var defaultSentStatus = "NOT_SENT";
                    var defaultCreateAt = new Date();

                    var query = "INSERT INTO billFiles (monthId, profileId, billingAccountNumber, invoiceId, " +
                        "type, sendStatus, paymentStatus, syncStatus, contentStatus, " +
                        "createdAt, filePath, totalAmount, leftAmount, syncedPwd, syncedName, syncedEmail, " +
                        "syncedNumber) VALUES (" +
                        db.escape(monthId) + ", " +
                        profileId + ", " +
                        db.escape(bill.billingAccountNumber) + ", " +
                        db.escape(bill.invoiceId) + ", " +
                        db.escape(bill.type) + ", " +
                        db.escape(defaultSentStatus) + ", " +
                        db.escape(defaultPaymentStatus) + ", " +
                        db.escape(bill.syncStatus) + ", " +
                        db.escape(bill.contentStatus) + ", " +
                        db.escape(defaultCreateAt) + ", " +
                        db.escape(bill.filePath) + ", " +
                        db.escape(bill.totalAmount) + ", " +
                        db.escape(bill.leftAmount) + ", " +
                        db.escape(bill.billPwd) + ", " +
                        db.escape(bill.syncedName) + ", " +
                        db.escape(bill.syncedEmail) + ", " +
                        db.escape(bill.syncedNumber) + ")";


                    var handleResult = (err, operation, insertId) => {
                        tasksFinishedCount++;
                        notifyProgress(tasks.length, tasksFinishedCount, "SAVE_RESULTS", progressCallback);
                        return callback(err, {operation: operation});
                    }

                    db.query_err(query, function (err, result) {
                        if (err) {

                            if (err.message && err.message.indexOf("ER_DUP_ENTRY") >= 0
                                && err.message.indexOf("unique_invoice") >= 0) {

                                var query = "UPDATE billFiles SET " +
                                    "profileId=" + profileId + ", " +
                                    "type=" + db.escape(bill.type) + ", " +
                                    "billingAccountNumber=" + db.escape(bill.billingAccountNumber) + ", " +
                                    "type=" + db.escape(bill.type) + ", " +
                                    "syncStatus=" + db.escape(bill.syncStatus) + ", " +
                                    "filePath=" + db.escape(bill.filePath) + ", " +
                                    "totalAmount=" + db.escape(bill.totalAmount) + ", " +
                                    "leftAmount=" + db.escape(bill.leftAmount) + ", " +
                                    "syncedPwd=" + db.escape(bill.billPwd) + ", " +
                                    "syncedName=" + db.escape(bill.syncedName) + ", " +
                                    "syncedEmail=" + db.escape(bill.syncedEmail) + ", " +
                                    "syncedNumber=" + db.escape(bill.syncedNumber);

                                // save content if new insert in any cases
                                // but update status only if verifyContent is TRUE
                                // it is done to avoid slow resync

                                if (options && options.verifyContent && !bill.contentStatusSkipped) {
                                    query += ", contentStatus=" + db.escape(bill.contentStatus);
                                }

                                // clear payment status if bill has been payed, success status can be synced only from bss,
                                // ops can also upload payment result file to update payment date and error statuses, that
                                // status will be cleaned after bills are payed

                                if (bill.type == "PAID" || bill.type == "NEGATIVE") {
                                    query += ", paymentStatus=" + db.escape("OK");
                                }

                                query += " WHERE invoiceId=" + db.escape(bill.invoiceId);
                                db.query_err(query, function (err) {
                                    if (err) {
                                        return handleResult(err);
                                    }
                                    return handleResult(undefined, "UPDATE", undefined);
                                });

                            } else if (err.message && err.message.indexOf("ER_DUP_ENTRY") >= 0
                                && err.message.indexOf("unique_customer_invoice_per_month") >= 0) {

                                var query = "UPDATE billFiles SET " +
                                    "profileId=" + profileId + ", " +
                                    "invoiceId=" + db.escape(bill.invoiceId) + ", " +
                                    "type=" + db.escape(bill.type) + ", " +
                                    "sendStatus=" + db.escape(defaultSentStatus) + ", " +
                                    "paymentStatus=" + db.escape(defaultPaymentStatus) + ", " +
                                    "syncStatus=" + db.escape(bill.syncStatus) + ", " +
                                    "contentStatus=" + db.escape(bill.contentStatus) + ", " +
                                    "createdAt=" + db.escape(defaultCreateAt) + ", " +
                                    "filePath=" + db.escape(bill.filePath) + ", " +
                                    "totalAmount=" + db.escape(bill.totalAmount) + ", " +
                                    "leftAmount=" + db.escape(bill.leftAmount) + ", " +
                                    "syncedPwd=" + db.escape(bill.billPwd) + ", " +
                                    "syncedName=" + db.escape(bill.syncedName) + ", " +
                                    "syncedEmail=" + db.escape(bill.syncedEmail) + ", " +
                                    "syncedNumber=" + db.escape(bill.syncedEmail) + ", " +
                                    "sentCount=0, " +
                                    "otherPaid=NULL, " +
                                    "otherUnpaid=NULL, " +
                                    "notificationActivity=NULL, " +
                                    "sentAt='0000-00-00  00:00:00', " +
                                    "paymentAt='0000-00-00  00:00:00', " +
                                    "regenerated=1 " +
                                    " WHERE monthId=" + db.escape(monthId) +
                                    " AND billingAccountNumber=" + db.escape(bill.billingAccountNumber);

                                db.query_err(query, function (err) {
                                    if (err) {
                                        return handleResult(err);
                                    }
                                    return handleResult(undefined, "UPDATE_REGENERATED", undefined);
                                });

                            } else {
                                return handleResult(err);
                            }
                        } else {
                            return handleResult(undefined, "INSERT", result.insertId)
                        }
                    });
                }

                billsInfo.bills.forEach((item) => {
                    if (!item.syncStatus) {
                        item.syncStatus = "OK";
                    }
                    if (!item.contentStatus) {
                        item.contentStatus = "UNKNOWN";
                    }

                    (function (bill) {
                        tasks.push((callback) => {
                            saveBill(monthId, bill, callback);
                        });
                    }(item));
                });

                async.parallelLimit(tasks, 50, (err, result) => {
                    if (err) {
                        return reject(err);
                    }

                    var entriesInsertedCount = 0;
                    var entriesUpdatedCount = 0;
                    var entriesUpdatedRegeneratedCount = 0;

                    result.forEach((item) => {
                        if (item.operation == "INSERT") {
                            entriesInsertedCount++;
                        } else if (item.operation == "UPDATE_REGENERATED") {
                            entriesUpdatedRegeneratedCount++;
                        } else {
                            entriesUpdatedCount++;
                        }
                    })

                    if (log) common.log("InvoiceManager", "syncBills->insertEntries: inserted count=" +
                    entriesInsertedCount + ", updated count=" + entriesUpdatedCount +
                    ", regenerated count=" + entriesUpdatedRegeneratedCount);

                    resolve(billsInfo);
                });
            });
        }).then((info) => {
            return getPromiseSyncUnpaidBills(info, options, progressCallback);
        }).then((info) => {
            return getPromiseUpdateBillsDependencies(info, options, progressCallback);
        }).then((info) => {
            processOperationResult(logId, undefined, info, options, lockCacheKey, progressCallback, callback);
        }).catch(function (err) {
            processOperationResult(logId, err, undefined, options, lockCacheKey, progressCallback, callback);
        });
    },

    /**
     *
     *
     */
    updateBillPaymentStatus: (invoiceId, payment, options, callback) => {
        if (!callback) callback = () => {
        }

        if (!invoiceId) {
            return callback(new BaseError("Invalid params, no invoice id", "ERROR_INVALID_PARAMS"))
        }

        var query;
        if (payment.paymentStatus && payment.paymentStatus == "OK") {
            query = "UPDATE billFiles SET" +
            (payment.batchStatus ? " batchStatus=" + db.escape(payment.batchStatus) + ", " : "") +
            (payment.leftAmount >= 0 ? " leftAmount=" + db.escape(payment.leftAmount) + ", " : "") +
            " type=" + db.escape(payment.leftAmount > 0 ? 'UNPAID' : 'PAID') + ", " +
            " paymentStatus=" + db.escape(payment.paymentStatus) + ", " +
            " paymentAt=" + db.escape(payment.date) +
            " WHERE invoiceId=" + db.escape(invoiceId);
        } else {

            // replace error with default if payment error is not defined properly

            if (payment.paymentStatus && payment.paymentStatus.indexOf("PF_") == -1) {
                payment.paymentStatus = "PF_OTHER";
            }

            query = "UPDATE billFiles SET" +
            (payment.batchStatus ? " batchStatus=" + db.escape(payment.batchStatus) + ", " : "") +
            (payment.leftAmount >= 0 ? " leftAmount=" + db.escape(payment.leftAmount) + ", " : "") +
            (payment.paymentStatus ? " paymentStatus=" + db.escape(payment.paymentStatus) + ", " : "") +
            " paymentAt=" + db.escape(payment.date) +
            " WHERE invoiceId=" + db.escape(invoiceId) +
            " AND type<>'PAID' AND type<>'NEGATIVE'";
        }

        db.query_err(query, function (err, result) {
            if (err) {
                if (err.message && err.message.indexOf("ER_LOCK_DEADLOCK") >= 0 && options.retryIfBlocked) {

                    // in some very rare cases transaction might be blocked, we need to wait and retry again
                    // since that is a very important to update status, sending of a correct notification depends on that

                    common.error("InvoiceManager", "[helper]updateBillPaymentStatus: ER_LOCK_DEADLOCK, re-trying...");
                    return setTimeout(() => {
                        module.exports.updateBillPaymentStatus(invoiceId,
                            payment, {retryIfBlocked: false}, callback);
                    }, 1500);
                } else {
                    return callback(err);
                }
            } else {
                callback(undefined, {
                    paymentStatus: payment.status,
                    paymentAt: payment.date,
                    dependency: result && result.metadata ? result.metadata.dependency : undefined
                });
            }
        });
    },

    /**
     *
     *
     */
    updateBillsDependencies: (billingAccountNumber, options, callback) => {
        if (!options) options = {};

        updateBillsDependencies(billingAccountNumber, {}, (err, result) => {
            if (err) {
                if (err.status == "ERROR_NO_BILLS_FOUND") {

                    // customer already terminated and does not have a bill in current billing cycle,
                    // no need to update dependencies

                    return callback(undefined, {
                        dependency: {status: "UPDATE_NOT_REQUIRED"}
                    });
                } else if (err.message && err.message.indexOf("ER_LOCK_DEADLOCK") >= 0 && options.retryIfBlocked) {

                    // in some very rare cases transaction might be blocked, we need to wait and retry again
                    // since that is a very important to update dependencies, sending of a correct notification depends on that

                    common.error("InvoiceManager", "[helper]updateBillsDependencies: ER_LOCK_DEADLOCK, re-trying...");
                    return setTimeout(() => {
                        module.exports.updateBillsDependencies(billingAccountNumber, {retryIfBlocked: false}, callback);
                    }, 1500);
                } else {
                    return callback(err);
                }
            } else {
                return callback(undefined, {
                    dependency: result && result.metadata ? result.metadata.dependency : undefined
                });
            }
        });
    },

    /**
     *
     *
     */
    updateBatchStatus: (invoiceId, transactionId, status, callback) => {
        if (!callback) callback = () => {
        }

        if (!invoiceId || !status || !transactionId) {
            return callback(new BaseError("Invalid params", "ERROR_INVALID_PARAMS"))
        }

        var query = "UPDATE billFiles SET transactionId=" + db.escape(transactionId) + ", " +
            " batchStatus=" + db.escape(status) +
            " WHERE invoiceId=" + db.escape(invoiceId);

        db.query_err(query, function (err, result) {
            if (err) {
                return callback(err);
            }
            return callback();
        });
    }
}

// handling sync success / failure

function processOperationResult(logId, err, info, options, lockCacheKey, progressCallback, callback) {
    if (!callback) callback = () => {
    }
    if (!progressCallback) progressCallback = () => {
    }

    if (!err || err.status != "ERROR_OPERATION_LOCKED") {
        if (log) common.log("InvoiceManager", "[helper]processOperationResult: remove lock");
        progressCallback(err ? "SYNC_ERROR" : "SYNC_SUCCESS", true, 100, true);
        db.unlockOperation(lockCacheKey);
    }

    var result = info ? info.metadata : {};
    var status = err ? (err.status ? err.status : "ERROR") : "OK";
    var errorMessage = err ? err.message : undefined;

    if (log) common.log("InvoiceManager", "[helper]processOperationResult: finished" +
    (errorMessage ? " with an error, error=" + errorMessage : " successfully"));

    if (logId) {
        if (log) common.log("InvoiceManager", "[helper]processOperationResult: update log, logId=" + logId);
        db.notifications_invoices_sync.update({_id: logId}, {
                $set: {
                    status: status,
                    error: errorMessage,
                    result: result
                }
            }, {multi: true},
            (logErr, result) => {
                if (logErr) common.error("InvoiceManager", "[helper]processOperationResult->updateLog: " +
                "failed to update log, error=" + logErr.message);
                // do nothing
            });
    }

    setTimeout(() => {
        callback(err, result);
    }, 200);
}

// progress handling

function saveProgressIntoCache(progressCacheKey, stepName, stepNumber, stepProgress, end) {
    db.cache.put("cache", progressCacheKey, {
        stepName: stepName,
        stepNumber: stepNumber,
        stepsCount: -1,
        stepProgress: stepProgress
    }, end ? 3000 : 300 * 1000);
}

// predefined promises

function getPromiseLogCreation(type, returnInfo, options, progressCallback, logCallback) {
    if (!options) options = {};
    if (!returnInfo) returnInfo = {};
    if (!returnInfo.metadata) returnInfo.metadata = {};
    if (!progressCallback) progressCallback => () => {
    }

    return new Promise((resolve, reject) => {
        var obj = {
            status: "IN_PROGRESS",
            error: "NONE",
            type: type,
            reportId: options.reportId,
            initiator: options.initiator,
            params: {
                options: options
            },
            result: {},
            ts: new Date().getTime()
        }

        db.notifications_invoices_sync.insert(obj, function (err, result) {
            if (err) {
                common.error("InvoiceManager", "[promise]getPromiseLogCreation: failed to save sync " +
                "log, error=" + err.message);
                return reject(err);
            }

            returnInfo.logId = result && result.insertedIds ? result.insertedIds[0] : undefined;
            if (logCallback) logCallback(returnInfo.logId);
            if (log) common.log("InvoiceManager", "[promise]getPromiseLogCreation->log: id=" + returnInfo.logId);

            resolve(returnInfo);
        });
    });
}

function getPromiseLock(returnInfo, options, operationKey, progressCallback) {
    if (!returnInfo) returnInfo = {};
    if (!returnInfo.metadata) returnInfo.metadata = {};
    if (!progressCallback) progressCallback => () => {
    }

    return new Promise((resolve, reject) => {
        if (!options || options.overrideLock) {
            return resolve(returnInfo);
        }

        db.lockOperation(operationKey, {}, 5 * 60 * 1000, function (err) {
            if (err) {
                common.error("InvoiceManager", "[promise]getPromiseLock->lock: failed for " +
                "start operation, err=" + err.message);
                return reject(err);
            }

            resolve(returnInfo);
        });
    });
}

function getPromiseUpdateBillsDependencies(returnInfo, options, progressCallback) {
    if (!returnInfo) returnInfo = {};
    if (!returnInfo.metadata) returnInfo.metadata = {};
    if (!progressCallback) progressCallback => () => {
    }

    return new Promise((resolve, reject) => {
        updateBillsDependencies(undefined, {}, (err, result) => {
            if (err) {
                return reject(err);
            }

            returnInfo.metadata.dependency = result.metadata.dependency;
            resolve(returnInfo);
        }, progressCallback);
    });
}

function getPromiseSyncUnpaidBills(returnInfo, options, progressCallback) {
    if (!returnInfo) returnInfo = {};
    if (!returnInfo.metadata) returnInfo.metadata = {};
    if (!progressCallback) progressCallback => () => {
    }

    return new Promise((resolve, reject) => {
        syncUnpaidBills((err, result) => {
            if (err) {
                return reject(err);
            }

            returnInfo.metadata.localUnpaidInvoices = result.metadata.localUnpaidInvoices;
            returnInfo.metadata.bssUnpaidInvoices = result.metadata.bssUnpaidInvoices;
            resolve(returnInfo);
        }, progressCallback);
    });
}


// sync operation functions

function syncUnpaidBills(callback, progressCallback) {
    if (!callback) callback = () => {
    }
    if (!progressCallback) progressCallback = () => {
    }

    if (log) common.log("InvoiceManager", "[helper]syncUnpaidBills: start sync...");
    Promise.resolve({metadata: {}}).then((info) => {
        return new Promise((resolve, reject) => {
            progressCallback("LOAD_UNPAID_LOCAL", true, 0);
            module.exports.loadBills(undefined, {
                type: "UNPAID",
                noLengthCheck: true
            }, (err, result) => {
                if (err) {
                    return reject(err);
                }

                var unpaidInvoicesIds = [];
                var unpaidInvoicesMap = {};
                if (result && result.data) {
                    result.data.forEach((item) => {
                        unpaidInvoicesIds.push(item.invoiceId);
                        unpaidInvoicesMap[item.invoiceId] = item;
                    });
                }

                if (log) common.log("InvoiceManager", "[helper]syncUnpaidBills->loadUnpaidLocalInvoices: " +
                "count=" + unpaidInvoicesIds.length);

                info.unpaidInvoicesIds = unpaidInvoicesIds;
                info.unpaidInvoicesMap = unpaidInvoicesMap;
                info.metadata.localUnpaidInvoices = {
                    countUnpaid: unpaidInvoicesIds.length
                };
                resolve(info)
            });
        });
    }).then((info) => {
        return new Promise((resolve, reject) => {
            progressCallback("LOAD_INVOICES_INFO", true, 0);
            var tasksFinishedCount = 0;

            var batches = getBatches(500, info.unpaidInvoicesIds);
            var tasks = convertBatchedToTasks(batches, (invoicesIds, callback) => {
                elitecoreBillService.loadBillInvoicesByIds(invoicesIds, (err, result) => {
                    if (err) {
                        return callback(err);
                    }

                    var processedResult = {notFoundInvoiceIds: [], invoices: []};
                    var foundInvoiceIds = [];

                    if (result) result.forEach((item) => {
                        foundInvoiceIds.push(item.invoiceId);
                        processedResult.invoices.push(item);
                    });

                    invoicesIds.forEach((item) => {
                        if (foundInvoiceIds.indexOf(item) == -1) {
                            processedResult.notFoundInvoiceIds.push(item);
                        }
                    });

                    notifyProgress(tasks.length, ++tasksFinishedCount, "LOAD_INVOICES_INFO", progressCallback);
                    callback(undefined, processedResult);
                });
            });

            async.parallelLimit(tasks, 1, (err, result) => {
                if (err) {
                    return reject(err);
                }

                var notFoundInvoiceIds = [];
                var changedPaymentStatus = [];
                var changedInvoicesIdsTop = [];
                if (result) result.forEach((processedResult) => {
                    processedResult.notFoundInvoiceIds.forEach((invoiceId) => {
                        notFoundInvoiceIds.push(invoiceId);
                    });
                    processedResult.invoices.forEach((invoice) => {
                        var originalInvoice = info.unpaidInvoicesMap[invoice.invoiceId];
                        if (invoice.type != "UNPAID" || (originalInvoice && invoice.leftAmount != originalInvoice.leftAmount)) {
                            changedPaymentStatus.push(invoice);
                            if (changedInvoicesIdsTop.length < 5) {
                                changedInvoicesIdsTop.push(invoice.invoiceId);
                            }
                        }
                    });
                });

                info.notFoundInvoiceIds = notFoundInvoiceIds;
                info.changedPaymentStatus = changedPaymentStatus;
                info.metadata.bssUnpaidInvoices = {
                    changedStatusCount: changedPaymentStatus.length,
                    notFoundInvoiceIdsCount: notFoundInvoiceIds.length,
                    notFoundInvoiceIds: notFoundInvoiceIds,
                    changedInvoicesIdsTop: changedInvoicesIdsTop
                };

                if (log) common.log("InvoiceManager", "[helper]syncUnpaidBills->loadInvoiceInfo: " +
                "changedStatusCount=" + changedPaymentStatus.length + ", notFoundInvoiceIds=" + notFoundInvoiceIds.length);
                resolve(info);
            });
        });
    }).then((info) => {
        if (!info) info = {metadata: {}};

        return new Promise((resolve, reject) => {
            progressCallback("SAVE_PAID_STATUS", true, 0);

            var tasks = [];
            var tasksFinishedCount = 0;

            var updateInvoiceStatus = (invoice, callback) => {
                var query = "UPDATE billFiles SET" +
                    " type=" + db.escape(invoice.type) + "," +
                    " leftAmount=" + db.escape(invoice.leftAmount) + "," +
                    " totalAmount=" + db.escape(invoice.totalAmount) + "," +
                    " paymentStatus='OK'" +
                    " WHERE invoiceId=" + db.escape(invoice.invoiceId);

                db.query_err(query, function (err, result) {
                    if (err) {
                        return callback(err);
                    }

                    notifyProgress(tasks.length, ++tasksFinishedCount, "LOAD_INVOICES_INFO", progressCallback);
                    callback(undefined, {status: result.affectedRows == 0 ? "IGNORED" : "UPDATED"});
                });
            }

            info.changedPaymentStatus.forEach((item) => {
                (function (invoice) {
                    tasks.push((callback) => {
                        updateInvoiceStatus(invoice, callback);
                    });
                }(item));
            });

            async.parallelLimit(tasks, 50, (err, result) => {
                if (err) {
                    return reject(err);
                }

                if (log) common.log("InvoiceManager", "[helper]updateBillsDependencies->updatePaidStatus: " +
                "updated count=" + result.length);
                resolve(info);
            });
        });
    }).then((info) => {
        if (log) common.log("InvoiceManager", "[helper]syncUnpaidBills: finished, remove lock...");
        callback(undefined, info);
    }).catch(function (err) {
        common.error("InvoiceManager", "[helper]syncUnpaidBills: error=" + (err ? err.message : "Unknown error")
        + ", status=" + (err && err.status ? err.status : "NONE"));
        callback(err);
    });
}

function updateBillsDependencies(billingAccountNumber, options, callback, progressCallback) {
    if (!options) options = {};
    if (!callback) callback = () => {
    }
    if (!progressCallback) progressCallback = () => {
    }

    if (log) common.log("InvoiceManager", "[helper]updateInvoiceDependencies: start sync...");
    Promise.resolve().then((info) => {
        if (!info) info = {metadata: {}};

        return new Promise((resolve, reject) => {
            progressCallback("LOAD_DEPENDENCIES", true, 0);

            // load last month invoice to find if there were any payments for old bills this month,
            // also check if there are any other open old bills

            var query = "SELECT id, monthDate, monthName FROM billMonths ORDER BY monthDate DESC LIMIT 1";
            db.query_err(query, function (err, results) {
                if (err) {
                    return reject(err);
                }
                if (!results || !results[0] || !results[0].id) {
                    return reject(new BaseError("Last month not found", "ERROR_NO_LAST_MONTH"));
                }

                var monthName = results[0].monthName;
                var monthDate = results[0].monthDate;
                var monthId = results[0].id;

                var handleBills = (sqlMessage) => {
                    var params = {noLengthCheck: true};
                    if (billingAccountNumber) {
                        params.billingAccountNumbers = [billingAccountNumber];
                    }

                    module.exports.loadBills(monthId, params, (err, result) => {
                        if (err) {
                            return reject(err);
                        }

                        if (!result || !result.data || !result.data[0]) {
                            return reject(new BaseError("Bills not found", "ERROR_NO_BILLS_FOUND"));
                        }
                        var allInvoices = result.data;

                        var tasksFinishedCount = 0;
                        var batches = getBatches(500, allInvoices);
                        var tasks = convertBatchedToTasks(batches, (invoices, callback) => {
                            var bans = [];
                            var invoicesMap = {};

                            invoices.forEach((item) => {
                                bans.push(item.billingAccountNumber);
                                invoicesMap[item.billingAccountNumber] = item;

                                //clear prev value
                                item.otherUnpaid = "";
                                item.otherPaid = "";
                            });

                            module.exports.loadBills(undefined, {
                                type: "UNPAID",
                                monthDateLt: monthDate,
                                billingAccountNumbers: bans,
                                noLengthCheck: true,
                                hasPaymentDate: true
                            }, (err, result) => {
                                if (err) {
                                    return callback(err);
                                }

                                if (result && result.data) result.data.forEach((item) => {
                                    var invoice = invoicesMap[item.billingAccountNumber];
                                    if (invoice) {
                                        if (invoice.otherUnpaid) invoice.otherUnpaid += ',';
                                        invoice.otherUnpaid += item.invoiceId;
                                    }
                                });

                                module.exports.loadBills(undefined, {
                                    type: "PAID",
                                    paymentAtGt: monthDate,
                                    monthIdNot: monthId,
                                    billingAccountNumbers: bans,
                                    noLengthCheck: true,
                                    hasPaymentDate: true
                                }, (err, result) => {
                                    if (err) {
                                        return callback(err);
                                    }

                                    if (result && result.data) result.data.forEach((item) => {
                                        var invoice = invoicesMap[item.billingAccountNumber];
                                        if (invoice) {
                                            if (invoice.otherPaid) invoice.otherPaid += ',';
                                            invoice.otherPaid += item.invoiceId;
                                        }
                                    });
                                    module.exports.loadBillsCount(bans, {}, (err, result) => {
                                        if (err) {
                                            return callback(err);
                                        }

                                        if (result && result.data) result.data.forEach((item) => {
                                            var invoice = invoicesMap[item.billingAccountNumber];
                                            if (invoice) {
                                                invoice.monthNumber = item.count;
                                            }
                                        });
                                        notifyProgress(tasks.length, ++tasksFinishedCount, "LOAD_DEPENDENCIES", progressCallback);
                                        callback(undefined, {});
                                    });
                                });
                            });
                        });

                        async.parallelLimit(tasks, 10, (err, result) => {
                            if (err) {
                                return reject(err);
                            }

                            var totalCount = 0;
                            var otherUnpaidCount = 0;
                            var otherPaidCount = 0;

                            allInvoices.forEach((item) => {
                                totalCount++;
                                if (item.otherUnpaid) {
                                    otherUnpaidCount++;
                                }
                                if (item.otherPaid) {
                                    otherPaidCount++;
                                }
                            });

                            if (log) common.log("InvoiceManager", "[helper]updateBillsDependencies->loadDependencies:" +
                            " total count=" + totalCount + ", other unpaid count=" + otherUnpaidCount +
                            ", other paid count=" + otherPaidCount + ", monthName=" + monthName);

                            info.invoices = allInvoices;
                            info.metadata.dependency = {
                                totalCount: totalCount,
                                otherUnpaidCount: otherUnpaidCount,
                                otherPaidCount: otherPaidCount
                            }

                            if (billingAccountNumber) {
                                // single customer update, can save full logs
                                info.metadata.dependency.sqlMessageClear = sqlMessage;
                                info.metadata.dependency.invoices = allInvoices;
                            }

                            resolve(info);
                        });
                    });
                }

                if (options.onlyRead) {
                    handleBills();
                } else {
                    var query = "UPDATE billFiles SET otherPaid=NULL, otherUnpaid=NULL, monthNumber=0 WHERE monthId<>" + db.escape(monthId) +
                        (billingAccountNumber ? " AND billingAccountNumber=" + db.escape(billingAccountNumber) : "") +
                        " AND (otherPaid IS NOT NULL OR otherUnpaid IS NOT NULL OR monthNumber <>0)";

                    db.query_err(query, function (err, result) {
                        if (!err) {
                            handleBills(result ? result.message : undefined);
                        } else if (err.message && err.message.indexOf("ER_LOCK_DEADLOCK") >= 0) {

                            // in some very rare cases transaction might be blocked, we need to wait and retry again
                            // since that is a very important to update status, sending of a correct notification depends on that

                            common.error("InvoiceManager", "[helper]updateBillsDependencies->resetDependencies:" +
                            " ER_LOCK_DEADLOCK, re-trying...");
                            return setTimeout(() => {
                                db.query_err(query, function (err, result) {
                                    if (err) {
                                        return reject(err);
                                    }
                                    handleBills(result ? result.message : undefined);
                                });
                            }, 1500);
                        } else {
                            return reject(err);
                        }
                    });
                }
            })
        });
    }).then((info) => {
        if (!info) info = {metadata: {}};

        return new Promise((resolve, reject) => {
            progressCallback("UPDATE_DEPENDENCIES", true, 0);

            var tasks = [];
            var tasksFinishedCount = 0;
            var saveDependencyStatus = (invoice, callback) => {
                var otherUnpaidValue = (invoice.otherUnpaid ? db.escape(invoice.otherUnpaid) : "NULL");
                var otherPaidValue = (invoice.otherPaid ? db.escape(invoice.otherPaid) : "NULL");
                var monthNumber = (invoice.monthNumber ? db.escape(invoice.monthNumber) : "0");

                var query = "UPDATE billFiles SET monthNumber=" + monthNumber + ", " +
                    "otherUnpaid=" + otherUnpaidValue + ", " +
                    " otherPaid=" + otherPaidValue + " WHERE id=" + db.escape(invoice.id) +
                    " AND (otherUnpaid" + (otherUnpaidValue == "NULL" ? " IS NOT NULL"
                        : "<>" + otherUnpaidValue + " OR otherUnpaid IS NULL") +
                    " OR otherPaid" + (otherPaidValue == "NULL" ? " IS NOT NULL"
                        : "<>" + otherPaidValue + " OR otherPaid IS NULL") + " OR monthNumber <>" +
                        monthNumber + ")";

                var handleSuccessResult = (result) => {
                    if (billingAccountNumber) {
                        // single customer update, can save extra logs
                        info.metadata.dependency.sqlMessageUpdate = result ? result.message : undefined;
                    }
                    notifyProgress(tasks.length, ++tasksFinishedCount, "UPDATE_DEPENDENCIES", progressCallback);
                    return callback(undefined, {});
                }

                db.query_err(query, function (err, result) {
                    if (!err) {
                        return handleSuccessResult(result);
                    } else if (err.message && err.message.indexOf("ER_LOCK_DEADLOCK") >= 0) {

                        // in some very rare cases transaction might be blocked, we need to wait and retry again
                        // since that is a very important to update status, sending of a correct notification depends on that

                        common.error("InvoiceManager", "[helper]updateBillsDependencies->updateDependencies:" +
                        " ER_LOCK_DEADLOCK, invoice id " + invoice.id + " re-trying...");
                        return setTimeout(() => {
                            db.query_err(query, function (err, result) {
                                if (err) {
                                    return callback(err);
                                }
                                handleSuccessResult(result);
                            });
                        }, 1500);
                    } else {
                        return callback(err);
                    }
                });
            }

            if (options.onlyRead) {
                resolve(info);
            } else {
                info.invoices.forEach((item) => {
                    (function (invoice) {
                        tasks.push((callback) => {
                            saveDependencyStatus(invoice, callback);
                        });
                    }(item));
                });

                async.parallelLimit(tasks, 50, (err, result) => {
                    if (err) {
                        return reject(err);
                    }

                    if (log) common.log("InvoiceManager", "[helper]updateBillsDependencies->updateDependencies: " +
                    "updated count=" + result.length);
                    resolve(info);
                });
            }
        });
    }).then((info) => {
        if (log) common.log("InvoiceManager", "[helper]updateBillsDependencies: finished");
        callback(undefined, info);
    }).catch(function (err) {
        common.error("InvoiceManager", "[helper]updateBillsDependencies: error=" + (err ? err.message : "Unknown error")
        + ", status=" + (err && err.status ? err.status : "NONE"));
        callback(err);
    });
}

// help functions

function loadInvoicesForMonth(monthDate, expectedCount, callback, progressCallback) {
    if (!monthDate) {
        return callback(new BaseError("Invalid params", "ERROR_INVALID_PARAMS"));
    }

    // need to update folder structure, all files should be located in the same folder
    // type (PAID, UNPAID, PARTIALLY_PAID, NEGATIVE) will be directly take from BSS

    var folderName = monthDate.toISOString().split('T')[0];
    var countHandled = 0;
    var pathMap = {};

    var loadInvoices = (index, folderName, pathMap) => {
        if (index == 0) {
            return loadInvoices(1, folderName, pathMap);
        }
        if (index > 99) {
            return callback(undefined, pathMap);
        }

        var folderIndex = index < 10 ? ("0" + index) : ("" + index);
        var folderPath = pdfRootFolder + '/live/all/' + folderName + '/' + folderIndex;
        common.log("InvoiceManager", "loadInvoicesForMonth: monthDate=" + folderName + ", countHandled=" + countHandled +
        ", folderIndex=" + folderIndex + ", folderPath=" + folderPath)

        loadInvoicesForFolder(folderPath, pathMap, (err, count) => {
            if (err) {
                if (index == 1) {
                    var folderPath = pdfRootFolder + '/live/all/' + folderName;
                    common.log("InvoiceManager", "loadInvoicesForMonth: monthDate=" + folderName + ", folderPath=" + folderPath)

                    return loadInvoicesForFolder(folderPath, pathMap, (err, count) => {
                        countHandled = count;
                        if (progressCallback) progressCallback(100);
                        return callback(undefined, pathMap, countHandled);
                    });
                } else {
                    if (progressCallback) progressCallback(100);
                    return callback(undefined, pathMap, countHandled);
                }
                return callback(err);
            }

            countHandled += count;
            if (progressCallback) {
                var progress = parseInt((countHandled / expectedCount) * 100);
                progressCallback(progress > 100 ? 100 : progress);
            }

            loadInvoices(index + 1, folderName, pathMap);
        });
    }

    loadInvoices(1, folderName, pathMap);
}

function loadInvoicesForFolder(path, pathMap, callback) {
    fs.readdir(path, (err, files) => {
        if (err) {
            return callback(err);
        }
        if (!files) {
            return callback(new Error("Not found or invalid"));
        }

        var count = 0;
        files.forEach((name) => {
            if (name.indexOf("LW") == 0 && name.indexOf("_") > 0 && name.indexOf(".pdf") > 0) {
                var filePath = path + "/" + name;
                var invoiceId = name.split('_')[1].split('.pdf')[0];
                if (pathMap[invoiceId]) {
                    if (!pathMap[invoiceId].paths) {
                        pathMap[invoiceId].paths = [];
                        pathMap[invoiceId].paths.push(pathMap[invoiceId].path);
                    }
                    pathMap[invoiceId].count++;
                    pathMap[invoiceId].paths.push(filePath);
                } else {
                    count++;
                    pathMap[invoiceId] = {
                        path: filePath,
                        count: 1
                    };
                }
            }
        });

        callback(undefined, count);
    });
}

/**
 * https://github.com/nisaacson/pdf-text-extract
 *
 * @param valuesToVerify
 * @param filePath
 * @param filePwd
 * @param callback
 */
function checkFilesContent(tempFolder, valuesToVerify, filePath, filePwd, callback) {
    var globalPath = filePath;
    var tempName = new Date().getTime() + "_" + Math.floor(Math.random() * 8999999 + 1000000) + ".pdf";
    var localPath = tempFolder + "/" + tempName;

    var startTime = new Date().getTime();

    copyFile(globalPath, localPath, (err) => {
        if (err) {
            return callback(new BaseError(err.message, "ERROR_FAILED_TO_COPY_PDF"));
        }

        extract(localPath, {ownerPassword: filePwd}, "pdftotext", function (err, pages) {
            var checkError = undefined;
            if (err) {
                if (err.message.indexOf("Incorrect password")) {
                    checkError = new BaseError("Failed to open file " + globalPath + " using pwd " + filePwd,
                        "ERROR_INVALID_PDF_PWD");
                } else {
                    checkError = new BaseError(err.message,
                        "ERROR_FAILED_TO_READ_PDF");
                }
            } else if (!pages || !pages[0]) {
                checkError = new BaseError("Invalid pdf content",
                    "ERROR_INVALID_PDF_CONTENT");
            } else {
                valuesToVerify.some((item) => {
                    var words = [];
                    if (item.value) {
                        words = item.value.split(/[\s,\.\-']+/);
                    }

                    var notWordLast;
                    var countFound = 0;

                    for (var i = 0; i < words.length; i++) {
                        var invalid = /[°"§%()\[\]{}=\\?´`'#<>|,;.:+_-]+/g;
                        var wordsPepl = words[i].replace(invalid, "");

                        if (pages[item.page].search(new RegExp(wordsPepl, "i")) >= 0) {
                            countFound++;
                        } else {
                            notWordLast = wordsPepl;
                        }
                    }

                    if ((words.length < 3 && countFound != words.length) || (words.length >= 3 && countFound < 2)) {
                        checkError = new BaseError("Failed to find " + item.key + " '" + item.value + "'('" + notWordLast + "') " +
                        "in content of file " + globalPath, "ERROR_INVALID_CONTENT_" + item.key);
                        return true;
                    }
                })
            }

            removeFile(localPath, (err) => {
                if (err) {
                    return callback(new BaseError(err.message, "ERROR_FAILED_TO_DELETE_TEMP_PDF"));
                }

                var time = new Date().getTime() - startTime;
                callback(checkError, {
                    metadata: {
                        time: time
                    }
                });
            });
        })
    });
}

function copyFile(source, target, callback) {
    var callbackCalled = false;
    var done = (err) => {
        if (!callbackCalled) {
            callbackCalled = true
            callback(err);
        }
    }

    try {
        var rd = fs.createReadStream(source);
        rd.on("error", done);
        var wr = fs.createWriteStream(target);
        wr.on("error", done);
        wr.on("close", function (ex) {
            done();
        });
        rd.pipe(wr);
    } catch (err) {
        done(err);
    }
}

function createAndClearFolder(path, callback) {
    fs.exists(path, function (exists) {
        if (!exists) {
            fs.mkdir(path, function (err) {
                if (err) {
                    return callback(err)
                }
                callback();
            });
        } else {
            fs.readdir(path, (err, files) => {
                if (err) {
                    return callback(err);
                }
                if (!files || files.length == 0) {
                    return callback();
                }

                var countToRemove = 0;
                var countRemoved = 0;

                files.forEach((name) => {

                    if (name.indexOf(".pdf") > 0) {
                        countToRemove++;

                        var target = path + "/" + name;
                        removeFile(target, () => {
                            countRemoved++;

                            if (countToRemove == countRemoved) {
                                callback(undefined, {countRemoved: countRemoved});
                            }
                        })
                    }
                });
            });
        }
    });
}

function removeFile(target, callback) {
    fs.unlink(target, function (err) {
        callback(err);
    });
}

function loadPaymentsResultsFromXLS(fileId, type, callback) {
    fileManager.loadParsedXLSFile(fileId, type, (err, content) => {
        if (err) {
            return callback(err);
        }
        if (!content[0] || !content[0].data[0]) {
            return callback(new BaseError("Invalid response"));
        }

        var jobNumberColumn = -1;
        var errorCodeColumn = -1;
        var responseCodeColumn = -1;
        var statusColumn = -1;
        var timeColumn = -1;
        var amountColumn = -1;
        var headers = content[0].data[0];

        headers.forEach((item, pos) => {
            if (item == 'Sales Draft No/Job Number') {
                jobNumberColumn = pos;
            } else if (item == 'Error Code') {
                errorCodeColumn = pos;
            } else if (item == 'Response Code') {
                responseCodeColumn = pos;
            } else if (item == 'Status') {
                statusColumn = pos;
            } else if (item == 'Trn Date') {
                timeColumn = pos;
            } else if (item == 'Amount') {
                amountColumn = pos;
            }
        });

        if (!jobNumberColumn || !errorCodeColumn || !responseCodeColumn) {
            return callback(new BaseError("Some columns are not found (Sales Draft No/Job Number, Error Code, Response Code)"));
        }

        var failedPayments = [];
        var successPayments = [];
        var errorsCount = 0;
        var defaultStatusCount = 0;

        var paymentManager = require('./paymentManager');
        content[0].data.forEach((item, pos) => {
            if (pos == 0) {
                // skip headers
                return;
            }

            var jobNumber = item[jobNumberColumn];
            var status = item[statusColumn];
            var errorCode = item[errorCodeColumn] ? parseInt(item[errorCodeColumn]) : 0;
            var responseCode = item[responseCodeColumn] ? parseInt(item[responseCodeColumn]) : 0;
            var amount = item[amountColumn] ? parseFloat(item[amountColumn]) * 100 : 0;
            var time = item[timeColumn] ? new Date(item[timeColumn]) : undefined;
            var invoiceId = jobNumber.indexOf("REG") >= 0 && jobNumber.indexOf("-") >= 0 ? jobNumber.split('-')[0] : undefined;

            if (!invoiceId) {
                errorsCount++;
                return;
            }
            if (time && !time.getTime()) {
                time = undefined;
            }

            if (status == "FAILED") {
                if (!errorCode) errorCode = -1;
                if (!responseCode) responseCode = -1;
                var reasonStatus = paymentManager.parseWirecardError(errorCode, responseCode);

                if (reasonStatus == "PF_OTHER") {
                    defaultStatusCount++;
                }

                failedPayments.push({
                    invoiceId: invoiceId,
                    status: reasonStatus,
                    errorCode: errorCode,
                    responseCode: responseCode,
                    time: time
                });
            } else {
                successPayments.push({
                    invoiceId: invoiceId,
                    amount: amount,
                    status: "OK",
                    time: time
                });
            }
        });

        callback(undefined, {success: successPayments, failure: failedPayments});
    });
}

function getBatches(batchSize, array) {
    var batches = [];
    var batch = [];

    if (array) array.forEach((item) => {
        if (batch.length < batchSize) {
            batch.push(item);
        } else {
            batch.push(item);
            batches.push(batch);
            batch = [];
        }
    });

    if (batch.length > 0) {
        batches.push(batch);
        batch = [];
    }

    return batches;
}

function convertBatchedToTasks(batches, functionRef) {
    var tasks = [];
    batches.forEach((batch) => {
        (function (functionBatch) {
            tasks.push((callback) => {
                functionRef(functionBatch, callback);
            });
        }(batch));
    });
    return tasks;
}

function notifyProgress(tasksCount, tasksFinishedCount, operationName, progressCallback) {
    if (tasksFinishedCount == 1 || tasksCount < 100
        || tasksFinishedCount < (tasksCount * 0.1) || tasksFinishedCount > (tasksCount * 0.9)
        || (tasksFinishedCount % parseInt(tasksCount / 100)) == 0) {
        var progress = Math.round((tasksFinishedCount / tasksCount) * 100);
        if (progressCallback) progressCallback(operationName, false, progress);
    }
}
