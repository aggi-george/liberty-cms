var async = require('async');
var dateformat = require('dateformat');
var fs = require('fs');

var config = require('../../../../config');
var common = require('../../../../lib/common');
var db = require('../../../../lib/db_handler');
var resourceManager = require('../../../../res/resourceManager');
var notificationSend = require(__core + '/manager/notifications/send');

var elitecoreConnector = require('../../../../lib/manager/ec/elitecoreConnector');
var elitecoreBillService = require('../../../../lib/manager/ec/elitecoreBillService');
var elitecoreUserService = require('../../../../lib/manager/ec/elitecoreUserService');
var paymentManager = require('./paymentManager');
var invoiceManager = require('./invoiceManager');

var BaseError = require('../../errors/baseError');

var log = config.LOGSENABLED;
var operationKey = "billFilesSyncSending";
var operationSyncProgressKey = "billFilesSyncSendingProgress";

module.exports = {


    /**
     *
     *
     */
    getFailureReasonText: (errorStatus) => {
        return getFailureReasonText(errorStatus)
    },

    /**
     *
     *
     */
    sendAllBills: (monthId, options, callback) => {
        if (!callback) callback = () => {
        }
        if (!options) options = {};
        var logId;

        if (!monthId || !options.batchSize) {
            return callback(new BaseError("Month ID is missing or batch size is not specified", "ERROR_INVALID_PARAMS"))
        }

        var updateCache = (stepName, stepNumber, stepsCount, stepProgress, end) => {
            db.cache.put("cache", operationSyncProgressKey, {
                stepName: stepName,
                stepNumber: stepNumber,
                stepsCount: stepsCount,
                stepProgress: stepProgress
            }, end ? 3000 : 300 * 1000);
        }

        var stepsCount = 3;
        var stepNumber = 0;

        if (log) common.log("InvoiceNotificationManager", "sendAllBills: start sync...");
        Promise.resolve().then(() => {
            return new Promise((resolve, reject) => {
                var obj = {
                    status: "IN_PROGRESS",
                    error: "NONE",
                    type: "SEND_ALL_PDF_BILLS",
                    initiator: options.initiator,
                    params: {
                        monthId: monthId,
                        options: options
                    },
                    result: {},
                    ts: new Date().getTime()
                }

                db.notifications_invoices_sync.insert(obj, function (err, result) {
                    if (err) {
                        common.error("InvoiceNotificationManager", "sendAllBills->saveLog: failed to save sync " +
                        "log, error=" + err.message);
                        return reject(err);
                    }

                    logId = result && result.insertedIds ? result.insertedIds[0] : undefined;
                    resolve({metadata: {}});
                });
            });
        }).then((info) => {
            return new Promise((resolve, reject) => {
                updateCache("CHECK_LOCK", ++stepNumber, stepsCount, 0);

                if (!options || options.overrideLock) {
                    return resolve(info);
                }

                db.lockOperation(operationKey, {}, 5 * 60 * 1000, function (err) {
                    if (err) {
                        common.error("InvoiceNotificationManager", "sendAllBills->lock: failed for " +
                        "start operation, err=" + err.message);
                        return reject(err);
                    }

                    resolve(info);
                });
            });
        }).then((info) => {
            return new Promise((resolve, reject) => {
                updateCache("LOAD_BILLS", ++stepNumber, stepsCount, 0);

                invoiceManager.loadBills(monthId, {
                    status: options.resendFailed ? "SEND_NEED_TO_SEND_AND_FAILED" : "SEND_NEED_TO_SEND",
                    type: options.type,
                    limit: options.batchSize,
                    paymentType: options.paymentType
                }, (err, result) => {
                    if (err) {
                        common.error("InvoiceNotificationManager", "sendAllBills->loadBillsToSend: failed load " +
                        "bills, err=" + err.message);
                        return reject(err);
                    }
                    if (!result || !result.data || !result.data.length) {
                        common.error("InvoiceNotificationManager", "sendAllBills->loadBillsToSend: failed load bills");
                        return reject(new BaseError("No Bills Found", "ERROR_NO_VALID_INVOICES"));
                    }

                    var totalCount = result.totalCount;
                    var fetchedCount = result.data.length;
                    var fetchedFailedCount = 0;

                    result.data.forEach((item) => {
                        if (item.sendStatus && item.sendStatus.indexOf("ERROR") >= 0) {
                            fetchedFailedCount++;
                        }
                    });

                    if (log) common.log("InvoiceNotificationManager", "sendAllBills->loadBillsToSend: loaded total count=" +
                    totalCount + ", fetched count=" + fetchedCount + ", fetched previously failed count=" + fetchedFailedCount);

                    info.bills = result.data
                    info.metadata.bills = {
                        totalCount: totalCount,
                        fetchedCount: fetchedCount,
                        fetchedFailedCount: fetchedFailedCount
                    }

                    resolve(info);
                });
            });
        }).then((info) => {
            return new Promise((resolve, reject) => {
                updateCache("SENDING_BILLS", ++stepNumber, stepsCount, 0);

                var tasks = [];
                var tasksFinishedCount = 0;

                var bills = info.bills;
                bills.forEach((item) => {
                    (function (bill) {
                        tasks.push((callback) => {
                            sendBill(bill, options, (err, result) => {

                                tasksFinishedCount++;
                                if (tasksFinishedCount % 500 == 0) {
                                    var progress = Math.round((tasksFinishedCount / tasks.length) * 100);
                                    updateCache("SENDING_BILLS", stepNumber, stepsCount, progress);
                                }

                                setTimeout(() => {
                                    callback(undefined, {status: err ? "ERROR" : "OK"});
                                }, 1);
                            })
                        });
                    }(item))
                });

                async.parallelLimit(tasks, 50, (err, result) => {
                    if (err) {
                        return reject(err);
                    }

                    var sendingCount = 0;
                    var failedCount = 0;
                    result.forEach((item) => {
                        if (item.status == "OK") {
                            sendingCount++;
                        } else {
                            failedCount++;
                        }
                    });

                    if (log) common.log("InvoiceManager", "syncPaymentsFromFile->loadDependencies: sending count=" + sendingCount +
                    ", failed count =" + failedCount);
                    info.metadata.notifications = {
                        sendingCount: sendingCount,
                        failedCount: failedCount
                    }

                    resolve(info);
                });
            })
        }).then((info) => {
            updateCache("SENDING_SUCCESS", stepNumber, stepsCount, 100, true);

            if (log) common.log("InvoiceNotificationManager", "sendAllBills: finished, remove lock...");
            db.unlockOperation(operationKey);

            var result = info.metadata;
            var status = "OK";

            db.notifications_invoices_sync.update({_id: logId}, {$set: {status: status, result: result}}, {multi: true},
                (logErr, result) => {
                    if (logErr) common.error("InvoiceNotificationManager", "sendAllBills->updateLog: " +
                    "failed to update log, error=" + logErr.message);
                    // do nothing
                });

            setTimeout(() => {
                callback(undefined, result);
            }, 200);

        }).catch(function (err) {
            updateCache("SENDING_ERROR", stepNumber, stepsCount, 100, true);

            common.error("InvoiceNotificationManager", "sendAllBills: error=" + (err ? err.message : "Unknown error")
            + ", status=" + (err && err.status ? err.status : "NONE"));

            if (!err || err.status != "ERROR_OPERATION_LOCKED") {
                common.error("InvoiceNotificationManager", "sendAllBills: remove lock");
                db.unlockOperation(operationKey);
            }

            var status = err && err.status ? err.status : "ERROR";
            var errorMessage = err ? err.message : undefined;

            db.notifications_invoices_sync.update({_id: logId}, {$set: {status: status, error: errorMessage}}, {multi: true},
                (logErr, result) => {
                    if (logErr) common.error("InvoiceNotificationManager", "sendAllBills->updateLog: " +
                    "failed to update log, error=" + logErr.message);
                    // do nothing
                });

            setTimeout(() => {
                callback(err);
            }, 200);
        });
    },

    /**
     *
     *
     */
    sendSingleBill: function (invoiceId, options, callback) {
        if (!callback) callback = () => {
        }
        if (!options) options = {};

        invoiceManager.loadBill(invoiceId, (err, invoice) => {
            if (err) {
                common.error("InvoiceNotificationManager", "sendSingleBill: failed to load " +
                "invoice, err=" + err.message);
                return callback(err);
            }
            if (!invoice) {
                common.error("InvoiceNotificationManager", "sendSingleBill: invoice " + invoiceId + " is not found");
                return callback(new BaseError("Failed to load invoice " + invoiceId, "ERROR_INVOICE_NOT_FOUND"));
            }

            options.delay = 500;
            sendBill(invoice, options, (err, result) => {
                if (err) {
                    common.error("InvoiceNotificationManager", "sendSingleBill: failed to send " +
                    "invoice " + invoiceId + ", err=" + err.message);
                    return callback(err);
                }

                callback(undefined, result);
            })
        });
    },

    /**
     *
     *
     */
    sendNonPaymentReminder: function (reminderType, options, callback) {
        if (!callback) callback = () => {
        }
        if (!options) options = {};
        var logId;

        var updateCache = (stepName, stepNumber, stepsCount, stepProgress, end) => {
            db.cache.put("cache", operationSyncProgressKey, {
                stepName: stepName,
                stepNumber: stepNumber,
                stepsCount: stepsCount,
                stepProgress: stepProgress
            }, end ? 3000 : 300 * 1000);
        }

        var stepNumber = 0;
        var stepsCount = 3;

        if (log) common.log("InvoiceNotificationManager", "sendNonPaymentReminder: start sync...");
        Promise.resolve().then(() => {
            return new Promise((resolve, reject) => {
                var obj = {
                    status: "IN_PROGRESS",
                    error: "NONE",
                    type: "SEND_NON_PAYMENT_REMINDER",
                    initiator: options.initiator,
                    params: {
                        reminderType: reminderType,
                        options: options
                    },
                    result: {},
                    ts: new Date().getTime()
                }

                db.notifications_invoices_sync.insert(obj, function (err, result) {
                    if (err) {
                        common.error("InvoiceNotificationManager", "sendNonPaymentReminder->saveLog: failed to save sync " +
                        "log, error=" + err.message);
                        return reject(err);
                    }

                    logId = result && result.insertedIds ? result.insertedIds[0] : undefined;
                    resolve({metadata: {}});
                });
            });
        }).then((info) => {
            return new Promise((resolve, reject) => {
                updateCache("CHECK_LOCK", ++stepNumber, stepsCount, 0);

                if (!options || options.overrideLock) {
                    return resolve(info);
                }

                db.lockOperation(operationKey, {}, 5 * 60 * 1000, function (err) {
                    if (err) {
                        common.error("InvoiceNotificationManager", "sendNonPaymentReminder->lock: failed for " +
                        "start operation, err=" + err.message);
                        return reject(err);
                    }

                    resolve(info);
                });
            });
        }).then((info) => {
            return new Promise((resolve, reject) => {
                updateCache("LOADING_BILLS", ++stepNumber, stepsCount, 0);

                invoiceManager.loadBills(undefined, {
                    type: "OPEN"
                }, (err, result) => {
                    if (err) {
                        common.error("InvoiceNotificationManager", "sendNonPaymentReminder->loadBillsToSend: failed load " +
                        "bills, err=" + err.message);
                        return reject(err);
                    }
                    if (!result || !result.data || !result.data.length) {
                        common.error("InvoiceNotificationManager", "sendNonPaymentReminder->loadBillsToSend: failed load " +
                        "bills, err=" + err.message);
                        return reject(new BaseError("No Unpaid Bills Found", "ERROR_NO_UNPAID_INVOICES"));
                    }

                    var customers = [];
                    var customersMap = {};

                    result.data.forEach((item) => {
                        var customer = customersMap[item.billingAccountNumber];
                        if (!customer) {
                            customer = {
                                billingAccountNumber: item.billingAccountNumber,
                                syncedName: item.syncedName,
                                syncedEmail: item.syncedEmail,
                                syncedNumber: item.syncedNumber,
                                openInvoices: []
                            }

                            customers.push(customer);
                            customersMap[item.billingAccountNumber] = customer;
                        }

                        var currentDate = new Date();
                        if (item.monthDate.getMonth() != currentDate.getMonth()
                            || item.monthDate.getYear() != currentDate.getYear()) {
                            customer.strongMessage = true;
                        }

                        customer.openInvoices.push({
                            monthId: item.monthId,
                            monthDate: item.monthDate,
                            monthName: item.monthName,
                            invoiceId: item.invoiceId,
                            paymentStatus: item.paymentStatus,
                            paymentAt: item.paymentAt,
                            leftAmount: item.leftAmount
                        });
                    });

                    var fetchedCount = result.data.length;
                    var customerCount = customers.length;
                    var multipleOpenBillsCount = 0;
                    var strongMessagesCount = 0;

                    customers.forEach((item) => {
                        if (item.openInvoices.length > 1) {
                            multipleOpenBillsCount++;
                        }
                        if (item.strongMessage) {
                            strongMessagesCount++;

                            // in case if customer has old open, pending invoice
                            // we send different reminder, strong

                            item.reminderType = "NON_PAYMENT_REMINDER_STRONG";
                        } else {
                            item.reminderType = reminderType;
                        }
                    });

                    if (log) common.log("InvoiceNotificationManager", "sendNonPaymentReminder->loadBillsToSend: fetched count=" + fetchedCount
                    + ", customers count=" + customerCount + ", multiple open invoices count=" + multipleOpenBillsCount
                    + ", message will be string for count=" + strongMessagesCount);

                    info.bills = result.data
                    info.customers = customers
                    info.metadata.customers = {
                        fetchedCount: fetchedCount,
                        customerCount: customerCount,
                        multipleOpenBillsCount: multipleOpenBillsCount,
                        strongMessagesCount: strongMessagesCount
                    }

                    resolve(info);
                });
            });
        }).then((info) => {
            return new Promise((resolve, reject) => {
                updateCache("SENDING_REMINDERS", ++stepNumber, stepsCount, 0);

                var timeUnique = options.timeUnique;
                var startData = new Date().getTime() - (timeUnique ? timeUnique : 0);

                db.notifications_logbook.find({
                    activity: {"$in": nonPaymentReminderNotifications()},
                    ts: {"$gt": startData}
                }, {billingAccountNumber: 1}).toArray(function (err, result) {
                    if (err) {
                        return reject(err);
                    }

                    var billingAccountsToIgnore = [];
                    if (result) result.forEach((item) => {
                        billingAccountsToIgnore.push(item.billingAccountNumber);
                    });

                    var tasks = [];
                    var tasksFinishedCount = 0;
                    var batchSize = options.batchSize ? options.batchSize : -1;
                    var skippedCount = 0;
                    var ignoredCount = 0;

                    var customers = info.customers;
                    customers.forEach((item) => {
                        (function (reminder) {

                            if (billingAccountsToIgnore.indexOf(item.billingAccountNumber) >= 0) {
                                ignoredCount++;
                                return;
                            }
                            if (tasks.length >= batchSize && batchSize != -1) {
                                skippedCount++;
                                return;
                            }

                            tasks.push((callback) => {
                                sendNonPaymentReminderNotification(reminder, options, (err, result) => {

                                    tasksFinishedCount++;
                                    if (tasksFinishedCount % 500 == 0) {
                                        var progress = Math.round((tasksFinishedCount / tasks.length) * 100);
                                        updateCache("SENDING_REMINDERS", stepNumber, stepsCount, progress);
                                    }

                                    setTimeout(() => {
                                        callback(undefined, {status: err ? "ERROR" : "OK"});
                                    }, 1);
                                })
                            });
                        }(item))
                    });

                    async.parallelLimit(tasks, 50, (err, result) => {
                        if (err) {
                            return reject(err);
                        }

                        var sendingCount = 0;
                        var failedCount = 0;
                        result.forEach((item) => {
                            if (item.status == "OK") {
                                sendingCount++;
                            } else {
                                failedCount++;
                            }
                        });

                        if (log) common.log("InvoiceManager", "sendNonPaymentReminder->loadBillsToSend: sending count=" + sendingCount +
                        ", failed count=" + failedCount + ", skipped count=" + skippedCount + ", ignored count=" + ignoredCount);
                        info.metadata.notifications = {
                            sendingCount: sendingCount,
                            failedCount: failedCount,
                            skippedCount: skippedCount,
                            ignoredCount: ignoredCount
                        }

                        resolve(info);
                    });
                });
            });
        }).then((info) => {
            updateCache("SENDING_SUCCESS", stepNumber, stepsCount, 100, true);

            if (log) common.log("InvoiceNotificationManager", "sendNonPaymentReminder: finished, remove lock...");
            db.unlockOperation(operationKey);

            var result = info.metadata;
            var status = "OK";

            db.notifications_invoices_sync.update({_id: logId}, {$set: {status: status, result: result}}, {multi: true},
                (logErr, result) => {
                    if (logErr) common.error("InvoiceNotificationManager", "sendNonPaymentReminder->updateLog: " +
                    "failed to update log, error=" + logErr.message);
                    // do nothing
                });

            setTimeout(() => {
                callback(undefined, result);
            }, 200);

        }).catch(function (err) {
            updateCache("SENDING_ERROR", stepNumber, stepsCount, 100, true);

            common.error("InvoiceNotificationManager", "sendNonPaymentReminder: error=" + (err ? err.message : "Unknown error")
            + ", status=" + (err && err.status ? err.status : "NONE"));

            if (!err || err.status != "ERROR_OPERATION_LOCKED") {
                common.error("InvoiceNotificationManager", "sendNonPaymentReminder: remove lock");
                db.unlockOperation(operationKey);
            }

            var status = err && err.status ? err.status : "ERROR";
            var errorMessage = err ? err.message : undefined;

            db.notifications_invoices_sync.update({_id: logId}, {$set: {status: status, error: errorMessage}}, {multi: true},
                (logErr, result) => {
                    if (logErr) common.error("InvoiceNotificationManager", "sendNonPaymentReminder->updateLog: " +
                    "failed to update log, error=" + logErr.message);
                    // do nothing
                });

            setTimeout(() => {
                callback(err);
            }, 200);
        });
    }
}

function sendBill(invoice, options, callback) {
    if (!callback) callback = () => {
    }
    if (!options) options = {};

    if (!invoice || !invoice.syncedName || !invoice.syncedNumber || !invoice.syncedEmail
        || !invoice.billingAccountNumber || !invoice.invoiceId || !invoice.monthName) {
        return callback(new BaseError("Invalid params", "ERROR_INVALID_NOTIFICATION_PARAMS"));
    }

    var activity;
    var failureReason = getFailureReasonText(invoice.paymentStatus);
    var failureDate = getFailureReasonTime(invoice.paymentAt);

    if (invoice.type == 'PAID') {
        if (invoice.otherUnpaid) {
            activity = "bill_payment_partially";
        } else {
            if (invoice.monthNumber === 1) {
                activity = "bill_payment_success_first_bill";
            } else {
                activity = "bill_payment_success";
            }
        }
    } else if (invoice.type == 'UNPAID') {
        if (invoice.otherPaid) {
            activity = "bill_payment_partially";
        } else {
            if (failureReason && failureDate) {
                if (invoice.monthNumber === 1) {
                    activity = "bill_payment_failure_first_bill";
                } else {
                    activity = "bill_payment_failure";
                }
            } else {
                if (invoice.monthNumber === 1) {
                    activity = "bill_payment_failure_first_bill_no_reason";
                } else {
                    activity = "bill_payment_failure_no_reason";
                }
            }
        }
    } else if (invoice.type == 'NEGATIVE') {
        activity = "bill_payment_negative";
    } else {
        return callback(new BaseError("Invalid params", "ERROR_UNSUPPORTED_NOTIFICATION_PARAMS"));
    }

    var fileName = invoice.billingAccountNumber + "_" + invoice.invoiceId + ".pdf";
    var notification = {
        teamID: 5,
        teamName: "Operations",
        activity: activity,
        delay: options.delay ? options.delay : 0,
//        prefix: "65",
//        number: invoice.syncedNumber,
        name: invoice.syncedName,
//        email: invoice.syncedEmail,
        billingAccountNumber: invoice.billingAccountNumber,
        invoicenumber: invoice.invoiceId,
        month: invoice.monthName,
        pdfpassword: invoice.syncedPwd,
        totalAmount: invoice.totalAmount,
        leftAmount: invoice.leftAmount,
        paymentFailureReason: failureReason,
        paymentFailureDate: failureDate,
        attachment: {
            global: true,
            filename: fileName,
            path: invoice.filePath
        },
        metadata: {
            updateTriggers: {
                email: [{
                    id: invoice.id,
                    table: "billFiles",
                    statusField: "sendStatus",
                    timeField: "sentAt"
                }]
            }
        }
    };

    var log = {
        status: "OK",
        type: invoice.type,
        id: invoice.id,
        invoiceId: invoice.invoiceId,
        initiator: options.initiator,
        billingAccountNumber: invoice.billingAccountNumber,
        filename: fileName,
        path: invoice.filePath,
        number: invoice.syncedNumber,
        params: {
            invoice: invoice,
            options: options
        },
        notification: notification,
        ts: new Date().getTime()
    }

    Promise.resolve().then(() => {
        return new Promise((resolve, reject) => {
            db.notifications_bills.count({ "billingAccountNumber" : invoice.billingAccountNumber }, (ierr, icount) => {
                if (icount === 0) log.firstBill = true;
                else log.firstBill = false;
                resolve();
            })
        });
    }).then(() => {
        return new Promise((resolve, reject) => {
            var query = "UPDATE billFiles SET sendStatus=" + db.escape("SENDING") + "," +
                " sentAt=" + db.escape(new Date()) + ", notificationActivity=" + db.escape(activity) +
                " WHERE invoiceId=" + db.escape(invoice.invoiceId);

            db.query_err(query, function (err, result) {
                if (err) {
                    return reject(err);
                }

                resolve({
                    invoiceId: invoice.invoiceId,
                    notification: notification,
                    metadata: {}
                });
            });
        });
    }).then((info) => {
        return new Promise((resolve, reject) => {
            notificationSend.deliver(info.notification, null, (err, obj) => {
                if (err) {
                    return reject(err);
                }

                if (obj) {
                    log.notificationId = obj.notificationId;
                    log.customerAccountNumber = obj.customerAccountNumber;
                    log.serviceInstanceNumber = obj.serviceInstanceNumber;
                    log.billingAccountNumber = obj.billingAccountNumber;
                    log.notification = {
                        filename: obj.attachment.filename,
                        path: obj.attachment.path,
                        number: obj.number,
                        email: obj.email,
                        name: obj.name,
                        account: obj.account,
                        transport: obj.transport
                    }
                }

                info.metadata.notification = {
                    id: obj.notificationId
                }

                resolve(info);
            });
        });
    }).then((info) => {
        db.notifications_bills.insert(log, function (logErr) {
            if (logErr) {
                common.error("InvoiceNotificationManager", "sendBill->sendBill: " +
                "failed to save send logs, error=" + logErr.message);
                // do nothing
            }

            callback(undefined, info.metadata);
        });
    }).catch(function (err) {

        log.status = err.status ? err.status : "ERROR";
        log.error = err.message

        db.notifications_bills.insert(log, function (logErr) {
            if (logErr) {
                common.error("InvoiceNotificationManager", "sendBill->sendBill: " +
                "failed to save send logs, error=" + logErr.message);
                // do nothing
            }

            var query = "UPDATE billFiles SET sendStatus=" + db.escape(err && err.status
                    ? err.status : "ERROR") + " WHERE invoiceId=" + db.escape(invoice.invoiceId);

            db.query_err(query, function (logErr) {
                if (logErr) {
                    common.error("InvoiceNotificationManager", "sendBill->sendBill: " +
                    "failed to save send status, error=" + logErr.message);
                    // do nothing
                }

                callback(err);
            });
        });
    });
}

function nonPaymentReminderNotifications() {
    return ["bill_payment_failure_reminder_1", "bill_payment_failure_reminder_1_no_reason",
        "bill_payment_failure_reminder_2", "bill_payment_failure_reminder_2_no_reason",
        "bill_payment_failure_reminder_strong", "bill_payment_failure_reminder_strong_no_reason"];
}

function sendNonPaymentReminderNotification(reminder, options, callback) {
    if (!callback) callback = () => {
    }
    if (!options) options = {};

    if (!reminder || !reminder.openInvoices || !reminder.openInvoices[0]) {
        return callback(new BaseError("Invalid params", "ERROR_UNSUPPORTED_NOTIFICATION_PARAMS"));
    }

    var activity;
    var reminderType = reminder.reminderType;
    var lastInvoice = reminder.openInvoices[0];
    var paymentFailureDate;
    var paymentFailureReason;
    var outstandingTotal = 0;
    var customTables = [];

    if (lastInvoice.paymentStatus != 'UNKNOWN' && lastInvoice.paymentStatus != 'PF_OTHER') {
        paymentFailureReason = getFailureReasonText(lastInvoice.paymentStatus);
        paymentFailureDate = getFailureReasonTime(lastInvoice.paymentAt);
    }

    var month;
    var invoicenumber;
    var outstanding;
    if (reminderType == "NON_PAYMENT_REMINDER_1") {
        month = lastInvoice.monthName;
        invoicenumber = lastInvoice.monthName;
        outstanding = lastInvoice.leftAmount;
        activity = paymentFailureReason ? "bill_payment_failure_reminder_1" : "bill_payment_failure_reminder_1_no_reason";
    } else if (reminderType == "NON_PAYMENT_REMINDER_2") {
        month = lastInvoice.monthName;
        invoicenumber = lastInvoice.monthName;
        outstanding = lastInvoice.leftAmount;
        activity = paymentFailureReason ? "bill_payment_failure_reminder_2" : "bill_payment_failure_reminder_2_no_reason";
    } else if (reminderType == "NON_PAYMENT_REMINDER_STRONG") {
        var rows = [];
        reminder.openInvoices.forEach((item) => {
            outstandingTotal += item.leftAmount;
            rows.push({month: item.monthName, invoicenumber: item.invoiceId, outstanding: (item.leftAmount / 100)})
        });
        customTables = [{
            rowtag: "open_invoices",
            rows: rows
        }]
        activity = paymentFailureReason ? "bill_payment_failure_reminder_strong" : "bill_payment_failure_reminder_strong_no_reason";
    } else {
        return callback(new BaseError("Invalid reminder type", "ERROR_UNSUPPORTED_REMINDER_TYPE"));
    }

    var notification = {
        teamID: 5,
        teamName: "Operations",
        activity: activity,
        delay: options.delay > 0 ? options.delay : 0,
//        prefix: "65",
//        number: reminder.syncedNumber,
        name: reminder.syncedName,
//        email: reminder.syncedEmail,
        billingAccountNumber: reminder.billingAccountNumber,
        outstandingTotal: outstandingTotal > 0 ? (outstandingTotal / 100) : undefined,
        paymentFailureReason: paymentFailureReason,
        paymentFailureDate: paymentFailureDate
    };

    if (month) {
        notification.month = month;
    }
    if (invoicenumber) {
        notification.invoicenumber = invoicenumber;
    }
    if (outstanding) {
        notification.outstanding = outstanding;
    }
    if (customTables && customTables.length > 0) {
        notification.customTables = customTables;
    }

    var log = {
        status: "OK",
        type: "REMINDER",
        reminderType: reminderType,
        invoiceId: lastInvoice.invoiceId,
        initiator: options.initiator,
        billingAccountNumber: reminder.billingAccountNumber,
        params: {
            reminder: reminder,
            options: options
        },
        notification: notification,
        ts: new Date().getTime()
    }

    var info = {
        notification: notification,
        metadata: {}
    }

    Promise.resolve().then(() => {
        return new Promise((resolve, reject) => {
            notificationSend.deliver(info.notification, null, (err, obj) => {
                if (err) {
                    return reject(err);
                }

                if (obj) {
                    log.notificationId = obj.notificationId;
                    log.customerAccountNumber = obj.customerAccountNumber;
                    log.serviceInstanceNumber = obj.serviceInstanceNumber;
                    log.billingAccountNumber = obj.billingAccountNumber;
                    log.notification = {
                        number: obj.number,
                        email: obj.email,
                        name: obj.name,
                        account: obj.account,
                        transport: obj.transport
                    }
                }

                info.metadata.notification = {
                    id: obj.notificationId
                }

                resolve(info);
            });
        });
    }).then((info) => {
        db.notifications_bills.insert(log, function (logErr) {
            if (logErr) {
                common.error("InvoiceNotificationManager", "sendNonPaymentReminderNotification->sendBill: " +
                "failed to save send logs, error=" + logErr.message);
                // do nothing
            }
            callback(undefined, info.metadata);
        });
    }).catch(function (err) {

        log.status = err.status ? err.status : "ERROR";
        log.error = err.message;

        db.notifications_bills.insert(log, function (logErr) {
            if (logErr) {
                common.error("InvoiceNotificationManager", "sendNonPaymentReminderNotification->sendBill: " +
                "failed to save send logs, error=" + logErr.message);
                // do nothing
            }
            callback(err);
        });
    });
}

function getFailureReasonText(errorStatus) {
    var stringId;
    if (errorStatus == "PF_NOT_HONORED") {
        stringId = "error_code_payment_pf_not_honored_email";
    } else if (errorStatus == "PF_LOST") {
        stringId = "error_code_payment_pf_lost_email";
    } else if (errorStatus == "PF_STOLEN") {
        stringId = "error_code_payment_pf_stolen_email";
    } else if (errorStatus == "PF_INSUFFICIENT_FUND") {
        stringId = "error_code_payment_pf_insufficient_fund_email";
    } else if (errorStatus == "PF_INVALID_CARD_INFORMATION") {
        stringId = "error_code_payment_pf_invalid_card_information_email";
    } else if (errorStatus == "PF_CARD_EXPIRED") {
        stringId = "error_code_payment_pf_card_expired_email";
    } else {
        return undefined;
    }
    return resourceManager.getString(stringId);
}

function getFailureReasonTime(errorDate) {
    return errorDate && new Date(errorDate).getTime() ? dateformat(new Date(errorDate), "dS mmmm") : errorDate;
}


