var request = require('request');
var async = require('async');

var config = require('../../../../config');
var common = require('../../../../lib/common');
var db = require('../../../../lib/db_handler');
var ec = require('../../../../lib/elitecore');
var elitecoreUserService = require('../../../../lib/manager/ec/elitecoreUserService');
var elitecoreBillService = require('../../../../lib/manager/ec/elitecoreBillService');
var configManager = require('../config/configManager');
const validationQ = require('./validationQ');
var billManager = require('./billManager');
var transactionManager = require('./transactionManager');
var notificationSend = require('../notifications/send');
var profileManager = require('../profile/profileManager');
var invoiceManager = require('../account/invoiceManager');
var invoiceNotificationManager = require('../account/invoiceNotificationManager');

var BaseError = require('../../errors/baseError');

var log = config.LOGSENABLED;
var billPaymentSyncKey = "billPaymentSyncKey";
var creditCapPaymentSyncKey = "creditCapPaymentSyncKey";

module.exports = {

    /**
     *
     */
    queueBillPaymentByBA: function (billingAccountNumber, invoicesInfo, options, callback) {
        if (!callback) callback = () => {
        }

        if (!billingAccountNumber || !invoicesInfo
            || !invoicesInfo[0]
            || !invoicesInfo[0].invoiceId
            || !invoicesInfo[0].amount) {
            return callback(new BaseError("Invalid params", "ERROR_CUSTOMER_NOT_FOUND"));
        }

        var job = db.queue.create('invoicePayment', {
            billingAccountNumber: billingAccountNumber,
            invoicesInfo: invoicesInfo,
            options: options
        }).events(false).removeOnComplete(true).delay(0).attempts(1).backoff({
            delay: 10 * 1000,
            type: "fixed"
        }).save(function (err) {
            if (err) {
                common.error("InvoiceManager", "scheduleBillPayment: failed to add job to the queue");
                return callback(err);
            }

            callback(undefined, job ? {
                id: job.id,
                type: job.type
            } : {id: -1});
        });
    },

    /**
     *
     */
    scheduleBillPaymentByBA: function (billingAccountNumber, invoicesInfo, options, ignoreIfAlreadyScheduled, callback) {
        if (!callback) callback = () => {
        }
        if (!options) options = () => {
        }

        if (!billingAccountNumber || !invoicesInfo
            || !invoicesInfo[0]
            || !invoicesInfo[0].invoiceId
            || !invoicesInfo[0].amount) {
            return callback(new BaseError("Invalid params", "ERROR_CUSTOMER_NOT_FOUND"));
        }

        var query = {
            start: {"$gt": new Date().getTime() - 60 * 60 * 1000},
            billingAccountNumber: billingAccountNumber,
            action: "invoice_payment",
            processed: {"$exists": 0}
        }

        elitecoreUserService.loadUserInfoByBillingAccountNumber(billingAccountNumber, function (err, cache) {
            if (err) {
                return callback(err);
            }
            if (!cache) {
                return callback(new BaseError("Customer BAN " + billingAccountNumber + " not found",
                    "ERROR_CUSTOMER_NOT_FOUND"));
            }

            db.scheduler.find(query).toArray(function (err, result) {
                if (err) {
                    common.log("SchedulerManager", "failed to load events, err=" + err.message);
                    return;
                }

                if (result && result[0]) {
                    var exisitingEventId = result[0]._id + "";
                    var startDate = new Date(result[0].start);

                    if (ignoreIfAlreadyScheduled) {
                        return callback(undefined, {
                            added: false,
                            alreadySchdeuled: true,
                            eventId: exisitingEventId,
                            startDate: startDate.toISOString()
                        });
                    }
                }

                var eventStartDate = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
                db.scheduler.insert({
                    start: eventStartDate.getTime(),
                    prefix: cache.prefix,
                    number: cache.number,
                    billingAccountNumber: cache.billingAccountNumber,
                    serviceInstanceNumber: cache.serviceInstanceNumber,
                    invoicesInfo: invoicesInfo,
                    options: options,
                    action: "invoice_payment",
                    allDay: false,
                    trigger: options.initiator ? options.initiator : "[CMS][INTERNAL]",
                    title: "Invoice Payment BAN " + common.escapeHtml(cache.billingAccountNumber)
                }, {writeConcern: {w: 'majority'}}, function (err, result) {
                    if (err) {
                        return callback(new BaseError("Scheduler Error", "ERROR_ADD_TO_SCHEDULER"));
                    }
                    if (!result || !result.insertedIds || !result.insertedIds[0]) {
                        return callback(new BaseError("Scheduler Error", "ERROR_FAILED_TO_ADD_TO_SCHEDULER"));
                    }

                    callback(undefined, {added: true, eventId: result.insertedIds[0]});
                });
            });
        });
    },

    /**
     *
     */
    makeBillPaymentByBA: function (billingAccountNumber, invoicesInfo, options, callback) {
        if (!callback) callback = () => {
        }

        if (!billingAccountNumber || !invoicesInfo
            || !invoicesInfo[0]
            || !invoicesInfo[0].invoiceId
            || !invoicesInfo[0].amount) {
            return callback(new BaseError("Invalid params", "ERROR_INVALID_PARAMS"));
        }

        var executionKey = config.OPERATIONS_KEY;
        var lockKey = billPaymentSyncKey + "_" + billingAccountNumber;

        var logId = -1;
        var startTime = new Date().getTime();
        var logsInfo = {metadata: {time: 0, paymentResults: []}};

        Promise.resolve(logsInfo).then((info) => {
            return new Promise((resolve, reject) => {
                elitecoreUserService.loadUserInfoByBillingAccountNumber(billingAccountNumber, function (err, cache) {
                    if (err) {
                        return reject(err);
                    }
                    if (!cache) {
                        return reject(new BaseError("Customer BAN " + billingAccountNumber + " not found",
                            "ERROR_CUSTOMER_NOT_FOUND"));
                    }

                    // clear cache before starting payment
                    info.cache = cache;
                    resolve(info);
                })
            });
        }).then((info) => {
            return getPromiseLogCreation("INVOICE", info.cache, {invoicesInfo: invoicesInfo}, "PAYMENT", info, options, (id) => {
                logId = id;
            });
        }).then((info) => {
            return new Promise((resolve, reject) => {
                if (!options || options.overrideLock) {
                    return resolve(info);
                }

                db.lockOperation(lockKey, {}, 5 * 60 * 1000, function (err) {
                    if (err) {
                        common.error("InvoiceManager", "makeBillPayment->lock: failed to lock, err=" + err.message);
                        return reject(new BaseError("Payment for BAN " + billingAccountNumber + " is in progress",
                            err.status));
                    }

                    resolve(info);
                });
            });
        }).then((info) => {
            return new Promise((resolve, reject) => {
                invoiceManager.loadCurrentMonth((err, currentMonth) => {
                    if (err) {
                        return reject(err);
                    }
                    if (!currentMonth) {
                        return reject(new BaseError("Current month invoices has not been synchronized, " +
                        "payments are disabled to avoid inconsistency", "ERROR_INVOICES_NOT_SYNCED"));
                    }

                    info.currentMonth = currentMonth;
                    info.metadata.currentMonthId = currentMonth.id;
                    resolve(info);
                });
            });
        }).then((info) => {
            return new Promise((resolve, reject) => {
                var invoiceIds = [];
                invoicesInfo.forEach((invoiceInfo) => {
                    invoiceIds.push(invoiceInfo.invoiceId);
                });

                invoiceManager.loadBills(undefined, {invoiceIds: invoiceIds}, (err, result) => {
                    if (err) {
                        return reject(err);
                    }

                    if (!result || invoiceIds.length != result.data.length) {
                        return reject(new BaseError("Some invoices " + JSON.stringify(invoiceIds) + " not found",
                            "ERROR_INVOICE_NOT_FOUND"));
                    }

                    var someInvalid = result.data.some((invoice) => {
                        if (invoice.syncStatus != "OK") {
                            return true;
                        }
                    });

                    if (someInvalid && !options.ignoreInvalidSync) {
                        return reject(new BaseError("Some invoices " + JSON.stringify(invoiceIds) +
                        " have invalid sync status", "ERROR_INVOICE_SYNC_FAILED"));
                    }

                    var localInvoicesMap = {}
                    result.data.forEach((invoice) => {
                        localInvoicesMap[invoice.invoiceId] = invoice;
                    });

                    info.localInvoiceIds = invoiceIds;
                    info.localInvoices = result.data;
                    info.localInvoicesMap = localInvoicesMap;
                    info.metadata.localInvoiceIds = invoiceIds;
                    resolve(info);
                });
            });
        }).then((info) => {
            return new Promise((resolve, reject) => {
                billManager.checkLastBillViewBlocked(info.cache.billingAccountNumber, function (err, result) {
                    if (err) {
                        return reject(err);
                    }
                    if (!result || result.blocked) {
                        return reject(new BaseError("Payments are blocked for BAN " + info.cache.billingAccountNumber,
                            "ERROR_BAN_PAYMENTS_BLOCKED"));
                    }
                    resolve(info);
                });
            });
        }).then((info) => {
            return new Promise((resolve, reject) => {
                if (options.batch || options.suspensionPaymentAttempt) {
                    return resolve(info);
                }

                invoiceManager.loadBills(undefined, {
                    billingAccountNumbers: [info.cache.billingAccountNumber],
                    status: "SEND_NOT_SENT"
                }, (err, result) => {
                    if (err) {
                        return reject(err);
                    }

                    if (result && result.data && result.data.length > 0) {
                        return reject(new BaseError("Payment for platform " + options.platform +
                        " is not allowed until all pdf bills are delivered", "ERROR_PAYMENTS_BLOCKED_NOT_ALL_DELIVERED"));
                    }
                    return resolve(info);
                });
            });
        }).then((info) => {
            return new Promise((resolve, reject) => {
                // check bill run cut off

                var now = common.getLocalTime();
                var firstDay = new Date(now.getFullYear(), now.getMonth(), 1, 48, 0, 0);
                var lastDay = new Date(now.getFullYear(), now.getMonth() + 1, 0, 22, 0, 0);
                if (now.getTime() > lastDay.getTime() || now.getTime() < firstDay.getDate()) {
                    return reject(new BaseError("Payment is disabled for all invoices due to a bill run",
                        "ERROR_ALL_INVOICES_PAYMENTS_DISABLED"));
                }

                // check platform, CMS payments generated buy CS and payment process should not be blocked
                // only customer generated payments should not go through

                if (options.batch || options.suspensionPaymentAttempt) {
                    return resolve(info);
                }

                configManager.loadConfigMapForKey("bill", "pay_now", function (err, item) {
                    if (err) {
                        return reject(err);
                    }
                    if (!item || !item.options || !item.options.enabled) {
                        return reject(new BaseError("Payments have been disabled for all invoices",
                            "ERROR_ALL_PAYMENTS_DISABLED"));
                    }
                    return resolve(info);
                });
            });
        }).then((info) => {
            return new Promise((resolve, reject) => {

                // do not allow payment if any invoice is blocked, batch stataus is not 'OK'

                var unfinishedTransactionInvoice;
                if (info.localInvoices) info.localInvoices.some((invoice) => {
                    if (invoice.batchStatus && invoice.batchStatus != "OK") {
                        unfinishedTransactionInvoice = invoice;
                        return true;
                    }
                });

                if (!options.retryUnfinished && unfinishedTransactionInvoice) {
                    return reject(new BaseError("Transaction has not been properly finished for invoice " +
                    unfinishedTransactionInvoice.invoiceId, "ERROR_UNFINISHED_TRANSACTION"));
                }

                resolve(info);
            });
        }).then((info) => {
            return new Promise((resolve, reject) => {
                // clear cache before starting payment
                elitecoreBillService.clearCustomerPaymentsCache(billingAccountNumber);

                billManager.loadOpenBillsHistoryByAccount(info.cache.customerAccountNumber, (err, openInvoices) => {
                    if (err) {
                        return callback(err);
                    }

                    if (!openInvoices || !openInvoices[0]) {
                        return reject(new BaseError("No open invoices found", "ERROR_OPEN_INVOICE_NOT_FOUND"));
                    }

                    info.openInvoices = openInvoices;
                    resolve(info);
                });
            });
        }).then((info) => {
            return new Promise((resolve, reject) => {
                var customerAccountNumber = info.cache.customerAccountNumber;
                var billingAccountNumber = info.cache.billingAccountNumber;

                async.mapSeries(invoicesInfo, (invoiceInfo, callback) => {

                    var invoiceId = invoiceInfo.invoiceId;
                    var amount = invoiceInfo.amount;
                    var paymentInfo = {invoiceId: invoiceId, amount: amount};
                    logsInfo.metadata.paymentResults.push(paymentInfo);

                    var foundInvoice;
                    if (info.openInvoices) info.openInvoices.some((bill) => {
                        if (bill.id == invoiceId) {
                            foundInvoice = bill;
                            return true;
                        }
                    });

                    // do not allow payments is BSS invoice is not found

                    if (!foundInvoice) {
                        paymentInfo.batchStatus = "OK";
                        paymentInfo.paymentStatus = "ERROR_OPEN_INVOICE_NOT_FOUND";
                        paymentInfo.paymentError = "Open invoice " + invoiceId + " is not found for CAN " + customerAccountNumber;
                        return callback(undefined, paymentInfo);
                    }

                    // do not allow payments is amount is greater then outstanding

                    paymentInfo.bssInvoice = foundInvoice;
                    var outstandingValue = foundInvoice.payNow && foundInvoice.payNow.value
                        ? foundInvoice.payNow.value : -1;

                    // round all cents vars to 0 decimals,
                    // round all $ vars to 2 decimals

                    outstandingValue = outstandingValue > 0 ? parseFloat(parseFloat(outstandingValue).toFixed(2)) : outstandingValue;
                    amount = amount > 0 ? parseFloat(parseFloat(amount).toFixed(2)) : 0;
                    var outstandingAfterPayment = parseFloat(parseFloat(outstandingValue - amount).toFixed(2));
                    var outstandingBeforePaymentCents = parseFloat(parseFloat(outstandingValue * 100).toFixed(0));
                    var outstandingAfterPaymentCents = parseFloat(parseFloat(outstandingAfterPayment * 100).toFixed(0));

                    if (outstandingValue < amount) {
                        paymentInfo.batchStatus = "OK";
                        paymentInfo.paymentStatus = "ERROR_INVALID_PAYMENT_AMOUNT";
                        paymentInfo.paymentError = "Invalid amount " + amount + ", invoice " + invoiceId + ", max " + outstandingValue;
                        return callback(undefined, paymentInfo);
                    }

                    transactionManager.performPayment(billingAccountNumber, {
                        type: transactionManager.TYPE_INVOICE_PAYMENT,
                        amount: amount,
                        internalId: invoiceId,
                        metadata: {
                            logId: logId,
                            outsBFCents: outstandingBeforePaymentCents,
                            outsAPCents: outstandingAfterPaymentCents,
                            actions: [transactionManager.createCloseInvoiceAction(invoiceId)]
                        }
                    }, (status, transaction, callback) => {
                        if (status == transactionManager.TR_STATUS_CREATED) {
                            // save payments results log
                            paymentInfo.transactionId = transaction.id;
                            // save transaction id for an invoice
                            invoiceManager.updateBatchStatus(invoiceId, transaction.id, "TRANSACTION_STARTED", (err) => {
                                return callback(err);
                            });
                        } else if (status == transactionManager.TR_STATUS_PAYMENT_FINISHED) {
                            // save payments results log
                            paymentInfo.eCommOrderRef = transaction.eCommOrderRef;
                            paymentInfo.paymentResult = transaction.paymentResult;
                            paymentInfo.paymentStatus = transaction.paymentStatus;
                            paymentInfo.outstandingAfterPayment = outstandingAfterPayment;
                            callback();
                        } else if (status == transactionManager.TR_STATUS_ACTIONS_FINISHED) {
                            // save subscription results log
                            paymentInfo.actionsResult = transaction.actionsResult;
                            callback();
                        } else if (status == transactionManager.TR_STATUS_FAILED || status == transactionManager.TR_STATUS_FINISHED) {
                            // save payments results log
                            paymentInfo.batchStatus = transaction.transactionStatus;
                            // update invoice batch status after a payment
                            var params = {
                                batchStatus: transaction.transactionStatus,
                                paymentStatus: transaction.transactionStatus == 'OK' ? transaction.paymentStatus : undefined,
                                leftAmount: (transaction.transactionStatus == 'OK' && transaction.paymentStatus == 'OK')
                                    ? outstandingAfterPaymentCents : undefined
                            };

                            invoiceManager.updateBillPaymentStatus(invoiceId, params, {retryIfBlocked: true}, (err) => {
                                return callback(err);
                            });
                        } else {
                            return callback();
                        }
                    }, (err, result) => {
                        if (err) {
                            return callback(err);
                        }

                        callback(undefined, paymentInfo);
                    });
                }, (err, result) => {
                    if (err) {
                        return reject(err);
                    }

                    info.paymentResults = result;
                    info.metadata.paymentResults = result;

                    // update last invoice dependencies

                    invoiceManager.updateBillsDependencies(billingAccountNumber, {retryIfBlocked: true}, (err, result) => {
                        if (err) {
                            return reject(err);
                        }

                        info.metadata.dependencies = result;
                        resolve(info);
                    });
                });
            });
        }).then((info) => {
            return new Promise((resolve, reject) => {
                if (options.silent) {
                    return resolve(info);
                }

                var localCurrentMonthInvoice;
                if (info.localInvoices) info.localInvoices.some((invoice) => {
                    if (invoice.monthId == info.currentMonth.id) {
                        localCurrentMonthInvoice = invoice;
                        return true;
                    }
                });

                if (localCurrentMonthInvoice
                    && localCurrentMonthInvoice.sendStatus == "NOT_SENT"
                    && !options.suspensionPaymentAttempt) {

                    // last invoice has 'NOT_SENT' status means the latest pdf bill has not been delivered
                    // need to sent notification with that bill, that notification will reflect grouped
                    // information regarding all unpaid / paid / partially paid bills

                    info.metadata.notifications = {
                        hasUnSentPDF: true,
                        invoiceId: localCurrentMonthInvoice.invoiceId
                    };

                    invoiceNotificationManager.sendSingleBill(localCurrentMonthInvoice.invoiceId, {}, (err, result) => {
                        if (err) common.error("PaymentManager", "makeBillPaymentByBA->sendPdfNotification: " +
                        "failed to send notification, error=" + err.message);

                        info.metadata.notifications.error = err ? err.message : undefined;
                        info.metadata.notifications.result = result;
                        resolve(info);
                    });
                } else {

                    // if last PDF files has been already delivered we have to send payment success confirmation
                    // for each successfully paid bill

                    info.metadata.notifications = {
                        hasUnSentPDF: false,
                        successInvoiceIds: []
                    };

                    if (info.paymentResults && info.localInvoicesMap) info.paymentResults.forEach((paymentResult) => {
                        var localInvoice = info.localInvoicesMap[paymentResult.invoiceId];
                        if (localInvoice && paymentResult.paymentStatus == "OK") {

                            var notification = {
                                teamID: 5,
                                teamName: "Operations",
                                activity: "bill_payment_success_after_failure",
                                delay: -1,
                                prefix: "65",
                                amount: paymentResult.amount,
                                number: localInvoice.syncedNumber,
                                name: localInvoice.syncedName,
                                email: localInvoice.syncedEmail,
                                billingAccountNumber: localInvoice.billingAccountNumber,
                                invoicenumber: localInvoice.invoiceId,
                                month: localInvoice.monthName,
                                pdfpassword: localInvoice.syncedPwd,
                                totalAmount: localInvoice.totalAmount,
                                leftAmount: localInvoice.leftAmount
                            };

                            info.notification = notification;
                            info.metadata.notifications.successInvoiceIds.push(localInvoice.invoiceId);

                            notificationSend.deliver(info.notification, null, (err) => {
                                if (err) common.error("PaymentManager", "makeBillPaymentByBA->sendSuccessPaymentNotification: " +
                                "failed to send notification, error=" + err.message);

                                // do not wait until the callback is called
                            });
                        }
                    });

                    resolve(info);
                }
            });
        }).then((info) => {
            return new Promise((resolve, reject) => {
                validationQ.queueValidateStatusByBA(info.cache.billingAccountNumber, {
                    immediateSuspension: options.immediateSuspension,
                    initiator: options.initiator,
                    ignoreSuspension: options.ignoreSuspension,
                    paymentLogId: logId
                }, (err, result) => {
                    info.metadata.statusValidation = {
                        error: err ? err.message : undefined,
                        job: result
                    };

                    resolve(info);
                });
            });
        }).then((info) => {
            info.metadata.time = new Date().getTime() - startTime;
            processOperationResult("INVOICE", logId, undefined, info, options, lockKey, callback)
        }).catch((err) => {
            logsInfo.metadata.time = new Date().getTime() - startTime;
            processOperationResult("INVOICE", logId, err, logsInfo, options, lockKey, callback)
        });
    },

    /**
     *
     */
    queueCreditCapPaymentByBA: function (billingAccountNumber, options, callback) {
        if (!options) options = {};
        if (!callback) callback = () => {
        }

        if (!billingAccountNumber) {
            return callback(new BaseError("Invalid params", "ERROR_INVALID_PARAMS"));
        }

        // delay is required to check DR
        options.delayed = true;
        var delay = options.overrideDelay > 0 ? options.overrideDelay : 30 * 60 * 1000;
        var creditCapAmount = options.overridePaymentCents > 0 ? (options.overridePaymentCents / 100) : 200;

        elitecoreUserService.loadUserInfoByBillingAccountNumber(billingAccountNumber, function (err, cache) {
            if (err) {
                return callback(err);
            }
            if (!cache) {
                return callback(new BaseError("Customer BAN " + billingAccountNumber + " not found",
                    "ERROR_CUSTOMER_NOT_FOUND"));
            }

            getPromiseLogCreation("CREDIT_CAP", cache, {
                capPayment: {creditCapAmount: creditCapAmount}
            }, "PAYMENT", {}, options, (id) => {
                options.logId = id;
            }).then(function (result) {

                delete options.delayed;
                var job = db.queue.create('creditCapPayment', {
                    billingAccountNumber: billingAccountNumber,
                    options: options
                }).events(false).removeOnComplete(true).delay(delay).attempts(1).backoff({
                    delay: 10 * 1000,
                    type: "fixed"
                }).save(function (err) {
                    if (err) {
                        common.error("InvoiceManager", "queueCreditCapPaymentByBA: failed to add job to the queue");
                        return callback(err);
                    }

                    callback(undefined, job ? {
                        id: job.id,
                        type: job.type,
                        delay: delay
                    } : {id: -1});
                });

            }).catch(function (err) {
                common.error("InvoiceManager", "queueCreditCapPaymentByBA: " +
                "failed to add job to the queue, error=" + err.message);
                callback(err)
            });
        });
    }
    ,

    /**
     *
     */
    makeCreditCapPaymentByBA: function (billingAccountNumber, options, callback) {
        if (!options) options = {};
        if (!callback) callback = () => {
        }

        if (!billingAccountNumber) {
            return callback(new BaseError("Invalid params", "ERROR_INVALID_PARAMS"));
        }

        var lockKey = creditCapPaymentSyncKey + "_" + billingAccountNumber;
        var creditCapDefaultAmount = 200;
        var creditCapDefaultAmountCents = creditCapDefaultAmount * 100;
        var creditCapOverriden = options.overridePaymentCents > 0 && (options.overridePaymentCents / 100) != creditCapDefaultAmount;
        var creditCapAmount = creditCapOverriden ? (options.overridePaymentCents / 100) : creditCapDefaultAmount;

        var logId = -1;
        var startTime = new Date().getTime();
        var logsInfo = {metadata: {time: 0, creditCapAmount, creditCapOverriden}};

        Promise.resolve(logsInfo).then((info) => {
            return new Promise((resolve, reject) => {
                elitecoreUserService.loadUserInfoByBillingAccountNumber(billingAccountNumber, function (err, cache) {
                    if (err) {
                        return reject(err);
                    }
                    if (!cache) {
                        return reject(new BaseError("Customer BAN " + billingAccountNumber + " not found",
                            "ERROR_CUSTOMER_NOT_FOUND"));
                    }

                    info.cache = cache;
                    resolve(info);
                })
            });
        }).then((info) => {
            return getPromiseLogCreation("CREDIT_CAP", info.cache, {
                platform: options.platform,
                capPayment: {
                    creditCapAmount: creditCapAmount
                }
            }, "PAYMENT", info, options, (id) => {
                logId = id;
            });
        }).then((info) => {
            return new Promise((resolve, reject) => {
                if (!options || options.overrideLock) {
                    return resolve(info);
                }

                db.lockOperation(lockKey, {}, 5 * 60 * 1000, function (err) {
                    if (err) {
                        common.error("PaymentManager", "makeCreditCapPaymentByBA->lock: failed to lock, err=" + err.message);
                        return reject(new BaseError("Credit Cap Payment for BAN " + billingAccountNumber + " is in progress",
                            err.status));
                    }

                    resolve(info);
                });
            });
        }).then((info) => {
            return new Promise((resolve, reject) => {
                var addonName = "Cap credit 200";
                var creditCapAddon = ec.findPackageName(ec.packages(), "Cap credit 200");
                if (!creditCapAddon) {
                    return reject(new BaseError("Credit cap addon '" + addonName + "' not found "));
                }

                info.creditCapAddon = creditCapAddon;
                info.creditCapAmount = creditCapAmount;
                resolve(info);
            });
        }).then((info) => {
            return new Promise((resolve, reject) => {
                var maxLimit = 3;
                var date = new Date(new Date().getTime() - 24 * 60 * 60 * 1000);

                db.notifications_credit_cap.count({
                    status: "OK",
                    billingAccountNumber: billingAccountNumber,
                    ts: {$gt: date.getTime()}
                }, (err, count) => {
                    if (err) {
                        return reject(err);
                    }

                    if (count >= maxLimit && !options.overrideLimit && !options.warningOnly) {
                        return reject(new BaseError("Credit Cap Payments for BAN " + billingAccountNumber +
                        " exceeded limit of " + maxLimit + " last 24h", "ERROR_CREDIT_CAP_PAYMENT_NOT_ALLOWED"));
                    }

                    info.metadata.prevPayments = {maxLimit: maxLimit, count: count};
                    return resolve(info);
                });
            });
        }).then((info) => {
            return new Promise((resolve, reject) => {
                elitecoreBillService.loadCustomerUsage(info.cache.serviceInstanceNumber, (err, result) => {
                    if (err) {
                        return reject(err);
                    }
                    var amountSpent = result && result.amountCents ? (result.amountCents / 100) : 0;

                    // there is no proper way to distinguish Credit Cap Payments and usual Andvanced Payment payments
                    // (Boost, Registration Fee, Suspension Fee, Plus) so in order to find Credit Cap payments we can
                    // only rely on paid amount - 200$ and 400$, that is why we filter all advanced payment below

                    billManager.loadAdvancedPayments(billingAccountNumber, {
                        filterAmountCents: [creditCapDefaultAmountCents, creditCapDefaultAmountCents * 2]
                    }, (err, result) => {
                        if (err) {
                            return reject(err);
                        }

                        var amountPaid = result && result.paymentsCents ? (result.paymentsCents / 100) : 0;
                        info.metadata.spendings = {amountSpent: amountSpent, amountPaid: amountPaid};

                        if (amountSpent - amountPaid < creditCapDefaultAmount && !options.overrideUsage && !options.warningOnly) {
                            return reject(new BaseError("Credit Cap Payments for BAN " + billingAccountNumber +
                                " is not required (used $" + amountSpent + ", paid $" + amountPaid + ")",
                                "ERROR_CREDIT_CAP_PAYMENT_NOT_REQUIRED"));
                        }

                        if (amountSpent - amountPaid > creditCapDefaultAmount * 2 && !creditCapOverriden) {
                            // in case if outstanding is greater then 2x of default
                            // amount (200$) we need to charge cutomer 2 times (400$)
                            info.creditCapAmount = (creditCapDefaultAmount * 2);
                            info.doublePayment = true;

                            //save info to metadata in order to reflect update amount in logs
                            info.metadata.doublePayment = info.doublePayment;
                            info.metadata.doublePaymentAmount = info.creditCapAmount;
                        }

                        return resolve(info);
                    });
                });
            });
        }).then((info) => {
            return new Promise((resolve, reject) => {
                if (options.warningOnly) {
                    return resolve(info);
                }

                var uuid = info.cache.customerAccountNumber + "_" + new Date().toISOString();
                var description = 'Payment for Usage Charges';

                info.metadata.paymentResult = {};

                var transactionActions = [
                    transactionManager.createAdvancedPaymentAction(),
                    transactionManager.createAddonSubAction(description, info.creditCapAddon.id)
                ];
                if (info.doublePayment) {
                    // in case if a customer has double payment
                    // we have to subscribe subscribe addons 2 times
                    transactionActions.push(transactionManager.createAddonSubAction(description, info.creditCapAddon.id));
                }

                transactionManager.performPayment(info.cache.billingAccountNumber, {
                    type: transactionManager.TYPE_CREDIT_CAP_PAYMENT,
                    amount: info.creditCapAmount,
                    internalId: info.creditCapAddon.id,
                    metadata: {
                        actions: transactionActions
                    }
                }, (status, transaction, callback) => {
                    if (log) common.log("PaymentManager", "makeCreditCapPaymentByBA->transaction: " +
                    "billingAccountNumber=" + info.cache.billingAccountNumber + ", status=" + status);

                    if (status == transactionManager.TR_STATUS_PAYMENT_FINISHED) {
                        // save payments results log
                        info.metadata.paymentResult.eCommOrderRef = transaction.eCommOrderRef;
                        info.metadata.paymentResult.paymentResult = transaction.paymentResult;
                        info.metadata.paymentResult.paymentStatus = transaction.paymentStatus;
                    }
                    callback();
                }, (err, result) => {
                    if (err) {
                        info.metadata.paymentResult.error = err.message;
                        return reject(err);
                    }
                    if (result) {
                        info.validPaymentError = result.validPaymentError;
                    }

                    info.metadata.paymentResult.result = result;
                    return resolve(info);
                });
            });
        }).then((info) => {
            return new Promise((resolve, reject) => {
                if (options.warningOnly || options.silent) {
                    return resolve(info);
                }
                if (options.platform == 'SELFCARE') {

                    // do not send failure / success notification in case if payment is done via selfcare
                    // customers suppose to see operation result in the "dialog response"

                    return resolve(info);
                }

                var notification = {
                    teamID: 5,
                    teamName: "Operations",
                    activity: "",
                    delay: -1,
                    prefix: "65",
                    number: info.cache.number,
                    name: info.cache.first_name,
                    email: info.cache.email,
                    billingAccountNumber: info.cache.billingAccountNumber,
                    amount: info.creditCapAmount,
                    paymentStatus: info.paymentStatus
                };

                if (info.validPaymentError) {
                    var paymentFailureReason = invoiceNotificationManager.getFailureReasonText(info.validPaymentError.status);
                    if (paymentFailureReason) {
                        notification.activity = "pay200_failure";
                        notification.paymentFailureReason = paymentFailureReason;
                    } else {
                        notification.activity = "pay200_failure_no_reason";
                    }
                } else {
                    notification.activity = "pay200_success";
                }

                info.notification = notification;
                notificationSend.deliver(info.notification, null, (err, result) => {
                    if (err) common.error("PaymentManager", "makeCreditCapPaymentByBA->makeCreditCapPaymentByBA: " +
                    "failed to send notification, error=" + err.message);

                    info.metadata.notification = {
                        error: err ? err.message : undefined,
                        result: result
                    };

                    resolve(info);
                });
            });
        }).then((info) => {
            info.metadata.time = new Date().getTime() - startTime;
            processOperationResult("CREDIT_CAP", logId, undefined, info, options, lockKey, callback)
        }).catch((err) => {
            if (!options.warningOnly) {
                var notification = {
                    teamID: 5,
                    teamName: "Operations",
                    activity: "",
                    delay: -1,
                    prefix: "65",
                    number: logsInfo.cache ? logsInfo.cache.number : undefined,
                    name: logsInfo.cache ? logsInfo.cache.billingFullName : undefined,
                    email: logsInfo.cache ? logsInfo.cache.email : undefined,
                    platform: options.platform,
                    billingAccountNumber: billingAccountNumber,
                    amount: logsInfo.creditCapAmount,
                    errorStatus: err.status ? err.status : "ERROR_UNKNOWN",
                    error: err.message,
                    paymentStatus: logsInfo.paymentStatus ? logsInfo.paymentStatus : "NONE"
                };

                if (err.status == "ERROR_CREDIT_CAP_PAYMENT_NOT_ALLOWED"
                    || err.status == "ERROR_CREDIT_CAP_PAYMENT_NOT_REQUIRED") {
                    notification.activity = "pay200_blocked";
                } else {
                    notification.activity = "pay200_internal_error";
                }

                notificationSend.deliver(notification, null, (err, result) => {
                    if (err) common.error("PaymentManager", "makeCreditCapPaymentByBA->catch: " +
                    "failed to send notification, error=" + err.message);
                    // do nothing
                });
            }

            logsInfo.metadata.time = new Date().getTime() - startTime;
            processOperationResult("CREDIT_CAP", logId, err, logsInfo, options, lockKey, callback)
        });
    },

    /**
     *
     */
    parseWirecardError: function (errorCode, responseCode) {
        if (responseCode == 0) {
            return undefined;
        } else if (responseCode == 5) {
            return "PF_NOT_HONORED";
        } else if (responseCode == 41) {
            return "PF_LOST";
        } else if (responseCode == 43) {
            return "PF_STOLEN";
        } else if (responseCode == 51) {
            return "PF_INSUFFICIENT_FUND";
        } else if (responseCode == 14) {
            return "PF_INVALID_CARD_INFORMATION";
        } else if (responseCode == 54) {
            return "PF_CARD_EXPIRED";
        } else {
            return "PF_OTHER";
        }
    }
}

function getPromiseLogCreation(collectionType, cache, params, type, returnInfo, options, idCallback) {
    if (!options) options = {};
    if (!params) params = {};
    if (!returnInfo) returnInfo = {};
    if (!returnInfo.metadata) returnInfo.metadata = {};

    return new Promise((resolve, reject) => {

        var invoiceId = "";
        var invoiceAmounts = "";
        var amount = 0;

        if (params.invoicesInfo) {
            params.invoicesInfo.forEach((item) => {
                if (invoiceId) invoiceId += ", ";
                invoiceId += item.invoiceId;
                amount += item.amount;
                if (invoiceAmounts) invoiceAmounts += ", ";
                invoiceAmounts += (item.amount + "");
            });
        }
        if (params.capPayment) {
            amount = params.capPayment.creditCapAmount;
        }

        // leave 2 decimals only
        amount = parseInt(amount * 100) / 100;

        var obj = {
            status: options.delayed ? "DELAYED" : "IN_PROGRESS",
            type: type,
            error: "",
            reportId: options.reportId,
            platform: options.platform ? options.platform : "UNKNOWN",
            initiator: options.initiator,
            options: options,
            prefix: cache.prefix,
            number: cache.number,
            accountNumber: cache.account,
            billingAccountNumber: cache.billingAccountNumber,
            customerAccountNumber: cache.customerAccountNumber,
            serviceInstanceNumber: cache.serviceInstanceNumber,
            invoiceId: invoiceId,
            amount: amount,
            invoiceAmounts: invoiceAmounts,
            invoicesInfo: params.invoicesInfo,
            capPayment: params.capPayment,
            eCommOrderRef: "",
            ts: new Date().getTime()
        }

        var collection;
        if (collectionType == "INVOICE") {
            collection = db.notifications_pay_now;
        } else if (collectionType == "CREDIT_CAP") {
            collection = db.notifications_credit_cap;
        } else {
            return reject(new BaseError("Log collection is not supported", "ERROR_UNSUPPORTED_LOG_COLLECTION"));
        }

        if (options.logId) {
            if (log) common.log("PaymentManager", "[helper]getPromiseLogCreation: update log, logId=" + options.logId);
            collection.update({_id: db.objectID(options.logId + "")}, {$set: obj}, {multi: true}, (err) => {
                if (err) {
                    common.error("PaymentManager", "[promise]getPromiseLogCreation(update): failed to update sync " +
                    "log, error=" + err.message);
                    return reject(err);
                }

                returnInfo.logId = db.objectID(options.logId + "");
                if (idCallback) idCallback(returnInfo.logId);
                if (log) common.log("PaymentManager", "[promise]getPromiseLogCreation->log(update): id=" + returnInfo.logId);

                resolve(returnInfo);
            });
        } else {
            collection.insert(obj, (err, result) => {
                if (err) {
                    common.error("PaymentManager", "[promise]getPromiseLogCreation->log(insert): failed to save sync " +
                    "log, error=" + err.message);
                    return reject(err);
                }

                returnInfo.logId = result && result.insertedIds ? result.insertedIds[0] : undefined;
                if (idCallback) idCallback(returnInfo.logId);
                if (log) common.log("PaymentManager", "[promise]getPromiseLogCreation->log(insert): id=" + returnInfo.logId);

                resolve(returnInfo);
            });
        }
    });
}

function processOperationResult(collectionType, logId, err, info, options, lockCacheKey, callback) {
    if (!callback) callback = () => {
    }

    if (!err || err.status != "ERROR_OPERATION_LOCKED") {
        if (log) common.log("PaymentManager", "[helper]processOperationResult: remove lock");
        db.unlockOperation(lockCacheKey);
    }

    var result = info ? info.metadata : {};
    var status = err ? (err.status ? err.status : "ERROR") : (options.warningOnly ? "WARNING" : "OK");
    var errorMessage = err ? err.message : undefined;

    if (log) common.log("PaymentManager", "[helper]processOperationResult: finished" +
    (errorMessage ? " with an error, error=" + errorMessage : " successfully"));

    var eCommOrderRef = "";
    if (result && result.paymentResults) {

        result.paymentResults.forEach((item) => {
            if (item.eCommOrderRef) {
                if (eCommOrderRef) eCommOrderRef += ", ";
                eCommOrderRef += item.eCommOrderRef;
            }
        });

        var paymentStatus = (status == "OK") ? "" : ((status == "ERROR") ? "ERROR_TRANSACTION" : status);
        result.paymentResults.forEach((item) => {
            if (item.paymentStatus && (status == "OK" || item.paymentStatus != status)) {
                if (paymentStatus) paymentStatus += ", ";
                paymentStatus += item.paymentStatus;
            }
        });
        status = paymentStatus;
    }

    if (result && result.paymentResult) {
        eCommOrderRef = result.paymentResult.eCommOrderRef;
        status = result.paymentResult && result.paymentResult.paymentStatus ? result.paymentResult.paymentStatus : status;
    }

    var collection;
    if (collectionType == "INVOICE") {
        collection = db.notifications_pay_now;
    } else if (collectionType == "CREDIT_CAP") {
        collection = db.notifications_credit_cap;
    } else {
        // do nothing
    }

    if (logId && collection) {

        var values = {
            status: status,
            eCommOrderRef: eCommOrderRef,
            error: errorMessage,
            result: result
        };
        if (result && result.doublePaymentAmount > 0) {
            values.amount = result.doublePaymentAmount;
        }

        if (log) common.log("PaymentManager", "[helper]processOperationResult: update log, logId=" + logId);
        collection.update({_id: logId}, {$set: values}, {multi: true}, (logErr, result) => {
            if (logErr) common.error("PaymentManager", "[helper]processOperationResult->updateLog: " +
            "failed to update log, error=" + logErr.message);
            // do nothing
        });
    }

    setTimeout(() => {
        callback(err, result);
    }, 200);
}

