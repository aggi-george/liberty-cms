var async = require('async');
var config = require('../../../../config');
var common = require('./../../../../lib/common');
var db = require('./../../../../lib/db_handler');
var ec = require('./../../../../lib/elitecore');
var fs = require('fs');

var LOG = true;
var MONTHS = ['January', 'February', 'March', 'April', 'May', 'June',
    'July', 'August', 'September', 'October', 'November', 'December'];

module.exports = {

    /**
     *
     *
     */
    loadAllFilesInformation: function (folder, callback, statusCallback, displayStart, displayLength) {
        module.exports.loadFilesInformationInternal(folder, null, function (list, invalidCount, totalCount) {
            if (callback) callback(list, invalidCount, totalCount);
        }, function (countLoaded, totalCount, loadedItems, done) {
            if (statusCallback) statusCallback(countLoaded, totalCount, loadedItems, done);
        }, displayStart, displayLength);
    },

    /**
     *
     *
     */
    loadSingleFileInformation: function (folder, file, callback) {
        module.exports.loadFilesInformationInternal(folder, file, function (list, invalidCount, totalCount) {
            if (callback) callback(list && list.length > 0 ? list[0] : null);
        });
    },

    /**
     *
     *
     */
    loadLastFileAfterDate: function (folder, file, callback, hasFolderNumber) {
        var folderInfo = folder.split("/");
        var archiveType = folderInfo[0] + "/" + folderInfo[1];
        var originalArchive = folderInfo[2];
        var folderNumber = hasFolderNumber ? folderInfo[3] : undefined;
        var creationDate = new Date(originalArchive);

        // if bill created not on the 1st day of the month
        // we still have to check one folder

        if (new Date(creationDate.getTime() + 24 * 60 * 60 * 1000).getMonth() != creationDate.getMonth()) {
            // workaround for incorrect time zone
            creationDate = new Date(creationDate.getTime() + 24 * 60 * 60 * 1000);
        } else if (creationDate.getDate() > 1) {
            creationDate = new Date(creationDate.getTime() - (creationDate.getDate() - 1) * 24 * 60 * 60 * 1000);
        }

        if (!creationDate.getTime()) {
            return callback(new Error("Invalid Time"));
        }

        var archive = creationDate.toISOString().split('T')[0];
        var month = getMonthName(archive);
        if (!month) {
            return callback(new Error("Invalid archive"));
        }

        var folderPath = config.BILLS_PATH + '/' + archiveType + '/' + archive + (folderNumber ? "/" + folderNumber : "");
        var filePath = folderPath + '/' + file;

        console.log(filePath);

        fs.stat(folderPath, function (err, stats){
            if (err) {
                err.status = "ERROR_FOLDER_NOT_FOUND";
                return callback(err);
            }
            if (!stats.isDirectory()) {
                var err = new Error("File not found")
                err.status = "ERROR_INVALID_FOLDER_PATH";
                return callback(err);
            }

            fs.stat(filePath, (err, contents) => {
                if (err) {
                    err.status = "ERROR_FILE_INVALID_PATH";
                    return callback(err);
                }

                if (!contents || !contents.isFile()) {
                    var err = new Error("File not found")
                    err.status = "ERROR_INVALID_FILE";
                    return callback(new Error("File not found"));
                }

                var info = {
                    pdf: file,
                    path: filePath,
                    month: month,
                    prefix: "65"
                }

                callback(undefined, info);
            });
        });
    },

    /**
     *
     *
     */
    loadFilesInformationInternal: function (archive, singleFile, callback, statusCallback, displayStart, displayLength) {
        var folder = config.BILLS_PATH + '/' + archive + '/';
        var baseUrl = '/api/1/web/bill/file/' + archive + '/';
        var paymentNegative = folder.indexOf("live_negative") >= 0;
        var paymentFailed = folder.indexOf("live_failed") >= 0;
        var paymentPartial = folder.indexOf("live_partially") >= 0;
        var pdfFilesMap = {};
        var billingAccountsList = [];
        var pdfFiles = [];
        var invalidFiles = [];
        var totalCount = 0;

        var month = getMonthName(archive);
        if (!month) {
            if (LOG) common.error('PdfBillManager', 'can not parse folder, name=' + folder);
            return callback();
        }

        if (LOG) common.log('PdfBillManager', 'loading bill for ' + month);

        fs.readdir(folder, (err, files) => {
            if (err) {
                if (LOG) common.error('PdfBillManager', 'can not read folder, err=' + err.message);
                return callback();
            }

            if (!files) {
                if (LOG) common.error('PdfBillManager', 'can not read folder, no files');
                return callback();
            }

            totalCount = files.length;
            var start = singleFile ? 0 : displayStart ? displayStart : 0;
            var length = singleFile ? files.length : displayLength ? displayStart + displayLength : files.length;

            for (var i = start; i < length && i < files.length; i++) {
                var fileName = files[i];
                var validPdf = fileName.length > 10 && fileName.substring(fileName.length - 4, fileName.length) === ".pdf";

                if (singleFile && singleFile !== fileName) {
                    continue;
                }

                if (validPdf) {
                    var account = "";
                    var invoiceNumber = "";

                    var parsed = fileName.split('_');
                    if (parsed.length > 1) {
                        account = parsed[0];

                        parsed = parsed[1].split('.');
                        if (parsed.length > 1) {
                            invoiceNumber = parsed[0];
                        }
                    }

                    var fileInfo = {
                        billingAccount: account,
                        pdf: fileName,
                        regNumber: invoiceNumber,
                        path: folder + fileName,
                        url: baseUrl + fileName
                    }

                    if (pdfFilesMap[account]) {
                        pdfFilesMap[account].push(fileInfo);
                    } else {
                        pdfFilesMap[account] = [fileInfo];
                        billingAccountsList.push(account);
                    }

                    pdfFiles.push(fileName);
                } else {
                    invalidFiles.push({
                        pdf: fileName,
                        path: folder + fileName
                    });
                }
            }

            if (!billingAccountsList || billingAccountsList.length == 0) {
                if (LOG) common.error('PdfBillManager', 'no bills files found');
                return callback();
            }

            var sortedBillingAccountsList = billingAccountsList.sort(function (a, b) {
                return a.localeCompare(b) ? -1 : 1;
            });

            if (statusCallback) {
                statusCallback(0, sortedBillingAccountsList.length, [], function () {
                });
            }

            db.notifications_bills.find({filename: {$in: pdfFiles}}).sort({"_id": -1}).toArray(function (err, items) {
                if (!items) {
                    return callback([], 0, totalCount);
                }

                var sentFilesMap = {};
                for (var i = 0; i < items.length; i++) {
                    var item = items[i];
                    if (sentFilesMap[item.filename]) {
                        sentFilesMap[item.filename].count++;
                    } else {
                        sentFilesMap[item.filename] = {count: 1, ts: item.ts};
                    }
                }

                var accountsCount = 0;
                var validCount = 0;
                var invalidCount = 0;

                function loadNumberCache(tasks, billingAccount) {
                    tasks.push(function (callback) {
                        ec.getCustomerDetailsBilling(billingAccount, function (err, obj) {
                            accountsCount++;

                            var errorMessage;
                            if (err) {
                                if (LOG) common.error('PdfBillManager', 'Can not load account, billingAccount=' + billingAccount + ', err=' + err);
                                errorMessage = 'Can not load account, billingAccount=' + billingAccount + ', err=' + err;
                                // error is not critical, ignore and continue
                            } else if (obj && obj.billingAccountNumber !== billingAccount) {
                                if (LOG) common.error('PdfBillManager', 'EC response contain incorrect billing account, billingAccount=' + billingAccount);
                                return callback(new Error("EC response contain incorrect billing account"));
                            } else if (obj && !pdfFilesMap[billingAccount]) {
                                if (LOG) common.error('PdfBillManager', 'Loaded account does not have any PDF files, billingAccount=' + billingAccount);
                                return callback(new Error("Loaded account does not have any PDF files"));
                            }

                            var pdfFiles = pdfFilesMap[billingAccount];
                            delete pdfFilesMap[billingAccount];

                            var loadedItems = [];
                            if (!obj) {
                                pdfFiles.forEach(function (pdfFile) {
                                    invalidCount++;
                                    loadedItems.push({
                                        billingAccount: pdfFile.billingAccount,
                                        regNumber: pdfFile.regNumber,
                                        pdf: pdfFile.pdf,
                                        path: pdfFile.path,
                                        month: month,
                                        url: pdfFile.url,
                                        invalid: true,
                                        errorMessage: errorMessage,
                                        paymentFailed: paymentFailed,
                                        paymentPartial: paymentPartial,
                                        paymentNegative: paymentNegative
                                    });
                                });
                            } else {
                                var pdfPassword = module.exports.getPdfPassword(obj);
                                pdfFiles.forEach(function (pdfFile) {
                                    var sentFileInfo = sentFilesMap[pdfFile.pdf];
                                    //if (LOG) common.log('PdfBillManager', 'loaded customer for ' + pdfFile.pdf + ', number=' + obj.number +
                                    //', name=' + obj.name + ', sentAt=' + (sentFileInfo ? sentFileInfo.ts : undefined));

                                    validCount++;
                                    loadedItems.push({
                                        billingAccount: pdfFile.billingAccount,
                                        regNumber: pdfFile.regNumber,
                                        pdf: pdfFile.pdf,
                                        path: pdfFile.path,
                                        url: pdfFile.url,
                                        month: month,
                                        prefix: "65",
                                        account: obj.account,
                                        number: obj.number,
                                        email: obj.email,
                                        name: obj.billingFullName,
                                        customerAccountNumber: obj.customerAccountNumber,
                                        serviceInstanceNumber: obj.serviceInstanceNumber,
                                        billingAccountNumber: obj.billingAccountNumber,
                                        pdfpassword: pdfPassword,
                                        sentAt: sentFileInfo ? sentFileInfo.ts : undefined,
                                        sentCount: sentFileInfo ? sentFileInfo.count : undefined,
                                        invalid: false,
                                        paymentFailed: paymentFailed,
                                        paymentPartial: paymentPartial,
                                        paymentNegative: paymentNegative
                                    });
                                });
                            }

                            if (statusCallback) {
                                statusCallback(accountsCount, sortedBillingAccountsList.length, loadedItems, function () {
                                    callback(undefined, loadedItems);
                                });
                            } else {
                                callback(undefined, loadedItems);
                            }
                        }, true);
                    });
                }

                var tasks = [];
                for (var i = 0; i < sortedBillingAccountsList.length; i++) {
                    loadNumberCache(tasks, sortedBillingAccountsList[i]);
                }

                async.parallelLimit(tasks, 5, function (err, result) {
                    if (err) {
                        return callback([], 0, totalCount);
                    }

                    var list = [];
                    if (result) {
                        result.forEach(function (items) {
                            items.forEach(function (item) {
                                list.push(item);
                            });
                        });
                    }

                    invalidFiles.forEach(function (item) {
                        list.push({
                            pdf: item.pdf,
                            path: item.path
                        });
                    });

                    // sot files by name
                    var sortedList = list.sort(function (file1, file2) {
                        return !file1.billingAccount ? 1 :
                            (!file2.billingAccount ? -1 : file1.billingAccount.localeCompare(file2.billingAccount));
                    });

                    callback(sortedList, invalidCount, totalCount);
                });
            });
        });
    },

    /**
     *
     *
     */
    loadBillsInformation: function (folder, callback) {
        var addTask = function (tasks, name, path) {
            tasks.push((callback) => {

                var year = parseInt(name.substring(0, 4));
                var month = parseInt(name.substring(5, 7));
                var day = parseInt(name.substring(8, 10));

                if (!year || !month || !day || year <= 0 || month <= 0 || day <= 0) {
                    if (LOG) common.error('PdfBillManager', 'can not parse folder, name=' + name);
                    return callback();
                }

                if (LOG) common.log('PdfBillManager', 'parsed folder, name=' + name);
                var paymentMonth = month - 1;
                if (paymentMonth == 0) {
                    paymentMonth = 12;
                }

                fs.readdir(path, (err, files) => {
                    if (err) {
                        if (LOG) common.error('PdfBillManager', 'can not read folder, err=' + err.message);
                        return callback(new Error('Can not read folder(' + err.message + ')'));
                    }

                    if (!files) {
                        if (LOG) common.error('PdfBillManager', 'can not read folder, no files');
                        return callback(new Error('Can not read folder'));
                    }

                    var countValid = 0;
                    var countInvalid = 0;

                    for (var i = 0; i < files.length; i++) {
                        var fileName = files[i];

                        if (fileName.length > 10
                            && fileName.substring(0, 2) === "LW"
                            && fileName.substring(fileName.length - 4, fileName.length) === ".pdf") {
                            countValid++;
                        } else {
                            countInvalid++;
                        }
                    }

                    callback(undefined, {
                        name: name,
                        month: MONTHS[paymentMonth - 1],
                        countValid: countValid,
                        countInvalid: countInvalid
                    });
                });
            });
        };

        fs.readdir(folder, (err, files) => {
            if (err) {
                if (LOG) common.error('PdfBillManager', 'can not read folder, err=' + err.message);
                return callback(new Error('Can not read folder(' + err.message + ')'));
            }

            if (!files) {
                if (LOG) common.error('PdfBillManager', 'can not read folder, no files');
                return callback(new Error('Can not read folder'));
            }

            var tasks = [];
            for (var i = 0; i < files.length; i++) {
                addTask(tasks, files[i], folder + files[i]);
            }

            async.parallelLimit(tasks, 20, function (err, dirs) {
                if (err) {
                    return callback(err);
                }

                dirs.sort(function (dir1, dir2) {
                    return dir2.name.localeCompare(dir1.name)
                });

                callback(dirs);
            });
        });
    },

    /**
     *
     *
     */
    findBillsForUser: function findBillsForUser(billingAccount, callback) {

        var loadFiles = function (type, billingAccount, fileCallback) {
            var rootFolder = config.BILLS_PATH + '/' + type + '/';
            var pdfFiles = [];
            var dirs;

            var baseUrl = '/api/1/web/bill/file/' + type + '/';
            fs.readdir(rootFolder, (err, dirs) => {
                if (err) {
                    if (LOG) common.error('PdfBillManager', 'can not read folder, err=' + err.message);
                    return callback(new Error('Can not read folder(' + err.message + ')'));
                }

                if (!dirs) {
                    if (LOG) common.error('PdfBillManager', 'can not read folder, no files');
                    return callback(new Error('Can not read folder'));
                }

                var addTask = (tasks, dir) => {
                    tasks.push((callback) => {
                        var path = rootFolder + dir;
                        fs.readdir(path + '/', (err, files) => {
                            if (err || !files) {
                                return callback();
                            }

                            var foundFiles = [];
                            for (var j = 0; j < files.length; j++) {
                                var fileName = files[j];

                                if (fileName && fileName.indexOf && fileName.indexOf(billingAccount) >= 0) {
                                    foundFiles.push({
                                        name: fileName, path: path, date: dir,
                                        url: baseUrl + dir + '/' + fileName
                                    });
                                }
                            }

                            callback(undefined, {files: foundFiles});
                        });
                    });
                }

                var tasks = [];
                for (var i = 0; i < dirs.length; i++) {
                    addTask(tasks, dirs[i]);
                }

                async.parallelLimit(tasks, 20, function (err, result) {
                    if (err) {
                        return callback(err);
                    }

                    var files = [];
                    if (result) result.forEach((item) => {
                        if (item && item.files) item.files.forEach((item) => {
                            if (item && item.name) files.push(item);
                        })
                    })

                    fileCallback(files);
                });
            });
        }

        loadFiles('live', billingAccount, function (filesLive) {
            loadFiles('live_failed', billingAccount, function (filesLiveFailed) {
                loadFiles('live_negative', billingAccount, function (filesLiveNegative) {
                    loadFiles('trial', billingAccount, function (filesTrial) {
                        var live = filesLive.concat(filesLiveFailed).concat(filesLiveNegative);
                        live.sort(function (a, b) {
                            return a.date < b.date;
                        });

                        var trial = filesTrial;
                        trial.sort(function (a, b) {
                            return a.date < b.date;
                        });

                        callback({
                            live: live,
                            trial: trial
                        });
                    });
                });
            });
        });
    },

    /**
     *
     *
     */
    getPdfPassword: function getPdfPassword(user) {
        if (user && user.birthday) {
            var dobParts = user.birthday.split("-");
            var pdfPassword = dobParts.length == 3 ? (dobParts[2] + dobParts[1] + dobParts[0]) : "";
            return pdfPassword;
        } else {
            return "";
        }
    }
}


function getMonthName(archive) {
    var month = archive && archive.length >= 10 ? parseInt(archive.substring(archive.length - 5, archive.length - 3)) : -1;
    if (month < 1 || month > 12) {
        if (LOG) common.error('PdfBillManager', 'getMonthName: can not parse folder, name=' + folder);
        return undefined;
    }

    var paymentMonth = month - 1;
    if (paymentMonth == 0) {
        paymentMonth = 12;
    }

    return MONTHS[paymentMonth - 1];
}

