var async = require('async');
var db = require('../../../../lib/db_handler');
var ec = require('../../../../lib/elitecore');
var elitecoreUsageService = require('../../../../lib/manager/ec/elitecoreUsageService');
var common = require('../../../../lib/common');
var diameterClient = require('../../../core/manager/diameter/diameterClient');
var constants = require('../../../core/constants');
var packageManager = require(__core + '/manager/packages/packageManager');


exports.chargeWhatsappPenalty = chargeWhatsappPenalty;
function chargeWhatsappPenalty(number, threshold, callback){
    db.cache_lock("cache",  "whatsapp_penalty_" + number, "cms", constants.TIMEOUTS.WHATSAPP_PENALTY_LOCK_TIMEOUT, (lock) => {
        if(lock == 1){
            common.log("WA_PENALTY_LOCK_SUCCESS", "Lock Acquired for " + number);
            var chargeType;
            var status;
            async.parallel([
                function(callback){
                    elitecoreUsageService.loadRemainingDataForPackage(number, ec.findPackageName(ec.packages(), "CirclesOne").id, callback);
                }, function(callback){
                    elitecoreUsageService.loadRemainingDataForPackage(number, ec.findPackageName(ec.packages(), "Plus 2020").id, callback);
                }, function(callback){
                    getPaneltyByDefinition("excess_whatsapp_6$", callback);
                }, function(callback){
                    getPaneltyByDefinition("excess_whatsapp_1GB", callback);
                }], function(err, results){
                    if(err){
                        callback(err);
                    }else{
                        var totalDataAvailable = results[0] + results[1];
                        if(totalDataAvailable > 1){
                            db.penaltyHistory.findOne({number: number, definitionKey: "excess_whatsapp_1GB", date: {$gt: new Date().getTime() - 1800000}}, function(err, item){
                                if(item){
                                    callback(null, {status: "panelty_data_already_charged"});
                                }else{
                                    chargePaneltyByData(number, results[3].key, results[3].value, function(err, data){
                                        if(err){
                                            callback(err, null);
                                        }else{
                                            addPaneltyHistory(number, results[3].key, function(err, data){
                                                if(err){
                                                    callback(err);
                                                }else{
                                                    callback(null, {status: "panelty_data_charged"});
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }else{
                            getPaneltyHistory(number, results[2].key, function(err, paneltyHistory){
                                if(paneltyHistory && paneltyHistory.length > 0 && new Date(new Date().setHours(0,0,0)).setDate(1) <  paneltyHistory[0].date){
                                    callback(null, {status: "panelty_addon_charged_before"});
                                }else{
                                    chargePaneltyByAddon(number, results[2].key, results[2].value, function(err, data){
                                        if(err){
                                            callback(err, null);
                                        }else{
                                            addPaneltyHistory(number, results[2].key, function(err, results){
                                                if(err){
                                                    callback(err);
                                                }else{
                                                    callback(null, {status: "panelty_addon_charged"});
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    }
            });
        }else {
            common.log("WA_PENALTY_LOCK_FAIL", "Lock Failed for " + number);
            callback(null, {status: "failed_to_acquire_penalty_lock"});
        }
    });
}

function chargePaneltyByAddon(number, paneltyDefinitiion, addonName, callback){
    var product = ec.findPackageName(ec.packages(), addonName);
    packageManager.addonUpdate("65", number, "PUT", product.id, undefined, true, function(err, payment){
    logPanelty(number, paneltyDefinitiion, err); 
        callback(err, payment);
    }, "0", "false");
}

function chargePaneltyByData(number, paneltyDefinitiion, dataAmount, callback){
    if(dataAmount.indexOf("MB") > -1){
        var burnout = parseFloat(dataAmount);
    }else{
        var burnout = parseFloat(dataAmount)*1024;
    }
    diameterClient.spendData("65" + number, burnout, 300, function(err, result){
        logPanelty(number, paneltyDefinitiion, err);
        callback(err, result);
    });
}

function chargePaneltyByCash(number, dataAmount, callback){

}

function logPanelty(number, paneltyDefinitiion, err){
    db.weblog.insert({
        "ts": new Date().getTime(),
        "ip": "webhook",
        "type": "panelty",
        "paneltyDefinitiion": paneltyDefinitiion,
        "id": number,
        "username": "ec",
        "result": {},
        "err": JSON.stringify(err),
        "code": 0
    });
}   

function addPaneltyDefinition(key, type, value, reason, callback){
    var paneltyDefinition = {
        key: key,
        type: type,
        value : value,
        reason: reason
    };
    db.paneltyDefinitions.insertOne(paneltyDefinition, function(err, result){
        callback(err, result);
    });
}


function addPaneltyHistory(number, definitionKey, callback){
    var paneltyHistory = {
        number: number,
        definitionKey: definitionKey,
        date: new Date().getTime()
    };
    db.penaltyHistory.insertOne(paneltyHistory, callback);
}

function getPaneltyHistory(number, definitionKey, callback){
    db.penaltyHistory.find({number: number, definitionKey: definitionKey}).sort({date: -1}).toArray(callback);
}

function getPaneltyByDefinition(definitionKey, callback){
    db.paneltyDefinitions.findOne({key: definitionKey}, callback);
}
