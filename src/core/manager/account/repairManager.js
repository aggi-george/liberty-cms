var config = require('../../../../config');
var common = require('../../../../lib/common');
var db = require('../../../../lib/db_handler');
var ec = require('../../../../lib/elitecore');
var async = require('async');
var request = require('request');

var BaseError = require('../../errors/baseError');
var elitecoreUserService = require('../../../../lib/manager/ec/elitecoreUserService');
var ecommManager = require('../ecomm/ecommManager');
var notificationSend = require(__core + '/manager/notifications/send');
var constants = require(__core + '/constants');

var log = config.LOGSENABLED;
var pairUnpaidLock = config.LOGSENABLED;

const accountManager = require('./accountManager');

module.exports = {

    /**
     *
     */
    simPair: function (serviceInstanceNumber, number, iccid, callback) {
        if (!callback) callback = () => {
        }

        if (!serviceInstanceNumber || !number || !iccid) {
            return callback(new BaseError("Invalid parameters", "ERROR_INVALID_PARAMS"));
        }

        var lockKey = pairUnpaidLock + "_" + serviceInstanceNumber;

        Promise.resolve({}).then((info) => {
            return new Promise((resolve, reject) => {
                db.lockOperation(lockKey, {}, 5 * 60 * 1000, function (err) {
                    if (err) {
                        common.error("RepairManager", "simPair->lock: failed to lock, err=" + err.message);
                        return reject(new BaseError("Sim pair for SIN " + serviceInstanceNumber + " is in progress", err.status));
                    }
                    resolve(info);
                });
            });
        }).then((info) => {
            return new Promise((resolve, reject) => {
                ec.getCustomerDetailsServiceInstance(serviceInstanceNumber, true, function (err, cache) {
                    if (err) {
                        return reject(err);
                    }
                    if (!cache) {
                        return reject(new BaseError("Customer not found", "ERROR_CUSTOMER_NOT_FOUND"));
                    }

                    var foundPairedICCID;
                    var foundPairedIMSI;
                    cache.serviceInstanceInventories.some((item) => {
                        if (item.number == number && item.iccid) {
                            foundPairedICCID = item.iccid;
                            foundPairedIMSI = item.imsi;
                            return true;
                        }
                    });

                    if (foundPairedICCID) {
                        return reject(new BaseError("Number " + number + " is already paired, iccid " +
                            foundPairedICCID + ", imsi " + foundPairedIMSI, "ERROR_NUMBER_ALREADY_PAIRED"));
                    }

                    resolve(info);
                });
            });
        }).then((info) => {
            return new Promise((resolve, reject) => {
                simRePair(serviceInstanceNumber, "UpdateCustomer", number, iccid, undefined, function (err, result) {
                    if (err) {
                        return reject(err);
                    }

                    var errorCode = !result || !result.return ? -1 : parseInt(result.return.responseCode);
                    var errorMessage = !result || !result.return ? "empty" : result.return.responseMessage;
                    if (errorCode < 0) {
                        return callback(new BaseError("Failed to pair SIM, invalid response, " +
                        "(errorCode=" + errorCode + ", errorMessage=" + errorMessage + ")", "ERROR_INVALID_BSS_RESPONSE"));
                    }

                    // re-cache customer account
                    elitecoreUserService.loadUserInfoByServiceInstanceNumber(serviceInstanceNumber, () => {
                        // do nothing
                    }, true);

                    info.result = {paired: true};
                    return resolve(info);
                });
            });
        }).then((info) => {
            db.unlockOperation(lockKey);
            callback(undefined, info.result)
        }).catch((err) => {
            if (!err || err.status != "ERROR_OPERATION_LOCKED") {
                if (log) common.log("RepairManager", "simPair: remove lock");
                db.unlockOperation(lockKey);
            }
            callback(err);
        });
    },

    /**
     *
     */
    simChange: function (serviceInstance, number, iccid, newIccid, callback) {
        if (!callback) callback = () => {
        }

        if (!serviceInstance || !number || !iccid || !newIccid) {
            return callback(new BaseError("Invalid parameters", "ERROR_INVALID_PARAMS"));
        }

        common.log("SimRepair", "started sim change");
        simUnPair(serviceInstance, "SimExchange", "1", number, function (err, result) {
            common.log("SimRepair", "sim unpair  error : " + JSON.stringify(err));
            common.log("SimRepair", "sim unpair result  : " + JSON.stringify(result));
            if (err) {
                return callback(err);
            }

            var errorCode = !result || !result.return ? -1 : parseInt(result.return.responseCode);
            common.log("SimRepair", "sim unpair  error code : " + errorCode);
            if (errorCode < 0) {
                return callback(new BaseError("Failed to un-pair SIM, errorCode=" + errorCode,
                    "ERROR_INVALID_BSS_RESPONSE"));
            }

            common.log("SimRepair", "started sim repair");
            simRePair(serviceInstance, "SimExchange", number, newIccid, undefined, function (err, result) {
                common.log("SimRepair", "sim repair  error : " + JSON.stringify(err));
                common.log("SimRepair", "sim repair result  : " + JSON.stringify(result));
                if (err) {
                    return callback(err);
                }

                var errorCode = !result || !result.return ? -1 : parseInt(result.return.responseCode);
                common.log("SimRepair", "sim repair  error code : " + errorCode);
                if (errorCode < 0) {
                    return callback(new BaseError("Failed to re-pair SIM, errorCode=" + errorCode,
                        "ERROR_INVALID_BSS_RESPONSE"));
                }

                // re-cache customer account
                elitecoreUserService.loadUserInfoByServiceInstanceNumber(serviceInstance, () => {
                    // do nothing
                }, true);

                console.log("paring success");
                callback(undefined, {paired: true});
            });
        });
    },

    /**
     *
     */
    numberChange: function (serviceInstance, number, iccid, newNumber, category, billing_cycle, callback) {
        if (!callback) callback = () => {
        }

        if (!serviceInstance || !number || !iccid || !newNumber) {
            return callback(new BaseError("Invalid parameters", "ERROR_INVALID_PARAMS"));
        }

        simUnPair(serviceInstance, "NumberExchange", "0", number, function (err, result) {
            if (err) {
                return callback(err);
            }

            var errorCode = !result || !result.return ? -1 : parseInt(result.return.responseCode);
            if (errorCode < 0) {
                return callback(new BaseError("Failed to un-pair sim, errorCode=" + errorCode,
                    "ERROR_INVALID_BSS_RESPONSE"));
            }

            simRePair(serviceInstance, "NumberExchange", number, iccid, newNumber, function (err, result) {
                if (err) {
                    return callback(err);
                }

                var errorCode = !result || !result.return ? -1 : parseInt(result.return.responseCode);
                if (errorCode < 0) {
                    return callback(new BaseError("Failed to re-pair sim with new number, errorCode=" + errorCode,
                        "ERROR_INVALID_BSS_RESPONSE"));
                }

                var product = getProduct(category);
                if (!product) {
                    return callback(new BaseError("Failed to find fees product, invalid response",
                        "ERROR_FEES_PRODUC_NOT_FOUND"));
                }

                var sub = {
                    product_id: product.id,
                    recurrent: product.recurrent,
                    effect: product.sub_effect
                };

                ec.addonSubscribe(number, serviceInstance, [sub], billing_cycle, function (err, result) {
                    if (err) {
                        return callback(err);
                    }
                    if (!result) {
                        return callback(new BaseError("Failed subscribe number fees, invalid response",
                            "ERROR_INVALID_BSS_RESPONSE"));
                    }

                    // re-cache customer account
                    elitecoreUserService.loadUserInfoByServiceInstanceNumber(serviceInstance, () => {
                        // do nothing
                    }, true);


                    let notification = {
                        activity: 'number_change',
                        // number: number,
                        // newNumber: newNumber,
                        oldNumber: number,
                        teamID: 4,
                        teamName: 'Operations',
                        serviceInstanceNumber : serviceInstance ? serviceInstance : 'N/A'
                    };
                    notificationSend.deliver(notification, null, (err3, result) => {
                        if(err3){
                            common.error('Notification', 'Failed to send number change notification');
                        }
                    });

                    callback(undefined, {paired: true});
                });
            });
        });
    },

    /**
     *
     */
    loadFreeICCIDFromPool: function (type, lockedBy, callback) {
        if (!callback) callback = () => {
        }

        if (!lockedBy) {
            return callback(new BaseError("Invalid parameters", "ERROR_INVALID_PARAMS"));
        }

        loadLockedICCIDs(lockedBy, function (err, alreadyLockedICCID) {
            if (err) {
                return callback(err);
            }
            if (alreadyLockedICCID) {
                return callback(undefined, {
                    code: 0,
                    iccid: alreadyLockedICCID
                });
            }

            loadAvailableICCIDs(type, function (err, result) {
                if (err) {
                    return callback(err);
                }
                if (!result || !result[0]) {
                    return callback(new BaseError("No available ICCIDs", "ERROR_NO_AVAILABLE_ICCID"));
                }

                lockICCID(result, lockedBy, function (error, lockedICCID) {
                    if (err) {
                        return callback(err);
                    }
                    if (!lockedICCID) {
                        return callback(new BaseError("Failed lo lock any ICCID", "ERROR_ICCID_NOT_LOCKED"));
                    }

                    callback(null, {
                        code: 0,
                        iccid: lockedICCID
                    });
                });

            });
        });
    },

    updateSimChangeStatusAndGetLinkFromEcomm: function (serviceInstanceNumber, status, newIccid, type, callback) {
        ecommManager.executeECommRequest("API", "GET", "api/kirk/v1/replace_sim_url/", {
            serviceInstanceNumber: serviceInstanceNumber,
            status: status,
            newIccid: newIccid ? newIccid : "NA",
            type: type
        }, {}, function (err, result) {
            if (err || result.status == "404") {
                callback(new BaseError("Failed to notify ECOMM for sim change", "SIM_CHANGE_NOTIFY_FAILURE"));
            } else {
                callback(null, result);
            }
        });
    },

    checkEcommStatusForRepairRequest: function (serviceInstanceNumber, callback) {
        ecommManager.executeECommRequest("API", "GET", "api/kirk/v1/replace_sim_allowed/", {serviceInstanceNumber: serviceInstanceNumber}, {}, function (err, result) {
            if (err || result.status == "404") {
                callback(new BaseError("Sim change staus check in ecomm failed", "SIM_CHANGE_REQUEST_STATUS_INQUIRE_FROM_ECOMM_FAILURE"));
            } else if (result.code == -1 && !result.allowed) {
                callback(new BaseError("Sim change request exist in ecomm", "SIM_CHANGE_REQUEST_ALREADY_EXIST_IN_ECOMM"));
            } else {
                callback(null, {repair: true});
            }
        });
    },

    simChangeAndNotify: function (serviceInstanceNumber, finalCallback) {
        common.log("SimRepair", "sim change request service instance number: " + serviceInstanceNumber);
        var tempStore = {};
        async.series([function (callback) {
            module.exports.checkEcommStatusForRepairRequest(serviceInstanceNumber, function (err, result) {
                if (err) {
                    callback({ecomm_check_failed: true, errorObj: err});
                } else {
                    callback(null);
                }
            });
        },
            function (callback) {
                getRequiredCustomerData(serviceInstanceNumber, function (err, result) {
                    if (!err && result.oldIccid) {
                        tempStore.oldIccid = result.oldIccid;
                        tempStore.serviceAccountEmail = result.serviceAccountEmail;
                        tempStore.number = result.number;
                        tempStore.name = result.name;
                    }
                    common.log("SimRepair", "customer data: " + JSON.stringify(tempStore));
                    console.log(tempStore);
                    callback(err, result);
                });
            }, function (callback) {
                module.exports.loadFreeICCIDFromPool("replacement", tempStore.number, function (err, result) {
                    common.log("SimRepair", "iccid data: " + JSON.stringify(result));
                    if (!err && result.iccid && result.iccid.length > 0) {
                        tempStore.newIccid = result.iccid;
                        callback(null, result);
                    } else {
                        callback(err ? err : new BaseError("iccid not available", "ICCID_NOT_AVAILABLE"));
                    }
                });
            }, function (callback) {
                module.exports.simChange(serviceInstanceNumber, tempStore.number, tempStore.oldIccid, tempStore.newIccid, function (err, result) {
                    common.log("SimRepair", "sim change: " + JSON.stringify(result));
                    if (err || result.paired != true) {
                        callback(new BaseError(err.message, "SIM_RE_PAIR_FAILED"));
                    } else {
                        callback(null, result);
                        module.exports.inventoryAlert();
                    }
                });
            }], function (err, result) {
            if (err && err.ecomm_check_failed) {
                return finalCallback(err.errorObj);
            }
            module.exports.updateSimChangeStatusAndGetLinkFromEcomm(serviceInstanceNumber, err ? err.status : "SUCCESS", tempStore.newIccid, "LOST", function (err2, results) {
                common.log("SimRepair", "sim change: " + JSON.stringify(results));
                if (err2 || results.code != 0 || err) {
                    finalCallback(new BaseError(err ? err.message : (err2 ? err2.message : "Failed to notify ECOMM for sim change"), err ? err.status : (err2 ? err2.status : "SIM_CHANGE_NOTIFY_FAILURE")));
                } else {
                    var notification = {
                        activity: "sim_loss_schedule_delivery",
                        name: tempStore.name,
                        prefix: "65",
                        number: tempStore.number,
                        email: tempStore.serviceAccountEmail,
                        teamID: 4,
                        teamName: "Operations",
                        sim_schedule_url: results.schedule_delivery_url
                    };
                    notificationSend.deliver(notification, null, function (err3, result) {
                        if (err3) {
                            finalCallback(new BaseError(err3.message, err3.status));
                        } else {
                            finalCallback(null);
                        }
                    });
                }
            });
        });

    },
    inventoryAlert: function(){
        db.iccidPool.count({status: "Available", purpose: "replacement"}, function(err, count){
            if(!err && count <= constants.ICCID_REPLACEMENT_INVENTORY_ALERT_START_THRESHOLD && count%10 == 0){
                var notification = {
                    activity: "replacement_iccid_inventory_low",
                    teamID: 5,
                    teamName: "Operations",
                    remaining: count
                };
                notificationSend.deliver(notification, null, function (err, result) {
                    if (err) {
                        common.error("Iccid inventory alert failed. " + JSON.stringify(err));
                    }
                });
            }
        });
    }
}

function getRequiredCustomerData(serviceInstanceNumber, callback) {
    var oldIccid;
    ec.getCustomerDetailsServiceInstance(serviceInstanceNumber, true, function (err, cache) {
        if (err) {
            callback(new BaseError("could not fetch customer from EC", "CUSTOMER_FETCH_ERROR"));
        } else {
            if (cache.serviceInstanceCustomerStatus == "Active") {
                var filteredItem = cache.serviceInstanceInventories.filter(function (item) {
                    return (item.number == cache.number && item.iccid)
                })[0];
                if (filteredItem && filteredItem.iccid && filteredItem.iccid.length > 0) {
                    oldIccid = filteredItem.iccid;
                    callback(null, {
                        oldIccid: oldIccid,
                        serviceInstanceNumber: cache.serviceInstanceNumber,
                        serviceAccountEmail: cache.serviceAccountEmail,
                        name: cache.billingFullName,
                        number: cache.number
                    });
                } else {
                    callback(new BaseError("User do not have an existing iccid", "CURRENT_ICCID_NOT_AVAILABLE"));
                }
            } else {
                callback(new BaseError("user inactive", "USER_INACTIVE"));
            }
        }
    }, false);
}

function lockICCID(list, lockedBy, callback) {
    if (!callback) callback = () => {
    }

    var tasks = [];
    list.forEach((item) => {
        (function (iccid) {
            tasks.push(function (cb) {
                db.cache_lock("cache", "iccidLock_" + iccid, lockedBy, 60000, (lock) => {
                    if (lock) {
                        // managed to lock an ICCID, need to interrupt async
                        // returning error in order to stop search
                        return cb(new BaseError("Lock interruption", "ERROR_INTERRUPTION"), iccid);
                    }

                    return cb(undefined);
                });
            });
        }(item))
    });

    async.series(tasks, (err, result) => {
        if (err) {
            if (err.status == "ERROR_INTERRUPTION") {
                return callback(undefined, result[result.length - 1]);
            }
            return callback(err);
        }

        return callback(new BaseError("No available ICCIDs", "ERROR_NO_AVAILABLE_ICCID"));
    });
}

function loadAvailableICCIDs(type, callback) {
    if (!callback) callback = () => {
    }
    var findQuery = {"status": "Available"};
    findQuery.purpose = type;

    db.iccidPool
        .find(findQuery)
        .sort({"_id": 1})
        .limit(10)
        .toArray((err, result) => {
            if (err) {
                return callback(err)
            }
            if (!result) {
                return callback(new BaseError("Empty db response", "ERROR_EMPTY_DB_RESPONSE"))
            }

            var tasks = [];
            result.forEach((item) => {
                (function (available) {
                    tasks.push((cb) => {
                        db.cache_get("cache", "iccidLock_" + available, (value) => {
                            if (value) {
                                return cb();
                            }

                            return accountManager.getInventoryStatusFromEC(available)
                                .then(({status}) => {
                                    if (status == "Available") {
                                        return cb(undefined, available);
                                    }
                                    db.iccidPool.update({"_id": available}, {"$set": {"status": status}});
                                    return cb();
                                })
                                .catch(err => {
                                    return cb();
                                })
                        });
                    });
                }(item._id))
            });

            async.parallel(tasks, (errAsync, resultAsync) => {
                callback(undefined, resultAsync.filter((o) => {
                    return o;
                }));
            });
        });
}

function loadLockedICCIDs(lockedBy, callback) {
    db.cache_keys("cache", "iccidLock_*", function (keys) {
        if (!keys) {
            return callback();
        }

        var tasks = [];
        keys.forEach((item) => {
            (function (key) {
                tasks.push(function (cb) {
                    db.cache_get("cache", key, (value) => {
                        if (!value || value != lockedBy) {
                            return cb();
                        }

                        var id = key.split("_")[1];
                        return accountManager.getInventoryStatusFromEC(id)
                            .then(({status}) => {
                                if (status == "Available") {
                            // found available ICCID, need to interrupt async
                            // returning error in order to stop search
                                    return cb(new BaseError("Lock interruption", "ERROR_INTERRUPTION"), id);
                                }
                                return cb();
                            })
                            .catch(err => {
                                return cb();
                            });
                    });
                });
            }(item))
        });

        async.series(tasks, (err, result) => {
            if (err) {
                if (err.status == "ERROR_INTERRUPTION") {
                    return callback(undefined, result[result.length - 1]);
                }
                return callback(err);
            }

            return callback();
        });
    });
}

function simUnPair(serviceInstance, remarks, reasonOperation, msisdn, callback) {
    common.log("SimRepair", "sim unpair call is been called");

    // $xml is to override, without this,
    // tns: will be appended to remarks and extraParam

    ec.unpairInventory({
        "$xml": "<remarks>" + remarks + "</remarks>" +
        "<reasonOperation>" + reasonOperation + "</reasonOperation>" +
        "<inventoryNumber>" + msisdn + "</inventoryNumber>" +
        "<extraParam><key>Provision_Type</key><value>" + remarks + "</value></extraParam>" +
        "<serviceInstanceNumber>" + serviceInstance + "</serviceInstanceNumber>"
    }, callback);
}

function simRePair(serviceInstance, remarks, msisdn, iccid, newMSISDN, callback) {

    // $xml is to override, without this,
    // tns: will be appended to remarks and extraParam

    if (!newMSISDN) {
        ec.repairInventory({
            "$xml": "<remarks>" + remarks + "</remarks>" +
            "<inventoryNumber>" + msisdn + "</inventoryNumber>" +
            "<bundleInventoryNumber>" + iccid + "</bundleInventoryNumber>" +
            "<extraParam><key>Provision_Type</key><value>" + remarks + "</value></extraParam>" +
            "<serviceInstanceNumber>" + serviceInstance + "</serviceInstanceNumber>"
        }, callback);
    } else {
        ec.repairInventory({
            "$xml": "<remarks>" + remarks + "</remarks>" +
            "<inventoryNumber>" + msisdn + "</inventoryNumber>" +
            "<bundleInventoryNumber>" + iccid + "</bundleInventoryNumber>" +
            "<extraParam><key>New_MSISDN</key><value>" + newMSISDN + "</value></extraParam>" +
            "<extraParam><key>Provision_Type</key><value>" + remarks + "</value></extraParam>" +
            "<serviceInstanceNumber>" + serviceInstance + "</serviceInstanceNumber>"
        }, callback);
    }
}

function getProduct(category) {
    var gold = ec.findPackageName(ec.packages(), "Gold Number");
    var silver = ec.findPackageName(ec.packages(), "Silver Number");
    var normal = ec.findPackageName(ec.packages(), "Number Change Fee");
    switch (category) {
        case "GOLD":
            return gold;
            break;
        case "SILVER":
            return silver;
            break;
        case "NORMAL":
            return normal;
            break;
        default:
            return undefined;
    }
}
