var ec = require('../../../../lib/elitecore');
var db = require('../../../../lib/db_handler');
var common = require('../../../../lib/common');
var invoiceManager = require('../../manager/account/invoiceManager');
var dateTimeLib = require('../../../utils/dateTime');
var util = require("util");
var elitecoreUtils = require('../../../../lib/manager/ec/elitecoreUtils');
var BaseError = require('../../../core/errors/baseError.js');
const Bluebird = require('bluebird');
const SUSPENSION_START_TIMETSAMP = {
    hour: 3,
    minute: 59,
    second: 59,
    millisecond: 0,
};

module.exports = {
    getSuspensionStartDates: function() {
        var date = new Date();
        var year = date.getFullYear();
        var month = date.getMonth();
        var lastDayOfMonth = new Date(year, month + 1, 0).getDate();
        var currentDay = date.getDate();
        var currentHour = date.getHours();
        var startDates = [];
        var nextCycleEndDate;
        // If the customer tries to schedule a termination after 12PM SGT on the last day
        // of the month, then he should be able to schedule only from the second month onwards.
        // for ex if the customer tries to schedule on 31 Oct 1 PM SGT the start dates for the
        // termination will be Dec 1 else the customer can schedule from Nov 1.
        if (currentDay === lastDayOfMonth && currentHour > SUSPENSION_START_TIMETSAMP.hour) {
            var cycle = elitecoreUtils.calculateBillCycle(undefined, undefined, date);
            var nextCycle = elitecoreUtils.calculateBillCycle(undefined, undefined, cycle.endDateUTC)
            startDates.push(getSuspensionDateTime(nextCycle.endDateUTC));
            nextCycleEndDate = nextCycle.end;
        } else {
            var nextCycle = elitecoreUtils.calculateBillCycle(undefined, undefined, date);
            nextCycleEndDate = nextCycle.end;
            startDates.push(getSuspensionDateTime(nextCycle.endDateUTC));
        }

        for (var i=0; i < 5; i++) {
            nextCycle = elitecoreUtils.calculateBillCycle(undefined, undefined, nextCycleEndDate);
            startDates.push(getSuspensionDateTime(nextCycle.endDateUTC));
            nextCycleEndDate = nextCycle.end;
        }
        return startDates;
    },

    getScheduledSuspensionBySIN: function (serviceInstanceNumber) {
        const query = {
            serviceInstanceNumber,
            action: 'suspend',
            activity: 'suspension_effective',
            processed: {
                $exists: false
            }
        };

        return new Bluebird((resolve, reject) => {
            db.scheduler.findOne(query, (err, result) => {
                if (err) {
                    reject(err);
                }else if (result) {
                    reject({
                        alreadySuspended: true
                    });
                }else {
                    resolve();
                }
            });
        });
    },

    // the customer should not have any unpaid invoices
    // the customer should have atleast one paid invoice
    checkInvoiceEligibility: function (billingAccountNumber) {
        if (!billingAccountNumber) {
            Bluebird.reject(new BaseError(
                "SuspensionManager: Missing billingAccountNumber in the params",
                "ERROR_INVALID_PARAMS"));
        }
        let suspendManager = module.exports;

        return suspendManager.checkforBill(billingAccountNumber, "OPEN")
            .then(({ totalCount }) => {
                if(totalCount > 0) {

                    common.error("SuspensionManager", "BillingAccountNumber " + billingAccountNumber +
                    " is ineligible for suspension since the number of unpaid invoice is > 0");
                    return getSuspensionElibilityResult(getInvoicePendingResponse());
                }
                return suspendManager.checkforBill(billingAccountNumber, "PAID")
                    .then(({ totalCount }) => {
                        if (!totalCount) {
                            common.error("SuspensionManager", "BillingAccountNumber " + billingAccountNumber +
                            " is ineligible for suspension since the number of paid invoices is equal to 0");
                            return getSuspensionElibilityResult(getEarlyTermResponse());
                        }
                        return getSuspensionElibilityResult(allowSuspensionResponse());
                    });
            });
    },

    checkforBill: (billAccNo, type) => {
        const loadBillPromise = Bluebird.promisify(invoiceManager.loadBills);
        const options = {
            type,
            billingAccountNumbers: [billAccNo],
        };
        return loadBillPromise(null, options);
    },

    cancelSuspension: (effectiveId, overId) => {
        return findAndCancelSuspension(effectiveId)
            .then(findAndCancelSuspension.bind(null, overId))
            .then(cancelNotifications);
    }
}

function allowSuspensionResponse() {
    return {
        value: true,
        reason: null,
        description: null,
        disclaimer: null
    };
}

function getEarlyTermResponse() {
    return {
        value: false,
        reason: 'EARLY_SUSPENSION',
        description: 'Not eligible for suspension due to: Number is less than 1 month',
        disclaimer: 'You must have at least 1 billing cycles'
    };
}

function getInvoicePendingResponse() {
    return {
        value: false,
        reason: 'PAYMENT_PENDING',
        description: 'Not eligible for suspension due to Outstanding balance',
        disclaimer: 'The balance must be settled before you can suspend this number'
    };
}

function getSuspensionDateTime(date) {
    var restrictedTime = {
        date: date,
        hour: SUSPENSION_START_TIMETSAMP.hour,
        minute: SUSPENSION_START_TIMETSAMP.minute,
        second: SUSPENSION_START_TIMETSAMP.second,
        millisecond: SUSPENSION_START_TIMETSAMP.millisecond
    };
    return dateTimeLib.generateDate(restrictedTime);
}

function getSuspensionElibilityResult(eligibility) {
    return {
        eligibility,
    }
}

function findAndCancelSuspension(id) {
    return new Bluebird((resolve, reject) => {
        db.scheduler.findOne({_id: db.objectID(id)}, (err, item) => {
            if (err) {
                reject(err);
            }else if(item && item.start) {
                db.scheduler.update(
                    {
                        _id: db.objectID(id)
                    },
                    {
                        "$set": {
                            "start": 0, 
                            "oldStart": item.start,
                            "processed": {
                                "result": "deleted"
                            }
                        }
                    }, 
                    (err, result) => {
                        if (!err) {
                            resolve(item);
                        }else {
                            reject(err);
                        }
                    }
                )
            }else {
                reject(new BaseError('Record not found', "NOT_FOUND"));
            }
        });
    })
}

function cancelNotifications({serviceInstanceNumber, start}) {
    return new Bluebird((resolve, reject) => {
        db.scheduler.updateMany({
            serviceInstanceNumber,
            action: 'notification'
        },{
            "$set": {
                "start": 0, 
                "oldStart": start,
                "processed": {
                    "result": "deleted"
                }
            }
        },
         (err, response) => {
            if (!err) {
                resolve()
            } else {
                reject(err);
            }
        })
    })
}
