const ec = require('../../../../lib/elitecore');
const db = require('../../../../lib/db_handler');
const common = require('../../../../lib/common');
const Bluebird = require('bluebird');
const invoiceManager = require('../../manager/account/invoiceManager');
const dateTimeLib = require('../../../utils/dateTime');

const terminationTimeStamp = {
    hour: 7,
    minute: 59,
    second: 59,
    millisecond: 59,
};

class TerminationManager {
    static getTerminationDate(instanceNum, numberOfMonths) {
        const getCustomerDetailsPromise = Bluebird.promisify(ec.getCustomerDetailsService);
        return getCustomerDetailsPromise(instanceNum, false)
            .then((cache) => {
                if (cache) {
                    const {
                        serviceInstanceEarliestTerminationDate,
                        billingAccountNumber, activationDate, billing_cycle } = cache;

                    return TerminationManager.getScheduledTerminationBySIN(instanceNum)
                        .then((isTerminated) => {
                            if (!isTerminated) {
                                const isEarlyTerminate = TerminationManager
                                    .checkForEarlyTermination(
                                        billing_cycle, activationDate);

                                if (isEarlyTerminate) {
                                    return TerminationManager.getEarlyTermResponse();
                                }
                                return TerminationManager.getUnpaidBill(billingAccountNumber)
                                    .then((isUnpaid) => {
                                        if (!isUnpaid) {
                                            return TerminationManager
                                                .generateTerminationDates(
                                                    serviceInstanceEarliestTerminationDate,
                                                    numberOfMonths);
                                        }
                                        return TerminationManager.getInvoicePendingResponse();
                                    });
                            }
                            return TerminationManager.alreadyTerminatedResponse();
                        });
                }
                throw new Error(`Cache not found for ${instanceNum}`);
            })
            .catch((err) => {
                common.error('TerminationManager Error Caught =>', err);
                throw new Error(err);
            });
    }

    static getScheduledTerminationBySIN(instanceNum) {
        const query = {
            serviceInstanceNumber: instanceNum,
            action: 'terminate',
            activity: 'termination_effective',
            processed: {
                $exists: false
            }
        };
        return new Bluebird((resolve, reject) => {
            db.scheduler.findOne(query, (err, result) => {
                if (err) {
                    reject(err);
                } else {
                    const milliSecondInOneDay = 86400000;
                    if (result && new Date().getTime() - result.start >= milliSecondInOneDay) {
                        resolve(null);
                    }else {
                        resolve(result);
                    }
                }
            });
        });
    }

    static generateTerminationDates(firstTermDate, numberOfMonths) {
        let earliesTermDate = TerminationManager.getFirstTerminationDate(firstTermDate);
        const termationDates = [{ date: earliesTermDate }];

        for (let i = 0; i < numberOfMonths; i += 1) {
            earliesTermDate = TerminationManager.getNextTermDate(earliesTermDate, 1);

            const terminationDateObj = {
                date: earliesTermDate,
            };
            termationDates.push(terminationDateObj);
        }

        return {
            eligibility: {
                value: true,
                reason: null,
                description: null,
                disclaimer: null,
            },
            termationDates,
        };
    }

    static getNextTermDate(date, months) { // get the last day of every month
        const nextMonthDate = dateTimeLib.addMonthToDate(date, months);
        const termDate = dateTimeLib.getLastDayOfMonth(nextMonthDate);

        return TerminationManager.setTerminationTime(termDate);
    }

    static getUnpaidBill(billAccNo) {
        const loadBillPromise = Bluebird.promisify(invoiceManager.loadBills);
        const options = {
            type: 'OPEN',
            billingAccountNumbers: [billAccNo],
        };
        return loadBillPromise(null, options)
            .then(({ totalCount }) => totalCount > 0);
    }

    static checkForEarlyTermination(billCycle, activationDate) {
        const { endDateUTC } = ec.nextBill(billCycle, undefined, activationDate);

        return new Date(dateTimeLib.getCurrentDate()) < new Date(endDateUTC);
    }

    static getEarlyTermResponse() {
        return {
            value: false,
            reason: 'EARLY_TERMINATION',
            description: 'Not eligible for termination due to: Number is less than 1 month',
            disclaimer: 'You must have at least 1 billing cycles',
        };
    }

    static getInvoicePendingResponse() {
        return {
            value: false,
            reason: 'PAYMENT_PENDING',
            description: 'Not eligible for termination due to Outstanding balance',
            disclaimer: 'The balance must be settled before you can terminate this number',
        };
    }

    static alreadyTerminatedResponse() {
        return {
            value: false,
            reason: 'ALREADY_SCHEDULED',
            description: 'Not eligible for termination due to:Termination is already scheduled for this number',
            disclaimer: 'Please contact our Happiness Experts at happiness@circles.life if you have any more questions.',
        };
    }

    static isEligibleForCurrMonth(earliesTermDate) {
        const dateWithTerminateTime = TerminationManager.getThresholdTime(earliesTermDate);

        return dateWithTerminateTime > new Date().toISOString();
    }

    static getFirstTerminationDate(earlyTermDate) {
        if (TerminationManager.isEligibleForCurrMonth(earlyTermDate)) {
            return TerminationManager.setTerminationTime(earlyTermDate);
        }

        return TerminationManager.getNextTermDate(earlyTermDate, 1);
    }

    static setTerminationTime(date) {
        const { hour, minute, second, millisecond } = terminationTimeStamp;
        const restrictedTime = {
            date,
            hour,
            minute,
            second,
            millisecond,
        };
        return dateTimeLib.generateDate(restrictedTime);
    }

    static getThresholdTime(date) {
        const { hour, minute, second, millisecond } = terminationTimeStamp;
        const restrictedTime = {
            date,
            hour: hour - 4,
            minute,
            second,
            millisecond,
        };
        return dateTimeLib.generateDate(restrictedTime);
    }

    static getTerminationScheduleForSpesificDate(start, prefix, number, serviceInstanceNumber, initiator){
        return {
            start: start,
            prefix: prefix,
            number: number,
            serviceInstanceNumber: serviceInstanceNumber,
            action: "terminate",
            activity: "termination_effective",
            allDay: false,
            trigger: initiator,
            parallelLimitOne: true,
            title: "Terminate " + common.escapeHtml(serviceInstanceNumber),
            createdDate: new Date().getTime()
        };
    }
}


module.exports = TerminationManager;
