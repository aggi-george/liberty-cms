var request = require('request');
var async = require('async');
var md5 = require('MD5');

var config = require('../../../../config');
var common = require('../../../../lib/common');
var db = require('../../../../lib/db_handler');
var ec = require('../../../../lib/elitecore');
var elitecoreUserService = require('../../../../lib/manager/ec/elitecoreUserService');
var elitecoreConnector = require('../../../../lib/manager/ec/elitecoreConnector');
var elitecoreBillService = require('../../../../lib/manager/ec/elitecoreBillService');
var elitecorePackagesService = require('../../../../lib/manager/ec/elitecorePackagesService');
var elitecoreUtils = require('../../../../lib/manager/ec/elitecoreUtils');
var ecommManager = require('../ecomm/ecommManager');
var profileManager = require('../profile/profileManager');
var invoiceManager = require('./invoiceManager');

var BaseError = require('../../errors/baseError');
var transactionRecoveryLockKeyPrefix = 'transaction_recovery_locked_';
var log = config.LOGSENABLED;

var supportedPaymentTypes = [
    'INVOICE_PAYMENT',
    'BOOST_PAYMENT',
    'PLUS_PAYMENT',
    'SUSPENSION_PAYMENT',
    'CREDIT_CAP_PAYMENT'
];

module.exports = {

    TYPE_INVOICE_PAYMENT: 'INVOICE_PAYMENT',
    TYPE_BOOST_PAYMENT: 'BOOST_PAYMENT',
    TYPE_PLUS_PAYMENT: 'PLUS_PAYMENT',
    TYPE_SUSPENSION_PAYMENT: 'SUSPENSION_PAYMENT',
    TYPE_CREDIT_CAP_PAYMENT: 'CREDIT_CAP_PAYMENT',

    DESCRIPTIONS: {
        INVOICE_PAYMENT: 'Payment for Invoice',
        BOOST_PAYMENT: 'Payment for Boost',
        PLUS_PAYMENT: 'Payment for Plus',
        SUSPENSION_PAYMENT: 'Payment for Suspension',
        CREDIT_CAP_PAYMENT: 'Payment for Credit Cap',
    },

    TR_STATUS_CREATED: 'TR_CREATED',
    TR_STATUS_FAILED: 'TR_FAILED',
    TR_STATUS_FINISHED: 'TR_FINISHED',
    TR_STATUS_PAYMENT_FINISHED: 'TR_PAYMENT_FINISHED',
    TR_STATUS_ACTIONS_FINISHED: 'TR_ACTIONS_EXECUTED',

    TR_ACTION_ADVANCE_PAYMENT: 'ADVANCE_PAYMENT',
    TR_ACTION_DEBIT_PAYMENT: 'DEBIT_PAYMENT',
    TR_ACTION_ADDON_SUBSCRIPTION: 'TR_ACTION_ADDON_SUBSCRIPTION',

    /**
     *
     *
     */
    loadTransactions: function (options, callback) {
        if (!callback) callback = () => {
        }
        if (!options) options = {};
        var startDate = new Date();

        var fields = 'id, uuid, billingAccountNumber, type, status, state, uniqueKey, ' +
            'externalTransactionId, internalId, amount, createdAt, completedAt, metadata';

        var where = '1=1 ' +
            (options.filter ? ' AND (' +
            ' uuid LIKE ' + db.escape('%' + options.filter + '%') + ' OR ' +
            ' billingAccountNumber LIKE ' + db.escape('%' + options.filter + '%') + ' OR ' +
            ' externalTransactionId LIKE ' + db.escape('%' + options.filter + '%') + ' OR ' +
            ' internalId LIKE ' + db.escape('%' + options.filter + '%') + ')' : '');

        if (options.type && options.type != 'ANY') {
            where += ' AND (type=' + db.escape(options.type) + ')';
        }
        if (options.status && options.status != 'ANY') {
            where += ' AND (status=' + db.escape(options.status) + ')';
        }
        if (options.state && options.state != 'ANY') {
            where += ' AND (state=' + db.escape(options.state) + ')';
        }
        if (options.id) {
            where += ' AND (id=' + db.escape(options.id) + ')';
        }

        if (options.monthDateLt && options.monthDateLt.getTime()) {
            where += ' AND billMonths.monthDate<' + db.escape(options.monthDateLt);
        } else if (options.monthDateGt && options.monthDateGt.getTime()) {
            where += ' AND billMonths.monthDate>=' + db.escape(options.monthDateLt);
        }

        if (options.completion && options.completion != 'ANY') {
            if (options.completion == 'UNFINISHED') {
                where += ' AND (uniqueKey IS NOT NULL AND createdAt<' + db.escape(new Date(new Date().getTime() - 60 * 1000)) + ')';
            } else if (options.completion == 'ACTIVE') {
                where += ' AND (uniqueKey IS NOT NULL AND createdAt>=' + db.escape(new Date(new Date().getTime() - 60 * 1000)) + ')';
            } else if (options.completion == 'FINISHED') {
                where += ' AND (uniqueKey IS NULL)';
            } else if (options.completion == 'RECOVERED') {
                where += ' AND (uniqueKey IS NULL && status LIKE \'RECOVERED%\')';
            }
        }

        if(options.startDate){
            where += ' AND createdAt > ' + db.escape(new Date(options.startDate));
        }

        if(options.endDate){
            where += ' AND createdAt < ' + db.escape(new Date(options.endDate));
        }

        if (options.billingAccountNumber) {
            where += ' AND (billingAccountNumber=' + db.escape(options.billingAccountNumber) + ')';
        }

        var q = 'SELECT ' + fields + ' FROM customerTransaction WHERE ' + where + ' ORDER BY id DESC ' +
            (options.limit > 0 ? ' LIMIT ' + db.escape(options.limit) : '') +
            (options.offset > 0 ? ' OFFSET ' + db.escape(options.offset) : '');

        db.query_err(q, function (err, rows) {
            if (err) {
                common.error('TransactionManager', 'loadTransactions: failed to load, error=' + err.message);
                return callback(err);
            }

            if (options.noLengthCheck) {
                return callback(undefined, {
                    time: (new Date().getTime() - startDate.getTime()),
                    totalCount: -1,
                    data: rows
                });
            }

            var selectCount = 'SELECT count(*) as count FROM customerTransaction WHERE ' + where;
            db.query_err(selectCount, function (err, countResult) {
                if (err) {
                    common.error('InvoiceManager', 'loadTransactions: failed to load count, error=' + err.message);
                    return callback(err);
                }

                var totalCount = countResult && countResult.length > 0
                    ? countResult[0].count : 0;

                callback(undefined, {
                    time: (new Date().getTime() - startDate.getTime()),
                    totalCount: totalCount,
                    data: rows
                });
            });
        });
    },

    /**
     *
     * Failure payment strategies: IGNORE, RETRY
     * Success payment strategies: VOID, COMPLETE
     * In Progress payment strategies: CONSIDER_AS_PAYMENT_SUCCESS, CONSIDER_AS_PAYMENT_FAILURE
     */
    recoverTransaction: function (id, paymentFailedStrategy, paymentSucceedStrategy, paymentInProgressStrategy, options, callback) {
        if (!callback) callback = () => {
        }
        if (!id || !paymentFailedStrategy || !paymentSucceedStrategy) {
            return callback(new BaseError('Invalid params', 'ERROR_INVALID_PARAMS'));
        }

        var lockKey = transactionRecoveryLockKeyPrefix + "_" + id;
        var info = {};

        Promise.resolve(info).then((info) => {
            return new Promise((resolve, reject) => {
                if (!options || options.overrideLock) {
                    return resolve(info);
                }

                db.lockOperation(lockKey, {}, 3 * 60 * 1000, function (err) {
                    if (err) {
                        common.error("TransactionManager", "recoverTransaction->lock: failed to lock, err=" + err.message);
                        return reject(new BaseError("Transactions recovery with id " + id + " is in progress", err.status));
                    }

                    resolve(info);
                });
            });
        }).then((info) => {
            return new Promise((resolve, reject) => {
                module.exports.loadTransactions({id: id}, (err, result) => {
                    if (err) {
                        return reject(err);
                    }
                    if (!result || !result.data || !result.data[0]) {
                        return reject(new BaseError('Transaction not found', 'ERROR_TRANSACTION_NOT_FOUND'));
                    }

                    var transaction = result.data[0];
                    if (transaction.metadata) {
                        transaction.metadata = common.safeParse(transaction.metadata);
                    } else {
                        transaction.metadata = {}
                    }

                    if (!transaction.uniqueKey) {
                        return reject(new BaseError('Transaction has been already recovered and is in correct state',
                            'ERROR_TRANSACTION_ALREADY_RECOVERED'));
                    }

                    info.transaction = transaction;
                    info.description = module.exports.DESCRIPTIONS[transaction.type];
                    resolve(info);
                });
            });
        }).then((info) => {
            return new Promise((resolve, reject) => {
                elitecoreUserService.loadUserInfoByBillingAccountNumber(info.transaction.billingAccountNumber, (err, cache) => {
                    if (err) {
                        common.error('TransactionManager', 'recoverTransaction->loadCustomer: error=' + err.message);
                        return reject(err);
                    }
                    if (!cache) {
                        common.error('TransactionManager', 'recoverTransaction->loadCustomer: not found');
                        return reject(new BaseError('Customer BAN ' + info.transaction.billingAccountNumber + ' not found',
                            'ERROR_CUSTOMER_NOT_FOUND'));
                    }
                    info.cache = cache;
                    resolve(info);
                })
            });
        }).then((info) => {
            return new Promise((resolve, reject) => {
                if (!info.transaction.state) {
                    // transaction has not started
                    info.paymentStatus = 'NONE';
                    return resolve(info);
                }

                var ban = info.transaction.billingAccountNumber;
                var uuid = info.transaction.uuid;
                var orderRef = info.transaction.externalTransactionId;
                ecommManager.checkStatus(ban, uuid, orderRef, (err, result) => {
                    if (err) {
                        return reject(err);
                    }
                    if (!result || !result.result || !result.result.response) {
                        return reject(new BaseError('Empty eComm API response', 'ERROR_ECOMM_EMPTY_RESPONSE'));
                    }

                    var response = result.result.response;
                    if (response.code == 1) {
                        if (paymentInProgressStrategy == 'CONSIDER_AS_PAYMENT_SUCCESS') {
                            info.paymentStatus = 'SUCCESS';
                            return resolve(info);
                        } else if (paymentInProgressStrategy == 'CONSIDER_AS_PAYMENT_FAILURE') {
                            info.paymentStatus = 'FAILED';
                            if (response.payment_status) {
                                info.paymentFailureReason = response.payment_status;
                            }
                            return resolve(info);
                        } else {
                            var orderRef = info.transaction.externalTransactionId;
                            var errorMessage = 'Payment is still processing... Please contant eCommerce team...' +
                                ' | External ID - ' + info.transaction.uuid +
                                ' | Order Ref - ' + (orderRef ? orderRef : 'EMPTY');
                            return reject(new BaseError(errorMessage, 'ERROR_PAYMENT_STILL_PROCESSING'));
                        }
                    } else if (response.code == -1) {
                        info.paymentStatus = 'FAILED';
                        if (response.payment_status) {
                            info.paymentFailureReason = response.payment_status;
                        }
                        return resolve(info);
                    } else if (response.code == 0) {
                        if (response.message == 'Payment was successfully cancelled.') { //need to add proper status
                            info.paymentStatus = 'CANCELED';
                        } else {
                            info.paymentStatus = 'SUCCESS';
                        }
                        return resolve(info);
                    } else {
                        return reject(new BaseError('Payment transaction code is not supported, ' +
                        'code=' + response.code, 'ERROR_UNSUPPORTED_STATUS'));
                    }
                });
            });
        }).then((info) => {
            return new Promise((resolve, reject) => {
                if ((info.paymentStatus == 'NONE' || info.paymentStatus == 'FAILED' || info.paymentStatus == 'CANCELED')
                    && module.exports.TYPE_INVOICE_PAYMENT == info.transaction.type) {

                    var invoiceId = info.transaction.internalId;
                    var paymentStatus = (info.paymentFailureReason ? info.paymentFailureReason : 'PF_OTHER');
                    var logId = (info.transaction.metadata ? info.transaction.metadata.logId : undefined);

                    unblockInvoice(invoiceId, paymentStatus, logId, undefined, info, (err) => {
                        if (err) {
                            reject(err);
                        }
                        return resolve(info);
                    });
                } else {
                    return resolve(info);
                }
            });
        }).then((info) => {
            return new Promise((resolve, reject) => {

                var ban = info.transaction.billingAccountNumber;
                var uuid = info.transaction.uuid;
                var orderRef = info.transaction.externalTransactionId;

                if (info.paymentStatus == 'CANCELED') {

                    // payment has already been canceled

                    finishTransaction(info.transaction.id, 'RECOVERED_VOIDED', (updateErr) => {
                        if (updateErr) {
                            common.error('TransactionManager', 'recoverTransaction->recoverTransaction(VP): ' +
                            'error=' + updateErr.message);
                            return reject(updateErr);
                        }

                        return resolve(info);
                    });
                } else if (info.paymentStatus == 'NONE' || info.paymentStatus == 'FAILED') {

                    // transaction failure strategies

                    if (paymentFailedStrategy == 'IGNORE') {
                        finishTransaction(info.transaction.id, 'RECOVERED_IGNORED', (updateErr) => {
                            if (updateErr) {
                                common.error('TransactionManager', 'recoverTransaction->recoverTransaction(ST): ' +
                                'error=' + updateErr.message);
                                return reject(updateErr);
                            }

                            return resolve(info);
                        });
                    } else if (paymentFailedStrategy == 'RETRY') {
                        if (info.transaction.type == module.exports.TYPE_INVOICE_PAYMENT) {
                            return reject(new BaseError('Failed invoice payments can NOT be re-triggered from transaction screen, ' +
                                'please run proper bach payment for such invoices', 'ERROR_UNSUPPORTED_FAILURE_STRATEGY'));
                        }

                        finishTransaction(info.transaction.id, 'RECOVERED_RETRY', (updateErr) => {
                            if (updateErr) {
                                common.error('TransactionManager', 'recoverTransaction->recoverTransaction(VP): ' +
                                'error=' + updateErr.message);
                                return reject(updateErr);
                            }

                            module.exports.performPayment(info.transaction.billingAccountNumber, {
                                type: info.transaction.type,
                                amount: info.transaction.amount / 100, // db contains value in cents, function takes value in $
                                internalId: info.transaction.internalId,
                                metadata: info.transaction.metadata
                            }, (status, data, callback) => {
                                // do nothing
                                return callback();
                            }, (err, result) => {
                                if (err) {
                                    // no need to return the error, it a part
                                    // of new transaction, not current one
                                }

                                info.retryResult = result;
                                return resolve(info);
                            });
                        });
                    } else {
                        return reject(new BaseError('Unsupported payment failure strategy ' + paymentFailedStrategy,
                            'ERROR_UNSUPPORTED_FAILURE_STRATEGY'));
                    }
                } else if (info.paymentStatus == 'SUCCESS') {

                    // transaction success strategies

                    if (paymentSucceedStrategy == 'VOID') {
                        if ((info.transaction.type == module.exports.TYPE_BOOST_PAYMENT
                            || info.transaction.type == module.exports.TYPE_PLUS_PAYMENT
                            || info.transaction.type == module.exports.SUSPENSION_PAYMENT
                            || info.transaction.type == module.exports.TYPE_CREDIT_CAP_PAYMENT)
                            && info.transaction.state == "PR_TR_ACTION_ADDON_SUBSCRIPTION") {
                            return reject(new BaseError('Payment can not be voided, transaction has already ' +
                            'been registered on BSS', 'ERROR_VOID_UNSUPPORTED'));
                        }

                        ecommManager.cancelPayment(ban, uuid, orderRef, (err, result) => {
                            if (err) {
                                return reject(err);
                            }
                            if (!result || !result.result || !result.result.response) {
                                return reject(new BaseError('Empty eComm API response', 'ERROR_ECOMM_EMPTY_RESPONSE'));
                            }

                            var response = result.result.response;
                            info.cancelResponse = response;

                            finishTransaction(info.transaction.id, 'RECOVERED_VOIDED', (updateErr) => {
                                if (updateErr) {
                                    common.error('TransactionManager', 'recoverTransaction->recoverTransaction(VP): ' +
                                    'error=' + updateErr.message);
                                    return reject(updateErr);
                                }

                                return resolve(info);
                            });
                        });
                    } else if (paymentSucceedStrategy == 'COMPLETE') {
                        if (!info.transaction.metadata || !info.transaction.metadata.actions
                            || !info.transaction.metadata.actions[0]) {
                            return resolve(info);
                        }
                        if (options.skipActions) {
                            common.error('TransactionManager', 'recoverTransaction->recoverTransaction(COMPLETE): skipped actions');
                            finishTransaction(info.transaction.id, 'RECOVERED_COMPLETED', (updateErr) => {
                                if (updateErr) {
                                    common.error('TransactionManager', 'recoverTransaction->recoverTransaction(VP): ' +
                                    'error=' + updateErr.message);
                                    return reject(updateErr);
                                }

                                return resolve(info);
                            });
                            return;
                        }

                        // we need skip all actions before the one transaction is stuck in,
                        // those actions should have already been processed

                        // example: 'PR_PAYMENT' -> 'PR_ACTION_NAME_1' -> 'PR_ACTION_NAME_2' -> 'PR_ACTION_NAME_3' -> NULL
                        // if stuck in PR_PAYMENT we need to re-run PR_ACTION_NAME_1 + PR_ACTION_NAME_2 + PR_ACTION_NAME_3
                        // if stuck in PR_ACTION_NAME_1 we need to re-run PR_ACTION_NAME_1 + PR_ACTION_NAME_2 + PR_ACTION_NAME_3
                        // if stuck in PR_ACTION_NAME_2 we need to re-run PR_ACTION_NAME_2 + PR_ACTION_NAME_3
                        // if stuck in PR_ACTION_NAME_3 we need to re-run PR_ACTION_NAME_3

                        var stuckInState = info.transaction.state;
                        var stuckInPaymentState  = (stuckInState == 'PR_PAYMENT');
                        var stuckActionFound = false;
                        var actionsToReRun = [];
                        var processedActions = [];
                        info.transaction.metadata.actions.forEach((action) => {
                            var actionState = 'PR_' + action.type;
                            if (stuckInPaymentState || stuckActionFound || stuckInState == actionState) {
                                actionsToReRun.push(action);
                                stuckActionFound = true;
                            } else {
                                processedActions.push(action);
                            }
                        });

                        var startActionsHandling = new Date();
                        async.mapSeries(actionsToReRun, (action, callback) => {
                            processAction(info.cache, info.transaction.id, info.transaction.amount, info.description, action, callback);
                        }, (err, result) => {
                            if (err) {
                                common.error('TransactionManager', 'recoverTransaction->actionsExecution(TA): ' +
                                'error=' + err.message);
                                return reject(err);
                            }

                            var executionTime = new Date() - startActionsHandling;
                            if (log) common.log('TransactionManager', 'recoverTransaction->actionsExecution(VP): actions count=' +
                            actionsToReRun.length + ', executionTime=' + executionTime + 'ms');

                            info.transaction.processedActions = processedActions;
                            info.transaction.actionsToReRun = actionsToReRun;
                            info.transaction.actionsToReRunResult = result;

                            finishTransaction(info.transaction.id, 'RECOVERED_COMPLETED', (updateErr) => {
                                if (updateErr) {
                                    common.error('TransactionManager', 'recoverTransaction->recoverTransaction(VP): ' +
                                    'error=' + updateErr.message);
                                    return reject(updateErr);
                                }

                                return resolve(info);
                            });
                        });
                    } else {
                        return reject(new BaseError('Unsupported payment success strategy ' + paymentSucceedStrategy,
                            'ERROR_UNSUPPORTED_SUCCESS_STRATEGY'));
                    }
                } else {
                    return reject(new BaseError('Invalid payment status ' + info.paymentStatus, 'ERROR_INVALID_PAYMENT_STATUS'));
                }
            });
        }).then((info) => {
            return new Promise((resolve, reject) => {
                if (info.paymentStatus == 'SUCCESS'
                    && module.exports.TYPE_INVOICE_PAYMENT == info.transaction.type) {

                    var invoiceId = info.transaction.internalId;
                    var logId = (info.transaction.metadata ? info.transaction.metadata.logId : undefined);
                    var paymentStatus = paymentSucceedStrategy == 'VOID' ? 'VOIDED' : 'OK';

                    // in case if payment has been successfully done and payment
                    // has been registered we have to update invoice status
                    var leftAmount = (paymentSucceedStrategy == 'COMPLETE' ? info.transaction.metadata.outsAPCents : undefined);

                    unblockInvoice(invoiceId, paymentStatus, logId, leftAmount, info, (err) => {
                        if (err) {
                            reject(err);
                        }
                        return resolve(info);
                    });
                } else {
                    return resolve(info);
                }
            });
        }).then((info) => {
            db.unlockOperation(lockKey);
            callback(undefined, info);
        }).catch((err) => {
            db.unlockOperation(lockKey);
            callback(err);
        });
    },

    /**
     *
     *
     */
    performPayment: (billingAccountNumber, options, transactionProgress, callback) => {
        if (!callback) callback = () => {
        }
        if (!transactionProgress) transactionProgress = (status, data, callback) => {
            callback();
        }

        if (!options) options = {};

        if (!billingAccountNumber || !options.type || !options.amount) {
            return callback(new BaseError('Invalid params', 'ERROR_INVALID_PARAMS'));
        }
        if (supportedPaymentTypes.indexOf(options.type) == -1) {
            return callback(new BaseError('Payment type is not supported (' + options.type + ')', 'ERROR_INVALID_TYPE'));
        }
        var info = {transaction: {}, description: module.exports.DESCRIPTIONS[options.type]};

        Promise.resolve(info).then((info) => {
            return new Promise((resolve, reject) => {
                elitecoreUserService.loadUserInfoByBillingAccountNumber(billingAccountNumber, function (err, cache) {
                    if (err) {
                        common.error('TransactionManager', 'performInvoicePayment->loadCustomer: error=' + err.message);
                        return reject(err);
                    }
                    if (!cache) {
                        common.error('TransactionManager', 'performInvoicePayment->loadCustomer: not found');
                        return reject(new BaseError('Customer BAN ' + billingAccountNumber + ' not found',
                            'ERROR_CUSTOMER_NOT_FOUND'));
                    }
                    info.cache = cache;
                    resolve(info);
                })
            });
        }).then((info) => {
            return new Promise((resolve, reject) => {
                profileManager.loadProfileDetailsBySI(info.cache.serviceInstanceNumber, function (err, profile) {
                    if (err) {
                        common.error('TransactionManager', 'performInvoicePayment->loadProfile: error=' + err.message);
                        return reject(err);
                    }
                    if (!profile) {
                        common.error('TransactionManager', 'performInvoicePayment->loadProfile: not found');
                        return reject(new BaseError('Customer profile SIN ' + info.cache.serviceInstanceNumber + ' not found',
                            'ERROR_PROFILE_NOT_FOUND'));
                    }
                    info.profile = profile;
                    resolve(info);
                })
            });
        }).then((info) => {
            return new Promise((resolve, reject) => {
                var uuid = md5(Math.random() + billingAccountNumber + new Date().getTime());
                var profileId = info.profile.profileId;

                createTransaction(uuid, profileId, billingAccountNumber, options, (err, insertId) => {
                    if (err) {
                        common.error('TransactionManager', 'performInvoicePayment->createTransaction: error=' + err.message);
                        return reject(err);
                    }

                    info.transaction = {
                        uuid: uuid,
                        id: insertId
                    }

                    transactionProgress(module.exports.TR_STATUS_CREATED, info.transaction, (err) => {
                        if (err) {
                            common.error('TransactionManager', 'performInvoicePayment->transactionProgress(TC): error=' + err.message);
                            return reject(err);
                        }
                        resolve(info);
                    });
                });
            });
        }).then((info) => {
            return new Promise((resolve, reject) => {
                var customerAccountNumber = info.cache.customerAccountNumber;
                var billingAccountNumber = info.cache.billingAccountNumber;
                var uuid = info.transaction.uuid;

                updateStateStatus(info.transaction.id, 'PR_PAYMENT', 'IN_PROGRESS', (err) => {
                    if (err) {
                        common.error('TransactionManager', 'performInvoicePayment->updateStateStatus: error=' + err.message);
                        return reject(err);
                    }

                    ecommManager.executePayment(customerAccountNumber, billingAccountNumber, options.amount, uuid, (err, result) => {
                        var paymentError = err;
                        var supportedErrors = module.exports.getSupportedECommErrorStatuses();
                        info.transaction.paymentResult = result;
                        info.transaction.paymentStatus = err ? (err.status ? err.status : 'ERROR') : 'OK';

                        if (result && result.result) {
                            var eCommPaymentResponse = result.result.response ? result.result.response : result.result;
                            info.transaction.eCommOrderRef = eCommPaymentResponse ? eCommPaymentResponse.order_ref : undefined;
                        }

                        if (err && supportedErrors.indexOf(err.status) == -1) {
                            common.error('TransactionManager', 'performInvoicePayment->transactionProgress(TPF): ' +
                            'transactionStatus=' + err.status);

                            if (!info.transaction.eCommOrderRef) {
                                return reject(err);
                            }

                            // sometimes transaction fails due to some internal error on status
                            // check or because it takes too much time (>90 sec), in such cases order ref. no.
                            // can be still presented and can be used

                            return updateExternalId(info.transaction.id, info.transaction.eCommOrderRef, (updateExtIdErr) => {
                                if (updateExtIdErr) {
                                    common.error('TransactionManager', 'performInvoicePayment->transactionProgress(TORU): ' +
                                    'error=' + updateExtIdErr.message);
                                    return reject(updateExtIdErr);
                                }

                                return reject(err);
                            });
                        } else {
                            updateExternalId(info.transaction.id, info.transaction.eCommOrderRef, (err) => {
                                if (err) {
                                    common.error('TransactionManager', 'performInvoicePayment->transactionProgress(TORU): ' +
                                    'error=' + err.message);
                                    return reject(err);
                                }

                                transactionProgress(module.exports.TR_STATUS_PAYMENT_FINISHED, info.transaction, (err) => {
                                    if (err) {
                                        common.error('TransactionManager', 'performInvoicePayment->transactionProgress(TPF): ' +
                                        'error=' + err.message);
                                        return reject(err);
                                    }
                                    resolve(info);
                                });
                            });
                        }
                    });
                });
            });
        }).then((info) => {
            return new Promise((resolve, reject) => {
                if (info.transaction.paymentStatus != 'OK') {
                    return resolve(info);
                }
                if (!options.metadata || !options.metadata.actions || !options.metadata.actions[0]) {
                    return resolve(info);
                }

                var startActionsHandling = new Date();
                async.mapSeries(options.metadata.actions, (action, callback) => {
                    processAction(info.cache, info.transaction.id, options.amount * 100, info.description, action, callback);
                }, (err, result) => {
                    if (err) {
                        common.error('TransactionManager', 'performInvoicePayment->actionsExecution(TA): billingAccountNumber=' +
                        billingAccountNumber + ', error=' + err.message);
                        return reject(err);
                    }

                    var executionTime = new Date() - startActionsHandling;
                    if (log) common.log('TransactionManager', 'performInvoicePayment->actionsExecution(VP): actions count=' +
                    options.metadata.actions.length + ', executionTime=' + executionTime + 'ms');

                    info.transaction.actionsResult = result;
                    transactionProgress(module.exports.TR_STATUS_ACTIONS_FINISHED, info.transaction, (err) => {
                        if (err) {
                            common.error('TransactionManager', 'performInvoicePayment->transactionProgress(TA): ' +
                            'error=' + err.message);
                            return reject(err);
                        }
                        resolve(info);
                    });
                });
            });
        }).then((info) => {
            info.transaction.transactionStatus = 'OK';

            var finalStatus = 'OK';
            var validPaymentError;
            if (info.transaction.paymentStatus != 'OK') {
                validPaymentError = new BaseError("Payment failed with status " +
                info.transaction.paymentStatus, info.transaction.paymentStatus)
                finalStatus = info.transaction.paymentStatus;
            }

            finishTransaction(info.transaction.id, finalStatus, (updateErr) => {
                if (updateErr) {
                    common.error('TransactionManager', 'performInvoicePayment->finishTransaction(FT): error=' + updateErr.message);
                    return callback(updateErr);
                }

                transactionProgress(module.exports.TR_STATUS_FINISHED, info.transaction, (progressErr) => {
                    if (progressErr) {
                        common.error('TransactionManager', 'performInvoicePayment->finishTransaction(TPU): error=' + progressErr.message);
                        return callback(progressErr);
                    }

                    callback(undefined, {
                        validPaymentError: validPaymentError,
                        transaction: info.transaction
                    });
                });
            });
        }).catch((err) => {
            var status = err.status ? err.status : 'ERROR';
            info.transaction.transactionStatus = status;

            updateStateStatus(info.transaction.id, undefined, status, (updateErr) => {
                if (updateErr) {
                    common.error('TransactionManager', 'performInvoicePayment->catch(TSU): error=' + updateErr.message);
                    return callback(updateErr);
                }

                transactionProgress(module.exports.TR_STATUS_FAILED, info.transaction, (progressErr) => {
                    if (progressErr) {
                        common.error('TransactionManager', 'performInvoicePayment->catch(TPU): error=' + progressErr.message);
                        return callback(progressErr);
                    }

                    return callback(err);
                });
            });
        });
    },

    /**
     *
     */
    createCloseInvoiceAction: (invoiceId) => {
        return {
            type: module.exports.TR_ACTION_DEBIT_PAYMENT,
            invoiceId: invoiceId
        }
    },

    /**
     *
     */
    createAdvancedPaymentAction: () => {
        return {
            type: module.exports.TR_ACTION_ADVANCE_PAYMENT
        }
    },

    /**
     *
     */
    createAddonSubAction: (description, addonId, recurrent, effect, waitForResponse) => {
        return {
            type: module.exports.TR_ACTION_ADDON_SUBSCRIPTION,
            description: description,
            addonId: addonId,
            recurrent: recurrent,
            effect: effect,
            waitForResponse: waitForResponse
        }
    },

    /**
     *
     */
    getSupportedECommErrorStatuses: () => {
        return ['PF_NOT_HONORED',
            'PF_LOST', 'PF_STOLEN',
            'PF_INSUFFICIENT_FUND',
            'PF_INVALID_CARD_INFORMATION',
            'PF_CARD_EXPIRED',
            'PF_OTHER'];
    }
}

function createTransaction(uuid, profileId, billingAccountNumber, options, callback) {
    var query = 'INSERT INTO customerTransaction (uuid, profileId, billingAccountNumber, ' +
        'status, uniqueKey, type, internalId, metadata, amount, createdAt) VALUES (' +
        db.escape(uuid) + ',' +
        db.escape(profileId) + ',' +
        db.escape(billingAccountNumber) + ',' +
        db.escape('IN_PROGRESS') + ',' +
        db.escape('UNFINISHED') + ',' +
        db.escape(options.type) + ',' +
        (options.internalId ? db.escape(options.internalId) : 'NULL') + ',' +
        (options.metadata ? db.escape(JSON.stringify(options.metadata)) : 'NULL') + ',' +
        db.escape(options.amount >= 0 ? options.amount * 100 : 0) + ',' +
        db.escape(new Date()) + ')';

    db.query_err(query, function (err, result) {
        if (err) {
            common.error('TransactionManager', 'createTransaction: error=' + err.message);
            if (err.code == 'ER_DUP_ENTRY') {
                return callback(new BaseError('Found unfinished transaction of ' +
                'the same type (' + options.type + ')', 'ERROR_UNFINISHED_TR'));
            } else {
                return callback(err);
            }
        }

        callback(undefined, result.insertId);
    });
}

function updateStateStatus(id, state, status, callback, noRetry) {
    var query = 'UPDATE customerTransaction SET ' + (state ? 'state=' + db.escape(state) + ',' : '') +
        ' status=' + db.escape(status) + ' WHERE id=' + db.escape(id);
    db.query_err(query, function (err, result) {
        if (err) {
            if (err.message && err.message.indexOf('ER_LOCK_DEADLOCK') >= 0 && !noRetry) {

                // in some very rare cases transaction might be blocked, we need to wait and retry again
                // since that is a very important to update status, sending of a correct notification depends on that

                common.error('TransactionManager', 'updateStateStatus: ER_LOCK_DEADLOCK, re-trying...');
                return setTimeout(() => {
                    updateStateStatus(id, state, status, callback, true);
                }, 1500);
            } else {
                common.error('TransactionManager', 'updateStateStatus: error=' + err.message);
                return callback(err);
            }
        }
        callback();
    });
}

function updateExternalId(id, externalTransactionId, callback, noRetry) {
    var query = 'UPDATE customerTransaction SET externalTransactionId=' +
        db.escape(externalTransactionId) + ' WHERE id=' + db.escape(id);
    db.query_err(query, function (err, result) {
        if (err) {
            if (err.message && err.message.indexOf('ER_LOCK_DEADLOCK') >= 0 && !noRetry) {

                // in some very rare cases transaction might be blocked, we need to wait and retry again
                // since that is a very important to update status, sending of a correct notification depends on that

                common.error('TransactionManager', 'updateExternalId: ER_LOCK_DEADLOCK, re-trying...');
                return setTimeout(() => {
                    updateExternalId(id, externalTransactionId, callback, true);
                }, 1500);
            } else {
                common.error('TransactionManager', 'updateExternalId: error=' + err.message);
                return callback(err);
            }
        }
        callback();
    });
}

function finishTransaction(id, status, callback, noRetry) {
    var query = 'UPDATE customerTransaction SET state=NULL, uniqueKey=NULL, ' +
        'status=' + db.escape(status) + ', completedAt=' + db.escape(new Date()) + ' WHERE id=' + db.escape(id);
    db.query_err(query, function (err, result) {
        if (err) {
            if (err.message && err.message.indexOf('ER_LOCK_DEADLOCK') >= 0 && !noRetry) {

                // in some very rare cases transaction might be blocked, we need to wait and retry again
                // since that is a very important to update status, sending of a correct notification depends on that

                common.error('TransactionManager', 'finishTransaction: ER_LOCK_DEADLOCK, re-trying...');
                return setTimeout(() => {
                    finishTransaction(id, status, callback, true);
                }, 1500);
            } else {
                common.error('TransactionManager', 'finishTransaction: error=' + err.message);
                return callback(err);
            }
        }
        callback(err);
    });
}

function processAction(cache, transactionId, amountCents, description, action, callback) {
    updateStateStatus(transactionId, 'PR_' + action.type, 'IN_PROGRESS', (err) => {
        if (err) {
            common.error('TransactionManager', 'performInvoicePayment->updateStateStatus(TA): error=' + err.message);
            return callback(err);
        }

        if (module.exports.TR_ACTION_DEBIT_PAYMENT == action.type) {
            return processInvoicePaymentAction(cache, amountCents, action, callback);
        } else if (module.exports.TR_ACTION_ADVANCE_PAYMENT == action.type) {
            return processAdvancedPaymentAction(cache, amountCents, description, action, callback);
        } else if (module.exports.TR_ACTION_ADDON_SUBSCRIPTION == action.type) {
            return processAddonSubscriptionAction(cache, action, callback);
        } else {
            return callback(new BaseError('Unsupported post-action type (' + action.type + ')',
                'ERROR_ACTION_NOT_SUPPORTED'));
        }
    });
}

function processAdvancedPaymentAction(cache, amountCents, description, action, callback) {
    if (!callback) callback = () => {
    }
    if (!action) action = {};

    if (!cache) {
        common.error('TransactionManager', 'performInvoicePayment->processAdvancedPaymentAction: invalid params');
        return callback(new BaseError('Invalid params', 'ERROR_INVALID_PARAMS'));
    }

    elitecoreBillService.clearCustomerPaymentsCache(cache.billingAccountNumber);
    elitecoreConnector.getPromise(elitecoreConnector.makeAdvancePaymentDataRequest, {
        accountNumber: cache.billingAccountNumber,
        transactionAmount: amountCents,
        documentNumber: action.invoiceId,
        description: description,
        paymentDate: new Date().toISOString().split('T')[0],
        channelAlias: 'BillManager',
        validResponses: [
            {keys: ['receivableAccountData', '$array0']}
        ]
    }).then((result) => {
        callback(undefined, result);
    }).catch((err) => {
        common.error('TransactionManager', 'performInvoicePayment->processAdvancedPaymentAction: error=' + err.message);
        callback(err);
    });
}

function processInvoicePaymentAction(cache, amountCents, action, callback) {
    if (!callback) callback = () => {
    }
    if (!action) action = {};

    if (!cache) {
        common.error('TransactionManager', 'performInvoicePayment->processInvoicePaymentAction: invalid params');
        return callback(new BaseError('Invalid params', 'ERROR_INVALID_PARAMS'));
    }

    elitecoreBillService.clearCustomerPaymentsCache(cache.billingAccountNumber);
    elitecoreConnector.getPromise(elitecoreConnector.makeDebitPaymentDataRequest, {
        accountNumber: cache.billingAccountNumber,
        transactionAmount: amountCents,
        documentNumber: action.invoiceId,
        paymentDate: new Date().toISOString().split('T')[0],
        channelAlias: 'BillManager',
        validResponses: [
            {keys: ['receivableAccountData', '$array0']}
        ]
    }).then((result) => {
        callback(undefined, result);
    }).catch((err) => {
        common.error('TransactionManager', 'performInvoicePayment->processInvoicePaymentAction: error=' + err.message);
        callback(err);
    });
}

function processAddonSubscriptionAction(cache, action, callback) {
    if (!callback) callback = () => {
    }
    if (!action) action = {};

    if (!cache || !action || !action.addonId) {
        common.error('TransactionManager', 'performInvoicePayment->processAddonSubscriptionAction: invalid params');
        return callback(new BaseError('Invalid params', 'ERROR_INVALID_PARAMS'));
    }

    elitecorePackagesService.loadHierarchy((err, hierarchy) => {
        var addon = elitecoreUtils.findPackageById(hierarchy, action.addonId);
        if (!addon) {
            return callback(new BaseError('Invalid addon id ' + action.addonId, 'ERROR_INVALID_ADDON_ID'));
        }

        var callbackExecuted = false;
        ec.addonSubscribe(cache.number, cache.serviceInstanceNumber, [{
            product_id: action.addonId,
            recurrent: action.recurrent ? action.recurrent : false,
            effect: action.effect > 0 ? action.effect : 0
        }], cache.billingCycleStartDay, (err, result) => {
            if (callbackExecuted) {
                if (err) {
                    common.error('TransactionManager', 'performInvoicePayment->processAddonSubscriptionAction: ' +
                    'error=' + err.message);
                } else {
                    if (log) common.log('TransactionManager', 'performInvoicePayment->processAddonSubscriptionAction: ' +
                    'finished after notification received');
                }
                return;
            }

            if (err) {
                common.error('TransactionManager', 'performInvoicePayment->processAddonSubscriptionAction: error=' + err.message);
                if (checkNotifications) clearInterval(checkNotifications);
                return callback(err);
            }

            elitecoreUserService.clearUserInfo(cache.customerAccountNumber);
            if (checkNotifications) clearInterval(checkNotifications);

            callback(false, {added: true, result: result});
        });

        var checkNotifications;
        if (action.waitForResponse) {
            var attempt = 0;
            checkNotifications = setInterval(() => {
                db.cache_get('last_subscription_cache', cache.number, function (addonname) {
                    if (attempt % 10 == 0) {
                        if (log) common.log('TransactionManager', 'processAddonSubscriptionAction: checking notifications... ' +
                        ', number=' + cache.number + ', billingAccountNumber=' + cache.billingAccountNumber + ', name=' + addon.name);
                    }

                    attempt++;
                    if (addonname == addon.name) {
                        if (log) common.log('TransactionManager', 'processAddonSubscriptionAction: found notification with the same name, ' +
                        ', number=' + cache.number + ', billingAccountNumber=' + cache.billingAccountNumber + ', name=' + addon.name);

                        callbackExecuted = true;
                        elitecoreUserService.clearUserInfo(cache.customerAccountNumber);
                        clearInterval(checkNotifications);

                        callback(undefined, {added: true});
                    }
                });
            }, 500);
        }
    });
}

function unblockInvoice(invoiceId, paymentStatus, logId, leftAmount, info, callback) {
    invoiceManager.updateBillPaymentStatus(invoiceId, {
        paymentStatus: paymentStatus,
        batchStatus: 'OK',
        leftAmount: leftAmount
    }, {retryIfBlocked: true}, (err) => {
        if (err) {
            common.error('TransactionManager', 'unblockInvoice: error=' + err.message);
            return callback(err);
        }

        if (!logId) {
            return callback(undefined, info);
        }

        var update = {recovery: {}};
        update.recovery[invoiceId] = info;
        db.notifications_pay_now.update({_id: db.objectID(logId)}, {$set: update}, {multi: false}, (err) => {
            if (err) {
                common.error('TransactionManager', 'unblockInvoice: error=' + err.message);
                return callback(err);
            }

            return callback(undefined, info);
        });
    });
}