var dateformat = require('dateformat');

const config = require(`${global.__base}/config`);
const common = require(`${global.__lib}/common`);
const db = require(`${global.__lib}/db_handler`);

var elitecoreUserService = require('../../../../lib/manager/ec/elitecoreUserService');
var elitecoreUsageService = require('../../../../lib/manager/ec/elitecoreUsageService');

var LOG = config.EC_LOGSENABLED;

const caamSchema = (config.DEV) ? 'crestelcaamtestlwp' : 'crestelcaamlwp';
const ocsSchema = (config.DEV) ? 'crestelocstestlwp' : 'crestelocslwp';
const pmSchema = (config.EC_STAGING) ? 'crestelpmtestlwp' : 'crestelpmlwp';

/**
 *  MB   =>  KB    => Discrepancy KB
 * 50MB  => 51200  => 1228.8 KB
 * 100MB => 102400 => 2457.6 KB
 * 150MB => 153600 => 3686.4 KB
 * 200BM => 204800 => 4915.2 KB
 * 250BM => 256000 => 6144 KB
 * 300BM => 307200 => 7372.8 KB
 * 400BM => 409600 => 9830.4 KB
 * 500BM => 512000 => 12288 KB
 * 600BM => 614400 => 14745.6 KB
 * 700BM => 716800 => 17203.2 KB
 * 750BM => 768000 => 18432 KB
 * 800BM => 819200 => 19660.8 KB
 * 900BM => 921600 => 22118.4 KB
 */

var DISCREPANCY = {
    51200: 1228.8,
    102400: 2457.6,
    153600: 3686.4,
    204800: 4915.2,
    256000: 6144,
    307200: 7372.8,
    409600: 9830.4,
    512000: 12288,
    614400: 14745.6,
    716800: 17203.2,
    768000: 18432,
    819200: 19660.8,
    921600: 22118.4
};

module.exports = {

    /**
     *
     */
    computeUsage: function (prefix, number, callback) {
        if (!callback) callback = () => {
        }

        elitecoreUserService.loadUserInfoByPhoneNumber(number, function (err, cache) {
            if (err) {
                return callback(err);
            }
            if (!cache) {
                return callback(new Error("Customer data not found, number=" + number));
            }

            elitecoreUsageService.loadCurrentCustomerUsage(cache.serviceInstanceNumber, (err, viewResponse) => {
                if (err) {
                    return callback(err);
                }

                var balanceData = viewResponse.balanceData;
                var counterBalanceData = viewResponse.counterBalanceData;

                createDataViewContainer(cache, balanceData, (err, dataResult) => {
                    if (err) {
                        return callback(err);
                    }

                    if (dataResult) {
                        dataResult.type = "data";
                    }

                    createCountersViewContainer(cache, balanceData, counterBalanceData, (err, otherResult) => {
                        if (err) {
                            return callback(err);
                        }

                        if (otherResult) {
                            otherResult.type = "other";
                        }

                        callback(undefined, {
                            data: dataResult,
                            other: otherResult
                        });
                    });
                });
            });
        });
    },


    /**
     * to get the usage of customer in last two months in MB
     * @param serviceInstantNumber
     * @param utilizationInMB - ex : 500
     * @param callback
     */
    isLowUsageUtilization: function (serviceInstantNumber,utilizationInMB, callback) {
        let query = `select /*+parallel(a 16)*/ sum(sessiondatatransfer)/1024/1024 MBUsage from ${ocsSchema}.tblcdrpostpaid a, ${ocsSchema}.ab_hierarchy b where trunc(processdate) between add_months(trunc(sysdate,'mm'),-3) and last_day(add_months(trunc(sysdate,'mm'),-1)) and packageid !=440 and A.DEBITEDACCOUNTID=B.SI_ID and B.SI_NUMBER='${serviceInstantNumber}'`;
        db.oracle.query(query, function (err, result) {
                if (err) {
                    if (LOG) common.error("usageManager", "getUsageInLastTwoMonth: can not load");
                    return callback(err);
                }else {
                    let found = false;
                    if (result) {
                        if (result.rows) {
                            let rows = result.rows;
                            if (rows[0]) {
                                rows[0][0] = rows[0][0] || 0;
                                if (rows[0][0] <= utilizationInMB) {
                                    found = true;
                                }
                            }
                        }
                    }
                    return callback(undefined, found);
                }
            }
        );
    }
}

function prettifyDataKb(kb) {
    if (kb >= 1024 * 1024) {
        return {value: Math.round((kb / 1024 / 1024) * 10) / 10, unit: "GB"};
    } else if (kb >= 1024) {
        return {value: Math.round(kb / 1024), unit: "MB"};
    } else {
        return {value: 0, unit: "MB"};
    }
}

function computeStaticData(balanceItem, dataAddon, canBeProrated, canAccumulate, dataResult) {

    var leftKb = parseFloat(balanceItem.unusedValue) / 1024;
    var usedKb = parseFloat(balanceItem.usedValue) / 1024;

    if (canBeProrated) {
        var prorateKb = leftKb + usedKb;
        var prorateRemoved = dataAddon.kb - prorateKb;
        dataResult.plan_kb = dataAddon.kb;

        if (prorateRemoved > 10240) {
            dataResult.prorated = true;
            dataResult.prorated_kb = prorateKb;
            usedKb += prorateRemoved;
        }
    }

    if (leftKb < 0) {
        usedKb = usedKb + leftKb;
        leftKb = 0;
    }

    if (dataAddon && DISCREPANCY[dataAddon.kb]) {
        var discrepancyKb = DISCREPANCY[dataAddon.kb];
        var corrector = (1 + discrepancyKb / dataAddon.kb);

        usedKb = parseInt(usedKb * corrector);
        leftKb = parseInt(leftKb * corrector);
    }

    if (canAccumulate) {
        dataResult.left += leftKb;
        dataResult.used += usedKb;
    } else {
        dataResult.left = leftKb;
        dataResult.used = usedKb;
    }
}

function createDataViewContainer(cache, balanceData, callback) {
    var now = new Date();

    var unit = "kilobytes";
    var boostSectionKb = 1024 * 1024;

    var data = {
        basic: {section: "DATA", used: 0, left: 0, prorated: 0, prorated_kb: 0, plan_kb: 0, unit: unit},
        extra: {section: "DATA", used: 0, left: 0, prorated: 0, prorated_kb: 0, plan_kb: 0, unit: unit},
        plus: {section: "", used: 0, left: 0, prorated: 0, prorated_kb: 0, plan_kb: 0, unit: unit},
        boost: {section: "BOOST", used: 0, left: 0, unit: unit, section_min_value: boostSectionKb},
        bonus: {section: "BONUS", used: 0, left: 0, unit: unit}
    };

    if (balanceData) balanceData.forEach(function (balanceItem) {
        if (balanceItem.validToDate < now.getTime()) {
            return;
        }

        if (cache.base && cache.base.name == balanceItem.packageName
            && balanceItem.serviceAlias == 'DATA') {

            computeStaticData(balanceItem, cache.base, true, false, data.basic);
        } else if (cache.extra_current && cache.extra_current.data
            && cache.extra_current.data.name == balanceItem.packageName) {

            computeStaticData(balanceItem, cache.extra_current.data, true, false, data.extra);
        } else {
            var dataAddon;
            var dataResult;

            for (var i = 0; cache.boost && !dataAddon && i < cache.boost.length; i++) {
                if (cache.boost[i].name == balanceItem.packageName) {
                    dataAddon = cache.boost[i];
                    dataResult = data.boost;
                    break;
                }
            }

            for (var i = 0; cache.bonus && !dataAddon && i < cache.bonus.length; i++) {
                if (cache.bonus[i].name == balanceItem.packageName) {
                    dataAddon = cache.bonus[i];
                    dataResult = data.bonus;
                    break;
                }
            }

            var dataPlus = ["Plus 2020", "Free Plus 2020"].indexOf(balanceItem.packageName) >= 0;
            for (var i = 0; cache.general && !dataAddon && dataPlus && i < cache.general.length; i++) {
                if (cache.general[i].name == balanceItem.packageName) {
                    dataAddon = cache.general[i];
                    dataResult = data.plus;

                    if (dataResult.name) dataResult.name += " / ";
                    dataResult.section += balanceItem.packageName;
                    break;
                }
            }

            if (dataAddon && dataResult) {
                computeStaticData(balanceItem, dataAddon, false, true, dataResult);
            }
        }
    });

    // if a client has some data stuck in bonus section, < 35MB,
    // we remove that data after 10MB of base section is consumed

    if (data.bonus.left < 35 * 1024 && (data.basic.used + data.extra.used + data.boost.used) > 10 * 1024) {
        data.bonus.used = data.bonus.left + data.bonus.used;
        data.bonus.left = 0;
    }

    if (cache.serviceInstanceUnlimitedData && cache.serviceInstanceUnlimitedData.enabled
        && (data.basic.left + data.extra.left + data.boost.left + data.bonus.left) < 40 * 1024) {
        data.basic.used = data.basic.left + data.basic.used;
        data.basic.left = 0;

        data.extra.used = data.extra.left + data.extra.used;
        data.extra.left = 0;

        data.boost.used = data.boost.left + data.boost.used;
        data.boost.left = 0;

        data.bonus.used = data.bonus.left + data.bonus.used;
        data.bonus.left = 0;

        data.plus.used = data.plus.left + data.plus.used;
        data.plus.left = 0;
    }

    var bonusText = prettifyDataKb(data.bonus.left + data.bonus.used);
    data.promotion_text = {
        line1: "Bonus",
        line2: (bonusText.value + " " + bonusText.unit)
    };

    if (data.basic.prorated || data.extra.prorated) {
        loadActivationDate(cache.number, function (date) {
            var dateFormatted = date ? dateformat(date, "dS mmmm") : undefined;
            var basicFormatted = prettifyDataKb(data.basic.used + data.basic.left + data.extra.used + data.extra.left);
            var proratedFormatted = prettifyDataKb(
                (data.basic.prorated ? data.basic.prorated_kb : data.basic.used + data.basic.left) +
                (data.extra.prorated ? data.extra.prorated_kb : data.extra.used + data.extra.left));

            data.basic.description = "Your data is pro-rated." +
                (dateFormatted ? " Your current plan was activated on the " + dateFormatted + " of the month." : "") +
                " You get " + proratedFormatted.value + " " + proratedFormatted.unit +
                " of your " + basicFormatted.value + " " + basicFormatted.unit + " monthly allocation." +
                " Of course, you will only be charged for the pro-rated amount in your bill."

            callback(undefined, data);
        });
    } else {
        callback(undefined, data);
    }
}

function loadActivationDate(number, callback) {
    db.notifications_webhook
        .find({number: number, activity: {"$in": ["validate_base_plan", "change_base_plan"]}}, {ts: 1, activity: 1})
        .sort({"ts": -1})
        .limit(1)
        .toArray(function (err, items) {
            var date = items && items.length > 0
                ? new Date(items[0].ts) : undefined;
            callback(date);
        });
}

function createCountersViewContainer(cache, balanceData, counterBalanceData, callback) {
    var now = new Date();

    var basicCallsUsed = 0;
    var basic_calls_total_secs = (!cache.base) ? 0 : (cache.extra_current.voice) ?
        (cache.extra_current.voice.mins + cache.base.mins) * 60 : cache.base.mins * 60;
    var base_used_secs = 0;
    var base_total_secs = (cache.base) ? cache.base.mins * 60 : 0;
    var extra_used_secs = 0;
    var extra_total_secs = (cache.extra_current.voice) ? cache.extra_current.voice.mins * 60 : 0;
    var basic_sms_used = 0;
    var basic_sms_total = (!cache.base) ? 0 : (cache.extra_current.sms)
        ? (parseInt(cache.extra_current.sms.sms) + parseInt(cache.base.sms)) : cache.base.sms;
    var base_used_sms = 0;
    var base_total_sms = (cache.base) ? cache.base.sms : 0;
    var extra_used_sms = 0;
    var extra_total_sms = (cache.extra_current.sms) ? cache.extra_current.sms.sms : 0;

    // counters
    var sms_idd = 0;
    var sms_price_idd = 0;
    var calls_incoming_idd = 0;
    var calls_incoming_price_idd = 0;
    var calls_outgoing_idd = 0;
    var calls_outgoing_price_idd = 0;
    var data_roam = 0;
    var data_price_roam = 0;
    var data_price_roam_actual = 0;
    var sms_roam = 0;
    var sms_price_roam = 0;
    var calls_incoming_roam = 0;
    var calls_incoming_price_roam = 0;
    var calls_outgoing_roam = 0;
    var calls_outgoing_price_roam = 0;
    var sms_local = 0;
    var sms_price_local = 0;
    var calls_incoming_local = 0;
    var calls_incoming_price_local = 0;
    var calls_outgoing_local = 0;
    var calls_outgoing_price_local = 0;

    if (balanceData) balanceData.every(function (balance) {
        if (balance.validToDate < now.getTime()) {
            return true;
        }

        if (cache.base && ((cache.base.sms > 0) || cache.extra_current.sms)
            && (balance.serviceAlias.indexOf("SMS_SERVICE_VAS") > -1)) {

            if (balance.packageName == cache.base.name) {
                base_used_sms = parseInt(balance.usedValue);
            } else if (balance.packageName == cache.extra_current.sms.name) {
                extra_used_sms = parseInt(balance.usedValue);
            }
            if ((balance.packageName == cache.base.name) ||
                (balance.packageName == cache.extra_current.sms.name)) {
                basic_sms_used += parseInt(balance.usedValue);
            }
        }
        return true;
    });

    var payg_airtime = 0;
    var total_airtime = 0;
    var unlimited_incoming_airtime = 0;

    if (counterBalanceData) counterBalanceData.every(function (counter) {
        if (counter.validToDate < now.getTime()) {
            return true;
        }

        if (counter.externalId && (counter.externalId == "AMOUNT_ROAMING_DATA")) {
            data_price_roam_actual = parseFloat(counter.usedValue) / 100;
            data_price_roam = data_price_roam_actual;
            if (data_price_roam < 5) {
                data_price_roam = 0;
            }
            return true;
        }

        if (counter.externalId && (counter.externalId == "AMOUNT_ROAMING_OUTGOING_CALLS")) {
            calls_outgoing_price_roam = parseFloat(counter.usedValue) / 100;
            return true;
        }
        if (counter.externalId && (counter.externalId == "AMOUNT_ROAMING_INCOMING_CALLS")) {
            calls_incoming_price_roam = parseFloat(counter.usedValue) / 100;
            return true;
        }
        if (counter.externalId && (counter.externalId == "AMOUNT_ROAMING_OUTGOING_SMS")) {
            sms_price_roam = parseFloat(counter.usedValue) / 100;
            return true;
        }
        if (counter.externalId && (counter.externalId == "AMOUNT_IDD_OUTGOING_CALLS")) {
            calls_outgoing_price_idd = parseFloat(counter.usedValue) / 100;
            return true;
        }
        if (counter.externalId && (counter.externalId == "AMOUNT_IDD_OUTGOING_SMS")) {
            sms_price_idd = parseFloat(counter.usedValue) / 100;
            return true;
        }
        if (counter.externalId && (counter.externalId == "AMOUNT_LOCAL_OUTGOING_CALLS")) {
            calls_outgoing_price_local = parseFloat(counter.usedValue) / 100;
            return true;
        }
        if (counter.externalId && (counter.externalId == "AMOUNT_LOCAL_INCOMING_CALLS")) {
            calls_incoming_price_local = parseFloat(counter.usedValue) / 100;
            return true;
        }
        if (counter.externalId && (counter.externalId == "AMOUNT_LOCAL_OUTGOING_SMS")) {
            sms_price_local = parseFloat(counter.usedValue) / 100;
            return true;
        }
        if (counter.externalId && (counter.externalId == "USAGE_ROAMING_DATA")) {
            data_roam = parseFloat(counter.usedValue);
            return true;
        }
        if (counter.externalId && (counter.externalId == "USAGE_ROAMING_OUTGOING_CALLS")) {
            calls_outgoing_roam = parseFloat(counter.usedValue);
            return true;
        }
        if (counter.externalId && (counter.externalId == "USAGE_ROAMING_INCOMING_CALLS")) {
            calls_incoming_roam = parseFloat(counter.usedValue);
            return true;
        }
        if (counter.externalId && (counter.externalId == "USAGE_ROAMING_OUTGOING_SMS")) {
            sms_roam = parseFloat(counter.usedValue);
            return true;
        }
        if (counter.externalId && (counter.externalId == "USAGE_IDD_OUTGOING_CALLS")) {
            calls_outgoing_idd = parseFloat(counter.usedValue);
            return true;
        }
        if (counter.externalId && (counter.externalId == "USAGE_IDD_OUTGOING_SMS")) {
            sms_idd = parseFloat(counter.usedValue);
            return true;
        }
        if (counter.externalId && (counter.externalId == "USAGE_LOCAL_OUTGOING_CALLS")) {
            calls_outgoing_local = parseFloat(counter.usedValue);
            payg_airtime += calls_outgoing_local;
            return true;
        }
        if (counter.externalId && (counter.externalId == "USAGE_LOCAL_INCOMING_CALLS")) {
            calls_incoming_local = parseFloat(counter.usedValue);
            payg_airtime += calls_incoming_local;
            return true;
        }
        if (counter.externalId && (counter.externalId == "USAGE_LOCAL_OUTGOING_SMS")) {
            sms_local = parseFloat(counter.usedValue);
            return true;
        }
        if (counter.externalId && (counter.externalId == "USAGE_UNLIMITED_LOCAL_INCOMING_CALLS")) {
            unlimited_incoming_airtime = parseFloat(counter.usedValue);
            return true;
        }
        if (counter.externalId && (counter.externalId == "USAGE_CALLS")) {
            total_airtime = parseFloat(counter.usedValue);
            return true;
        }
        return true;
    });

    total_airtime = total_airtime - payg_airtime - unlimited_incoming_airtime;

    // bug in ec where payg > airtime
    if (total_airtime < 0) {
        total_airtime = 0;
    }

    if (cache.base && (total_airtime < (cache.base.mins * 60))) {
        base_used_secs = total_airtime;
        extra_used_secs = 0;
        basicCallsUsed = total_airtime;
    } else if (cache.base && cache.extra_current.voice
        && (total_airtime < ((cache.base.mins + cache.extra_current.voice.mins) * 60))) {
        base_used_secs = cache.base.mins * 60;
        extra_used_secs = (cache.extra_current.voice) ? total_airtime - base_used_secs : 0;
        basicCallsUsed = total_airtime;
    } else {
        base_used_secs = (cache.base) ? cache.base.mins * 60 : 0;
        extra_used_secs = (cache.extra_current && cache.extra_current.voice) ? cache.extra_current.voice.mins * 60 : 0;
        basicCallsUsed = (cache.base) ? (cache.base.mins * 60) + extra_used_secs : 0;
    }

    var basic = {
        calls: {
            used: basicCallsUsed,
            left: basic_calls_total_secs < basicCallsUsed ? 0 : basic_calls_total_secs - basicCallsUsed,
            base_used: base_used_secs,
            base_left: base_total_secs - base_used_secs,
            extra_used: extra_used_secs,
            extra_left: extra_total_secs - extra_used_secs,
            unit_type: "seconds"
        },
        sms: {
            used: basic_sms_used,
            left: basic_sms_total - basic_sms_used,
            base_used: base_used_sms,
            base_left: base_total_sms - base_used_sms,
            extra_used: extra_used_sms,
            extra_left: extra_total_sms - extra_used_sms,
            unit_type: "sms"
        }
    }

    sms_price_idd = (cache.base.global_per_sms) ? cache.base.global_per_sms * sms_idd : sms_price_idd;
    sms_local = sms_local - basic_sms_used < 0 ? 0 : sms_local - basic_sms_used;

    var callsIddTotalSecs = calls_incoming_idd + calls_outgoing_idd;
    var callsIddTotalPrice = calls_incoming_price_idd + calls_outgoing_price_idd;
    var callsRoamTotalSecs = calls_incoming_roam + calls_outgoing_roam;
    var callsRoamTotalPrice = calls_incoming_price_roam + calls_outgoing_price_roam;
    var callsLocalTotalSecs = calls_incoming_local + calls_outgoing_local;
    var callsLocalTotalPrice = calls_incoming_price_local + calls_outgoing_price_local;

    var payAsYouGo = {
        calls_idd: {
            visible: true,
            total: {value: callsIddTotalSecs, unit_type: "seconds", price_total: createPriceObj(callsIddTotalPrice)},
            incoming: {
                value: calls_incoming_idd,
                unit_type: "seconds",
                price_total: createPriceObj(calls_incoming_price_idd)
            },
            outgoing: {
                value: calls_outgoing_idd,
                unit_type: "seconds",
                price_total: createPriceObj(calls_outgoing_price_idd)
            }
        },
        sms_idd: {
            visible: true,
            total: {value: sms_idd, unit_type: "sms", price_total: createPriceObj(sms_price_idd)}
        },
        data_roam: {
            visible: true,
            total: {
                value: data_roam, unit_type: "kilobytes", price_total: createPriceObj(data_price_roam),
                price_actual: createPriceObj(data_price_roam_actual)
            }
        },
        calls_roam: {
            visible: true,
            total: {value: callsRoamTotalSecs, unit_type: "seconds", price_total: createPriceObj(callsRoamTotalPrice)},
            incoming: {
                value: calls_incoming_roam,
                unit_type: "seconds",
                price_total: createPriceObj(calls_incoming_price_roam)
            },
            outgoing: {
                value: calls_outgoing_roam,
                unit_type: "seconds",
                price_total: createPriceObj(calls_outgoing_price_roam)
            }
        },
        sms_roam: {
            visible: true,
            total: {value: sms_roam, unit_type: "sms", price_total: createPriceObj(sms_price_roam)}
        },
        calls: {
            visible: true,
            total: {
                value: callsLocalTotalSecs, unit_type: "seconds", price_total: createPriceObj(callsLocalTotalPrice),
                price_per_unit: createPriceObj(cache.base.local_per_min, "minutes")
            },
            incoming: {
                value: calls_incoming_local,
                unit_type: "seconds",
                price_total: createPriceObj(calls_incoming_price_local)
            },
            outgoing: {
                value: calls_outgoing_local,
                unit_type: "seconds",
                price_total: createPriceObj(calls_outgoing_price_local)
            }
        },
        sms: {
            visible: true,
            total: {
                value: sms_local, unit_type: "sms", price_total: createPriceObj(sms_price_local),
                price_per_unit: createPriceObj(cache.base.local_per_sms, "sms")
            }
        }
    };

    var reply = {
        basic: basic,
        pay_as_you_go: payAsYouGo
    };

    callback(null, reply);
}

function createPriceObj(amout, unit) {
    var priceObj = {
        prefix: "$",
        value: amout,
        postfix: "SGD"
    }

    if (unit) {
        priceObj.unit = unit;
    }

    return priceObj;
}
