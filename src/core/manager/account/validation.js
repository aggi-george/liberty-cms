const config = require(`${global.__base}/config`);
const common = require(`${global.__lib}/common`);
const elitecoreBillService = require(`${global.__bss}/elitecore/elitecoreBillService`);
const elitecoreUserService = require(`${global.__bss}/elitecore/elitecoreUserService`);
const ecommManager = require(`${global.__manager}/ecomm/ecommManager`);
const notificationSend = require(`${global.__manager}/notifications/send`);
const profileManager = require(`${global.__manager}/profile/profileManager`);
const BaseError = require(`${global.__core}/errors/baseError`);
const billManager = require('./billManager');
const invoiceManager = require('./invoiceManager');
const actions = require('./actions');
const paymentManager = require('./paymentManager');

const LOG = config.LOGSENABLED;

class Validation {

    /**
     *
     */
    static validateStatusByBA(billingAccountNumber, options, callback) {
        if (!callback) callback = () => {};
        if (!options) options = {};

        if (!billingAccountNumber) {
            return callback(new BaseError('Invalid params',
                'ERROR_INVALID_PARAMS'));
        }

        const executionKey = config.NOTIFICATION_KEY;
        const initiator = options.initiator ? options.initiator : '[CMS][INTERNAL]';

        elitecoreUserService.loadUserInfoByBillingAccountNumber(billingAccountNumber, (err, cache) => {
            if (err) {
                return callback(err);
            }
            if (!cache) {
                return callback(new BaseError(`Customer BAN ${billingAccountNumber} not found`,
                    'ERROR_CUSTOMER_NOT_FOUND'));
            }
            if (!cache.number || cache.status == 'Terminated') {
                return callback(new BaseError(`Reactivation / Suspension is not allowed for ${billingAccountNumber}, customer status has ${cache.status} status`,
                    'ERROR_CUSTOMER_IS_TERMINATED'));
            }

            // clear cache before starting status check
            elitecoreBillService.clearCustomerPaymentsCache(billingAccountNumber);

            billManager.loadOutstandingAmount(cache.serviceInstanceNumber, (err, result) => {
                if (err) {
                    return callback(err);
                }

                const outstandingCurrentMonthAmount = result && result.amount ? result.amount : 0;
                billManager.loadOpenBillsHistoryByAccount(cache.customerAccountNumber, (err, allOpenBills) => {
                    if (err) {
                        return callback(err);
                    }

                    let openInvoiceIds = [];
                    allOpenBills.forEach((item) => {
                        openInvoiceIds.push(item.id);
                    });

                    invoiceManager.loadBills(undefined, {
                        invoiceIds: openInvoiceIds,
                        billingAccountNumbers: [ billingAccountNumber ]
                    }, (err, openLocalInvoices) => {
                        if (err) {
                            return callback(err);
                        }

                        let openBills = [];
                        let newUnsentInvoiceIds = [];
                        let negativeInvoiceIds = [];
                        allOpenBills.forEach((bssInvoice) => {
                            const invoiceDate = new Date(bssInvoice.date);
                            const currentDate = new Date();

                            const found = openLocalInvoices.data.some((localInvoice) => {
                                if (bssInvoice.id == localInvoice.invoiceId) {
                                    if (localInvoice.type == 'NEGATIVE') {
                                        negativeInvoiceIds.push(bssInvoice.id);
                                    } else if (invoiceDate.getTime() < currentDate.getTime() - 30 * 24 * 60 * 60 * 1000) {
                                        // for old invoices there is no need to check local notification status
                                        // all open invoices older than 1 month should be taken into consideration

                                        openBills.push(bssInvoice);
                                        return;
                                    } else if (localInvoice.sendStatus == 'OK'
                                        || localInvoice.sendStatus == 'SENDING'
                                        || options.ignoreSendStatus) {
                                        // invoices that has not been delivered
                                        // should not be part of equation when calculate

                                        openBills.push(bssInvoice);
                                    } else {
                                        newUnsentInvoiceIds.push(bssInvoice.id);
                                    }
                                    return true;
                                }
                            });

                            // if invoice is not synced locally it not supposed
                            // to be taken into consideration when suspend customers

                            if (!found) {
                                newUnsentInvoiceIds.push(bssInvoice.id);
                            }
                        });

                        if (LOG) common.log('AccountManager', `validateStatus: open count=${openBills.length}, outstandingAmount=${outstandingCurrentMonthAmount},
                            newUnsentInvoiceIds=${JSON.stringify(newUnsentInvoiceIds)}, negativeInvoiceIds=${JSON.stringify(negativeInvoiceIds)}`);

                        if (openBills.length == 0) {
                            if (cache.status !== 'Inactive') {
                                return callback(undefined, {
                                    message: `Customer has correct status - ${cache.status}, (re-activation is not required)`,
                                    status: 'STATUS_ACTIVE_CHANGE_NOT_REQUIRED'
                                });
                            }

                            profileManager.loadASelfcareSetting(cache.prefix, cache.number, 'voluntary_suspension',
                                config.NOTIFICATION_KEY, (err, type, value) => {
                                    if (err) {
                                        return callback(err);
                                    }
                                    if (value) {
                                        return callback(undefined, {
                                            message: 'Reactivation is not required, customer has a voluntary suspension',
                                            status: 'CUSTOMER_HAS_VOLUNTARY_SUSPENSION'
                                        });
                                    }

                                    ecommManager.checkBlacklist(cache.billingAccountNumber, (err, result) => {
                                        if (err) {
                                            return callback(err);
                                        }
                                        if (result && result.status == 0) {
                                            return callback(undefined, {
                                                message: 'Reactivation is not allowed, customer is blacklisted',
                                                status: 'CUSTOMER_IS_BACKLISTED'
                                            });
                                        }

                                        actions.activateAccount(cache.serviceInstanceNumber, 'ACTIVE_DUE_TO_BILL_PAYMENT',
                                            initiator, executionKey, (err, result) => {
                                                if (err) {
                                                    return callback(err);
                                                }

                                                callback(undefined, {
                                                    message: 'Account has been reactivated',
                                                    status: 'ACCOUNT_REACTIVATED',
                                                    newUnsentInvoiceIds: newUnsentInvoiceIds,
                                                    negativeInvoiceIds: negativeInvoiceIds,
                                                    activationResult: result
                                                });
                                            });
                                    });
                                });
                        } else {
                            if (cache.status !== 'Active') {
                                return callback(undefined, {
                                    message: `Customer has correct status - ${cache.status}, (suspension is not required)`,
                                    status: 'STATUS_INACTIVE_CHANGE_NOT_REQUIRED',
                                    openBills: allOpenBills
                                });
                            }
                            if (options.ignoreSuspension) {
                                return callback(undefined, {
                                    message: 'Suspension check is ignored',
                                    status: 'STATUS_INACTIVE_CHANGE_IGNORED',
                                    openBills: allOpenBills
                                });
                            }

                            let suspensionRequired;
                            let suspensionStatus;

                            let rows = [];
                            let outstandingTotal = 0;
                            openBills.forEach((item) => {
                                outstandingTotal += item.payNow ? item.payNow.value : 0;
                                rows.push({
                                    month: invoiceManager.getMonthName(item.date),
                                    invoicenumber: item.id,
                                    outstanding: item.payNow ? item.payNow.value : -1
                                });
                            });

                            if (openBills.length > 1) {
                                suspensionRequired = true;
                                suspensionStatus = 'OPEN_MULTIPLE_INVOICES';
                            } else if (openBills.length == 1) {
                                const invoiceDate = new Date(openBills[0].date);
                                const currentDate = new Date();

                                if (invoiceDate.getTime() < currentDate.getTime() - 50 * 24 * 60 * 60 * 1000) {
                                    suspensionRequired = true;
                                    suspensionStatus = 'OPEN_SINGLE_OLD_INVOICES';
                                } else if (outstandingCurrentMonthAmount + outstandingTotal >= 200) {
                                    suspensionRequired = true;
                                    suspensionStatus = 'OPEN_SINGLE_INVOICES_LARGE_OUTSTANDING';
                                }
                            }

                            if (!suspensionRequired) {
                                return callback(undefined, {
                                    message: `Customer has correct status - ${cache.status}, (suspension is not required)`,
                                    status: 'STATUS_INACTIVE_CHANGE_NOT_REQUIRED',
                                    newUnsentInvoiceIds: newUnsentInvoiceIds,
                                    negativeInvoiceIds: negativeInvoiceIds,
                                    suspensionStatus: suspensionStatus,
                                    outstandingTotal: outstandingTotal,
                                    outstandingCurrentMonthAmount: outstandingCurrentMonthAmount,
                                    openInvoices: rows,
                                    openBills: allOpenBills
                                });
                            }

                            const suspensionOptions = {
                                notificationVariables: {customTables: [{rowtag: 'open_invoices', rows: rows}]},
                                outstandingTotal: outstandingTotal,
                                outstandingCurrentMonthAmount: outstandingCurrentMonthAmount,
                                suspensionStatus: suspensionStatus
                            };

                            if (LOG) common.log('AccountManager', `validateStatus: suspension required, suspensionStatus=${suspensionStatus},
                                immediateSuspension=${options.immediateSuspension}`);

                            if (options.immediateSuspension) {
                                actions.suspendAccount(cache.serviceInstanceNumber, 'SUSPENDED_DUE_TO_BILL_PAYMENT_FAILURE',
                                    initiator, executionKey, (err, result) => {
                                        if (err) {
                                            return callback(err);
                                        }

                                        callback(undefined, {
                                            message: 'Account has been suspended',
                                            status: 'ACCOUNT_SUSPENDED',
                                            newUnsentInvoiceIds: newUnsentInvoiceIds,
                                            negativeInvoiceIds: negativeInvoiceIds,
                                            suspensionStatus: suspensionStatus,
                                            outstandingTotal: outstandingTotal,
                                            outstandingCurrentMonthAmount: outstandingCurrentMonthAmount,
                                            openInvoices: rows,
                                            openBills: allOpenBills,
                                            suspensionResult: result
                                        });
                                    }, suspensionOptions);
                            } else {
                                let invoicesInfo = [];
                                openBills.forEach((item) => {
                                    invoicesInfo.push({
                                        invoiceId: item.id,
                                        amount: item.payNow.value
                                    });
                                });

                                paymentManager.scheduleBillPaymentByBA(cache.billingAccountNumber, invoicesInfo, {
                                    immediateSuspension: true,
                                    suspensionPaymentAttempt: true,
                                    platform: 'SCHEDULER'
                                }, true, (err, result) => {
                                    if (err) {
                                        return callback(err);
                                    }

                                    const unpaidInvoices = rows;
                                    const notification = {
                                        teamID: 5,
                                        teamName: 'Operations',
                                        activity: 'bill_failure_suspension_day_before',
                                        prefix: cache.prefix,
                                        number: cache.number,
                                        name: cache.billingFullName,
                                        email: cache.email,
                                        billingAccountNumber: cache.billingAccountNumber,
                                        serviceInstanceNumber: cache.serviceInstanceNumber,
                                        outstandingTotal: outstandingTotal,
                                        suspensionStatus: suspensionStatus,
                                        customTables: [{rowtag: 'open_invoices', rows: rows}]
                                    };

                                    const notificationResultHandler = (err, notificationResult) => {
                                        callback(undefined, {
                                            message: 'Account will be suspended in 24h',
                                            status: 'ACCOUNT_SUSPENSION_SCHEDULED',
                                            newUnsentInvoiceIds: newUnsentInvoiceIds,
                                            negativeInvoiceIds: negativeInvoiceIds,
                                            suspensionStatus: suspensionStatus,
                                            outstandingTotal: outstandingTotal,
                                            outstandingCurrentMonthAmount: outstandingCurrentMonthAmount,
                                            eventResult: result,
                                            unpaidInvoices: unpaidInvoices,
                                            openBills: allOpenBills,
                                            notificationResult: {
                                                error: err ? err.message : undefined,
                                                notificationId: notificationResult ? notificationResult.notificationId : undefined
                                            }
                                        });
                                    };

                                    if (result && result.added) {
                                        notificationSend.deliver(notification, undefined, notificationResultHandler, cache);
                                    } else {
                                        notificationResultHandler();
                                    }
                                });
                            }
                        }
                    });
                });
            });
        });
    }

}

module.exports = Validation;
