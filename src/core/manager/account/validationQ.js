const config = require(`${global.__base}/config`);
const common = require(`${global.__lib}/common`);
const db = require(`${global.__lib}/db_handler`);
const betaUsersManager = require(`${global.__manager}/beta/betaUsersManager`);
const BaseError = require(`${global.__core}/errors/baseError`);

const LOG = config.LOGSENABLED;

class ValidationQ {

    /**
     *
     */
    static queueValidateStatusByBA(billingAccountNumber, options, callback) {
        if (!callback) callback = () => {};

        if (!billingAccountNumber) {
            return callback(new BaseError('Invalid params', 'ERROR_CUSTOMER_NOT_FOUND'));
        }

        betaUsersManager.isBetaCustomer(billingAccountNumber, 'bills_dr_only', (err, backupOnly) => {
            if (err) {
                if (LOG) common.log('queueValidateStatusByBA', 'beta error');
            }

            // for some customer we can get valid data from DR onl so for such customer we need to check bill
            // status after some time, not immediately, because DR may not be synced, DR sync ~ 1-2 min

            var job = db.queue.create('statusValidation', {
                billingAccountNumber: billingAccountNumber,
                options: options
            }).events(false).removeOnComplete(true).delay(backupOnly ? 10 * 60 * 1000 : 10 * 1000).attempts(2).backoff({
                delay: 10 * 1000,
                type: 'fixed'
            }).save((err) => {
                if (err) {
                    common.error('InvoiceManager', 'scheduleValidateStatus: failed to add job to the queue');
                    return callback(err);
                }

                callback(undefined, job ? {
                    id: job.id,
                    type: job.type
                } : {id: -1});
            });
        });
    }

}

module.exports = ValidationQ;
