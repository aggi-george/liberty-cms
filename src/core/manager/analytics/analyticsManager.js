var request = require('request');
var async = require('async');

var config = require('../../../../config');
var common = require('../../../../lib/common');
var db = require('../../../../lib/db_handler');
var ec = require('../../../../lib/elitecore');

var log = config.LOGSENABLED;

module.exports = {

    /**
     *
     */
    loadEcOverallStats: function (startDate, endDate, callback) {
        if (!callback) callback = () => {
        }

        var queryStart = new Date();
        module.exports.loadEcStats(startDate, endDate, 2, undefined, "HOST_ALL_IN_ONE", (err, result) => {
            if (err) {
                common.error("AnalyticsManager", "loadEcOverallStats: failed to load, error=" + err.message);
                return callback(err);
            }

            if (!result || !result[0] || !result[0].list || !result[0].list[0]) {
                common.error("AnalyticsManager", "loadEcOverallStats: empty result");
                return callback(new Error("Empty result"));
            }

            let totalTimeouts = 0;
            result.forEach((host) => {
                if (host && host.list && host.list[0]) totalTimeouts += host.list[0].totalTimeouts;
            });
            callback(undefined, {
                stats: result,
                totalTimeouts,
                queryTime: (new Date().getTime() - queryStart.getTime())
            });
        })
    },

    /**
     *
     */
    loadEcStats: function (startDate, endDate, countOfPoints, host, method, callback) {
        if (!startDate || !endDate || !countOfPoints) {
            common.error("AnalyticsManager", "loadEcStats, invalid params");
            if (callback) callback(new Error("Invalid params"));
            return;
        }

        var start = new Date(startDate).getTime();
        var end = new Date(endDate).getTime();

        if (start > end) {
            common.error("AnalyticsManager", "loadEcStats, Start date is greater than end date");
            if (callback) callback(new Error("Start date is greater than end date"));
            return;
        }

        var step = parseInt((end - start) / (countOfPoints - 1));
        var params = [
            {
                $unwind: "$requests"
            }, {
                $match: {$and: [{ts: {$gt: start}}, {ts: {$lt: end}}]}
            }, {
                $project: {
                    _id: 0,
                    "host": 1,
                    "requests.method": 1,
                    "requests.executionTime": 1,
                    "requests.executionMaxTime": 1,
                    "requests.requestsCount": 1,
                    "requests.errorsCount": 1,
                    "requests.timeoutsCount": 1,
                    ts: 1,
                    pivot: {$divide: [{$subtract: [{$subtract: ["$ts", start]}, {$mod: [{$subtract: ["$ts", start]}, step]}]}, step]}
                }
            }, {
                "$group": {
                    _id: {pivot: "$pivot", method: "$requests.method"},
                    avgTime: {$avg: "$requests.executionTime"},
                    maxTime: {$max: "$requests.executionMaxTime"},
                    totalRequests: {$sum: "$requests.requestsCount"},
                    totalErrors:{$sum:"$requests.errorsCount"},
                    totalTimeouts:{$sum:"$requests.timeoutsCount"}
                }
            }, {
                $sort: {"_id.method": 1, "_id.pivot": 1}
            }];

        if (method === "ALL_IN_ONE") {
            delete params[3].$group._id.method;
        } else if (method === "HOST_ALL_IN_ONE") {
            delete params[3].$group._id.method;
            params[3].$group._id.host = '$host';
        } else if (method) {
            params[1].$match["requests.method"] = method;
        }

        if (host) {
            params[1].$match["host"] = host;
        }

        if (log) common.log('loadECStats', JSON.stringify(params));
        db.ecstats.aggregate(params, { readPreference: db.readPreference.SECONDARY_PREFERRED }).toArray(function (err, result) {
            if (err) {
                common.error("AnalyticsManager", "loadEcStats, load error, error=" + err.message);
                if (callback) callback(err);
                return;
            }

            var aggregated = {};
            var resultFormatted = [];

            result.forEach(function(item) {
                var aggregatedObj = item._id.host ? aggregated[item._id.host] : aggregated[item._id.method];

                if (!aggregatedObj) {
                    aggregatedObj = {
                        api: item._id.method,
                        host: item._id.host,
                        list:[],
                        prevPivot: -1
                    }

                    const keyid = item._id.host ? item._id.host : item._id.method;
                    aggregated[keyid] = aggregatedObj;
                    resultFormatted.push(aggregatedObj);
                }

                // add missing points in the middle

                for (var i = aggregatedObj.prevPivot + 1; i < item._id.pivot; i++) {
                    aggregatedObj.list.push({
                        pointNum: i,
                        blockTime: new Date(i * step + start),
                        avgTime: 0,
                        maxTime: 0,
                        totalRequests: 0,
                        totalErrors: 0,
                        totalTimeouts: 0
                    });
                }

                aggregatedObj.prevPivot = item._id.pivot;
                aggregatedObj.list.push({
                    pointNum: item._id.pivot,
                    blockTime: new Date(item._id.pivot * step + start),
                    avgTime: item.avgTime,
                    maxTime: item.maxTime,
                    totalRequests: item.totalRequests,
                    totalErrors: item.totalErrors,
                    totalTimeouts: item.totalTimeouts
                });
            });

            // add missing points in the end
            resultFormatted.forEach(function(item) {
                for (var i = item.prevPivot + 1; i < countOfPoints; i++) {
                    item.list.push({
                        pointNum: i,
                        blockTime: new Date(i * step + start),
                        avgTime: 0,
                        maxTime: 0,
                        totalRequests: 0,
                        totalErrors: 0,
                        totalTimeouts: 0
                    });
                }

                delete item.prevPivot;
            });

            if (callback) callback(undefined, resultFormatted);
            return;
        });
    },

    /**
     *
     */
    loadClassesStats: function (startDate, endDate, countOfPoints, type, callback) {
        if (!startDate || !endDate || !countOfPoints) {
            common.error("AnalyticsManager", "loadEcStats, invalid params");
            if (callback) callback(new Error("Invalid params"));
            return;
        }

        var start = new Date(startDate).getTime();
        var end = new Date(endDate).getTime();

        if (start > end) {
            common.error("AnalyticsManager", "loadEcStats, Start date is greater than end date");
            if (callback) callback(new Error("Start date is greater than end date"));
            return;
        }

        var step = parseInt((end - start) / (countOfPoints));
        var params = [
            {
                $unwind: "$classes"
            }, {
                $match: {$and: [{ts: {$gt: start}}, {ts: {$lt: end}}]}
            }, {
                $project: {
                    _id: 0,
                    "classes.type": 1,
                    "classes.count": 1,
                    ts: 1,
                    pivot: {$divide: [{$subtract: [{$subtract: ["$ts", start]}, {$mod: [{$subtract: ["$ts", start]}, step]}]}, step]}
                }
            }, {
                "$group": {
                    _id: {pivot: "$pivot", type: "$classes.type"},
                    totalCount: {$max: "$classes.count"}
                }
            }, {
                $sort: {"_id.type": 1, "_id.pivot": 1}
            }];

        if (type === "ALL_IN_ON") {
            delete params[3].$group._id.type;
        } else if (type) {
            params[1].$match["classes.type"] = type;
        }

        db.classifiertats.aggregate(params, { readPreference: db.readPreference.SECONDARY_PREFERRED }).toArray(function (err, result) {
            if (err) {
                common.error("AnalyticsManager", "loadEcStats, load error, error=" + err.message);
                if (callback) callback(err);
                return;
            }

            var aggregated = {};
            var resultFormatted = [];

            result.forEach(function(item) {
                var aggregatedObj = aggregated[item._id.type];

                if (!aggregatedObj) {
                    aggregatedObj = {
                        type: type === "ALL_IN_ON" ? "ALL_IN_ON" : item._id.type,
                        list:[],
                        prevPivot: -1
                    }

                    aggregated[item._id.type] = aggregatedObj;
                    resultFormatted.push(aggregatedObj);
                }

                // add missing points in the middle

                for (var i = aggregatedObj.prevPivot + 1; i < item._id.pivot; i++) {
                    aggregatedObj.list.push({
                        pointNum: i,
                        blockTime: new Date(i * step + start),
                        totalCount: 0
                    });
                }

                aggregatedObj.prevPivot = item._id.pivot;
                aggregatedObj.list.push({
                    pointNum: item._id.pivot,
                    blockTime: new Date(item._id.pivot * step + start),
                    totalCount: item.totalCount
                });
            });

            // add missing points in the end

            resultFormatted.forEach(function(item) {
                for (var i = item.prevPivot + 1; i < countOfPoints; i++) {
                    item.list.push({
                        pointNum: i,
                        blockTime: new Date(i * step + start),
                        totalCount: 0
                    });
                }

                delete item.prevPivot;
            });

            if (callback) callback(undefined, resultFormatted);
            return;
        });
    }
}


