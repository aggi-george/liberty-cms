var request = require('request');
var async = require('async');

var config = require('../../../../../config');
var common = require('../../../../../lib/common');
var betaUsersManager = require('../../beta/betaUsersManager');
var baseLeaderboardClass = require('./base/baseLeaderboardClass');

var log = config.LOGSENABLED;

module.exports = {

    verifyCategory: (cache, callback) => {
        if (!cache || !cache.serviceInstanceNumber || !cache.number) {
            return callback(undefined, false);
        }

        betaUsersManager.isBetaCustomer(cache.number, "circles_internal", (err, isInGroup) => {
            return callback(undefined, isInGroup);
        });
    },

    aggregationType: () => {
        return "LEADERBOARD_DATA";
    }
}