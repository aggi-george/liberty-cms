var request = require('request');
var async = require('async');

var config = require('../../../../../config');
var common = require('../../../../../lib/common');
var baseLeaderboardClass = require('./base/baseLeaderboardClass');

var log = config.LOGSENABLED;

module.exports = {

    verifyCategory: (cache, callback) => {
        callback(undefined, cache.serviceInstanceNumber
        && cache.serviceInstanceCustomerStatus != 'Terminated'
        && cache.serviceInstanceCustomerStatus != 'Active'
        && cache.serviceInstanceCustomerStatus != 'Unknown');
    }
}