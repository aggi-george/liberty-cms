var config = require('../../../../../config');
var common = require('../../../../../lib/common');
var betaUsersManager = require('../../beta/betaUsersManager');

var log = config.LOGSENABLED;

module.exports = {

    verifyCategory: (cache, callback) => {
        if (!cache || !cache.serviceInstanceNumber) {
            return callback(undefined, false);
        }

        betaUsersManager.isBetaCustomer(cache.serviceInstanceNumber,
            "august_free_20_20", (err, isInGroup) => {
                return callback(undefined, isInGroup);
        });
    }
}