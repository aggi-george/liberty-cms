var request = require('request');
var async = require('async');

var config = require('../../../../../../config');
var common = require('../../../../../../lib/common');
var db = require('../../../../../../lib/db_handler');
var elitecoreUserService = require('../../../../../../lib/manager/ec/elitecoreUserService');

var log = config.LOGSENABLED;

var memoryPlus2020Cache = {};
var memoryPlus2020CacheInit = false;

module.exports = {

    loadPackages: (packageName, cache, callback) => {
        if (!callback) callback = () => {
        };

        if (!cache || !cache.serviceInstanceNumber) {
            return callback(undefined, 0);
        }

        if (packageName != "Plus 2020") {
            return callback(undefined, 0);
        }

        if (packageName == "Plus 2020") {
            if (memoryPlus2020CacheInit) {
                if (memoryPlus2020Cache[cache.serviceInstanceNumber]) {
                    return callback(undefined, memoryPlus2020Cache[cache.serviceInstanceNumber]);
                } else {
                    return callback(undefined, {active: false});
                }
            }
        }

        elitecoreUserService.loadActivePackages(cache.serviceInstanceNumber, (err, packages, packagesCached) => {
            if (err) {
                common.error("ClassManager", "loadPackages: failed to load referrals count, error=" + err.message);
                return callback(err);
            }

            if (!packages) {
                common.error("ClassManager", "loadPackages: failed to load active packages");
                return callback(new Error("Empty db response"));
            }

            var found = {active: false};
            packages.forEach((item) => {
                if (item.name == packageName) {
                    found.active = true;
                }
            });

            callback(undefined, found);
        });
    },

    initMemoryCache: (name, callback) => {
        if (!callback) callback = () => {
        };

        if (name == "Plus 2020") {

            memoryPlus2020Cache = {};
            memoryPlus2020CacheInit = false;

            elitecoreUserService.loadCustomerWithPackage(name, function (err, ids) {
                if (err) {
                    common.error("ClassManager", "loadPackages: failed to load " + name + " pluses, error=" + err.message);
                    return callback(err);
                }

                ids.forEach((sin) => {
                    memoryPlus2020Cache[sin] = {active: true};
                });

                return callback();
            });
        } else {
            return callback();
        }
    },

    cleanMemoryCache: () => {
        memoryPlus2020Cache = {};
        memoryPlus2020CacheInit = false;
    }
}