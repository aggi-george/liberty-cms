var request = require('request');
var async = require('async');

var config = require('../../../../../../config');
var common = require('../../../../../../lib/common');
var db = require('../../../../../../lib/db_handler');

var log = config.LOGSENABLED;
var memoryCache = {};

module.exports = {

    loadLeaderboardData: (cache, callback) => {
        if (!callback) callback = () => {
        };

        if (!cache || !cache.serviceInstanceNumber) {
            return callback(undefined, 0);
        }

        if (memoryCache[cache.serviceInstanceNumber] >= 0) {
            return callback(undefined, memoryCache[cache.serviceInstanceNumber]);
        }

        var query = "SELECT SUM(bonus_kb_pretty) / 1000 / 1000 as bonus_gb " +
            "FROM historyBonuses WHERE deactivated=0 AND (display_until_ts < 1000 || display_until_ts = '') AND " +
            "service_instance_number=" + db.escape(cache.serviceInstanceNumber) + " GROUP BY service_instance_number";

        db.query_err(query, function (err, rows) {
            if (err) {
                common.error("ClassManager", "loadLeaderboardData: failed to load leaderboard gb, error=" + err.message);
                return callback(err);
            }

            if (!rows || !rows[0]) {
                callback(undefined, 0);
            } else {
                callback(undefined, rows[0].bonus_gb);
            }
        });
    },

    initMemoryCache: (callback) => {
        if (!callback) callback = () => {
        };

        memoryCache = {};

        var query = "SELECT service_instance_number, SUM(bonus_kb_pretty) / 1000 / 1000 as bonus_gb " +
            " FROM historyBonuses WHERE deactivated=0 AND (display_until_ts < 1000 || display_until_ts = '')" +
            " GROUP BY service_instance_number";

        db.query_err(query, function (err, rows) {
            if (err) {
                common.error("ClassManager", "loadLeaderboardData: failed to load leaderboard gb, error=" + err.message);
                return callback(err);
            }

            if (!rows || !rows[0]) {
                return callback();
            }

            rows.forEach((item) => {
                memoryCache[item.service_instance_number] = item.bonus_gb;
            });

            return callback();
        });
    },

    cleanMemoryCache: () => {
        memoryCache = {};
    }
}