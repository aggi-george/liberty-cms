var request = require('request');
var async = require('async');

var config = require('../../../../../../config');
var common = require('../../../../../../lib/common');
var db = require('../../../../../../lib/db_handler');

var log = config.LOGSENABLED;
var memoryCache = {};

module.exports = {

    loadCountReferrals: (cache, callback) => {
        if (!callback) callback = () => {
        };

        if (!cache || !cache.serviceInstanceNumber) {
            return callback(undefined, 0);
        }

        if (memoryCache[cache.serviceInstanceNumber] >= 0) {
            return callback(undefined, memoryCache[cache.serviceInstanceNumber]);
        }

        var query = "SELECT count(*) count FROM historyBonuses WHERE bonus_type="
            + db.escape('referral') + " AND service_instance_number=" + db.escape(cache.serviceInstanceNumber);

        db.query_err(query, function (err, rows) {
            if (err) {
                common.error("ClassManager", "loadCountReferrals: failed to load referrals count, error=" + err.message);
                return callback(err);
            }

            if (!rows || !rows[0]) {
                common.error("ClassManager", "loadCountReferrals: failed to load referrals count");
                return callback(new Error("Empty db response"));
            }

            callback(undefined, rows[0].count);
        });
    },

    initMemoryCache: (callback) => {
        if (!callback) callback = () => {
        };

        memoryCache = {};
        var query = "SELECT service_instance_number, count(*) count FROM historyBonuses WHERE bonus_type="
            + db.escape('referral');

        db.query_err(query, function (err, rows) {
            if (err) {
                common.error("ClassManager", "loadCountReferrals: failed to load referrals count, error=" + err.message);
                return callback(err);
            }

            if (!rows || !rows[0]) {
                return callback();
            }

            rows.forEach((item) => {
                memoryCache[item.service_instance_number] = item.count;
            });

            return callback();
        });
    },

    cleanMemoryCache: () => {
        memoryCache = {};
    }
}