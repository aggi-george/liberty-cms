'use strict';

module.exports = ClassProcessingAggregator;

function ClassProcessingAggregator(type) {
    this.id = -1;
    this.type = type;
    this.handlers = [];
};

ClassProcessingAggregator.prototype.addClassHandler = function(classHandlers) {
    this.handlers.push(classHandlers);
}

ClassProcessingAggregator.prototype.verifyCategory = function(cache, callback) {
    var results = [];
    this.handlers.forEach((classHandler) => {
        (function (cache, classHandler, type, expectedLength) {
            classHandler.handler.verifyCategory(cache, (err, inClass, metadata) => {
                results.push({
                    error: err ? err.message : undefined,
                    inClass: err ? false : inClass,
                    classId: classHandler.id,
                    classType: classHandler.type,
                    metadata: metadata
                });

                if (results.length == expectedLength) {
                    if (callback) callback(undefined, results, {isArray: true, type: type});
                }
            });
        })(cache, classHandler, this.type, this.handlers.length);
    })
};