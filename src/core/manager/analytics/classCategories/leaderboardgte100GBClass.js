var request = require('request');
var async = require('async');

var config = require('../../../../../config');
var common = require('../../../../../lib/common');
var baseLeaderboardClass = require('./base/baseLeaderboardClass');

var log = config.LOGSENABLED;

module.exports = {

    verifyCategory: (cache, callback) => {
        baseLeaderboardClass.loadLeaderboardData(cache, (err, totalGb) => {
            callback(err, cache.serviceInstanceNumber && totalGb >= 100, {dataInt1: totalGb * 1000});
        });
    },

    aggregationType: () => {
        return "LEADERBOARD_DATA";
    }
}