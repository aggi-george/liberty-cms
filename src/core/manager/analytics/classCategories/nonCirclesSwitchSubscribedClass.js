const request = require('request');
const async = require('async');

const config = require('../../../../../config');
const common = require('../../../../../lib/common');
const baseAddonsClass = require('./base/baseAddonsClass');

const log = config.LOGSENABLED;

module.exports = {
    verifyCategory: (cache, callback) => {
        callback(undefined, cache.serviceInstanceNumber && cache.serviceInstanceBasePlanName !== 'CirclesSwitch' && cache.serviceInstanceCustomerStatus !== 'Terminated');
    }
}