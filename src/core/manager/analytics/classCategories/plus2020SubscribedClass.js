var request = require('request');
var async = require('async');

var config = require('../../../../../config');
var common = require('../../../../../lib/common');
var baseAddonsClass = require('./base/baseAddonsClass');

var log = config.LOGSENABLED;

module.exports = {

    verifyCategory: (cache, callback) => {
        baseAddonsClass.loadPackages("Plus 2020", cache, (err, activePackage) => {
            callback(err, cache.serviceInstanceNumber && activePackage && activePackage.active);
        });
    },

    aggregationType: () => {
        return "ADDONS";
    }
}