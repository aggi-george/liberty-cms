var request = require('request');
var async = require('async');

var config = require('../../../../../config');
var common = require('../../../../../lib/common');
var baseReferralsClass = require('./base/baseReferralsClass');

var log = config.LOGSENABLED;

module.exports = {

    verifyCategory: (cache, callback) => {
        baseReferralsClass.loadCountReferrals(cache, (err, count) => {
            callback(err, cache.serviceInstanceNumber
            && cache.serviceInstanceCustomerStatus == 'Active' && count == 0, {dataInt1: count});
        });
    },

    aggregationType: () => {
        return "REFERRAL_COUNT";
    }
}