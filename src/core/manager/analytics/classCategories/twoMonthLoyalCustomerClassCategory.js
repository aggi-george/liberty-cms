const billManager = require('../../account/billManager');

class TwoMonthLoyalCustomerClassCategory{
	
	constructor(){
		this.verifyCategory = this.verifyCategory.bind(this);
	};
	
	verifyCategory(cache, callback){
		if (!cache || !cache.billingAccountNumber) {
			return callback(undefined, false);
		}
		billManager.isCustomerLoyal(cache.billingAccountNumber,2,callback);
	};
}

const twoMonthLoyalCustomerClassCategory = new TwoMonthLoyalCustomerClassCategory();
module.exports = twoMonthLoyalCustomerClassCategory;