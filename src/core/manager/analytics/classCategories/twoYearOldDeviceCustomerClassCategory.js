const deviceManager = require('../../account/deviceManager');

class TwoYearOldDeviceCustomerClassCategory{
	
	constructor(){
		this.verifyCategory = this.verifyCategory.bind(this);
	};
	
	verifyCategory(cache, callback){
		if (!cache || !cache.number || !cache.prefix) {
            return callback(undefined, false);
        }

        deviceManager.isTheDeviceOld(cache.prefix, cache.number,2,callback);
	};
}

const twoYearOldDeviceCustomerClassCategory = new TwoYearOldDeviceCustomerClassCategory();
module.exports = twoYearOldDeviceCustomerClassCategory;