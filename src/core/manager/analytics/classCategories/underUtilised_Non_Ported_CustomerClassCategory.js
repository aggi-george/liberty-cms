const usageManager = require('../../account/usageManager');

class UnderUtilisedNonPortedCustomerClassCategory{
	
	constructor(){
		this.verifyCategory = this.verifyCategory.bind(this);
		this.aggregationType = this.aggregationType.bind(this);
	};
	
	verifyCategory(cache, callback){
		if (!cache || !cache.serviceInstanceNumber) {
            return callback(undefined, false);
        }
        if(cache.ported){
            return callback(undefined, false);
        }else{
            usageManager.isLowUsageUtilization(cache.serviceInstanceNumber,500,callback);
        }
	};
	aggregationType(){
        return 'PORT_IN_STATUS';
    };
}

const underUtilisedNonPortedCustomerClassCategory = new UnderUtilisedNonPortedCustomerClassCategory();
module.exports = underUtilisedNonPortedCustomerClassCategory;