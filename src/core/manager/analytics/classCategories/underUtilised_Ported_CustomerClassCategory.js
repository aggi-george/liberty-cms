const usageManager = require('../../account/usageManager');

class UnderUtilisedPortedCustomerClassCategory{
	
	constructor(){
		this.verifyCategory = this.verifyCategory.bind(this);
		this.aggregationType = this.aggregationType.bind(this);
	};
	
	verifyCategory(cache, callback){
		if (!cache || !cache.serviceInstanceNumber) {
            return callback(undefined, false);
        }
        if(cache.ported){
            usageManager.isLowUsageUtilization(cache.serviceInstanceNumber,500,callback);
        }else{
            return callback(undefined, false);
        }
	};
	aggregationType(){
        return 'PORT_IN_STATUS';
    };
}

const underUtilisedPortedCustomerClassCategory = new UnderUtilisedPortedCustomerClassCategory();
module.exports = underUtilisedPortedCustomerClassCategory;