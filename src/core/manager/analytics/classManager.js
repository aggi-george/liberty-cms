var request = require('request');
var async = require('async');

const config = require(`${global.__base}/config`);
const common = require(`${global.__lib}/common`);
const db = require(`${global.__lib}/db_handler`);

var elitecoreUserService = require('../../../../lib/manager/ec/elitecoreUserService');
var profileManager = require('../profile/profileManager');

const LOG = config.LOGSENABLED;

var operationKey = "classifyCustomers";
var operationSyncProgressKey = "classifyCustomersProgress";

var baseAddonsClass = require('./classCategories/base/baseAddonsClass');
var baseLeaderboardClass = require('./classCategories/base/baseLeaderboardClass');
var baseReferralsClass = require('./classCategories/base/baseReferralsClass');
var ClassProcessingAggregator = require('./classCategories/base/classProcessingAggregator');

var BaseError = require('../../errors/baseError');

module.exports = {

    getCategoryHandlers: () => {
        return {
            REFERRALS_ZERO: require("./classCategories/referrals0ClassCategory"),
            REFERRALS_ZERO_TO_TWO: require("./classCategories/referrals0to2ClassCategory"),
            REFERRALS_THREE_PLUS: require("./classCategories/referrals3plusClassCategory"),

            REFERRALS_P2_ZERO: require("./classCategories/referralsP2R0ClassCategory"),
            REFERRALS_P2_ONE_TO_TWO: require("./classCategories/referralsP2R1to2ClassCategory"),
            REFERRALS_P2_THREE_TO_NINE: require("./classCategories/referralsP2R3to9ClassCategory"),
            REFERRALS_P2_TEN_PLUS: require("./classCategories/referralsP2R10plusClassCategory"),

            NUMBER_PORTED: require("./classCategories/portInClassCategory"),
            NUMBER_NON_PORTED: require("./classCategories/nonPortInClassCategory"),
            LEADERBOARD_GTE_0GB_LT_3GB: require("./classCategories/leaderboardgte0GBlt3GBClass"),
            LEADERBOARD_GTE_3GB_LT_5GB: require("./classCategories/leaderboardgte3GBlt5GBClass"),
            LEADERBOARD_GTE_5GB_LT_15GB: require("./classCategories/leaderboardgte5GBlt15GBClass"),
            LEADERBOARD_GTE_15GB_LT_30GB: require("./classCategories/leaderboardgte15GBlt30GBClass"),
            LEADERBOARD_GTE_30GB_LT_50GB: require("./classCategories/leaderboardgte30GBlt50GBClass"),
            LEADERBOARD_GTE_50GB_LT_70GB: require("./classCategories/leaderboardgte50GBlt70GBClass"),
            LEADERBOARD_GTE_70GB_LT_100GB: require("./classCategories/leaderboardgte70GBlt100GBClass"),
            LEADERBOARD_GTE_100GB: require("./classCategories/leaderboardgte100GBClass"),
            ACCOUNT_ACTIVE: require("./classCategories/accountActiveClass"),
            ACCOUNT_TERMINATED: require("./classCategories/accountTerminatedClass"),
            ACCOUNT_SUSPENDED: require("./classCategories/accountSuspendedClass"),
            ACCOUNT_NOT_FOUND: require("./classCategories/accountNotFoundClass"),
            ACCOUNT_BETA: require("./classCategories/accountBeta"),
            PLUS_20_FOR_20_NOT_SUBSCRIBED: require("./classCategories/plus2020NotSubscribedClass"),
            PLUS_20_FOR_20_SUBSCRIBED: require("./classCategories/plus2020SubscribedClass"),
            AUGUST_FREE_20_20: require("./classCategories/augustFree2020Class"),
            NON_CIRCLES_SWITCH: require("./classCategories/nonCirclesSwitchSubscribedClass"),

            //INSERT INTO `cms_dev`.`customerClass` (`name`, `type`) VALUES ('Two Months Loyal', 'TWO_MONTH_LOYAL');
            TWO_MONTH_LOYAL: require("./classCategories/twoMonthLoyalCustomerClassCategory"),

           //INSERT INTO `cms_dev`.`customerClass` (`name`, `type`) VALUES ('Two Year Old Device Users', 'TWO_YEAR_OLD_DEVICE_USERS');
            TWO_YEAR_OLD_DEVICE_USERS: require("./classCategories/twoYearOldDeviceCustomerClassCategory"),

            //INSERT INTO `cms_dev`.`customerClass` (`name`, `type`) VALUES ('Under Utilized Non Ported < 500mb in last two months', 'UNDER_UTILISED_NONPORTED');
            UNDER_UTILISED_NONPORTED: require("./classCategories/underUtilised_Non_Ported_CustomerClassCategory"),

            //INSERT INTO `cms_dev`.`customerClass` (`name`, `type`) VALUES ('Under Utilized Ported < 500mb in last two months', 'UNDER_UTILISED_PORTED');
            UNDER_UTILISED_PORTED: require("./classCategories/underUtilised_Ported_CustomerClassCategory")
        }
    },

    /**
     *
     */
    classifyCustomers: (callback) => {
        if (!callback) callback = () => {
        }

        var updateCache = (stepNumber, stepsCount, stepProgress, end) => {
            db.cache.put("cache", operationSyncProgressKey, {
                stepNumber: stepNumber,
                stepsCount: stepsCount,
                stepProgress: stepProgress
            }, end ? 3000 : 300 * 1000);
        }

        var stepsCount = 9;
        var stepNumber = 0;
        Promise.resolve().then(() => {
            return new Promise((resolve, reject) => {
                updateCache(++stepNumber, stepsCount, 0);
                db.lockOperation(operationKey, {}, 300 * 1000, function (err) {
                    if (err) {
                        common.error("PortinManager", "classifyCustomers:lock: failed for start operation, err=" + err.message);
                        return reject(err);
                    }
                    resolve();
                });
            });
        }).then(() => {
            return new Promise((resolve, reject) => {
                updateCache(++stepNumber, stepsCount, 0);

                baseAddonsClass.cleanMemoryCache();
                baseLeaderboardClass.cleanMemoryCache();
                baseReferralsClass.cleanMemoryCache();

                baseLeaderboardClass.initMemoryCache((err) => {
                    if (err) {
                        common.error("PortinManager", "classifyCustomers:initMemoryCache: failed init, err=" + err.message);
                        return reject(err);
                    }

                    if (LOG) common.log("ClassManager", "classifyCustomers:initMemoryCache: leaderboard cached");
                    baseReferralsClass.initMemoryCache((err) => {
                        if (err) {
                            common.error("PortinManager", "classifyCustomers:initMemoryCache: failed init, err=" + err.message);
                            return reject(err);
                        }

                        if (LOG) common.log("ClassManager", "classifyCustomers:initMemoryCache: referrals cached");
                        baseAddonsClass.initMemoryCache("Plus 2020", (err) => {
                            if (err) {
                                common.error("PortinManager", "classifyCustomers:initMemoryCache: failed init, err=" + err.message);
                                return reject(err);
                            }

                            if (LOG) common.log("ClassManager", "classifyCustomers:initMemoryCache: Plus 2020 cached");
                            resolve();
                        })
                    });
                });
            });
        }).then(() => {
            return new Promise((resolve, reject) => {
                updateCache(++stepNumber, stepsCount, 0);
                elitecoreUserService.loadAllUsersBaseInfo({}, (err, result) => {
                    if (err) {
                        common.error("ClassManager", "classifyCustomers:loadAllUsersBaseInfo: failed to load customer information");
                        return reject(err);
                    }

                    if (LOG) common.log("ClassManager", "classifyCustomers:loadAllUsersBaseInfo: loaded " + result.length + " customers");
                    return resolve(result);
                });
            });
        }).then((customerInfo) => {
            return new Promise((resolve, reject) => {
                updateCache(++stepNumber, stepsCount, 0);
                profileManager.loadAllProfilesBaseInfo((err, result) => {
                    if (err) {
                        common.error("ClassManager", "classifyCustomers:loadAllProfilesBaseInfo: failed to load customer profiles");
                        return reject(err);
                    }

                    if (LOG) common.log("ClassManager", "classifyCustomers:loadAllProfilesBaseInfo: loaded " + result.length + " profiles");
                    return resolve({customerInfo: customerInfo, profileInfo: result});
                });
            });
        }).then((info) => {
            return new Promise((resolve, reject) => {
                updateCache(++stepNumber, stepsCount, 0);

                var customerInfoMap = {};
                info.customerInfo.forEach((item) => {
                    customerInfoMap[item.serviceInstanceNumber] = item;
                });

                var customersToUpdate = [];
                var noInfoFound = 0;

                info.profileInfo.forEach((item) => {
                    var customerCache = customerInfoMap[item.serviceInstanceNumber];
                    if (customerCache) {
                        customerCache.profileId = item.id;
                        customersToUpdate.push(customerCache);
                    } else {
                        noInfoFound++;
                        customersToUpdate.push({profileId: item.id});
                    }
                });

                if (LOG) common.log("ClassManager", "classifyCustomers:profilesAnalysis: loaded " +
                customersToUpdate.length + " valid profiles, no info found count=" + noInfoFound);
                return resolve(customersToUpdate);
            });
        }).then((customersToUpdate) => {
            return new Promise((resolve, reject) => {
                updateCache(++stepNumber, stepsCount, 0);

                var asyncTasks = [];
                var tasksProcessed = 0;
                var tasksFailed = 0;
                var tasksProcessedPercents = 0;

                customersToUpdate.forEach((customerItem) => {
                    (function (customerItem) {
                        asyncTasks.push((callback) => {

                            insertCustomerTempDetails(customerItem, (err) => {
                                if (err) {
                                    tasksFailed++;
                                }

                                setTimeout(() => {
                                    tasksProcessed++;
                                    tasksProcessedPercents = parseInt((tasksProcessed / asyncTasks.length) * 100);

                                    if (tasksProcessed % 3000 == 0) {
                                        if (LOG) common.log("ClassManager", "classifyCustomers:syncProfileDetails: processed "
                                        + tasksProcessed + " tasks (" + tasksProcessedPercents + "%), " + tasksFailed + " failed");
                                    }
                                    if (tasksProcessed % 50 == 0) {
                                        updateCache(stepNumber, stepsCount, tasksProcessedPercents);
                                    }

                                    callback();
                                }, 1);
                            });
                        });
                    }(customerItem));
                });

                if (LOG) common.log("ClassManager", "classifyCustomers:syncProfileDetails: processing " + asyncTasks.length
                + " async tasks");

                async.parallelLimit(asyncTasks, 50, (err, result) => {
                    if (LOG) common.log("ClassManager", "classifyCustomers:syncProfileDetails: processed "
                    + tasksProcessed + " tasks (" + tasksProcessedPercents + "%), " + tasksFailed + " failed");

                    updateCache(stepNumber, stepsCount, 100, true);
                    return resolve(customersToUpdate);
                });
            });
        }).then((customersToUpdate) => {
            return new Promise((resolve, reject) => {
                updateCache(++stepNumber, stepsCount, 0);

                loadClassHandlers((err, customerClasses) => {
                    if (err) {
                        common.error("ClassManager", "classifyCustomers:classAnalysis: failed to load class handlers");
                        return reject(err);
                    }

                    if (LOG) common.log("ClassManager", "classifyCustomers:classAnalysis: loaded " + customerClasses.length
                    + " valid class handlers");

                    var asyncTasks = [];
                    var customerProfileClasses = [];

                    var tasksProcessed = 0;
                    var tasksProcessedPercents = 0;

                    customersToUpdate.forEach((customerInfo) => {
                        var customerProfileClass = {
                            serviceInstanceNumber: customerInfo.serviceInstanceNumber,
                            profileId: customerInfo.profileId,
                            classIds: []
                        };

                        customerProfileClasses.push(customerProfileClass);
                        customerClasses.forEach((classHandler) => {
                            (function (customerProfileClass, customerInfo, classHandler) {
                                asyncTasks.push((callback) => {
                                    if (!classHandler || !classHandler.handler || !classHandler.handler.verifyCategory) {
                                        return callback();
                                    }

                                    classHandler.handler.verifyCategory(customerInfo, (err, value, metadata) => {

                                        // in case if handlers are aggregated value will be an array
                                        // we need to save all results

                                        if (metadata && metadata.isArray) {
                                            value.forEach((result) => {
                                                if (result.inClass) {
                                                    customerProfileClass.classIds.push({
                                                        id: result.classId,
                                                        metadata: result.metadata
                                                    });
                                                }
                                            });
                                        } else {
                                            if (value) {
                                                customerProfileClass.classIds.push({
                                                    id: classHandler.id,
                                                    metadata: metadata
                                                });
                                            }
                                        }

                                        // timeout is a protection from handler
                                        // which have only sync operations

                                        setTimeout(() => {
                                            tasksProcessed++;
                                            tasksProcessedPercents = parseInt((tasksProcessed / asyncTasks.length) * 100);

                                            if (tasksProcessed % 4000 == 0) {
                                                if (LOG) common.log("ClassManager", "classifyCustomers:classAnalysis: processed "
                                                + tasksProcessed + " tasks (" + tasksProcessedPercents + "%)");
                                            }
                                            if (tasksProcessed % 200 == 0) {
                                                updateCache(stepNumber, stepsCount, tasksProcessedPercents);
                                            }


                                            return callback();
                                        }, 1);
                                    });
                                });
                            }(customerProfileClass, customerInfo, classHandler));
                        });
                    });

                    if (LOG) common.log("ClassManager", "classifyCustomers:classAnalysis: processing " + asyncTasks.length
                    + " async tasks");

                    async.parallelLimit(asyncTasks, 100, (err, result) => {
                        if (LOG) common.log("ClassManager", "classifyCustomers:classAnalysis: processed "
                        + tasksProcessed + " tasks (" + tasksProcessedPercents + "%)");

                        updateCache(stepNumber, stepsCount, 100, true);
                        return resolve(customerProfileClasses);
                    });
                });
            });
        }).then((customerClasses) => {
            return new Promise((resolve, reject) => {
                updateCache(++stepNumber, stepsCount, 0);

                var asyncTasks = [];
                var tasksProcessed = 0;
                var tasksFailed = 0;
                var tasksProcessedPercents = 0;

                customerClasses.forEach((classItem) => {
                    (function (classItem) {
                        asyncTasks.push((callback) => {

                            deleteOldConnections(classItem, (err) => {
                                if (err) {
                                    tasksFailed++;
                                }

                                tasksProcessed++;
                                tasksProcessedPercents = parseInt((tasksProcessed / asyncTasks.length) * 100);

                                if (tasksProcessed % 3000 == 0) {
                                    if (LOG) common.log("ClassManager", "classifyCustomers:cleaningClasses: processed "
                                    + tasksProcessed + " tasks (" + tasksProcessedPercents + "%), " + tasksFailed + " failed");
                                }
                                if (tasksProcessed % 50 == 0) {
                                    updateCache(stepNumber, stepsCount, tasksProcessedPercents);
                                }

                                callback();
                            });
                        });
                    }(classItem));
                });

                if (LOG) common.log("ClassManager", "classifyCustomers:cleaningClasses: processing " + asyncTasks.length
                + " async tasks");

                async.parallelLimit(asyncTasks, 1, (err, result) => {
                    if (LOG) common.log("ClassManager", "classifyCustomers:cleaningClasses: processed "
                    + tasksProcessed + " tasks (" + tasksProcessedPercents + "%), " + tasksFailed + " failed");

                    updateCache(stepNumber, stepsCount, 100, true);
                    return resolve(customerClasses);
                });
            });
        }).then((customerClasses) => {
            return new Promise((resolve, reject) => {
                updateCache(++stepNumber, stepsCount, 0);

                var tasksProcessed = 0;
                var tasksFailed = 0;
                var tasksProcessedPercents = 0;

                let asyncQ = async.queue((job, cb) => {
                    let profileItem = job.classItem;
                    insertNewConnections(profileItem, (err) => {
                        if (err) {
                            tasksFailed++;
                        }
                        tasksProcessed++;
                        tasksProcessedPercents = parseInt((tasksProcessed / customerClasses.length) * 100);

                        if (tasksProcessed % 3000 == 0) {
                            if (LOG) common.log("ClassManager", "classifyCustomers:savingClasses: processed "
                                + tasksProcessed + " tasks (" + tasksProcessedPercents + "%), " + tasksFailed + " failed");
                        }
                        if (tasksProcessed % 50 == 0) {
                            updateCache(stepNumber, stepsCount, tasksProcessedPercents);
                        }
                        cb();
                    });
                },50);

                asyncQ.drain = () => {
                    return resolve(customerClasses);
                };

                if (LOG) common.log("ClassManager", "classifyCustomers:savingClasses: processing " + customerClasses.length
                    + " tasks");

                //Sorted the customerClasses by profileId
                customerClasses.sort((a,b)=>{
                    return a.profileId - b.profileId;
                });

                customerClasses.forEach((classItem) => {
                    asyncQ.push({classItem});
                });
            });
        }).then(() => {
            return new Promise((resolve, reject) => {
                if (LOG) common.log("ClassManager", "classifyCustomers: save results");
                module.exports.saveCurrentState((err) => {
                    resolve();
                });
            });
        }).then(() => {
            updateCache(stepNumber, stepsCount, 100, true);

            if (LOG) common.log("ClassManager", "classifyCustomers: finished");
            db.unlockOperation(operationKey);

            callback();
        }).catch(function (err) {
            common.error("ClassManager", "classifyCustomers: error=" + (err ? err.message : "Unknown error") + ", status=" + err.status);

            if (!err || err.status != "ERROR_OPERATION_LOCKED") {
                common.error("ClassManager", "classifyCustomers: remove lock");
                db.unlockOperation(operationKey);
            }

            callback(err);
        });
    },

    /**
     *
     */
    saveCurrentState: function (callback) {
        if (!callback) callback = () => {
        }

        module.exports.loadClasses(undefined, (err, classes) => {
            if (err) {
                common.error("ClassManager", "saveCurrentState: failed to load classes, error=" + err.message);
                return callback(err);
            }

            var item = {classes: []};
            var date = new Date(new Date().setHours(4, 0, 0, 0));

            if (classes && classes.data) classes.data.forEach((classItem) => {
                item.classes.push({
                    type: classItem.type,
                    count: classItem.count
                })
            });
            item.ts = date.getTime();

            if (LOG) common.log("ClassManager", "saveCurrentState: date=" + date + ", log=" + JSON.stringify(item));
            db.classifiertats.remove({ts: item.ts}, function (err) {
                db.classifiertats.insert(item, function (err) {
                    callback(undefined, item);
                });
            });
        });
    },

    /**
     *
     */
    classifyCustomersProgress: function (callback) {
        db.cache.get("cache", operationSyncProgressKey, (cache) => {
            callback(undefined, cache ? cache : {});
        });
    },

    /**
     *
     */
    loadClasses: function (id, callback, limit, offset, filter) {
        if (!callback) callback = () => {
        }

        var fields = "*";
        var where = (id ? " AND id=" + db.escape(id) : "") + (filter ? " AND (name LIKE " + db.escape("%" + filter + "%")
            + " OR type LIKE " + db.escape("%" + filter + "%") + ")" : "");

        var q = "SELECT " + fields +
            ", (SELECT count(*) FROM customerProfileClass WHERE classId=customerClass.id) as count " +
            " FROM customerClass WHERE 1=1 " + where + " ORDER BY id DESC" +
            (limit > 0 ? " LIMIT " + db.escape(limit) : "") + (offset > 0 ? " OFFSET " + db.escape(offset) : "");

        db.query_err(q, function (err, rows) {
            if (err) {
                common.error("ClassManager", "loadClasses: failed to load classes, error=" + err.message);
                return callback(err);
            }

            // return single category in case if only 1 is required
            if (id) {
                return callback(undefined, rows ? rows[0] : undefined);
            }

            var selectCount = "SELECT count(*) as count FROM customerClass WHERE 1=1 " + where;
            db.query_err(selectCount, function (err, countResult) {
                if (err) {
                    common.error("ClassManager", "loadClasses: failed to load classes count, error=" + err.message);
                    return callback(err);
                }

                var totalCount = countResult && countResult.length > 0
                    ? countResult[0].count : 0;

                callback(undefined, {
                    totalCount: totalCount,
                    data: rows
                });
            });
        });
    },

    /**
     *
     */
    loadCustomers: function (id, classId, callback, limit, offset, filter) {
        if (!callback) callback = () => {
        }

        var fields = "*";
        var from = "customerProfileClass LEFT JOIN customerProfile ON (customerProfile.id=customerProfileClass.profileId) " +
            " LEFT JOIN customerLastSyncedDetails ON (customerProfile.id=customerLastSyncedDetails.profileId)";
        var where = (id ? " AND customerProfile.id=" + db.escape(id) : "") +
            (filter ? " AND (service_instance_number LIKE " + db.escape("%" + filter + "%") +
            " OR customerLastSyncedDetails.name LIKE " + db.escape("%" + filter + "%") +
            " OR customerLastSyncedDetails.email LIKE " + db.escape("%" + filter + "%") +
            " OR customerLastSyncedDetails.orderReferenceNumber LIKE " + db.escape("%" + filter + "%") +
            " OR customerLastSyncedDetails.billingAccountNumber LIKE " + db.escape("%" + filter + "%") +
            " OR customerLastSyncedDetails.customerAccountNumber LIKE " + db.escape("%" + filter + "%") +
            " OR customerLastSyncedDetails.customerID LIKE " + db.escape("%" + filter + "%") +
            " OR customerLastSyncedDetails.primaryNumber LIKE " + db.escape("%" + filter + "%") + ")" : "") +
            (classId ? " AND customerProfileClass.classId=" + db.escape(classId) : "");

        var q = "SELECT " + fields + " FROM " + from + " WHERE 1=1 " + where + " ORDER BY customerProfile.id DESC" +
            (limit > 0 ? " LIMIT " + db.escape(limit) : "") + (offset > 0 ? " OFFSET " + db.escape(offset) : "");

        db.query_err(q, function (err, rows) {
            if (err) {
                common.error("ClassManager", "loadCustomers: failed to load customers, error=" + err.message);
                return callback(err);
            }

            // return single category in case if only 1 is reqired
            if (id) {
                return callback(undefined, rows ? rows[0] : undefined);
            }

            var selectCount = "SELECT count(*) as count FROM " + from + " WHERE 1=1 " + where;
            db.query_err(selectCount, function (err, countResult) {
                if (err) {
                    common.error("ClassManager", "loadCustomers: failed to load customers count, error=" + err.message);
                    return callback(err);
                }

                var totalCount = countResult && countResult.length > 0
                    ? countResult[0].count : 0;

                var selectCount = "SELECT avg(dataInt1) as dataInt1Average FROM " + from + " WHERE 1=1 " + where;
                db.query_err(selectCount, function (err, averagetResult) {
                    if (err) {
                        common.error("ClassManager", "loadCustomers: failed to load average data 1, error=" + err.message);
                        return callback(err);
                    }

                    var dataInt1Average = averagetResult && averagetResult.length > 0
                        ? averagetResult[0].dataInt1Average : 0;

                    callback(undefined, {
                        dataInt1Average: dataInt1Average,
                        totalCount: totalCount,
                        data: rows
                    });
                });
            });
        });
    }
}

function loadClassHandlers(callback) {
    if (!callback) callback = () => {
    }

    var query = "SELECT id, type FROM customerClass";
    db.query_err(query, function (err, rows) {
        if (err) {
            common.error("ClassManager", "loadClassHandlers: can not load profile from db");
            return callback(err);
        }

        var validClasses = [];
        var invalidTypes = [];

        var aggregatorClasses = {};

        if (rows) rows.forEach((row) => {
            var handler = module.exports.getCategoryHandlers()[row.type];
            if (handler) {
                var customerClass = {
                    id: row.id,
                    type: row.type,
                    handler: handler
                };

                if (handler.aggregationType && handler.aggregationType()) {
                    var aggregatedClass = aggregatorClasses[handler.aggregationType()];
                    if (!aggregatedClass) {
                        aggregatedClass = new ClassProcessingAggregator(handler.aggregationType());
                        aggregatorClasses[handler.aggregationType()] = aggregatedClass;

                        validClasses.push({
                            id: aggregatedClass.id,
                            type: aggregatedClass.type,
                            handler: aggregatedClass
                        });
                    }
                    aggregatedClass.addClassHandler(customerClass);
                } else {
                    validClasses.push(customerClass);
                }
            } else {
                invalidTypes.push(row.type)
            }
        });

        if (invalidTypes.length > 0) {
            common.error("ClassManager", "loadClassHandlers: invalid types " + JSON.stringify(invalidTypes));
        }

        callback(undefined, validClasses);
    });
}

/**
 * This method is taking a profile item, and against all classes this profile belongs to an db insert is done
 *
 * @param profileItem - Profile item of one customer
 * @param callback
 * @returns {*}
 */
function insertNewConnections(profileItem, callback) {

    if (!callback) callback = () => {
    };

    let errors = [];
    let queries = [];
    if (!profileItem.classIds || !profileItem.classIds.length) {
        return callback();
    }

    let queryBegin = `INSERT INTO customerProfileClass (profileId, classId, dataInt1, dataInt2, data1, data2) VALUES `;
    let queryEnd = ` ON DUPLICATE KEY UPDATE dataInt1=VALUES(dataInt1), dataInt2=VALUES(dataInt2), data1=VALUES(data1), data2=VALUES(data2)`;

    let valuesQuery = [];

    //sorting classIds
    profileItem.classIds.sort((a,b)=>{
        return a.id - b.id;
    });

    profileItem.classIds.forEach((classInfo, index) => {
        let metadata = classInfo.metadata ? classInfo.metadata : {};

        let profileId = db.escape(profileItem.profileId);
        let classId = db.escape(classInfo.id);
        let data1 = db.escape(metadata.data1);
        let data2 = db.escape(metadata.data);
        let dataInt1 = db.escape(metadata.dataInt1 >= 0 ? metadata.dataInt1 : 0);
        let dateInt2 = db.escape(metadata.dataInt2 >= 0 ? metadata.dataInt2 : 0);

        valuesQuery.push(`(${profileId}, ${classId}, ${dataInt1}, ${dateInt2}, ${data1}, ${data2})`);
    });

    let finalQuery = queryBegin + valuesQuery.join(',') + queryEnd;

    db.query_err(finalQuery, function (err, rows) {
        if (err) {
            common.error(`ClassManager`, `insertNewConnections: can not save profile-class connection on profile id ${profileItem.profileId}, details, error=${err.message}`);
            return callback(err,undefined);
        }
        callback(undefined,rows);
    });
}

function deleteOldConnections(item, callback) {
    if (!callback) callback = () => {
    }

    var existingClassIds = "";
    if (item.classIds) item.classIds.forEach((classInfo, i) => {
        if (existingClassIds.length > 0) existingClassIds += ',';
        existingClassIds += db.escape(classInfo.id);
    });

    var deleteOld = "DELETE FROM customerProfileClass WHERE profileId=" + db.escape(item.profileId) +
        (existingClassIds.length > 0 ? " AND classId NOT IN (" + existingClassIds + ")" : "");

    db.query_err(deleteOld, function (err, rows) {
        if (err) {
            common.error("ClassManager", "insertNewConnections: can not clean old profile-class connections, error=" + err.message);
            return callback(err);
        }
        callback();
    });
}

function insertCustomerTempDetails(item, callback) {
    if (!callback) callback = () => {
    }

    var primaryNumber = item.serviceInstanceInventoryPrimary ? item.serviceInstanceInventoryPrimary.number : undefined;
    var primaryNumberICCID = item.serviceInstanceInventoryPrimary ? item.serviceInstanceInventoryPrimary.iccid : undefined;
    var primaryNumberIMSI = item.serviceInstanceInventoryPrimary ? item.serviceInstanceInventoryPrimary.imsi : undefined;
    var customerID = item.customerIdentity ? item.customerIdentity.number : undefined;
    var customerIDType = item.customerIdentity ? item.customerIdentity.typeFormatted : undefined;
    var customerBirthday = item.customerBirthday ? item.customerBirthday : 0;
    var customerActivation = item.serviceInstanceCreationDate ? item.serviceInstanceCreationDate : 0;

    var query = "INSERT INTO customerLastSyncedDetails (profileId, customerAccountNumber, serviceInstanceNumber, " +
        "serviceAccountNumber, billingAccountNumber, status, name, email, primaryNumber, primaryNumberICCID, " +
        "primaryNumberIMSI, primaryNumberPorted, customerID, customerIDType, customerBirthday, customerActivation, " +
        "orderReferenceNumber) VALUES (" +
        db.escape(item.profileId) + ", " + db.escape(item.customerAccountNumber) + ", " +
        db.escape(item.serviceInstanceNumber) + ", " + db.escape(item.serviceAccountNumber) + ", " +
        db.escape(item.billingAccountNumber) + ", " + db.escape(item.status) + ", " +
        db.escape(item.billingFullName) + ", " + db.escape(item.billingAccountEmail) + ", " +
        db.escape(primaryNumber) + ", " + db.escape(primaryNumberICCID) + ", " +
        db.escape(primaryNumberIMSI) + ", " + db.escape(item.ported ? 1 : 0) + ", " +
        db.escape(customerID) + ", " + db.escape(customerIDType) + ", " +
        db.escape(customerBirthday) + ", " + db.escape(customerActivation) + ", " +
        db.escape(item.orderReferenceNumber) + ") " +
        " ON DUPLICATE KEY UPDATE " +
        " customerAccountNumber=" + db.escape(item.customerAccountNumber) + "," +
        " serviceInstanceNumber=" + db.escape(item.serviceInstanceNumber) + "," +
        " serviceAccountNumber=" + db.escape(item.serviceAccountNumber) + "," +
        " billingAccountNumber=" + db.escape(item.billingAccountNumber) + "," +
        " status=" + db.escape(item.status) + "," +
        " name=" + db.escape(item.billingFullName) + "," +
        " email=" + db.escape(item.billingAccountEmail) + "," +
        " primaryNumber=" + db.escape(primaryNumber) + "," +
        " primaryNumberICCID=" + db.escape(primaryNumberICCID) + "," +
        " primaryNumberIMSI=" + db.escape(primaryNumberIMSI) + "," +
        " primaryNumberPorted=" + db.escape(item.ported ? 1 : 0) + "," +
        " customerID=" + db.escape(customerID) + "," +
        " customerIDType=" + db.escape(customerIDType) + "," +
        " customerBirthday=" + db.escape(customerBirthday) + "," +
        " customerActivation=" + db.escape(customerActivation) + "," +
        " orderReferenceNumber=" + db.escape(item.orderReferenceNumber);

    db.query_err(query, function (err, rows) {
        if (err) {
            common.error("ClassManager", "insertCustomerTempDetails: can not save customer details, error=" + err.message);
            return callback(err);
        }

        callback();
    });
}