var request = require('request');
var async = require('async');

var config = require('../../../../config');
var common = require('../../../../lib/common');
var db = require('../../../../lib/db_handler');
var elitecoreUserService = require('../../../../lib/manager/ec/elitecoreUserService');
var profileManager = require('../profile/profileManager');
var classManager = require('./classManager');
var notificationSend = require(__core + '/manager/notifications/send');

var BaseError = require('../../errors/baseError');

var log = config.LOGSENABLED;

var operationKey = "sendNotificationToClass";
var operationSendingProgressKey = "sendNotificationToClassProgress";

module.exports = {

    /**
     *
     */
    sendNotificationsProgress: function (callback) {
        db.cache.get("cache", operationSendingProgressKey, (cache) => {
            callback(undefined, cache ? cache : {});
        });
    },

    /**
     *
     */
    sendNotifications: (classId, activity, options, callback) => {
        if (!callback) callback = () => {
        }
        if (!options) options = {}
        var delay = options.delay;
        var emailUnique = options.emailUnique;
        var timeUnique = options.timeUnique;

        if (!classId || !activity) {
            return callback(new BaseError("Invalid Class ID or Notification Activity", "ERROR_INVALID_PARAMS"));
        }
        if (!delay) {
            delay = 3000;
        }

        if (log) common.log("ClassNotificationManager", "sendNotifications: customer loaded, classId=" +
        classId + ", activity=" + activity + ", delay=" + delay + ", timeUnique=" + timeUnique + ", emailUnique=" + emailUnique);

        var updateCache = (stepNumber, stepsCount, stepProgress, end) => {
            db.cache.put("cache", operationSendingProgressKey, {
                stepNumber: stepNumber,
                stepsCount: stepsCount,
                stepProgress: stepProgress
            }, end ? 3000 : 300 * 1000);
        }

        var stepNumber = 0;
        var stepsCount = 4;

        Promise.resolve().then(() => {
            return new Promise((resolve, reject) => {
                updateCache(++stepNumber, stepsCount, 0);
                db.lockOperation(operationKey, {}, 5 * 60 * 1000, function (err) {
                    if (err) {
                        common.error("ClassNotificationManager", "classifyCustomers:lock: failed for start operation, err=" + err.message);
                        return reject(err);
                    }
                    resolve({metadata: {}});
                });
            });
        }).then((info) => {
            return new Promise((resolve, reject) => {
                updateCache(++stepNumber, stepsCount, 0);

                var query = "SELECT activity FROM notifications WHERE activity=" + db.escape(activity);
                db.query_err(query, function (err, result) {
                    if (err) {
                        return reject(err);
                    }

                    if (!result || !result.length) {
                        return reject(new BaseError("Activity '" + activity + "' not found", "ERROR_ACTIVITY_NOT_FOUND"));
                    }

                    var pattern = {
                        teamID: 5,
                        teamName: "Operations",
                        activity: activity,
                        delay: delay,
                        prefix: "",
                        number: "",
                        name: "",
                        email: ""
                    };

                    info.pattern = pattern;
                    resolve(info);
                });
            });
        }).then((info) => {
            return new Promise((resolve, reject) => {
                updateCache(++stepNumber, stepsCount, 0);

                classManager.loadCustomers(undefined, classId, (err, result) => {
                    if (err) {
                        return reject(err);
                    }

                    if (log) common.log("ClassNotificationManager", "sendNotifications: customer loaded, count=" +
                    (result.data ? result.data.length : 0));

                    if (!result.data || !result.data[0]) {
                        return reject(new BaseError("Class '" + classId + "' is empty", "ERROR_EMPTY_CLASS"));
                    }

                    info.customers = result.data;
                    resolve(info);
                });
            });
        }).then((info) => {
            return new Promise((resolve, reject) => {
                updateCache(++stepNumber, stepsCount, 0);

                var tasks = [];
                var expectedCustomerCount = info && info.customers ? info.customers.length : 0;
                var processedCustomerCount = 0;

                var processedEmails = [];
                var errorFetchBatchNotificationCount = 0;
                var errorFetchBatchAccountCount = 0;
                var errorNotificationCount = 0;
                var errorContentCount = 0;
                var emailSkippedCount = 0;
                var ignoredCount = 0;
                var successCount = 0;

                var batch = [];
                var batchSize = 200;

                var sendNotificationBulk = (serviceInstances, callback) => {

                    var startData = new Date().getTime() - (timeUnique ? timeUnique : 0);
                    db.notifications_logbook.find({
                        serviceInstanceNumber: {"$in": serviceInstances},
                        activity: activity,
                        ts: {"$gt": startData}
                    }, {serviceInstanceNumber: 1}).toArray(function (err, result) {
                        if (err) {
                            errorFetchBatchNotificationCount++;
                            return callback();
                        }

                        var ignoreServiceInstances = [];
                        if (result) result.forEach((item) => {
                            ignoreServiceInstances.push(item.serviceInstanceNumber);
                        });

                        elitecoreUserService.loadAllUsersBaseInfo({serviceInstances: serviceInstances}, (err, result) => {
                            if (err) {
                                errorFetchBatchAccountCount++;
                                return callback();
                            }

                            if (!result || !result[0]) {
                                errorFetchBatchAccountCount++;
                                return callback();
                            }

                            var batchProcessedCount = 0;
                            var onNotificationHandled = () => {
                                batchProcessedCount++;
                                processedCustomerCount++;

                                var tasksProcessedPercents = parseInt((processedCustomerCount / expectedCustomerCount) * 100);
                                if (processedCustomerCount % 100 == 0) {
                                    updateCache(stepNumber, stepsCount, tasksProcessedPercents);
                                }
                                if (processedCustomerCount % 400 == 0) {
                                    if (log) common.log("ClassNotificationManager", "classesSendNotification: processed count="
                                    + processedCustomerCount + ", expected count=" + expectedCustomerCount);
                                    updateCache(stepNumber, stepsCount, tasksProcessedPercents);
                                }

                                if (batchProcessedCount == result.length) {
                                    // batch has been processed
                                    return callback();
                                }
                            }

                            result.forEach((cache) => {
                                var notification = common.deepCopy(info.pattern);
                                notification.prefix = cache.prefix;
                                notification.number = cache.number;
                                notification.name = cache.billingFullName;
                                notification.email = cache.email;
                                notification.customerAccountNumber = cache.customerAccountNumber;
                                notification.billingAccountNumber = cache.billingAccountNumber;
                                notification.serviceInstanceNumber = cache.serviceInstanceNumber;

                                // protection from sending an email / push with empty mandatory values

                                if (!notification.email || !notification.number || !notification.name) {
                                    errorContentCount++;
                                    return onNotificationHandled();
                                }

                                // ignore if has been already been sent in a given period of time
                                if (ignoreServiceInstances.indexOf(cache.serviceInstanceNumber) >= 0) {
                                    ignoredCount++;
                                    return onNotificationHandled();
                                }

                                // protection from sending same marketing
                                // message to a person with multiple lines but with the same email

                                if (emailUnique) {
                                    if (processedEmails.indexOf(notification.email) >= 0) {
                                        console.log(notification.email);
                                        emailSkippedCount++;
                                        return onNotificationHandled();
                                    }
                                    processedEmails.push(notification.email);
                                }

                                var onNotificationSent = (err, result) => {
                                    if (err) {
                                        common.error("ClassNotificationManager", "classesSendNotification: " +
                                        "failed to send notification, error=" + err.message);
                                        errorNotificationCount++;
                                        return onNotificationHandled();
                                    } else if (!result.notificationId) {
                                        common.error("ClassNotificationManager", "classesSendNotification: " +
                                        "failed to send notification, empty ID");
                                        errorNotificationCount++;
                                        return onNotificationHandled();
                                    } else {
                                        successCount++;
                                        return onNotificationHandled();
                                    }
                                }

                                notificationSend.deliver(notification, undefined, onNotificationSent, cache);
                            });
                        });
                    });
                }

                var taskWorker = (job, callback) => {
                    sendNotificationBulk(job.serviceInstances, callback);
                }

                var taskResultHandler = (err) => {
                    if (q.isKilled) return;
                    if (err) {
                        q.isKilled = true;
                        q.kill();
                        taskDrainHandler(err);
                    }
                }

                var taskDrainHandler = (err) => {
                    if (err) {
                        return reject(err);
                    }

                    if (log) common.log("ClassNotificationManager", "sendNotification: finished, successCount=" + successCount +
                    ", errorNotificationCount=" + errorNotificationCount + ", errorContentCount=" + errorContentCount +
                    ", emailSkippedCount=" + emailSkippedCount + ", errorFetchBatchAccountCount=" + errorFetchBatchAccountCount +
                    ", errorFetchBatchNotificationCount=" + errorFetchBatchNotificationCount + ", ignoredCount=" + ignoredCount +
                    ", processedCustomerCount=" + processedCustomerCount + ", expectedCustomerCount=" + expectedCustomerCount);

                    return resolve(undefined, {
                        errorFetchBatchAccountCount: errorFetchBatchAccountCount,
                        errorFetchBatchNotificationCount: errorFetchBatchNotificationCount,
                        errorNotificationCount: errorNotificationCount,
                        emailSkippedCount: emailSkippedCount,
                        ignoredCount: ignoredCount,
                        successCount: successCount
                    });
                }

                var q = async.queue(taskWorker, 5);
                q.drain = taskDrainHandler;

                info.customers.forEach((item) => {
                    batch.push(item.serviceInstanceNumber);
                    if (batch.length >= batchSize) {
                        q.push({serviceInstances: batch}, taskResultHandler);
                        batch = [];
                    }
                });
                if (batch.length > 0) {
                    q.push({serviceInstances: batch}, taskResultHandler);
                    batch = [];
                }
            });
        }).then(() => {
            updateCache(stepNumber, stepsCount, 100, true);

            if (log) common.log("ClassNotificationManager", "sendNotification: finished");
            db.unlockOperation(operationKey);

            callback();
        }).catch(function (err) {
            common.error("ClassNotificationManager", "sendNotification: error="
            + (err ? err.message : "Unknown error") + ", status=" + err.status);

            if (!err || err.status != "ERROR_OPERATION_LOCKED") {
                common.error("ClassManager", "sendNotification: remove lock");
                db.unlockOperation(operationKey);
            }

            callback(err);
        });
    }
}
