var db = require('../../../../lib/db_handler');
var common = require('../../../../lib/common');
var config = require('../../../../config');

var SG_PREFIX = "65";
var LOG = config.LOGSENABLED;

module.exports = {

    removeAppDevices : function (app, prefix, number, callback) {
        var appCond = (app == "all") ?
            "app_type IN ('gentwo', 'selfcare')" :
            "app_type = " + db.escape(app);
        var numCond = "number=" + db.escape(prefix + "" + number);
        if (Array.isArray(number)) {
            if (number.length == 0) return callback(new Error("Empty number list"));
            var numbers = "";
            number.forEach(function(num) { numbers += db.escape(prefix + "" + num) + "," });
            numCond = "number IN (" + numbers.slice(0,-1) + ")";
        }
        var query = "DELETE FROM app WHERE " + numCond + " AND " + appCond;
        if (LOG) common.log("removeAppDevices", query);
        db.query_noerr(query, function(rows) {
            if (rows) callback(undefined, rows.affectedRows);
            else callback(new Error("RemoveAppDevices Query Error"));
        });
    },

    removeDevice : function (user_key, callback) {
        var query = "DELETE FROM device WHERE user_key=" + db.escape(user_key);
        if (LOG) common.log("removeDevice", query);
        db.query_noerr(query, function(rows) {
            if (rows) callback(undefined, rows.affectedRows);
            else callback(new Error("RemoveDevice Query Error"));
        });
    },

    getDevices: function(number, prefix, appType, callback) {
        if (!prefix) prefix = SG_PREFIX;
        var fullNumber = prefix + "" + number;
        var cond = "";
        if ( appType ) {
            cond = " AND a.app_type=" + appType + " ";
        } 
        var q = "SELECT a.app_type, d.device_id, d.status, d.type token_type, d.token, d.vapn, d.flavor, d.user_key FROM app a, device d WHERE a.id=d.app_id " + cond + " AND a.number=" + fullNumber;	
        if (LOG) common.log("getDevices", q);
        db.query_noerr(q, function(rows) {
            callback(rows);
        });
    },

    gentwo: {

        plans: function(callback) {
            db.query_noerr("SELECT planID, name FROM plans", function (rows) {
                callback(undefined, rows);
            });
        },

        setPlan: function(number, plan, callback) {
            if (!plan) {
                if (callback) return callback();
                else return;
            }
            db.query_noerr("UPDATE msisdn SET plan_id=" + db.escape(plan) + " WHERE " +
                    " number=" + db.escape(number), function (rows) {
                if (callback) callback(undefined, rows);
            });
        },

        getPlanCredit: function(number, callback) {
            var q = "SELECT m.plan_id, m.credit, c.vsUSD, m.credit * c.vsUSD sgd FROM msisdn m, currencies c WHERE " +
                " c.currencyCode='SGD' AND number=" + db.escape(number);
            if (LOG) common.log("getPlanCredit", q);
            db.query_noerr(q, function (rows) {
                if (!rows || !rows[0]) {
                    callback(undefined, { "credit" : 0, "sgd" : 0 });
                } else {
                    callback(undefined, { "plan_id" : rows[0].plan_id, "credit" : rows[0].credit, "sgd" : rows[0].sgd });
                }
            });
        },

        setCredit: function(number, amount, callback) {
            if (!amount) {
                if (callback) return callback();
                else return;
            }
            var q = "UPDATE msisdn set credit=credit+" + db.escape(amount) + " WHERE number=" + db.escape(number);
            if (LOG) common.log("setCredit", q);
            db.query_noerr(q, function(result) {
                if (callback) callback(undefined, result);
            });
        }
    }
}
