var async = require('async');
var fs = require('fs');
var common = require('../../../../lib/common');
var db = require('../../../../lib/db_handler');

var groups = {};

module.exports = {

    /**
     *
     *
     */
    isBetaCustomer: function (customerId, groupName, callback) {
        if (!callback) callback = () => {
        }

        var query = "SELECT * FROM betaCustomers JOIN betaGroups ON betaGroups.id=betaCustomers.groupId " +
            "WHERE betaCustomers.customerId=" + db.escape(customerId) + " AND betaGroups.name=" + db.escape(groupName);

        db.query_err(query, function (err, rows) {
            if (err) {
                return callback(err);
            }

            callback(undefined, rows.length > 0);
        });
    }
}