var request = require('request');
var async = require('async');

var config = require('../../../../config');
var common = require('../../../../lib/common');
var db = require('../../../../lib/db_handler');
var ec = require('../../../../lib/elitecore');
var bonusTypesService = require('../../../../res/bonus/bonusTypesService');
var resourceManager = require('../../../../res/resourceManager');
var configManager = require('../config/configManager');
var boostManager = require('../boost/boostManager');
var profileManager = require('../profile/profileManager');
var leaderboardManager = require('./leaderboardManager');

var BaseError = require('../../errors/baseError');

var LOG = config.LOGSENABLED;

let Q = require('q');

var PRETTIFIED_KIB = {
    "0": 0,
    "51200": 50000,
    "102400": 100000,
    "153600": 150000,
    "204800": 200000,
    "256000": 250000,
    "307200": 300000,
    "409600": 400000,
    "512000": 500000,
    "614400": 600000,
    "716800": 700000,
    "768000": 750000,
    "819200": 800000,
    "921600": 900000,
    "1048576": 1000000,
    "1572864": 1500000,
    "2097152": 2000000,
    "2621440": 2500000,
    "3145728": 3000000,
    "3670016": 3500000,
    "4194304": 4000000,
    "5242880": 5000000,
    "6291456": 6000000,
    "7340032": 7000000,
    "8388608": 8000000,
    "9437184": 9000000,
    "10485760": 10000000,
    "20971520": 20000000,
    "41943040": 40000000,
    "62914560": 60000000,
    "83886080": 80000000,
    "104857600": 100000000
}

module.exports = {

    /**
     *
     */
    bonusEarnerSoFar: (instanceNum) => {
        let query = `SELECT added_ts, bonus_kb_pretty, display_until_ts FROM historyBonuses 
        WHERE service_instance_number = ${db.escape(instanceNum)}`;
        
        return Q.nfcall(db.query_err, query);
    },
    loadTotalGivenBonus: function (callback) {
        if (!callback) {
            callback = () => {
            }
        }
        var query = "SELECT sum(bonus_kb_pretty) / 1000000 as givenGb FROM historyBonuses";
        if (LOG) common.log("BonusManager", "query=" + query);

        db.query_err(query, function (err, result) {
            if (err) {
                return callback(err);
            }

            if (!result || !result.length) {
                return callback(new Error("Failed to load total given bonus"));
            }

            var givenGb = parseFloat(result[0].givenGb);
            givenGb = Math.round(givenGb * 100) / 100;

            callback(undefined, {
                value: givenGb,
                unit: "GB"
            })
        });
    },

    /**
     *
     */
    prettifyKB: function (kb) {
        var prettyKb = PRETTIFIED_KIB[kb + ""];
        return prettyKb ? prettyKb : 0;
    },

    /**
     *
     */
    loadCacheAndRegistrationDate: function (prefix, number, bonusType, callback) {
        if (!callback) callback = () => {
        }

        ec.getCustomerDetailsNumber(prefix, number, false, function (err, cache) {
            if (err) {
                return callback(new BaseError("EC load error (" + err.message + ")", "EC_ERROR"), cache);
            }
            if (!cache) {
                return callback(new BaseError("User not found via EC DB", "USER_NOT_FOUND"));
            }

            var checkBonus = (joinDate, cache) => {
                if (!bonusType) {
                    return callback(undefined, cache, joinDate, false);
                }

                var query = "SELECT * FROM historyBonuses WHERE " +
                    "service_instance_number=" + db.escape(cache.serviceInstanceNumber) + " AND " +
                    "bonus_type=" + db.escape(bonusType);

                db.query_noerr(query, function (result) {
                    var bonusPresented = result && result.length > 0;
                    return callback(undefined, cache, joinDate, bonusPresented);
                });
            }

            var logsCb = function (logs) {
                var joinDate = (logs && logs.length > 0) ? new Date(logs[0].ts) : undefined;

                if (!joinDate) {
                    joinDate = new Date(cache.serviceInstanceCreationDate);
                }
                if (!joinDate || !joinDate.getTime()) {
                    joinDate = new Date();
                }

                checkBonus(joinDate, cache);
            }

            if (!cache.orderReferenceNumber) {
                return logsCb();
            }

            db.notifications_logbook_get({
                order_reference_number: cache.orderReferenceNumber,
                activity: "payment_successful"
            }, {}, {"ts": -1}, logsCb);
        });
    },

    /**
     *
     */
    loadRegistrationDateByORN: function (orderReferenceNumber, activationDate, callback) {
        db.notifications_logbook_get({
                order_reference_number: orderReferenceNumber,
                activity: "payment_successful"
            },
            {}, {"ts": -1}, function (logs) {
                var joinDate = logs && logs.length > 0 ? new Date(logs[0].ts) : new Date(activationDate);
                var referralCode = logs && logs.length > 0 ? logs[0].refferal_code : "";

                if (callback) {
                    callback(undefined, joinDate, referralCode);
                }
            });
    },

    /**
     *
     */
    addSelfcareInstallationBonus: function (prefix, number, newUser, ignoreDuplicates, executionKey, callback) {
        db.cache_lock("cache", "selfcare_add_" + number, 1, 15 * 60 * 1000, function (lock) {
            if (!lock) {
                if (LOG) common.log("addSelfcareInstallationBonus", "Selfcare bonus is locked");
                return callback(new BaseError("Operation locked", "OPERATION_LOCKED"));
            } else {
                configManager.loadConfigMapForKey("bonus", "bonus_selfcare_install", function (error, item) {
                    if (error) {
                        return callback(error);
                    }

                    if (!item.options.enabled) {
                        if (LOG) common.log("BonusManager", "Care Installation bonus disabled");
                        return callback(new BaseError("Disabled in Settings", "DISABLED"));
                    }

                    ec.getCustomerDetailsNumber(prefix, number, false, function (err, cache) {
                        if (err) {
                            return callback(new BaseError("EC load error (" + err.message + ")", "USER_NOT_FOUND"), cache);
                        }
                        if (!cache) {
                            return callback(new BaseError("User not found in EC", "USER_NOT_FOUND"));
                        }

                        var query = "SELECT * FROM historyBonuses WHERE service_instance_number=" + db.escape(cache.serviceInstanceNumber) +
                            " AND bonus_type='selfcare_install'";
                        if (LOG) common.log("BonusManager", "query=" + query);

                        db.query_noerr(query, function (result) {
                            if (result && result.length > 0 && !ignoreDuplicates) {
                                return callback(new BaseError("User already has 'selfcare_install' bonus", "BONUS_ALREADY_ADDED"));
                            }

                            db.notifications_logbook_get({
                                    number: number,
                                    activity: "payment_successful"
                                },
                                {}, {"ts": 1}, function (logs) {
                                    var productId;
                                    var joinDate = logs && logs.length > 0 ? new Date(logs[0].ts)
                                        : (new Date(cache.serviceInstanceCreationDate));

                                    if (item && item.options) {
                                        productId = item.options.bonus_product_starting_m4;
                                    } else {
                                        productId = undefined;
                                    }

                                    module.exports.addBonus({
                                        noneRecurrentBonusId: productId,
                                        continueRecurrent: true,
                                        continueMonth: -1,
                                        prefix: prefix,
                                        number: number,
                                        executionKey: executionKey,
                                        metadata: undefined,
                                        bonusSubType: "1",
                                        waitForResponse: true
                                    }, function (err, result) {
                                        var response = {};
                                        response["bonusResult"] = {
                                            err: err ? err.message : null,
                                            id: "selfcareInstallationBonus",
                                            result: result
                                        }

                                        callback(undefined, response);
                                    });
                                });
                        });
                    });
                });
            }
        });
    },

    /**
     *
     */
    add1GBSelfcareInstallationBonus: function (prefix, number, executionKey, callback) {
        module.exports.addBonus({
            noneRecurrentBonusId: "PRD00479",
            continueRecurrent: true,
            continueMonth: -1,
            prefix: prefix,
            number: number,
            executionKey: executionKey,
            metadata: undefined,
            bonusSubType: "1",
            waitForResponse: true
        }, function (err, response) {
            callback(err, response);
        });
    },

    /**
     *
     */
    addPortInBonus: function (prefix, number, executionKey, callback) {
        configManager.loadConfigMapForKey("bonus", "port_in_bonus", function (error, item) {
            if (error) {
                return callback(error);
            }

            if (!item.options.enabled) {
                if (LOG) common.log("BonusManager", "Port-In bonus disabled");
                return callback(new BaseError("Disabled in Settings", "DISABLED"));
            }

            module.exports.loadCacheAndRegistrationDate(prefix, number, "portin",
                function (err, cache, joinedDate, bonusPresented) {
                    if (err) {
                        return callback(err);
                    }
                    if (!cache) {
                        return callback(new BaseError("User not found in EC", "USER_NOT_FOUND"));
                    }

                    if (bonusPresented) {
                        var errMessage = "Port In Day Bonus already presented";
                        return callback(new Error(errMessage), {
                            added: false,
                            reason: errMessage
                        });
                    }

                    var productId = item.options.bonus_product_starting_M13;
                    module.exports.addBonus({
                        noneRecurrentBonusId: productId,
                        continueRecurrent: true,
                        continueMonth: -1,
                        prefix: prefix,
                        number: number,
                        executionKey: executionKey,
                        metadata: undefined,
                        bonusSubType: "1",
                        waitForResponse: true
                    }, function (err, response) {
                        callback(err, response);
                    });
                }, true);
        });
    },

    /**
     *
     */
    addLoyaltyBonus: function (prefix, number, executionKey, callback) {
        db.cache_lock("cache", "loyalty_add_" + number, 1, 15 * 60 * 1000, function (lock) {
            if (!lock) {
                if (LOG) common.log("BonusManager", "Loyalty bonus is locked");
                return callback(undefined, {added: false, reason: 'locked'});
            } else {
                ec.getCustomerDetailsNumber(prefix, number, false, function (err, cache) {
                    if (!cache) {
                        return callback(new Error("Customer not found (" + number + ")"));
                    }
                    var now = new Date();
                    var activation = new Date(cache.serviceInstanceCreationDate);
                    var milestoneMonth = now.getMonth() - activation.getMonth()
                        + (12 * (now.getFullYear() - activation.getFullYear()));
                    module.exports.addLoyaltyBonusSI(cache.serviceInstanceNumber, milestoneMonth, executionKey, callback);
                });
            }
        });
    },

    addLoyaltyBonusSI: function (serviceInstanceNumber, milestoneMonth, executionKey, callback) {
        db.cache_lock("cache", "loyalty_add_" + serviceInstanceNumber, 1, 15 * 60 * 1000, function (lock) {
            if (!lock) {
                if (LOG) common.log("BonusManager", "Loyalty bonus is locked");
                return callback(undefined, {added: false, reason: 'locked'});
            } else {
                configManager.loadConfigMapForKey("bonus", "loyalty_bonus", function (error, item) {
                    if (error) {
                        return callback(error);
                    }

                    if (!item.options.enabled) {
                        if (LOG) common.log("BonusManager", "Loyalty bonus disabled");
                        return callback(undefined, {added: false, reason: 'not enabled'});
                    }

                    var productId = item && item.options ? item.options.bonus_product : undefined;
                    module.exports.addBonus({
                        noneRecurrentBonusId: productId,
                        continueRecurrent: true,
                        continueMonth: -1,
                        serviceInstanceNumber: serviceInstanceNumber,
                        bonusSubType: milestoneMonth,
                        executionKey: executionKey,
                        metadata: {
                            data1: ("" + milestoneMonth)
                        },
                        waitForResponse: true
                    }, function (err, response) {
                        callback(err, response);
                        if (!err || (err && (err.message == "ETIMEDOUT"))) {
                            if (milestoneMonth == 6 ) {
                                sendNotificationSI(serviceInstanceNumber, undefined, "six_month_customer", undefined, undefined, executionKey, () => {
                                });
                            } else if (milestoneMonth == 12) {
                                sendNotificationSI(serviceInstanceNumber, undefined, "one_year_customer", undefined, undefined, executionKey, () => {
                                });
                            } else if (milestoneMonth == 24) {
                                sendNotificationSI(serviceInstanceNumber, undefined, "two_year_customer", undefined, undefined, executionKey, () => {
                                });
                            }
                        }
                    });
                });
            }
        });
    },

    addRetentionBonusSI: function (serviceInstanceNumber, milestoneMonth, options, executionKey, callback) {
        db.cache_lock('cache', 'retention_add_' + serviceInstanceNumber, 1, 15 * 60 * 1000, function (lock) {
            if (!lock) {
                if (LOG) common.log('BonusManager', 'Retention bonus is locked');
                return callback(undefined, {added: false, reason: 'locked'});
            } else {
                configManager.loadConfigMapForKey('bonus', 'retention_bonus', function (error, item) {
                    if (error) {
                        return callback(error);
                    }
                    if (!item.options.enabled) {
                        if (LOG) common.log('BonusManager', 'Retention bonus disabled');
                        return callback(undefined, {added: false, reason: 'not enabled'});
                    }
                    let continueMonth = 1;
                    let continueRecurrent = false;
                    if (parseInt(options.continueMonth)) {
                        continueMonth = parseInt(options.continueMonth);
                    }

                    if (typeof (options.continueRecurrent) === 'Boolean') {
                        continueRecurrent = options.continueRecurrent;
                    }
                    var productId = item && item.options ? item.options.retention_product : undefined;
                    module.exports.addBonus({
                        noneRecurrentBonusId: productId,
                        continueRecurrent: continueRecurrent,
                        continueMonth: continueMonth,
                        serviceInstanceNumber: serviceInstanceNumber,
                        bonusSubType: milestoneMonth,
                        executionKey: executionKey,
                        metadata: {
                            data1: ('' + milestoneMonth)
                        },
                        waitForResponse: true
                    }, function (err, response) {
                        return callback(err, response);
                    });
                });
            }
        });
    },

    /**
     *
     */
    addBirthdayBonus: function (prefix, number, executionKey, callback) {
        db.cache_lock("cache", "birthday_add_" + number, 1, 15 * 60 * 1000, function (lock) {
            if (!lock) {
                if (LOG) common.log("BonusManager", "Adding of birthday bonus is locked");
                return callback(undefined, {added: false, reason: 'locked'});
            } else {
                ec.getCustomerDetailsNumber(prefix, number, false, function (err, cache) {
                    if (!cache) {
                        return callback(new Error("Customer not found (" + number + ")"));
                    }
                    module.exports.addBirthdayBonusSI(cache.serviceInstanceNumber, executionKey, callback);
                });
            }
        });
    },

    addBirthdayBonusSI: function (serviceInstanceNumber, executionKey, callback) {
        db.cache_lock("cache", "birthday_add_" + serviceInstanceNumber, 1, 15 * 60 * 1000, function (lock) {
            if (!lock) {
                if (LOG) common.log("BonusManager", "Adding of birthday bonus is locked");
                return callback(undefined, {added: false, reason: 'locked'});
            } else {
                configManager.loadConfigMapForKey("bonus", "birthday_bonus", function (error, item) {
                    if (error) {
                        return callback(error);
                    }

                    if (!item.options.enabled) {
                        if (LOG) common.log("BonusManager", "Adding of birthday bonus is disabled");
                        return callback(undefined, {added: false, reason: 'not enabled'});
                    }

                    var productId = item && item.options ? item.options.bonus_product : undefined;
                    module.exports.addAvailableBonusSI(productId, item.options.valid_days, serviceInstanceNumber, executionKey, function (err, result) {
                        return callback(err, result);
                    });
                });
            }
        });
    },

    /**
     *
     */
    addSurpriseBonus: function (prefix, number, productId, executionKey, callback) {
        module.exports.addAvailableBonus(productId, 90, prefix, number, executionKey, function (err, result) {
            return callback(err, result);
        });
    },

    /**
     *
     */
    addAvailableBonus: function (productId, validDays, prefix, number, executionKey, callback) {
        db.cache_lock("cache", "available_add_" + number + "_" + productId, 1, 15 * 60 * 1000, function (lock) {
            if (!lock) {
                if (LOG) common.log("addAvailableBonus", "addAvailableBonus is locked");
                return callback(new BaseError("Operation locked", "OPERATION_LOCKED"));
            } else {
                ec.getCustomerDetailsNumber(prefix, number, false, function (err, cache) {
                    if (err || !cache) {
                        return callback(new BaseError("User is not found in db", "USER_NOT_FOUND"));
                    }
                    module.exports.addAvailableBonusSI(productId, validDays, cache.serviceInstanceNumber, executionKey, callback);
                });
            }
        });
    },

    addAvailableBonusSI: function (productId, validDays, serviceInstanceNumber, executionKey, callback) {
        db.cache_lock("cache", "available_add_" + serviceInstanceNumber + "_" + productId, 1, 15 * 60 * 1000, function (lock) {
            if (!lock) {
                if (LOG) common.log("addAvailableBonus", "addAvailableBonus is locked");
                return callback(new BaseError("Operation locked", "OPERATION_LOCKED"));
            } else {
                var product = ec.findPackage(ec.packages(), productId);
                if (!product) {
                    return callback(new BaseError("Can not find product", "PRODUCT_NOT_FOUND"));
                }
                var bonus_type = product.app ? db.escape(product.app) : "null";
                var bonus_sub_type = (product.app == "birthday") ? db.escape(new Date().getFullYear()) : "null"
                var insertQuery = "INSERT INTO availableBonuses (bonus_product_id, service_instance_number, valid_until_ts, bonus_type, bonus_sub_type) VALUES " +
                    "(" + db.escape(productId) + ", " + db.escape(serviceInstanceNumber) +
                    ", " + db.escape(new Date(new Date().getTime() + validDays * 24 * 60 * 60 * 1000).toISOString()) + "," + bonus_type + "," + bonus_sub_type + ")";
                db.query_err(insertQuery, function (err, rows) {
                    if (err || !rows || !rows.affectedRows) {
                        if (LOG) common.log("BonusManager", "bonus is not added into db");
                        return callback(new BaseError("Bonus is not added into db", "DB_ERROR"));
                    }

                    if (executionKey) {
                        if (product.name.indexOf("Birthday") >= 0) {
                            sendNotificationSI(serviceInstanceNumber, product, "bonus_birthday_provided", undefined, undefined, executionKey,
                                function (err) {
                                    return callback(undefined, {added: rows.affectedRows});
                                });
                        } else if (product.name.indexOf("Surprise") >= 0) {
                            sendNotificationSI(serviceInstanceNumber, product, "bonus_provided", undefined, undefined, executionKey,
                                function (err) {
                                    return callback(undefined, {added: rows.affectedRows});
                                });
                        } else {
                            return callback(undefined, {added: rows.affectedRows});
                        }
                    } else {
                        return callback(undefined, {added: rows.affectedRows});
                    }
                });
            }
        });
    },

    /**
     *
     */
    loadAvailableBonusList: function (prefix, number, validOnly, callback) {
        if (!number || !prefix) {
            return callback(new BaseError("Params are incorrect", "PARAMS_ERROR"));
        }

        ec.getCustomerDetailsNumber(prefix, number, false, function (err, cache) {
            if (err || !cache) {
                return callback(new BaseError("User is not found", "USER_NOT_FOUND"));
            }

            var query;
            if (validOnly) {
                query = "SELECT id, bonus_product_id, added_ts, valid_until_ts FROM availableBonuses WHERE " +
                "service_instance_number=" + db.escape(cache.serviceInstanceNumber) + " AND used=0 AND processing=0 " +
                "AND valid_until_ts>" + db.escape(new Date()) + " ORDER BY added_ts";
            } else {
                query = "SELECT id, bonus_product_id, used, processing, added_ts, used_ts, valid_until_ts FROM availableBonuses WHERE " +
                "service_instance_number=" + db.escape(cache.serviceInstanceNumber) + " ORDER BY added_ts";
            }

            if (LOG) common.log("BonusManager loadAvailableBonusList", "query=" + query);
            db.query_noerr(query, function (result) {
                var items = [];
                if (result && result.length > 0) {
                    result.forEach(function (item) {
                        var product = ec.findPackage(ec.packages(), item.bonus_product_id);
                        if (product) {
                            if (validOnly) {
                                items.push({
                                    id: item.id,
                                    product_id: product.id,
                                    product_kb: product.kb,
                                    product_name: product.name,
                                    added_date: item.added_ts,
                                    valid_until: item.valid_until_ts
                                });
                            } else {
                                items.push({
                                    id: item.id,
                                    product_id: product.id,
                                    product_kb: product.kb,
                                    product_name: product.name,
                                    used: item.used,
                                    processing: item.processing,
                                    added_ts: item.added_ts,
                                    used_ts: item.used_ts,
                                    valid_until_ts: item.valid_until_ts
                                });
                            }
                        }
                    });
                }
                return callback(undefined, items);
            });
        });
    },

    /**
     *
     */
    useAvailableBonus: function (prefix, number, id, callback) {
        db.cache_lock("cache", "available_use_" + number + "_" + id, 1, 15 * 60 * 1000, function (lock) {
            if (!lock) {
                if (LOG) common.log("useAvailableBonus", "useAvailableBonus is locked");
                return callback(new BaseError("Operation locked", "OPERATION_LOCKED"));
            } else {
                if (!number || !prefix || !id) {
                    return callback(new BaseError("Params are incorrect", "PARAMS_ERROR"));
                }
                ec.getCustomerDetailsNumber(prefix, number, false, function (err, cache) {
                    if (err || !cache) {
                        return callback(new BaseError("User is not found", "USER_NOT_FOUND"));
                    }
                    module.exports.useAvailableBonusSI(cache.serviceInstanceNumber, id, callback);
                });
            }
        });
    },

    /**
     *
     */
    useAvailableBonusSI: function (serviceInstanceNumber, id, callback) {
        db.cache_lock("cache", "available_use_" + serviceInstanceNumber + "_" + id, 1, 15 * 60 * 1000, function (lock) {
            if (!lock) {
                if (LOG) common.log("useAvailableBonus", "useAvailableBonus is locked");
                return callback(new BaseError("Operation locked", "OPERATION_LOCKED"));
            } else {
                if (!serviceInstanceNumber || !id) {
                    return callback(new BaseError("Params are incorrect", "PARAMS_ERROR"));
                }
                var query = "SELECT * FROM availableBonuses WHERE id=" + db.escape(parseInt(id));
                if (LOG) common.log("BonusManager", "query=" + query);

                db.query_noerr(query, function (result) {
                    if (!result || !result[0]) {
                        if (LOG) common.log("BonusManager", "bonus is not found in db");
                        return callback(new BaseError("Bonus is not found in db", "NOT_FOUND"));
                    }

                    var item = result[0];
                    if (item.used) {
                        if (LOG) common.log("BonusManager", "already used");
                        return callback(new BaseError("Already used", "ALREADY_USED"));
                    } else if (item.processing) {
                        if (LOG) common.log("BonusManager", "processing");
                        return callback(new BaseError("Processing", "PROCESSING"));
                    }

                    var product = ec.findPackage(ec.packages(), item.bonus_product_id);
                    if (!product) {
                        if (LOG) common.log("BonusManager", "product not found, product_id=" + item.boost_product_id);
                        return callback(new BaseError("Product not found", "PRODUCT_NOT_FOUND_EXCEEDED"));
                    }
                    var bonusSubType = (product.app == 'birthday') ? new Date().getFullYear() : undefined;
                    var updateQuery = "UPDATE availableBonuses SET processing=1 WHERE id=" + db.escape(parseInt(id));
                    if (LOG) common.log("BonusManager", "updateQuery=" + updateQuery);

                    db.query_err(updateQuery, function (err, result) {
                        if (err) {
                            if (LOG) common.error("BonusManager", "can not update current item in db, err.code=" + err.code);
                            return callback(new BaseError("Can not update current item in db", "CAN_NOT_UPDATE"));
                        }

                        module.exports.addBonus({
                            noneRecurrentBonusId: product.id,
                            continueRecurrent: false,
                            continueMonth: 1,
                            serviceInstanceNumber: serviceInstanceNumber,
                            executionKey: undefined,
                            bonusSubType: bonusSubType,
                            metadata: undefined,
                            waitForResponse: false
                        }, function (err, response) {
                            if (err) {
                                common.error("BonusManager", "failed to subscribe for an addon, id=" + product.id + " error=" + err.message);
                            } else {
                                if (LOG) common.log("BonusManager", "succeed to subscribe for an addon, " +
                                "product_id=" + product.id);
                            }
                            var updateQuery = "UPDATE availableBonuses SET processing=0, used=1, used_ts="
                                + db.escape(new Date().toISOString()) + " WHERE id=" + db.escape(parseInt(id));
                            if (LOG) common.log("BonusManager", "updateQuery=" + updateQuery);

                            ec.getCustomerDetailsServiceInstance(serviceInstanceNumber, true, function (err, userCache) {
                            });

                            db.query_err(updateQuery, function () {
                                return callback(undefined, {
                                    name: product.name,
                                    product_id: product.id,
                                    kb: product.kb
                                });
                            });
                        });
                    });
                });
            }
        });
    },

    /**
     *
     */
    addSingleRecurrentBonus: function (serviceInstanceNumber, bonusId, toDate, bonusApp, activity, executionKey, metadata, callback) {
        var product = bonusId ? ec.findPackage(ec.packages(), bonusId) : undefined;
        if (!product) {
            return callback(new BaseError("Product is not found, productId " + bonusId));
        }
        if (!bonusApp) {
            return callback(new BaseError("Notification type is not specified"));
        }
        if (!toDate) {
            return callback(new BaseError("End date is not specified", "NO_END_DATE"));
        }

        // we need to make a deep copy to modify product,
        // it is forbidden to modify original object
        var product = common.deepCopy(product);
        product.app = bonusApp;

        ec.getCustomerDetailsServiceInstance(serviceInstanceNumber, false, function (err, userCache) {
            if (err) {
                return callback(new BaseError(err));
            }
            if (!userCache) {
                return callback(new BaseError("User is not found", "USER_NOT_FOUND"));
            }

            var prefix = userCache.prefix;
            var number = userCache.number;
            var sin = userCache.serviceInstanceNumber;
            var billingCycle = userCache.billing_cycle;

            //ignore time and specify the last sec of the day in SGT
            toDate = new Date(toDate.setHours(15, 59, 59, 0));

            var sub = {
                "product_id": product.id,
                "recurrent": true,
                "effect": 0,
                "toDate": toDate
            };

            if (LOG) common.log("BonusManager", "subscribe single recurrent addon, name=" + product.name +
            ", sub=" + JSON.stringify(sub));

            var subscriptionSucceed = function (subscriptionResult, notificationResult) {
                ec.setCache(number, prefix, undefined, userCache.account, () => {
                    callback(null, {
                        added: true,
                        productId: product.id,
                        subscriptionResult: subscriptionResult,
                        notificationResult: notificationResult
                    });
                });
            }

            saveHistory(product, prefix, number, sin, billingCycle, false, undefined, toDate, undefined, metadata, () => {
                if (err) {
                    common.error("BonusManger", "failed to save history for a R bonus, error=" + err.message);
                    return callback(err);
                }

                ec.addonSubscribe(userCache.number, sin, [sub], userCache.billing_cycle, (err, result) => {
                    if (err) {
                        common.error("BonusManger", "failed to subscribe a R bonus, error=" + err.message);
                        return callback(err);
                    }

                    if (LOG) common.log("BonusManager", "R bonus subscribed, sin=" + sin + ", type=" + bonusApp);
                    if (!activity) {
                        return subscriptionSucceed(result);
                    }

                    sendNotification(prefix, number, product, activity, undefined, undefined, executionKey, (err, notifResult) => {
                        return subscriptionSucceed(result, err ? {error: err.message} : notifResult);
                    });
                });
            });
        });
    },

    /**
     *
     */
    addSingleNonRecurrentBonus: function (serviceInstanceNumber, bonusId, bonusApp, activity, executionKey, metadata, callback) {
        if (!callback) callback = () => {
        }

        var product = bonusId ? ec.findPackage(ec.packages(), bonusId) : undefined;
        if (!product) {
            return callback(new Error("Product is not found, productId " + bonusId));
        }
        if (!bonusApp) {
            return callback(new Error("Notification type is not specified"));
        }

        // we need to make a deep copy to modify product,
        // it is forbidden to modify original object
        var product = common.deepCopy(product);
        product.app = bonusApp;

        ec.getCustomerDetailsServiceInstance(serviceInstanceNumber, false, function (err, userCache) {
            if (err) {
                return callback(err);
            }
            if (!userCache) {
                return callback(new BaseError("User is not found", "USER_NOT_FOUND"));
            }

            var prefix = userCache.prefix;
            var number = userCache.number;
            var billingCycle = userCache.billing_cycle;
            var sin = userCache.serviceInstanceNumber;

            //ignore time and specify the last sec of the day in SGT
            var date = new Date();
            var toDate = new Date(date.getFullYear(), date.getMonth() + 1, 0);
            toDate = new Date(toDate.setHours(15, 59, 59, 0));
            var sub = {
                "product_id": product.id,
                "recurrent": false,
                "effect": 0,
                "toDate": toDate
            };

            if (LOG) common.log("BonusManager", "subscribe single non-R addon, name=" + product.name +
            ", sub=" + JSON.stringify(sub));

            var subscriptionSucceed = function (subscriptionResult, notificationResult) {
                ec.setCache(number, prefix, undefined, userCache.account, function (err, result) {
                    callback(null, {
                        added: true,
                        productId: product.id,
                        subscriptionResult: subscriptionResult,
                        notificationResult: notificationResult
                    });
                });
            }

            saveHistory(product, prefix, number, sin, billingCycle, false, undefined, toDate, undefined, metadata, (err) => {
                if (err) {
                    common.error("BonusManger", "failed to save history for a non-R bonus, error=" + err.message);
                    return callback(err);
                }

                if (LOG) common.log("BonusManager", "bonus history saved, sin=" + sin + ", type=" + product.app);
                ec.addonSubscribe(number, sin, [sub], billingCycle, (err, result) => {
                    if (err) {
                        common.error("BonusManger", "failed to subscribe a non-R bonus, error=" + err.message);
                        return callback(err);
                    }

                    if (LOG) common.log("BonusManager", "non-R bonus subscribed, sin=" + sin + ", type=" + product.app);
                    if (!activity) {
                        return subscriptionSucceed(result);
                    }

                    sendNotification(prefix, number, product, activity, undefined, undefined, executionKey, (err, notifResult) => {
                        return subscriptionSucceed(result, err ? {error: err.message} : notifResult);
                    });
                });
            });
        });
    },

    /**
     *
     */
    getCodeBonusProduct: function (bonusData, bonusDataType, duration, bonusProductId) {
        var bonusProduct;

        if (bonusData) {
            if (bonusData == 512000) {
                bonusProduct = ec.findPackageName(ec.packages(), "Bonus 500MB Common");
            } else if (bonusData == 1048576) {
                bonusProduct = ec.findPackageName(ec.packages(), "Bonus 1GB Common");
            } else if (bonusData == 1572864) {
                bonusProduct = ec.findPackageName(ec.packages(), "Bonus 1.5GB Common");
            } else if (bonusData == 2097152) {
                bonusProduct = ec.findPackageName(ec.packages(), "Bonus 2GB Common");
            } else if (bonusData == 20971520) {
                bonusProduct = ec.findPackageName(ec.packages(), "Bonus 20GB Common");
            } else {
                return callback(new Error("BonusData " + bonusData + " is not supported recurrent"));
            }

            if (!bonusProduct) {
                return undefined;
            }

            // override bonus value to save correct data
            // value even if bonus has incorrect kb value

            bonusProduct.kb = bonusData;
            bonusProduct.originalProduct = false;
        } else if (bonusProductId) {
            bonusProduct = ec.findPackage(ec.packages(), bonusProductId);

            if (!bonusProduct) {
                return undefined;
            }

            bonusProduct.originalProduct = true;
        } else {
            return undefined;
        }

        bonusProduct.prettyData = common.prettifyKBUnit(bonusProduct.kb);
        bonusProduct.dataPrefix = bonusProduct.prettyData.value + " " + bonusProduct.prettyData.unit;

        // update bonus type,
        // existing will be overridden

        if (bonusDataType) {
            bonusProduct.app = bonusDataType;
            bonusProduct.name = "Bonus " + bonusProduct.prettyData.value + bonusProduct.prettyData.unit;
        }

        // load product bonus info,  that info might be used to check
        // if we need to send notification after adding bonus

        if (bonusProduct && bonusProduct.app) {
            var bonusDataInfo = bonusTypesService.getBonusInfo(bonusProduct.app);
            if (bonusDataInfo && bonusDataInfo.type != "default") {
                var promoTitle = resourceManager.getString(bonusDataInfo.titleId);

                bonusProduct.promoTitle = promoTitle;
                bonusProduct.activity = bonusDataInfo.activity;
                bonusProduct.overrideBssNotification = bonusDataInfo.overrideBssNotification;
                bonusProduct.title = promoTitle;

                if (!bonusProduct.originalProduct) {
                    bonusProduct.name = (bonusProduct.name ? bonusProduct.name + " " : "") + promoTitle;
                }
            }
        }

        bonusProduct.recurrentHistory = duration == -1;
        bonusProduct.monthCount = duration > 0 ? duration : 0;

        if (bonusProduct.name.indexOf(' R ') > -1 || bonusProduct.recurrentHistory) {
            bonusProduct.recurrentSuffix = '/mo forever';
        } else if (bonusProduct.monthCount == 1) {
            bonusProduct.recurrentSuffix = '/mo for 1 month';
        } else if (bonusProduct.monthCount > 1) {
            bonusProduct.recurrentSuffix = '/mo for ' + bonusProduct.monthCount + ' months';
        } else {
            bonusProduct.recurrentSuffix = '';
        }

        if (bonusProduct.dataPrefix && bonusProduct.recurrentSuffix) {
            bonusProduct.publicTitle = bonusProduct.dataPrefix + bonusProduct.recurrentSuffix +
            " (" + (bonusProduct.promoTitle ? bonusProduct.promoTitle + " " : "") + "Bonus)";
        }

        return bonusProduct;
    },

    /**
     *
     */
    addBonus: function (options, callback) {
        var noneRecurrentBonusId = options.noneRecurrentBonusId;
        var continueRecurrent = options.continueRecurrent;
        var continueMonth = options.continueMonth;
        var prefix = options.prefix;
        var number = options.number;
        var serviceInstanceNumber = options.serviceInstanceNumber;
        var executionKey = options.executionKey;
        var metadata = options.metadata;
        var waitForResponse = options.waitForResponse;
        var bonusData = options.bonusData;
        var bonusDataType = options.bonusDataType;
        var bonusSubType = options.bonusSubType;
        var noDataHistoryItem = options.noDataHistoryItem;
        var getCache = function (cb) {
            if (serviceInstanceNumber) {
                ec.getCustomerDetailsServiceInstance(serviceInstanceNumber, false, cb);
            } else if (prefix && number) {
                ec.getCustomerDetailsNumber(prefix, number, false, cb);
            } else {
                cb(new BaseError("User is not found", "USER_NOT_FOUND"));
            }
        };

        if (continueRecurrent) {
            continueMonth = -1;
        } else if (continueMonth < 1) {
            if (LOG) common.log("BonusManager", "continueMonth set to default value 1");
            continueMonth = 1;
        } else if (continueMonth > 12) {
            if (LOG) common.log("BonusManager", "continueMonth > " + continueMonth + ", it is not allowed");
            continueMonth = 12;
        }

        // if bonus data is provided it has higher priority over product,
        // product will be ignored, eventually product not supposed to be used

        // NOTE: bonus product can be modified due to being deep copy of an original bonus
        // that will not affect original business hierarchy

        var bonusProduct = module.exports.getCodeBonusProduct(bonusData, bonusDataType,
            continueRecurrent ? -1 : continueMonth, noneRecurrentBonusId);
        var bonusDataInfo = bonusProduct ? bonusTypesService.getBonusInfo(bonusProduct.app) : undefined;

        if (!bonusProduct) {
            return callback(new Error("Product is not found, productId " + noneRecurrentBonusId));
        } else if (bonusProduct.name.indexOf(" R ") >= 0) {
            return callback(new Error("Product is already recurrent, productId " + noneRecurrentBonusId));
        }

        if (noDataHistoryItem) {
            bonusProduct.kb = 0;
        }

        var saveBonusHistory = function (prefix, number, callback) {
            getCache(function (err, userCache) {
                if (err || !userCache) {
                    return callback(err);
                }
                saveHistory(bonusProduct, userCache.prefix, userCache.number, userCache.serviceInstanceNumber,
                    userCache.billing_cycle, continueRecurrent, continueMonth, undefined, bonusSubType,
                    metadata, (err) => {
                        return callback(err);
                    });
            });
        }

        var subscribeBonus = function (originalProduct, extraMonth, historyDuration, product, prefix, number, callback) {
            getCache(function (err, userCache) {
                if (err || !userCache) {
                    return callback(err);
                }
                var name = product.name;
                var sub;
                if (originalProduct) {
                    sub = {
                        "product_id": product.id,
                        "recurrent": false,
                        "effect": 0
                    };
                } else if (extraMonth) {
                    sub = {
                        "product_id": product.id,
                        "recurrent": false,
                        "effect": 2
                    };
                } else {
                    sub = {
                        "product_id": product.id,
                        "recurrent": true,
                        "effect": 2
                    };
                }

                var resultSent = false;
                var attempt = 0;

                if (LOG) common.log("BonusManager", "subscribe addon, name=" + name + ", sub=" + JSON.stringify(sub));
                ec.addonSubscribe(userCache.number, userCache.serviceInstanceNumber, [sub], userCache.billing_cycle, (err, result) => {
                    if (!err) {
                        ec.setCache(userCache.number, userCache.prefix, undefined, userCache.account, () => {
                        });
                    }

                    if (resultSent) {
                        if (LOG) common.log("BonusManager", "ignored ec response, notification response was returned faster, name=" + name);
                        return;
                    }
                    resultSent = true;

                    if (err) {
                        return callback(err);
                    }
                    if (!result) {
                        return callback(new BaseError("Subscription failed", "ERROR_BSS_EMPTY_RESPONSE"));
                    }
                    if (originalProduct && !extraMonth) {
                        module.exports.scheduleAggregation(userCache.prefix, userCache.number,
                            "[CMS][BonusManager]", executionKey);
                    }

                    callback(null, {
                        added: true,
                        number: userCache.number,
                        account: userCache.account,
                        productId: product.id,
                        subscriptionResult: result
                    });
                });

                if (!waitForResponse) {
                    var checkNotification = function () {
                        db.cache_get("last_subscription_cache", userCache.number, function (addonname) {
                            if (LOG) common.log("BonusManager", "checking, attempt=" + attempt + ", number=" + userCache.number +
                            ", addonname=" + addonname);

                            if (resultSent) {
                                if (LOG) common.log("BonusManager", "ignored, ec response was returned faster, name=" + name);
                                return;
                            }

                            if (addonname == name) {
                                resultSent = true;

                                callback(null, {
                                    added: true,
                                    number: userCache.number,
                                    account: userCache.account,
                                    productId: product.id,
                                    subscriptionResult: {
                                        reason: "Notification received for product name " + addonname
                                        + ", no reason to wait for API response"
                                    }
                                });

                            } else if (attempt < 60) {
                                setTimeout(checkNotification, 300);
                                attempt++;
                            }
                        });
                    };

                    if (LOG) common.log("BonusManager", "start checking subscription result, account=" + userCache.account);
                    checkNotification();
                }
            });
        }

        var handleSubscriptionResult = (bonusHistoryErr, mainProductError, result) => {
            if (bonusHistoryErr) {
                return callback(bonusHistoryErr, {
                    added: false,
                    result: {
                        error: bonusHistoryErr.message,
                        subscription: result
                    }
                });
            }

            // in case if subscription of main product fails bonus data can still
            // be recovered because of "recovery script" we run at night


            if (mainProductError) {

                // we may need to add notification to scheduler if adding of bonus has failed
                // so next morning after it is recovered we can deliver it to the customer

                return callback(mainProductError, {
                    added: true,
                    result: {
                        error: mainProductError.message,
                        subscription: result
                    }
                });
            }

            // notification to customer should be sent only
            // if bonus history is saved and bonus for current month is added
            if (bonusProduct && bonusDataInfo && bonusDataInfo.activity && bonusDataInfo.overrideBssNotification) {
                sendNotification(prefix, number, bonusProduct, bonusDataInfo.activity, undefined, undefined, executionKey, (err) => {
                    if (err) common.error("BonusManager", "Failed to send notification, error=" + err.message);
                });
            }

            return callback(undefined, {
                added: true,
                result: {
                    subscription: result
                }
            });
        }

        var response = {};
        saveBonusHistory(prefix, number, function (bonusHistoryErr, bonusHistoryResult) {
            if (bonusHistoryErr) {
                return handleSubscriptionResult(bonusHistoryErr);
            }

            response.bonusHistoryAdded = true;
            response.bonusHistoryResult = bonusHistoryResult;

            if (!bonusProduct.kb) {
                return handleSubscriptionResult(undefined, undefined, response);
            }

            if (continueRecurrent) {
                subscribeBonus(true, false, -1, bonusProduct, prefix, number,
                    function (originalBonusErr, originalBonusResult) {
                        response.originalBonusAdded = originalBonusErr ? false : true;
                        response.originalBonusResult = originalBonusErr ? originalBonusErr.message : originalBonusResult;

                        return handleSubscriptionResult(undefined, originalBonusErr, response);
                    });

            } else if (continueMonth > 0) {
                subscribeBonus(true, false, continueMonth, bonusProduct, prefix, number,
                    function (originalBonusErr, originalBonusResult) {
                        response.originalBonusAdded = originalBonusErr ? false : true;
                        response.originalBonusResult = originalBonusErr ? originalBonusErr.message : originalBonusResult;

                        // actual maximum is 12 month,
                        // bonus data would be recovered after first 2 month

                        if (!continueMonth || continueMonth <= 1) {
                            return handleSubscriptionResult(undefined, originalBonusErr, response);
                        }

                        subscribeBonus(false, true, continueMonth, bonusProduct, prefix, number, (extraBonusErr, extraBonusResult) => {
                            response.extraBonusAdded = extraBonusResult ? false : true;
                            response.extraBonusResult = extraBonusResult ? extraBonusResult.message : extraBonusResult;

                            return handleSubscriptionResult(undefined, originalBonusErr, response);
                        });
                    });
            } else {
                return handleSubscriptionResult(new Error("Invalid bonus configuration, (continueRecurrent=" +
                continueRecurrent + ", continueMonth=" + continueMonth + ")"));
            }
        });
    },

    /**
     *
     */
    loadHistoryBonusList: function (prefix, number, validOnly, callback) {
        if (!number || !prefix) {
            return callback(new BaseError("Params are incorrect", "PARAMS_ERROR"));
        }

        ec.getCustomerDetailsNumber(prefix, number, false, function (err, cache) {
            if (err || !cache) {
                return callback(new BaseError("User is not found", "USER_NOT_FOUND"));
            }

            module.exports.loadHistoryBonusListBySI(cache.serviceInstanceNumber, validOnly, callback);
        });
    },

    /**
     *
     */
    loadHistoryBonusListBySI: function (serviceInstanceNumber, validOnly, callback) {
        var currentDate = new Date();
        var query = "SELECT id, bonus_type, bonus_sub_type, bonus_kb, bonus_kb_pretty, added_ts, display_until_ts, data1, data2, data3, " +
            " data4, data5, data6, data7, dataInt1, deactivated FROM historyBonuses WHERE service_instance_number="
            + db.escape(serviceInstanceNumber) + (validOnly ? " AND added_ts <= NOW() AND deactivated=" + db.escape(0)
            + " AND (display_until_ts >" + db.escape(currentDate) + " OR display_until_ts = '')" : "") + " ORDER BY added_ts DESC";

        if (LOG) common.log("BonusManager loadHistoryBonusListBySI", "query=" + query);
        db.query_err(query, function (err, result) {
            if (err) {
                return callback(err);
            }

            var items = [];
            var leaderboardDataKb = 0;
            if (result && result.length > 0) {
                result.forEach(function (item) {

                    // a workaround to show proper image on the app
                    if (item.bonus_type === "promo_extra_portin") {
                        item.bonus_type = "portin";
                    }

                    var active = item.deactivated == 0 && (item.display_until_ts === "0000-00-00 00:00:00"
                        || new Date(item.display_until_ts).getTime() > currentDate.getTime()) &&
                        (new Date(item.added_ts).getTime() <= new Date().getTime());

                    var info = bonusTypesService.getBonusInfo(item.bonus_type);
                    if (!info) info = {};

                    var obj = {
                        id: item.id,
                        product_kb: item.bonus_kb,
                        product_kb_pretty: item.bonus_kb_pretty,
                        product_type: item.bonus_type,
                        product_sub_type: item.bonus_sub_type,
                        added_date: item.added_ts,
                        deactivated: item.deactivated,
                        active: active,
                        title: resourceManager.getString(info.titleId),
                        imageId: info.imageId
                    }

                    if (item.display_until_ts !== "0000-00-00 00:00:00") {
                        obj["valid_until"] = item.display_until_ts;
                        obj["leaderboard_included"] = false;
                    } else {
                        leaderboardDataKb += item.bonus_kb_pretty;
                        obj["leaderboard_included"] = true;
                    }

                    if (item.data1 && item.data1 != null) obj["metadata1"] = item.data1;
                    if (item.data2 && item.data2 != null) obj["metadata2"] = item.data2;
                    if (item.data3 && item.data3 != null) obj["metadata3"] = item.data3;
                    if (item.data4 && item.data4 != null) obj["metadata4"] = item.data4;
                    if (item.data5 && item.data5 != null) obj["metadata5"] = item.data5;
                    if (item.data6 && item.data6 != null) obj["metadata6"] = item.data6;
                    if (item.data7 && item.data7 != null) obj["metadata7"] = item.data7;
                    if (item.dataInt1 && item.dataInt1 != null) obj["metadataInt1"] = item.dataInt1;
                    if (item.dataInt2 && item.dataInt2 != null) obj["metadataInt2"] = item.dataInt2;
                    if (item.dataInt3 && item.dataInt3 != null) obj["metadataInt3"] = item.dataInt3;
                    if (item.dataDate1 && item.dataDate1 != null) obj["metadataDate1"] = item.dataDate1;
                    items.push(obj);
                });
            }

            leaderboardManager.loadLeaderboardRatingBySI(serviceInstanceNumber, function (err, leaderboardInfo) {
                if (err) {
                    return callback(err);
                }

                return callback(undefined, items, leaderboardInfo);
            });
        });
    },

    /**
     *
     */
    deleteBonusHistory: function (prefix, number, id, callback) {
        var selectQuery = "DELETE FROM historyBonuses WHERE id=" + db.escape(id);
        db.query_err(selectQuery, function (err) {
            if (err) {
                if (LOG) common.error("ReferralManger", "can not delete history, DB error, err=" + err);
                return callback(new BaseError("Can not delete history", "DB_ERROR"));
            }

            var notificationKey = config.ELITECORE_KEY;
            leaderboardManager.updateRating(notificationKey, function (err) {
                // do nothing
            });

            callback();
        });
    },

    /**
     *
     */
    scheduleAggregation: function (prefix, number, initiator, executionKey, callback) {
        var job = db.queue.create('aggregation', {
            prefix: prefix,
            number: number,
            initiator: initiator,
            executionKey: executionKey
        }).events(false).removeOnComplete(true).delay(0).attempts(3).backoff({
            delay: 10 * 1000,
            type: "fixed"
        }).save(function (err, result) {
            if (err) {
                if (LOG) common.log("BonusManager", "bonus aggregation failed");
            } else {
                if (LOG) common.log("BonusManager", "bonus aggregation succeed");
            }

            if (callback) callback(err, result);
        });
    },

    /**
     *
     */
    saveHistoryItem: function (prefix, number, bonusType, bonusKb, serviceInstanceNumber,
                               displayUntil, bonusSubType, metadata, initiator, executionKey, callback) {
        saveProductHistory(prefix, number, bonusType, bonusKb, serviceInstanceNumber,
            displayUntil, bonusSubType, metadata, (err, result) => {
                module.exports.scheduleAggregation(prefix, number, initiator, executionKey);
                callback(err, result);
            });
    }
}

function saveHistory(bonusProduct, prefix, number, serviceInstanceNumber, billingCycle, continueRecurrent, historyDuration,
                     historyEndDate, bonusSubType, metadata, callback) {

    var bonusType = bonusProduct.app;
    var bonusKb = bonusProduct.kb;
    var serviceInstanceNumber = serviceInstanceNumber;
    var displayUntil;

    if (historyEndDate) {
        displayUntil = historyEndDate;
    } else if (!continueRecurrent) {
        displayUntil = ec.nextBill(billingCycle).endDateUTC;
        for (var i = 1; i < historyDuration; i++) {
            displayUntil = ec.nextBill(billingCycle,
                new Date(displayUntil.getTime() + 24 * 60 * 60 * 1000)).endDateUTC;
        }
    }

    if (displayUntil && displayUntil.getTime()) {
        displayUntil = new Date(displayUntil.getTime() - 60 * 1000);
    }

    saveProductHistory(prefix, number, bonusType, bonusKb,
        serviceInstanceNumber, displayUntil, bonusSubType, metadata, callback);
}

function saveProductHistory(prefix, number, bonusType, bonusKb, serviceInstanceNumber, displayUntil, bonusSubType, metadata, callback) {
    if (!bonusKb) bonusKb = 0;
    if (!bonusType) bonusType = "unknown";

    var notificationKey = config.ELITECORE_KEY;
    var bonusKbPretty = PRETTIFIED_KIB["" + bonusKb];
    if (!bonusKbPretty) bonusKbPretty = 0;

    if (LOG) common.log("BonusManager", "save bonus history, bonusType=" + bonusType + ", bonusKb=" + bonusKb
    + ", bonusKbPretty=" + bonusKbPretty + ", serviceInstanceNumber=" + serviceInstanceNumber
    + ", displayUntil=" + displayUntil + ", metadata=" + JSON.stringify(metadata));

    var variables = "service_instance_number, bonus_type, bonus_kb, bonus_kb_pretty, bonus_sub_type";
    var values = db.escape(serviceInstanceNumber) + "," + db.escape(bonusType) + ", "
        + db.escape(bonusKb) + ", " + db.escape(bonusKbPretty) + ", " + (bonusSubType ? db.escape(bonusSubType) : "NULL");

    if (displayUntil) {
        variables += ", display_until_ts";
        values += ", " + db.escape(displayUntil.toISOString());
    }

    if (metadata) {
        for (var i = 1; i <= 10; i++) {
            var key = "data" + i;
            if (metadata[key]) {
                variables += ", " + key;
                values += ", " + db.escape(metadata[key]);
            }
        }
        for (var i = 1; i <= 3; i++) {
            var key = "dataInt" + i;
            if (metadata[key]) {
                variables += ", " + key;
                values += ", " + db.escape(metadata[key]);
            }
        }
        for (var i = 1; i <= 1; i++) {
            var key = "dataDate" + i;
            if (metadata[key]) {
                variables += ", " + key;
                values += ", " + db.escape(metadata[key]);
            }
        }
    }

    var insertQuery = "INSERT INTO historyBonuses (" + variables + ") VALUES " + "(" + values + ")";
    if (LOG) common.log("BonusManager", "save bonus history query, insertQuery=" + insertQuery);

    db.query_err(insertQuery, function (err, rows) {
        if (err) {
            if (err.code == "ER_DUP_ENTRY") {
                return callback(new BaseError("Duplicate of a bonus history (owner:" +
                serviceInstanceNumber + ", type:" + bonusType + ", subtype:" + bonusSubType + ")", "ERROR_DUPLICATE_BONUS_HISTORY"));
            }
            if (LOG) common.log("BonusManager", "bonus history is not saved into db, error=" + err.message);
            if (callback) callback(err);
        } else if (!rows) {
            if (LOG) common.log("BonusManager", "bonus history is not saved into db, empty response");
            if (callback) callback(new Error("Invalid result"));
        } else {
            if (callback) callback();
        }

        // run after calling callback
        // so rating update would not slow down main operation

        profileManager.loadASelfcareSetting(prefix, number, "golden_circles", undefined, (err, type, value) => {
            if (err) {
                return;
            }

            if (value) {
                if (bonusType == 'referral') {

                    // check current referral count if customer
                    // is already in golden circles club

                    leaderboardManager.addGoldenTicketIfEntitledBySI(serviceInstanceNumber, {waiver: true}, (err) => {
                        if (err) {
                            if (LOG) common.log("BonusManager", "failed to add Nth golden ticket, error=" + err.message);
                            return;
                        }
                    });
                }

                // no need to update leaderboard rating,
                // users from golden circle does not affect it

            } else {
                leaderboardManager.loadLeaderboardRatingBySI(serviceInstanceNumber, (err, result) => {
                    if (err) {
                        if (LOG) common.log("BonusManager", "failed to load customer rating, error=" + err.message);
                        return;
                    }

                    if (result.dataGb < leaderboardManager.getGoldenCircleConfig().dataGB) {

                        // customer has not reached limitations,
                        // no need to move him to golden circles board

                        leaderboardManager.updateRating(notificationKey, function () {
                            // do nothing
                        });
                        return;
                    }

                    profileManager.saveSetting(prefix, number, "selfcare", "golden_circles", "Boolean", true, notificationKey, (err) => {
                        if (err) {
                            if (LOG) common.log("BonusManager", "failed to save golden ticket satting, error=" + err.message);
                            return;
                        }

                        // after first entering golden circle board hi / she is
                        // entitled for one golden ticket the same month

                        leaderboardManager.addGoldenTicket(serviceInstanceNumber, {waiver: true, firstTicket: true}, (err) => {
                            if (err) {
                                if (LOG) common.log("BonusManager", "failed to add 1st golden ticket, error=" + err.message);
                                return;
                            }

                            // leaderboard rating should be updated after saving golden tickets setting that will generate
                            // proper notification to customer, stating he / she is in golden circle leaderboard

                            leaderboardManager.updateRating(notificationKey, function (err) {
                                // do nothing
                            });
                        });
                    });
                });
            }
        });

        sendBonusProgressNotification(prefix, number, config.OPERATIONS_KEY, function () {
            // do nothing
        });

        if (bonusType === 'referral') {
            sendReferred3Notification(prefix, number, config.OPERATIONS_KEY, function () {
                // do nothing
            });
        }
    });
}

function sendReferred3Notification(prefix, number, executionKey, callback) {
    module.exports.loadHistoryBonusList(prefix, number, true, function (err, items, rating) {
        if (err) {
            return callback(err)
        }

        if (items) {
            var referralCount = 0;
            items.forEach(function (item) {
                if (item.product_type === 'referral') {
                    referralCount++;
                }
            });
        }

        var send = (referralCount == 3);
        if (LOG) common.log("BonusManager", "send 'referred 3' notification, send="
        + send + ", referralCount=" + referralCount);

        if (send) {
            profileManager.saveSetting(prefix, number, "selfcare", "leaderboard_3_referrals", "Integer", 1, executionKey,
                function (err, result) {
                    if (err) {
                        return callback(err)
                    }

                    sendNotification(prefix, number, undefined, 'leaderboard_referred_three', undefined, undefined,
                        executionKey, function (err) {
                            callback(err);
                        });
                });
        }
    });
}

function sendBonusProgressNotification(prefix, number, executionKey, callback) {
    var notifications = [{
        category: "leaderboard_progress",
        activity: "leaderboard_prize_near_5gb",
        valueGB: 3.5,
        targetGB: 5
    }, {
        category: "leaderboard_progress",
        activity: "leaderboard_prize_near_15gb",
        valueGB: 13,
        targetGB: 15
    }, {
        category: "leaderboard_progress",
        activity: "leaderboard_prize_near_30gb",
        valueGB: 28,
        targetGB: 30
    }, {
        category: "leaderboard_progress",
        activity: "leaderboard_prize_near_50gb",
        valueGB: 48,
        targetGB: 50
    }, {
        category: "leaderboard_progress",
        activity: "leaderboard_prize_near_70gb",
        valueGB: 68,
        targetGB: 70
    }, {
        category: "leaderboard_progress",
        activity: "leaderboard_prize_3gb",
        valueGB: 3
    }, {
        category: "leaderboard_progress",
        activity: "leaderboard_prize_5gb",
        valueGB: 5
    }, {
        category: "leaderboard_progress",
        activity: "leaderboard_prize_15gb",
        valueGB: 15
    }, {
        category: "leaderboard_progress",
        activity: "leaderboard_prize_30gb",
        valueGB: 30
    }, {
        category: "leaderboard_progress",
        activity: "leaderboard_prize_50gb",
        valueGB: 50
    }, {
        category: "leaderboard_progress",
        activity: "leaderboard_prize_70gb",
        valueGB: 70
    }];

    var sendPromoNotifications = function (currentGb, lastHandledGb) {

        var lbNotification;
        notifications.forEach(function (item) {
            if (item.valueGB > lastHandledGb && item.valueGB <= currentGb
                && (!lbNotification || item.valueGB >= lbNotification.valueGB)) {
                lbNotification = item;
            }
        });

        if (LOG) common.log("BonusManager", "need to send, notification=" + (lbNotification ? lbNotification.activity : "NONE"));

        profileManager.saveSetting(prefix, number, "selfcare", "leaderboard_notification_kb",
            "Integer", parseInt(currentGb * 1000 * 1000), executionKey, function (err, result) {
                if (err) {
                    return callback(err)
                }

                if (lbNotification) {
                    sendNotification(prefix, number, undefined, lbNotification.activity,
                        lbNotification.targetGB > 0 ? Math.round((lbNotification.targetGB - currentGb) * 10) / 10 : undefined,
                        lbNotification.targetGB > 0 ? "GB" : undefined, executionKey, function (err) {
                            callback(err);
                        });

                } else {
                    callback();
                }
            });
    }

    module.exports.loadHistoryBonusList(prefix, number, true, function (err, items, rating) {
        if (err) {
            return callback(err)
        }

        profileManager.loadASelfcareSetting(prefix, number, "leaderboard_notification_kb",
            executionKey, function (err, type, value) {
                if (err) {
                    return callback(err)
                }

                var currentGb = rating.dataGb;
                var lastHandledGb = value ? Math.round((value / 1000 / 1000) * 10) / 10 : 0;

                if (LOG) common.log("BonusManager", "checked leaderboard notification, number=" + number
                + ", currentGb=" + currentGb + ", lastHandledGb=" + lastHandledGb);
                sendPromoNotifications(currentGb, lastHandledGb);
            });
    });
}

function sendNotification(prefix, number, product, activity, leftValue, leftUnit, notificationKey, callback) {
    ec.getCustomerDetailsNumber(prefix, number, false, function (err, cache) {
        if (err || !cache) {
            return callback(new BaseError("User is not found", "USER_NOT_FOUND"));
        }
        sendNotificationSI(cache.serviceInstanceNumber, product, activity, leftValue, leftUnit, notificationKey, callback)
    });
}

function sendNotificationSI(serviceInstanceNumber, product, activity, leftValue, leftUnit, notificationKey, callback) {
    var url = "http://" + config.MYSELF + ":" + config.WEBPORT +
        "/api/1/webhook/notifications/internal/" + notificationKey;

    var value;
    var unit;
    var productName;
    var productTitle;
    var recurrentsuffix;

    if (product) {
        var prettifiedData = common.prettifyKBUnit(product.kb);
        productName = product.name;
        productTitle = product.title;
        value = Math.round(prettifiedData.value * 10) / 10;
        unit = prettifiedData.unit;

        if (product.name.indexOf(' R ') > -1 || product.recurrentHistory) {
            recurrentsuffix = ' /mo';
        } else if (product.monthCount == 1) {
            recurrentsuffix = '/1 month';
        } else if (product.monthCount > 1) {
            recurrentsuffix = '/' + product.monthCount + ' months';
        } else {
            recurrentsuffix = '';
        }
    }

    request({
        uri: url,
        method: 'POST',
        timeout: 20000,
        json: {
            activity: activity,
            serviceInstanceNumber: serviceInstanceNumber,
            variables: {
                value: value,
                unit: unit,
                left_value: leftValue,
                left_unit: leftUnit,
                addonname: productName,
                title: productTitle,
                recurrentsuffix: recurrentsuffix,
                recurrentSuffix: recurrentsuffix
            }
        }
    }, function (err, response, body) {
        if (err) {
            return callback(err);
        }
        if (callback) callback(undefined, body);
    });
}


