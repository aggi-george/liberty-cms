var request = require('request');
var async = require('async');

var config = require('../../../../config');
var common = require('../../../../lib/common');
var db = require('../../../../lib/db_handler');
var ec = require('../../../../lib/elitecore');
var BaseError = require("../../errors/baseError");

var profileManager = require('../profile/profileManager');
var waiverManager = require('./waiverManager');

var LOG = config.LOGSENABLED;
var LIMIT = 5;

var queryGoldenCircles = "SELECT service_instance_number FROM customerOptions " +
    " LEFT JOIN customerProfile ON (customerOptions.profile_id = customerProfile.id) " +
    " WHERE customerOptions.option_key='golden_circles' AND customerOptions.app_type = 'selfcare' " +
    " AND customerOptions.value='1'";

module.exports = {

    /**
     *
     */
    getGoldenCircleConfig: function () {
        return {
            dataGB: 150,
            ticketReferrals: 30,
            waiverCents: 2800
        };
    },

    /**
     *
     */
    loadLeaderboard: function (callback) {
        if (LOG) common.log("LeaderboardManager", "load leaderboard");

        var queryLeaders = "SELECT count(*) peopleCount, (totalBonus / 1000 / 1000) as bonusDataGb, " +
            " GROUP_CONCAT(service_instance_number) as serviceInstanceNumbers FROM (SELECT service_instance_number, " +
            " sum(bonus_kb_pretty) as totalBonus FROM historyBonuses " +
            " WHERE service_instance_number NOT IN (" + queryGoldenCircles + ") AND display_until_ts=0 AND deactivated=0" +
            " GROUP BY service_instance_number) as userBonus GROUP BY totalBonus " +
            " ORDER BY bonusDataGb DESC LIMIT " + db.escape(LIMIT);

        db.query_err(queryLeaders, function (err, results) {
            if (err || !results) {
                return callback(new Error("Can not load leaderboard", "DB_ERROR"));
            }

            var leaderboard = [];
            var concatSIN = "";
            for (var i = 0; i < results.length; i++) {
                var item = results[i];
                var itemSIN = item.serviceInstanceNumbers.split(",");

                leaderboard.push({
                    position: (i + 1),
                    peopleCount: item.peopleCount,
                    bonusDataGb: item.bonusDataGb,
                    serviceInstanceNumbers: itemSIN,
                    users: []
                });

                itemSIN.forEach(function (item) {
                    if (concatSIN.length > 0) concatSIN += ',';
                    concatSIN += db.escape(item);
                });
            }

            var queryProfiles = "SELECT service_instance_number as serviceInstanceNumber, nick_name as nickname, picture " +
                " FROM customerProfile WHERE service_instance_number IN (" + concatSIN + ") ORDER BY nick_name";

            db.query_err(queryProfiles, function (err, results) {
                if (err || !results) {
                    return callback(new Error("Can not load profiles", "DB_ERROR"));
                }

                for (var i = 0; i < results.length; i++) {
                    var item = results[i];

                    for (var j = 0; j < leaderboard.length; j++) {
                        var leaderboardItem = leaderboard[j];
                        if (leaderboardItem.serviceInstanceNumbers.indexOf(item.serviceInstanceNumber) >= 0) {
                            leaderboardItem.users.push({
                                nickname: item.nickname,
                                picture: item.picture,
                                serviceInstanceNumber: item.serviceInstanceNumber
                            });
                        }
                    }
                }

                return callback(undefined, leaderboard);
            });
        });
    },

    /**
     *
     */
    addGoldenTicketIfEntitledBySI: function (serviceInstanceNumber, options, callback) {
        var ticketConfig = module.exports.getGoldenCircleConfig();

        ec.getCustomerDetailsServiceInstance(serviceInstanceNumber, false, (err, cache) => {
            if (err) {
                common.error("LeaderboardManager", "addGoldenTicketIfEntitled: customer not found, error=" + err.message);
                return callback(err);
            }
            if (err) {
                common.error("LeaderboardManager", "addGoldenTicketIfEntitled: customer not found " +
                "serviceInstanceNumber=" + serviceInstanceNumber);
                return callback(new BaseError("Customer is not found", "ERROR_CUSTOMER_NOT_FOUND"));
            }

            var time = new Date();
            var localTime = new Date(time.getTime() + 8 * 60 * 60 * 1000);
            var localTimeStart = new Date(localTime.getFullYear(), localTime.getMonth(), 1);
            var localTimeEnd = new Date(localTime.getFullYear(), localTime.getMonth() + 1, 0);
            var timeStart = new Date(localTimeStart.getTime() - 8 * 60 * 60 * 1000);
            var timeEnd = new Date(localTimeEnd.getTime() - 8 * 60 * 60 * 1000);

            var insertQuery = "SELECT count(*) as count FROM  historyBonuses" +
                " WHERE service_instance_number=" + db.escape(serviceInstanceNumber) +
                " AND bonus_type='referral' AND added_ts>=" + db.escape(timeStart) +
                " AND added_ts<" + db.escape(timeEnd);

            db.query_err(insertQuery, function (err, result) {
                if (err) {
                    if (LOG) common.log("LeaderboardManager", "addGoldenTicketIfEntitled: failed to load count, error=" + err.message);
                    return callback(err);
                }
                if (!result || !result[0]) {
                    return callback(new BaseError("Invalid response", "ERROR_INVALID_RESPONSE"));
                }

                var count = result && result[0] ? result[0].count : 0;
                var monthKey = localTime.getFullYear() + "_" + (localTime.getMonth() + 1);
                if (LOG) common.log("LeaderboardManager", "addGoldenTicketIfEntitled: customer has "
                + count + " referrals this month, timeStart=" + timeStart.toISOString() + ", timeEnd=" + timeEnd.toISOString());

                module.exports.loadGoldenTicketsListBySI(serviceInstanceNumber, (err, result) => {
                    if (err) {
                        if (LOG) common.log("LeaderboardManager", "addGoldenTicketIfEntitled: " +
                        "failed to load list, error=" + err.message);
                        return callback(err);
                    }

                    if (!result) {
                        return callback(new BaseError("Invalid response", "ERROR_INVALID_RESPONSE"));
                    }

                    var alreadyAdded = result.some((item) => {
                        if (item.monthKey == monthKey) {
                            return true;
                        }
                    })

                    if (alreadyAdded) {
                        if (LOG) common.log("LeaderboardManager", "addGoldenTicketIfEntitled: customer already has golden " +
                        "ticket for month key " + monthKey);

                        return callback(undefined, {
                            added: false,
                            alreadyPresented: true,
                            monthKey: monthKey
                        });
                    }

                    if (!count || count < ticketConfig.ticketReferrals) {
                        if (LOG) common.log("LeaderboardManager", "addGoldenTicketIfEntitled: customer needs more referrals " +
                        "to get golden ticket this month, currentCount=" + count);

                        var notificationKey = config.NOTIFICATION_KEY;
                        var activity = "leaderboard_golden_circle_ticket_new_referral";

                        sendNotification(cache.prefix, cache.number, activity, {
                            waiverCents: ticketConfig.waiverCents,
                            ticketReferrals: ticketConfig.ticketReferrals,
                            countCurrent: count
                        }, notificationKey, () => {
                            // do nothing
                        });

                        return callback(undefined, {
                            added: false,
                            alreadyPresented: false,
                            monthKey: monthKey,
                            referralsCount: count
                        });
                    }

                    module.exports.addGoldenTicket(serviceInstanceNumber, options, (err) => {
                        if (err) {
                            if (LOG) common.log("LeaderboardManager", "addGoldenTicketIfEntitled: " +
                            "failed to add golden ticket, error=" + err.message);
                            return callback(err);
                        }

                        return callback(undefined, {
                            added: true,
                            alreadyPresented: false,
                            monthKey: monthKey
                        });
                    });
                });
            });
        });
    },

    /**
     *
     */
    addGoldenTicket: function (serviceInstanceNumber, options, callback) {
        if (!callback) callback = () => {
        }
        if (!options) options = {};
        if (!serviceInstanceNumber) {
            return callback(new BaseError("Invalid params", "ERROR_INVALID_PARAMS"));
        }

        if (LOG) common.log("LeaderboardManager", "addGoldenTicket: add ticket, serviceInstanceNumber=" + serviceInstanceNumber);
        ec.getCustomerDetailsServiceInstance(serviceInstanceNumber, false, (err, cache) => {
            if (err) {
                common.error("LeaderboardManager", "addGoldenTicket: customer not found, error=" + err.message);
                return callback(err);
            }
            if (err) {
                common.error("LeaderboardManager", "addGoldenTicket: customer not found serviceInstanceNumber=" + serviceInstanceNumber);
                return callback(new BaseError("Customer is not found", "ERROR_CUSTOMER_NOT_FOUND"));
            }

            var time = options.retrospectiveDate ? new Date(options.retrospectiveDate) : new Date();
            var localTime = new Date(time.getTime() + 8 * 60 * 60 * 1000);
            var monthKey = localTime.getFullYear() + "_" + (localTime.getMonth() + 1);

            var insertQuery = "INSERT INTO historyGoldenTickets (serviceInstanceNumber, monthKey, status, addedAt)" +
                " VALUES (" + db.escape(serviceInstanceNumber) + "," + db.escape(monthKey) +
                "," + db.escape("PENDING") + "," + db.escape(new Date()) + ")";
            db.query_err(insertQuery, function (err, result) {
                if (err) {
                    if (err.code == "ER_DUP_ENTRY") {
                        return callback(new BaseError("Duplicate of a golden ticket (monthKey:" + monthKey + ")",
                            "ERROR_DUPLICATE_GOLDEN_TICKET"));
                    }
                    if (LOG) common.log("LeaderboardManager", "ticket history is not saved into db, error=" + err.message);
                    return callback(err);
                }

                if (!result || !result.insertId) {
                    if (LOG) common.log("LeaderboardManager", "ticket history is not saved into db, empty response");
                    return callback(new BaseError("Invalid result", "ERROR_INVALID_RESPONSE"));
                }

                var rawId = result.insertId;
                var ticketConfig = module.exports.getGoldenCircleConfig();
                var handleWaiverResult = (waiverErr, status, waiverCents) => {
                    var insertQuery = "UPDATE historyGoldenTickets SET status = " + db.escape(status) + " WHERE id=" + db.escape(rawId);
                    db.query_err(insertQuery, function (err) {
                        if (err) {
                            if (LOG) common.log("LeaderboardManager", "ticket history status is not updated, error=" + err.message);
                            return callback(err);
                        }

                        if (waiverErr) {
                            return callback(waiverErr);
                        }

                        if (options.silent) {
                            return callback();
                        }

                        var notificationKey = config.NOTIFICATION_KEY;
                        var activity = options.firstTicket
                            ? "leaderboard_golden_circle_ticket_added_first"
                            : "leaderboard_golden_circle_ticket_added";

                        sendNotification(cache.prefix, cache.number, activity, {
                            waiverCents: waiverCents,
                            ticketReferrals: ticketConfig.ticketReferrals
                        }, notificationKey, () => {
                            callback();
                        });
                    });
                }

                if (options.waiver && ticketConfig && ticketConfig.waiverCents) {
                    waiverManager.addCreditNotesBySI(serviceInstanceNumber, "GOLDEN_TICKET", ticketConfig.waiverCents, (err) => {
                        handleWaiverResult(err, err ? "ERROR_WAIVER" : "ADDED_WAIVER", ticketConfig.waiverCents);
                    });
                } else {
                    handleWaiverResult(undefined, "NO_WAIVER", 0);
                }
            });
        });
    },

    /**
     *
     */
    loadGoldenCircleRatingBySI: function (serviceInstanceNumber, callback) {

        ec.getCustomerDetailsServiceInstance(serviceInstanceNumber, false, function (err, cache) {
            if (err) {
                return callback(err);
            }
            if (!cache) {
                return callback(new Error("Can not load customer", "ERROR_CUSTOMER_NOT_FOUND"));
            }

            profileManager.loadASelfcareSetting(cache.prefix, cache.number, "golden_circles", undefined, (err, type, value) => {
                if (err) {
                    return callback(err);
                }

                if (!value) {
                    return callback(undefined, {position: -1, ticketsCount: 0});
                }

                var queryGoldenCircleLeaders = "SELECT count(*) peopleCount, totalTickets as ticketsCount, " +
                    " GROUP_CONCAT(serviceInstanceNumber) as serviceInstanceNumbers FROM (SELECT serviceInstanceNumber, " +
                    " count(*) as totalTickets FROM historyGoldenTickets GROUP BY serviceInstanceNumber) as userGoldenTickets " +
                    " GROUP BY totalTickets ORDER BY totalTickets DESC";

                db.query_err(queryGoldenCircleLeaders, function (err, results) {
                    if (err) {
                        return callback(err);
                    }
                    if (!results) {
                        return callback(new Error("Can not load rating", "DB_ERROR"));
                    }

                    for (var i = 0; i < results.length; i++) {
                        if (results[i].serviceInstanceNumbers.indexOf(serviceInstanceNumber) >= 0) {
                            return callback(undefined, {position: i, ticketsCount: results[i].ticketsCount});
                        }
                    }

                    return callback(undefined, {position: -1, ticketsCount: 0});
                })
            });
        });
    },

    /**
     *
     */
    loadGoldenCircleBoard: function (callback) {
        if (LOG) common.log("LeaderboardManager", "load loadGoldenCircleBoard");

        var queryGoldenCircleLeaders = "SELECT count(*) peopleCount, totalTickets as ticketsCount, " +
            " GROUP_CONCAT(serviceInstanceNumber) as serviceInstanceNumbers FROM (SELECT serviceInstanceNumber, " +
            " count(*) as totalTickets FROM historyGoldenTickets GROUP BY serviceInstanceNumber) as userGoldenTickets " +
            " GROUP BY totalTickets ORDER BY totalTickets DESC";

        db.query_err(queryGoldenCircleLeaders, function (err, results) {
            if (err) {
                return callback(err);
            }

            var leaderboard = [];
            var sinWithTickets = [];
            var concatSIN = "";

            for (var i = 0; results && i < results.length; i++) {
                var item = results[i];
                var itemSIN = item.serviceInstanceNumbers.split(",");

                leaderboard.push({
                    position: (i + 1),
                    peopleCount: item.peopleCount,
                    ticketsCount: item.ticketsCount,
                    serviceInstanceNumbers: itemSIN,
                    users: []
                });

                itemSIN.forEach(function (item) {
                    sinWithTickets.push(item);

                    if (concatSIN.length > 0) concatSIN += ',';
                    concatSIN += db.escape(item);
                });
            }

            db.query_err(queryGoldenCircles, function (err, results) {
                if (err) {
                    return callback(err);
                }

                var itemSIN = [];
                if (results) results.forEach((item) => {
                    if (sinWithTickets.indexOf(item.service_instance_number) == -1) {
                        sinWithTickets.push(item.service_instance_number);
                        itemSIN.push(item.service_instance_number);

                        if (concatSIN.length > 0) concatSIN += ',';
                        concatSIN += db.escape(item.service_instance_number);
                    }
                })

                if (itemSIN.length > 0) {
                    leaderboard.push({
                        position: leaderboard.length,
                        peopleCount: itemSIN.length,
                        ticketsCount: 0,
                        serviceInstanceNumbers: itemSIN,
                        users: []
                    });
                }

                if (!concatSIN) {
                    return callback(undefined, []);
                }

                var queryProfiles = "SELECT service_instance_number as serviceInstanceNumber, nick_name as nickname, picture " +
                    " FROM customerProfile WHERE service_instance_number IN (" + concatSIN + ") ORDER BY nick_name";

                db.query_err(queryProfiles, function (err, results) {
                    if (err || !results) {
                        return callback(new Error("Can not load profiles", "DB_ERROR"));
                    }

                    for (var i = 0; i < results.length; i++) {
                        var item = results[i];

                        for (var j = 0; j < leaderboard.length; j++) {
                            var leaderboardItem = leaderboard[j];
                            if (leaderboardItem.serviceInstanceNumbers.indexOf(item.serviceInstanceNumber) >= 0) {
                                leaderboardItem.users.push({
                                    nickname: item.nickname,
                                    picture: item.picture,
                                    serviceInstanceNumber: item.serviceInstanceNumber
                                });
                            }
                        }
                    }

                    return callback(undefined, leaderboard);
                });
            });
        });
    },

    /**
     *
     */
    loadGoldenTicketsListBySI: function (serviceInstanceNumber, callback) {
        var query = "SELECT * FROM historyGoldenTickets WHERE serviceInstanceNumber="
            + db.escape(serviceInstanceNumber) + " ORDER BY monthKey DESC";

        if (LOG) common.log("BonusManager loadHistoryBonusListBySI", "query=" + query);
        db.query_err(query, function (err, result) {
            if (err) {
                return callback(err);
            }
            return callback(undefined, result ? result : []);
        });
    },

    /**
     *
     */
    deleteGoldenTicket: function (id, callback) {
        var query = "DELETE FROM historyGoldenTickets WHERE id=" + db.escape(id);

        if (LOG) common.log("BonusManager loadHistoryBonusListBySI", "query=" + query);
        db.query_err(query, function (err, result) {
            if (err) {
                return callback(err);
            }
            return callback();
        });
    },

    /**
     *
     */
    loadLeaderboardRatingBySI: function (serviceInstanceNumber, callback) {
        var query = "SELECT (SUM(bonus_kb_pretty) / 1000 / 1000) as dataGb FROM historyBonuses" +
            " WHERE service_instance_number=" + db.escape(serviceInstanceNumber) +
            " AND deactivated=0 AND (display_until_ts = '0000-00-00 00:00:00' OR display_until_ts = '')";

        if (LOG) common.log("BonusManager loadHistoryBonusListBySI", "query=" + query);
        db.query_err(query, function (err, result) {
            if (err) {
                return callback(err);
            }

            var leaderboardDataGb = 0;
            if (result && result[0]) {
                leaderboardDataGb = result[0].dataGb;
            }

            module.exports.loadLeaderboard((err, result) => {
                if (err) {
                    return callback(err);
                }

                var count = 0;
                var position = 0;
                var needToReachGb = 0;

                if (result && result.length) {
                    count = result.length;
                    result.forEach((item) => {
                        if (item.serviceInstanceNumbers.indexOf(serviceInstanceNumber) >= 0) {
                            position = item.position;
                        }
                    });

                    var needToReachTopGb = result[result.length - 1].bonusDataGb - leaderboardDataGb;
                    if (needToReachTopGb < 0) {
                        needToReachTopGb = 0;
                    }
                }

                if (leaderboardDataGb) {
                    leaderboardDataGb = parseInt(leaderboardDataGb * 100) / 100;
                }
                if (needToReachTopGb) {
                    needToReachTopGb = parseInt(needToReachTopGb * 100) / 100;
                }

                return callback(undefined, {
                    dataGb: leaderboardDataGb,
                    rating: {
                        count: count,
                        position: position,
                        needToReachGb: needToReachTopGb
                    }
                });
            })
        });
    },

    /**
     *
     */
    updateRating: function (executionKey, callback) {
        if (LOG) common.log("LeaderboardManager", "update rating");

        var loadNewLeaders = function (callback) {
            var queryLeaders = "SELECT count(*) peopleCount, (totalBonus / 1000 / 1000) as bonusDataGb, " +
                " GROUP_CONCAT(service_instance_number) as serviceInstanceNumbers FROM (SELECT service_instance_number, " +
                " sum(bonus_kb_pretty) as totalBonus FROM historyBonuses " +
                " WHERE service_instance_number NOT IN (" + queryGoldenCircles + ") AND display_until_ts=0 AND deactivated=0" +
                " GROUP BY service_instance_number) as userBonus GROUP BY totalBonus " +
                " ORDER BY bonusDataGb DESC LIMIT " + db.escape(LIMIT);

            db.query_err(queryLeaders, function (err, results) {
                if (err || !results) {
                    return callback(new Error("Can not load new leaders", "DB_ERROR"));
                }

                var leaders = {
                    list: []
                };
                for (var i = 0; i < results.length; i++) {
                    var item = results[i];
                    var serviceInstanceNumbers = item.serviceInstanceNumbers.split(",");
                    serviceInstanceNumbers.forEach(function (sin) {
                        var position = {
                            positionNew: i,
                            sin: sin
                        }

                        leaders[sin] = position;
                        leaders.list.push(position);
                    });
                }

                return callback(undefined, leaders);
            });
        }

        var loadCurrentLeaders = function (callback) {
            var queryCurrentLeaders = "SELECT customerProfile.service_instance_number, customerProfile.leaderboard_position, " +
                "customerOptions.value as golden_circles FROM customerProfile LEFT JOIN customerOptions " +
                "ON (customerOptions.profile_id = customerProfile.id AND customerOptions.app_type = 'selfcare' " +
                "AND option_key='golden_circles') WHERE leaderboard_position >= 0";

            db.query_err(queryCurrentLeaders, function (err, results) {
                if (err || !results) {
                    return callback(new Error("Can not load existing leaders", "DB_ERROR"));
                }

                var leaders = {
                    list: []
                };
                for (var i = 0; i < results.length; i++) {
                    var item = results[i];
                    var position = {
                        positionOld: item.leaderboard_position,
                        goldenCircles: item.golden_circles == '1',
                        sin: item.service_instance_number
                    }

                    leaders[position.sin] = position;
                    leaders.list.push(position);
                }

                return callback(undefined, leaders);
            });
        }

        var loadProfiles = function (positionNew, positionChanged, positionLost, callback) {

            var loadInfoProfile = function (sin, callback) {
                ec.getCustomerDetailsServiceInstance(sin, false, function (err, cache) {
                    if (err) {
                        common.error("LeaderboardManager", "profile loading error, " + sin + ", err=" + err.message);
                        return callback(undefined, {error: err.message, message: "EC error", sin: sin, sent: false});
                    }

                    if (typeof(profileManager.loadProfileDetails) != 'function') {    // remove this if issue is fixed
                        common.error("leaderboardManager loadProfiles", "profileManager.loadProfileDetails is not a function");
                        profileManager = require('../profile/profileManager');
                    }
                    profileManager.loadProfileDetails(cache.prefix, cache.number, executionKey, function () {
                        callback();
                    });
                });
            }

            if (positionNew.length == 0 && positionChanged.length == 0 && positionLost.length == 0) {
                return callback();
            }

            var checkedCount = 0;
            var checkResponse = function () {
                checkedCount++;
                if (checkedCount == positionNew.length + positionChanged.length + positionLost.length) {
                    return callback();
                }
            }

            positionNew.forEach(function (item) {
                loadInfoProfile(item.sin, checkResponse);
            });
            positionChanged.forEach(function (item) {
                loadInfoProfile(item.sin, checkResponse);
            });
            positionLost.forEach(function (item) {
                loadInfoProfile(item.sin, checkResponse);
            });
        }

        var updateDatabase = function (positionNew, positionChanged, positionLost, callback) {

            db.transaction(function (err, connection) {
                if (!connection || err) {
                    common.error("LeaderboardManager", "Database transaction error");
                    return callback(new Error("Database transaction error"));
                } else {

                    var rollbackCallback = function (message) {
                        connection.rollback(function () {
                            connection.release();
                            common.error("LeaderboardManager", message);
                            return callback(new Error(message));
                        });
                    };

                    var commitCallback = function () {
                        connection.commit(function (err) {
                            if (err) {
                                rollbackCallback("Commit failed")
                            } else {
                                connection.release();
                                callback();
                            }
                        });
                    };

                    var queries = [];
                    var createQuery = function (sin, position) {
                        queries.push("UPDATE customerProfile SET leaderboard_position=" + db.escape(position)
                        + " WHERE service_instance_number=" + db.escape(sin));
                    };

                    positionNew.forEach(function (item) {
                        createQuery(item.sin, item.positionNew);
                    });
                    positionChanged.forEach(function (item) {
                        createQuery(item.sin, item.positionNew);
                    })
                    positionLost.forEach(function (item) {
                        createQuery(item.sin, item.positionNew);
                    });

                    if (queries.length == 0) {
                        callback();
                    }

                    var countProcessed = 0;
                    var failed = false;

                    queries.forEach(function (query) {
                        connection.query(query, function (err, result) {
                            countProcessed++;

                            // ignore if already failed
                            if (failed) {
                                return;
                            }

                            if (err || !result) {
                                failed = true;
                                return rollbackCallback("Options update failed on operation #" + countProcessed);
                            }

                            if (countProcessed == queries.length) {
                                if (LOG) common.log("LeaderboardManager", "Update position succeed, count=" + countProcessed);
                                return commitCallback();
                            }
                        });
                    });
                }
            });
        }

        var sendNotifications = function (positionNew, positionChanged, positionLost, callback) {

            var loadInfoAndSendNotification = function (activity, sin, positionNew, positionOld, callback) {
                ec.getCustomerDetailsServiceInstance(sin, false, function (err, cache) {
                    if (err) {
                        common.error("LeaderboardManager", "loading new leaders error, err=" + err.message);
                        return callback(undefined, {error: err.message, message: "EC error", sent: false});
                    }

                    if (!cache) {
                        common.error("LeaderboardManager", "not found, sin=" + sin);
                        return callback(undefined, {error: "Cache not found", message: "EC cache not found", sent: false});
                    }

                    sendNotification(cache.prefix, cache.number, activity, {
                        positionNew: positionNew, positionOld: positionOld, waiverCents: 0
                    }, executionKey, function () {
                        callback(undefined, {sent: true});
                    });
                });
            }

            if (positionNew.length == 0 && positionChanged.length == 0 && positionLost.length == 0) {
                return callback();
            }

            var sentCount = 0;
            var checkResponse = function () {
                sentCount++;

                if (sentCount == positionNew.length + positionChanged.length + positionLost.length) {
                    return callback();
                }
            }

            positionNew.forEach(function (item) {
                loadInfoAndSendNotification("leaderboard_live_rank_new", item.sin,
                    item.positionNew, item.positionOld, checkResponse);
            });
            positionChanged.forEach(function (item) {
                loadInfoAndSendNotification((item.positionNew >= item.positionOld
                        ? "leaderboard_live_rank_up" : "leaderboard_live_rank_down"),
                    item.sin, item.positionNew, item.positionOld, checkResponse);
            });
            positionLost.forEach(function (item) {
                if (item.goldenCircles) {
                    loadInfoAndSendNotification("leaderboard_golden_circle_new", item.sin,
                        -1, -1, checkResponse);
                } else {
                    loadInfoAndSendNotification("leaderboard_live_rank_lost", item.sin,
                        item.positionNew, item.positionOld, checkResponse);
                }
            });
        }

        var lockCacheKey = "leaderBoardRatingSync";
        db.lockOperation(lockCacheKey, {}, 1 * 60 * 1000, function (err) {
            if (err) {
                if (err.status == "ERROR_OPERATION_LOCKED") {
                    if (LOG) common.log("LeaderboardManager", "rating update is in the progress");
                    return callback();
                }

                common.error("LeaderboardManager", "lock error=" + err.message);
                return callback(err);
            }

            loadNewLeaders(function (err, newLeaders) {
                if (err) {
                    common.error("LeaderboardManager", "loading new leaders error, err=" + err.message);
                    db.unlockOperation(lockCacheKey);
                    return callback(err);
                }

                loadCurrentLeaders(function (err, currentLeaders) {
                    if (err) {
                        common.error("LeaderboardManager", "loading current leaders error, err=" + err.message);
                        db.unlockOperation(lockCacheKey);
                        return callback(err);
                    }

                    if (LOG) common.log("LeaderboardManager", "newLeaders=" + JSON.stringify(newLeaders)
                    + ", currentLeaders=" + JSON.stringify(currentLeaders));

                    var positionNew = [];
                    var positionChanged = [];
                    var positionLost = [];

                    newLeaders.list.forEach(function (item) {
                        if (!currentLeaders[item.sin]) {
                            item.positionOld = -1;
                            positionNew.push(item);
                        } else if (currentLeaders[item.sin].positionOld != item.positionNew) {
                            item.positionOld = currentLeaders[item.sin].positionOld;
                            positionChanged.push(item);
                        }
                    });

                    currentLeaders.list.forEach(function (item) {
                        if (!newLeaders[item.sin]) {
                            item.positionNew = -1;
                            positionLost.push(item);
                        }
                    });

                    if (LOG) common.log("LeaderboardManager", "positionNew=" + JSON.stringify(positionNew)
                    + ", positionChanged=" + JSON.stringify(positionChanged) + ", positionLost=" + JSON.stringify(positionLost));

                    loadProfiles(positionNew, positionChanged, positionLost, function (err) {
                        if (err) {
                            common.error("LeaderboardManager", "failed to load user profiles, err=" + err.message);
                            db.unlockOperation(lockCacheKey);
                            return callback(err);
                        }

                        updateDatabase(positionNew, positionChanged, positionLost, function (err) {
                            if (err) {
                                common.error("LeaderboardManager", "failed to update database, err=" + err.message);
                                db.unlockOperation(lockCacheKey);
                                return callback(err);
                            }

                            sendNotifications(positionNew, positionChanged, positionLost, function (err) {
                                if (err) {
                                    common.error("LeaderboardManager", "failed to send notifications database, err=" + err.message);
                                    db.unlockOperation(lockCacheKey);
                                    return callback(err);
                                }

                                db.unlockOperation(lockCacheKey);
                                callback();
                            })
                        });
                    });

                });
            });
        });
    }
}

function sendNotification(prefix, number, activity, options, notificationKey, callback) {
    var url = "http://" + config.MYSELF + ":" + config.WEBPORT +
        "/api/1/webhook/notifications/internal/" + notificationKey;

    var positionNew = options.positionNew;
    var positionOld = options.positionOld;
    var waiverCents = options.waiverCents;
    var ticketReferrals = options.ticketReferrals;
    var countCurrent = options.countCurrent;
    var countLeft = (countCurrent && ticketReferrals
    && ticketReferrals > countCurrent ? ticketReferrals - countCurrent : 0);

    if (positionNew >= 0) {
        positionNew += 1;
    }

    if (positionOld >= 0) {
        positionOld += 1;
    }

    request({
        uri: url,
        method: 'POST',
        timeout: 20000,
        json: {
            activity: activity,
            prefix: prefix,
            number: number,
            variables: {
                positionNew: positionNew,
                positionOld: positionOld,
                count_required: ticketReferrals,
                count_current: countCurrent,
                count_left: countLeft,
                waiver: (waiverCents > 0 ? waiverCents / 100 : 0)
            }
        }
    }, function (err, response, body) {
        if (err) {
            if (callback) callback(err);
            return;
        }
        if (callback) callback();
    });
}
