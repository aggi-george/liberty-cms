var request = require('request');
var async = require('async');

var config = require('../../../../config');
var common = require('../../../../lib/common');
var db = require('../../../../lib/db_handler');
var ec = require('../../../../lib/elitecore');
var usageManager = require('../account/usageManager');
var BaseError = require('../../errors/baseError');

var LOG = config.LOGSENABLED;

/**
 *  MB   =>  KB    => Discrepancy KB
 * 50MB  => 51200  => 1228.8 KB
 * 100MB => 102400 => 2457.6 KB
 * 150MB => 153600 => 3686.4 KB
 * 200BM => 204800 => 4915.2 KB
 * 250BM => 256000 => 6144 KB
 * 300BM => 307200 => 7372.8 KB
 * 400BM => 409600 => 9830.4 KB
 * 500BM => 512000 => 12288 KB
 * 600BM => 614400 => 14745.6 KB
 * 700BM => 716800 => 17203.2 KB
 * 750BM => 768000 => 18432 KB
 * 800BM => 819200 => 19660.8 KB
 * 900BM => 921600 => 22118.4 KB
 */

var MB50 = 50 * 1024;

var DISCREPANCY = {
    51200: 1228.8,
    102400: 2457.6,
    153600: 3686.4,
    204800: 4915.2,
    256000: 6144,
    307200: 7372.8,
    409600: 9830.4,
    512000: 12288,
    614400: 14745.6,
    716800: 17203.2,
    768000: 18432,
    819200: 19660.8,
    921600: 22118.4
};

module.exports = {


    /**
     *
     */
    updateNextBillBonuses: function (prefix, number, initiator, executionKey, callback) {
        if (!number || !prefix) {
            return callback(new Error("Params are incorrect", "PARAMS_ERROR"));
        }

        ec.getCustomerDetailsNumber(prefix, number, false, function (err, cache) {
            if (err) {
                if (LOG) common.error("SubscriptionManager", "failed to load client cache, error=" + err.message);
                return callback(err);
            }
            if (!cache) {
                if (LOG) common.error("SubscriptionManager", "customer does not exist");
                return callback(new Error("Cache is empty"));
            }

            var key = "re_subscription_" + cache.serviceInstanceNumber;
            var cache = "cache";

            db.cache_lock(cache, key, {
                prefix: prefix,
                number: number
            }, 120000, function (lock) {
                if (lock) {

                    var unlock = function () {
                        db.cache_del(cache, key);
                    }

                    updateNextBillBonusesInternal(prefix, number, initiator, executionKey, function (err, result) {
                        callback(err, result);
                        unlock();
                    });

                } else {
                    if (LOG) common.error("SubscriptionManager", "Re-subscription is in progress");
                    return callback(new Error("Re-subscription is in progress"));
                }
            });
        });
    },

    /**
     *
     */
    updateCurrentBillBonuses: function (prefix, number, initiator, executionKey, callback) {
        if (!number || !prefix) {
            return callback(new Error("Params are incorrect", "PARAMS_ERROR"));
        }

        ec.getCustomerDetailsNumber(prefix, number, false, function (err, cache) {
            if (err) {
                if (LOG) common.error("SubscriptionManager", "failed to load client cache, error=" + err.message);
                return callback(err);
            }
            if (!cache) {
                if (LOG) common.error("SubscriptionManager", "customer does not exist");
                return callback(new Error("Cache is empty"));
            }

            var key = "update_current_month_data_" + cache.serviceInstanceNumber;
            var cache = "cache";

            db.cache_lock(cache, key, {
                prefix: prefix,
                number: number
            }, 120000, function (lock) {
                if (lock) {

                    var unlock = function () {
                        db.cache_del(cache, key);
                    }

                    updateCurrentBillBonusesInternal(prefix, number, initiator, executionKey, function (err, result) {
                        if (!err && result && !result.ignored && !result.inactive) {
                            module.exports.updateNextBillBonuses(prefix, number, initiator, executionKey, function(err) {
                                callback(err, result);
                                unlock();
                            })
                        } else {
                            callback(err, result);
                            unlock();
                        }
                    });

                } else {
                    if (LOG) common.error("SubscriptionManager", "Bonus correction is in progress");
                    return callback(new Error("Bonus correction is in progress"));
                }
            });
        });
    },

    getAddonSubscriptionForCustomer: function(number, productId, startTs, endTs, callback){
        db.weblog.findOne({"type": "updateAddon", "id": number, "action" : new RegExp(productId), "ts": {"$gte": startTs, "$lt": endTs}}, function(err, data){
            if(err){
                callback(new BaseError("Failed to retrieve generals history", "ERROR_RETRIEVE_GENERALS_HISTORY"));
            }else if(data){
                callback(null, data);
            }else{
                callback();
            }
        });
    }
}

function updateNextBillBonusesInternal(prefix, number, initiator, executionKey, callback) {
    if (LOG) common.log("SubscriptionManager", "number=" + number + ", update next bill bonuses, initiator="
    + initiator + ", executionKey=" + executionKey);

    var bonusHistory;
    var mutuallyExclusiveBonuses;
    var unsubscribeRecurrentBonuses;
    var unSubscriptionResult;
    var subscribeRecurrentBonuses;
    var subscriptionResult;

    var skippedResult = function () {

        var saveLogs = function () {
            var obj = {
                cycle: "NEXT",
                status: "IGNORED",
                prefix: prefix,
                number: number,
                ignored: true,
                initiator: initiator,
                recurrentBonusHistory: bonusHistory,
                dev: config.DEV,
                ts: new Date().getTime()
            }

            if (LOG) common.log("SubscriptionManager", "re-subscription skipped, obj=" + JSON.stringify(obj));
            db.notifications_resubscription.insert(obj, function () {
                if (callback) callback(undefined, obj);
            });
        }

        saveLogs();
    }

    var handleResult = function (executionErr, inactive) {

        var saveLogs = function (err, inactive) {
            var obj = {
                cycle: "NEXT",
                status: err ? "ERROR" : inactive ? "ERROR_INACTIVE" : "OK",
                error: err ? err.message : undefined,
                prefix: prefix,
                number: number,
                initiator: initiator,
                inactive: inactive,
                mutuallyExclusiveBonuses: mutuallyExclusiveBonuses,
                unsubscribeRecurrentBonuses: unsubscribeRecurrentBonuses,
                unSubscriptionResult: unSubscriptionResult,
                recurrentBonusHistory: bonusHistory,
                subscribeRecurrentBonuses: subscribeRecurrentBonuses,
                subscriptionResult: subscriptionResult,
                dev: config.DEV,
                ts: new Date().getTime()
            }

            if (LOG) common.log("SubscriptionManager", "re-subscription report, obj=" + JSON.stringify(obj));
            db.notifications_resubscription.insert(obj, function () {
                if (callback) callback(err, obj);
            });
        }

        saveLogs(executionErr, inactive);
    }

    ec.getCustomerDetailsNumber(prefix, number, false, function (err, cache) {
        if (err || !cache) {
            if (LOG) common.error("SubscriptionManager", "failed to load client cache " + err);
            return cache(err ? err : new Error("Cache is empty"));
        }

        var effect = 2;
        var subscribe = function (bonuses) {
            if (!bonuses || bonuses.length == 0) {
                if (LOG) common.log("SubscriptionManager", "number=" + number + ", no bonus to subscribe");
                return handleResult();
            }

            var subscribeAddons = [];
            bonuses.forEach(function (bonus) {
                subscribeAddons.push({
                    product_id: bonus.id,
                    recurrent: bonus.recurrent,
                    effect: effect
                })
            });

            if (LOG) common.log("SubscriptionManager", "number=" + number + ", subscribe=" + JSON.stringify(subscribeAddons));
            ec.addonSubscribe(cache.number, cache.serviceInstanceNumber, subscribeAddons, cache.billing_cycle, function (err, result) {
                subscriptionResult = {
                    success: err ? false : true,
                    error: err ? err.message : undefined,
                    result: result
                }

                if (err) {
                    if (LOG) common.error("SubscriptionManager", "number=" + number +
                    ", failed to subscribe " + err.message);
                    return handleResult(err);
                }

                if (LOG) common.log("SubscriptionManager", "number=" + number + ", subscribe result=" + JSON.stringify(result));
                ec.setCache(number, prefix, undefined, cache.account, function (err, result) {
                    handleResult();
                });
            });
        }

        var unsubscribe = function (bonuses, bonusToResubscribe) {
            if (!bonuses || bonuses.length == 0) {
                subscribe(bonusToResubscribe);
                return;
            }

            var unSubscribeAddons = [];
            bonuses.forEach(function (bonus) {
                unSubscribeAddons.push({
                    product_id: bonus.id,
                    packageHistoryId: bonus.packageHistoryId,
                    current: bonus.current,
                    effect: effect
                });
            });

            if (LOG) common.log("SubscriptionManager", "number=" + number + ", unsubscribe=" +
            JSON.stringify(unSubscribeAddons));

            let uniqueUnsubAddons = common.uniqify(unSubscribeAddons, ['product_id', 'effect', 'current']);
            if (uniqueUnsubAddons.length < 1) {
                uniqueUnsubAddons = unSubscribeAddons
            }
            ec.addonUnsubscribe(cache.number, cache.serviceInstanceNumber, uniqueUnsubAddons,
                cache.billing_cycle, function (err, result) {
                    unSubscriptionResult = {
                        success: err ? false : true,
                        error: err ? err.message : undefined,
                        result: result
                    }

                    if (err) {
                        common.error("SubscriptionManager", "number=" + number +
                        ", failed to unSubscribe " + err.message);
                        return handleResult(err);
                    }

                    if (LOG) common.log("SubscriptionManager", "number=" + number +
                    ", unsubscribe result=" + JSON.stringify(result));
                    ec.setCache(number, prefix, undefined, cache.account, function (err, result) {
                        subscribe(bonusToResubscribe);
                    });
                });
        }

        var findBonuses = function (activeBonuses, dataKb, excludeMutuallyExclusive) {
            if (!cache.status || cache.status.toUpperCase() !== "ACTIVE") {
                return handleResult(undefined, true);
            }

            var silentBonuses = [];
            ec.packages().bonus.forEach(function (bonus) {
                if (bonus.recurrent
                    && (!bonus.app || bonus.app.length == 0)
                    && bonus.name.startsWith("Bonus ")
                    && bonus.name.endsWith("R")
                    && bonus.name != "Bonus 0MB R") {
                    silentBonuses.push(bonus);
                }
            });

            silentBonuses.sort(function (a, b) {
                return b.kb - a.kb;
            });

            var minBonusSize = silentBonuses[silentBonuses.length - 1].kb;
            var bonusToResubscribe = [];

            var pos = 0;
            var leftDataKb = dataKb;
            while (leftDataKb >= minBonusSize && pos < silentBonuses.length) {
                var bonus = silentBonuses[pos];
                var key = "" + bonus.kb;
                var discrepancyKb = DISCREPANCY[key] ? DISCREPANCY[key] : 0;

                if (bonus.kb <= leftDataKb) {
                    leftDataKb -= (bonus.kb + discrepancyKb);
                    removeUnusedFields(bonus);
                    bonusToResubscribe.push(bonus);
                }
                pos++;
            }

            if (leftDataKb > MB50) {
                if (LOG) common.error("SubscriptionManager", "can not resolve re-subscription, dataKb="
                + dataKb + ", leftDataKb=" + leftDataKb);

                return handleResult(new Error("Can not resolve re-subscription (data: "
                + (dataKb / 1024) + "MB, left=" + (leftDataKb / 1024) + "MB"));
            }

            var canceledBonuses = [];
            if (excludeMutuallyExclusive) {
                // removing mutually exclusive bonuses when
                // history is already clean and has no duplicates

                bonusToResubscribe.forEach(function (reSubscribeItem) {
                    activeBonuses.forEach(function (activeItem) {
                        if (reSubscribeItem.name === activeItem.name) {
                            canceledBonuses.push(activeItem.name);
                        }
                    });
                });

                var filteredActive = [];
                activeBonuses.forEach(function (item) {
                    if (canceledBonuses.indexOf(item.name) == -1) {
                        filteredActive.push(item);
                    }
                });

                var filteredResubscribe = [];
                bonusToResubscribe.forEach(function (item) {
                    if (canceledBonuses.indexOf(item.name) == -1) {
                        filteredResubscribe.push(item);
                    }
                });

                activeBonuses = filteredActive;
                bonusToResubscribe = filteredResubscribe;
            }

            if (LOG) common.log("SubscriptionManager", "number=" + number + ", re-subscription calculation," +
            " dataKb=" + dataKb + ", leftDataKb=" + leftDataKb + ", canceledBonuses=" + JSON.stringify(canceledBonuses));

            subscribeRecurrentBonuses = bonusToResubscribe;
            mutuallyExclusiveBonuses = canceledBonuses;
            unsubscribe(activeBonuses, bonusToResubscribe)
        }

        var checkIfUnSubscriptionRequired = function (totalActiveDataKb, activeBonuses, totalDataKb, bonusHistory) {
            var hasDuplicates;
            var addonsNames = [];

            activeBonuses.forEach(function (item) {
                if (addonsNames.indexOf(item.name) >= 0) {
                    if (LOG) common.log("SubscriptionManager", "found an addon subscribed "
                    + "multiple times, name=" + item.name);
                    hasDuplicates = true;
                }
                addonsNames.push(item.name);
            });

            if (LOG) common.log("SubscriptionManager", "totalActiveDataKb=" + totalActiveDataKb
            + ", totalDataKb=" + totalDataKb + ", hasDuplicates=" + hasDuplicates);

            if (hasDuplicates) {
                return findBonuses(activeBonuses, totalDataKb, false);
            } else if (Math.abs(totalActiveDataKb - totalDataKb) > MB50) {
                return findBonuses(activeBonuses, totalDataKb, true);
            } else {
                return skippedResult();
            }
        }

        var loadHistory = function (totalActiveDataKb, activeBonuses) {
            var serviceInstanceNumber = cache.serviceInstanceNumber;
            var query = "SELECT * FROM historyBonuses WHERE service_instance_number=" + db.escape(serviceInstanceNumber) +
                " AND display_until_ts < 1000 AND deactivated=0";

            db.query_err(query, function (err, results) {
                var totalDataKb = 0;
                results.forEach(function (item) {
                    removeUnusedFields(item);
                    totalDataKb += item.bonus_kb;

                    var key = "" + item.bonus_kb
                    if (DISCREPANCY[key]) {
                        totalDataKb += DISCREPANCY[key];
                    }
                });

                bonusHistory = results;
                checkIfUnSubscriptionRequired(totalActiveDataKb, activeBonuses, totalDataKb, results);
            });
        }

        var loadActiveBonuses = function () {

            var bonuses = [];
            cache.bonus.forEach(function (bonus) {
                if (bonus.name != "Bonus 0MB R") {
                    bonus.current = true;
                    bonuses.push(bonus);
                }
            });
            cache.bonus_future.forEach(function (bonus) {
                if (bonus.name != "Bonus 0MB R") {
                    bonus.current = false;
                    bonuses.push(bonus);
                }
            });

            var activeBonuses = [];
            bonuses.forEach(function (bonus) {
                var dateRecurrent = new Date("9999-01-01T00:00:00.000Z");
                var dateAddon = new Date(bonus.expiryDate);
                var recurrent = dateRecurrent.getTime() < dateAddon.getTime();
                var canBeRemoved = bonus.packageHistoryId && bonus.packageHistoryId.length > 0;
                if (recurrent && canBeRemoved) {
                    removeUnusedFields(bonus);
                    activeBonuses.push(bonus);
                }
            });

            unsubscribeRecurrentBonuses = activeBonuses;

            var totalActiveDataKb = 0;
            var hasInvalidKbValue = false;
            activeBonuses.forEach(function (item) {
                if (item.kb) {
                    totalActiveDataKb += item.kb;
                    var key = "" + item.kb
                    if (DISCREPANCY[key]) {
                        totalActiveDataKb += DISCREPANCY[key];
                    }
                } else {
                    if (LOG) common.error("SubscriptionManager", "bonus has invalid kb value, name="
                    + item.name + ", kb=" + item.kb);
                    hasInvalidKbValue = true;
                }
            });

            if (hasInvalidKbValue) {
                return handleResult(new Error("Can not aggregate when kb of any bonus is invalid"));
            }

            loadHistory(totalActiveDataKb, activeBonuses);
        }

        loadActiveBonuses();
    });
}

function updateCurrentBillBonusesInternal(prefix, number, initiator, executionKey, callback) {
    if (LOG) common.log("SubscriptionManager", "number=" + number + ", update current bill bonuses, initiator="
    + initiator + ", executionKey=" + executionKey);

    var bonusHistory;
    var subscribeRecurrentBonuses;
    var subscriptionResult;

    var skippedResult = function () {

        var obj = {
            cycle: "CURRENT",
            status: "IGNORED",
            prefix: prefix,
            number: number,
            ignored: true,
            initiator: initiator,
            recurrentBonusHistory: bonusHistory,
            dev: config.DEV,
            ts: new Date().getTime()
        }

        if (LOG) common.log("SubscriptionManager", "re-subscription skipped, obj=" + JSON.stringify(obj));
        db.notifications_resubscription.insert(obj, function () {
            if (callback) callback(undefined, obj);
        });
    }

    var handleResult = function (err, inactive) {

        var obj = {
            cycle: "CURRENT",
            status: err ? (inactive ? "ERROR_INACTIVE" : "ERROR") : "OK",
            error: err ? err.message : undefined,
            prefix: prefix,
            number: number,
            initiator: initiator,
            inactive: inactive,
            recurrentBonusHistory: bonusHistory,
            subscribeRecurrentBonuses: subscribeRecurrentBonuses,
            subscriptionResult: subscriptionResult,
            dev: config.DEV,
            ts: new Date().getTime()
        }

        if (LOG) common.log("SubscriptionManager", "re-subscription report, obj=" + JSON.stringify(obj));
        db.notifications_resubscription.insert(obj, function () {
            if (callback) callback(err, obj);
        });
    }

    ec.getCustomerDetailsNumber(prefix, number, false, function (err, cache) {
        if (err || !cache) {
            if (LOG) common.error("SubscriptionManager", "failed to load client cache " + err);
            return handleResult(err ? err : new Error("Cache is empty"));
        }

        var inactive = !cache.status || cache.status.toUpperCase() !== "ACTIVE";
        var subscribe = function (bonuses, totalActiveDataKb, totalDataKb) {
            if (!bonuses || bonuses.length == 0) {
                if (LOG) common.log("SubscriptionManager", "number=" + number + ", no bonus to subscribe");
                return handleResult(undefined, inactive);
            }

            var subscribeAddons = [];
            var effect = 0;
            bonuses.forEach(function (bonus) {
                subscribeAddons.push({
                    product_id: bonus.id,
                    recurrent: bonus.recurrent,
                    effect: effect
                })
            });

            var totalActiveDataGb = parseInt((totalActiveDataKb / 1024 / 1024) * 100) / 100;
            var totalDataGb = parseInt((totalDataKb / 1024 / 1024) * 100) / 100;

            if (LOG) common.log("SubscriptionManager", "number=" + number + ", subscribe=" + JSON.stringify(subscribeAddons));
            ec.addonSubscribe(cache.number, cache.serviceInstanceNumber, subscribeAddons, cache.billing_cycle, function (err, result) {
                subscriptionResult = {
                    success: err ? false : true,
                    error: err ? err.message : undefined,
                    result: result,
                    totalActiveDataGb: totalActiveDataGb,
                    totalDataGb: totalDataGb
                }

                if (err) {
                    if (LOG) common.error("SubscriptionManager", "number=" + number +
                    ", failed to subscribe, error=" + err.message);
                    return handleResult(err, inactive);
                }

                if (LOG) common.log("SubscriptionManager", "number=" + number + ", subscribe result=" + JSON.stringify(result));
                ec.setCache(number, prefix, undefined, cache.account, function (err, result) {
                    handleResult(undefined, inactive);
                });
            });
        }

        var findBonuses = function (totalActiveDataKb, totalDataKb) {
            if (!cache.status || cache.status.toUpperCase() !== "ACTIVE") {
                return handleResult(new Error("Can not add bonuses for inactive customers"), true);
            }

            var recoveryBonuses = [];
            ec.packages().bonus.forEach(function (bonus) {
                if (bonus.name.startsWith("Bonus ") && bonus.name.endsWith(" Recovery")) {
                    recoveryBonuses.push(bonus);
                }
            });

            if (recoveryBonuses.length == 0) {
                if (LOG) common.error("SubscriptionManager", "number=" + number +
                ", no recovery bonuses are found");
                return handleResult(new Error("No Recovery Bonuses are found"), inactive);
            }

            recoveryBonuses.sort(function (a, b) {
                return b.kb - a.kb;
            });

            var minBonusSize = recoveryBonuses[recoveryBonuses.length - 1].kb;
            var bonusToResubscribe = [];

            var pos = 0;
            var leftDataKb = totalDataKb - totalActiveDataKb;

            if (leftDataKb < minBonusSize) {
                return skippedResult();
            }

            while (leftDataKb >= minBonusSize) {
                var bonus = recoveryBonuses[pos];
                var key = "" + bonus.kb;
                var discrepancyKb = DISCREPANCY[key] ? DISCREPANCY[key] : 0;

                if (bonus.kb <= leftDataKb) {
                    leftDataKb -= (bonus.kb + discrepancyKb);
                    removeUnusedFields(bonus);
                    bonusToResubscribe.push(bonus);
                } else {
                    pos++;
                }
            }

            if (LOG) common.log("SubscriptionManager", "number=" + number + ", one time bonuses calculation," +
            "totalActiveDataKb=" + totalActiveDataKb + ", totalDataKb=" + totalDataKb + ", leftDataKb=" + leftDataKb);

            subscribeRecurrentBonuses = bonusToResubscribe;
            subscribe(bonusToResubscribe, totalActiveDataKb, totalDataKb)
        }

        var loadHistory = function (totalActiveDataKb) {
            var serviceInstanceNumber = cache.serviceInstanceNumber;
            var query = "SELECT * FROM historyBonuses WHERE service_instance_number=" + db.escape(serviceInstanceNumber) +
                " AND (display_until_ts < 1000 OR display_until_ts > " + db.escape(new Date()) + ") AND deactivated=0";

            db.query_err(query, function (err, results) {
                var totalDataKb = 0;
                results.forEach(function (item) {
                    removeUnusedFields(item);
                    totalDataKb += item.bonus_kb;

                    var key = "" + item.bonus_kb
                    if (DISCREPANCY[key]) {
                        totalDataKb += DISCREPANCY[key];
                    }
                });

                bonusHistory = results;
                findBonuses(totalActiveDataKb, totalDataKb);
            });
        }

        var loadActiveData = function () {

            usageManager.computeUsage(prefix, number, function (err, usageResult) {
                if (err) {
                    if (LOG) common.error("SubscriptionManager", "number=" + number +
                    ", failed to compute data " + JSON.stringify(err));
                    return handleResult(err, inactive);
                }

                if (!usageResult || !usageResult.data || !usageResult.data.bonus) {
                    if (LOG) common.error("SubscriptionManager", "number=" + number +
                    ", failed to load bonus data, empty result");
                    return handleResult(new Error("Bonus data is empty"), inactive);
                }

                var totalBonus = usageResult.data.bonus.left + usageResult.data.bonus.used;
                loadHistory(totalBonus);
            });

        }

        loadActiveData();
    });
}

function removeUnusedFields(obj) {
    delete obj.order_id;
    delete obj.title;
    delete obj.app;
    delete obj.subtitle;
    delete obj.description_short;
    delete obj.description_full;
    delete obj.disclaimer;
    delete obj.sub_effect;
    delete obj.unsub_effect;
    delete obj.action;
    delete obj.enabled;

    delete obj.display_until_ts;
    delete obj.deactivated_ts;
    delete obj.deactivated;
    delete obj.data1;
    delete obj.data2;
    delete obj.data3;
    delete obj.data4;
    delete obj.data5;
    delete obj.data6;
    delete obj.data7;
    delete obj.data8;
    delete obj.data9;
    delete obj.data10;
    delete obj.dataInt1;
    delete obj.dataInt2;
    delete obj.dataInt3;
}
