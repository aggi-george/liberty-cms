var request = require('request');
var async = require('async');

var config = require('../../../../config');
var common = require('../../../../lib/common');
var db = require('../../../../lib/db_handler');
var ec = require('../../../../lib/elitecore');

var LOG = config.LOGSENABLED;


module.exports = {

    /**
     *
     *
     */
    addCreditNotes: function (prefix, number, reason, amountCents, callback) {
        ec.getCustomerDetailsNumber(prefix, number, false, function (err, cache) {
            if (err || !cache) {
                return callback(new Error("Can not load account info"));
            }

            module.exports.addCreditNotesBySI(cache.serviceInstanceNumber, reason, amountCents, callback);
        });
    },

    /**
     *
     *
     */
    addCreditNotesBySI: function (serviceInstanceNumber, reason, amountCents, callback) {
        ec.getCustomerDetailsServiceInstance(serviceInstanceNumber, false, function (err, cache) {
            if (err || !cache) {
                return callback(new Error("Can not load account info"));
            }
            var amount = amountCents ? parseInt(amountCents) : 0;
            var date = new Date().toISOString().split("T")[0];

            var params = {
                "accountCurrencyAlias": "SGD",
                "accountList": [{
                    "accountName": cache.billingFullName,
                    "accountNumber": cache.billingAccountNumber,
                    "applyTax": "Y",
                    "exReceivedAmount": amount,
                    "receivedAmount": amount
                }],
                "eventAlias": "CREATE_CREDIT_EVENT",
                "paymentCurrencyAlias": "SGD",
                "paymentDate": date,
                "reasonAlias": reason,
                "staffId": "S000103",
                "staffName": "LWUser"
            };

            ec.createCreditNote(params, function (err, result) {
                if (err || !result) {
                    if (LOG) common.error("WaiverManager", "add credit notes error, err=" + err ? err.message : "'EMPTY RESPONSE'");
                    return callback(err ? err : new Error("Failed to add credit notes"));
                }

                if (LOG) common.log("WaiverManager", "add credit notes result=" + JSON.stringify(result));
                return callback(undefined, {
                    "waived": true,
                    "waiverCents": amount
                });
            });
        });
    }
}
