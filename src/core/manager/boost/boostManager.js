var request = require('request');
var config = require('../../../../config');
var common = require('../../../../lib/common');
var db = require('../../../../lib/db_handler');
var ec = require('../../../../lib/elitecore');
var elitecoreUtils = require('../../../../lib/manager/ec/elitecoreUtils');
var transactionManager = require('../account/transactionManager');

var usageManager = require('../account/usageManager');
var configManager = require('../config/configManager');
var profileManager = require('../profile/profileManager');

var LOG = config.LOGSENABLED;
var COUNT_POSTPONED_AUTOBOOST = 25;

module.exports = {

    /**
     *
     */
    loadFreeBoostList: function (prefix, number, callback) {
        if (!number || !prefix) {
            return callback(new Error("Params are incorrect", "PARAMS_ERROR"));
        }

        ec.getCustomerDetailsNumber(prefix, number, false, function (err, cache) {
            if (err || !cache) {
                return callback(new Error("User is not found", "USER_NOT_FOUND"));
            }

            var query = "SELECT id, boost_product_id FROM promoBoost WHERE " +
                "service_instance_number=" + db.escape(cache.serviceInstanceNumber) + " AND used=0 AND processing=0 ORDER BY added_ts";
            if (LOG) common.log("BoostManger", "query=" + query);

            db.query_noerr(query, function (result) {
                var items = [];
                if (result && result.length > 0) {
                    result.forEach(function (item) {
                        var product = ec.findPackage(ec.packages(), item.boost_product_id);
                        if (product) {
                            items.push({
                                id: item.id,
                                product_id: product.id,
                                product_kb: product.kb,
                                product_name: product.name
                            })
                        }
                    });
                }

                return callback(undefined, items);
            });
        });
    },

    /**
     *
     */
    useFreeBoost: function (prefix, number, id, callback) {
        if (!number || !prefix || !id) {
            return callback(new Error("Params are incorrect", "PARAMS_ERROR"));
        }

        ec.getCustomerDetailsNumber(prefix, number, false, function (err, cache) {
            if (err || !cache) {
                return callback(new Error("User is not found", "USER_NOT_FOUND"));
            }

            var query = "SELECT * FROM promoBoost WHERE id=" + db.escape(parseInt(id));
            db.query_noerr(query, function (result) {
                if (!result || !result[0]) {
                    if (LOG) common.log("BoostManger", "free boost not found in db");
                    return callback(new Error("Free boost not found in db", "NOT_FOUND"));
                }

                var item = result[0];
                if (item.used) {
                    if (LOG) common.log("BoostManger", "already used");
                    return callback(new Error("Already used", "ALREADY_USED"));
                } else if (item.processing) {
                    if (LOG) common.log("BoostManger", "processing");
                    return callback(new Error("Processing", "PROCESSING"));
                }

                var product = ec.findPackage(ec.packages(), item.boost_product_id);
                if (!product) {
                    if (LOG) common.log("BoostManger", "product not found, product_id=" + item.boost_product_id);
                    return callback(new Error("Product not found", "PRODUCT_NOT_FOUND_EXCEEDED"));
                }

                var updateQuery = "UPDATE promoBoost SET processing=1 WHERE id=" + db.escape(parseInt(id));
                db.query_err(updateQuery, function (err, result) {
                    if (err) {
                        if (LOG) common.error("BoostManger", "can update current item in db, err.code=" + err.code);
                        return callback(new Error("Can update current item in db", "CAN_NOT_UPDATE"));
                    }

                    subscribeBoostProduct(prefix, number, product.id, false, undefined, function (err) {
                        if (err) {
                            if (LOG) common.log("BoostManger", "failed to subscribe for an addon, product_id=" + item.product_id);
                            var updateQuery = "UPDATE promoBoost SET processing=0 WHERE id=" + db.escape(parseInt(id));

                            db.query_err(updateQuery, function () {
                                if (LOG) common.log("BoostManger", "item marked as not used");
                                return callback(new Error("Subscription failed", "EC_ERROR"));
                            });
                        } else {
                            if (LOG) common.log("BoostManger", "succeed to subscribe for an addon, product_id=" + product.id);
                            var updateQuery = "UPDATE promoBoost SET processing=0,used=1,used_ts="
                                + db.escape(new Date().toISOString()) + " WHERE id=" + db.escape(parseInt(id));

                            db.query_err(updateQuery, function () {
                                return callback(undefined, {
                                    added: true,
                                    name: product.name,
                                    product_id: product.id,
                                    kb: product.kb
                                });
                            });
                        }
                    });
                });
            });
        });
    },

    /**
     *
     */
    addFreeBoost: function (productId, count, prefix, number, executionKey, callback) {
        ec.getCustomerDetailsNumber(prefix, number, false, function (err, cache) {
            if (err || !cache) {
                return callback(new Error("Free boost not found in db", "USER_NOT_FOUND"));
            }

            var product = ec.findPackage(ec.packages(), productId);
            if (!product) {
                return callback(new Error("Can not find product", "PRODUCT_NOT_FOUND"));
            }

            var insertQuery = "INSERT INTO promoBoost (boost_product_id, service_instance_number) VALUES ";
            for (var i = 0; i < count; i++) {
                if (i > 0) insertQuery += ",";
                insertQuery += "(" + db.escape(productId) + ", " + db.escape(cache.serviceInstanceNumber) + ")";
            }

            db.query_err(insertQuery, function (err, rows) {
                if (err || !rows || !rows.affectedRows) {
                    if (LOG) common.log("BoostManger", "free boost not found in db");
                    return callback(new Error("Free boost not found in db", "NOT_FOUND"));
                }

                if (executionKey) {
                    sendNotification("freeboost_provided", prefix, number, product, count,
                        product.price, executionKey, function (err) {
                            return callback(undefined, {added: rows.affectedRows});
                        });
                } else {
                    return callback(undefined, {added: rows.affectedRows});
                }
            });
        });
    },

    /**
     *
     */
    addAutoBoost: function (prefix, number, checkData, executionKey, callback) {
        if (!number || !prefix) {
            return callback(new Error("Params are incorrect", "PARAMS_ERROR"));
        }

        var cache = "cache";
        var key = "autoboost_" + prefix + "_" + number;
        var expire = 30;

        db.cache_lock(cache, key, {
            prefix: prefix,
            number: number
        }, expire * 1000, function (lock) {
            if (lock) {

                var unlock = function () {
                    db.cache_del(cache, key);
                }

                var subscribe = function (autoBoostProduct) {
                    subscribeBoostProduct(prefix, number, autoBoostProduct, true, executionKey, function (err, result) {
                        if (err) {
                            unlock();
                            if (LOG) common.error("BoostManger", "autoboost subscription error, err=" +
                            err.message + ", result=" + JSON.stringify(result));
                            return callback(new Error("Free boost not found in db", "NOT_FOUND"), {added: false});
                        }

                        if (LOG) common.log("BoostManger", "autoboost added, result=" + JSON.stringify(result));

                        unlock();
                        callback(undefined, {
                            added: true,
                            subscriptionResult: result
                        });
                    });
                };

                var sendAutoboostDisabledNotification = function (callback) {
                    if (typeof(profileManager.loadASelfcareSetting) != 'function') {     // remove this if issue is fixed
                        common.error("boostManager sendAutoboostDisabledNotification 1", "profileManager.loadASelfcareSetting is not a function");
                        profileManager = require('../profile/profileManager');
                    }
                    profileManager.loadASelfcareSetting(prefix, number, "autoboost_disabled_notified", executionKey,
                        function (err, type, value) {
                            if (err) {
                                if (LOG) common.error("BoostManger", "can not send 'disabled' "
                                + "notification, err=" + err.getMessage());
                                return;
                            }

                            if (!value) {
                                sendNotification("autoboost_disabled", prefix, number, undefined, undefined,
                                    executionKey, function (err) {
                                        // do nothing
                                    });

                                if (typeof(profileManager.saveSetting) != 'function') {    // remove this if issue is fixed
                                    common.error("boostManager sendAutoboostDisabledNotification 2", "profileManager.saveSetting is not a function");
                                    profileManager = require('../profile/profileManager');
                                }
                                profileManager.saveSetting(prefix, number, "selfcare", "autoboost_disabled_notified",
                                    "Boolean", true, executionKey, function () {
                                        callback(undefined, {send: true});
                                    })
                            } else {
                                callback(undefined, {send: false});
                            }
                        });
                };

                var sendRunningOutOfDataWithFreeFlowNotification = function (serviceInstanceNumber, billingCycleStartDay, callback) {
                    var activity = "autoboost_not_required_free_3g";
                    var dates = elitecoreUtils.calculateBillCycle(billingCycleStartDay, undefined, new Date());

                    db.notifications_logbook_get({
                            serviceInstanceNumber: serviceInstanceNumber,
                            activity: activity,
                            ts: {"$gt": dates.startDateUTC.getTime()}
                        },
                        {}, {"ts": -1}, function (logs) {
                            var exists = logs && logs.length > 0;
                            if (!exists) {
                                sendNotification(activity, prefix, number, undefined, undefined, undefined,
                                    executionKey, function (err) {
                                        callback(undefined, {send: true});
                                    });
                            } else {
                                callback(undefined, {send: false});
                            }
                        });
                };

                var checkPersonalSetting = function (autoBoostProduct) {
                    if (typeof(profileManager.loadASelfcareSetting) != 'function') {    // remove this if issue is fixed
                        common.error("boostManager checkPersonalSetting 1", "profileManager.loadASelfcareSetting is not a function");
                        profileManager = require('../profile/profileManager');
                    }
                    profileManager.loadASelfcareSetting(prefix, number, "autoboost_limit", executionKey,
                        function (err, type, value) {
                            if (err) {
                                unlock();
                                return callback(err);
                            }

                            // -1 - No Limits
                            // 0 - Disables
                            // 1 .. * - Not supported for now

                            if (value == 0) {
                                sendAutoboostDisabledNotification(function (err, result) {
                                    unlock();

                                    if (LOG) common.log("BoostManger", "user disabled autoboost");
                                    return callback(new Error("User disabled autoboost"),
                                        {added: false, notificationResult: result});
                                });
                            } else {
                                subscribe(autoBoostProduct);
                            }
                        });
                };

                var computeData = function (autoBoostProduct) {
                    ec.getCustomerDetailsNumber(prefix, number, true, function (err, cache) {
                        if (err || !cache) {
                            unlock();
                            return callback(new Error("User is not found",
                                "USER_NOT_FOUND"), {added: false});
                        }

                        usageManager.computeUsage(prefix, number, function (err, usageResult) {
                            if (err || !usageResult || !usageResult.data || !usageResult.data.basic || !usageResult.data.extra
                                || !usageResult.data.boost || !usageResult.data.bonus) {
                                unlock();
                                return callback(new Error("Data is not loaded, can not auto boost",
                                    "DATA_NOT_FOUND"), {added: false});
                            }

                            var totalLeftMb = parseInt((usageResult.data.basic.left + usageResult.data.extra.left
                            + usageResult.data.plus.left + usageResult.data.boost.left + usageResult.data.bonus.left) / 1024);
                            var autoboostThreshold = (cache.serviceInstanceBasePlanName === 'CirclesSwitch') ? 450 : 100;


                            if (!checkData) {
                                checkPersonalSetting(autoBoostProduct);
                            } else if (totalLeftMb <= autoboostThreshold) {
                                // subscribe autoboost only if left data is lower 100 MB

                                var freeFlow = cache.serviceInstanceUnlimitedData && cache.serviceInstanceUnlimitedData.enabled;
                                var billingCycleStartDay = cache.billingCycleStartDay;

                                if (freeFlow) {
                                    sendRunningOutOfDataWithFreeFlowNotification(cache.serviceInstanceNumber, billingCycleStartDay,
                                        function (err, result) {
                                            unlock();

                                            if (LOG) common.log("BoostManger", "User has FreeFlow Data");
                                            return callback(new Error("User has FreeFlow Data"),
                                                {added: false, notificationResult: result});
                                        })
                                } else if(cache.serviceInstanceBasePlanName === 'CirclesSwitch'){
                                    unlock();
                                    return callback(new Error("User has Circles Switch"), {added: false});
                                } else {
                                    checkPersonalSetting(autoBoostProduct);
                                }
                            } else {
                                unlock();
                                if (LOG) common.log("BoostManger", "no need to add autoboost, totalLeftMb=" + totalLeftMb);
                                return callback(new Error("No need to add autoboost, totalLeftMb=" + totalLeftMb), {
                                    added: false,
                                    reason: "User data >70 MB (" + totalLeftMb + " MB left)"
                                });
                            }
                        });
                    });
                };

                var checkConfig = function () {
                    configManager.loadConfigMapForKey("notification", "data_warning_notification", function (error, item) {
                        if(!item){
                            unlock();
                            return callback(new Error("No data_warning_notification for the user"), {added: false});
                        }
                        if (!item.options.autoboost_enabled) {
                            unlock();
                            if (LOG) common.log("BoostManger", "Adding of autoboost disabled");
                            return callback(new Error("Autoboost disabled"), {added: false});
                        }

                        var productId = item && item.options ? item.options.autoboost_product : undefined;
                        var product = productId ? ec.findPackage(ec.packages(), productId) : undefined;

                        if (!product) {
                            unlock();
                            if (LOG) common.log("BoostManger", "Autoboost product not found, productId=" + productId);
                            return callback(new Error("Autoboost product not found, productId=" + productId), {added: false});
                        }

                        computeData(productId);
                    });
                };

                checkConfig();
            } else {
                if (LOG) common.error("BoostManger", "autoboost subscription is in progress.");
                return callback(new Error("Autoboost subscription is in progress."));
            }
        });
    }
}

function subscribeBoostProduct(prefix, number, productId, waitForResponse, executionKey, callback) {
    ec.getCustomerDetailsNumber(prefix, number, false, function (err, cache) {
        if (err || !cache) {
            return callback(new Error("User is not found", "USER_NOT_FOUND"));
        }

        var product = ec.findPackage(ec.packages(), productId);
        if (!product) {
            if (LOG) common.log("BoostManger", "subscribeBoostProduct: product not found, product_id=" + productId);
            return callback(new Error("Product not found", "PRODUCT_NOT_FOUND_EXCEEDED"));
        }

        if (LOG) common.log("BoostManger", "subscribeBoostProduct: subscribe for a product, number=" + number
        + ", account=" + cache.account + ", productId=" + product.id);

        var startSubscription = function (eCommResult, countAdded) {
            if (LOG) common.log("BoostManger", "subscribeBoostProduct[startSubscription]: number=" + number);

            ec.addonSubscribe(cache.number, cache.serviceInstanceNumber, [{
                "product_id": product.id,
                "recurrent": product.recurrent,
                "effect": product.sub_effect
            }], cache.billing_cycle, function (err, result) {
                if (!err && result) {
                    // in order to update boosts history, need to reload cache
                    // no need to wait for response, return response tp original caller rightaway

                    ec.getCustomerDetailsNumber(prefix, number, true, function (err, cache) {
                    });

                    callback(undefined, {
                        added: true, countAdded: (countAdded + 1),
                        ecResult: result, eCommResult: eCommResult
                    });
                } else {
                    callback(new Error("Product has not been subscribed"), {
                        added: false, eCommResult: eCommResult, ecResult: result
                    });
                }
            });
        }

        var executePayment = function (countAdded) {
            if (LOG) common.log("BoostManger", "subscribeBoostProduct[executePayment]: number=" + number);

            var description = (!product.name ? "Payment" : "Payment for " + product.name);
            transactionManager.performPayment(cache.billingAccountNumber, {
                type: transactionManager.TYPE_BOOST_PAYMENT,
                amount: product.price,
                internalId: product.id,
                metadata: {
                    actions: [
                        transactionManager.createAdvancedPaymentAction(),
                        transactionManager.createAddonSubAction(description, product.id, false, 0, waitForResponse)
                    ]
                }
            }, (status, transaction, callback) => {
                if (LOG) common.log("BoostManger", "subscribeBoostProduct[executePayment]: transaction status " +
                "changed, number=" + number + ", status=" + status);
                callback();
            }, (err, result) => {

                sendNotification(err ? "autoboost_payment_failure" : "autoboost_payment_success",
                    prefix, number, product, (countAdded + 1), product.price, executionKey, (err) => {
                        // do nothing
                    });

                if (err) {
                    if (LOG) common.error("BoostManger", "subscribeBoostProduct: failed to charge a customer for an autoboost, " +
                    "result=" + JSON.stringify(result));

                    // disable autoboost

                    if (typeof(profileManager.saveSetting) != 'function') {    // remove this if issue is fixed
                        common.error("boostManager executePayment 1", "profileManager.saveSetting is not a function");
                        profileManager = require('../profile/profileManager');
                    }
                    profileManager.saveSetting(prefix, number, "selfcare", "autoboost_limit", "Integer", 0,
                        executionKey, function (error, result) {
                            // do nothing
                        });

                    return callback(new Error("Failed to charge for an autoboost " +
                    "(payment mandatory for #" + (countAdded + 1) + ")"), {
                        added: false, eCommResult: result, ecResult: result
                    });
                }

                if (LOG) common.log("BoostManger", "payment success for an autoboost, " +
                "result=" + JSON.stringify(result));

                callback(undefined, {
                    added: true, countAdded: (countAdded + 1), eCommResult: result
                });

            });
        }

        var checkAutoboostCount = function () {
            if (LOG) common.log("BoostManger", "subscribeBoostProduct[checkAutoboostCount]: number=" + number +
            ", sin=" + cache.serviceInstanceNumber);

            var currentBillCycle = ec.nextBill(cache.billing_cycle);
            db.notifications_logbookwebhook_count_get({
                serviceInstanceNumber: cache.serviceInstanceNumber,
                activity: 'autoboost_added',
                ts: {'$gt': currentBillCycle.startDateUTC.getTime()}
            }, function (count) {
                if (LOG) common.log("BoostManger", "compute autoboost count, " +
                "count=" + count + ", number=" + number);

                if (count > COUNT_POSTPONED_AUTOBOOST) {
                    executePayment(count);
                } else {
                    startSubscription(undefined, count);
                }
            });
        }

        if (product.app === "auto") {
            checkAutoboostCount();
        } else {
            startSubscription(undefined, 0);
        }
    });
}

function sendNotification(activity, prefix, number, product, count, amount, notificationKey, callback) {
    var url = "http://" + config.MYSELF + ":" + config.WEBPORT +
        "/api/1/webhook/notifications/internal/" + notificationKey;

    request({
        uri: url,
        method: 'POST',
        timeout: 20000,
        json: {
            activity: activity,
            prefix: prefix,
            number: number,
            variables: {
                count: count,
                addonname: product ? product.name : undefined,
                amount: amount
            }
        }
    }, function (err, response, body) {
        if (err) {
            return callback(err);
        }
        if (callback) callback();
    });
}
