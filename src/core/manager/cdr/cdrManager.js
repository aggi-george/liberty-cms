var async = require('async');
var bluebird = require('bluebird');
var db = require(__lib + '/db_handler');
var ec = require(__lib + '/elitecore');
var config = require(__base + '/config');
var common = require(__lib + '/common');
var elitecoreConnector = require(__lib + '/manager/ec/elitecoreConnector');
var elitecorePackagesService = require(__lib + '/manager/ec/elitecorePackagesService');
var elitecoreUtils = require(__lib + '/manager/ec/elitecoreUtils');

var LOG = config.LOGSENABLED;

module.exports = {

    ecRGID: [
            { "id" : 300, "label" : "GENERAL" },
            { "id" : 301, "label" : "WHATSAPP" },
            { "id" : 302, "label" : "FB MESSENGER" },
            { "id" : 303, "label" : "FB" },
            { "id" : 319, "label" : "MMS" },
    ],

    elitecore: function(params, callback) {

        var ecDataServiceId = 3;
        var ecVoiceServiceId = 10;
        var ecSMSServiceId = 107;
        var ecMMSRatingGroupId = 319;
        var rgidDecode = "DECODE(ratinggroupid, " +
            module.exports.ecRGID.map(function (o) { return o.id + ",'" + o.label + "'"}).toString() +
            ", 'OTHER')";

        if (!params || !params.serviceInstanceNumber || !params.type || !params.start || !params.end) {
            common.error("elitecore CDR", "Incomplete Parameters");
            return callback(new Error("Incomplete Parameters"));
        }
        var category = params.type.split('_')[0]; 
        var type = params.type.split('_')[1]; 
        var startEnd = " AND callstart>=TO_TIMESTAMP(" + db.escape(params.start) + ", 'YYYY-MM-DD HH24:MI:SS') " +
            " AND callend<TO_TIMESTAMP(" + db.escape(params.end) + ", 'YYYY-MM-DD HH24:MI:SS') ";
        var serviceId = "";
        var general5 = "";
        var accounted = "";
        var summarySelect = "";
        if (type == "DATA") {
            if (params.rgid) serviceId += " AND ratinggroupid=" + params.rgid + " ";
            serviceId += " AND serviceid=" + ecDataServiceId + " ";
            accounted = "accounteddatatransfer/1024/1024 accounteddatatransfer, sessiondatatransfer/1024/1024 sessiondatatransfer, ";
            summarySelect = "SUM(sessiondatatransfer)/1024 sessiondata, SUM(accounteddatatransfer)/1024 accounteddata, ";
        } else if (type == "VOICE") {
            if (category == "GLOBAL") serviceId += " AND usagetype!=10 AND usagetype!=11 ";
            serviceId += " AND serviceid=" + ecVoiceServiceId + " ";
            accounted = "accountedtime, '' da, ";
            summarySelect = "SUM(accountedtime) accountedtime, ";
	} else if (type == "MMS") {
            serviceId += " AND serviceid=" + ecDataServiceId + " AND ratinggroupid=" + ecMMSRatingGroupId + " ";
            accounted = "accounteddatatransfer/1024/1024 accounteddatatransfer, sessiondatatransfer/1024/1024 sessiondatatransfer, ";
            summarySelect = "SUM(sessiondatatransfer)/1024 sessiondata, SUM(accounteddatatransfer)/1024 accounteddata, ";
        } else if (type == "SMS") {
            if (category == "IMSG") serviceId += " AND calledstationid LIKE '44778620%' ";
            else if (category == "GLOBAL") serviceId += " AND usagetype=717 ";
            serviceId += " AND serviceid=" + ecSMSServiceId + " ";
            accounted = "'' se, '' da, ";
            summarySelect = "";
        } else {
            return callback(new Error("Unknown Type"));
        }
        if (category == "ROAMING") {
            general5 += " AND (General5!='525' AND General5!='52503') ";
        }
        var query = "SELECT callstart, callend, accountedcost/100 accountedcost, " + accounted +
            " DECODE(usagetype, " +
            "     715, 'ROAMING DATA', 714, 'LOCAL DATA', " +
            "     716, 'LOCAL SMS', 717, 'GLOBAL SMS', " +
            "     10, 'VOICE LOCAL IN', 11, 'VOICE LOCAL OUT', " +
            "     'OTHER') usagetype, " +
            " " + rgidDecode + " ratinggroupid, " +
            " callingstationid, calledstationid, CONCAT('PRD',LPAD(packageid, 5, '0')) packageid, rownum row_num FROM " +
            " tblcdrpostpaid WHERE username=" + db.escape(params.serviceInstanceNumber) +
            " " + serviceId + " " + general5; 
        var queryDetail = (params.displayStart && params.displayLength) ? "SELECT * FROM (" + query + " " + startEnd + " AND " +
            " ROWNUM<=" + (params.displayStart + params.displayLength) + ") WHERE row_num>" + params.displayStart :
            query + " " + startEnd;
        if (LOG) common.log("elitecore CDR detail", queryDetail);

        var querySummary = "SELECT COUNT(*) total, SUM(accountedcost) accountedcost, " + summarySelect + " ratinggroupid FROM " +
            " (" + query + " " + startEnd + ") GROUP BY ratinggroupid";
        if (LOG) common.log("elitecore CDR summary", querySummary);

        var tasks = {
            "list" : function(cb) { db.oracle.query(queryDetail, function (e, r) {
                if (r && r.rows) {
                    elitecorePackagesService.loadHierarchy(function (err, hierarchy) {
                        r.rows.forEach(function(item) {
                            item[9] = elitecoreUtils.findPackageById(hierarchy, item[9]).name; 
                        });
                        cb(e, r.rows);
                    });
                } else cb(e, []);
            }) },
            "summary" : function(cb) { db.oracle.query(querySummary, { "outFormat" : "object" }, function (e, r) {
                if (r && r.rows) cb(e, r.rows);
                else cb(e, []);
            }) },
        }
        async.parallel(tasks, callback);
    },

    reassurance: (params, callback) => {
        const start = new Date(params.start).getTime();
        const end = new Date(params.end).getTime();
        if (!params.type) return callback(new Error('Unknown type'));
        const suffix = params.request ? 'Req' : 'Res';
        let match = { '$match': { time: { '$gte': start, '$lt': end } } };
        if (params.mobile) match['$match']['User-Name'] = params.mobile;
        if (params.session) match['$match']['Session-Id'] = params.session;
        const sort = { '$sort': { time: 1 }};
        const skip = params.displayStart == parseInt(params.displayStart) ? { '$skip': params.displayStart } : undefined;
        const limit = params.displayLength == parseInt(params.displayLength) ? { '$limit': params.displayLength } : undefined;
        const project = { '$project': {
        }};
        const unwind = { '$unwind' : '$multi'};
        const group = { '$group': {
            _id: 'summary',
            TOTAL: { '$sum': 1 },
            SESSIONDATA: { '$sum': '$CC-Total-Octets' },
            ACCOUNTEDTIME: { '$sum': '$CC-Time' },
        }};
        const summaryPipeline = [ match, group ];
        const listPipeline = [ match, sort ];
        if (skip) listPipeline.push(skip);
        if (limit) listPipeline.push(limit);
        const summaryPromise = new bluebird.Promise((resolve, reject) => {
            if (LOG) common.log(`reassurance summary ${params.type}${suffix}`, JSON.stringify(summaryPipeline));
            db[`${params.type}${suffix}`].aggregate(summaryPipeline, { readPreference: db.readPreference.SECONDARY_PREFERRED }, (err, result) => {
                if (err) return reject(err);
                const summary = result && result[0] ? result : [ { RATINGGROUPID: '', TOTAL: 0, SESSIONDATA: 0, ACCOUNTEDCOST: 9 } ];
                delete summary._id;
                summary.forEach((o) => { o.SESSIONDATA = o.SESSIONDATA / 1024 / 1024 / 1024 });
                resolve({ summary });
            });
        });
        const listPromise = new bluebird.Promise((resolve, reject) => {
            if (LOG) common.log(`reassurance list ${params.type}${suffix}`, JSON.stringify(listPipeline));
            db[`${params.type}${suffix}`].aggregate(listPipeline, { readPreference: db.readPreference.SECONDARY_PREFERRED }, (err, result) => {
                if (err) return reject(err);
                resolve({ list: result });
            });
        });
        new bluebird.all([
            summaryPromise, listPromise
        ]).then((result) => {
            let obj = {};
            result.forEach((item) => {
                if (item && item.summary) obj.summary = item.summary;
                if (item && item.list) obj.list = item.list;
            });
            callback(undefined, obj);
        }).catch((err) => {
            callback(err);
        });
    },

}
