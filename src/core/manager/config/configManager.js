var config = require('../../../../config');
var common = require('../../../../lib/common');
var db = require('../../../../lib/db_handler');
var configMapping = require('./configMapping');

var LOG = config.LOGSENABLED;

module.exports = {

    /**
     *
     */
    loadConfigMapForKey: function (group, key, callback) {
        module.exports.loadConfigMapForKeys(group, [key], function (error, resultList, resultMap) {
            if (callback) callback(error, resultMap ? resultMap[key] : undefined);
        });
    },

    /**
     *
     */
    loadConfigMapForGroup: function (group, callback) {
        module.exports.loadConfigMapForKeys(group, undefined, callback);
    },

    /**
     *
     */
    loadConfigMapForKeys: function (group, keys, callback) {
        var settingsMap = {};
        var settingsKeysArray = [];

        var configItemsMapping = group ? configMapping.mapping[group] : undefined;
        if (configItemsMapping) {
            configItemsMapping.forEach(function (item) {
                if (!keys || keys.indexOf(item.key) >= 0) {
                    settingsMap[item.key] = item;
                    settingsKeysArray.push(item.key);
                }
            });
        }

        if (settingsKeysArray.length == 0) {
            return callback(undefined, [], {});
        }

        var selectQuery = "SELECT * FROM configMap LEFT OUTER JOIN configOptions ON (configMap.id = configOptions.config_id) " +
            "WHERE configMap.config_key IN (" + db.arrayToString(settingsKeysArray) + ") ORDER BY configMap.config_key";
        //if (LOG) common.log("loadConfigMapForKeys", "selectQuery=" + selectQuery);

        db.query_err(selectQuery, function (err, rows) {
            if (err || !rows) {
                if (LOG) common.log("loadConfigMapForKeys", "can not load config");
                return callback(new Error("Database error"));
            }

            var dbSettingsMap = {};
            rows.forEach(function (item) {
                var key = item.config_key;
                var obj = {
                    key: item.option_key,
                    productId: item.product_id,
                    promoCategoryId: item.promo_category_id,
                    value: item.value,
                    type: item.type
                };

                if (!dbSettingsMap[key]) {
                    dbSettingsMap[key] = {
                        key: key,
                        options: [],
                        optionsMap: {}
                    };
                }

                dbSettingsMap[key].options.push(obj);
                dbSettingsMap[key].optionsMap[obj.key] = obj;
            });

            var resultMap = {};
            var resultList = [];
            settingsKeysArray.forEach(function (settingKey) {
                var item = settingsMap[settingKey];
                var dbItem = dbSettingsMap[settingKey];

                var resultItem = {
                    key: item.key,
                    options: {
                        metadata: {}
                    },
                    metadata: {
                        display: item.display,
                        optionKeys: []
                    }
                };

                if (item.options) {
                    item.options.forEach(function (option) {
                        var dbOption = dbItem ? dbItem.optionsMap[option.key] : undefined;

                        if (dbOption && dbOption.type === option.type) {
                            resultItem.options[option.key] = getParsedValueForType(dbOption.productId,
                                dbOption.promoCategoryId, dbOption.value, option.type, option.cleanIfPassed);
                        }

                        if (!resultItem.options[option.key]) {
                            resultItem.options[option.key] = getDefaultValueForType(option.type);
                        }

                        resultItem.metadata.optionKeys.push(option.key);
                        resultItem.options.metadata[option.key] = option;
                    });
                    resultItem.options.metadata.currentDate = new Date().toISOString();
                }

                resultList.push(resultItem);
                resultMap[resultItem.key] = resultItem;
            });

            if (callback) {
                callback(undefined, resultList, resultMap)
            }
        });
    },

    /**
     *
     */
    updateConfigItem: function (key, options, callback) {
        if (LOG) common.log("updateConfigItem", "Update request, key=" + key + ", options=" + JSON.stringify(options));

        if (!key || !options) {
            if (LOG) common.error("updateConfigItem", "Params are missing, key or options");
            return callback(new Error("Params are missing"));
        }

        var settingConfig;
        Object.keys(configMapping.mapping).forEach(function (group) {
            configMapping.mapping[group].forEach(function (item) {
                if (item && item.key === key) {
                    settingConfig = item;
                }
            });
        });

        if (!settingConfig) {
            if (LOG) common.error("updateConfigItem", "Can not load config mapping");
            return callback(new Error("Can not find config mapping"));
        }

        db.transaction(function (err, connection) {
            if (!connection || err) {
                if (LOG) common.error("updateConfigItem", "Database transaction error");
                return callback(new Error("Database transaction error"));
            } else {

                var rollbackCallback = function (reason, err) {
                    connection.rollback(function () {
                        connection.release();
                        if (LOG) common.error("updateConfigItem", "Can not create setting. " + reason + " " + err);
                        return callback(new Error("Can not create setting. " + reason));
                    });
                };

                var commitCallback = function () {
                    connection.commit(function (err) {
                        if (err) {
                            rollbackCallback("Commit failed.", err)
                        } else {
                            connection.release();
                            callback();
                        }
                    });
                };

                var updateOptionsCallback = function (settingId) {
                    var optionKeys = [];
                    var updateQueries = [];

                    if (settingConfig.options) {
                        settingConfig.options.forEach(function (item) {
                            if (item && item.key && item.type) {
                                var value = getDbTypeValueForType(options[item.key], options[item.key],
                                    options[item.key], item.type);
                                var escapedValue;
                                var escapedProductId;
                                var escapedPromoCategoryId;

                                var escapedKey = db.escape(item.key);
                                var escapedType = db.escape(item.type);

                                if (item.type === 'ProductId') {
                                    escapedProductId = value ? db.escape("" + value) : "NULL";
                                    escapedPromoCategoryId = 'NULL';
                                    escapedValue = 'NULL';
                                } else if (item.type === 'PromoCategoryId') {
                                    escapedProductId = 'NULL';
                                    escapedPromoCategoryId = value ? db.escape(parseInt(value)) : "NULL";
                                    escapedValue = 'NULL';
                                } else {
                                    escapedPromoCategoryId = 'NULL';
                                    escapedProductId = 'NULL';
                                    escapedValue = value ? db.escape("" + value) : "NULL";
                                }

                                optionKeys.push(item.key);
                                updateQueries.push("INSERT INTO configOptions (config_id, option_key, product_id, "
                                + "promo_category_id, value, type) " + "VALUES (" + settingId + ", " + escapedKey + ","
                                + escapedProductId + "," + escapedPromoCategoryId + "," + escapedValue + ","
                                + escapedType + ") " + "ON DUPLICATE KEY UPDATE product_id=" + escapedProductId
                                + ", promo_category_id=" + escapedPromoCategoryId + ", value=" + escapedValue
                                + ", type=" + escapedType);
                            } else {
                                if (LOG) common.error("updateConfigItem",
                                    "Setting is not properly described, key or type is missing");
                            }
                        });
                    }

                    updateQueries.push("DELETE FROM configOptions WHERE config_id=" + settingId
                    + " AND option_key NOT IN (" + db.arrayToString(optionKeys) + ")");

                    if (LOG) common.log("updateConfigItem", "Queries=" + JSON.stringify(updateQueries));

                    var countProcessed = 0;
                    var failed = false;
                    updateQueries.forEach(function (query) {
                        connection.query(query, function (err, result) {
                            countProcessed++;
                            // ignore if already failed
                            if (failed) {
                                return;
                            }

                            if (err || !result) {
                                failed = true;
                                return rollbackCallback("Options update failed on operation #" + countProcessed, err);
                            }

                            if (countProcessed == updateQueries.length) {
                                if (LOG) common.log("updateConfigItem", "Option update succeed, count=" + countProcessed);
                                return commitCallback();
                            }
                        });
                    });
                };

                var updateSettingCallback = function (key) {
                    var queryCreateSetting = "INSERT INTO configMap (config_key) VALUES (" +
                        db.escape(key) + ") ON DUPLICATE KEY UPDATE config_key=config_key";

                    connection.query(queryCreateSetting, function (err, result) {
                        if (err || !result) {
                            return rollbackCallback("Insert config failed", err);
                        }

                        if (result.insertId == 0) {
                            // item was already inserted, need to query to get option item
                            var queryGetSetting = "SELECT id FROM configMap WHERE config_key=" + db.escape(key);
                            connection.query(queryGetSetting, function (err, result) {
                                if (err || !result || !result[0] || !result[0].id) {
                                    return rollbackCallback("Select config failed", err);
                                }
                                if (LOG) common.log("updateConfigItem", "Loaded setting, key=" + key + ", insertId=" + result[0].id);
                                updateOptionsCallback(result[0].id);
                            });
                        } else {
                            if (LOG) common.log("updateConfigItem", "Added new setting, key=" + key + ", insertId=" + result.insertId);
                            updateOptionsCallback(result.insertId);
                        }
                    });
                };

                updateSettingCallback(key);
            }
        });
    },

    /**
     *
     *
     */
    parseTime: function (value) {
        return parseTimeString(value);
    },

}

function getDefaultValueForType(type) {
    if (type === 'Integer') {
        return 0;
    } else if (type === 'String') {
        return '';
    } else if (type === 'Boolean') {
        return false;
    } else if (type === 'ProductId') {
        return '';
    } else if (type === 'Time') {
        return '00:00:00';
    } else if (type === 'PromoCategoryId') {
        return 0;
    } else {
        return undefined;
    }
}

function getParsedValueForType(productId, promoCategoryId, value, type, cleanIfPassed) {
    if (type === 'Integer') {
        return value ? parseInt(value) : getDefaultValueForType(type);
    } else if (type === 'String') {
        return value ? ("" + value) : getDefaultValueForType(type);
    } else if (type === 'Boolean') {
        return value ? parseInt(value) > 0 : getDefaultValueForType(type);
    } else if (type === 'ProductId') {
        return productId ? ("" + productId) : getDefaultValueForType(type);
    } else if (type === 'PromoCategoryId') {
        return promoCategoryId ? parseInt(promoCategoryId) : getDefaultValueForType(type);
    } else if (type === 'DateTime') {
        try {
            if (!value) {
                return ""
            }
            var date = new Date(value);
            if (cleanIfPassed) {
                var currentDate = new Date();
                if (currentDate.getTime() > date.getTime()) {
                    return "";
                }
            }
            return date.toISOString();
        } catch (e) {
            return "";
        }
    } else if (type === 'Time') {
        return value;
    } else {
        return undefined;
    }
}

function getDbTypeValueForType(productId, promoCategoryId, value, type) {
    if (type === 'Integer') {
        return value ? ("" + parseInt(value)) : 0;
    } else if (type === 'String') {
        return value ? ("" + value) : "";
    } else if (type === 'Boolean') {
        return value === true || parseInt(value) ? "1" : "0";
    } else if (type === 'DateTime') {
        try {
            return !value ? "" : new Date(new Date(value).getTime() - 8 * 60 * 60 * 1000).toISOString();
        } catch (e) {
            return undefined;
        }
    } else if (type === 'Time') {
        return parseTimeString(value).text;
    } else if (type === 'ProductId') {
        return productId && productId.length > 0 ? productId : undefined;
    } else if (type === 'PromoCategoryId') {
        return promoCategoryId && parseInt(promoCategoryId) > 0 ? parseInt(promoCategoryId) : undefined;
    } else {
        return undefined;
    }
}

function parseTimeString(timeString) {
    if (timeString) {
        var params = timeString.split(":");
        if (params.length >= 2) {
            var hours = parseInt(params[0]);
            var mins = parseInt(params[1]);
            var secs = params.length > 2 ? parseInt(params[2]) : 0;

            if (!hours || hours < -1 || hours > 23) {
                hours = 0;
            }
            if (!mins || mins < -1 || mins > 59) {
                mins = 0;
            }
            if (!secs || secs < -1 || secs > 59) {
                secs = 0;
            }

            var hoursString = hours > 9 ? "" + hours : "0" + hours;
            var minsString = mins > 9 ? "" + mins : "0" + mins;
            var secsString = secs > 9 ? "" + secs : "0" + secs;

            return {
                h: hours,
                m: mins,
                s: secs,
                text: hoursString + ":" + minsString + ":" + secsString
            }
        }
    }

    return {
        h: 0,
        m: 0,
        s: 0,
        text: "00:00:00"
    };
}
