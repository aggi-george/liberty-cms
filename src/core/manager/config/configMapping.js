exports.mapping = {
    elitecore: [
        {
            key: 'elitecore_bss_pool',
            display: 'BSS Pool Quarantine',
            options: [
                {
                    key: 'bss1_disabled_until',
                    type: 'DateTime',
                    cleanIfPassed: true,
                    display: 'BSS1 Disabled Until'
                },
                {
                    key: 'bss2_disabled_until',
                    type: 'DateTime',
                    cleanIfPassed: true,
                    display: 'BSS2 Disabled Until'
                },
                {
                    key: 'bss3_disabled_until',
                    type: 'DateTime',
                    cleanIfPassed: true,
                    display: 'BSS3 Disabled Until'
                },
                {
                    key: 'bss4_disabled_until',
                    type: 'DateTime',
                    cleanIfPassed: true,
                    display: 'BSS4 Disabled Until'
                },
                {
                    key: 'bssstaging_disabled_until',
                    type: 'DateTime',
                    cleanIfPassed: true,
                    display: 'BSS Staging Disabled Until'
                }
            ]
        },
        {
            key: 'elitecore_api',
            display: 'BSS API',
            options: [
                {
                    key: 'ec_cms_bss_api_disabled',
                    type: 'Boolean',
                    display: 'Offline Mode (CMS)'
                },
                {
                    key: 'ec_mobile_bss_api_disabled',
                    type: 'Boolean',
                    display: 'Offline Mode (MOBILE)'
                }
            ]
        }
    ],
    bonus: [
        {
            key: 'bonus_selfcare_install',
            display: 'CirclesCare Installation Bonus',
            options: [
                {
                    key: 'bonus_product_starting_m4',
                    type: 'ProductId',
                    display: 'Bonus Addon',
                    product: {
                        category: 'bonus'
                    }
                },
                {
                    key: 'enabled',
                    type: 'Boolean',
                    display: 'Enabled'
                }
            ]
        },
        {
            key: 'birthday_bonus',
            display: 'Birthday Bonus',
            options: [
                {
                    key: 'bonus_product',
                    type: 'ProductId',
                    display: 'Bonus Addon',
                    product: {
                        category: 'bonus'
                    }
                },
                {
                    key: 'valid_days',
                    type: 'Integer',
                    display: 'Valid Days'
                },
                {
                    key: 'enabled',
                    type: 'Boolean',
                    display: 'Enabled'
                }
            ]
        },
        {
            key: 'referral_bonus',
            display: 'Referral Bonus',
            options: [
                {
                    key: 'bonus_product_referrer',
                    type: 'ProductId',
                    display: 'Bonus Addon',
                    product: {
                        category: 'bonus'
                    }
                },
                {
                    key: 'discount_cents_joined',
                    type: 'Integer',
                    display: 'Registration Discount (₵)'
                },
                {
                    key: 'flash_sale_bonus_product',
                    type: 'ProductId',
                    display: 'Flash Sale Bonus Addon',
                    product: {
                        category: 'bonus'
                    }
                },
                {
                    key: 'flash_sale_start_date',
                    type: 'DateTime',
                    display: 'Flash Sale Start Date'
                },
                {
                    key: 'flash_sale_end_date',
                    type: 'DateTime',
                    display: 'Flash Sale End Date'
                },
                {
                    key: 'enabled',
                    type: 'Boolean',
                    display: 'Enabled'
                }
            ]
        },
        {
            key: 'loyalty_bonus',
            display: 'Loyalty Bonus',
            options: [
                {
                    key: 'bonus_product',
                    type: 'ProductId',
                    display: 'Bonus Addon',
                    product: {
                        category: 'bonus'
                    }
                },
                {
                    key: 'enabled',
                    type: 'Boolean',
                    display: 'Enabled'
                }
            ]
        },
        {
            key: 'retention_bonus',
            display: 'Retention Bonus',
            options: [
                {
                    key: 'retention_product',
                    type: 'ProductId',
                    display: 'Bonus Addon',
                    product: {
                        category: 'bonus'
                    }
                },
                {
                    key: 'enabled',
                    type: 'Boolean',
                    display: 'Enabled'
                }
            ]
        },
        {
            key: 'port_in_bonus',
            display: 'Port-In Bonus',
            options: [
                {
                    key: 'bonus_product_starting_M13',
                    type: 'ProductId',
                    display: 'Bonus Addon',
                    product: {
                        category: 'bonus'
                    }
                },
                {
                    key: 'enabled',
                    type: 'Boolean',
                    display: 'Enabled'
                }
            ]
        },
        {
            key: 'bonus_port_in_promo_campaign',
            display: 'Port-In Promo Campaign',
            options: [
                {
                    key: 'promo_category_tag',
                    type: 'String',
                    display: 'Promo Category Tag'
                },
                {
                    key: 'start_date',
                    type: 'DateTime',
                    display: 'Start Date'
                },
                {
                    key: 'end_date',
                    type: 'DateTime',
                    display: 'End Date'
                },
                {
                    key: 'enabled',
                    type: 'Boolean',
                    display: 'Enabled'
                }
            ]
        }
    ],
    bill: [
        {
            key: 'pay_now',
            display: 'Pay Now',
            options: [
                {
                    key: 'enabled',
                    type: 'Boolean',
                    display: 'Enabled'
                }
            ]
        }
    ],
    notification: [
        {
            key: 'data_warning_notification',
            display: 'BSS Low Data Warning Notification',
            options: [
                {
                    key: 'autoboost_product',
                    type: 'ProductId',
                    product: {
                        category: 'boost'
                    },
                    display: 'Autoboost Addon'
                },
                {
                    key: 'autoboost_enabled',
                    type: 'Boolean',
                    display: 'Autoboost Enabled'
                },
            ]
        }
    ],
    selfcare: [
        {
            key: 'settings',
            display: 'Settings',
            options: [
                {
                    key: 'help_show_whatsapp',
                    type: 'Boolean',
                    display: 'Show Help WhatsApp'
                },
                {
                    key: 'settings_show_autoboost_limit',
                    type: 'Boolean',
                    display: 'Show Settings Autoboost'
                },
                {
                    key: 'settings_show_port_in_later',
                    type: 'Boolean',
                    display: 'Show PortIn Later'
                },
                {
                    key: 'settings_show_all_test_number',
                    type: 'String',
                    display: 'Show All Settings (Test Number)'
                },
                {
                    key: 'latest_app_version_ios',
                    type: 'String',
                    display: 'Latest iOS app version'
                },
                {
                    key: 'latest_app_version_android',
                    type: 'String',
                    display: 'Latest Android app version'
                },
                {
                    key: 'bonus_banner_enabled',
                    type: 'Boolean',
                    display: 'Show Bonus Banner'
                }
            ]
        },
        {
            key: 'theme',
            display: 'Theme',
            options: [
                {
                    key: 'theme_header_primary_color',
                    type: 'String',
                    display: 'Header Primary Color'
                },
                {
                    key: 'theme_header_secondary_color',
                    type: 'String',
                    display: 'Header Secondary Color'
                },
                {
                    key: 'theme_header_icon',
                    type: 'String',
                    display: 'Header Icon'
                },
                {
                    key: 'theme_header_title',
                    type: 'String',
                    display: 'Header Title'
                },
                {
                    key: 'theme_image_preview',
                    type: 'String',
                    display: 'Promo Image Preview'
                },
                {
                    key: 'theme_image_full',
                    type: 'String',
                    display: 'Promo Image Full'
                },
                {
                    key: 'theme_enabled',
                    type: 'Boolean',
                    display: 'Enabled'
                }
            ]
        },
        {
            key: 'circles_switch',
            display: 'CirclesSwitch',
            options: [
                {
                    key: 'upgrade_end_date',
                    type: 'DateTime',
                    display: 'CircleSwitch Update End Date'
                }
            ]
        }
    ],
    dbs: [
        {
            key: 'dbs_promo_campaign',
            display: 'DBS Promo Campaign',
            options: [
                {
                    key: 'enabled',
                    type: 'Boolean',
                    display: 'Enabled'
                }
            ]
        }
    ],
    general: []
}
