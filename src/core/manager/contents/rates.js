var config = require(__base + '/config');
var common = require(__lib + '/common');
var db = require(__lib + '/db_handler');
var ec = require(__lib + '/elitecore');

var LOG = config.LOGSENABLED;

var header = {
    "circlestalk" : {
        "label" : "Circles Talk",
        "country" : "Country",
        "low" : "Low (S$/min)",
        "high" : "High (S$/min)",
        "avg" : "Average (S$/min)" },
    "idd002" : {
        "label" : "International Calls",
        "prefix" : "Prefix",
        "country" : "Country",
        "peak" : "Peak (S$/min)",
        "nonPeak" : "Non-Peak (S$/min)" },
    "idd021" : {
        "label" : "Budget International Calls (IDD021)",
        "prefix" : "Prefix",
        "country" : "Country",
        "peak" : "Peak (S$/min)",
        "nonPeak" : "Non-Peak (S$/min)" },
    "ppurroaming" : {
        "label" : "Roaming Rates",
        "prefix" : "Prefix",
        "country" : "Country",
        "discountoperator" : "Foreign Carrier",
        "discountdata" : "Data Roamer (S$/100MB)",
        "discountmccmnc" : "Data Roamer MCC/MNC",
        "mccmnc" : "MCC/MNC",
        "ppurdatamms" : "Data (S$/10KB)",
        "ppurvoicesg" : "Voice Call Singapore (S$/min)",
        "ppurvoiceintl" : "Voice Call International (S$/min)",
        "ppurvoicelocal" : "Voice Call Local (S$/min)",
        "ppurvoiceincoming" : "Incoming Voice Call (S$/min)",
        "ppursmssend" : "SMS Sent" } };

var tooltip = {
    "idd002" : {
        "label" : "The regular IDD option with high quality international calls. " +
            "Simply dial the number you wish to call with the international country code. " +
            "E.g : to call Malaysia, dial + 60 XXXXXXXXX",
        "peak" : "9am - 9pm (Mon-Sat)",
        "nonPeak" : "9pm - 9am (Mon-Sat), whole day on Sunday and Public Holidays" },
    "idd021" : {
        "label" : "A budget-friendly IDD service. To use 021 IDD, dial 021 before " +
            "the international country code and the number you wish to call. " +
            "E.g : to call Malaysia, dial 021 60 XXXXXXXXX",
        "peak" : "7am - 9pm",
        "nonPeak" : "9pm - 7am" },
    "ppurroaming" : {
        "ppurvoicesg" : "Call from the country you are in to Singapore",
        "ppurvoiceintl" : "Call from the country you are in to any other country except Singapore. " +
                "The rate depends of the country you are calling from and is the same " +
                "independently of the country you are calling",
        "ppurvoicelocal" : "Call from the country you are in to a local number",
        "ppursmssend" : "all received SMS are free of charge. The rates below apply to SMS sent from " +
                "an abroad country to any other country" } };

module.exports = {

    get: function(type, prefixQuery, iso, callback) {
        var result = new Object;
        result.cat = "rates";
        result.type = type;
        var q2 = "";
        result.header = header;
        result.tooltip = tooltip;
        var prefix = "";
        if (prefixQuery) {
            var prefixes = prefixQuery.split(",");
            prefixes.forEach(function (pref) {
                prefix += " r.prefix LIKE " + db.escape(pref + "%") + " OR ";
            });
        }
        if ( type == "circlestalk" ) {
            q2 = "SELECT c.countryName country, c.alpha2code iso, MIN(r.rate) low, MAX(r.rate) high, CEIL(AVG(r.rate)*100)/100 ave FROM ratesGentwo r, countries c WHERE c.alpha2code=r.iso GROUP BY r.iso";
        } else if ( type == "idd002" ) {
            q2 = "SELECT r.prefix, r.country, r.peak, r.nonPeak FROM ratesIDD002 r LEFT JOIN isoPrefix i ON i.prefix=r.prefix WHERE 1 ";
            if ( iso && iso != "" ) q2 += " AND i.iso=" + db.escape(iso) + " ";
            if ( prefix && prefix != "" ) q2 += " AND ( " + prefix.slice(0, -3) + " ) ";
        } else if ( type == "idd021" ) {
            q2 = "SELECT r.prefix, r.country, r.peak, r.nonPeak FROM ratesIDD021 r LEFT JOIN isoPrefix i ON i.prefix=r.prefix WHERE 1 ";
            if ( iso && iso != "" ) q2 += " AND i.iso=" + db.escape(iso) + " ";
            if ( prefix && prefix != "" ) q2 += " AND ( " + prefix.slice(0, -3) + " ) ";
        } else if ( type == "ppurroaming" ) {
            if (iso == "000") {
                q2 = "SELECT '' prefix, pr.country country, pr.data ppurdatamms, " +
                    "GROUP_CONCAT(DISTINCT IF(dr.rate is null,null,imm.operator) SEPARATOR '/') discountoperator, " +
                    "GROUP_CONCAT(DISTINCT dr.rate) discountdata, pr.voicesg ppurvoicesg, pr.voiceintl ppurvoiceintl, pr.voicelocal ppurvoicelocal, " +
                    "pr.voiceincoming ppurvoiceincoming, pr.smssend ppursmssend, " +
                    "GROUP_CONCAT(IF(dr.rate is null,null,CONCAT(imm.mcc,';',imm.mnc))) discountmccmnc, " +
                    "GROUP_CONCAT(IF(dr.rate is not null,null,CONCAT(imm.mcc,';',imm.mnc))) mccmnc " +
                    "FROM plmnRates pr LEFT JOIN dataRoamer dr ON (pr.mcc=dr.mcc and pr.mnc=dr.mnc) " +
                    "LEFT JOIN isoMCCMNC imm ON pr.mcc=imm.mcc AND pr.mnc=imm.mnc " +
                    "LEFT JOIN countries co ON imm.iso=co.alpha2code WHERE co.countryname IS NULL GROUP BY pr.country";
            } else {
                q2 = "SELECT co.countryname, r.prefix prefix, pr.country country, pr.data ppurdatamms, " +
                    "GROUP_CONCAT(DISTINCT IF(dr.rate is null,null,imm.operator) SEPARATOR '/') discountoperator, " +
                    "GROUP_CONCAT(DISTINCT dr.rate) discountdata, pr.voicesg ppurvoicesg, pr.voiceintl ppurvoiceintl, pr.voicelocal " +
                    "ppurvoicelocal, pr.voiceincoming ppurvoiceincoming, pr.smssend ppursmssend, " +
                    "GROUP_CONCAT(IF(dr.rate is null,null,CONCAT(imm.mcc,';',imm.mnc))) discountmccmnc, " +
                    "GROUP_CONCAT(IF(dr.rate is not null,null,CONCAT(imm.mcc,';',imm.mnc))) mccmnc " +
                    "FROM countries co, isoPrefix r, isoMCCMNC imm, plmnRates pr LEFT JOIN dataRoamer dr on (pr.mcc=dr.mcc and pr.mnc=dr.mnc) " +
                    "WHERE r.iso=co.alpha2code and imm.iso=r.iso and (imm.mnc=pr.mnc and imm.mcc=pr.mcc)";
                if ( iso && iso != "" ) q2 += " AND co.alpha2code=" + db.escape(iso) + " ";
                if ( prefix && prefix != "" ) q2 += " AND ( " + prefix.slice(0, -3) + " ) ";
                q2 += " GROUP BY pr.country";
            }
        } else {
            return callback(new Error("Unknown type"));
        }
        db.query_noerr(q2, function (rows) {
            if (rows && rows[0]) {
                result.countryname = (rows && rows[0] && rows[0].country) ? rows[0].country : "";
                var qRows = rows.filter(function (r) {
                    if ( (iso == "US") && (r.country == "Canada") ) return undefined;
                    else if ( (iso == "CA") && (r.country == "USA") ) return undefined;
                    else return r;
                });
                qRows.forEach(function (r) {
                    if (!r.discountoperator) r.discountoperator = "";
                    if (!r.discountdata) r.discountdata = "";
                    if (r.countryname) delete r.countryname;
                });
                result.rates = JSON.parse(JSON.stringify(qRows));
                qRows.forEach(function (row) {
                    Object.keys(row).forEach(function (key) {
                        if (header[type][key]) row[header[type][key]] = row[key];
                        delete row[key];
                    });
                });
                result.content = qRows;
            } else {
                result.rates = [];
                result.content = [];
            }
            callback(undefined, result);
        });
    },

    getIngressISO: function(iso, callback) {
        var qIngress = "SELECT GROUP_CONCAT(s.prefix), s.destination, s.rate FROM (SELECT r.*, cr.dialprefix, UPPER(REPLACE(cr.destination,'- ','')) destination FROM " +
            " ratesGentwo r LEFT JOIN call_rates cr ON r.prefix=cr.dialprefix WHERE providerID IN (7,8,10) AND r.iso='" + iso + "' GROUP BY dialprefix) s " +
            " GROUP BY s.rate, s.destination";
        db.query_noerr(qIngress, function(rows) {
            callback(undefined, rows);
        });
    }
}

