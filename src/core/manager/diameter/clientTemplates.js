'use strict';

exports.getCapabilitiesExchangeHeaders = getCapabilitiesExchangeHeaders;
exports.getInitRequestHeaders = getInitRequestHeaders;
exports.getUpdateRequestHeaders = getUpdateRequestHeaders;
exports.getTerminationReqHeaders = getTerminationReqHeaders;

function getCapabilitiesExchangeHeaders(hname, realm, localIp){
    return [
        [ 'Origin-Host', hname ],
        [ 'Origin-Realm', realm ],
        [ 'Host-IP-Address', localIp ],
        [ 'Vendor-Id', 193 ],
        [ 'Product-Name', 'Ericsson J20 GGSN' ],
        [ 'Origin-State-Id', 14 ],
        ['Inband-Security-Id', 0],
        [ 'Supported-Vendor-Id', 10415 ],
        [ 'Supported-Vendor-Id', 12645 ],
        [ 'Auth-Application-Id', 'Diameter Credit Control' ],
        ['Vendor-Specific-Application-Id', [
                ['Vendor-Id', 10415],
                ['Auth-Application-Id', 16777223]
            ]
            ],
            ['Vendor-Specific-Application-Id', [
                ['Vendor-Id', 10415],
                ['Auth-Application-Id', 16777238]
            ]
            ],
            ['Vendor-Specific-Application-Id', [
                ['Vendor-Id', 10415],
                ['Auth-Application-Id', 16777266]
            ]
            ],
            ['Vendor-Specific-Application-Id', [
                ['Vendor-Id', 10415],
                ['Auth-Application-Id', 16777272]
            ]
            ],
            ['Firmware-Revision', 0]
    ];
}

function getInitRequestHeaders(hname, realm, destHost, destRealm, localIp, msisdn, rgId) {
    var body = [
        ['Origin-Host', hname],
        ['Auth-Application-Id', 'Diameter Credit Control'],
        ['Origin-Realm', realm],
        ['Destination-Realm', destRealm],
        ['Service-Context-Id', '32767@3gpp.org'],
        ['CC-Request-Type', "INITIAL_REQUEST"],
        ['CC-Request-Number', 0],
        ['Destination-Host', destHost],
        ['Origin-State-Id', 14],
        ['Host-IP-Address', localIp],
        ['Subscription-Id', [
            ['Subscription-Id-Type', 0],
            ['Subscription-Id-Data', msisdn]
        ]
        ],
        ['Subscription-Id', [
            ['Subscription-Id-Type', 1],
            ['Subscription-Id-Data', '525038762005204']
        ]
        ],
        ['Multiple-Services-Indicator', 1],
        ['Multiple-Services-Credit-Control', [
            ['Requested-Service-Unit', ''],
            ['Rating-Group', rgId]
        ]
        ],
        ['User-Equipment-Info', [
            ['User-Equipment-Info-Type', 0],
            ['User-Equipment-Info-Value', new Buffer([
                0x53, 0x35, 0x60, 0x60, 
                0x49, 0x84, 0x77, 0x10])]
        ]
        ],
        ['Service-Information', [
            ['PS-Information', [
                ['3GPP-Charging-Id', new Buffer([0xc2, 0xf0, 0xda, 0x81])],
                ['3GPP-PDP-Type', 0],
                ['PDP-Address', '49.245.23.192'],
                ['3GPP-GPRS-Negotiated-QoS-profile', 
                    '08-58090000fa000003e800'],
                ['SGSN-Address', '202.65.240.226'],
                ['GGSN-Address', '202.65.241.102'],
                ['CG-Address', '172.20.0.49'],
                ['3GPP-IMSI-MCC-MNC', '52503'],
                ['3GPP-GGSN-MCC-MNC', '52503'],
                ['3GPP-NSAPI', '5'],
                ['Called-Station-Id', 'sunsurf'],
                ['3GPP-Selection-Mode', '0'],
                ['3GPP-Charging-Characteristics', '0a00'],
                ['3GPP-SGSN-MCC-MNC', '52503'],
                ['3GPP-MS-TimeZone', new Buffer([0x23, 0x00])],
                ['3GPP-User-Location-Info', new Buffer([
                    0x82, 0x25, 0xf5, 0x30, 
                    0xac, 0x4b, 0x25, 0xf5, 
                    0x30, 0x02, 0x13, 0x7c, 0x06])],
                ['3GPP-RAT-Type', new Buffer([0x01])],
                ['PDP-Context-Type', 'PRIMARY'],
                ['Charging-Rule-Base-Name', 'up_lw_normal']
            ]
            ]
        ]
        ],
        ['Vendor-Id', 10415],
        ['Supported-Vendor-Id', 10415]
    ];
    return body;
}

function getUpdateRequestHeaders(hname, realm, destHost, destRealm, localIp, msisdn, mbsize, reqnum, rgId) {
    var body = [
        ['Origin-Host', hname],
        ['Auth-Application-Id', 'Diameter Credit Control'],
        ['Origin-Realm', realm],
        ['Destination-Realm', destRealm],
        ['Service-Context-Id', '32767@3gpp.org'],
        ['CC-Request-Type', "UPDATE_REQUEST"],
        ['CC-Request-Number', reqnum],
        ['Destination-Host', destHost],
        ['Origin-State-Id', 14],
        ['Host-IP-Address', localIp],
        ['Subscription-Id', [
            ['Subscription-Id-Type', 0],
            ['Subscription-Id-Data', msisdn]
        ]
        ],
        ['Subscription-Id', [
            ['Subscription-Id-Type', 1],
            ['Subscription-Id-Data', '525038762005204']
        ]
        ],
        ['Multiple-Services-Indicator', 1],
        ['Multiple-Services-Credit-Control', [
            ['Requested-Service-Unit', ''],
            ['Rating-Group', rgId],
            ['Used-Service-Unit',[
                ['CC-Time', 1],
                ['CC-Total-Octets', mbsize*1024*1024],
                ['CC-Input-Octets', mbsize*1024*1024/2],
                ['CC-Output-Octets', mbsize*1024*1024/2]
            ]],
        ]
        ],
        ['User-Equipment-Info', [
            ['User-Equipment-Info-Type', 0],
            ['User-Equipment-Info-Value', new Buffer([
                0x53, 0x35, 0x60, 0x60,
                0x49, 0x84, 0x77, 0x10])]
        ]
        ],
        ['Service-Information', [
            ['PS-Information', [
                ['3GPP-Charging-Id', new Buffer([0xc2, 0xf0, 0xda, 0x81])],
                ['3GPP-PDP-Type', 0],
                ['PDP-Address', '49.245.23.192'],
                ['3GPP-GPRS-Negotiated-QoS-profile',
                    '08-58090000fa000003e800'],
                ['SGSN-Address', '202.65.240.226'],
                ['GGSN-Address', '202.65.241.102'],
                ['CG-Address', '172.20.0.49'],
                ['3GPP-IMSI-MCC-MNC', '52503'],
                ['3GPP-GGSN-MCC-MNC', '52503'],
                ['3GPP-NSAPI', '5'],
                ['Called-Station-Id', 'sunsurf'],
                ['3GPP-Selection-Mode', '0'],
                ['3GPP-Charging-Characteristics', '0a00'],
                ['3GPP-SGSN-MCC-MNC', '52503'],
                ['3GPP-MS-TimeZone', new Buffer([0x23, 0x00])],
                ['3GPP-User-Location-Info', new Buffer([
                    0x82, 0x25, 0xf5, 0x30,
                    0xac, 0x4b, 0x25, 0xf5,
                    0x30, 0x02, 0x13, 0x7c, 0x06])],
                ['3GPP-RAT-Type', new Buffer([0x01])],
                ['PDP-Context-Type', 'PRIMARY'],
                ['Charging-Rule-Base-Name', 'up_lw_normal']
            ]
            ]
        ]
        ],
        ['Vendor-Id', 10415],
        ['Supported-Vendor-Id', 10415]
    ];
    return body;
}

function getTerminationReqHeaders(hname, realm, destHost, destRealm, localIp, msisdn, mbsize, reqnum, rgId) {

        var body = [
            ['Origin-Host', hname],
            ['Auth-Application-Id', 'Diameter Credit Control'],
            ['Origin-Realm', realm],
            ['Destination-Realm', destRealm],
            ['Service-Context-Id', '32767@3gpp.org'],
            ['CC-Request-Type', "TERMINATION_REQUEST"],
            ['CC-Request-Number', reqnum],
            ['Destination-Host', destHost],
            ['Origin-State-Id', 14],
            ['Host-IP-Address', localIp],
            ['Subscription-Id', [
                ['Subscription-Id-Type', 0],
                ['Subscription-Id-Data', msisdn]
            ]
            ],
            ['Subscription-Id', [
                ['Subscription-Id-Type', 1],
                ['Subscription-Id-Data', '525038762005204']
            ]
            ],
            ['Termination-Cause', 'DIAMETER_LOGOUT'],
            ['Multiple-Services-Indicator', 1],
            ['Multiple-Services-Credit-Control', [
                ['Used-Service-Unit',[
                    ['CC-Time', 1],
                    ['CC-Total-Octets', mbsize*1024*1024],
                    ['CC-Input-Octets', mbsize*1024*1024/2],
                    ['CC-Output-Octets', mbsize*1024*1024/2]
                ]],
                ['Rating-Group', rgId],
                ['Reporting-Reason', 'FINAL']
            ]
            ],
            ['User-Equipment-Info', [
                ['User-Equipment-Info-Type', 0],
                ['User-Equipment-Info-Value', 
                    new Buffer([0x53,0x35,0x60,0x60,
                        0x49,0x84,0x77,0x10])]
            ]
            ],
            ['Service-Information', [
                ['PS-Information', [
                    ['3GPP-Charging-Id', new Buffer(
                        [0xc2,0xf0,0xda,0x81])],
                    ['3GPP-PDP-Type', 0],
                    ['PDP-Address', '49.245.23.192'],
                    ['3GPP-GPRS-Negotiated-QoS-profile',
                        '08-58090000fa000003e800'],
                    ['SGSN-Address', '202.65.240.226'],
                    ['GGSN-Address', '202.65.241.102'],
                    ['CG-Address', '172.20.0.49'],
                    ['3GPP-IMSI-MCC-MNC', '52503'],
                    ['3GPP-GGSN-MCC-MNC', '52503'],
                    ['3GPP-NSAPI', '5'],
                    ['Called-Station-Id', 'sunsurf'],
                    ['3GPP-Selection-Mode', '0'],
                    ['3GPP-Charging-Characteristics', '0a00'],
                    ['3GPP-SGSN-MCC-MNC', '52503'],
                    ['3GPP-MS-TimeZone', new Buffer([0x23,0x00])],
                    ['3GPP-User-Location-Info', 
                        new Buffer([0x82,0x25,0xf5,0x30,
                            0xac,0x4b,0x25,0xf5,
                            0x30,0x02,0x13,0x7c,0x06])],
                    ['3GPP-RAT-Type', new Buffer([0x01])],
                    ['PDP-Context-Type', 'PRIMARY'],
                    ['Charging-Rule-Base-Name', 'up_lw_normal']
                ]
                ]
            ]
            ],
            ['Vendor-Id', 10415],
            ['Supported-Vendor-Id', 10415]
        ]; 
        return body;
    }
