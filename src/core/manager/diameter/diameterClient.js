const async = require('async');
const diameter = require('diameter');
const davp = require('diameter-avp-object');
const diameterTemplates = require('./clientTemplates');
const config = require('../../../../config');
const common = require('../../../../lib/common');
const db = require('../../../../lib/db_handler');

const LOG = config.LOGSENABLED;
const DEBUG = false;

const HOST = config.DIAMOPTS ? config.DIAMOPTS.dest : undefined;
const PORT = config.DIAMOPTS ? config.DIAMOPTS.dport : undefined;
const hname = config.DIAMHOSTNAME ? config.DIAMHOSTNAME : undefined;
const realm = config.DIAMOPTS ? config.DIAMOPTS.realm : undefined;
const localIp = config.MYSELF ? config.MYSELF : undefined;
const destRealm = config.DIAMOPTS ? config.DIAMOPTS.destRealm : undefined;
const destHost = config.DIAMOPTS ? config.DIAMOPTS.destHost : undefined;

let options = { port: PORT, host: HOST };

if (DEBUG) {
    options.beforeAnyMessage = diameter.logMessage;
    options.afterAnyMessage = diameter.logMessage;
}

class DiameterClient {

    static spendData(msisdn, amountMb, rgId, callback) {
        let reqnum = 0;
        const socket = sessionInit(function(err, connection) {
            if (!connection) {
                common.error('spendData sessionInit', err);
                return callback('Connection error');
            }
            sessionCCR(connection, 'request', undefined, msisdn, amountMb, 0, rgId, function(ccrErr, response, sessionId) {
                if (ccrErr || !response || !response.body) {
                    const error = ccrErr ? ccrErr.message : response;
                    common.error(`spendData sessionCCR ${sessionId}`, error);
                    return callback(new Error('Allocation failed'));
                }
                sessionCCR(connection, 'terminate', sessionId, msisdn, amountMb, 1, rgId, function(ccrErr, response) {
                    socket.destroy();
                    callback(undefined, { amountMb });
                });
            });
        });
    }

    static spendDataChunks(msisdn, amountMb, rgId, callback) {
        let runningAmount = 0;
        let reqnum = 0;
        const socket = sessionInit(function(err, connection) {
            if (!connection) {
                common.error('spendDataChunks sessionInit', err);
                return callback('Connection error');
            }
            sessionCCR(connection, 'request', undefined, msisdn, 0, 0, rgId, function(ccrErr, response, sessionId) {
                if (ccrErr || !response || !response.body) {
                    const error = ccrErr ? ccrErr.message : response;
                    common.error(`spendDataChunks sessionCCR ${sessionId}`, error);
                    return callback(new Error('Allocation failed'));
                }
                const parseAllot = function(body) {
                    const reservation = davp.toObject(body);
                    if (!reservation || !reservation.multipleServicesCreditControl || !reservation.multipleServicesCreditControl.volumeQuotaThreshold)
                        return -1; 
                    let volume = parseInt(reservation.multipleServicesCreditControl.volumeQuotaThreshold)/1024/1024;
                    if (volume > 1) volume = volume - 1;
                    return volume;
                }
                let initAllot = parseAllot(response.body);
                if ((runningAmount + initAllot) >= amountMb) initAllot = amountMb - runningAmount;
                async.whilst(function() {
                    return runningAmount < amountMb;
                }, function (cbAsync) {
                    if (!response || !response.body) return callback(new Error('Allocation failed'));
                    let updateAllot = parseAllot(response.body);
                    if ((runningAmount + updateAllot) >= amountMb) updateAllot = amountMb - runningAmount;
                    runningAmount += updateAllot;
                    reqnum++;
                    sessionCCR(connection, 'update', sessionId, msisdn, updateAllot, reqnum, rgId, cbAsync);
                }, function(wErr, wResult) {
                    if (!response) {
                        return cb(new Error('Update failed'));
                    } else { 
                        reqnum++;
                        const remaining = amountMb >= runningAmount ? amountMb - runningAmount : 0;
                        sessionCCR(connection, 'terminate', sessionId, msisdn, remaining, reqnum, rgId, function(ccrErr, response) {
                            socket.destroy();
                            callback(undefined, { amountMb, runningAmount });
                        }); 
                    }
                });
            });
        });
    }
}


function testResponseForSuccess(response){
    for(let i = 0; i < response.body.length; i++){
        if(response.body[i][0] && response.body[i][0] == 'Result-Code'){
            return (response.body[i][1] && response.body[i][1] == 'DIAMETER_SUCCESS') ? true : false;
        }
    }
    return false;
}

function sessionInit(callback) {
    const socket = diameter.createConnection(options, function() {
        if (!socket || !socket.diameterConnection) return callback(new Error('sessionInit Socket Error'));
        const connection = socket.diameterConnection;
        let request = connection.createRequest('Diameter Common Messages', 'Capabilities-Exchange');
        if (!request) return callback(new Error('createRequest error'));
        request.body.shift();
        request.body = request.body.concat(diameterTemplates.getCapabilitiesExchangeHeaders(hname, realm, localIp));
        connection.sendRequest(request).then(function (response) {
            if (testResponseForSuccess(response)) {
                if (LOG) common.log('sessionInit', response);
                callback(undefined, connection);
            } else {
                const error = 'Failed to initialize diameter client Capabilities-Exchange';
                common.error('sessionInit', error);
                callback(new Error(error));
            }
        }, callback);
    });
    socket.on('diameterMessage', function(evt) {
        if (LOG) common.log('sessionInit received message', evt);
    });
    socket.on('error', function(err) {
        common.error('spendDataChunks socket on', err);
        common.error(err)
    });
    return socket;
}

function sessionCCR(connection, type, sessionId, msisdn, amountMb, reqnum, rgId, callback) {
    if (!connection || !msisdn || !rgId || amountMb < 0) return callback(new Error('Incomplete parameters'));
    let request = connection.createRequest('Diameter Credit Control Application', 'Credit-Control');
    if (!request) return callback(new Error('createRequest error'));
    if (type == 'terminate') {
        request.body[0][1] = sessionId;
        request.body = request.body.concat(diameterTemplates.getTerminationReqHeaders(hname, realm, destHost, destRealm, localIp, msisdn, amountMb, reqnum, rgId));
    } else if (type == 'request') {
        request.body = request.body.concat(diameterTemplates.getInitRequestHeaders(hname, realm, destHost, destRealm, localIp, msisdn, rgId));
        sessionId = request.body[0][1];
    } else if (type == 'update') {
        request.body[0][1] = sessionId;
        request.body = request.body.concat(diameterTemplates.getUpdateRequestHeaders(hname, realm, destHost, destRealm, localIp, msisdn, amountMb, reqnum, rgId));
    }
    connection.sendRequest(request).then(function(response){
        if (testResponseForSuccess(response)) {
            if (LOG) common.log('sessionCCR', response);
            callback(undefined, response, sessionId);
        } else {
            const error = 'Failed to send request';
            common.error('sessionCCR', error);
            callback(new Error(error));
        }
    }, callback);
}

function logDiameterResult(number, sessionId, request, requestType, response){
    db.weblog.insert({ 
        ts: new Date().getTime(),
        type: 'diameterClient',
        requestType,
        sessionId,
        number,
        request,
        response
    });
}

module.exports = DiameterClient;
