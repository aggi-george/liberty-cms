'use strict'

var avp = require('diameter-avp-object')
var fs = require('fs')
var common = require(__lib + '/common');
var db = require(__lib + '/db_handler');  
var config = require(__base + '/config');
var templates = require(__core + '/manager/diameter/serverTemplates');
var profile = templates.bypassProfile(config.OCSBYPASS);   
const elitecoreUserService = require('../../../../lib/manager/ec/elitecoreUserService');
const Bluebird = require('bluebird');
var LOG = config.LOGSENABLED;
var cdrPath = config.BYPASSCDRPATH;

function cache_incr(key, ttl) {
    db.cache_ttl("diameter", key, (cache)=>{
        var ttl = (cache <= 0) ? (ttl * 1000) : undefined;
        db.cache_incr("diameter", key, ttl, ()=>{});
    });
}

function cache_hput(diameterMessage, type){
    // Can be request by client or response by server
    if (typeof diameterMessage.sessionId != 'undefined'){
        var msisdn = getMsisdn(diameterMessage)
        var sid = diameterMessage.sessionId
        var originHost = diameterMessage.originHost
        var sessionData = { 
            'msg': diameterMessage,
            'ts': new Date().getTime(),
            'type': type
        }
        cache_incr(templates.alias(originHost) + "TPS", 5);
        db.cache_hput(
            'diameter',
            templates.alias(originHost)+'_'+msisdn,
            { [sid] : JSON.stringify(sessionData) },
            3600 * 1000
        )
    }
}

module.exports = {

    triage: function(type, cmd, msg, callback) {

        function cbHandle(err, res) {
            if (typeof msg.sessionId != 'undefined'){
                var msisdn = getMsisdn(msg)
                var response = res.concat(
                    [
                        ['Session-Id', msg.sessionId],
                        ['Subscription-Id', [
                            ['Subscription-Id-Type', 0],
                            ['Subscription-Id-Data', msisdn[0]]
                        ]
                        ],
                        ['Subscription-Id', [
                            ['Subscription-Id-Type', 1],
                            ['Subscription-Id-Data', 0]
                        ]
                        ]
                    ]
                )
                var resObj = avp.toObject(response)
                cache_hput(resObj, 'response')
            }
            callback(err, res)
        }

        cache_hput(msg, 'request')

        let capX = templates.gyCer;
        switch (type){
            case "OCS":
                profile.originHost = profile.originHostGy
                break;
            case "PCRF":
                profile.originHost = profile.originHostGx
                capX = templates.gxCer;
                break;
            default:
                common.error('Diameter Error',
                    new Error('Service Type Incorrect'))
                break;
        }
        if (LOG) common.log("Diameter Command Type", cmd)
        if (
            LOG &&
            typeof msg.serviceContextId != 'undefined'
        ) common.log("Service Type", msg.serviceContextId);

        switch (cmd) {
            case "Capabilities-Exchange":
                cbHandle(
                    undefined,
                    capX(
                        profile.hostIp,
                        profile.originHost,
                        profile.originRealm)
                );
                break;
            case "Device-Watchdog":
                cache_incr(templates.alias(msg.originHost) + 'WatchDog', 60);
                cbHandle(
                    undefined,
                    templates.watchdog(
                        profile.originHost,
                        profile.originRealm)
                );
                break;
            case "Credit-Control":
                switch (type) {
                    case "OCS":
                        switch (msg.serviceContextId) {
                            case "voice@circles.asia":
                                voice(
                                    msg,
                                    profile.originHost,
                                    profile.originRealm,
                                    cbHandle);
                                break;
                            case "sms@circles.asia":
                                sms(
                                    msg,
                                    profile.originHost,
                                    profile.originRealm,
                                    cbHandle);
                                break;
                            default:
                                mobileData(
                                    msg,
                                    profile.originHost,
                                    profile.originRealm,
                                    cbHandle);
                                break;
                        }
                        break;
                    case "PCRF":
                        mobilePolicy(
                            msg,
                            profile.originHost,
                            profile.originRealm,
                            cbHandle);
                        break;
                    default:
                        cbHandle(new Error("Unexpected Application Code"));
                        break;
                }
                break;
            default:
                cbHandle(new Error("Unexpected Command Code"));
                break;
        }

    }

}

function voice(msg, originHost, originRealm, callback) {
    if (LOG) {
        common.log("Credit-Control", "VOICE service activated");
        common.log('CC-Request-Type', msg.ccRequestType)
    }
    var reqMsisdn = getMsisdn(msg)

    switch (msg.ccRequestType) {
        case "INITIAL_REQUEST":
            if (LOG) common.log(
                "Credit-Control VOICE INITIAL_REQUEST",
                "Sending response to " +
                msg.ccRequestType +
                " for " +
                reqMsisdn);
            callback(
                undefined,
                templates.roInit(
                    msg.ccRequestNumber,
                    originHost,
                    originRealm)
            );
            break;

        case "UPDATE_REQUEST":
            if (LOG) common.log(
                "Credit-Control VOICE UPDATE_REQUEST",
                "Sending response to " +
                msg.ccRequestType +
                " for " +
                reqMsisdn);
            callback(
                undefined,
                templates.roUpdate(
                    msg.ccRequestNumber,
                    originHost,
                    originRealm,
                    3375,
                    3750)
            );
            break;

        case "TERMINATION_REQUEST":
            if (LOG) common.log(
                "Credit-Control VOICE TERMINATION_REQUEST",
                "Sending response to " +
                msg.ccRequestType +
                " for " +
                reqMsisdn);
            callback(
                undefined,
                templates.roTerm(
                    msg.ccRequestNumber,
                    originHost,
                    originRealm,
                    3375,
                    3750)
            );
            break;

        case "EVENT_REQUEST":
            if (LOG) common.log(
                "Credit-Control VOICE EVENT_REQUEST",
                "Sending response to " +
                msg.ccRequestType +
                " for " +
                reqMsisdn);

            callback(
                undefined,
                templates.roEvt(
                    msg.ccRequestNumber,
                    originHost,
                    originRealm)
            );
            break;

        default:
            callback(new Error("Unknown Request Type"));
            break;
    }
}

function sms(msg, originHost, originRealm, callback) {
    if (LOG) {
        common.log("Credit-Control",'SMS service activated')
        common.log("CC-Request-Type",msg.ccRequestType)
    };
    switch (msg.ccRequestType) {
        case "EVENT_REQUEST":
            if (LOG) common.log(
                "Credit-Control SMS EVENT_REQUEST",
                "Sending response to " +
                "SMS EVENT_REQUEST" +
                " for " +
                getMsisdn(msg));

            callback(
                undefined,
                templates.roEvt(
                    msg.ccRequestNumber,
                    originHost,
                    originRealm)
            );
            break;

        default:
            callback(new Error("Unknown Request Type"));
            break;
    }
}

function mobileData(msg, originHost, originRealm, callback) {
    if (LOG) {
        common.log("Credit-Control",'GGSN Gy service activated');
        common.log("CC-Request-Type",msg.ccRequestType);
    }

    var RGID = getRatingGroup(msg);
    const key = templates.alias(originHost) + 'CdrCount';
    const filePath = cdrPath+'/'+profile.dataCDR+'-'+getDateString();
    if (msg.ccRequestType == 'UPDATE_REQUEST'
        ||
        msg.ccRequestType == 'TERMINATION_REQUEST') {
            makeCdr(msg, RGID)
            db.cache_get("diameter", key, value => {
                if (!value) {
                   countLines(filePath)
                    .then(lines => {
                        db.cache_put("diameter", key, lines, 3600);
                    })    
                } else {
                    cache_incr(key, 3600);
                }
            });
        };

    var reqMsisdn = getMsisdn(msg)
    if (LOG) common.log("SUBSCRIBER MSISDN",reqMsisdn)

    switch (msg.ccRequestType) {
        case "INITIAL_REQUEST":
            if (LOG) common.log(
                "3GPP-Gy",
                'Sending response to ' +
                msg.ccRequestType +
                ' for ' + reqMsisdn
            );
            cache_incr(templates.alias(originHost) + "InitialRequest", 3600);
            const number = reqMsisdn[0].slice(2);
            getCustomerAccountStatus(number, originHost)
            callback(
                undefined,
                templates.gyInit(
                    msg.ccRequestNumber,
                    originHost,
                    originRealm,
                    RGID,
                    43200,
                    10485760,
                    1048576,
                    3600
                )
            );
            break;
        case "UPDATE_REQUEST":
            if (LOG) common.log(
                "3GPP-Gy",
                'Sending response to ' +
                msg.ccRequestType +
                ' for ' + reqMsisdn
            );
            cache_incr(templates.alias(originHost) + "UpdateRequest", 3600);
            callback(
                undefined,
                templates.gyUpdate(
                    msg.ccRequestNumber,
                    originHost,
                    originRealm,
                    RGID,
                    43200,
                    10485760,
                    1048576,
                    3600
                )
            )
            break;
        case "TERMINATION_REQUEST":
            if (LOG) common.log(
                "3GPP-Gy",
                'Sending response to ' +
                msg.ccRequestType +
                ' for ' + reqMsisdn
            );
            cache_incr(templates.alias(originHost) + "TerminateRequest", 3600);
            callback(
                undefined, 
                templates.gyTerm(
                    msg.ccRequestNumber,
                    originHost,
                    originRealm,
                    RGID,
                    43200,
                    10485760,
                    1048576,
                    3600
                )
            )
            break;
        case "EVENT_REQUEST":
            if (LOG) common.log(
                "3GPP-Gy",
                'Sending response to ' +
                msg.ccRequestType +
                ' for ' + reqMsisdn
            );
            callback(
                undefined,
                templates.gyEvt(
                    msg.ccRequestNumber,
                    originHost,
                    originRealm
                )
            );
            break;
    };
}

function mobilePolicy(msg, originHost, originRealm, callback) {
    if (LOG) {
        common.log("3GPP-Gx",'GGSN Gx service activated');
        common.log("CC-Request-Type",msg.ccRequestType);
    }

    var terminationCause = (x=>{
        if (typeof msg.terminationCause != 'undefined'
            &&
            msg.terminationCause == 'DIAMETER_SERVICE_NOT_PROVIDED'){
                cache_incr(templates.alias(originHost) + "Errors", 3600);
                common.error(
                    "3GPP-Gx Service Error",
                    msg.terminationCause+
                    ' from '+templates.alias(msg.originHost)+
                    ' for subscriber '+getMsisdn(msg)
                )
                return msg.terminationCause
            } else {
                return null
            }
    })()

    var qosUL = (x=>{
        if ( msg.qosInformation && 
            (typeof msg.qosInformation.apnAggregateMaxBitrateUl
                ==
                'number')
        ) {
            return msg.qosInformation.apnAggregateMaxBitrateUl
        } else {
            return 64000000
        }
    })()

    var qosDL = (x=>{
        if ( msg.qosInformation &&
            (typeof msg.qosInformation.apnAggregateMaxBitrateDl
                ==
                'number')
        ) {
            return msg.qosInformation.apnAggregateMaxBitrateDl
        } else {
            return 256000000
        }
    })()

    if (LOG) common.log(
        "GX PCRF POLICY GIVEN",
        'Download Rate: '+qosDL+
        ' '+
        'Upload Rate: '+qosUL
    )

    var reqMsisdn = getMsisdn(msg)

    switch (msg.ccRequestType) {
        case "INITIAL_REQUEST":
            if (LOG) common.log(
                "3GPP-Gx",
                'Sending response to ' +
                msg.ccRequestType +
                ' for ' + reqMsisdn
            );
            callback(
                undefined,
                templates.gxInit(
                    msg.ccRequestNumber,
                    originHost,
                    originRealm,
                    qosDL,
                    qosUL
                )
            );
            break;
        case "UPDATE_REQUEST":
            if (LOG) common.log(
                "3GPP-Gx",
                'Sending response to ' +
                msg.ccRequestType +
                ' for ' + reqMsisdn
            );
            callback(
                undefined,
                templates.gxUpdate(
                    msg.ccRequestNumber,
                    originHost,
                    originRealm,
                    qosDL,
                    qosUL
                )
            )
            break;
        case "TERMINATION_REQUEST":
            if (LOG) common.log(
                "3GPP-Gx",
                'Sending response to ' +
                msg.ccRequestType +
                ' for ' + reqMsisdn
            );
            callback(
                undefined, 
                templates.gxTerm(
                    msg.ccRequestNumber,
                    originHost,
                    originRealm,
                    qosDL,
                    qosUL
                )
            )
            break;
        case "EVENT_REQUEST":
            if (LOG) common.log(
                "3GPP-Gx",
                'Sending response to ' +
                msg.ccRequestType +
                ' for ' + reqMsisdn
            );
            callback(
                undefined,
                templates.gxEvt(
                    msg.ccRequestNumber,
                    originHost,
                    originRealm,
                    qosDL,
                    qosUL
                )
            );
            break;
    };
}

function getDateString(){
    let x = new Date()
    x.ds = x.getFullYear()+'-'+(x.getMonth()+1)+'-'+x.getDate()
    return x.ds
}

function timeAvp() {
    var time = parseInt(
        new Date().getTime() / 1000 + 2208988800
    );
    return time;
}

function timeUnix() {
    var time = parseInt(
        new Date().getTime() / 1000
    );
    return time;
}


function writeCDR (
    reqMsisdn,
    sessionId,
    timeUnix,
    mccMnc,
    RGID,
    totalOctets,
    type,
    filename) {
        if (LOG) {
            common.log(
                'WRITING CDR',
                reqMsisdn
                +','+type
                +','+sessionId
                +','+RGID
                +','+totalOctets
                +','+mccMnc
                +','+timeUnix
            )};
        if (
            typeof totalOctets != 'undefined' &&
            totalOctets > 0
        ) {
            fs.appendFile(cdrPath+'/'+filename+'-'+getDateString(),
                reqMsisdn
                +','+type
                +','+sessionId
                +','+RGID
                +','+totalOctets
                +','+mccMnc
                +','+timeUnix + '\n'
                , (err)=>{
                    if (err)
                        common.error('CDR WRITE ERROR',err)
                }
            );
        }
    }

function makeCdr (msg, RGID) {
    var sessionId = msg.sessionId;
    var reqMsisdn = getMsisdn(msg)
    var mccMnc = (
        msg && msg.serviceInformation
        && msg.serviceInformation.psInformation
        && msg.serviceInformation.psInformation['3gppSgsnMccMnc']
    ) ? msg.serviceInformation.psInformation['3gppSgsnMccMnc'] : null

    if (!mccMnc) {
        mccMnc = 52503
        common.error(
            '3GPP-Gy Data CDR Error','No SGSN-MCCMNC for '+ 
            reqMsisdn+
            ' Using '+mccMnc 
        )
    }


    if(
        has(msg, 'multipleServicesCreditControl.usedServiceUnit.ccTotalOctets.low')
    ) {
        var totalOctets = msg.multipleServicesCreditControl.usedServiceUnit.ccTotalOctets.low
    } else {
        var totalOctets = 0
        common.error('3GPP-Gy Make CDR Error', 'No Used-Service-Unit')
    }
    
    writeCDR (
        reqMsisdn,
        sessionId,
        timeUnix(),
        mccMnc,
        RGID,
        totalOctets,
        'DATA',
        profile.dataCDR
    )
}

function getRGID (msg){
    try {
        return msg.multipleServicesCreditControl.ratingGroup
    } catch (err) {
        return 300
    }
}

function getRatingGroup (msg){
    common.log('3GPP-Gy', 'Getting Rating Group ID')
    if (has(msg,'multipleServicesCreditControl.ratingGroup')) {
        common.log('3GPP-Gy Rating-Group',
            msg.multipleServicesCreditControl.ratingGroup)
        return msg.multipleServicesCreditControl.ratingGroup
    } else {
        common.log('3GPP-Gy Rating-Group', 'Unable to determine. Using 300')
        return 300 
    }
}

function getMsisdn (msg){
    if (!msg || !msg.subscriptionId || !msg.subscriptionId.length) {
        common.error("getMsisdn", "Invalid Parameters");
        return undefined;
    } 
    var msisdn = msg.subscriptionId.map((o)=>{
        return o.subscriptionIdType=='END_USER_E164' ? o.subscriptionIdData : undefined
    }).filter((o)=>{return o})
    return msisdn
}

// If obj x.a.b.c.d.e , get(x, 'b.c.d.e') for val of e
function get (obj, key) {
    return key.split(".").reduce(function(o, x) {
        return (typeof o == "undefined" || o === null) ? o : o[x];
    }, obj);
}


// If obj x.a.b.c.d.e , has(x, 'b.c.d.e') for bool true if e exists
function has (obj, key) {
    return key.split(".").every(function(x) {
        if(typeof obj != "object" || obj === null || (! x in obj))
            return false;
        obj = obj[x];
        return true;
    });
}

function getCustomerAccountStatus(number, originHost) {
    const loadUserInfoPromise = Bluebird.promisify(elitecoreUserService.loadUserInfoByPhoneNumber);
    return loadUserInfoPromise(number)
        .then(cache => {
            const sin = cache.serviceInstanceNumber;
            const query = `SELECT accountstatus FROM ecAccounts WHERE serviceinstanceaccount='${sin}'`
            const key = templates.alias(originHost) + "SuspendedCount";
            const supsendedNumKey = templates.alias(originHost) + "SuspendedNumbers";
            db.query_err(query, (err, rows) => {
                if (!err && rows && rows.length) {
                    if (rows[0].accountstatus === 'Suspended') {
                        cache_incr(key, 3600);
                        addSuspendedNumbersInCache(supsendedNumKey, number);
                    }
                    return rows[0].accountstatus;
                }
                return null;
            });
        })
        .catch(err => {
            common.log('DiameterManager.getCustomerAccountStatus(): Error', err);
            return null;
        })
}

function countLines(filePath) {
    return new Bluebird((resolve, reject) => {
        let lineCount = 0;
        let i = 0;
        fs.createReadStream(filePath)
          .on("data", (buffer) => {
            for (i = 0; i < buffer.length; ++i) {
              if (buffer[i] == 10) lineCount++;
            }
          }).on("end", () => {
            resolve(lineCount);
          }).on("error", reject);
    })
}

function addSuspendedNumbersInCache(key, number) {
    db.cache_get("diameter", key, value => {
        if (!value) {
            db.cache.put("diameter", key, [number]);
        }else {
            value.push(number);
            db.cache.put("diameter", key, value);
        }
    })
}