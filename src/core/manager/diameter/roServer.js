var db = require(__lib + '/db_handler');
var ec = require(__lib + '/elitecore');
var common = require(__lib + '/common');
var config = require(__base + '/config');

var ORIGINREALM = 'circles.asia';
var LOG = config.LOGSENABLED;

exports.triage = triage;

function triage(cmd, msg, callback) {
    switch(cmd) {
        case "Device-Watchdog":
            deviceWatchdog(msg, callback);
            break;
        case "Capabilities-Exchange":
            capabilitiesExchange(msg, callback);
            break;
        case "Credit-Control":
            creditControl(msg, callback);
            break;
        default:
            callback(new Error("Invalid Event"));
    }
}

function deviceWatchdog(msg, callback) {
    var response = [
        ['Origin-Host', config.MYFQDN],
        ['Origin-Realm', ORIGINREALM] ];
    callback(undefined, response);
}

function capabilitiesExchange(msg, callback) {
    var response = [
        ['Result-Code', 'DIAMETER_SUCCESS'],
        ['Origin-Host', config.MYFQDN],
        ['Origin-Realm', ORIGINREALM],
        ['Host-IP-Address', config.MYSELF],
        ['Vendor-Id', 10415],
        ['Product-Name', 'Circles-OCS'],
        ['Vendor-Specific-Application-Id', [
            [ 'Vendor-Id', 10415 ],
            [ 'Acct-Application-Id', '3GPP CX/DX' ] ] ],
        [ 'Acct-Application-Id', '3GPP CX/DX' ],
        ['Vendor-Specific-Application-Id', [
            [ 'Vendor-Id', 10415 ],
            [ 'Auth-Application-Id', '3GPP CX/DX' ] ] ],
        [ 'Auth-Application-Id', '3GPP CX/DX' ],
        ['Vendor-Specific-Application-Id', [
            [ 'Vendor-Id', 10415 ],
            [ 'Acct-Application-Id', 'Diameter Credit Control' ] ] ],
        [ 'Acct-Application-Id', 'Diameter Credit Control' ],
        [ 'Supported-Vendor-Id', 10415 ] ];
    callback(undefined, response);
}

function getCredit(msg, callback) {
    var origin = msg.originHost[0];
    var requested = msg.multipleServicesCreditControl.requestedServiceUnit ?
            msg.multipleServicesCreditControl.requestedServiceUnit.ccTime : 0;
    var used = msg.multipleServicesCreditControl.usedServiceUnit ?
            msg.multipleServicesCreditControl.usedServiceUnit.ccTime : 0;
    var callid = msg.serviceInformation.imsInformation.userSessionId;
    var ingressTrunk = parseInt(msg.serviceInformation.imsInformation.trunkGroupId.incomingTrunkGroupId);
    var egressTrunk = parseInt(msg.serviceInformation.imsInformation.trunkGroupId.outgoingTrunkGroupId);
    var aAddr = msg.serviceInformation.imsInformation.callingPartyAddress;
    var bAddr = msg.serviceInformation.imsInformation.calledPartyAddress;
    var aNum = aAddr.split('@')[0].split(':')[1];
    var bNum = bAddr.split('@')[0].split(':')[1];
    var releaseCause = msg.terminationCause;

    var cacheHget = function(callid, callback) {
        db.cache_hget("switch", callid, function (active) {
            var cdr = (active && active.cdr) ? common.safeParse(active.cdr) : undefined;
            var usage = (active.usage) ? common.safeParse(active.usage) : {
                "duration" : 0,
                "ingress_billtime" : 0,
                "ingress_cost" : 0,
                "egress_billtime" : 0,
                "egress_cost" : 0,
                "credit" : ((cdr) ? cdr.credit_start : 0),
            };
            if (cdr && !usage.cdr) usage.cdr = cdr;
            else if (!cdr && usage.cdr) cdr = usage.cdr;
            callback(cdr, usage);
        });
    }

    var getUsage = function (cdr, usage) {
        usage.duration = (usage.duration) ? usage.duration + used : used;
        if (cdr && cdr.egress_bill_start && cdr.egress_bill_increment) {
            usage.egress_billtime = getBillTime(cdr.egress_bill_start, cdr.egress_bill_increment, usage.duration);
            usage.egress_cost = parseInt(cdr.egress_rate / 60 * usage.egress_billtime * 1000000) / 1000000;
        }
        if (cdr && cdr.ingress_bill_start && cdr.ingress_bill_increment) {
            usage.ingress_billtime = getBillTime(cdr.ingress_bill_start, cdr.ingress_bill_increment, usage.duration);
            if ( (cdr.ingress_rate === 0) ||
                (egressTrunk === 0) ||         // SIP and SERVICES force allow
                ((ingressTrunk >= 19) && (ingressTrunk < 30)) ) {    // allow all
                return requested;
            } else {
                usage.ingress_cost = parseInt(cdr.ingress_rate / 60 * usage.ingress_billtime * 1000000) / 1000000;
                var credit = cdr.credit_start;		// fix this for multiple simultaneous
                credit = (credit - usage.ingress_cost) > 0 ? credit - usage.ingress_cost : 0 ;
                usage.credit = credit;
                if (requested) {
                    var seconds = Math.floor(usage.credit / cdr.ingress_rate * 60);
                    return (seconds < cdr.ingress_bill_increment) ? 0 :
                        (seconds > requested) ? requested : cdr.ingress_bill_increment;
                } else {
                    return;
                }
            }
        }
        // not sure what to do with "cdr.rounding" for now
        // catch all
        return 0;
    }

    var getBillTime = function(start, increment, duration) {
        if ( duration == 0 ) {
            return 0;
        } else if ( duration <= start ) {
            return start;
        } else {
            var next = increment * Math.ceil((duration - start) / increment);
            return start + next;
        }
    }

    cacheHget(callid, function (cdr, usage) {
        if ( msg.ccRequestType == "INITIAL_REQUEST" ) {
            if (!cdr || !usage) {
                common.error(msg.ccRequestType, aNum + " - " + bNum + " - cache error on cdr or usage");
                return callback(undefined, requested, 0);
            }
            var prefixes = "";
	    bNum.split('').forEach(function (ch, idx) { prefixes += "'" + bNum.substring(0, idx+1) + "',"; });
            var granted = 0;
            if ( (ingressTrunk >= 19) && (ingressTrunk < 30) ) {    // moc or activation allow all, 19 is activation
                granted = requested;
            } else {
                var seconds = Math.floor(usage.credit / cdr.ingress_rate * 60);
                granted = seconds < cdr.ingress_bill_start ? 0 : seconds < requested ? ingress_bill_start : requested;
                if (egressTrunk === 0) {
                    granted = requested;    // SIP and SERVICES force allow
                }
            }
            var prefix = (aNum.substring(0,2) == "65") ? "65" : undefined;
            if (prefix) {
                var number = aNum.substring(prefix.length);
                ec.is_circles(number, prefix, function(circles, base_plan) {
                    if (circles) {
                        ec.getCustomerDetailsNumber(prefix, number, false, function(err, cache) {
                            usage.serviceInstanceNumber = cache ? cache.serviceInstanceNumber : undefined;
                            db.cache_hput("switch", callid, { "usage" : JSON.stringify(usage) }, 60000);
                        });
//                    granted = requested;
                    }
                    if (LOG) common.log("getCredit Initial", aNum + " credit : " + usage.credit
                        + " granted : " + granted + " circles : " + circles);
                    db.cache_hput("switch", callid, { "usage" : JSON.stringify(usage) }, 60000);
                    callback(undefined, requested, granted);
                });
            } else {
                if (LOG) common.log("getCredit Initial", aNum + " credit : " + usage.credit
                    + " granted : " + granted + " circles : 0");
                db.cache_hput("switch", callid, { "usage" : JSON.stringify(usage) }, 60000);
                callback(undefined, requested, granted);
            }
        } else if ( msg.ccRequestType == "UPDATE_REQUEST" ) {
            if (!cdr || !usage) {
                common.error(msg.ccRequestType, aNum + " - " + bNum + " - cache error on cdr or usage");
                return callback(undefined, requested, 0);
            }
            var granted = 0;
            if (cdr) {
                if (!cdr.ingress_bill_increment) {
                    cdr.ingress_bill_increment = requested;
                }
                granted = (cdr) ? getUsage(cdr, usage) : 0;
            } else {
                common.error("UPDATE_REQUEST No Cache!", callid);
            }
//           if (usage.serviceInstanceNumber) granted = requested;
            if (LOG) common.log("getCredit Update", aNum + " credit : " + usage.credit
                    + " granted : " + granted + " used : " + used);
            db.cache_hput("switch", callid, { "usage" : JSON.stringify(usage) }, 60000);
            callback(undefined, requested, granted);
        } else if ( msg.ccRequestType == "TERMINATION_REQUEST" ) {
            if (!cdr || !usage) {
                common.error(msg.ccRequestType, aNum + " - " + bNum + " - cache error on cdr or usage");
                return callback(undefined, 0, 0, 0);
            }
            getUsage(cdr, usage);
            usage.end_time = new Date().getTime();
            if (config.DEV) usage.dev = 1;
            callback(undefined, 0, 0, 0);
            if (usage) {
                if (LOG) common.log("getCredit Terminate", aNum + " credit : " + usage.credit + " used : " + used);
                var update = {
                    "duration" : usage.duration,
                    "ingress_billtime" : usage.ingress_billtime,
                    "ingress_cost" : usage.ingress_cost,
                    "egress_billtime" : usage.egress_billtime,
                    "egress_cost" : usage.egress_cost,
                    "credit" : usage.credit,
                    "serviceInstanceNumber" : usage.serviceInstanceNumber
                };
                if (usage.ingress_cost) {
                   var q = "UPDATE msisdn SET credit = credit - " + db.escape(usage.ingress_cost) + " WHERE " +
                        " number = " + db.escape(aNum);
                    if (LOG) common.log("getCredit Terminate Debit", q);
                   db.query_noerr(q, function () { });
                }
                if (LOG) common.log("getCredit Terminate CDR " + callid, JSON.stringify(update));
                db.cdrs.update({ "call_id" : callid }, { "$set" : update }, { "upsert" : true });
                db.cache_hdel("switch", callid, "usage");
            }
        }
    });
}

function creditControl(msg, callback) {
    getCredit(msg, function (error, requested, granted) {
        var mscc;
        if (!error && requested) {
            mscc = (granted < requested) ? [
                ['Validity-Time', granted],
                ['Final-Unit-Indication', [
                    ['Final-Unit-Action', 'TERMINATE']
                ]],
                ['Granted-Service-Unit', [
                    ['CC-Time', granted],
                ]],
                ['Requested-Service-Unit', [
                    ['CC-Time', requested],
                ]] ] : [
                ['Validity-Time', granted],
                ['Granted-Service-Unit', [
                    ['CC-Time', granted],
                ]],
                ['Requested-Service-Unit', [
                    ['CC-Time', requested],
                ]] ];
        }
        var response = [
            ['Result-Code', 'DIAMETER_SUCCESS'], // You can also define enum values by their integer codes
            ['Origin-Host', config.MYFQDN], // or AVP names, this is 'Origin-Host'
            ['Origin-Realm', ORIGINREALM],
            ['Auth-Application-Id', 'Diameter Credit Control'],
            ['CC-Request-Type', msg.ccRequestType],
            ['CC-Request-Number', msg.ccRequestNumber] ];
        if (mscc) {
            response.push(
                ['Multiple-Services-Indicator', 'MULTIPLE_SERVICES_SUPPORTED' ],
                ['Multiple-Services-Credit-Control', mscc ] );
        }
        callback(undefined, response);
    });
}
