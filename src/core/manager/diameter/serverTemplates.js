'use strict'

// Gx PCRF Exports
exports.gxCer = gxCer;
exports.gxInit = gxInit;
exports.gxUpdate = gxUpdate;
exports.gxEvt = gxEvt;
exports.gxTerm = gxTerm;

// Gy OCS Exports
exports.gyCer = gyCer;
exports.gyInit = gyInit;
exports.gyUpdate = gyUpdate;
exports.gyEvt = gyEvt;
exports.gyTerm = gyTerm;

// Ro OCS Exports
exports.roCer = roCer;
exports.roInit = roInit;
exports.roEvt = roEvt;
exports.roUpdate = roUpdate;
exports.roTerm = roTerm;

// Others
exports.time = time;
exports.watchdog = watchdog;

// Calculate timestamp for AVP format
function time() {
    var time = parseInt(new Date().getTime() / 1000 + 2208988800);
    return time;
}

// Gx - Capabilities Exchange Response
function gxCer(hostIp, originHost, originRealm) {
    var cerBody = [
        ['Origin-Host', originHost],
        ['Origin-Realm', originRealm],
        ['Result-Code', 'DIAMETER_SUCCESS'],
        ['Origin-State-Id', 0],
        ['Host-IP-Address', hostIp],
        ['Vendor-Id', 21067],
        ['Product-Name', 'EliteAAA'],
        ['Inband-Security-Id', 0],
        ['Auth-Application-Id', 'Diameter Credit Control'],
        ['Vendor-Specific-Application-Id',[
            ['Vendor-Id', 10415],
            ['Auth-Application-Id', 16777238]
        ] 
        ]
    ];
    return cerBody;
};

// Gx - Initial Response
function gxInit(ccRequestNo, originHost, originRealm, qosDL, qosUL) {
    var response = [
        ['Auth-Application-Id', 16777238],
        ['CC-Request-Type', 'INITIAL_REQUEST'],
        ['CC-Request-Number', ccRequestNo],
        ['Origin-Host', originHost],
        ['Origin-Realm', originRealm],
        ['Result-Code', 'DIAMETER_SUCCESS'],
        [1001, [ 
            ['Charging-Rule-Base-Name', 'up_lw_normal']
        ]
        ],
        [ 1016, [
            ['QoS-Class-Identifier',9],
            [ 'APN-Aggregate-Max-Bitrate-UL', qosUL],
            [ 'APN-Aggregate-Max-Bitrate-DL', qosDL]
        ]
        ],
        ['Online', 1],
        ['Offline', 1],
        [1006, 14]
    ];
    return response;
}

// Gx - Update Response
function gxUpdate(ccRequestNo, originHost, originRealm, qosDL, qosUL) {
    var response = [
        ['Auth-Application-Id', 16777238],
        ['CC-Request-Type', 'UPDATE_REQUEST'],
        ['CC-Request-Number', ccRequestNo],
        ['Origin-Host', originHost],
        ['Origin-Realm', originRealm],
        ['Result-Code', 'DIAMETER_SUCCESS'],
        [1001, [ 
            ['Charging-Rule-Base-Name', 'up_lw_normal']
        ]
        ],
        [ 1016, [
            ['QoS-Class-Identifier',9],
            [ 'APN-Aggregate-Max-Bitrate-UL', qosUL],
            [ 'APN-Aggregate-Max-Bitrate-DL', qosDL]
        ]
        ],
        ['Online', 1],
        ['Offline', 1],
        [1006, 14]
    ];
    return response;
}

// Gx - Event Response
function gxEvt(ccRequestNo, originHost, originRealm, qosDL, qosUL) {
    var response = [
        ['Auth-Application-Id', 16777238],
        ['CC-Request-Type', 'EVENT_REQUEST'],
        ['CC-Request-Number', ccRequestNo],
        ['Origin-Host', originHost],
        ['Origin-Realm', originRealm],
        ['Result-Code', 'DIAMETER_SUCCESS'],
        [ 1016, [
            ['QoS-Class-Identifier',9],
            [ 'APN-Aggregate-Max-Bitrate-UL', qosUL],
            [ 'APN-Aggregate-Max-Bitrate-DL', qosDL]
        ]
        ],
        ['Online', 1],
        ['Offline', 1],
        [1006, 14]
    ];
    return response;
}

// Gx - Termination Response
function gxTerm(ccRequestNo, originHost, originRealm, qosDL, qosUL) {
    var response = [
        ['Auth-Application-Id', 16777238],
        ['CC-Request-Type', 'TERMINATION_REQUEST'],
        ['CC-Request-Number', ccRequestNo],
        ['Origin-Host', originHost],
        ['Origin-Realm', originRealm],
        ['Result-Code', 'DIAMETER_SUCCESS'],
        [ 1016, [
            ['QoS-Class-Identifier',9],
            [ 'APN-Aggregate-Max-Bitrate-UL', qosUL],
            [ 'APN-Aggregate-Max-Bitrate-DL', qosDL]
        ]
        ],
        ['Online', 1],
        ['Offline', 1],
        [1006, 14]
    ];
    return response;
}

// Gy - Capabilities Exchange Response
function gyCer(hostIp, originHost, originRealm) {
    var cerBody = [
        ['Origin-Host', originHost],
        ['Origin-Realm', originRealm],
        ['Host-IP-Address', hostIp],
        ['Vendor-Id', 123],
        ['Result-Code', 'DIAMETER_SUCCESS'],
        ['Product-Name', 'node-diameter']
    ];
    return cerBody;
};

// Gy - Initial Response
function gyInit(
    ccRequestNo,
    originHost,
    originRealm,
    ratingGroupId,
    ccTime,
    ccTotalOctets,
    volumeQuotaThreshold,
    quotaHoldingtime
) {
    var response = [
        ['Auth-Application-Id', 'Diameter Credit Control'],
        ['CC-Request-Type', 'INITIAL_REQUEST'],
        ['CC-Request-Number', ccRequestNo], 
        ['Origin-Host', originHost],
        ['Origin-Realm', originRealm],
        ['Result-Code', 'DIAMETER_SUCCESS'],
        ['Multiple-Services-Credit-Control', [
            ['Rating-Group', ratingGroupId],
            ['Granted-Service-Unit', [
                ['CC-Time', ccTime],
                ['CC-Total-Octets', ccTotalOctets]
            ]],
            ['Result-Code', 'DIAMETER_SUCCESS'],	
            [869, volumeQuotaThreshold],
            [871 , quotaHoldingtime]

        ]],
        ['Event-Timestamp', time()]
    ];
    return response;
}

// Gy - Update Response
function gyUpdate(
    ccRequestNo,
    originHost,
    originRealm,
    ratingGroupId,
    ccTime,
    ccTotalOctets,
    volumeQuotaThreshold,
    quotaHoldingtime
) {
    var response = [
        ['Auth-Application-Id', 'Diameter Credit Control'],
        ['CC-Request-Type', 'UPDATE_REQUEST'],
        ['CC-Request-Number', ccRequestNo],
        ['Origin-Host', originHost],
        ['Origin-Realm', originRealm],
        ['Result-Code', 'DIAMETER_SUCCESS'],
        ['Multiple-Services-Credit-Control', [
            ['Rating-Group', ratingGroupId],
            ['Granted-Service-Unit', [
                ['CC-Time', ccTime],
                ['CC-Total-Octets', ccTotalOctets]
            ]],
            ['Result-Code', 'DIAMETER_SUCCESS'],	
            [869, volumeQuotaThreshold],
            [871 , quotaHoldingtime]

        ]],
        ['Event-Timestamp', time()]
    ];
    return response;
}

// Gy - Event Response
function gyEvt(ccRequestNo, originHost, originRealm) {
    var response = [
        ['Auth-Application-Id', 'Diameter Credit Control'],
        ['CC-Request-Type', 'EVENT_REQUEST'],
        ['CC-Request-Number', ccRequestNo],
        ['Origin-Host', originHost],
        ['Origin-Realm', originRealm],
        ['Result-Code', 'DIAMETER_SUCCESS'],
        ['Event-Timestamp', time()],
        ['Multiple-Services-Credit-Control', [
            ['Result-Code', 'DIAMETER_SUCCESS'],	
        ]]
    ];
    return response;
}

// Gy - Termination Response
function gyTerm(
    ccRequestNo,
    originHost,
    originRealm,
    ratingGroupId,
    ccTime,
    ccTotalOctets,
    volumeQuotaThreshold,
    quotaHoldingtime) {
    var response = [
        ['Auth-Application-Id', 'Diameter Credit Control'],
        ['CC-Request-Type', 'TERMINATION_REQUEST'],
        ['CC-Request-Number', ccRequestNo],
        ['Origin-Host', originHost],
        ['Origin-Realm', originRealm],
        ['Result-Code', 'DIAMETER_SUCCESS'],
        ['Multiple-Services-Credit-Control', [
            ['Rating-Group', ratingGroupId],
            ['Granted-Service-Unit', [
                ['CC-Time', ccTime],
                ['CC-Total-Octets', ccTotalOctets]
            ]],
            ['Result-Code', 'DIAMETER_SUCCESS'],	
            [869, volumeQuotaThreshold],
            [871 , quotaHoldingtime]

        ]],
        ['Event-Timestamp', time()]
    ];
    return response;
}

// Ro - Capabilities Exchange Response
function roCer(hostIp, originHost, originRealm) {
    var cerBody = [
        ['Origin-Host', originHost],
        ['Origin-Realm', originRealm],
        ['Host-IP-Address', hostIp],
        ['Vendor-Id', 123],
        ['Result-Code', 'DIAMETER_SUCCESS'],
        ['Product-Name', 'node-diameter']
    ];
    return cerBody;
};

// Ro - Init Response
function roInit(ccRequestNo, originHost, originRealm) {
    var response = [
        ['Auth-Application-Id', 'Diameter Credit Control'],
        ['CC-Request-Type', 'INITIAL_REQUEST'],
        ['CC-Request-Number', ccRequestNo], 
        ['Origin-Host', originHost],
        ['Origin-Realm', originRealm],
        ['Result-Code', 'DIAMETER_SUCCESS'],
        ['Event-Timestamp', time()]
    ];
    return response;
}

// Ro - Event Response
function roEvt(ccRequestNo, originHost, originRealm) {
    var response = [
        ['Auth-Application-Id', 'Diameter Credit Control'],
        ['CC-Request-Type', 'EVENT_REQUEST'],
        ['CC-Request-Number', ccRequestNo],
        ['Origin-Host', originHost],
        ['Origin-Realm', originRealm],
        ['Result-Code', 'DIAMETER_SUCCESS'],
        ['Event-Timestamp', time()],
        ['Multiple-Services-Credit-Control', [
            ['Result-Code', 'DIAMETER_SUCCESS'],	
        ]]
    ];
    return response;
}

// Ro - Update Response
function roUpdate(ccRequestNo, originHost, originRealm, TQT, ccTime) {
    var response = [
        ['Auth-Application-Id', 'Diameter Credit Control'],
        ['CC-Request-Type', 'UPDATE_REQUEST'],
        ['CC-Request-Number', ccRequestNo],
        ['Origin-Host', originHost],
        ['Origin-Realm', originRealm],
        ['Result-Code', 'DIAMETER_SUCCESS'],
        ['Multiple-Services-Credit-Control', [
            ['Time-Quota-Threshold', TQT],
            ['Granted-Service-Unit', [
                ['CC-Time', ccTime]
            ]
            ],
            ['Result-Code', 'DIAMETER_SUCCESS']	
        ]
        ],
        ['Event-Timestamp', time()]
    ];
    return response;
}

// Ro - Termination Response
function roTerm(
    ccRequestNo,
    originHost,
    originRealm,
    ratingGroupId,
    ccTime,
    ccTotalOctets,
    volumeQuotaThreshold,
    quotaHoldingtime
) {
    var response = [
        ['Auth-Application-Id', 'Diameter Credit Control'],
        ['CC-Request-Type', 'TERMINATION_REQUEST'],
        ['CC-Request-Number', ccRequestNo],
        ['Origin-Host', originHost],
        ['Origin-Realm', originRealm],
        ['Result-Code', 'DIAMETER_SUCCESS'],
        ['Multiple-Services-Credit-Control', [
            ['Rating-Group', ratingGroupId],
            ['Granted-Service-Unit', [
                ['CC-Time', ccTime],
                ['CC-Total-Octets', ccTotalOctets]
            ]],
            ['Result-Code', 'DIAMETER_SUCCESS'],	
            [869, volumeQuotaThreshold],
            [871 , quotaHoldingtime]

        ]],
        ['Event-Timestamp', time()]
    ];
    return response;
}

function watchdog(originHost, originRealm) {
    var response = [
        ['Origin-Host', originHost],
        ['Origin-Realm', originRealm],
        ['Result-Code', 'DIAMETER_SUCCESS']
    ];
    return response;
}

// Get impersonated host's profile
exports.bypassProfile = (hostname)=>{
    let profile = undefined;
    switch (hostname) {
        case "LWEPC01":
            profile = {
                'listenIp' : '0.0.0.0',
                'portGx' : 4868,
                'portGy' : 4869,
                'portSy' : 4870,
                'hostIp' : '172.16.62.141',
                'originIp' : '172.16.62.141',
                'originHostGx' : 'Libertygx.circle.asia',
                'originHostGy' : 'Libertygy.circle.asia',
                'originRealm' : 'circle.asia',
                'dataCDR' : 'LWEPC01-dataCDR',
                'voiceCDR' : 'LWEPC01-voiceCDR'
            }
            break;
        case "LWEPC02":
            profile = {
                'listenIp' : '0.0.0.0',
                'portGx' : 5868,
                'portGy' : 5869,
                'portSy' : 5870,
                'hostIp' : '172.16.62.142',
                'originIp' : '172.16.62.142',
                'originHostGx' : 'Libertygx2.circle.asia',
                'originHostGy' : 'Libertygy2.circle.asia',
                'originRealm' : 'circle.asia',
                'dataCDR' : 'LWEPC02-dataCDR',
                'voiceCDR' : 'LWEPC02-voiceCDR'
            }
            break;
        case "LWTES01":
            profile = {
                'listenIp' : '0.0.0.0',
                'portGx' : 6868,
                'portGy' : 6869,
                'portSy' : 6870,
                'hostIp' : '172.16.62.132',
                'originIp' : '172.16.62.132',
                'originHostGx' : 'testlibertygx.circle.asia',
                'originHostGy' : 'testgy.circles.asia',
                'originRealm' : 'circle.asia',
                'dataCDR' : 'LWTES01-dataCDR',
                'voiceCDR' : 'LWTES01-voiceCDR'
            }
            break;
        case "CirclesDR1":
            profile = {
                'listenIp' : '0.0.0.0',
                'portGx' : 3868,
                'portGy' : 3869,
                'portSy' : 3870,
                'hostIp' : '172.16.62.133',
                'originIp' : '172.16.62.133',
                'originHostGx' : 'circlesdr1gx.circles.asia',
                'originHostGy' : 'circlesdr1gy.circles.asia',
                'originRealm' : 'circles.asia',
                'dataCDR' : 'CirclesDR1-dataCDR',
                'voiceCDR' : 'CirclesDR1-voiceCDR'
            }
            break;
        case "CirclesDR2":
            profile = {
                'listenIp' : '0.0.0.0',
                'portGx' : 3868,
                'portGy' : 3869,
                'portSy' : 3870,
                'hostIp' : '172.16.62.134',
                'originIp' : '172.16.62.134',
                'originHostGx' : 'circlesdr2gx.circles.asia',
                'originHostGy' : 'circlesdr2gy.circles.asia',
                'originRealm' : 'circles.asia',
                'dataCDR' : 'CirclesDR2-dataCDR',
                'voiceCDR' : 'CirclesDR2-voiceCDR'
            }
            break;
    }
    if (profile) {
        return profile 
    } else {
        return new Error('No profile found for your selection'); 
    }

}

exports.peers = {
    Gx: [ 'mocGx', 'rocGx' ],
    Gy: [ 'mocGy', 'rocGy' ],
}

exports.alias = longName=>{
    switch (longName){
        case "Gx.mocugw.epc.mnc003.mcc525.3gppnetwork.org":
            return 'mocGx'
            break;
        case "Gy.mocugw.epc.mnc003.mcc525.3gppnetwork.org":
            return 'mocGy'
            break;
        case "Gx.rocugw.epc.mnc003.mcc525.3gppnetwork.org":
            return 'rocGx'
            break;
        case "Gy.rocugw.epc.mnc003.mcc525.3gppnetwork.org":
            return 'rocGy'
            break;
        case "ndjs01.lwp.com":
            return 'ndjs01'
            break;
        case "testlibertygx.circle.asia":
            return 'testLibertyGx'
            break;
        case "testgy.circles.asia":
            return 'testLibertyGy'
            break;
        case "Libertygx.circle.asia":
            return 'libertyGx'
            break;
        case "Libertygy.circle.asia":
            return 'libertyGy'
            break;
        case "Libertygx2.circle.asia":
            return 'libertyGx2'
            break;
        case "Libertygy2.circle.asia":
            return 'libertyGy2'
            break;
        default:
            return new Error('No Device found by this name ' + longName)
            break;
    }
}
