var request = require('request');
var md5 = require('MD5');
var config = require('../../../../config');
var common = require('../../../../lib/common');
var db = require('../../../../lib/db_handler');
var BaseError = require('../../errors/baseError');
var ec = require('../../../../lib/elitecore');
var elitecoreConnector = require('../../../../lib/manager/ec/elitecoreConnector');
var elitecoreBillService = require('../../../../lib/manager/ec/elitecoreBillService');
var randomManager = require('../../../utils/randomManager');
var resourceManager = require('../../../../res/resourceManager');
let Bluebird = require('bluebird');

var log = config.LOGSENABLED;
var serverNum = 0;

module.exports = {

    /**
     *
     */
    generateCreditCardUpdateLink: (account, callback) => {

        ec.getCustomerDetailsAccount(account, false, (err, cache) => {
            if (err || !cache) {
                return callback(new Error(`Customer data not found, account = ${account}`));
            }
            let params = { account_number: account };
            if (cache.number) params.mobile_number = cache.number;
            module.exports.executeECommRequest('API', 'GET', 'api/payment/v1/update_credit_cards/get_url.json',
                            params, undefined, (err, result) => {
                if (err) {
                    common.error('generateCreditCardUpdateLink', err);
                    return callback(err);
                } else if (!result) {
                    common.error('generateCreditCardUpdateLink', 'paas error');
                    return callback(new Error('generateCreditCardUpdateLink paas error'));
                } else {
                    if (!result || result.code != 0) return callback(new Error('generateCreditCardUpdateLink Unknown Error'));
                    const expiryDuration = 20 * 60 * 1000; // 20 min
                    const expiryTime = new Date(new Date().getTime() + expiryDuration);
                    const obj = {
                        code: result.code,
                        linkId : randomManager.randomPredefinedString(45),
                        url: result.url,
                        refNum: result.ref_num,
                        expiryTs: expiryTime.getTime(),
                        ts: new Date().getTime(),
                        expiryDuration,
                        expiryTime
                    }
                    db.templinks.insert(obj, function () {
                        if (callback) callback(undefined, obj);
                    });
                }
            });
        });
    },

    /**
     *
     */
    creditCardUpdateStatus: (linkId, refNum, callback) => {
        db.templinks.find({ linkId }).toArray((err, result) => {
            if (err || !result || result.length == 0) return callback(new Error('creditCardUpdateStatus Invalid linkId'));
            const params = { ref_num: refNum };
            module.exports.executeECommRequest('API', 'GET', 'api/payment/v1/update_credit_cards/show_status.json',
                params, undefined, callback);
        });
    },

    /**
     *
     */
    notifyTerminated: function (prefix, number, callback) {
        ec.getCustomerDetailsNumber(prefix, number, false, function (err, cache) {
            if (err || !cache) {
                return callback(new Error("Customer data not found, prefix=" + prefix
                + ", number=" + number));
            }

            module.exports.executeECommRequest("ADMIN", "PUT", "api/v1/orders/" + cache.serviceInstanceNumber + "/cancel.json",
                undefined, undefined, function (err, result) {
                    if (callback) callback(err, result);
                });
        });
    },

    executePayment: function (customerAccountNumber, billingAccountNumber, amount, uuid, callback) {
        payInternal(customerAccountNumber, billingAccountNumber, amount, uuid, callback);
    },

    checkBlacklist: function (billingAccountNumber, callback) {
        module.exports.executeECommRequest("API", "GET", "api/kirk/v1/blacklists/" + billingAccountNumber,
            undefined, undefined, function (err, result) {
                if (callback) callback(err, result);
            });
    },

    numberChange: function (serviceInstanceNumber, number, newNumber, callback) {
        var params = JSON.parse(JSON.stringify(config.PAAS_CREDS));
        params.new_number = newNumber;
        params.temp_number = number;
        module.exports.executeECommRequest("ADMIN", "PUT", "api/v1/orders/" + serviceInstanceNumber + "/change_number.json",
            params, undefined, function (err, result) {
                if (callback) callback(err, result);
            });
    },

    deliveryList: function (date, slot, courier, callback) {
        var params = JSON.parse(JSON.stringify(config.PAAS_CREDS));
        params.date_filter = "delivery_schedule";
        params.exclude_states = "TER";
        params.scheduled_delivery_date = date;
        params.scheduled_delivery_slot = slot;
        params.courier = courier;    // SINGPOST OR XDEL
        params.shipment_type = "NORMAL";
        params.status = "";
        params.transaction_type = "initial";
        module.exports.executeECommRequest("ADMIN", "GET", "sales_orders.json",
            params, undefined, function (err, result) {
                if (callback) callback(err, result);
            });
    },

    customerDeviceInfo: (account_number, callback) => {
        const params = { account_number };
        module.exports.executeECommRequest('API', 'GET', 'api/mobile/v1/customer_device_info.json',
            params, undefined, (err, result) => { if(callback) callback(err, result); });
    },

    executeECommRequest: function (type, method, path, qs, json, callback) {
        var host;
        var port;
        if (type === "API") {
            if (Array.isArray(config.ECOMM_API_HOST)) {
                if (serverNum >= config.ECOMM_API_HOST.length) {
                    serverNum = 0;
                }

                host = config.ECOMM_API_HOST[serverNum];
                serverNum++;
            } else {
                host = config.ECOMM_API_HOST;
            }

            port = config.ECOMM_API_PORT;
        } else if (type === "ADMIN") {
            host = config.ECOMM_ADMIN_HOST;
            port = config.ECOMM_ADMIN_PORT;
        } else {
            return callback(new Error("Type " + type + " is not supported"));
        }

        var url = "http://" + host + ":" + port + "/" + path;
        if (!qs) qs = {};

        qs["user_email"] = config.ECOMM_ADMIN_USER_EMAIL;
        qs["user_token"] = config.ECOMM_ADMIN_USER_TOKEN;

        if (log) common.log("ECommManager", "execute eComm request, type=" + type + ", method=" + method +
        ", url=" + url + ", query=" + JSON.stringify(qs) + ", json=" + JSON.stringify(json));

        request({
            uri: url,
            method: method,
            qs: qs,
            json: json,
            timeout: 35000,
            headers: {
                Authorization: "aca2286153a959de1d341abc0d4a5b15"
            }
        }, function (err, response, body) {
            if (err) {
                if (log) common.log('ECommManager',`Error: ${err}`);
                if (callback) return callback(err);
            } else {
                var bodyObj = common.safeParse(body);
                if (log) common.log('ECommManager', `Response body: ${bodyObj}`);
                if (callback) callback(undefined, bodyObj);
            }
        });
    },

    /**
     *
     */
    checkStatus: (billingAccountNumber, uuid, orderRef, callback) => {
        checkStatusInternal(undefined, billingAccountNumber, uuid, orderRef, callback);
    },

    /**
     *
     */
    cancelPayment: (billingAccountNumber, uuid, orderRef, callback) => {
        cancelPaymentInternal(undefined, billingAccountNumber, uuid, orderRef, callback)
    },

    getPaymentUrl: (data) => {
        let { serviceInstanceNumber, bankName, addonId } = data;
        let { price, title, description_short } = ec.findPackage(ec.packages(), addonId);
        let apiPromise = Bluebird.promisify(module.exports.executeECommRequest);

        return apiPromise("API", "POST", "api/payment/v1/boost_with_bank_rewards_points.json",
            {
              description: description_short,
              serviceInstanceNumber,
              bankName,
              addonId,
              price,
              title
            }, 
            {}
          );
    },

    updateSuspensionState: (suspension_state, service_instance_number) => {
        let apiPromise = Bluebird.promisify(module.exports.executeECommRequest);
        let urlPath = "api/kirk/v1/update_suspension_status";

        return apiPromise("API", "POST", urlPath,
        {
            service_instance_number,
            suspension_state
        },
        {}
      );
    }
}

function checkStatusInternal(customerAccountNumber, billingAccountNumber, uuid, orderRef, callback) {
    if (!callback) callback = () => {
    }

    var urlPath = "api/payment/v1/payment_with_recurrent_token_query_status.json";
    var chargeParams = {};
    if (orderRef) {
        chargeParams.billingAccountNumber = billingAccountNumber;
        chargeParams.order_ref = orderRef;
    } else if (uuid) {
        chargeParams.billingAccountNumber = billingAccountNumber;
        chargeParams.external_id = uuid;
        chargeParams.externalId = uuid;
    }

    var handleResult = (err, result) => {
        var response = {
            type: "CHECK_STATUS",
            orderReferenceNumber: orderRef,
            uuid: orderRef,
            customerAccountNumber: customerAccountNumber,
            billingAccountNumber: billingAccountNumber,
            error: err ? err.message : undefined,
            status: err ? err.status : "OK",
            errorDescription: resourceManager.getErrorValues(err),
            params: chargeParams,
            result: result,
            ts: new Date().getTime()
        }

        // no need to save log

        if (callback) callback(err, response);
    }

    module.exports.executeECommRequest("API", "GET", urlPath, undefined, chargeParams, (err, response) => {
        if (err) {
            var status = (err.message === "ETIMEDOUT" || err.message === "ESOCKETTIMEDOUT") ? "ERROR_TIMEOUT" : "ERROR_NETWORK";
            common.error("ECommManager", "checkStatusInternal: start transaction error, billingAccountNumber=" + billingAccountNumber +
            ", status=" + status + ", error=" + err.message);
            return handleResult(new BaseError(err.message, status));
        }

        if (log) common.log("ECommManager", "checkStatus: start transaction result, " +
        "billingAccountNumber=" + billingAccountNumber + ", response=" + JSON.stringify(response));

        if (!response) {
            common.error("ECommManager", "checkStatusInternal: start transaction response is empty, " +
            "billingAccountNumber=" + billingAccountNumber);
            return handleResult(new BaseError("Start transaction response is empty", "ERROR_EMPTY_RESPONSE"), response);
        }

        return handleResult(undefined, response)
    });
}

function cancelPaymentInternal(customerAccountNumber, billingAccountNumber, uuid, orderRef, callback) {
    if (!callback) callback = () => {
    }

    var start = new Date();
    var totalTime = 0;

    var urlPath = "api/payment/v1/cancel_payment_with_ref.json";
    var urlStatusPath = "/api/payment/v1/payment_with_recurrent_token_query_status.json";
    var params = {};
    if (orderRef) {
        params.customerAccountNumber = customerAccountNumber;
        params.billingAccountNumber = billingAccountNumber;
        params.order_ref = orderRef;
    } else if (uuid) {
        params.customerAccountNumber = customerAccountNumber;
        params.billingAccountNumber = billingAccountNumber;
        params.external_id = uuid;
        params.externalId = uuid;
    }

    var handleResult = (err, result) => {
        var response = {
            type: "CANCEL",
            orderReferenceNumber: orderRef,
            uuid: uuid,
            customerAccountNumber: customerAccountNumber,
            billingAccountNumber: billingAccountNumber,
            error: err ? err.message : undefined,
            status: err ? err.status : "OK",
            errorDescription: resourceManager.getErrorValues(err),
            params: params,
            result: result,
            ts: new Date().getTime()
        }

        // clear cache after cancellation
        elitecoreBillService.clearCustomerPaymentsCache(billingAccountNumber);

        db.notifications_paas.insert(response, function (logError) {
            if (logError) common.error("ECommManager", "cancelPaymentInternal: log saving error, " +
            "billingAccountNumber=" + billingAccountNumber + ", error=" + logError.message + ", " + JSON.stringify(response));
            if (callback) callback(err, response);
        });
    }

    module.exports.executeECommRequest("API", "POST", urlPath, undefined, params, (err, response) => {
        if (err) {
            var status = (err.message === "ETIMEDOUT" || err.message === "ESOCKETTIMEDOUT") ? "ERROR_TIMEOUT" : "ERROR_NETWORK";
            common.error("ECommManager", "cancelPaymentInternal: start transaction error, billingAccountNumber=" + billingAccountNumber +
            ", status=" + status + ", error=" + err.message);
            return handleResult(new BaseError(err.message, status));
        }

        if (log) common.log("ECommManager", "cancelPaymentInternal: start transaction result, " +
        "billingAccountNumber=" + billingAccountNumber + ", response=" + JSON.stringify(response));

        if (response.response) response = response.response;
        if (response.code != 0) {
            var status = response.charge_status;
            var responseText = response ? JSON.stringify(response) : "empty response";
            if (responseText.length > 300) {
                responseText = responseText.substring(0, 300);
            }

            common.error("ECommManager", "cancelPaymentInternal: failed to start payment gateway cancel transaction, response code "
            + response.code + ", status=" + status + ", data=" + responseText);
            return handleResult(new BaseError("Failed to start payment gateway cancellation transaction, response code "
            + response.code + " (" + responseText + ")", (status ? status : "ERROR_TR_CANCEL_FAILED")), response);
        }

        var statusCheckRequest = function () {
            if (log) common.log("ECommManager", "cancelPaymentInternal: check transaction status, " +
            "account=" + customerAccountNumber + ", orderRef=" + orderRef);

            var start = new Date();
            module.exports.executeECommRequest("API", "POST", urlStatusPath, undefined, params, function (err, response) {
                totalTime += new Date() - start;
                if (err) {
                    common.error("ECommManager", "cancelPaymentInternal: check transaction status error, " +
                    "account=" + customerAccountNumber + ", err=" + err);
                    return handleResult(new BaseError(err.message, "ERROR_NETWORK"));
                }

                if (log) common.log("ECommManager", "cancelPaymentInternal: check transaction, billingAccountNumber="
                + billingAccountNumber + " totalTime=" + totalTime + "ms, response=" + JSON.stringify(response));

                if (!response) {
                    common.error("ECommManager", "cancelPaymentInternal: check transaction response is empty, " +
                    "billingAccountNumber=" + billingAccountNumber);
                    return handleResult(new BaseError("Check transaction response is empty",
                        "ERROR_TR_CANCEL_EMPTY_RESPONSE"), response);
                }

                if (response.response) response = response.response;

                /**
                 *  code: -1 -> Failure
                 *  code:  0 -> Success
                 *  code:  1 -> Still processing
                 */
                if (response.code == 0) {
                    if (log) common.log("ECommManager", "cancelPaymentInternal: transaction status=success, " +
                    "billingAccountNumber=" + billingAccountNumber);

                    return handleResult(undefined, {
                        totalTime: totalTime,
                        response: response,
                        chargeParams: params
                    });

                } else if (response.code == -1) {
                    var status = response.payment_status;
                    var responseText = response ? JSON.stringify(response) : "empty response";
                    if (responseText.length > 300) {
                        responseText = responseText.substring(0, 300);
                    }

                    if (!response) response = {};
                    if (!response.order_ref) {
                        response.order_ref = orderRef;
                    }

                    common.error("ECommManager", "cancelPaymentInternal: transaction status=failure, " +
                    "billingAccountNumber=" + billingAccountNumber + ", code="
                    + response.code + ", status=" + status + ", data=" + responseText);

                    return handleResult(new BaseError("Failed to check payment cancellation status, response code "
                    + response.code + " (" + responseText + ")", (status ? status : "ERROR_TR_CANCEL_STATUS_CHECK_FAILURE")), response);

                } else if (response.code == 1) {
                    if (log) common.log("ECommManager", "cancelPaymentInternal: transaction status=processing, " +
                    "billingAccountNumber=" + billingAccountNumber);

                    if (totalTime < 90000) {
                        var requestDelay = totalTime < 10000 ? 500 : 2000;
                        totalTime += requestDelay;
                        setTimeout(statusCheckRequest, requestDelay);
                    } else {
                        if (!response) response = {};
                        if (!response.order_ref) {
                            response.order_ref = orderRef;
                        }

                        common.error("ECommManager", "cancelPaymentInternal: check transaction status timeout, " +
                        "billingAccountNumber=" + billingAccountNumber + ", totalTime=" + totalTime);

                        return handleResult(new BaseError("Transaction has not been processed in 60 sec",
                            "ERROR_TR_CANCEL_SLOW_TRANSACTION"), response);
                    }
                } else {
                    if (log) common.log("ECommManager", "cancelPaymentInternal: transaction status=unknown, unrecognized status, " +
                    ", billingAccountNumber=" + billingAccountNumber + ", response code=" + response.code);

                    return handleResult(new BaseError("Transaction error, unrecognized status",
                        "ERROR_TR_CANCEL_INVALID_RESPONSE"), response);
                }
            });
        };

        totalTime += 1500;
        setTimeout(() => {
            statusCheckRequest();
        }, 1500);
    });
}

function payInternal(customerAccountNumber, billingAccountNumber, amount, uuid, callback) {
    if (!callback) callback = () => {
    }

    var transactionStart = new Date();
    var totalTimeAbsolute = 0;
    var start = new Date();
    var totalTime = 0;

    var urlPath = "api/payment/v1/payment_with_recurrent_token.json";
    var urlStatusPath = "api/payment/v1/payment_with_recurrent_token_query_status.json";
    var chargeParams = {
        account_number: customerAccountNumber,
        billing_account_number: billingAccountNumber,
        amount: amount,
        uuid: uuid,
        description: 'Payment',
        externalId: uuid,
        external_id: uuid,
        channel:  'Payment-Only',
        invoiceId: 'N/A',
        package_name: 'N/A'
    }

    var orderRef;
    var handleResult = function (err, result) {
        var response = {
            type: "PAYMENT",
            orderReferenceNumber: orderRef,
            uuid: uuid,
            customerAccountNumber: customerAccountNumber,
            billingAccountNumber: billingAccountNumber,
            amount: amount,
            error: err ? err.message : undefined,
            status: err ? err.status : "OK",
            errorDescription: resourceManager.getErrorValues(err),
            totalTime: totalTime,
            params: chargeParams,
            result: result,
            ts: new Date().getTime()
        }

        // clear cache after payment finished
        elitecoreBillService.clearCustomerPaymentsCache(billingAccountNumber);

        db.notifications_paas.insert(response, function (logError) {
            if (logError) common.error("ECommManager", "payInternal: log saving error, " +
            "billingAccountNumber=" + billingAccountNumber + ", error=" + logError.message + ", " + JSON.stringify(response));
            if (callback) callback(err, response);
        });
    }

    elitecoreConnector.loadBssMode(function (err, result) {
        if (result && result.offline) {
            return handleResult(new BaseError("CMS is in OFFLINE mode, all API calls blocked", "ERROR_OFFLINE_MODE"));
        }

        // clear cache before starting payment
        elitecoreBillService.clearCustomerPaymentsCache(billingAccountNumber);

        if (log) common.log("ECommManager", "payInternal: start transaction, " +
        "billingAccountNumber=" + billingAccountNumber + ", params=" + JSON.stringify(chargeParams));

        module.exports.executeECommRequest("API", "POST", urlPath, undefined, chargeParams, function (err, response) {
            totalTime = new Date() - start;
            if (err) {
                var status = (err.message === "ETIMEDOUT" || err.message === "ESOCKETTIMEDOUT") ? "ERROR_TIMEOUT" : "ERROR_NETWORK";
                common.error("ECommManager", "payInternal: start transaction error, " +
                "billingAccountNumber=" + billingAccountNumber + ", status=" + status + ", error=" + err.message);
                return handleResult(new BaseError(err.message, status));
            }

            if (log) common.log("ECommManager", "payInternal: transaction started, " +
            "billingAccountNumber=" + billingAccountNumber + ", totalTime=" + totalTime + "ms, response=" + JSON.stringify(response));

            if (!response) {
                common.error("ECommManager", "payInternal: start transaction response is empty, " +
                "billingAccountNumber=" + billingAccountNumber);

                return handleResult(new BaseError("Start transaction response is empty", "ERROR_EMPTY_RESPONSE"), response);
            }

            if (response.response) response = response.response;
            if (response.code != 0) {
                var status = response.charge_status;
                var responseText = response ? JSON.stringify(response) : "empty response";
                if (responseText.length > 300) {
                    responseText = responseText.substring(0, 300);
                }

                common.error("ECommManager", "payInternal: failed to start payment gateway transaction, response code "
                + response.code + ", status=" + status + ", data=" + responseText);
                return handleResult(new BaseError("Failed to start payment gateway transaction, response code "
                + response.code + " (" + responseText + ")", (status ? status : "ERROR_TR_PAYMENT_INITIATION_FAILED")), response);
            }

            orderRef = response.order_ref;
            var statusParams = {
                order_ref: orderRef
            }

            var statusCheckRequest = function () {
                if (log) common.log("ECommManager", "payInternal: check transaction status, " +
                "billingAccountNumber=" + billingAccountNumber + ", orderRef=" + orderRef);

                var start = new Date();
                module.exports.executeECommRequest("API", "POST", urlStatusPath, undefined, statusParams, function (err, response) {
                    totalTimeAbsolute = new Date() - transactionStart;
                    totalTime += new Date() - start;

                    if (err) {
                        common.error("ECommManager", "payInternal: check transaction status error, " +
                        "billingAccountNumber=" + billingAccountNumber + ", err=" + err);
                        return handleResult(new BaseError(err.message, "ERROR_NETWORK"));
                    }

                    if (log) common.log("ECommManager", "payInternal: checked transaction status, billingAccountNumber="
                    + billingAccountNumber + ", totalTime=" + totalTime + "ms, totalTimeAbsolute=" +
                    totalTimeAbsolute + "ms, response=" + JSON.stringify(response));

                    if (!response) {
                        common.error("ECommManager", "payInternal: check transaction response is empty, " +
                        "billingAccountNumber=" + billingAccountNumber);

                        return handleResult(new BaseError("Check transaction response is empty",
                            "ERROR_TR_PAYMENT_EMPTY_RESPONSE"), response);
                    }

                    if (response.response) response = response.response;

                    /**
                     *  code: -1 -> Failure
                     *  code:  0 -> Success
                     *  code:  1 -> Still processing
                     */
                    if (response.code == 0) {
                        if (log) common.log("ECommManager", "payInternal: transaction status=success, " +
                        "billingAccountNumber=" + billingAccountNumber);

                        return handleResult(undefined, {
                            totalTime: totalTime,
                            totalTimeAbsolute: totalTimeAbsolute,
                            response: response,
                            chargeParams: chargeParams
                        });

                    } else if (response.code == -1) {
                        var status = response.payment_status;
                        var responseText = response ? JSON.stringify(response) : "empty response";
                        if (responseText.length > 300) {
                            responseText = responseText.substring(0, 300);
                        }

                        if (!response) response = {};
                        if (!response.order_ref) {
                            response.order_ref = orderRef;
                        }

                        if (log) common.log("ECommManager", "payInternal: transaction status=failure, " +
                        "billingAccountNumber=" + billingAccountNumber + ", code="
                        + response.code + ", status=" + status + ", data=" + responseText);

                        if (!status || status.indexOf("PF_") == -1) {
                            common.error("ECommManager", "payInternal: internal transaction failure, " +
                            "billingAccountNumber=" + billingAccountNumber + ", code="
                            + response.code + ", unknown status=" + status + ", data=" + responseText);
                        }

                        var errStatus = (status ? status : "ERROR_TR_PAYMENT_STATUS_CHECK_FAILURE");
                        return handleResult(new BaseError("Failed to check payment transaction status, response code "
                        + response.code + " (" + responseText + ")", errStatus), response);

                    } else if (response.code == 1) {
                        if (log) common.log("ECommManager", "payInternal: transaction status=processing, " +
                        "billingAccountNumber=" + billingAccountNumber);

                        if (totalTime < 90000) {
                            var requestDelay = totalTime < 10000 ? 500 : 2000;
                            totalTime += requestDelay;
                            setTimeout(statusCheckRequest, requestDelay);
                        } else {
                            if (!response) response = {};
                            if (!response.order_ref) {
                                response.order_ref = orderRef;
                            }

                            common.error("ECommManager", "payInternal: check transaction status timeout, " +
                            "billingAccountNumber=" + billingAccountNumber + ", totalTime=" + totalTime);

                            return handleResult(new BaseError("Transaction has not been processed in 60 sec",
                                "ERROR_TR_PAYMENT_SLOW_TRANSACTION"), response);
                        }
                    } else {
                        if (log) common.log("ECommManager", "payInternal: transaction status=unknown, " +
                        "unrecognized status, billingAccountNumber=" + billingAccountNumber + ", response code=" + response.code);

                        return handleResult(new BaseError("Transaction error, unrecognized status",
                            "ERROR_TR_PAYMENT_INVALID_RESPONSE"), response);
                    }
                });
            };

            totalTime += 1500;
            setTimeout(() => {
                statusCheckRequest();
            }, 1500);
        });
    });
}
