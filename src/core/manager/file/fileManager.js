var async = require('async');
var request = require('request');
var xlsx = require('node-xlsx');

var config = require('../../../../config');
var common = require('../../../../lib/common');
var db = require('../../../../lib/db_handler');

var BaseError = require('../../errors/baseError');

var log = config.LOGSENABLED;

module.exports = {

    loadFileBuffer: (fileId, type, callback) => {
        if (!callback) callback = () => {
        }

        loadFileFromDatabase(fileId, type, (err, result) => {
            if (err) {
                return callback(err);
            }

            callback(undefined, result.file.buffer);
        });
    },

    loadParsedXLSFile: (fileId, type, callback) => {
        if (!callback) callback = () => {
        }

        loadFileFromDatabase(fileId, type, (err, result) => {
            if (err) {
                return callback(err);
            }

            try {
                var obj = xlsx.parse(result.file.buffer);
            } catch (err) {
                return callback(new BaseError("Failed to parse XLS file (" + err.message + ")", "ERROR_FAILED_TO_PARSE"));
            }

            callback(undefined, obj);
        });
    },

    loadFileFromDatabase: (fileId, type, callback) => {
        loadFileFromDatabase(fileId, type, callback);
    }
}

function loadFileFromDatabase(fileId, type, callback) {
    if (!callback) callback = () => {
    }

    if (!fileId || !type) {
        return callback(new BaseError("Invalid params", "ERROR_INVALID_PARAMS"));
    }

    var collection;
    if (type === "documents") {
        collection = db.profile_doc_files_get;
    } else if (type === "temp") {
        collection = db.tempFilesGet;
    } else if (type === "resource") {
        collection = db.resourceFilesGet;
    } else {
        return callback(new BaseError("Invalid type", "ERROR_INVALID_TYPE"));
    }

    collection(fileId, function (err, item) {
        if (err) {
            return callback(err);
        }
        if (!item) {
            return callback(new BaseError("Invalid type", "ERROR_FILE_NOT_FOUND"));
        }

        callback(undefined, item);
    });
}