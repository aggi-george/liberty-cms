var soap = require('soap');
var xmlParse = require('xml2js').parseString;
var config = require(__base + '/config');
var common = require(__lib + '/common');

var LOG = config.LOGSENABLED;
var AUTH_HDR;

module.exports = {

    VASMaintenance: function(data, callback) {
        var start = new Date();
        var method = "Execute";
        var client = ServiceInterface;
        var xml = "<![CDATA[<GenericInput messageName=\"VASMaintenance\">" +
            "<ExternalRefId>" + data.serviceInstanceNumber + "</ExternalRefId>" +
            "<MSISDN>" + data.msisdn + "</MSISDN>" +
            "<IMSINo>" + data.imsi + "</IMSINo>" +
            "<Action>" + data.action + "</Action></GenericInput>]]>";
        var parameters = {
            "inputXML" : { "$xml" : xml },
            "sChannelId" : "CM001",
            "sUserId" : "0",
            "onErrorContinue" : "false" };
        soapCall(client, method, parameters, start, callback);
    },
    
    CheckOrderStatus: function(data, callback) {
        var start = new Date();
        var method = "Execute";
        var client = ServiceInterface;
        var xml = "<![CDATA[<CheckOrderStatusInput messageName=\"CheckOrderStatus\">" +
            "<ExternalRefId>" + data.serviceInstanceNumber + "</ExternalRefId>" +
            "<M1RefId>" + data.m1RefId + "</M1RefId>" +
            "<MSISDN>" + data.msisdn + "</MSISDN>" +
            "<IMSINo>" + data.imsi + "</IMSINo></CheckOrderStatusInput>]]>";
        var parameters = {
            "inputXML" : { "$xml" : xml },
            "sChannelId" : "CM001",
            "sUserId" : "0",
            "onErrorContinue" : "false" };
        soapCall(client, method, parameters, start, callback);
    },

    MNPCreatePortInOrder: function(data, callback) {
        var start = new Date();
        var method = "Execute";
        var client = ServiceInterface;
        var xml = "<![CDATA[<MNPMvnoOrderInput messageName=\"MNPCreatePortInOrder\">" +
            "<DonorTelco>" + data.donorTelco + "</DonorTelco>" +
            "<ExternalReferenceID>" + Math.floor(Math.random() * 1000000) + "</ExternalReferenceID>" +
            "<OldReferenceId></OldReferenceId>" +
            "<ByLOA>1</ByLOA>" +
            "<UndertakingAck>1</UndertakingAck>" +
            "<OrderTransferTime>" + common.getDateDayFirst(new Date()) + " 13:00:00</OrderTransferTime>" +
            "<OrderApprovalTime>" + common.getDateDayFirst(new Date()) + " 23:00:00</OrderApprovalTime>" +
            "<RouteNumber>1410</RouteNumber>" +
            "<ServiceType>Postpaid</ServiceType>" +
            "<CustomerType>Corporate</CustomerType>" +
            "<CustomerName>" + data.customerName + "</CustomerName>" +
            "<CustomerId>" + data.customerId + "</CustomerId>" +
            "<IdType>5</IdType>" +
            "<CustomerSignDate>" + common.getDateDayFirst(new Date()) + "</CustomerSignDate>" +
            "<AccountNo>" + data.serviceInstanceNumber + "</AccountNo>" +
            "<AuthorName></AuthorName>" +
            "<AuthorPhone></AuthorPhone>" +
            "<AuthorEmail></AuthorEmail>" +
            "<AuthorFAX></AuthorFAX>" +
            "<AuthorDate></AuthorDate>" +
            "<Remark></Remark>" +
            "<CreationDate>" + common.getDateDayFirst(new Date()) + "</CreationDate>" +
            "<ServiceNos><PortingServiceNoInfo>" +
            "    <PortInServiceNo>" + data.portinNumber + "</PortInServiceNo>" +
            "    <M1No>" + data.tempNumber + "</M1No>" +
            "    <M1IMSI>" + data.imsiNumber + "</M1IMSI>" +
            "    <M1HLR>G01</M1HLR>" +
            "</PortingServiceNoInfo></ServiceNos>" +
            "<CaseID></CaseID>" +
            "</MNPMvnoOrderInput> ]]>";
        var parameters = {
            "inputXML" : { "$xml" : xml },
            "sChannelId" : "CM001",
            "sUserId" : "0",
            "onErrorContinue" : "false"
        }
        soapCall(client, method, parameters, start, callback);
    }

}

var url_options = {
    ignoredNamespaces: {
        namespaces: [ 'targetNamespace', 'typedNamespace', 'xs' ],
        override: true
    }
}

var ServiceFacade;
soap.createClient('http://' + config.HLRAUTH + '/SingleSignOn/ServiceFacade.asmx?wsdl',
        url_options, function(err, client) {
    if ( err || !client ) {
        common.error("WSDL Error", "ServiceFacade");
    } else {
        ServiceFacade = client;
        var authInterval = setInterval(function redo() {
            reauth();
            return redo;
        }(), 12 * 60 * 60 * 1000);      // reauthenticate every half day
    }
});

var ServiceInterface;
soap.createClient('http://' + config.HLRHOST + '/MobileOneBusinessFascade/ServiceInterface/ServiceInterface.asmx?wsdl',
        url_options, function(err, client) {
    if ( err || !client ) {
        common.error("WSDL Error", "ServiceInterface");
    } else {
        ServiceInterface = client;
    }
});

function soapCall(client, method, parameters, start, callback) {
    if (!client){
        var error = "SOAP error - " + method + " - " +
            JSON.stringify(parameters) + " - " +
            "client not ready";
        common.error(method, error);
        callback(new Error(error));
    } else {
        if ( method != "GetAuthenticationTicketForClients" ) {
            var soapHeader = { "tns:AuthenticationSoapHeader" : {
                        "tns:Credential" : AUTH_HDR } };
            client.clearSoapHeaders();
            client.addSoapHeader(soapHeader);
        }
        client[method](parameters, function(err, result) {
            if (err) {
                var error = "SOAP error - " + method + " - " +
                    JSON.stringify(parameters) + " - " +
                    JSON.stringify(err);
                common.error(method, error);
                callback(error);
            } else {
                if ( (method == "Execute") && result.ExecuteResult ) {
                    processExecute(result.ExecuteResult, callback);
                } else {
                    callback(false, result);
                }
            }
        }, { "timeout" : 15000 });
    }
}

function processExecute(execute, callback) {
    var reply = new Object;
    reply.errorCode = (execute.WebServiceResult) ? execute.WebServiceResult.ErrorCode : undefined;
    if (execute.Return && execute.Return.string && execute.Return.string[0]) {
        xmlParse(execute.Return.string[0], function (jsErr, jsResult) {
            if (jsErr) return callback(jsErr);
            var result = jsResult.TransactionResult ? jsResult.TransactionResult :
                jsResult.MNPOrderResult ? jsResult.MNPOrderResult : undefined;
            if (!result || !result.M1RefId || !result.Status) return callback(new Error("Unknown result"));
            reply.m1RefId = result.M1RefId[0];
            reply.status = result.Status[0];
            callback(undefined, reply);
        });
    } else {
        if ( execute.WebServiceResult && (([-80406, -80408]).indexOf(execute.WebServiceResult.ErrorCode) > -1) ) reauth();
        callback({ "message" : execute });
    }
}

function GetAuthenticationTicketForClients(callback) {
    var start = new Date();
    var method = "GetAuthenticationTicketForClients";
    var client = ServiceFacade;
    var parameters = {      "username" : "LWUser",
                "password" : "bRxg4ERrRSTINSR3",
                "accountType" : "PSWebServices" };
    soapCall(client, method, parameters, start, callback);
}

function reauth() {
    GetAuthenticationTicketForClients(function (err, result) {
        if (!err && result && result.GetAuthenticationTicketForClientsResult) {
            common.log("getAuthentication", "refresh AUTH_HDR");
            AUTH_HDR = result.GetAuthenticationTicketForClientsResult.AuthTicket;
        } else {
            common.error("WSDL Error", "ServiceFacade GetAuthentication");
        }
    });
}
