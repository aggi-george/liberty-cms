var async = require('async');
var common = require(__lib + '/common');
var ec = require(__lib + '/elitecore');
var db = require(__lib + '/db_handler');
var config = require(__base + '/config');
var connector = require(__manager + '/hlr/connector');

var LOG = config.LOGSENABLED;

module.exports = {

    addCallerId: "AddCallerID",

    removeCallerId: "RemoveCallerID",

    addCallerIdNonDisplay: "AddCallerIDNonDisplay",

    removeCallerIdNonDisplay: "RemoveCallerIDNonDisplay",

    suspension: "Suspension",

    reconnection: "Reconnection",

    roamingOff: "roamingOff",

    datavoicesms: function() {
        return ec.packages().general.map(function (o) {
            if (o.app == "vas_data") return o.id; else return undefined;
        }).filter(function (o) { return o;})[0];
    },

    voicesms: function() {
        return ec.packages().general.map(function (o) {
            if (o.app == "vas_auto") return o.id; else return undefined;
        }).filter(function (o) { return o;})[0];
    },

    //todo: cid: function() {},

    set: function(type, prefix, number, suppressNotify, callback) {
        if (!type || !prefix || !number) return callback(new Error("Incomplete Parameters"));
        var tasks = new Array;
        if (type == module.exports.roamingOff) {
            tasks.push(function (callback) {
                hlrUpdate(type, prefix, number, "RemoveAutoRoaming", suppressNotify, callback);
            });
            tasks.push(function (callback) {
                hlrUpdate(type, prefix, number, "RemoveDataRoaming", suppressNotify, callback);
            });
        } else if (type == module.exports.datavoicesms()) {
            tasks.push(function (callback) {
                hlrUpdate(type, prefix, number, "AddAutoRoaming", suppressNotify, callback);
            });
            tasks.push(function (callback) {
                hlrUpdate(type, prefix, number, "AddDataRoaming", suppressNotify, callback);
            });
        } else if (type == module.exports.voicesms()) {
            tasks.push(function (callback) {
                hlrUpdate(type, prefix, number, "AddAutoRoaming", suppressNotify, callback);
            });
            tasks.push(function (callback) {
                hlrUpdate(type, prefix, number, "RemoveDataRoaming", suppressNotify, callback);
            });
        } else if (type == module.exports.suspension) {
            tasks.push(function (callback) {
                hlrUpdate(type, prefix, number, "Suspension", suppressNotify, callback);
            });
        } else if (type == module.exports.reconnection) {
            tasks.push(function (callback) {
                hlrUpdate(type, prefix, number, "Reconnection", suppressNotify, callback);
            });
        } else if (type == module.exports.addCallerIdNonDisplay) {
            tasks.push(function (callback) {
                hlrUpdate(type, prefix, number, "AddCallerIDNonDisplay", suppressNotify, callback);
            });
        } else if (type == module.exports.removeCallerIdNonDisplay) {
            tasks.push(function (callback) {
                hlrUpdate(type, prefix, number, "RemoveCallerIDNonDisplay", suppressNotify, callback);
            });
        } else if (type == module.exports.addCallerId) {
            tasks.push(function (callback) {
                hlrUpdate(type, prefix, number, "AddCallerID", suppressNotify, callback);
            });
        } else if (type == module.exports.removeCallerId) {
            tasks.push(function (callback) {
                hlrUpdate(type, prefix, number, "RemoveCallerID", suppressNotify, callback);
            });
        } else {
            return callback(new Error("Unknown type"));
        }
        async.parallel(tasks, function (err, result) {
            callback(err, result);
        });
    },

    getPending: function(callback) {
        var query = "SELECT p.service_instance_number, o.value FROM customerOptions o LEFT JOIN customerProfile p ON p.id=o.profile_id WHERE " +
            " o.option_key IN ('roaming_auto', 'roaming_data') AND o.value NOT IN ('0','1') ORDER BY CAST(value AS INT) ASC";
        db.query_err(query, function(err, rows) {
            var tasks = new Array;
            if (rows) rows.forEach(function(row) {
                tasks.push(function(cb) {
                    ec.getCustomerDetailsService(row.service_instance_number, false, function(error, cache) {
                        var data = {
                            "serviceInstanceNumber" : row.service_instance_number,
                            "m1RefId" : row.value,
                            "msisdn" : cache.number,
                            "imsi" : cache.imsi
                        }
                        connector.CheckOrderStatus(data, function(cErr, cResult) {
                            data.result = cResult;
                            cb(undefined, data);
                        });
                    });
                });
            });
            async.series(tasks, function(aErr, aResult) {
                callback(aErr, aResult);
            });
        });
    },

    logTransaction: function(args, error, result) {
        var logStatus = (result && (result.errorCode === 0)) ? "IN_PROGRESS" : "ERROR";
        var method = (args && args.action) ? args.action : "UNKNOWN";
        var serviceInstanceNumber = (args && args.serviceInstanceNumber) ? args.serviceInstanceNumber : "UNKNOWN";
        db.addonlog.insert({
            "ts" : new Date().getTime(),
            "myfqdn" : config.MYFQDN,
            "method" : method,
            "type" : args.type,
            "serviceInstanceNumber" : serviceInstanceNumber,
            "args" : args,
            "ts_end" : new Date().getTime(),
            "status" : logStatus,
            "result" : result,
            "error" : error,
            "number" : args.msisdn,
            "m1RefId" : (result ? result.m1RefId : undefined),
            "trace" : [ {
                "ts" : new Date().getTime(),
                "myfqdn" : config.MYFQDN,
                "error" : error,
                "result" : result
            } ]
        });
    }

}

function hlrUpdate(type, prefix, number, action, suppressNotify, callback) {
    if (LOG) common.log("hlrUpdate", prefix + " " + number + " " + action);
    var args = new Object;
    args.prefix = prefix;
    args.msisdn = number;
    args.type = type;
    args.action = action;
    args.suppressNotify = suppressNotify;
    if (!action || !prefix || !number || !action) {
        var error = "Missing Parameters";
        common.error("hlrUpdate - " + number, error);
        module.exports.logTransaction(args, error, undefined);
        return callback(new Error(error));
    }
    ec.getCustomerDetailsNumber(prefix, number, false, function(err, cache) {
        if (!cache) {
            var error = "Customer not found";
            common.error("hlrUpdate - " + number, error);
            module.exports.logTransaction(args, error, undefined);
            return callback(new Error(error));
        }
        args.imsi = cache.imsi;
        args.serviceInstanceNumber = cache.serviceInstanceNumber;
        if (!cache.serviceInstanceNumber || !cache.imsi) {
            var error = "Customer missing attributes";
            common.error("hlrUpdate - " + number, error);
            module.exports.logTransaction(args, error, undefined);
            return callback(new Error(error));
        } else if ( cache.imsi == "Unpaired" ) {
            var error = "IMSI Unpaired";
            common.error("hlrUpdate - " + number, error);
            module.exports.logTransaction(args, error, undefined);
            return callback(new Error(error));
        }
        if (LOG) common.log("HLRManager", "hlrUpdate: create new job action=" + action
            + ", args=" + JSON.stringify(args));

        new Promise(function (resolve, reject) {
            var timeout = setTimeout(function () {
                common.error("hlrManager hlrUpdate", "timeout, " + JSON.stringify(args));
                return reject(new Error("Job create timeout"));
            }, 15 * 1000);

            var job = db.queue.create('hlr', args)
                .events(false)
                .removeOnComplete(true)
                .attempts(3)
                .backoff({ "delay" : 2000, "type" : "fixed" })
                .save(function(err) { if (LOG) common.log("hlrUpdate new job", job.id); });
            db.queue.on('job failed', function(id, result) {
                if ( job && (job.id != id) ) return;
                clearTimeout(timeout);
                var error = "Error job creation failed " + id;
                common.error("hlrUpdate", error);
                return reject(new Error(error));
            });
            db.queue.on('job complete', function(id, result){
                if ( job && (job.id != id) ) return;
                clearTimeout(timeout);
                return resolve(result);
            });
        }).then(function (result) {
            return callback(undefined, result);
        }).catch(function (err) {
            return callback(err);
        });

    });
}
