var config = require(__base + "/config");
var common = require(__lib + "/common");
var db = require(__lib + "/db_handler");


module.exports = {

    paymentObj: {
        "receipt" : "",
        "productId" : undefined,
        "subscription" : false,
        "packageName" : "com.circles.gentwo"
    },

    invalidate: function(token, callback) {
        var invalidate_q = "UPDATE inapp_purchase i, msisdn m SET " +
            " i.voided = 1, " +
            " m.credit = m.credit-i.user_credit"+
            " WHERE a.number=m.number AND i.receipt=" + db.escape(token);
        common.log("invalidate apple invalidate_q", invalidate_q);
        db.query_noerr(invalidate_q, function(rows) {
            callback(undefined, { "code" : 0, "status" : "Voided", "result" : (rows ? rows.affectedRows : 0) });
        });
    },

    isValid: function(id, response) {
        if (response.receipt && response.receipt.in_app) {
            var inapp = response.receipt.in_app.filter(function(item) {
                if (item && item.transaction_id == id) return item;
                else return undefined;
            })[0];
            if (inapp && inapp.cancellation_date) return 0;    // cancelled     
            else if (inapp) return 1;    // valid id
            else return 0;    // invalid id, not found
        } else {
            return 0;
        }
    },

    end: function(user_key, transactionId, transactionReceipt, sku, sandbox, callback) {
        var bonus = 1;
        var q_end = "INSERT INTO inapp_purchase (product_id, transaction_id, receipt, user_credit, sandbox, number, status, ts_start, ts_end) SELECT " +
            " " + db.escape(sku) + ", " +
            " " + db.escape(transactionId) + ", " +
            " " + db.escape(transactionReceipt) + ", " +
            " gn.price*" + bonus + ", " +
            " " + db.escape(sandbox) + ", " +
            " a.number, 'end', NOW(), NOW() FROM app a, device d, groupName gn WHERE " +
            " gn.sku=" + db.escape(sku) + " AND gn.platform='ios' AND " +
            " d.app_id=a.id AND d.user_key=" + db.escape(user_key);
        common.log("verifyPayment q_end", q_end);
        db.query_err(q_end, function(err, result) {
            if (!result || (result && !result.insertId)) {
                common.error("apple inapp err", "Insert Fail")
                return callback(new Error("Insert Fail"));
            }
            callback(err, result);
        });
    },

    addCredit: function(user_key, transactionId, callback) {
        var q_topup = "UPDATE msisdn m, inapp_purchase i SET m.credit=m.credit+i.user_credit WHERE " +
                " m.number=i.number AND i.transaction_id=" + db.escape(transactionId);
        common.log("addCredit q_topup", q_topup);
        db.query_err(q_topup, function(err, result) {
            logCDR(user_key, transactionId);
            callback(err, result);
        });
    }

}

function logCDR(user_key, transactionId) {
    var cdrQ = "SELECT m.number, m.credit, i.user_credit topup FROM msisdn m, app a, device d, inapp_purchase i " +
        " WHERE m.number=a.number AND a.id=d.app_id AND m.number=i.number " +
        " AND i.transaction_id=" + db.escape(transactionId) +
        " AND d.user_key=" + db.escape(user_key);
    db.query_noerr(cdrQ, function (origRows) {
        if (!origRows || !origRows[0]) return common.error("apple cdrQ empty", cdrQ);
        db.cdrs.insert({
            "start_time" : new Date().getTime(),
            "orig_ani" : origRows[0].number,
            "previous" : origRows[0].credit - origRows[0].topup,
            "top_up" : origRows[0].topup,
            "credit" : origRows[0].credit,
            "id" : transactionId
        }, function () { });
    });
}
