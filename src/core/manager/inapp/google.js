var config = require(__base + "/config");
var common = require(__lib + "/common");
var db = require(__lib + "/db_handler");

module.exports = {

    paymentObj: {
        "receipt" : "",
        "productId" : "",
        "subscription" : false,
        "packageName" : "com.circles.gentwo",
        "keyObject" : config.iapGoogle
    },

    invalidate: function(token, callback) {
        var invalidate_q = "UPDATE android_inapp a, msisdn m SET " +
            " a.voided = 1, " +
            " m.credit = m.credit-a.user_credit"+
            " WHERE a.number=m.number AND a.purchase_token=" + db.escape(token);
        common.log("invalidate google invalidate_q", invalidate_q);
        db.query_noerr(invalidate_q, function(rows) {
            callback(undefined, { "code" : 0, "status" : "Voided", "result" : (rows ? rows.affectedRows : 0) });
        });
    },

    isValid: function(id, response) {
        if (response.receipt) {
            if (response.receipt.purchaseState == 0) return 1;
            else return 0;
        } else {
            return 0;
        }
    },

    start: function(user_key, payload, email, sku, callback) {
        var q = "INSERT INTO google_purchase (email, sku, payload, number) SELECT " +
            " " + db.escape(email) + ", " +
            " " + db.escape(sku) + ", " +
            " " + db.escape(payload) + ", " +
            " a.number FROM app a, device d WHERE d.app_id=a.id AND d.user_key=" + db.escape(user_key);
        db.query_err(q, function(error, result) {
            if ( result && (result.affectedRows == 1) ) callback(undefined, result);
            else callback(new Error("start INSERT error"));
        });
    },

    cancel: function(user_key, payload, error_code, callback) {
        var q = "UPDATE google_purchase gp, app a, device d SET " +
            " gp.ts_end=NOW(), gp.status='cancel', error_code=" +
            " " + db.escape(error_code) +
            " WHERE gp.payload = " + db.escape(payload) + " AND gp.number=a.number " +
            " AND gp.status='start' AND d.app_id=a.id AND d.user_key=" + db.escape(user_key);
        db.query_err(q, function(error, result) {
            if ( result && (result.affectedRows == 1) ) callback(undefined, result);
            else callback(new Error("cancel UPDATE error"));
        });
    },

    end: function(user_key, payload, token, sku, order_id, purchaseTimeMillis, purchaseState, callback) {
        var d = new Date();
        d.setHours(d.getHours() + 8);           // SGT
        var multiplier = ( common.getDate(d) == "2015-09-24" ) ? 1.5 : 1;       // Promo day
        var q_insert = "INSERT INTO android_inapp (creationdate, number, device_id, productID, orderID, purchase_date, user_credit, " +
            " purchase_state, purchase_token, actual_credit, payload) SELECT NOW(), gp.number, d.device_id, gn.sku," +
            " " + (order_id ? db.escape(order_id) : "NULL") + ", " +
            " " + db.escape(purchaseTimeMillis) + ", " +
            " IF(gn.qty=0," + multiplier + "*gn.price,0), " +
            " " + db.escape(purchaseState) + ", " +
            " " + db.escape(token) + ", " +
            " gn.price, gp.payload FROM groupName gn, google_purchase gp, app a, device d " +
            " WHERE a.number=gp.number AND a.id=d.app_id AND d.status='A' AND d.user_key=" + db.escape(user_key) +
            " AND gn.sku=" + db.escape(sku) +
            " AND gp.payload=" + db.escape(payload) +
            " AND gn.platform='android'" +
            " AND gp.status='start'";
        common.log("verifyPayment credit q_insert", q_insert);
        db.query_err(q_insert, function(err, result) {
            if (!result || (result && !result.insertId)) {
                common.error("google inapp err", "Insert Fail");
                return callback(new Error("Insert Fail"));
            }
            updateGooglePurchase(user_key, payload, callback);
        });
    },

    endSubscription: function(user_key, payload, token, sku, expiry, autoRenewing, callback) {
        var q_update_cust = "INSERT INTO customer_subscription (number, group_plan, group_plan_expire, payload, purchase_token, autorenew) " +
            " SELECT a.number, gn.groupID, " +
            " " + db.escape(common.getDateTime(expiry)) + ", " +
            " " + db.escape(payload) + ", " +
            " " + db.escape(token) + ", " +
            " " + db.escape(autoRenewing) +
            " FROM app a, device d, groupName gn WHERE gn.platform='android' AND d.app_id=a.id AND " +
            " d.user_key=" + db.escape(user_key) + " AND gn.sku=" + db.escape(sku) +
            " ON DUPLICATE KEY UPDATE number=VALUES(number), group_plan_expire=VALUES(group_plan_expire), autorenew=VALUES(autorenew)";
        common.log("verifyPayment subscription q_update_cust", q_update_cust);
        db.query_err(q_update_cust, function(err, result) {
            updateGooglePurchase(user_key, payload, function(uErr, uResult) {
                callback(undefined, result);
            });
        });
    },

    addCredit: function(user_key, order_id, payload, callback) {
        var q_update_m = "UPDATE msisdn m, android_inapp a " +
            " SET m.credit=m.credit+a.user_credit " +
            " WHERE m.number=a.number " +
            " AND (a.orderID=" + db.escape(order_id) + " OR " +
            " a.orderID IS NULL) " +
            " AND a.payload=" + db.escape(payload);
        common.log("verifyPayment credit q_update_m", q_update_m);
        db.query_err(q_update_m, function(mErr, mResult) {
            if (mErr) common.error("google inapp mErr", mErr.message)
            logCDR(user_key, order_id, payload);
            callback(mErr, mResult);
        });
    },

    fetchPrevious: function(order_id, payload, callback) {
        // include already voided transactions to track top-up history of customer
        var validate_q = "SELECT a2.orderID, a2.purchase_token, a2.productID, a2.user_credit, a2.voided FROM " +
            " android_inapp a1, android_inapp a2 " +
            " WHERE a1.number=a2.number AND (a1.orderID=" + db.escape(order_id) + " OR a1.orderID IS NULL) " +
            " AND a1.payload=" + db.escape(payload) +
            " AND a2.purchase_date>a1.purchase_date-(7*24*60*60*1000) " +
            " ORDER BY a2.creationdate DESC";
        common.log("fetchPrevious", validate_q);
        db.query_err(validate_q, function(error, rows) {
            if (error) callback(error);
            else if (!rows || !rows[0] || !rows[0].purchase_token) callback(undefined, []);
            else callback(undefined, rows);
        });
    }

}

function updateGooglePurchase(user_key, payload, callback) {
    var q_update_gp = "UPDATE google_purchase gp, app a, device d SET gp.ts_end=NOW(), gp.status='end' WHERE " +
        " gp.payload=" + db.escape(payload) + " AND gp.number=a.number AND gp.status='start' AND " +
        " d.app_id=a.id AND d.user_key=" + db.escape(user_key);
    common.log("updateGooglePurchase", q_update_gp);
    db.query_err(q_update_gp, function(err, result) {
        callback(err, result);
    });
}

function logCDR(user_key, order_id, payload) {
    var cdrQ = "SELECT m.number, m.credit, aa.user_credit topup FROM msisdn m, app a, device d, android_inapp aa " +
        " WHERE m.number=a.number AND a.id=d.app_id AND m.number=a.number " +
        " AND (aa.orderID=" + db.escape(order_id) + " OR aa.orderID IS NULL) " +
        " AND aa.payload=" + db.escape(payload) + " AND d.user_key=" + db.escape(user_key);
    db.query_noerr(cdrQ, function (origRows) {
        if (!origRows || !origRows[0]) return common.error("google cdrQ empty", cdrQ);
        db.cdrs.insert({
            "start_time" : new Date().getTime(),
            "orig_ani" : origRows[0].number,
            "previous" : origRows[0].credit - origRows[0].topup,
            "top_up" : origRows[0].topup,
            "credit" : origRows[0].credit,
            "id" : order_id
        }, function () { });
    });
}

