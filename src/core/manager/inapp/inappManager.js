var iap = require('iap');
var jwt = require('./jwt');
var request = require('request');
var async = require('async');
var common = require(__lib + '/common');
var db = require(__lib + '/db_handler');
var config = require(__base + '/config');
var google = require(__core + '/manager/inapp/google');
var apple = require(__core + '/manager/inapp/apple');

var LOG = config.LOGSENABLED;

module.exports = {

    google: {

        validate: function(id, token, sku, autoreverse, callback) {
            isSubscription(sku, function(err, subscription) {
                if (err) return callback(err);
                var payment = JSON.parse(JSON.stringify(google.paymentObj));
                payment.receipt = token;
                payment.productId = sku;
                payment.subscription = subscription;
                verifyPayment("google", id, payment, autoreverse, callback); 
            });
        },

        invalidate: function(token, callback) {
            google.invalidate(token, callback);
        },

        isValid: google.isValid,

        cancelSubscription: function(token, sku, callback) {
            var payment = JSON.parse(JSON.stringify(google.paymentObj));
            payment.receipt = token;
            payment.productId = sku;
            payment.subscription = true;
            iap.cancelSubscription("google", payment, callback);
        },

        start: google.start,

        cancel: google.cancel,

        end: function(user_key, payload, token, sku, order_id, purchaseTimeMillis, purchaseState, callback) { 
            google.end(user_key, payload, token, sku, order_id, purchaseTimeMillis, purchaseState, function(err, result) {
                if (err) {
                    common.error("google inapp end err", err.message);
                    return callback(err);
                }
                google.fetchPrevious(order_id, payload, function(error, rows) {
                    if (!rows || rows.length === 0) return google.addCredit(user_key, order_id, payload, callback);
                    validatePrevious("google", rows, function(vErr, valid) {
                        if (!valid) {
                            common.error("google inapp vErr", "Invalid topup potential cheater!");
                            callback(new Error("Invalid topup potential cheater!"));
                        } else {
                            google.addCredit(user_key, order_id, payload, callback);
                        }
                    });
                });
            });
        },

        endSubscription: google.endSubscription,

        voided: function(callback) {
            var keyObject = config.iapGoogle;
            var publisherScope = "https://www.googleapis.com/auth/androidpublisher";
            var payment = JSON.parse(JSON.stringify(module.exports.paymentObj));
            jwt.getToken(keyObject.client_email, keyObject.private_key, publisherScope, function (error, token) {
                if (error) return callback(error);
                var requestUrl = "https://www.googleapis.com/androidpublisher/v2/applications/" +
                    "" + payment.packageName +
                    "/purchases/voidedpurchases?access_token=" +
                    "" + token;
                request({
                    uri: requestUrl,
                    method: 'GET',
                }, function (error, res, responseString) {
                    if (error) return callback(error);
                    if (res.statusCode !== 200)
                        return callback(new Error('Received ' + res.statusCode + ' status code with body: ' + responseString));
                    var responseObject;
                    try {
                        responseObject = JSON.parse(responseString);
                    } catch (e) {
                        return callback(e);
                    }
                    return callback(null, responseObject);
                });
            });
        }

    },

    apple: {

        validate: function(id, token, sku, autoreverse, callback) {
            isSubscription(sku, function(err, subscription) {
                if (err) return callback(err);
                var payment = JSON.parse(JSON.stringify(apple.paymentObj));
                payment.receipt = token;
                //payment.productId = sku; this is broken!
                payment.subscription = subscription;
                verifyPayment("apple", id, payment, autoreverse, callback); 
            });
        },

        invalidate: function(token, callback) {
            apple.invalidate(token, callback);
        },

        isValid: apple.isValid,

        end: function(user_key, transactionId, transactionReceipt, sku, sandbox, callback) {
            apple.end(user_key, transactionId, transactionReceipt, sku, sandbox, function(err, result) {
                if (err) {
                    common.error("apple inapp end err", err.message);
                    return callback(err);
                }
                else return apple.addCredit(user_key, transactionId, callback);
            });
        },

    }

}

function isSubscription(sku, callback) {
    var q = "SELECT qty FROM groupName WHERE sku=" + db.escape(sku);
    db.query_err(q, function(error, rows) {
        if (error || !rows) return callback(new Error("SKU not found"));
        var subscription = ( rows && rows[0] && (rows[0].qty == 0) ) ? false : true;
        callback(error, subscription);
    });
}

function verifyPayment(platform, id, payment, autoreverse, callback) {
    iap.verifyPayment(platform, payment, function (err, response) {
        if (LOG) common.log("verifyPayment " + platform, (err ? err.message : JSON.stringify(response)));
        if (err || !response) {
            common.error(platform + " verifyPayment err", err.message);
            callback(err);
        } else if (module.exports[platform].isValid(id, response) === 1) {
            callback(undefined, { "code" : 0, "result" : response, "status" : "Valid" });
        } else if (module.exports[platform].isValid(id, response) === 0) {
            if (autoreverse) {
                module.exports[platform].invalidate(payment.receipt, callback);
            } else {
                callback(undefined, { "code" : -1, "result" : response, "status" : "Invalid" });
            }
        } else {
            callback(undefined, { "code" : 1, "result" : response, "status" : "Unknown" });
        }
    });
}

function validatePrevious(platform, list, callback) {
    var tasks = new Array;
    list.forEach(function (item, idx) {
        if (idx > 0) tasks.push(function (cb) {
            if (item.voided) return cb(undefined, 1);
            module.exports[platform].validate(item.orderID, item.purchase_token, item.productID, true, cb);
        });
    });
    async.series(tasks, function(aErr, aRes) {
        var block = (aRes && (aRes.length > 0)) ? aRes.reduce(function(a,b) { return a+b; }) : undefined;
        // if customer has at least 3 voided transactions in history, reverse current topup
        if ( block && (block >= 3) ) {
            module.exports[platform].invalidate(list[0].purchase_token, function() {
                common.error("validatePrevious", "3 refunds, block!");
                callback(undefined, false);
            });
        } else {
            callback(undefined, true);
        }
    });
}
