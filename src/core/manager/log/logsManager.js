var request = require('request');
var config = require('../../../../config');
var common = require('../../../../lib/common');
var db = require('../../../../lib/db_handler');
var ec = require('../../../../lib/elitecore');

var log = config.LOGSENABLED;
var requestNum = 0;

module.exports = {

    /**
     *
     */
    loadLogs: function (category, type, offset, limit, period, filter, searchType, matchType, callback, projections, noTotalCount) {
        var afterTs = period ? new Date((new Date().getTime() - period * 60 * 60 * 1000)) : "";
        var match = period ? {ts: {"$gt": afterTs.getTime()}} : {};
        var search;

        if (filter) {
            search = filter.replace("\t", "").trim();
            if (search.length > 64) {
                // max 64 chars
                search = search.slice(0, 64);
            }

            if (category === 'account' && type === 'pay_now' && (searchType == "STATUS" || searchType == "INVOICE_ID")) {
                // invoice payments logs combine multiple statuses and invoice ids in a single field,
                // to avoid any confusion REGEX should be used all the time
                matchType = 'REGEX';
            }

            var getSearchObject = (search) => {
                return matchType == "EXACT" ? search : new RegExp(common.escapeRegExp(search), "i");
            }

            if (searchType == "NUMBER") {
                match["$or"] = [{"number": getSearchObject(search)}];
                if (type === 'sms') {
                    match["$or"].push({"to": getSearchObject(search)});
                }
                if (type === 'feedback') {
                    match["$or"].push({"callInfo.aNumber": getSearchObject(search)});
                    match["$or"].push({"callInfo.bNnumber": getSearchObject(search)});
                }
            } else if (searchType == "EMAIL") {
                match["$or"] = [{"email": getSearchObject(search)}];
                if (type === 'email') {
                    match["$or"].push({"to": getSearchObject(search)});
                }
            } else if (searchType == "ACTIVITY") {
                match["$or"] = [{"activity": getSearchObject(search)}];
            } else if (searchType == "SIN") {
                match["$or"] = [{"serviceInstanceNumber": getSearchObject(search)}];
            } else if (searchType == "CAN") {
                match["$or"] = [{"customerAccountNumber": getSearchObject(search)}];
            } else if (searchType == "BAN") {
                match["$or"] = [{"billingAccountNumber": getSearchObject(search)}];
            } else if (searchType == "TYPE") {
                match["$or"] = [{"type": getSearchObject(search)}];
            } else if (searchType == "STATUS") {
                match["$or"] = [
                    {"status": getSearchObject(search)},
                    {"statusEmail": getSearchObject(search)},
                    {"statusInternal": getSearchObject(search)},
                    {"statusSMS": getSearchObject(search)},
                    {"statusSelfcare": getSearchObject(search)},
                ];
            } else if (searchType == "CODE") {
                match["$or"] = [{"code": getSearchObject(search)}];
            } else if (searchType == "REQUEST_ID") {
                match["$or"] = [{"params.id": parseInt(search)},
                    {"activeRequest.requestId": parseInt(search)},
                    {"activeRequest.id": parseInt(search)}];
            } else if (searchType == "ID") {
                match["$or"] = [{"notificationId": getSearchObject(search)}];
                if (type === 'feedback') {
                    match["$or"].push({"callInfo.callId": getSearchObject(search)});
                }
            } else if (searchType == "ORDER_NUMBER") {
                match["$or"] = [
                    {"params.orderReferenceNumber": getSearchObject(search)},
                    {"order_reference_number": getSearchObject(search)},
                    {"orderno": getSearchObject(search)},
                    {"eCommOrderRef": getSearchObject(search)},
                    {"orderReferenceNumber": getSearchObject(search)}
                ];
            } else if (searchType == "INVOICE_ID") {
                match["$or"] = [
                    {"invoiceId": getSearchObject(search)}
                ];
            } else if (searchType == "REPORT_ID") {
                match["$or"] = [
                    {"reportId": getSearchObject(search)}
                ];
            } else if (searchType == "USER_NAME") {
                match["$or"] = [{"username": getSearchObject(search)}];
            } else if (searchType == "METHOD") {
                match["$or"] = [{"method": getSearchObject(search)}];
            } else if (searchType == "ACCOUNT_STATUS_VALIDATION") {
                match["$or"] = [{"result.statusValidation.result.status": getSearchObject(search)}];
            }
        }

        requestNum++;

        var reqNum = requestNum;
        if (log) common.log("LogManager", "loading request requestNum=" + reqNum + ", afterTs=" + afterTs +
        ", filter=" + filter + ", match=" + JSON.stringify(match) + ", offset=" + offset + ", limit=" + limit);

        var startTime = new Date().getTime();
        var storage = getStorage(category, type);
        if (storage) {
            storage
                .find(match, projections ? projections : {},{ readPreference: db.readPreference.SECONDARY_PREFERRED })
                .skip(offset)
                .sort({"_id": -1})
                .limit(limit)
                .toArray(function (err, items) {
                    if (err) {
                        console.log("from storage");
                        console.log(JSON.stringify(err));
                        if (callback) callback(err);
                        return;
                    }
                    if (!items) {
                        items = [];
                    }

                    if (log) common.log("LogManager", "loaded data, count=" + items.length + ", reqNum=" + reqNum +
                    ", time=" + (new Date().getTime() - startTime) + ", noTotalCount=" + noTotalCount);

                    if ((items.length != 0 && items.length < limit) || noTotalCount) {
                        if (callback) callback(undefined, {data: items, totalCount: (offset + items.length)});
                        return;
                    }

                    startTime = new Date().getTime();

                    new Promise(
                        function (resolve, reject) {
                            var timeout = setTimeout(function () {
                                if (log) common.log("LogManager", "failed to load total count, reqNum="
                                + reqNum + ", time=" + (new Date().getTime() - startTime));

                                reject(new Error("Loading total count timeout"));
                            }, 3000);

                            storage.count(match, function (err, count) {
                                if (err) {
                                    if (log) common.log("LogManager", "failed to load total count, error="
                                    + err.message + ", time=" + (new Date().getTime() - startTime));

                                    reject(err);
                                    return;
                                }
                                if (!count) {
                                    count = 0;
                                }

                                if (log) common.log("LogManager", "loaded total count, reqNum=" + reqNum + ", count=" + count +
                                ", time=" + (new Date().getTime() - startTime));

                                resolve({totalCount: count})
                            });
                        }).then(function (result) {
                            if (callback) callback(undefined, {data: items, totalCount: result.totalCount});
                        }).catch(function (err) {
                            if (callback) callback(undefined, {data: items, totalCount: 9999999});
                        });
                });

        } else {
            console.log("from storage not found to category");
            if (callback) callback(new Error("Storage is not found to category '"
            + category + "' and type '" + type + "'"), []);
        }
    },


    logsAppStates: (logs, callback) => {
        db.appStates.insert(logs, callback);
    }

}

function getStorage(category, type) {
    if (category === 'data') {
        if (type === 'aggregation') {
            return db.notifications_resubscription;
        }
    } else if (category === 'notifications') {
        if (type === 'webhook') {
            return db.notifications_webhook;
        } else if (type === 'all') {
            return db.notifications_logbook;
        } else if (type === 'sms') {
            return db.notifications_sms;
        } else if (type === 'email') {
            return db.notifications_emails;
        } else if (type === 'push') {
            return db.notifications_push;
        }
    } else if (category === 'account') {
        if (type === 'status_change') {
            return db.notifications_status_change;
        } else if (type === 'pay_now') {
            return db.notifications_pay_now;
        } else if (type === 'creation') {
            return db.notifications_account;
        } else if (type === 'creditcap') {
            return db.notifications_credit_cap;
        }
    } else if (category === 'gentwo') {
        if (type === 'feedback') {
            return db.circlesTalkStats;
        }
    } else if (category === 'paas') {
        if (type === 'payments') {
            return db.notifications_paas;
        }
    } else if (category === 'bill') {
        if (type === 'sent') {
            return db.notifications_bills;
        } else if (type === 'sync') {
            return db.notifications_invoices_sync;
        }
    } else if (category === 'internal') {
        if (type === 'web') {
            return db.weblog;
        }
    } else if (category === 'ec') {
        if (type === 'errors') {
            return db.ecerrors;
        }
    } else if (category === 'portin') {
        if (type === 'status') {
            return db.notifications_port_in;
        }
    } else if (category === 'promo') {
        if (type === 'codes') {
            return db.notifications_promo_codes;
        }
    } else if (category === 'addon') {
        return db.addonlog;
    }
    return undefined;
}
