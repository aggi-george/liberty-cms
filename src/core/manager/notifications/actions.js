var async = require('async');
var dateformat = require('dateformat');
var elitecorePortInService = require('../../../../lib/manager/ec/elitecorePortInService');
var ec = require(__lib + '/elitecore');
var db = require(__lib + '/db_handler');
var common = require('../../../../lib/common');
var paas = require('../../../../lib/paas');
var config = require('../../../../config');
var billHelper = require('../account/pdfBillingManager');
var usageManager = require('../account/usageManager');
var bonusTypesService = require('../../../../res/bonus/bonusTypesService');
var bonusManager = require(__core + '/manager/bonus/bonusManager');
var boostManager = require(__core + '/manager/boost/boostManager');
var packagesHelper = require('../../../../lib/packages_helper');
var profileManager = require('../../../core/manager/profile/profileManager');
var promotionsManager = require('../../../core/manager/promotions/promotionsManager');
var referralManager = require('../../../core/manager/promotions/referralManager');
var penaltyManager = require('../../../core/manager/account/penaltyManager');
var paymentManager = require('../../../core/manager/account/paymentManager');
var portInManager = require('../porting/portInManager');
var packages = require('../../../api/internal/packages');
var notificationSend = require('./send');
var constants = require(__core + '/constants');

var LOG = config.LOGSENABLED;

var SG_PREFIX = "65";

// exports

exports.addOnSubscription = addOnSubscription;
exports.addOnUnsubscription = addOnUnsubscription;
exports.pay200 = pay200;
exports.pay200Warning = pay200Warning;
exports.checkRoamingCap = checkRoamingCap;
exports.usageConsumptionData = usageConsumptionData;
exports.portInSuccess = portInSuccess;
exports.portinStatus = portinStatus;
exports.portInFailure = portInFailure;
exports.portInApproved = portInApproved;
exports.appCreditCardChange = appCreditCardChange;
exports.selfcareInstall = selfcareInstall;
exports.emailChanged = emailChanged;
exports.deliveryFailed = deliveryFailed;
exports.chargeWhatsappPenalty = chargeWhatsappPenalty;
exports.ignoreWhatsappPenaltyNotificationIfDFF = ignoreWhatsappPenaltyNotificationIfDFF;
exports.handleFupNotification = handleFupNotification;
exports.validateBasePlan = validateBasePlan;
exports.billResend = billResend;
exports.voidAddon = voidAddon;

// functions

function chargeWhatsappPenalty(obj, cache, callback){
    if(cache.serviceInstanceUnlimitedData && cache.serviceInstanceUnlimitedData.enabled){
        obj.isIgnored = true;
        obj.ignoreReason = "customer has DFF";
        callback(null, obj);
    }else{
        penaltyManager.chargeWhatsappPenalty(obj.number, obj.value, function(err, results){
            if(err){
                common.log("webHookInternal", "error charging whatsapp panelry " + JSON.stringify(err));
                obj.penalty = "error_charging_whatsapp_penalty";
            }else if(results.status == "panelty_addon_charged"){
                obj.penalty = results.status;
            }else if(results.status == "failed_to_acquire_penalty_lock"){
                obj.ignoreReason = "failed to acquire lock. Recent notification exists";
                obj.isIgnored = true;
                obj.penalty = results.status;
            }else{
                obj.ignoreReason = "penalty addon not charged";
                obj.isIgnored = true;
                obj.penalty = results.status;
            }
            callback(null, obj);
        });
    }
}

function ignoreWhatsappPenaltyNotificationIfDFF(obj, cache, callback){
    if(cache.serviceInstanceUnlimitedData && cache.serviceInstanceUnlimitedData.enabled){
        obj.isIgnored = true;
        obj.ignoreReason = "customer has DFF";
        callback(null, obj);
    }else{
        callback(null, obj);
    }
}

function handleFupNotification(obj, cache, callback){
    if(cache.serviceInstanceUnlimitedData && cache.serviceInstanceUnlimitedData.enabled){
        obj.isIgnored = true;
        obj.ignoreReason = "customer has DFF";
        callback(null, obj);
    }else{
        if(cache.general.filter(function(item){return item.name == constants.DATA_UNTHROTTLE_ADDON_NAME}).length > 0){
            obj.isIgnored = true;
            obj.ignoreReason = "customer has Unthrottle";
            callback(null, obj);
        }else{
            callback(null, obj);
        }
    }
}

function getCustomer(obj, callfront, callback, basicInfo) {
    var getCache = function (cb) {
        if (obj.serviceInstanceNumber) {
            ec.getCustomerDetailsServiceInstance(obj.serviceInstanceNumber, false, cb, basicInfo);
        } else {
            ec.getCustomerDetailsNumber(obj.prefix, obj.number, false, cb, basicInfo);
        }
    };

    getCache(function (err, cache) {
        if (err) {
            obj.cacheError = err.message;
        }
        if (!cache) {
            obj.cacheFound = false;
        }

        if (!cache) {
            if (!callfront) {    // we don't need cache
                callback(null, obj);
            } else {
                var error = "Customer not found (" + (obj.serviceInstanceNumber ? obj.serviceInstanceNumber : obj.number) + ")";
                if (LOG) common.error("webHookInternal", error);
                callback(new Error(error), obj);
            }
        } else {
            obj.billingAccountNumber = cache.billingAccountNumber;
            obj.customerAccountNumber = cache.customerAccountNumber;
            obj.serviceInstanceNumber = cache.serviceInstanceNumber;
            if (obj.teamID != undefined) {    // not from elitecore
                obj.account = cache.account;
                obj.billingAccount = cache.billingAccountNumber;
                if (obj.serviceSource == "notification") {
                    // force number and email from account if serviceinstance specified
                    obj.number = cache.number;
                    obj.numberSource = "account";
                    obj.email = cache.email;
                    obj.emailSource = "account";
                }
                if (!obj.cardlast4digits) {
                    obj.cardlast4digits = cache.billingCreditCard.last4Digits;
                    obj.cardlast4digitSource = "account";
                }
                if (!obj.email) {
                    obj.emailSource = "account";
                    obj.email = cache.email;
                }
                if (!obj.name) {
                    obj.nameSource = "account";
                    obj.name = cache.billingFullName;
                }
            }
            if (callfront) callfront(obj, cache, callback);
            else callback(null, obj);
        }
    }, basicInfo);
}

function appCreditCardChange(obj, cache, callback) {
    callback(null, obj);
}

function portInSuccess(obj, cache, callback) {
    var prefix = SG_PREFIX;
    var number = obj.number;
    var executionKey = config.ELITECORE_KEY;

    portInManager.onPortInNotification(prefix, number, "DONE", false, function (err, result) {
        obj.portInLaterResult = result;
        obj.portInLaterError = err ? err.message : undefined;

        bonusManager.addPortInBonus(prefix, number, executionKey, function (err, result) {
            obj.portInSubscriptionResult = result;
            obj.portInSubscriptionError = err ? err.message : undefined;

            callback(null, obj);
        });
    }, executionKey, undefined, obj.initiator);
}

function portInApproved(obj, cache, callback) {
    var prefix = SG_PREFIX;
    var number = obj.number;
    var executionKey = config.ELITECORE_KEY;

    portInManager.onPortInNotification(prefix, number, "DONOR_APPROVED", false, (err, result) => {
        obj.portInStatusUpdateResult = result;
        obj.portInStatusUpdateError = err ? err.message : undefined;

        callback(null, obj);
    }, executionKey, undefined, obj.initiator);
}

function portInFailure(obj, cache, callback) {
    var prefix = SG_PREFIX;
    var number = obj.number;
    var executionKey = config.ELITECORE_KEY;

    portInManager.onPortInNotification(prefix, number, "FAILED", false, (err, result) => {
        obj.portInStatusUpdateResult = result;
        obj.portInStatusUpdateError = err ? err.message : undefined;

        callback(null, obj);
    }, executionKey, undefined, obj.initiator);
}

function emailChanged(obj, cache, callback) {
    paas.getWebPortalEmail(obj.account, function (err, email) {
        if (err) {
            var error = "can not find web portal email, account=" + obj.account;
            if (LOG) common.error("webHookInternal", error);
            callback(new Error(error), obj);
        } else {
            obj.webportalemail = email;
            callback(null, obj);
        }
    });
}

function usageConsumptionData(obj, cache, callback) {
    obj.type = obj.type ? obj.type.toLowerCase() : "";
    var getUsage = function(cb) {
        db.cache_lock("cache", "autoboost_" + cache.serviceInstanceNumber, 1, 15000, (lock) => {
            if (lock == 1) {
                usageManager.computeUsage(SG_PREFIX, obj.number, cb);
            } else {
                return callback(new Error("autoboost locked"), obj);
            }
        });
    }
    getUsage((err, usageResult) => {
        if (err || !usageResult || !usageResult.data || !usageResult.data.basic) {
            return callback(new Error("Data is not loaded, can not auto boost"), obj);
        }

        var addonDataLeftKb;
        if (obj.type === "autoboostdata") {
            obj.activity = "usage_autoboost_consumption_data";
            // default autoboost is 100MB
            addonDataLeftKb = 100 * 1024;
        } else if(obj.type === "plus_20_20"){
            obj.activity = "circles_switch_usage_plus_consumption_data";
            addonDataLeftKb = 1024 * 1024 * 20;
        }else if (obj.type === "data") {
            // percentage is only relevant to base data (can be prorated)
            // so to calculate left data we use base (do include extra data)
            if (usageResult && usageResult.data && usageResult.data.basic && usageResult.data.basic.prorated_kb) {
                addonDataLeftKb = usageResult.data.basic.prorated_kb;
            } else if (usageResult && usageResult.data && usageResult.data.basic && usageResult.data.basic.plan_kb) {
                addonDataLeftKb = usageResult.data.basic.plan_kb;
            } else {
                // default base plan is 3GB
                addonDataLeftKb = 3 * 1024 * 1024;
            }
        } else {
            addonDataLeftKb = 0;
        }

        var dataLeft = Math.round(addonDataLeftKb * (1 - (obj.percentage / 100)));
        var prettifiedData = common.prettifyKBUnit(dataLeft);
        obj.value = Math.round(prettifiedData.value / 10) * 10;
        obj.unit = prettifiedData.unit;

        var lowData = (dataLeft / 1024) < 100;
        var baseData = obj.type === "data";
        var freeFlow = cache.serviceInstanceUnlimitedData && cache.serviceInstanceUnlimitedData.enabled;
        let circlesSwitchUser = cache.serviceInstanceBasePlanName === 'CirclesSwitch';

        // ignore all notifications apart of base data
        // ignore if data < 100 MB, autoboost logic method take care
        // ingore is data free flow is ON
        // ignore if circles switch user
        if (ec.isLockdown(cache.billing_cycle).enabled === 1 || !baseData || lowData || freeFlow) {
            obj.isIgnored = true;
        }

        if(obj.activity !== 'circles_switch_usage_plus_consumption_data' && circlesSwitchUser && lowData){
            obj.isIgnored = true;
        }

        if(obj.activity === 'circles_switch_usage_plus_consumption_data'){
            if(!circlesSwitchUser){
                obj.isIgnored = true;
                obj.ignoreReason = 'Notification ignored as customer not in Circles Switch plan';
            }else{
                delete obj.isIgnored;
                delete obj.ignoreReason
            }
        }

        boostManager.addAutoBoost(SG_PREFIX, obj.number, true, obj.teamKey, function(err, result) {
            if (result) {
                result.lowData = lowData;
                result.baseData = baseData;
                result.freeFlow = freeFlow;
            }

            obj.autoboostResult = {error: err ? err.message : undefined, result: result};
            callback(undefined, obj);
        });
    });
}

function pay200(obj, cache, callback) {
    paymentManager.queueCreditCapPaymentByBA(cache.billingAccountNumber, {platform: "BSS"}, (err, result) => {
        obj.creaditCapPaymentResult = {error: err ? err.message : undefined, result: result};
        callback(null, obj);
    });
}

function pay200Warning(obj, cache, callback) {
    paymentManager.makeCreditCapPaymentByBA(cache.billingAccountNumber, {warningOnly: true, platform: "BSS"}, (err, result) => {
        obj.creaditCapPaymentWarningResult = {error: err ? err.message : undefined, result: result};
        callback(null, obj);
    });
}

function checkRoamingCap(obj, cache, callback) {
    if (typeof(profileManager.loadASelfcareSetting) != 'function') {    // remove this if issue is fixed
        common.error("notificationActions checkRoaming", "profileManager.loadASelfcareSetting is not a function");
        profileManager = require('../../../core/manager/profile/profileManager');
    }
    profileManager.loadASelfcareSetting(cache.prefix, cache.number, "roaming_cap_notification_amount", obj.teamKey,
        function (err, type, value) {
            if (err) {
                if (LOG) common.error("webHook", "can not load profile err=" + err.message);
                return;
            }

            obj.priceLimit = cache.roaming ? cache.roaming.limit : 100;
            obj.price = parseInt(obj.amount);

            if (LOG) common.log("webHook", "checkRoamingCap: number=" + cache.number +
            ", price=" + obj.price + ", priceLimit=" + obj.priceLimit + ", config=" + value);

            // users can configure notifications for roaming charges < 100$
            // for customers who increased roaming can there would be sent 2 extra notifications
            // 50$ and 3$ before reaching the limit

            if (obj.amount >= 96 && obj.amount <= 100) {
                if (obj.priceLimit > 100) {
                    // do nothing show original notification
                } else {
                    obj.activity = 'cap_roaming_data_last';
                }
            } else {
                if ((obj.price < value - 3 || obj.price > value + 3) && obj.price < 100) {
                    obj.isIgnored = true;
                } else if (obj.price > 100) {
                    obj.isIgnored = true;
                }
            }

            callback(null, obj);
        });
}

function billResend(obj, cache, callback) {
    obj.billingAccount = cache.billingAccountNumber;
    obj.filePdf = obj.billingAccount + "_" + obj.invoicenumber + ".pdf";

    var handleResponse = (err, fileInfo) => {
        if (err) {
            err.status = "ERROR_FILE_NOT_FOUND";
            callback(err, obj);
            return;
        }
        if (!fileInfo) {
            var err = new Error("File is not found or invalid");
            err.status = "ERROR_FILE_NOT_FOUND";
            callback(err, obj);
            return;
        }
        obj.fileInfo = fileInfo;
        obj.month = fileInfo.month;
        obj.attachment = {
            filename: fileInfo.pdf,
            path: fileInfo.path
        };
        callback(null, obj);
    }

    var loadInvoices = (index) => {
        if (index == 0) {
            return loadInvoices(1);
        }
        if (index > 99) {
            return callback(undefined, obj);
        }

        var folderIndex = index < 10 ? ("0" + index) : ("" + index);
        var folderPath = 'live/all/' + obj.archive + '/' + folderIndex;
        common.log("NotificationActions", "billResend: folderPath=" + folderPath)

        billHelper.loadLastFileAfterDate(folderPath, obj.filePdf, function (err, fileInfo) {
            if (err) {
                if (err.status == "ERROR_FOLDER_NOT_FOUND") {
                    if (index == 1) {
                        var folderPath = 'live/all/' + obj.archive;
                        common.log("NotificationActions", "billResend: folderPath=" + folderPath)

                        return billHelper.loadLastFileAfterDate(folderPath, obj.filePdf, function (err, fileInfo) {
                            common.log("NotificationActions", "billResend: fileInfo=" + JSON.stringify(fileInfo))
                            return handleResponse(err, fileInfo);
                        });
                    }
                } else {
                    return loadInvoices(index + 1);
                }
            }

            if (err) {
                common.error("NotificationActions", "billResend: error=" + err.message);
            } else {
                common.log("NotificationActions", "billResend: fileInfo=" + JSON.stringify(fileInfo));
            }
            return handleResponse(err, fileInfo);
        }, true);
    }

    loadInvoices(1);
}

function paymentSuccessful(obj, cache, callback) {
    callback(null, obj);
}

function selfcareInstall(obj, cache, cb) {
    var tasks = [];
    tasks.push(function (callback) {
        bonusManager.addSelfcareInstallationBonus(cache.prefix, cache.number, obj.newUser, false, obj.teamKey,
            function (err, result) {
                setTimeout(function () {
                    callback(undefined, {err: err ? err.message : null, id: "addSelfcareInstallationBonus", result: result});
                }, 500);
            });
    });
    tasks.push(function (callback) {
        promotionsManager.applyPendingPromo(cache.prefix, cache.number, cache.orderReferenceNumber, "DEFAULT",
        obj.teamKey, function (err, result) {
                callback(undefined, {err: err ? err.message : null, id: "pendingPromoBonuses", result: result});
            });
    });
    tasks.push(function (callback) {
        if (!cache.referralCode || cache.referralCode.length == 0) {
            return callback(undefined, {err: null, id: "referralBonus", result: {emptyReferralCode: true}});
        }

        promotionsManager.loadPromoCode(cache.referralCode, (err, result) => {
            if (err && err.status == "CODE_NOT_FOUND") {
                var notificationKey = config.NOTIFICATION_KEY;
                if (LOG) common.log("WebHookActions", "NOT found internal code, " +
                "code=" + cache.referralCode + ", trying to apply personal");

                referralManager.addReferralBonus(cache.prefix, cache.number, cache.referralCode, notificationKey, (err, result) => {
                        callback(undefined, {
                            id: "referralBonus",
                            error: err ? err.message : undefined,
                            result: result
                        });
                    });
            } else {
                if (LOG) common.log("WebHookActions", "Found internal code, " +
                "code=" + cache.referralCode);

                callback(undefined, {
                    id: "referralBonus",
                    result: {
                        internalReferralFound: true,
                        result: result
                    }
                });
            }
        });
    });
    async.parallelLimit(tasks, 1, function (err, asyncResult) {
        obj.subsResult = asyncResult;
        if (err) {
            cb(err, obj);    // unused for now, for future
        } else {
            cb(undefined, obj);
        }
    });
}

function validateBasePlan(obj, cache, callback) {
    obj.prefix = SG_PREFIX;

    var executionKey = config.ELITECORE_KEY;

    var handleResult = ()=> {
        callback(null, obj);
    }

    var initProfile = ()=> {
        var next = handleResult;

        if (typeof(profileManager.loadProfileDetails) != 'function') {    // remove this if issue is fixed
            common.error("notificationActions initProfile", "profileManager.loadProfileDetails is not a function");
            profileManager = require('../../../core/manager/profile/profileManager');
        }
        // load profile which would generate default nickname and picture
        profileManager.loadProfileDetails(obj.prefix, obj.number, executionKey, function (err) {
            next();
        });
    }

    var addReferralBonus = () => {
        var next = initProfile;

        if (!cache.referralCode || cache.referralCode.length == 0) {
            return next();
        }

        // after customer installs the app we try to add referral bonus again,
        // we have to make sure that codes that are added internally stay in db
        // forever or else this check may not work and in case of overlaps
        // of referral and internal codes customers may start receiving referral bonuses

        obj.referralCode = cache.referralCode.toUpperCase();
        promotionsManager.loadPromoCode(cache.referralCode, (err, result) => {
            if (err && err.status == "CODE_NOT_FOUND") {
                if (LOG) common.log("WebHookActions", "NOT found internal code, " +
                "code=" + cache.referralCode + ", trying to apply personal");

                referralManager.addReferralBonus(obj.prefix, obj.number, obj.referralCode, executionKey, function (err, result) {
                    obj.referralSubscriptionResult = {error: err ? err.message : undefined, result: result};
                    handleResult();
                });
            } else {
                if (LOG) common.log("WebHookActions", "Found internal code, " +
                "code=" + cache.referralCode);

                obj.referralSubscriptionResult = {
                    error: err ? err.message : undefined,
                    result: {internalReferralFound: true, result: result}
                };
                handleResult();
            }
        });
    }

    var updateCustomerNotifications = () => {
        var next = addReferralBonus;

        if (!cache.orderReferenceNumber) {
            return next();
        }

        var update = {
            billingAccountNumber: cache.billingAccountNumber,
            customerAccountNumber: cache.customerAccountNumber,
            serviceInstanceNumber: cache.serviceInstanceNumber
        }

        db.notifications_webhook.update({
            "ts":{"$gt":new Date().getTime() - (60*24*60*60*1000)},
            "serviceInstanceNumber":null,
            "$or":[{"notification.order_reference_number": cache.orderReferenceNumber},
                {"notification.orderno": cache.orderReferenceNumber}]
        }, {$set: update}, {multi: true}, function (err, result) {
            if (err) {
                common.error("NotificationActionManger",
                    "validateBasePlan: failed to update old webhook notifications, error=" + err.message);
            } else if (result && result.result) {
                if (LOG) common.log("NotificationActionManger",
                    "validateBasePlan: updated old webhook notifications, count=" + result.result.n);
            }

            db.notifications_logbook.update({
                "$or":[{order_reference_number: cache.orderReferenceNumber},
                    {orderno: cache.orderReferenceNumber}]
            }, {$set: update}, {multi: true}, function (err, result) {
                if (err) {
                    common.error("NotificationActionManger",
                        "validateBasePlan: failed to update old logbook notifications, error=" + err.message);
                } else {
                    if (LOG) common.log("NotificationActionManger",
                        "validateBasePlan: updated old logbook notifications, count=" + result.result.n);
                }

                next();
            });
        });
    }
    db.notifications_webhook.find({
            "serviceInstanceNumber" : cache.serviceInstanceNumber,
            "activity" : "validate_base_plan" }).toArray(function(findErr, findRes) {
        if ( findRes && (findRes.length > 0) ) {
            obj.isIgnored = true;
            callback(null, obj);
        } else {
            updateCustomerNotifications();
        }
    });
}

function voidAddon(obj, product, callback) {
    // need fresh cache
    ec.getCustomerDetailsNumber(SG_PREFIX, obj.number, true, function (err, cache) {
        if (!cache) {
            obj.isVoidError = true;
            var error = "Can not find user " + obj.number;
            if (LOG) common.error("validateNotification", error);
            return callback(new Error(error), obj, product);
        }

        var unSubscribe;
        cache.general.forEach(function (item) {
            if (item.id == product.id) {
                unSubscribe = {
                    "product_id": product.id,
                    "packageHistoryId": item.packageHistoryId,
                    "effect": item.unsub_effect
                }
            }
        });

        if (!unSubscribe) {
            obj.isVoidError = true;
            var error = "User " + obj.number + " does not have product with id " + product.id;
            if (LOG) common.error("validateNotification", error);
            return callback(new Error(error), obj, product);
        }

        if (LOG) common.log("validateNotification", "UnSubscribe addon, info=" + JSON.stringify(unSubscribe));
        // assume always immediate or else has to pass current:true or false
        ec.addonUnsubscribe(cache.number, cache.serviceInstanceNumber, [unSubscribe], cache.billing_cycle, function (err, result) {
            if (err) {
                obj.isVoidError = true;
                common.error("validateNotification", "Failed to unSubscribe " + JSON.stringify(result));
                return callback(new Error("UnSubscription of product with id " +
                product.id + " failed for " + obj.number), obj, product);
            }

            ec.setCache(cache.number, cache.prefix, undefined, cache.account, function (err, result) {
                if (LOG) common.log("validate_base_plan", "UnSubscription of product with id " +
                product.id + " succeed for " + obj.number);
                return callback(undefined, obj, product);
            });
        });
    });
}

function addOnUnsubscription(obj, cache, callback) {
    var product = ec.findPackageName(ec.packages(), obj.addonname);
    if (!product) {
        var error = "Product not found '" + obj.addonname + "'";
        if (LOG) common.error("webHookInternal", error);
        obj.isProductInvalid = true;
        callback(new Error(error), obj);
        return;
    }

    if (LOG) common.log("webHook", "delay notification - add_on_subscription, number=" +
    obj.number + ", account=" + cache.account);
    obj.nextbillcycle = ec.nextBill(cache.billing_cycle);
    obj.nextcycledate = dateformat(Date.parse(obj.nextbillcycle.end), "dS mmmm");

    if (product && product.type == "base") {
        setTimeout(function () {
            ec.setCache(obj.number, SG_PREFIX, null, cache.account, function (err, cache) {
                if (!cache) {
                    return callback(new Error("Customer info was not updated"), obj, product);
                }

                var futureComponent;
                if (product.component === "data") {
                    futureComponent = cache.extra.data;
                } else if (product.component === "voice") {
                    futureComponent = cache.extra.voice;
                } else if (product.component === "sms") {
                    futureComponent = cache.extra.sms;
                } else {
                    return callback(new Error("Component type is not supported"), obj, product);
                }

                obj.futureComponentName = (futureComponent ? futureComponent.name : "NONE");
                obj.removedComponentName = product.name;

                if (LOG) common.log("webHook", "delay notification - add_on_subscription, number=" + obj.number +
                ", account=" + cache.account + ", futureComponent=" + obj.futureComponentName +
                ", removedComponent=" + obj.removedComponentName);

                if (!futureComponent) {
                    if (product.component == "data") {
                        obj.activity = "selfcare_data_changed";
                    } else if (product.component == "voice") {
                        obj.activity = "selfcare_calls_changed";
                    } else if (product.component == "sms") {
                        obj.activity = "selfcare_sms_changed";
                    }
                    product.removed = true;
                    return callback(null, obj, product);
                } else {
                    obj.isIgnored = true;
                    return callback(null, obj, product);
                }
            });
        }, 7000);
    } else if (product && product.type == "general") {
        packagesHelper.loadPackageInfo(product.id, function (aviability, info) {
            if (!aviability || !info) {
                return callback(new Error("Product related config is not found for " + product.id + ""), obj, product);
            }

            // process addons displayed on mobile only
            var availableOnMobile = packagesHelper.isShownOnMobile(aviability);
            if (!availableOnMobile) {
                obj.isIgnored = true;
                return callback(null, obj, product);
            }

            // 0 - immediate, 1 - tomorrow, 2 - next billing cycle
            var unSubEffect = info.unsub_effect;
            if (unSubEffect == 0) {
                obj.activity = "plus_removed_immediate";
            } else if (unSubEffect == 1) {
                obj.activity = "plus_removed_tomorrow";
            } else if (unSubEffect == 2) {
                obj.activity = "plus_removed_next_cycle";
            } else {
                return callback(new Error("Sub. effect type " + unSubEffect + " is not supported"), obj, product);
            }

            if (info.title) {
                product.originalAddonname = product.addonname;
                obj.addonname = info.title;
            }

            callback(null, obj, product);
        });
    } else {
        obj.isIgnored = true;
        return callback(null, obj, product);
    }
}

function autoboostAdded(obj, cache, callback) {
    var product = ec.findPackageName(ec.packages(), obj.addonname);
    if (!product) {
        var error = "Product not found '" + obj.addonname + "'";
        if (LOG) common.error("webHookInternal", error);
        obj.isProductInvalid = true;
        callback(new Error(error), obj);
        return;
    }

    db.notifications_logbookwebhook_count_get({
        "number": obj.number,
        "activity": "autoboost_added",
        "ts": {"$gt": obj.nextbillcycle.startDateUTC.getTime()}
    }, function (count) {
        obj.boostcount = count + 1;
        callback(null, obj, product);

        if (obj.boostcount == 10 || obj.boostcount == 30 || obj.boostcount == 50) {
            var childObj = JSON.parse(JSON.stringify(obj));
            childObj.parentActivity = childObj.activity;
            childObj.activity = "autoboost_" + obj.boostcount + "_added";
            callback(null, childObj, product);
        }
    });
}

function addOnSubscription(obj, cache, callback) {
    if (obj.addonname && obj.number) {
        if (LOG) common.log("webHook", "cache last subscription for 3sec, number=" + obj.number +
        ", addonname=" + obj.addonname);
        db.cache_put("last_subscription_cache", obj.number, obj.addonname, 3 * 1000);
    }

    var product = ec.findPackageName(ec.packages(), obj.addonname);
    if (!product) {
        var error = "Product not found '" + obj.addonname + "'";
        if (LOG) common.error("webHookInternal", error);
        obj.isProductInvalid = true;
        callback(new Error(error), obj);
        return;
    }

    obj.nextbillcycle = ec.nextBill(cache.billing_cycle);
    obj.nextcycledate = dateformat(Date.parse(obj.nextbillcycle.end), "dS mmmm");

    if (product && product.type === "boost") {
        if (product.app === "auto") {
            obj.activity = "autoboost_added";
            db.notifications_logbookwebhook_count_get({
                "number": obj.number,
                "activity": obj.activity,
                "ts": {"$gt": obj.nextbillcycle.startDateUTC.getTime()}
            }, function (count) {
                obj.boostcount = count + 1;
                callback(null, obj, product);

                if (obj.boostcount == 10 || obj.boostcount == 30 || obj.boostcount == 50) {
                    var activity = "autoboost_" + obj.boostcount + "_added";
                    var variables;
                    notificationSend.internal(obj.prefix, obj.number, obj.email, obj.name, activity, variables, obj.teamKey);
                }
            });
        } else if (product.app === "free") {
            obj.activity = "freeboost_added";
            callback(null, obj, product);
        } else if (product.app === "dbs") {
            obj.activity = "boost_added_dbs";
            callback(null, obj, product);
        } else {
            obj.activity = "boost_added";
            callback(null, obj, product);
        }
    } else if (product && product.type === "bonus") {
        var info = bonusTypesService.getBonusInfo(product.app);
        if (!info || info.ignoreBssNotification) {
            obj.isIgnored = true;
        } else if (info) {
            obj.activity = info.activity;

            if (product.app === "loyalty") {
                var activation = new Date(cache.activationDate);
                var now = new Date();
                obj.milestone_month = now.getMonth() - activation.getMonth()
                + (12 * (now.getFullYear() - activation.getFullYear()));
            }
        }

        if (obj.addonname.indexOf(' R ') > -1) {
            obj.recurrentsuffix = ' /mo';
        } else {
            obj.recurrentsuffix = '';
        }

        callback(null, obj, product);
    } else if (product && product.type == "base") {
        if (product.component == "data") {
            obj.activity = "selfcare_data_changed";
        } else if (product.component == "voice") {
            obj.activity = "selfcare_calls_changed";
        } else if (product.component == "sms") {
            obj.activity = "selfcare_sms_changed";
        }
        callback(null, obj, product);
    } else if (product && product.type == "general") {
        packagesHelper.loadPackageInfo(product.id, function (aviability, info) {
            if (!aviability || !info) {
                return callback(new Error("Product related config is not found for " + product.id + ""), obj, product);
            }

            var availableOnMobile = packagesHelper.isShownOnMobile(aviability);
            if (!availableOnMobile) {
                obj.isIgnored = true;
                return callback(null, obj, product);
            }

            var subEffect = info.sub_effect;
            if (subEffect == 0) {
                obj.activity = "plus_added_immediate";
            } else if (subEffect == 1) {
                obj.activity = "plus_added_tomorrow";
            } else if (subEffect == 2) {
                obj.activity = "plus_added_next_cycle";
            } else {
                return callback(new Error("Sub. effect type " + subEffect + " is not supported"), obj, product);
            }
            if (info.title) {
                product.originalAddonname = product.addonname;
                obj.addonname = info.title;
            }
            if(obj.addonname === 'Caller Number Display' && cache.serviceInstanceBasePlanName === 'CirclesSwitch'){
                obj.isIgnored = true;
                obj.ignoreReason = 'Circles switch customer';
            }
            callback(null, obj, product);
        });
    } else {
        obj.isIgnored = true;
        return callback(null, obj, product);
    }
}

function deliveryFailed(obj, cache, callback) {
    if ( obj && (obj.port_request == "Yes") ) {
        var prefix = (obj.prefix) ? obj.prefix : SG_PREFIX;
        obj.number = obj.temporary_number;

        //portInManager.cancelRequest(prefix, obj.temporary_number, prefix, obj.actual_number, function (err, result) {
        //    obj.cancelRequestResult = {error: err ? err.message : undefined, result: result};
        //    if (err) {
        //        common.error("deliveryFailed", JSON.stringify(err));
        //    }
            callback(null, obj, undefined);
        //}, undefined, undefined, "[CMS][delivery_failed]");
    } else {
        obj.portcancel = "Not handled";
        callback(null, obj, undefined);
    }
}

function portinStatus(obj, cache, callback) {
    var params = { "qs" : JSON.parse(JSON.stringify(config.PAAS_CREDS)) };
    if (cache && cache.serviceInstanceNumber) params.qs.serviceInstanceNumber = cache.serviceInstanceNumber;
    params.qs.number = obj.number;
    params.qs.port_in_status = obj.status;
    params.qs.port_in_fail_reason = obj.reason;
    var path = "/api/kirk/v1/update_port_in_status";
    paas.connect("put", path, params, function (err, result) {
        if (result) obj.paas_result = result;
        callback(err, obj, undefined);
    });
}
