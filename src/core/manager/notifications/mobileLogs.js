var db = require(__lib + "/db_handler");

module.exports = {
    
    getUserNotifications: function(serviceInstanceNumber, anchor, callback) {
        var query = new Object;
        query.serviceInstanceNumber = serviceInstanceNumber;
        query.transport = "selfcare";
        query.app = {"$exists": true};
        query["$and"] = [{"statusSelfcare": {"$ne": "RESTRICT"}}, {"statusSelfcare": {"$ne": "IGN_MULTI"}}];
        if (anchor) query.ts = {"$gt": parseInt(anchor)};
        var projection = new Object;
        projection.ts = 1;
        projection.read = 1;
        projection.app = 1;
        projection.content = 1;
        db.notifications_logbook_get(query, projection, {"_id": -1}, function (group) {
            var list = new Array;
            if (group) group.forEach(function (item) {
                var notif = new Object;
                if (item.app) {
                    notif.id = item._id.toString();
                    notif.date = new Date(item.ts).toISOString();
                    notif.anchor = item.ts.toString();
                    notif.read = item.read;
                    notif.mime_type = item.app.mime_type;
                    notif.title = item.app.title;
                    notif.subtitle = item.app.subtitle;
                    notif.text = item.app.text;
                    notif.link = item.app.link;
                    notif.image_preview = item.app.image_preview;
                    notif.image = item.app.image;
                    notif.content = item.content;
                    list.push(notif);
                }
            });
            callback(undefined, list);
        });
    },

    setReadUserNotifications: function(list, callback) {
        if (!list) return callback();
        else if (typeof(list) == "string") list = list.split(",");
        db.notifications_logbook_set_read(list, function (group) {
            callback();
        });
    }

}
