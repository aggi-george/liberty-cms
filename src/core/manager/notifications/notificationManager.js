var crypto = require("crypto");
var config = require('../../../../config');
var common = require('../../../../lib/common');
var db = require('../../../../lib/db_handler');
var ec = require('../../../../lib/elitecore');
var BaseError = require('../../errors/baseError');
var actions = require('./actions');

var LOG = config.LOGSENABLED;

var SG_PREFIX = "65";
var CACHE_PREFIX_PLAN_VALIDATED = "plan_validated_";
var CACHE_NOTIFICATION_DISABLED = "addon_notification_disabled_";

var PAY200_ADDON_NAME = "Cap credit 200";
var IGNORED_ADD_ON_SUBSCRIPTION_PRODUCTS = [
    PAY200_ADDON_NAME];
var IGNORED_AFTER_VALIDATION_NOTIFICATIONS = [
    "activate_app",
    "selfcare_calls_changed",
    "selfcare_sms_changed",
    "selfcare_data_changed",
    "plus_added_immediate",
    "plus_added_tomorrow",
    "plus_added_next_cycle",
    "plus_removed_immediate",
    "plus_removed_tomorrow",
    "plus_removed_next_cycle"];

module.exports = {

    suppressNotificationsTimeout: function() { return 120 * 1000 },

    suppressNotificationsList: function() { return IGNORED_AFTER_VALIDATION_NOTIFICATIONS },

    suppressNotifications: function(number, serviceInstanceNumber, reason) {
        if (serviceInstanceNumber) db.cache_put(
            "account_cache",
            CACHE_PREFIX_PLAN_VALIDATED + serviceInstanceNumber,
            { "prefix" : SG_PREFIX, "serviceInstanceNumber" : serviceInstanceNumber, "reason" : reason },
            module.exports.suppressNotificationsTimeout()
        );
        if (number) db.cache_put(
            "account_cache",
            CACHE_PREFIX_PLAN_VALIDATED + number,
            { "prefix" : SG_PREFIX, "serviceInstanceNumber" : serviceInstanceNumber, "reason" : reason },
            module.exports.suppressNotificationsTimeout()
        );
    },

    suppressAddonNotifications: function (serviceInstanceNumber, addonname, reason, callback, overrideTimeout) {
        if (!serviceInstanceNumber || !addonname) {
            return callback(new Error("Invalid params"));
        }

        var key = CACHE_NOTIFICATION_DISABLED + "_" + serviceInstanceNumber + "_" + addonname;
        var timeout = overrideTimeout ? overrideTimeout : module.exports.suppressNotificationsTimeout();
        db.cache_put("account_cache", key, {
            serviceInstanceNumber: serviceInstanceNumber,
            reason: reason
        }, timeout);

        return callback();
    },

    isAddonNotificationsSuppressed: function (serviceInstanceNumber, addonname, callback) {
        if (!serviceInstanceNumber || !addonname) {
            return callback(new Error("Invalid params"));
        }

        var key = CACHE_NOTIFICATION_DISABLED + "_" + serviceInstanceNumber + "_" + addonname;
        db.cache_get("account_cache", key, function (cache) {
            return callback(undefined, cache);
        });
    },

    enableNotifications: function(number, serviceInstanceNumber) {
        if (serviceInstanceNumber) db.cache_del("account_cache", CACHE_PREFIX_PLAN_VALIDATED + serviceInstanceNumber);
        if (number) db.cache_del("account_cache", CACHE_PREFIX_PLAN_VALIDATED + number);
    },

    suppressNotificationsStatus: function(number, serviceInstanceNumber, callback) {
        var id = serviceInstanceNumber ? serviceInstanceNumber : number;
        db.cache_get("account_cache", CACHE_PREFIX_PLAN_VALIDATED + id, function (cache) {
            callback(cache);
        });
    },

    enqueue: function(obj, key, callback) {
        if (!obj.prefix) obj.prefix = SG_PREFIX;
        if (!obj.activity) {
            return callback(new BaseError("Notification error (Activity is missing)", 400));
        }
        if (!(obj.email || obj.number || obj.serviceInstanceNumber)) {
            return callback(new BaseError("Notification error (Recipient is missing)", 400));
        }
        var query = "SELECT id, name FROM teams WHERE apiKey=" + db.escape(key);
        if (LOG) common.log("notificationManager send", "query team, query=" + query);
        db.query_err(query, function (err, rows) {
            if (err || !rows || rows.length == 0) {
                if (LOG) common.error("notificationManager send", "authorization failed, key " + key + " is invalid");
                return callback(new BaseError("Notification error (Unauthorized)", 403));
            }
            obj.notificationId = crypto.randomBytes(16).toString("hex");
            if (!obj.ip) obj.ip = config.MYSELF;
            obj.server_dev = config.DEV;
            obj.teamID = rows[0].id;
            obj.teamName = rows[0].name;
            if (obj.number) obj.numberSource = "notification";
            if (obj.serviceInstanceNumber) obj.serviceSource = "notification";
            if (obj.email) obj.emailSource = "notification";
            if (obj.name) obj.nameSource = "notification";
            if (obj.cardlast4digits) obj.cardlast4digitSource = "notification";
            if (!obj.name) {
                if (obj.customer_name) {
                    obj.name = obj.customer_name;
                } else if (obj.firstname) {
                    obj.name = obj.firstname;
                }
            }
            var retries = 1;
            var boff = { "type" : "fixed", "delay" : 20 * 1000 };
            if (obj.activity == "usage_consumption_data") {
               retries = 6;
            }
            var job = db.queue.create('ingress', templateObj)
                .events(false)
                .removeOnComplete(true)
                .attempts(retries)
                .backoff(boff)
                .ttl(60 * 60 * 1000)
                .save(function (err) {
            });
            if (callback) callback(undefined, obj);
        });
    },

    saveLogs: function(err, obj, product, queryTime, callback) {
        var log = {
            ip: obj.ip,
            status: "UNKNOWN",
            prefix: obj.prefix,
            serviceInstanceNumber: obj.serviceInstanceNumber,
            customerAccountNumber: obj.customerAccountNumber,
            billingAccountNumber: obj.billingAccountNumber,
            number: obj.number,
            email: obj.email,
            notificationId: obj.notificationId,
            activity: obj.activity,
            processingTime: queryTime,
            status: obj.status,
            notification: obj
        };

        if (err) {
            if (err.message) {
                log.errorMessage = err.message;
            }
            if (obj.isActivityInvalid) {
                log.status = "ERROR_INVALID_ACTIVITY";
            } else if (obj.isProductInvalid) {
                log.status = "ERROR_PRODUCT_NOT_FOUND";
            } else if (obj.isVoidError) {
                log.status = "ERROR_CAN_NOT_VOID";
            } else {
                log.status = "ERROR";
            }
            callback(err, obj);
        } else if (obj.isVoid) {
            log.status = "VOIDED";
            callback(null, obj);
        } else if (obj.isIgnored) {
            log.status = "IGNORED";
            callback(null, obj);
        } else if (obj.isNotFound) {
            log.status = "NOT_FOUND";
            callback(null, obj);
        } else {
            log.status = "OK";
        }
        var getCache = function (prefix, number, serviceInstanceNumber, callback) {
            if (serviceInstanceNumber) {
                ec.getCustomerDetailsServiceInstance(log.serviceInstanceNumber, false, callback, true);
            } else {
                ec.getCustomerDetailsNumber(log.prefix, log.number, false, callback, true);
            }
        };

        if (log.serviceInstanceNumber && log.billingAccountNumber && log.customerAccountNumber) {
            db.notifications_insert("webhook", log);
            if (callback) callback(null, obj, product);
        } else {
            getCache(log.prefix, log.number, log.serviceInstanceNumber, function(err, cache) {
                if (cache) {
                    log.billingAccountNumber = cache.billingAccountNumber;
                    log.customerAccountNumber = cache.customerAccountNumber;
                    log.serviceInstanceNumber = cache.serviceInstanceNumber;
                }
                db.notifications_insert("webhook", log);
                if (callback) callback(null, obj, product);
            });
        }
    },

    triage: function(obj, callback) {
        switch (obj.activity) {
            case "add_on_subscription":
                if (IGNORED_ADD_ON_SUBSCRIPTION_PRODUCTS.indexOf(obj.addonname) >= 0) {
                    obj.isIgnored = true;
                    return callback(null, obj);
                }
                getCustomer(obj, actions.addOnSubscription, function(err, obj, product) {
                    // ignore all plus_added_notifications after base pla
                    db.cache_get("account_cache", CACHE_PREFIX_PLAN_VALIDATED + obj.serviceInstanceNumber, function (cache) {
                        if (cache) {
                            obj.isIgnored = true;
                            if (product.id === "PRD00315" || product.id === "PRD00647") {
                                obj.isVoid = true;
                            }
                        }
                        if (obj.isVoid) {
                            // return callback if not in ignore list -- is this really necessary?
                            if (IGNORED_AFTER_VALIDATION_NOTIFICATIONS.indexOf(obj.activity) < 0) return callback(undefined, obj, product);
                            actions.voidAddon(obj, product, callback);
                        } else {
                            callback(null, obj, product);
                        }
                    });
                });
                break;
            case "add_on_unsubscription":
                getCustomer(obj, actions.addOnUnsubscription, callback);
                break;
            case "pay200_warning":
                getCustomer(obj, actions.pay200Warning, callback);
                break;
            case "pay200":
                getCustomer(obj, actions.pay200, callback);
                break;
            case "cap_roaming_data":
                getCustomer(obj, actions.checkRoamingCap, callback);
                break;
            case "usage_consumption_data":
                getCustomer(obj, actions.usageConsumptionData, callback);
                break;
            case "port_in_success": // success notifications from EC
                getCustomer(obj, actions.portInSuccess, function (err, obj) {
                    // inform ecomm regardless of error
                    obj.status = "success";
                    obj.reason = "success";
                    getCustomer(obj, actions.portinStatus, callback);
                });
                break;
            case "port_in_failure": // failure notifications from EC
                getCustomer(obj, actions.portInFailure, function (err, obj) {
                    // inform ecomm regardless of error
                    obj.status = "fail";
                    obj.reason = obj.reason ? obj.reason : "ERROR_UNKNOWN";
                    getCustomer(obj, actions.portinStatus, callback);
                });
                break;
            case "port_in_approved": // approved notifications from EC
                getCustomer(obj, actions.portInApproved, function (err, obj) {
                    // inform ecomm regardless of error
                    obj.status = "approved";
                    obj.reason = "approved";
                    getCustomer(obj, actions.portinStatus, callback);
                });
                break;
            case "bill_resend":
                getCustomer(obj, actions.billResend, callback);
                break;
            case "change_base_plan":
                if (obj.oldpackage && obj.oldpackage === "CirclesRegistration"
                    && obj.newpackage && obj.newpackage === "CirclesOne") {
                    obj.activity = "validate_base_plan";
                    module.exports.suppressNotifications(obj.number, obj.serviceInstanceNumber, "validate_base_plan notification");
                    getCustomer(obj, actions.validateBasePlan, callback);
                } else callback(null, obj);
                break;
            case "validate_base_plan":
                module.exports.suppressNotifications(obj.number, obj.serviceInstanceNumber, "validate_base_plan notification");
                getCustomer(obj, actions.validateBasePlan, callback);
                break;
            case "app_credit_card_change":
                getCustomer(obj, actions.appCreditCardChange, callback);
                break;
            case "selfcare_install":
                getCustomer(obj, actions.selfcareInstall, callback);
                break;
            case "email_changed_new":
            case "email_changed_old":
                actions.emailChanged(obj, undefined, callback);
                break;
            case "delivery_failed":
                actions.deliveryFailed(obj, undefined, callback);
                break;
            case "whatsapp_consumption_data_800mb":
            case "whatsapp_consumption_data_1gb":
            case "whatsapp_consumption_data_2gb":
            case "whatsapp_consumption_data_3gb":
                getCustomer(obj, actions.ignoreWhatsappPenaltyNotificationIfDFF, callback);    
            case "whatsapp_consumption_data_common":
                getCustomer(obj, actions.chargeWhatsappPenalty, callback);
                break;
            case "policy_fair_usage_30gb":
            case "policy_fair_usage_50gb":
            case "policy_fair_usage_55gb":
            case "policy_fair_usage_65gb":
            case "policy_fair_usage_70gb":
                getCustomer(obj, actions.handleFupNotification, callback);
                break;
            default:
                getCustomer(obj, undefined, callback, true);
        }
    }

}

function getCustomer(obj, callfront, callback, basicInfo) {
    var getCache = function (cb) {
        if (obj.serviceInstanceNumber) {
            ec.getCustomerDetailsServiceInstance(obj.serviceInstanceNumber, false, cb, basicInfo);
        } else {
            ec.getCustomerDetailsNumber(obj.prefix, obj.number, false, cb, basicInfo);
        }
    };

    getCache(function (err, cache) {
        if (err) {
            obj.cacheError = err.message;
        }
        if (!cache) {
            obj.cacheFound = false;
        }

        if (!cache) {
            if (!callfront) {    // we don't need cache
                callback(null, obj);
            } else {
                var error = "Customer not found (" + (obj.serviceInstanceNumber ? obj.serviceInstanceNumber : obj.number) + ")";
                if (LOG) common.error("webHookInternal", error);
                callback(new Error(error), obj);
            }
        } else {
            obj.billingAccountNumber = cache.billingAccountNumber;
            obj.customerAccountNumber = cache.customerAccountNumber;
            obj.serviceInstanceNumber = cache.serviceInstanceNumber;
            if (obj.teamID != undefined) {    // not from elitecore
                obj.account = cache.account;
                obj.billingAccount = cache.billingAccountNumber;
                if (obj.serviceSource == "notification") {
                    // force number and email from account if serviceinstance specified
                    obj.number = cache.number;
                    obj.numberSource = "account";
                    obj.email = cache.email;
                    obj.emailSource = "account";
                }
                if (!obj.cardlast4digits && cache.billingCreditCard) {
                    obj.cardlast4digits = cache.billingCreditCard.last4Digits;
                    obj.cardlast4digitSource = "account";
                }
                if (!obj.email) {
                    obj.emailSource = "account";
                    obj.email = cache.email;
                }
                if (!obj.name) {
                    obj.nameSource = "account";
                    obj.name = cache.billingFullName;
                }
            }
            if (callfront) callfront(obj, cache, callback);
            else callback(null, obj);
        }
    }, basicInfo);
}
