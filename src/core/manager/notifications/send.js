var request = require('request');
var crypto = require("crypto");
var fs = require('fs');

var config = require(__base + '/config');
var common = require(__lib + '/common');
var db = require(__lib + '/db_handler');
var ec = require(__lib + '/elitecore');
var pushq = require('../../../notifications/queues/pushq');
var mailq = require('../../../notifications/queues/mailq');
var smsq = require('../../../notifications/queues/smsq');
var slowq = require(__lib + '/slowq');
var constants = require(__core + '/constants');
var async = require('async');

var log = config.LOGSENABLED;

var CACHE_PREFIX_PLAN_VALIDATED = "plan_validated_";

var header;
var footer;

var SG_PREFIX = "65";

module.exports = {

    internal: function(prefix, number, email, name, activity, variables, notificationKey, callback) {
        var params;
        if (arguments.length == 3) {
            if (typeof(arguments[0]) != "object") return callback(new Error("Params Error"));
            params = arguments[0];
            notificationKey = arguments[1];
            callback = arguments[2];
        } else {
            if (!prefix) prefix = SG_PREFIX;
            params = {
                "activity": activity,
                "prefix": prefix,
                "number": number,
                "email": email,
                "name": name,
                "variables": variables
            }
        }

        var url = "http://" + config.MYSELF + ":" + config.WEBPORT +
            "/api/1/webhook/notifications/internal/" + (notificationKey ? notificationKey : config.NOTIFICATION_KEY);

        request({
            "uri" : url,
            "method" : 'POST',
            "timeout" : 20000,
            "json" : params,
        }, function (err, response, body) {
            if (callback) {
                if (err) callback(err);
                else callback(null, body);
            }
            return;
        });
    },

    elitecore: function(params, callback) {
        var path = "http://" + config.MYSELF + ":" + config.WEBPORT +
            "/api/1/webhook/notifications/" + config.ELITECORE_KEY;

        path += "?template=" + JSON.stringify(params);
        //Object.keys(params).forEach((item) => {
        //    path += "\"" + item + "\":\"" + params[item] + "\"";
        //});
        //path += "}";

        request({
            "uri" : path,
            "method" : 'POST',
            "timeout" : 20000
        }, function (err, response, body) {
            if (callback) {
                if (err) callback(err);
                else callback(body);
            }
            return;
        });
    },

    deliver: function (obj, product, callback, cache) {
        var handleResult = function (err, result, notifications) {
            if (!result) result = {};

            if (result.isNotFound) {
                result.status = "ERROR_NOT_FOUND";
            } else if (result.isInvalidTeam) {
                result.status = "ERROR_INVALID_TEAM";
            } else if (result.isServieInstanceRequired) {
                result.status = "ERROR_SIN_REQUIRED";
            } else if (result.isProfileNotFound) {
                result.status = "ERROR_PROFILE_NOT_FOUND";
            } else if (result.isUnsubscribed) {
                result.status = "ERROR_UNSUBSCRIBED";
            }

            result.status = result.status ? result.status : (err ? "ERROR" : "OK");
            result.error = err ? err.message : undefined;

            var notificationsAllowed = [];
            var id = result.serviceInstanceNumber ? result.serviceInstanceNumber : result.number;
            db.cache_get("account_cache", CACHE_PREFIX_PLAN_VALIDATED + id, function (cache) {
                if(cache && cache.reason && id && id.length > 0){
                    result.status = "IGNORED";
                    db.notifications_insert("log", result, function () {
                        if (log) {
                            var info = JSON.stringify(result);
                            if (info.length > 300) info = info.substring(0, 300) + ".....";
                            common.log("NotificationManager", "notification added to 'log' collection, activity="
                            + obj.activity + ", result=" + info);
                        }
    
                        if (callback) return callback(err, result);
                    });
                }else{
                    async.eachLimit(notifications, 10, function (item, callback) {
                        if (result.basePlanId && result.basePlanId === constants.CIRCLES_SWITCH_PLAN_ID 
                            && constants.CIRCLES_SWITCH_BLOCKED_NOTIFICATIONS.indexOf(item.activity) > -1) {
                                result.status = "IGN_SWITCH";
                                return callback();
                            } else {
                                checkTestNumbers(item, function(err, sendToTestNum){
                                    if(sendToTestNum){
                                        checkRestrictions(obj, item, function (err, restrictiedReasons) {
                                            if (!err && restrictiedReasons && restrictiedReasons.length > 0) {
                                                switch (item.type) {
                                                    case "push":
                                                        result.statusSelfcare = "RESTRICT";
                                                        break;
                                                    case "email":
                                                        result.statusEmail = "RESTRICT";
                                                        break;
                                                    case "sms":
                                                        result.statusSMS = "RESTRICT";
                                                        break;
                                                }
                                                return callback();
                                            } else {
                                                checkAndAdjustResendWindow(obj, item, function (err, send) {
                                                    if (send) {
                                                        notificationsAllowed.push(item);
                                                    } else {
                                                        switch (item.type) {
                                                            case "push":
                                                                result.statusSelfcare = "IGN_MULTI";
                                                                break;
                                                            case "email":
                                                                result.statusEmail = "IGN_MULTI";
                                                                break;
                                                            case "sms":
                                                                result.statusSMS = "IGN_MULTI";
                                                                break;
                                                        }
                                                    }
                                                    return callback();
                                                });
                                            }
                                        });
                                    }else{
                                        result.statusSMS = "IGN_TEST";
                                        return callback();
                                    }
                                });
                            }
                    }, function(){
                        db.notifications_insert("log", result, function () {
                            if (log) {
                                var info = JSON.stringify(result);
                                if (info.length > 300) info = info.substring(0, 300) + ".....";
                                common.log("NotificationManager", "notification added to 'log' collection, activity="
                                + obj.activity + ", result=" + info);
                            }

                            if(notificationsAllowed.length > 0){
                                sendToQueue(obj, notificationsAllowed);
                            }
        
                            if (callback) return callback(err, result);
                        });
                    });
                }
            });
        }

        // check default params
        if (!obj || !obj.activity || !obj.teamID) {
            return handleResult(new Error("Invalid params, activity and teamID should not be empty"), obj);
        }

        // add default params
        if (!obj.prefix) obj.prefix = "65";
        if (!obj.whatsapp_support_number) obj.whatsapp_support_number = config.WHATSAPP_SUPPORT_NUMBER;
        if (!obj.transport) obj.transport = [];
        if (!obj.notificationId) obj.notificationId = crypto.randomBytes(16).toString("hex");
        obj.activity = obj.activity.toLowerCase();

        if (log) common.log("NotificationManager", "send: load notification, teamId=" + obj.teamID + ", activity=" + obj.activity);

        // in case if loadFullCache is false data is loaded from database directly and it does not affect BSS api performance
        var loadFullCache = product && ((product.type == "bonus") || (product.type == "boost") || (product.type == "base"));

        var handleNotification =  function (err, cache) {
            if (err) {
                obj.cacheError = err.message;
            }

            if (cache) {
                obj.billingAccountNumber = cache.billingAccountNumber;
                obj.customerAccountNumber = cache.customerAccountNumber;
                obj.serviceInstanceNumber = cache.serviceInstanceNumber;
                obj.basePlanId = cache.base ? cache.base.id ? cache.base.id : null : null;
                if (!obj.number) obj.number = cache.number;
                if (!obj.email) obj.email = cache.email;
            }

            var query = "SELECT n.subject, n.plain, n.html, n.app, n.html_internal_1, n.email_internal_1, " +
                "n.subject_internal_1, GROUP_CONCAT(tr.id) transports, n.groupId, GROUP_CONCAT(tn.teamID) teams " +
                "FROM notifications n, teamsNotifications tn, teams te, notificationsTransports nt, transports tr " +
                "WHERE n.id=tn.notificationID AND tn.teamID=te.id AND n.id=nt.notificationID AND nt.transportID=tr.id " +
                "AND n.activity=" + db.escape(obj.activity) + " GROUP BY n.id"

            db.query_err(query, function (err, result) {
                if (err) {
                    return handleResult(err, obj);
                }
                if (!result || result.length == 0) {
                    obj.isNotFound = true;
                    return handleResult(new Error("Notification " + obj.activity + " is not found for team " + obj.teamID), obj);
                }

                var notificationsToSend = [];
                var notificationObj = (result != 0 && result.length > 0 ? result[0] : undefined);
                notificationObj.teamID = obj.teamID;

                obj.isValidTeam = isValidTeam(obj.teamID, result[0].teams);
                if (!obj.isValidTeam) {
                    obj.isInvalidTeam = true;
                    return handleResult(new Error("It is not allowed to send notification using team ID " + obj.teamID), obj);
                }
                if (notificationObj.groupId > 0 && !obj.serviceInstanceNumber) {
                    obj.isServieInstanceRequired = true;
                    return handleResult(new Error("Notification " + obj.activity + " requires serviceInstance to " +
                    "check subscribed/unsubscribed marketing groups"), obj);
                }

                var query = "SELECT cp.id, GROUP_CONCAT(nu.groupId) unsubscribedGroupIds FROM customerProfile cp " +
                    " LEFT JOIN notificationsUnsubscribtions nu ON (nu.profileId=cp.id) WHERE service_instance_number=" +
                    (obj.serviceInstanceNumber ? db.escape(obj.serviceInstanceNumber) : "NULL") + " GROUP BY id";

                db.query_err(query, function (err, groupResult) {
                    if (err) {
                        return handleResult(err, obj);
                    }
                    if (notificationObj.groupId > 0) {
                        if (!groupResult || !groupResult[0]) {
                            obj.isProfileNotFound = true;
                            return handleResult(new Error("Profile is not found for SIN " + obj.serviceInstanceNumber), obj);
                        }

                        obj.profileId = groupResult[0].id;
                        obj.isUnsubscribed = isUnsubscribed(notificationObj.groupId, groupResult[0].unsubscribedGroupIds);
                        if (obj.isUnsubscribed) {
                            obj.isUnsubscribed = true;
                            return handleResult(new Error("Customer unsubscribed from group id " + notificationObj.groupId), obj);
                        }
                    }

                    if (obj.name) obj.name = toTitleCase(obj.name);
                    else if (cache && cache.first_name) obj.name = toTitleCase(cache.first_name);
                    if (obj.first_name) obj.first_name = toTitleCase(obj.billingFullName ? obj.billingFullName : obj.first_name);
                    if (obj.firstName) obj.firstName = toTitleCase(obj.firstName);
                    if (obj.last_name) obj.last_name = toTitleCase(obj.last_name);
                    if (obj.lastName) obj.lastName = toTitleCase(obj.lastName);
                    if(obj.joined_user_name) obj.joined_user_name = toTitleCase(obj.joined_user_name);
                    if(obj.customer_name) obj.customer_name = toTitleCase(obj.customer_name);

                    obj.plain = notificationObj.plain;
                    obj.subject = notificationObj.subject;
                    obj.html = notificationObj.html;
                    obj.email_internal_1 = notificationObj.email_internal_1;
                    obj.subject_internal_1 = notificationObj.subject_internal_1;
                    obj.html_internal_1 = notificationObj.html_internal_1;
                    obj.app_type = getAppType(notificationObj.transports);
                    obj.groupId = notificationObj.groupId;

                    obj.send_push = (obj.app_type);
                    obj.send_email = isTransport("EMAIL", result[0].transports);
                    obj.send_sms = isTransport("SMS", result[0].transports);
                    obj.send_internal = isTransport("INTERNAL", result[0].transports);

                    //send App Push, app related notification can be handled for user whose information we have in cache
                    if (cache && obj.number && obj.send_push && obj.app_type && notificationObj.app) {
                        if (log) common.log("NotificationManager", "send: push to number" + obj.number);

                        if (product && ((product.type == "bonus")
                            || (product.type == "boost")
                            || (product.type == "base"))) {

                            obj.product_removed = product.removed;
                            if (product.component == "voice") {
                                obj.value = cache.base.mins + (product.removed ? 0 : product.mins);
                                obj.unit = "MINS";
                                obj.base_value = cache.base.mins;
                                obj.base_change = product.mins;
                            } else if (product.component == "sms") {
                                obj.value = cache.base.sms + (product.removed ? 0 : product.sms);
                                obj.unit = "SMS";
                                obj.base_value = cache.base.sms;
                                obj.base_change = product.sms;
                            } else {
                                var base_data = 0;
                                if (product.type == "base") {
                                    if (product.component == "data") {
                                        base_data = cache.base.kb;
                                    }
                                }

                                var kb = base_data + (product.removed ? 0 : product.kb);
                                var dataPrettified = common.prettifyKBUnit(kb);
                                obj.value = dataPrettified.value;
                                obj.unit = dataPrettified.unit;
                                obj.kbvalue = kb;
                                obj.base_value = base_data;
                                obj.base_change = product.kb;
                            }
                        }

                        obj.content = {};
                        obj.read = false;
                        obj.app = common.safeParse(evaluate(obj, notificationObj.app));
                        obj.transport.push(obj.app_type);

                        if (obj.app_type === "selfcare") {
                            obj.statusSelfcare = "SENDING";
                        } else if (obj.app_type === "gentwo") {
                            obj.statusGentwo = "SENDING";
                        }

                        notificationsToSend.push({
                            type: "push", appType: obj.app_type, message: evaluate(obj, notificationObj.app),
                            activity: obj.activity
                        });
                        if (loadFullCache) {
                            //re-cache customer data in case of any app related notifications
                            ec.setCache(obj.number, obj.prefix, null, cache.account, function (err, obj) {
                                //do nothing
                            });
                        }
                    }

                    //send SMS
                    if (obj.number && obj.send_sms && notificationObj.plain) {
                        if (log) common.log("NotificationManager", "send: SMS to number=" + obj.number);
                        obj.transport.push("sms");
                        obj.statusSMS = "SENDING";
                        notificationsToSend.push({
                            type: "sms", activity: obj.activity, from: "CirclesLife",
                            to: obj.number, message: evaluate(obj, obj.plain)
                        });
                    }

                    var addEmailItem = function(emailType, from, to, subject, html) {

                        if (!header || !footer || config.DEV) {
                            var headerFile = fs.readFileSync(__res + "/notification/email-header.html", "utf8");
                            var footerFile = fs.readFileSync(__res + "/notification/email-footer.html", "utf8");
                            var myRegExp = new RegExp("\\$imagehost", 'g');
                            header = headerFile.replace(myRegExp, config.IMAGEHOST);
                            footer = footerFile.replace(myRegExp, config.IMAGEHOST);
                        }

                        var selectedNumber = obj.number_selected ? obj.number_selected : obj.number;
                        var footerText = selectedNumber && emailType !== "INTERNAL"
                            ? "This email is about your Circles.Life account under number " + selectedNumber + "." : "";
                        var footerUpdated = footer.replace(new RegExp("\\$number_disclaimer_footer", 'g'), footerText);

                        var footerUnSubscriptionText = obj.groupId > 0 ? 'To stop receiving these emails, <a href="https://www.' +
                        (config.DEV ? 'staging.circles.asia' : 'circles.life') + '/settings/notifications/unsubscribe?' +
                        'profileId=' + obj.profileId + '&email=' + obj.email + '" style="color: #0099ff;">unsubscribe here</a>.' : '';
                        var footerUpdated = footerUpdated.replace(new RegExp("\\$unsubscription_footer", 'g'), footerUnSubscriptionText);

                        var replacements = {};
                        if (emailType === "INTERNAL") {
                            replacements.number = "<a href='https://" + config.MYFQDN + ":" + config.WEBSPORT
                            + "/customers/customers.html?number=$number' style='color: #0099ff;'>$number</a>";
                        }

                        var subject = evaluate(obj, subject);
                        var message = evaluate(obj, header + "" + html + "" + footerUpdated, replacements);
                        var hasAttachment = obj.attachment && obj.attachment.path && obj.attachment.filename;
                        var globalAttachment = obj.attachment && obj.attachment.global;

                        if (log) common.log("NotificationManager", "send: internal to " + to + ", hasAttachment=" +
                        hasAttachment + ", globalAttachment=" + globalAttachment);

                        var addValidEmail = function (files, fileNames, attachments) {
                            if (emailType === "INTERNAL") {
                                obj.transport.push("internal");
                                obj.statusInternal = "SENDING";
                            } else {
                                obj.transport.push("email");
                                obj.statusEmail = "SENDING";
                            }
                            notificationsToSend.push({
                                type: "email", from: from, to: to, emailType: emailType, subject: subject, message: message,
                                attachments: attachments, files: files, fileNames: fileNames,
                                activity: obj.activity
                            });
                        }

                        var logErrorEmail = function (err) {
                            if (err) obj.attachmentError = err.message;
                            if (emailType === "INTERNAL") {
                                obj.statusInternal = "ERROR";
                                obj.transport.push("internal_error");
                            } else {
                                obj.statusEmail = "ERROR";
                                obj.transport.push("email_error");
                            }
                        }

                        if (!hasAttachment) {
                            addValidEmail();
                        } else if (globalAttachment) {
                            addValidEmail([obj.attachment.path], [obj.attachment.filename]);
                        } else {
                            var attachments = [];
                            try {
                                var data = fs.readFileSync(obj.attachment.path);
                                attachments.push({filename: obj.attachment.filename, content: data});
                                if (log) common.log("NotificationManager", "send: loaded email data " + data.length);
                                addValidEmail([obj.attachment.path], [obj.attachment.filename], attachments);
                            } catch (e) {
                                logErrorEmail(err);
                            }
                        }
                    }

                    // send Email
                    var fromEmail = "Circles.Life <happiness@circles.life>";

                    if (obj.email && obj.send_email && notificationObj.subject && notificationObj.html) {
                        addEmailItem("DEFAULT", fromEmail, obj.email, notificationObj.subject, notificationObj.html);
                    }

                    if (obj.send_internal && notificationObj.email_internal_1
                        && notificationObj.subject_internal_1  && notificationObj.html_internal_1) {
                        var fromEmailInternal = "Circles.Life No Reply <care@circles.asia>";
                        addEmailItem("INTERNAL", fromEmailInternal, notificationObj.email_internal_1,
                            notificationObj.subject_internal_1, notificationObj.html_internal_1);
                    }
                    handleResult(undefined, obj, notificationsToSend);
                });
            });

        }

        if (cache) {
            handleNotification(undefined, cache);
        } else if (obj.number) {
            ec.getCustomerDetailsNumber(obj.prefix, obj.number, false, handleNotification, !loadFullCache);
        } else if (obj.billingAccount){
            ec.getCustomerDetailsBilling(obj.billingAccount, handleNotification, !loadFullCache);
        } else if (obj.billingAccountNumber){
            ec.getCustomerDetailsBilling(obj.billingAccountNumber, handleNotification, !loadFullCache);
        } else if (obj.serviceInstanceNumber){
            ec.getCustomerDetailsService(obj.serviceInstanceNumber, false, handleNotification, !loadFullCache);
        } else if (obj.customerAccountNumber){
            ec.getCustomerDetailsAccount(obj.customerAccountNumber, false, handleNotification, !loadFullCache);
        } else {
            handleNotification();
        }
    }

}

function checkAndAdjustResendWindow(obj, item, callback){
    if(item.emailType && item.emailType == "INTERNAL"){
        return callback(null, true);
    }
    if(constants.NOTIFICATION_RESEND_BLOCK_BLACKLIST.indexOf(obj.activity) != -1 || (obj.email && item.type == "email" && obj.email.length == 0) || (obj.number && item.type != "email" && obj.number.length <= 2)){
        return callback(null, true);
    }else{
        var key = "limit_" + obj.activity + "_" + item.type + "_";
        if(obj.serviceInstanceNumber && obj.serviceInstanceNumber.length > 0){
            key += obj.serviceInstanceNumber;
        }else if(obj.number && obj.number.length > 0){
            key += obj.number;
        }else if(obj.email && obj.email.length > 0){
            key += obj.email;
        }
        db.cache_lock("cache",  key, "api", constants.NOTIFICATION_RESEND_WINDOW, (lock) => {
            if(lock == 1){
                callback(null, true);
            }else{
                db.cache_incr("cache", key, constants.NOTIFICATION_RESEND_WINDOW, (result) => {
                    common.log("Notification not allowed. Already sent recently, Key : " + key);
                    callback(null, false);
                });
            }
        });
    }
}

function sendToQueue(obj, items) {
    if (!items) return;

    var sendImmediately = !obj.delay || obj.delay <= 0;
    var sendWithDelay = [];

    items.forEach(function (notification) {

        var data = {
            activity: obj.activity,
            notificationId: obj.notificationId,
            notificationType: notification.type,
            prefix: obj.prefix,
            number: obj.number,
            customerAccountNumber: obj.customerAccountNumber,
            serviceInstanceNumber: obj.serviceInstanceNumber
        };

        if (obj.metadata && obj.metadata.updateTriggers) {
            data.updateTriggers = obj.metadata.updateTriggers[notification.type];
        }

        if (notification.type === 'push') {
            data.appType = notification.appType;
            data.message = notification.message;

            if (sendImmediately) {
                pushq.send(data);
            } else {
                sendWithDelay.push(data);
            }
        } else if (notification.type === 'email') {
            data.from = notification.from;
            data.to = notification.to;
            data.subject = notification.subject;
            data.html = notification.message;
            data.attachments = notification.attachments;
            data.files = notification.files;
            data.fileNames = notification.fileNames;
            data.type = notification.emailType;

            if (sendImmediately) {
                mailq.send(data);
            } else {
                sendWithDelay.push(data);
            }
        } else if (notification.type === 'sms') {
            data.from = notification.from;
            data.to = notification.to;
            data.plain = notification.message;

            if (sendImmediately) {
                smsq.send(data);
            } else {
                sendWithDelay.push(data);
            }
        }
    });

    if (sendWithDelay.length > 0) {
        slowq.send({
            activity: obj.activity,
            prefix: obj.prefix,
            number: obj.number,
            delay: obj.delay,
            customerAccountNumber: obj.customerAccountNumber,
            serviceInstanceNumber: obj.serviceInstanceNumber,
            items: sendWithDelay
        });
    }
}

function checkRestrictions(obj, item, callback){
    if(obj.serviceInstanceNumber && obj.serviceInstanceNumber.length > 0){
        var collectionName;
        switch(item.type){
            case "push":
                collectionName = "notifications_push";
                break;
            case "email":
                collectionName = "notifications_emails";
                break;
            case "sms":
                collectionName = "notifications_sms";
                break;
        }
        if(collectionName){
            var q = "SELECT s.id, s.name, SUM(s.value) value FROM (SELECT DISTINCT r.id, r.name, IF(n.activity=" + db.escape(obj.activity) + ",nr.value,0) value FROM restrictions r LEFT JOIN notificationsRestrictions nr ON r.id=nr.restrictionID LEFT JOIN notifications n ON n.id=nr.notificationID) s GROUP BY s.id";
            db.query_err(q, function(err, rows){
                if(err || !rows || rows.length == 0){
                    callback();
                }else{
                    var restrictiedReasons = [];
                    async.each(rows, function(row, cb){
                        if(parseInt(row.value) != 0){
                            switch(row.name){
                                case "ALLOW_AGAIN_AFTER_DAYS":
                                    db[collectionName].findOne({serviceInstanceNumber: obj.serviceInstanceNumber, activity: obj.activity, ts: {"$gt": (new Date().getTime() - (3600000 * 24 * parseInt(row.value)))}}, function(err, notification){
                                        if(!err && notification){
                                            restrictiedReasons.push({reason: "ALLOW_AGAIN_AFTER_DAYS"});
                                            cb();
                                        }else{
                                            cb();
                                        }
                                    });
                                    break;
                                case "ALLOW_MAX_COUNT":
                                    db[collectionName].find({serviceInstanceNumber: obj.serviceInstanceNumber, activity: obj.activity}).toArray(function(err, notifications){
                                        if(!err && notifications && notifications.length >= parseInt(row.value)){
                                            restrictiedReasons.push({reason: "ALLOW_MAX_COUNT"});
                                            cb();
                                        }else{
                                            cb();
                                        }
                                    });
                                    break;
                                default:
                                    cb();
                            }
                        }else{
                            cb();
                        }
                    }, function(err){
                        callback(null, restrictiedReasons);
                    })
                }
            });
        }else{
            callback();
        }
    }else{
        callback();
    }
}

function checkTestNumbers(item, callback){
    if(!config.DEV){
        callback(null, true);
    } else if(item.type == "sms"){
        db.numberPool.findOne({_id: item.to}, function(err, item){
            if(err){
                callback(null, true);
            }else if(item){
                callback(null, true);
            }else{
                callback(null, false);
            }
        });
    }else{
        callback(null, true);
    }
}

function evaluate(obj, message, replacements) {
    Object.keys(obj).forEach(function (key) {
        if (key && key.length > 0) {
            if (key != "customTables") {
                var myRegExp = new RegExp("\\$" + key, 'g');
                var myReplacement = replacements && replacements[key];
                var repl = "";

                if (typeof(obj[key]) == "object" && obj[key] && obj[key] != null) {
                    if (obj[key].head && obj[key].body) {
                        repl = tableify(obj[key]);
                    } else {
                        repl = JSON.stringify(obj[key]);
                    }
                } else {
                    if (myReplacement) {
                        if(constants.TEMPLATE_NAME_VARIABLES.indexOf(key) != -1){
                            repl = myReplacement.replace(myRegExp, toTitleCase(obj[key]));
                        }else{
                            repl = myReplacement.replace(myRegExp, obj[key]);
                        }
                    } else {
                        repl = (constants.TEMPLATE_NAME_VARIABLES.indexOf(key) != -1) ? toTitleCase(obj[key]) : obj[key];
                    }
                }

                message = message.replace(/\n/g, '');
                message = message.replace(myRegExp, repl);
            } else {
                if (obj.customTables.length > 0) {
                    obj.customTables.forEach((tableInfo) => {
                        if (tableInfo.rowtag) {
                            var myRegExp = new RegExp("<tr[^>]*rowtag=\"" + tableInfo.rowtag + "\"(.*?)<\/tr>" , 'g');
                            var originalReplacements = message.match(myRegExp);
                            if (originalReplacements && originalReplacements.length > 0) {
                                var originalReplacement = originalReplacements[0];

                                var myReplacement = "";
                                tableInfo.rows.forEach((row) => {
                                    myReplacement += evaluate(row, common.deepCopy(originalReplacement))
                                });

                                message = message.replace(originalReplacement, myReplacement);
                            }
                        }
                    });
                }
            }
        }
    });
    return message;
}

function tableify(tableArr) {
    var tableStyle = (tableArr.style && tableArr.style.table) ? "style='" + tableArr.style.table + "'" : "";
    var thStyle = (tableArr.style && tableArr.style.th) ? "style='" + tableArr.style.th + "'" : "";
    var tdStyle = (tableArr.style && tableArr.style.td) ? "style='" + tableArr.style.td + "'" : "";
    var table = "<table " + tableStyle + ">";
    table += "<thead>";
    tableArr.head.forEach(function (th) {
        table += "<th " + thStyle + ">" + th + "</th>";
    });
    table += "</thead>";
    table += "<tbody>";
    tableArr.body.forEach(function (tr) {
        table += "<tr>";
        tr.forEach(function (td) {
            table += "<td " + tdStyle + ">" + td + "</td>";
        });
        table += "</tr>";
    });
    table += "</tbody>";
    table += "</table>";
    return table;
}

function getAppType(transports) {
    if (transports.indexOf(3) > -1) {
        return "selfcare";
    } else if (transports.indexOf(5) > -1) {
        return "gentwo";
    } else {
        return undefined;
    }
}

/**
 * 1 - SMS,
 * 2 - Email,
 * 3 - Care,
 * 4 - USSD,
 * 5 - Talk,
 * 7 - Internal
 * @param type
 * @param transports
 * @returns {boolean}
 */
function isTransport(type, transports) {
    if ("SELFCARE" === type && transports.indexOf(3) > -1) {
        return true;
    } else if ("GENTWO" === type && transports.indexOf(5) > -1) {
        return true;
    } else if ("INTERNAL" === type && transports.indexOf(7) > -1) {
        return true;
    } else if ("EMAIL" === type && transports.indexOf(2) > -1) {
        return true;
    } else if ("SMS" === type && transports.indexOf(1) > -1) {
        return true;
    } else {
        return false;
    }
}

function isValidTeam(teamID, teams) {
    return teams && teams.indexOf(teamID) > -1 ? true : false;
}

function isUnsubscribed(groupID, groups) {
    return groups && groups.indexOf(groupID) > -1 ? true : false;
}

function toTitleCase(str) {
    if (str && str.replace) {
        return str.replace(/\w[\S\.]*/g, function (txt) {
            return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
        });
    } else {
        return str;
    }
}
