var async = require('async');
var config = require('../../../../config');
var common = require('../../../../lib/common');
var db = require('../../../../lib/db_handler');

var LOG = config.LOGSENABLED;

module.exports = {

    enqueue: function(sms, retry, callback) {
        var attempts = retry ? 3 : 1;
        var job = db.queue.create('sms', sms)
            .events(false)
            .removeOnComplete(true)
            .delay(0)
            .attempts(attempts)
            .backoff({"delay": 10 * 1000, "type": "fixed"})
            .save(function (err) {
                if (LOG) common.log("sms queued", job.id);
                if (callback) callback(err, job.id);
            });
    },

    send: function(carrier, retry, sender, prefix, number, plain, callback) {
        module.exports.enqueue({
                "carrier" : carrier,
                "from" : sender, 
                "prefix" : prefix,
                "to" : number,
                "plain" : plain }, retry, function(err, jobid) {
            db.queue.on('job failed', function(id, result) {
                if (jobid != id) return;
                if (LOG) common.error("smsManager", "send: job failed, id=" + jobid);
                return callback(result);
            });
            db.queue.on('job complete', function(id, result){
                if (jobid != id) return;
                if (LOG) common.log("sms completed", jobid);
                return callback(undefined, result);
            });  
        });
    },

    test: function(prefix, number, callback) {
        Promise.resolve({}).then(() => {
            return new Promise((resolve, reject) => {
                module.exports.send("sdp", false, "CirclesLife", prefix, number, "Test SMS 1 of 2", (e,r)=>{
                    resolve({ "error" : e, "result" : r });
                });
            });
        }).then((sdpResult) => {
            return new Promise((resolve, reject) => {
                module.exports.send("nexmo", false, "CirclesLife", prefix, number, "Test SMS 2 of 2", (e,r)=>{
                    resolve({ "sdp" : sdpResult, "nexmo" : { "error" : e, "result" : r }});
                });
            });
        }).then((result) => {
            callback(undefined, result);
        });
    }

}
