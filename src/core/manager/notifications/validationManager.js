

const crypto = require('crypto');
const config = require(`${__base}/config`);
const common = require(`${__lib}/common`);
const db = require(`${__lib}/db_handler`);
const notificationSend = require(`${__manager}/notifications/send`);

const LOG = config.LOGSENABLED;

class ValidationManager {
    static pinSMS(args, callback) {
        if (!args || !args.teamID || !args.teamName || !args.activity || !args.prefix || !args.number)
            return callback(new Error('Incomplete Parameters'));
        function pinGet(cb) {
            const digits = 4;
            const pin = Math.floor(Math.pow(10,digits-1) + Math.random() * Math.pow(10,digits)).toString().substring(0, digits);
            db.cache_lock('cache', `${args.activity}_${args.number}`, pin, 5 * 60 * 1000, (lock) => {
                if (lock) return cb(undefined, pin);
                db.cache_get('cache', `${args.activity}_${args.number}`, (pin) => {
                    cb(undefined, pin);
                });
            });
        }
        pinGet((err, pin) => {
            if (!pin) return callback(new Error('PIN Error'));
            const message = {
                teamID: args.teamID,
                teamName: args.teamName,
                activity: args.activity,
                prefix: args.prefix,
                number: args.number,
                pin: pin.toString(),
                notificationId: crypto.randomBytes(16).toString('hex'),
            };
            notificationSend.deliver(message, undefined, (err) => {
                if (err) return callback(err);
                else return callback(undefined, {
                    notificationId: message.notificationId
                });
            });
        });
    }

    static pinCompare(args, callback) {
        if (!args || !args.notificationId || !args.pin)
            return callback(new Error('Incomplete Parameters'));
        db.notifications_logbook.findOne({
            notificationId: args.notificationId,
            pin: args.pin
        }, (err, result) => {
            if (err) return callback(new Error('Internal Error'));
            else if (!result) return callback(undefined, {
                notificationId: args.notificationId,
                validation: 0
            });
            else return callback(undefined, {
                notificationId: args.notificationId,
                validation: 1
            });
        });
    }
}

module.exports = ValidationManager;
