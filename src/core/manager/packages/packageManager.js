var dateformat = require('dateformat');
var config = require(__base + '/config');
var common = require(__lib + '/common');
var ec = require(__lib + '/elitecore');
var db = require(__lib + '/db_handler');
var notificationSend = require(__core + '/manager/notifications/send');
var hlrManager = require(__core + '/manager/hlr/hlrManager');
var transactionManager = require('../account/transactionManager');
var constants = require('../../constants');
var BaseError = require('../../errors/baseError');

var LOG = config.LOGSENABLED;

//exports

module.exports = {

    prematureTermCharge: (config.DEV ? "PRD00765" : "PRD00617"),

    getNotificationActivities: function (type, options) {
        if (!options) options = {};

        if (type == "FREE") {
            return {
                suppressBss: true,
                activityImmediate: "plus_added_free_immediate",
                activityImmediateWithExpiration: "plus_added_free_immediate_with_expiration",
                activityTomorrow: "",
                activityTomorrowWithExpiration: "",
                activityNextCycle: "plus_added_free_next_cycle",
                activityNextCycleWithExpiration: "plus_added_free_next_cycle_with_expiration",
                activityExpirationReminder: "plus_expiration_free_tomorrow",
                params: options.params,
                paramsReminder: options.paramsReminder
            }
        } else if (type == "FREE_ON_ACTIVATION") {
            return {
                suppressBss: true,
                activityExpirationReminder: "plus_expiration_free_tomorrow",
                params: options.params,
                paramsReminder: options.paramsReminder
            }
        }

        return {};
    },

    getCustomerGeneralHistoryIds: function (productId, cache) {
        var ids = "";
        if (!cache) {
            return ids;
        }

        if (cache.general) cache.general.forEach(function (item) {
            if (item.packageHistoryId && (item.id == productId)) {
                if (ids) ids += ',';
                ids += item.packageHistoryId;
            }
        });


        if (cache.general_future) cache.general_future.forEach(function (item) {
            if (item.packageHistoryId && (item.id == productId) && !ids.includes(item.packageHistoryId)) {
                if (ids) ids += ',';
                ids += item.packageHistoryId;
            }
        });

        return ids;
    },

    addonUpdate: function (prefix, number, cmd, productId, historyList, suppressNotify, callback,
                           overrideEffect, overrideRecurrent, options) {
        if (!options) options = {};
        if (!callback) callback = () => {
        };

        if (LOG) common.log("PackageManager", "addonUpdate: prefix=" + prefix + ", number=" + number +
        ", cmd=" + cmd + ", productId=" + productId + ", historyList=" + JSON.stringify(historyList));

        if (productId == hlrManager.roamingOff) {
            return hlrManager.set(hlrManager.roamingOff, prefix, number, suppressNotify, callback);
        } else if (productId == hlrManager.datavoicesms()) {
            return hlrManager.set(hlrManager.datavoicesms(), prefix, number, suppressNotify, callback);
        } else if (productId == hlrManager.voicesms()) {
            return hlrManager.set(hlrManager.voicesms(), prefix, number, suppressNotify, callback);
        }

        var product = (productId) ? ec.findPackage(ec.packages(), productId) : undefined;
        if (!prefix || !number || !product) {
            common.error("PackageManager", "addonUpdate: invalid params");
            callback(new Error("Invalid params"));
            return;
        }

        ec.getCustomerDetailsNumber(prefix, number, false, function (err, cache) {
            if (err) {
                common.error("PackageManager", "addonUpdate: failed to load a customer info, error=" + err.message);
                return callback(err);
            }
            if (!cache) {
                common.error("PackageManager", "addonUpdate: customer not found for number=" + number);
                return callback(new Error("Customer not found"));
            }

            var current = [];
            var future = [];

            var paidReplica;
            var restrictOverlapping = false;

            if (product.type == "general") {

                // in order to find any version of addon (paid, free) we need to check all possible combinations
                // when free version of paid addon is subscribed, when paid version of free addon is subscribed
                // that way we can avoid any double subscriptions of addons
                if (constants.OVERLAPPING_RESTRICTEDADDON_IDS.indexOf(product.id) > -1) {
                    restrictOverlapping = true;
                }
                paidReplica = ec.findPackageWithFreeId(ec.packages(), product.id);
                var paidReplicaId = paidReplica ? paidReplica.id : undefined;
                var freeReplicaId = product.freePackage ? product.freePackage : undefined;
                var originalId = product.id;

                current = !cache.general ? undefined : cache.general.filter(function (o) {
                    return (o.id == originalId || (paidReplicaId && o.id == paidReplicaId)
                    || (freeReplicaId && o.id == freeReplicaId)) ? o : undefined;
                });
                future = !cache.general_future ? undefined : cache.general_future.filter(function (o) {
                    return (o.id == originalId || (paidReplicaId && o.id == paidReplicaId)
                    || (freeReplicaId && o.id == freeReplicaId)) ? o : undefined;
                });
            } else if (product.type == "bonus") {
                current = !cache.bonus ? undefined : cache.bonus.filter(function (o) {
                    return o.id == product.id ? o : undefined;
                });
                future = !cache.bonus_future ? undefined : cache.bonus_future.filter(function (o) {
                    return o.id == product.id ? o : undefined;
                });
            } else if (product.type == "boost") {
                current = !cache.boost ? undefined : cache.boost.filter(function (o) {
                    return o.id == product.id ? o : undefined;
                });
            }

            if ((cmd == "PUT") || (cmd == "subscribe")) {
                if (cache.serviceInstanceUnlimitedData.enabled === true &&
                    constants.RESTRICTED_ADDONS_FOR_UNLIMITED_DATA_USERS.indexOf(product.id) >= 0) {
                    return callback(new BaseError(product.title + " Addon cannot be subscribed since you are " +
                        "already subscribed to " + constants.DFF_ADDON_NAME + " addon which gives you maximum data.",
                        "ERROR_RESTRICTED_ADDON_SUBSCRIPTION"));
                }

                if ((!options || !options.allowMultiple) && product.type == "general" && current && current[0]
                    && (new Date(current[0].expiryDate).toISOString() == "9999-01-01T15:59:59.000Z")) {
                    // limit general addons to 1
                    return callback(new Error("Addon already subscribed"));
                }

                // if addon is in current, sub for next cycle
                var currentlyActive = current ? current.length > 0 : false;
                var subEffect = currentlyActive ? 2 : 0;

                var toDate;
                var fromDateReplica;
                var productFromDate = null;
                var billCycle;

                if (currentlyActive) {
                    billCycle = ec.nextBill(1, undefined, new Date(current[0].expiryDate));
                    if (restrictOverlapping === true) {
                        var currentAddonExpiryDateUTC = new Date(new Date(current[0].expiryDate).getTime() + 1000);
                        productFromDate = new Date(currentAddonExpiryDateUTC.getFullYear(),
                        currentAddonExpiryDateUTC.getMonth(),
                        currentAddonExpiryDateUTC.getDate() + 1).toISOString().split('.')[0];
                        subEffect = 4;
                    } else {
                        subEffect = 2;
                    }
                } else {
                    subEffect = 0;
                }
                if (options.activeMonths > 0) {
                    // if override effect = 2 means subscribe from the next billing cycle onwards
                    // for activeMonths. If it's currently active subscribe from next billing cycle.
                    for (var i = (overrideEffect == 2 && !currentlyActive) ? -1 : 0; i < options.activeMonths; i++) {
                        billCycle = ec.nextBill(1, undefined, billCycle ? billCycle.endDateUTC : new Date());
                    }
                    toDate = billCycle.endDateUTC;
                    fromDateReplica = new Date(billCycle.endDateUTC.getFullYear(),
                        billCycle.endDateUTC.getMonth(),
                        billCycle.endDateUTC.getDate() + 1).toISOString().split('.')[0];
                }

                var effect = overrideEffect ? parseInt(overrideEffect) : subEffect;
                var recurrent = overrideRecurrent ? (overrideRecurrent == "true" ? true : false) : product.recurrent;

                //  If paidAddonNextMonth is false do not subscribe to the paid addon. 
                var paidAddonNextMonth = true;
                if (options.paidAddonNextMonth == false) {
                    paidAddonNextMonth = options.paidAddonNextMonth;
                }

                var sub = {
                    "product_id": product.id,
                    "recurrent": recurrent,
                    "toDate": toDate
                };
                // If effect = 4 means that subscribe the addon from the date supplied.
                // There maybe cases when the there might be overlapping addons, in that case
                // we accumulate the addon to the next month. Eg 2020 and dff.
                if (effect === 4) {
                    sub.fromDate = productFromDate;
                } else {
                    sub.effect = effect;
                }

                var subReplica = (!paidReplica || !fromDateReplica || !paidAddonNextMonth) ?
                    undefined : {
                    "product_id": paidReplica.id,
                    "recurrent": paidReplica.recurrent,
                    "fromDate": fromDateReplica
                };

                var sendPaymentNotifications = () => {
                    var addonName = product.title ? product.title : (product.name ? product.name : product.id);

                    var notification = {
                        teamID: 1,
                        prefix: cache.prefix,
                        number: cache.number,
                        email: cache.email,
                        name: cache.first_name,
                        addonname: addonName,
                        amount: product.price,
                        serviceInstanceNumber: cache.serviceInstanceNumber,
                        activity: 'plus_charged_immediate'
                    }

                    notificationSend.deliver(notification, undefined, function () {
                        // do nothing
                    }, cache);
                }

                var sendNotifications = () => {
                    if (options.notification) {
                        var nextBillCycle = ec.nextBill(cache.billing_cycle);
                        var nextCycleDate = dateformat(Date.parse(nextBillCycle.end), "dS mmmm");
                        var expirationDate = dateformat(toDate, "dS mmmm");
                        var addonName = product.title ? product.title : (product.name ? product.name : product.id);

                        var activity;
                        if (effect == 0) { // Immediate
                            activity = !toDate ? options.notification.activityImmediate
                                : options.notification.activityImmediateWithExpiration;
                        } else if (effect == 1) { // Tomorrow
                            activity = !toDate ? options.notification.activityTomorrow
                                : options.notification.activityTomorrowWithExpiration;
                        } else if (effect == 2) { // Next Billing Cycle
                            activity = !toDate ? options.notification.activityNextCycle
                                : options.notification.activityNextCycleWithExpiration;
                        }

                        if (activity) {
                            var notification = {
                                teamID: 1,
                                prefix: cache.prefix,
                                number: cache.number,
                                email: cache.email,
                                name: cache.name,
                                addonname: addonName,
                                nextcycledate: nextCycleDate,
                                expirationdate: expirationDate,
                                serviceInstanceNumber: cache.serviceInstanceNumber,
                                activity: activity
                            }

                            if (options.notification.params) {
                                Object.keys(options.notification.params).forEach((key) => {
                                    notification[key] = options.notification.params[key];
                                })
                            }

                            if (LOG) common.log("PackageManager", "addonUpdate: send subscription notification" +
                            ", serviceInstanceNumber=" + cache.serviceInstanceNumber + ", addonName=" + addonName +
                            ", activity=" + activity);

                            notificationSend.deliver(notification, undefined, function () {
                                // do nothing
                            }, cache);
                        }

                        var activityExpiration = options.notification.activityExpirationReminder;
                        if (toDate && activityExpiration) {

                            var currentDate = new Date();
                            var startDate = new Date(toDate.getTime() - 12 * 60 * 60 * 1000);
                            var title = "Addon '" + product.title + "' expiration reminder for " + cache.serviceInstanceNumber;
                            var trigger = "[CMS][SUBSCRIPTION]";

                            if (LOG) common.log("PackageManager", "addonUpdate: schedule expire reminder" +
                            ", serviceInstanceNumber=" + cache.serviceInstanceNumber + ", date=" + startDate.toISOString() +
                            ", activityExpiration=" + activityExpiration + ", current date=" + currentDate.toISOString());

                            // in case if a customer joins afternoon on the last day of a month,
                            // notification should be send in 1 hour from now

                            if (startDate.getTime() < (currentDate.getDate() + 60 * 60 * 1000)) {
                                startDate = new Date(currentDate.getDate() + 60 * 60 * 1000);
                            }

                            var event = {
                                action: "notification",
                                start: startDate.getTime(),
                                serviceInstanceNumber: cache.serviceInstanceNumber,
                                prefix: cache.prefix,
                                number: cache.number,
                                email: cache.email,
                                name: cache.billingFullName,
                                addonname: addonName,
                                activity: activityExpiration,
                                allDay: false,
                                trigger: trigger,
                                title: title
                            };

                            if (options.notification.paramsReminder) {
                                Object.keys(options.notification.paramsReminder).forEach((key) => {
                                    event[key] = options.notification.paramsReminder[key];
                                })
                            }

                            db.scheduler.insert(event);
                        }
                    }
                }

                if (options.notification && options.notification.suppressBss && product) {
                    if (LOG) common.log("PackageManager", "addonUpdate: suppress BSS notifications, " +
                    "serviceInstanceNumber=" + cache.serviceInstanceNumber + ", product name=" + product.name);

                    var notificationManager = require(__core + '/manager/notifications/notificationManager');
                    notificationManager.suppressAddonNotifications(cache.serviceInstanceNumber, product.name,
                        "Suppressed by subscriber manager", (err, result) => {
                            // do nothing
                        }, 60 * 1000);
                }

                if (!options.paymentsDisabled && !currentlyActive && product.advancedPayment && product.price > 0) {
                    var addonName = (product.title ? product.title : product.name);
                    var description = 'Payment for ' + addonName;

                    transactionManager.performPayment(cache.billingAccountNumber, {
                        type: transactionManager.TYPE_PLUS_PAYMENT,
                        amount: product.price,
                        internalId: productId,
                        metadata: {
                            actions: [
                                transactionManager.createAdvancedPaymentAction(),
                                transactionManager.createAddonSubAction(description, productId, true, 0)
                            ]
                        }
                    }, (status, transaction, callback) => {
                        if (LOG) common.log("PackageManager", "addBoostHandler: transaction status " +
                        "changed, account=" + cache.account + ", status=" + status);
                        callback();
                    }, (err, result) => {
                        if (err) {
                            return callback(err);
                        }
                        if (result && result.validPaymentError)  {
                            return callback(result.validPaymentError);
                        }

                        sendPaymentNotifications();
                        sendNotifications();
                        callback(undefined, result);
                    });
                } else {
                    var subArray = subReplica ? [sub, subReplica] : [sub];
                    ec.addonSubscribe(number, cache.serviceInstanceNumber, subArray,
                        cache.billing_cycle, function (err, result) {
                            ec.getCustomerDetailsNumber(cache.prefix, cache.number, true, function () {
                                var response = {
                                    "args": {"msisdn": cache.number, "serviceInstanceNumber": cache.serviceInstanceNumber},
                                    "result": result, "sub": sub, "error": (err ? err.message : undefined)
                                };
                                sendNotifications();
                                callback(err, response);
                            });
                        });
                }
            } else if ((cmd == "DELETE") || (cmd == "unsubscribe")) {
                if (!historyList) {
                    return callback(new Error("History List Empty"));
                }

                if (typeof historyList === "string") {
                    historyList = historyList.split(",");
                }
                var unsub = new Array;
                var unsubCurrent = (current.length > 0) ? true : false;
                current.forEach(function (item) {
                    if (item && (historyList.indexOf(item.packageHistoryId) > -1)) {
                        unsub.push({
                            "product_id": item.id,
                            "packageHistoryId": item.packageHistoryId,
                            "effect": overrideEffect ? parseInt(overrideEffect) : item.unsub_effect,
                            "current": unsubCurrent
                        });
                        historyList[historyList.indexOf(item.packageHistoryId)] = undefined;
                    }
                });

                // for general unsubscribe future addon
                future.forEach(function (item) {
                    if (item && (historyList.indexOf(item.packageHistoryId) > -1)) {
                        unsub.push({
                            "product_id": item.id,
                            "packageHistoryId": item.packageHistoryId,
                            "effect": overrideEffect ? parseInt(overrideEffect) : item.unsub_effect,
                            "current": false
                        });
                    }
                });

                ec.addonUnsubscribe(number, cache.serviceInstanceNumber, unsub,
                    cache.billing_cycle, function (err, result) {
                        ec.getCustomerDetailsNumber(cache.prefix, cache.number, true, function () {
                            var response = {
                                "args": {"msisdn": cache.number, "serviceInstanceNumber": cache.serviceInstanceNumber},
                                "result": result, "unsub": unsub
                            };
                            callback(err, response);
                        });
                    });
            } else {
                callback(new Error("Unknown cmd"));
            }
        });
    }
}
