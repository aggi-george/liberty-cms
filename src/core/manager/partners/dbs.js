'use strict';
let Bluebird = require('bluebird');
let ecommManager = require('../ecomm/ecommManager');
let ec = require('../../../../lib/elitecore');
let packageManager = require('../packages/packageManager');
let BaseError = require('../../errors/baseError')
class DBS {

  static getPaymentUrl (data) {
    return ecommManager.getPaymentUrl(data)
      .then(result => {
        if (result && result.code == -1) {
          const status = result.status ? result.status : 'GET_PAYMENT_URL_ECOMM_FAILURE';
          throw new BaseError("Get Payment url API ecomm failed", status);
        }
        return result;
      })
  }

  static addonSubscribeWithoutCharging(addonId, serviceInstanceNumber) {

    let getCachePromise = Bluebird.promisify(ec.getCustomerDetailsServiceInstance);

    return getCachePromise(serviceInstanceNumber, false)
      .then(cache => {
        let { id, recurrent, sub_effect } = ec.findPackage(ec.packages(), addonId);
        let { number, billing_cycle } = cache;
        let addonSubscribePromise = Bluebird.promisify(ec.addonSubscribe);

        return addonSubscribePromise(number, serviceInstanceNumber, [{
            "product_id": id,
            "recurrent": recurrent,
            "effect": sub_effect
          }], billing_cycle)
          .then(res => {
            return getCachePromise(serviceInstanceNumber, true);
          });
      })
  }
}

module.exports = DBS;