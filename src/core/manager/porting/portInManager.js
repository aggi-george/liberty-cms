var dateformat = require('dateformat');
var async = require('async');
var request = require('request');
var config = require('../../../../config');
var common = require('../../../../lib/common');
var db = require('../../../../lib/db_handler');
var ec = require('../../../../lib/elitecore');

var profileManager = require('../profile/profileManager');
var elitecoreUserService = require('../../../../lib/manager/ec/elitecoreUserService');
var elitecorePortInService = require('../../../../lib/manager/ec/elitecorePortInService');
var elitecoreConnector = require('../../../../lib/manager/ec/elitecoreConnector');
var hlrConnector = require('../../../core/manager/hlr/connector');
var constants = require('../../../core/constants');
var appManager = require(__manager+ '/app/appManager');
var fileManager = require("../file/fileManager");
const accountActions = require(`${global.__manager}/account/actions`);
const notificationSend = require(__core + '/manager/notifications/send');
const terminationManager = require('../../../core/manager/account/terminationManager');
const schedulerManager = require('../scheduler/schedulerManager');

const promotionsManager = require('../promotions/promotionsManager');
const configManager = require('../config/configManager');
const packageManager = require('../packages/packageManager');
const dateTimeLib = require('../../../utils/dateTime');

const portInAndTerminationDatesDiff = 4

var BaseError = require('../../errors/baseError');

var log = config.LOGSENABLED;

var INACTIVE_REQUESTS_DATE = "2038-01-01 00:00:00";

var NETWORKS = [
    {code: "001", name: "M1"},
    {code: "002", name: "StarHub"},
    {code: "003", name: "Singtel"}
]

var TIME_SHIFT = {
    "65": 8 * 60 * 60 * 1000
}

var holidays = [
    // 2017
    new Date("2017-01-01"),
    new Date("2017-01-28"),
    new Date("2017-01-30"),
    new Date("2017-04-14"),
    new Date("2017-05-01"),
    new Date("2017-05-10"),
    new Date("2017-06-25"),
    new Date("2017-06-26"),
    new Date("2017-08-09"),
    new Date("2017-09-01"),
    new Date("2017-10-18"),
    new Date("2017-12-25"),
    // 2018
    new Date("2018-01-01"),
    new Date("2018-02-16"),
    new Date("2018-02-17"),
    new Date("2018-03-30"),
    new Date("2018-05-01"),
    new Date("2018-05-29"),
    new Date("2018-06-15"),
    new Date("2018-08-09"),
    new Date("2018-08-22"),
    new Date("2018-11-06"),
    new Date("2018-12-25")
]

const bluebird = require("bluebird");

module.exports = {

    /**
     *
     */
    getAvailablePortInDates: function (prefix, startDate) {
        var dates = [];

        if (startDate) {
            // convert to local time
            startDate = new Date(startDate.getTime() + 8 * 60 * 60 * 1000);

            // reset to proper port-in time 00.30 AM -> local 08.30 PM
            startDate = new Date(startDate.setHours(0, 30, 0, 0));

            // first day is not included in result
            // so to avoid that move start date to -1 day
            startDate = new Date(startDate.getTime() - 24 * 60 * 60 * 1000);
        }

        var i = 0;
        var currentDateMs = (startDate ? new Date(startDate) : new Date()).setHours(0, 0, 0, 0);
        var aStartShift = 30 * 60 * 1000;
        var aDayMs = 24 * 60 * 60 * 1000;

        while (dates.length < 15) {
            i++;

            var date = new Date(currentDateMs + i * aDayMs);
            var dateFoValidation = new Date(date.getTime());

            dateFoValidation.setHours(0, 0, 0, 0);
            var dayOfWeek = dateFoValidation.getDay();
            var weekend = (dayOfWeek == 0 || dayOfWeek == 6);

            var invalid = holidays.some((date) => {
                if (dateFoValidation.getTime() == date.getTime() ||
                    dateFoValidation.getTime() == date.getTime() - 24 * 60 * 60 * 1000) {
                    return true;
                }
            })

            if (!invalid && !weekend) {
                dates.push({date: new Date(date.getTime() + aStartShift).toISOString()});
            }
        }

        return dates;
    },

    /**
     *
     */
    loadActiveRequestByServiceInstanceNumber: function (sin, options, callback) {
        if (!callback) callback = () => {
        }
        if (!options) options = {};

        elitecoreUserService.loadUserInfoByServiceInstanceNumber(sin, function (err, cache) {
            if (err) {
                return callback(err);
            }
            if (!cache) {
                return callback(new BaseError("Customer not found", "ERROR_CUSTOMER_NOT_FOUND"));
            }

            module.exports.loadActiveRequest(cache.prefix, cache.number, options.key, callback);
        });
    },

    /**
     *
     */
    loadActiveRequest: function (prefix, number, executionKey, callback) {

        if (typeof(profileManager.loadProfileDetails) != 'function') {    // remove this if issue is fixed
            common.error("portInManager loadActiveRequest", "profileManager.loadProfileDetails is not a function");
            profileManager = require('../profile/profileManager');
        }

        var availablePortInDates = module.exports.getAvailablePortInDates(prefix);

        profileManager.loadProfileDetails(prefix, number, executionKey, function (err, profile) {
            if (err) {
                return callback(err);
            }

            var selectQuery = "SELECT * FROM customerPortIn WHERE profile_id=" + db.escape(profile.profileId) +
                " AND status IN (" + db.escape("WAITING") + "," + db.escape("IN_PROGRESS") + ","
                + db.escape("WAITING_RETRY") + "," + db.escape("WAITING_APPROVE") + "," + db.escape("DONOR_APPROVED") + ")"
                + " ORDER BY id DESC";

            db.query_err(selectQuery, function (err, rows) {
                if (err) {
                    if (log) common.error("PortinManager", "Failed to load port in requests, err=" + err.message);
                    return callback(new Error("Bonus is not added into db", "DB_ERROR"));
                }

                var authFormUrl = "https://www.circles.life/ownershipform.pdf";

                ec.getCustomerDetailsNumber(prefix, number, false, (err, cache) => {
                    if (!err) {
                         return terminationManager.getScheduledTerminationBySIN(cache.serviceInstanceNumber)
                            .then(terminateResult => {
                                const filteredPortInDates = [];
                                if (terminateResult && terminateResult.start) {
                                    availablePortInDates.forEach(val => {
                                        const portInDate = new Date(val.date);
                                        const terminationDate = new Date(terminateResult.start);

                                        const diff = dateTimeLib.getDaysDiff(portInDate, terminationDate);

                                        if (diff > portInAndTerminationDatesDiff) {
                                            filteredPortInDates.push(val);
                                        } 
                                    })
                                }
                                if (!rows || rows.length < 1) {
                                    return callback(undefined, {
                                        country_code: 65,
                                        status: "NOT_FOUND",
                                        donor_networks: NETWORKS,
                                        auth_form_url: authFormUrl,
                                        start_dates: filteredPortInDates.length ? filteredPortInDates : availablePortInDates
                                    });
                                }
                
                                var start_date = rows[0].start_ts && new Date(rows[0].start_ts).getTime() > 0
                                    ? new Date(rows[0].start_ts).toISOString() : undefined;
                                var scheduled = false;
                                if (start_date && new Date(start_date).getTime() < new Date(INACTIVE_REQUESTS_DATE).getTime()) {
                                    scheduled = true;
                                }
                
                                var result = {
                                    requestId: rows[0].id,
                                    country_code: 65,
                                    status: rows[0].status,
                                    porting_number: rows[0].number,
                                    donor_network: {
                                        name: rows[0].donor_network,
                                        code: rows[0].donor_network_code
                                    },
                                    auth_form_url: authFormUrl,
                                    customer_id_number: rows[0].customer_id_number,
                                    owner_id_number: rows[0].owner_id_number,
                                    start_date: start_date,
                                    scheduled: scheduled
                                }
                
                                if (!scheduled && availablePortInDates && availablePortInDates[0]) {
                                    result.available_activation_date = availablePortInDates[0].date;
                                }
                
                                return callback(undefined, result);
                            });
                    }
                })

                
            });
        });
    },

    /**
     *
     */
    loadRequestReportByType: function (options, startDate, endDate, callback) {
        if (!callback) callback = () => {
        }

        if (!startDate || !endDate) {
            return callback(new BaseError("No startDate or endDate"));
        }

        var filterByIntention = options.filterByIntention;
        var dateFilterColumn = filterByIntention ? "intention_added_ts" : "start_ts";
        var orderSourceColumn = filterByIntention ? "intention_order_source" : "order_source";

        // correct SGT time

        if (startDate) {
            startDate.setHours(0, 0, 0, 0);
            startDate = new Date(startDate.getTime() - 8 * 60 * 60 * 1000);
        }
        if (endDate) {
            endDate.setHours(0, 0, 0, 0);
            endDate = new Date(endDate.getTime() - 8 * 60 * 60 * 1000);
        }

        var report = [];
        var dates = [];

        var errors = [
            {status: "ERROR_PREPAID"},
            {status: "ERROR_ID_MISTMATCH"},
            {status: "ERROR_PENDING_ORDER"},
            {status: "ERROR_SUSPENDED"},
            {status: "ERROR_INACTIVE"},
            {status: "ERROR_INCORRECT_TRANSFER_TIME"},
            {status: "ERROR_DONOR_TELCO"},
            {status: "ERROR_MSISDN_NOT_EXISTS"},
            {status: "ERROR_DOC_REJECTED"},
            {status: "ERROR_TERMINATED_ACCOUNT"},
            {status: "ERROR_OTHER"}
        ];

        var orderSources = []
        var reportSourceItem = {status: "ORDER_SOURCE", orderSources: orderSources};
        var activeSourceItem = {status: "PENDING"};

        if (filterByIntention) {
            report.push(activeSourceItem);
        } else {
            report.push({status: "WAITING"});
            report.push({status: "WAITING_APPROVE"});
            report.push({status: "WAITING_RETRY"});
            report.push({status: "IN_PROGRESS"});
            report.push({status: "DONOR_APPROVED"});
        }
        report.push({status: "FAILED", errors: errors});
        report.push({status: "CANCELED"});
        report.push({status: "DONE"});
        if (filterByIntention) {
            report.push({status: ""});
            report.push(reportSourceItem);
        }

        var currentDate = new Date(startDate);
        while (currentDate.getTime() < endDate.getTime()) {
            currentDate = new Date(currentDate.getTime() + 24 * 60 * 60 * 1000);
            dates.push(currentDate);

            var dateKey = currentDate.toISOString();
            report.forEach((item) => {
                item[dateKey] = 0;
            });
            errors.forEach((item) => {
                item[dateKey] = 0;
            });
        }

        module.exports.loadRequest(undefined, options, (err, result) => {
                if (err) {
                    return callback(err);
                }

                result.data.forEach((item)=> {
                    var foundItem;

                    if (filterByIntention
                        && (item.status == 'WAITING'
                        || item.status == 'WAITING_APPROVE'
                        || item.status == 'WAITING_RETRY'
                        || item.status == 'IN_PROGRESS'
                        || item.status == 'DONOR_APPROVED')) {
                        foundItem = activeSourceItem;
                    } else {
                        report.some((reportItem) => {
                            if (item.status === reportItem.status) {
                                foundItem = reportItem;
                                return true;
                            }
                        })
                    }

                    if (foundItem) {
                        var date = new Date(item[dateFilterColumn]);
                        var foundDate;
                        dates.some((reportDate) => {
                            if (date.getTime() < reportDate.getTime()) {
                                foundDate = reportDate;
                                return true;
                            }
                        });

                        if (foundDate) {
                            var dateKey = foundDate.toISOString();
                            foundItem[dateKey] = foundItem[dateKey] ? foundItem[dateKey] + 1 : 1;
                            foundItem.count = foundItem.count ? foundItem.count + 1 : 1;

                            if (foundItem.status === 'FAILED') {
                                if (!item.error_status) {
                                    item.error_status = "ERROR_OTHER";
                                }

                                var errStatus = item.error_status;
                                var foundError;
                                foundItem.errors.some((reportError) => {
                                    if (reportError.status == item.error_status) {
                                        foundError = reportError;
                                        return true;
                                    }
                                });

                                if (foundError) {
                                    foundError[dateKey] = foundError[dateKey] ? foundError[dateKey] + 1 : 1;
                                    foundError.count = foundError.count ? foundError.count + 1 : 1;
                                } else {
                                    foundItem.errors.push({
                                        status: item.error_status,
                                        count: 1
                                    })
                                }
                            }

                            var orderSource = item[orderSourceColumn];
                            if (orderSource) {
                                reportSourceItem[dateKey] = reportSourceItem[dateKey] ? reportSourceItem[dateKey] + 1 : 1;
                                reportSourceItem.count = reportSourceItem.count ? reportSourceItem.count + 1 : 1;

                                var foundSourceItem;
                                orderSources.some((reportSourceItem) => {
                                    if (orderSource === reportSourceItem.status) {
                                        foundSourceItem = reportSourceItem;
                                        return true;
                                    }
                                })
                                if (!foundSourceItem) {
                                    foundSourceItem = {
                                        status: orderSource,
                                        count: 0
                                    }
                                    orderSources.push(foundSourceItem);
                                }
                                foundSourceItem[dateKey] = foundSourceItem[dateKey] ? foundSourceItem[dateKey] + 1 : 1;
                                foundSourceItem.count = foundSourceItem.count ? foundSourceItem.count + 1 : 1;
                            }

                        } else {
                            common.error("PortinManager", "failed to categorize request, item=" + JSON.stringify(item))
                        }
                    } else {
                        common.error("PortinManager", "failed to categorize request, item=" + JSON.stringify(item))
                    }
                });

                var columnLabels = [];
                var columnKeys = [];

                var pastKeys = [];
                var futureKeys = [];
                var todaysKeys = [];

                var todayEnd = new Date(new Date().setHours(16, 0, 0, 0));
                var todayStart = new Date(new Date().setHours(0, 0, 0, 0) - 8 * 60 * 60 * 1000);

                orderSources = orderSources.sort(function (a, b) {
                    return b.status > a.status;
                });

                dates.forEach((date) => {
                    columnLabels.push(new Date(date.getTime() - 16 * 60 * 60 * 1000).toISOString().split("T")[0]);

                    var dateKey = date.toISOString();
                    columnKeys.push(dateKey);

                    if (date.getTime() <= todayStart.getTime()) {
                        pastKeys.push(dateKey);
                    } else if (date.getTime() > todayEnd.getTime()) {
                        futureKeys.push(dateKey);
                    } else {
                        todaysKeys.push(dateKey);
                    }
                });

                callback(undefined, {
                    data: report,
                    errors: errors,
                    orderSources: orderSources,
                    columnLabels: columnLabels,
                    columnKeys: columnKeys,
                    pastKeys: pastKeys,
                    futureKeys: futureKeys,
                    todaysKeys: todaysKeys
                });
            }, undefined, undefined, undefined, undefined,
            undefined, undefined, startDate, endDate);
    },

    /**
     *
     */
    loadRequestWithWarnings: function (callback, limit, offset, orderColumn, orderDirection) {
        module.exports.loadRequest(undefined, {status: "ACTIVE"}, callback, limit, offset,
            undefined, orderColumn, orderDirection, true);
    },

    /**
     *
     */
    loadRecentRequest: function (prefix, number, callback) {
        if (!callback) callback = () => {
        }

        var lastMonth = new Date(new Date() - 30 * 24 * 60 * 60 * 1000);
        var query = "SELECT * FROM customerPortIn WHERE start_ts>" + db.escape(lastMonth)
            + " AND (number=" + db.escape(number) + "OR temp_number=" + db.escape(number) + ") ORDER BY start_ts DESC LIMIT 1";

        db.query_err(query, function (err, result) {
            if (err) {
                common.error("PortinManager", "loadLastRequest: failed to load last port-in request, err=" + err.message);
                return callback(new Error("Bonus is not added into db", "ERROR_SQL"));
            }

            if (!result || !result[0]) {
                return callback();
            }

            var inputType = result[0].number == number ? "PORT_IN_NUMBER" : "TEMP_NUMBER";
            var portinNumber = result[0].number;
            var tempNumber = result[0].temp_number;
            var active = result[0].status == "WAITING" || result[0].status == "WAITING_APPROVE"
                || result[0].status == "WAITING_RETRY" || result[0].status == "IN_PROGRESS"
                || result[0].status == "DONOR_APPROVED";

            callback(undefined, {
                inputType: inputType,
                portinNumber: portinNumber,
                tempNumber: tempNumber,
                active: active,
                request: result[0]
            });
        });
    },

    /**
     *
     */
    loadRequest: function (serviceInstanceNumber, options, callback, limit, offset,
                           filter, orderColumn, orderDirection, warningsOnly, startDate, endDate) {
        if (!callback) callback = () => {
        }
        if (!options) {
            options = {}
        }

        var donorCode = options.donorCode;
        var source = options.source;
        var orderSource = options.orderSource;
        var status = options.status;
        var uniqueCustomer = options.uniqueType == "CUSTOMER";
        var filterByIntention = options.filterByIntention;

        var statuses = "";
        if (status === "ACTIVE") {
            statuses = db.escape("WAITING") + "," + db.escape("WAITING_APPROVE") + ","
            + db.escape("WAITING_RETRY") + "," + db.escape("IN_PROGRESS") + "," + db.escape("DONOR_APPROVED");
        } else if (status === "INACTIVE") {
            statuses = db.escape("WAITING");
        } else if (status === "ALL") {
            statuses = db.escape("WAITING") + "," + db.escape("WAITING_APPROVE") + ","
            + db.escape("WAITING_RETRY") + "," + db.escape("IN_PROGRESS") + "," + db.escape("DONOR_APPROVED")
            + "," + db.escape("FAILED") + "," + db.escape("DONE") + "," + db.escape("CANCELED");
        } else {
            statuses = db.escape(status);
        }

        if (orderColumn == "UPDATE_DATE") {
            orderColumn = "status_update_ts";
        } else if (orderColumn == "START_DATE") {
            orderColumn = "start_ts";
        } else if (orderColumn == "WARNING_DATE") {
            orderColumn = "warning_after_ts";
        } else {
            orderColumn = "id";
        }
        if (!orderDirection) {
            orderDirection = "DESC";
        }

        var selectQueryWhereSectionL1 = "cp0.status IN (" + statuses + ")" +
            (status === "INACTIVE"
                ? " AND (start_ts = '0000-00-00  00:00:00' OR start_ts>= " + db.escape(new Date(INACTIVE_REQUESTS_DATE)) + ")"
                : " AND start_ts< " + db.escape(new Date(INACTIVE_REQUESTS_DATE))) +
            (warningsOnly ? " AND warning_after_ts < " + db.escape(new Date()) : "") +
            (donorCode ? " AND cp0.donor_network_code=" + db.escape(donorCode) : "") +
            (uniqueCustomer ? " AND cp0.old=0" : "") +
            (serviceInstanceNumber ? " AND customerProfile.service_instance_number=" + db.escape(serviceInstanceNumber) : "") +
            (filter ? " AND (customerProfile.service_instance_number LIKE " + db.escape("%" + filter + "%") +
            " OR cp0.donor_network LIKE " + db.escape("%" + filter + "%") +
            " OR cp0.donor_network_code LIKE " + db.escape("%" + filter + "%") +
            " OR cp0.number LIKE " + db.escape("%" + filter + "%") +
            " OR cp0.temp_number LIKE " + db.escape("%" + filter + "%") +
            " OR cp0.source LIKE " + db.escape("%" + filter + "%") +
            " OR cp0.status LIKE " + db.escape("%" + filter + "%") +
            " OR cp0.order_reference_number LIKE " + db.escape("%" + filter + "%") +
            " OR cp0.error_status LIKE " + db.escape("%" + filter + "%") + ")"
                : "");

        var dateFilterColumn = filterByIntention ? "intention_added_ts" : "start_ts";
        var sourceColumn = filterByIntention ? "intention_source" : "source";
        var orderSourceColumn = filterByIntention ? "intention_order_source" : "order_source";

        var selectQueryWhereSectionL2 = "1=1 " +
            (source ? " AND " + sourceColumn + "=" + db.escape(source) : "") +
            (orderSource ? " AND " + orderSourceColumn + "=" + db.escape(orderSource) : "") +
            (startDate ? " AND " + dateFilterColumn + " > " + db.escape(startDate) : "") +
            (endDate ? " AND " + dateFilterColumn + " < " + db.escape(endDate) : "")

        var selectExtraColumns = (filterByIntention ? ", (SELECT added_ts from customerPortIn c1 WHERE cp0.profile_id=c1.profile_id " +
            "ORDER BY added_ts DESC LIMIT 1) as intention_added_ts" : "") +
            (filterByIntention ? ", (SELECT source from customerPortIn c1 WHERE cp0.profile_id=c1.profile_id " +
            "ORDER BY added_ts DESC LIMIT 1) as intention_source" : "") +
            (filterByIntention ? ", (SELECT order_source from customerPortIn c1 WHERE cp0.profile_id=c1.profile_id " +
            "ORDER BY added_ts DESC LIMIT 1) as intention_order_source" : "");

        var selectQuery = "SELECT * FROM (SELECT cp0.*, customerProfile.service_instance_number " + selectExtraColumns +
            " FROM customerPortIn cp0 LEFT OUTER JOIN customerProfile ON (customerProfile.id = cp0.profile_id) " +
            " WHERE " + selectQueryWhereSectionL1 +
            " ORDER BY " + orderColumn + " " + orderDirection +
            (limit > 0 ? " LIMIT " + db.escape(limit) : "") +
            (offset > 0 ? " OFFSET " + db.escape(offset) : "") + ") " +
            "as cpL2 WHERE " + selectQueryWhereSectionL2 + " ORDER BY cpL2." + orderColumn + " " + orderDirection;

        db.query_err(selectQuery, function (err, rows) {
            if (err) {
                common.error("PortinManager", "Failed to load port-in requests, err=" + err.message);
                return callback(err);
            }

            var selectCount = "SELECT count(*) as count FROM (SELECT cp0.id, cp0.added_ts, cp0.start_ts ," +
                " cp0.source , cp0.order_source " + selectExtraColumns +
                " FROM customerPortIn cp0 LEFT OUTER JOIN customerProfile ON (customerProfile.id = cp0.profile_id) " +
                " WHERE " + selectQueryWhereSectionL1 + ") as cpL2 " +
                " WHERE " + selectQueryWhereSectionL2;

            db.query_err(selectCount, function (err, countResult) {
                if (err) {
                    common.error("PortinManager", "Failed to load port-in requests count, err=" + err.message + " " + selectCount);
                    return callback(err);
                }

                var totalCount = countResult && countResult.length > 0
                    ? countResult[0].count : 0;

                if (!rows || !rows.length) {
                    return callback(undefined, {
                        totalCount: totalCount,
                        data: []
                    });
                }

                callback(undefined, {
                    totalCount: totalCount,
                    data: rows
                });
            });
        });
    },

    putCorporateRequest: function(data, username, ip, callback){
        hlrConnector.MNPCreatePortInOrder(data, function(err, results){
            db.weblog.insert({
                "ts" : new Date().getTime(),
                "ip" : ip,
                "type" : "corporatePortin",
                "tempNumber" : data.tempNumber ? data.tempNumber : "N/A",
                "portinNumber" : data.portinNumber ? data.portinNumber : "N/A",
                "action" : "createCorporatePortin",
                "username" : username,
                "results" : results,
                "error" : err ? err : null
            });
            callback(err, results);
        });
    },

    /**
     *
     */
    putRequest: function (prefix, number, portInPrefix, portingNumber, donorNetworkCode, startDate, source, options, callback) {
        if (!callback) callback = () => {
        }
        if (!options) options = {};

        var executionKey = (!options.executionKey) ? config.NOTIFICATION_KEY : options.executionKey;
        var initiator = (!options.initiator) ? "[CMS][INTERNAL]" : options.initiator;

        var status = options.alreadyScheduled ? "IN_PROGRESS" : options.needDocUpdate ? "WAITING_APPROVE" : "WAITING";
        var docUpdateStatus = options.needDocUpdate ? "PENDING" : "NOT_REQUIRED";
        var warning = startDate && !options.inactivePortIn ? getWarningInfo(status, startDate) : undefined;

        // in case of port-in requests initiated from eComm approve will be received next day only
        // port-in starts on the next day after delivery, therefore, approve will be sent on that day
        if (options.alreadyScheduled && warning && warning.type === "WAITING_DONOR_APPROVE" && startDate) {
            warning.after = new Date(startDate.getTime() + 2 * 60 * 60 * 1000);
        }

        var obj = {
            type: "ADD_REQUEST",
            initiator: initiator,
            executionKey: executionKey,
            params: {
                number: number,
                portInNumber: portingNumber,
                status: status,
                donorNetworkCode: donorNetworkCode,
                startDate: startDate,
                warning: warning,
                needDocUpdate: options.needDocUpdate,
                docUpdateStatus: docUpdateStatus,
                fileIdFront: options.fileIdFront,
                fileIdBack: options.fileIdBack,
                fileAuthForm: options.fileAuthForm,
                alreadyScheduled: options.alreadyScheduled,
                planName: options.planName,
                inactivePortIn: options.inactivePortIn
            },
            changes: {}
        }

        var handleResult = (err, result) => {
            obj.status = err ? (err.status ? err.status : "ERROR") : "OK";
            obj.errorMessage = err ? err.message : undefined;
            obj.ts = new Date().getTime();

            if (log) common.log("PortinManager", "putRequest: save log");
            db.notifications_port_in.insert(obj, function () {
                if (err) common.error("PortinManager", "putRequest: save log error, error=" + err.message);
                callback(err, result);
            });
        }

        if (typeof(profileManager.loadProfileDetails) != 'function') {    // remove this if issue is fixed
            common.error("portInManager putRequest", "profileManager.loadProfileDetails is not a function");
            profileManager = require('../profile/profileManager');
        }
        profileManager.loadProfileDetails(prefix, number, executionKey, function (err, profile) {
            if (err) {
                return handleResult(err);
            }

            var networkName;
            NETWORKS.forEach(function (item) {
                if (item.code === donorNetworkCode) {
                    networkName = item.name;
                }
            });

            obj.params.networkName = networkName;
            obj.serviceInstanceNumber = profile.serviceInstanceNumber;

            if (!networkName) {
                return handleResult(new Error("Donor network is not found"));
            }

            var isNum = /^\d+$/.test(portingNumber);
            if (!isNum || !portingNumber || portingNumber.length < 5 || portingNumber.length > 12) {
                common.error("ProfileManager", "Invalid number format, portingNumber=" + portingNumber);
                return handleResult(new BaseError("Invalid number format", "ERROR_INVALID_NUMBER_FORMAT"));
            }

            var sendNotification = (prevStatus, newStatus, errorReason, callback) => {
                sendStatusUpdateNotification(prevStatus, newStatus, prefix, number,
                    portingNumber, networkName, startDate, errorReason, undefined, executionKey, function (err, result) {
                        if (err) {
                            common.error("PortinManager", "Failed to send notification, err=" + err.message);
                        }

                        if (callback) callback(err, result);
                    });
            }

            module.exports.loadActiveRequest(prefix, number, executionKey, (err, activeItem) => {
                if (err) {
                    return handleResult(err);
                }

                delete activeItem.start_dates;
                delete activeItem.donor_networks;
                delete activeItem.auth_form_url;
                obj.activeRequest = activeItem;

                if (activeItem.status !== "NOT_FOUND") {
                    if (log) common.error("ProfileManager", "Already has active port in request");
                    return handleResult(new BaseError("Already has active port in request", "ERROR_PORT_IN_PENDING"));
                }

                var insertQuery = "INSERT INTO customerPortIn (profile_id, status, number, temp_number,"
                    + " source, donor_network_code, donor_network, warning_type, warning_after_ts, status_update_ts, "
                    + " added_ts, start_ts, doc_update_status, file_id_front, file_id_back, file_auth_form, "
                    + " order_reference_number, owner_name, owner_id_type, owner_id_number, planName)"
                    + " VALUES ("
                    + db.escape(profile.profileId) + ","
                    + db.escape(status) + ","
                    + db.escape(portingNumber) + ","
                    + db.escape(number) + ","
                    + db.escape(source) + ","
                    + db.escape(donorNetworkCode) + ","
                    + db.escape(networkName) + ","
                    + (warning ? db.escape(warning.type) : "NULL") + ","
                    + (warning ? db.escape(warning.after) : "NULL") + ","
                    + db.escape(new Date()) + ","
                    + (options.checkoutDate ? db.escape(options.checkoutDate) : db.escape(new Date())) + ","
                    + (options.inactivePortIn ? db.escape(new Date(INACTIVE_REQUESTS_DATE)) : db.escape(startDate)) + ","
                    + db.escape(docUpdateStatus) + ","
                    + (options.fileIdFront ? db.escape(options.fileIdFront) : "NULL") + ","
                    + (options.fileIdBack ? db.escape(options.fileIdBack) : "NULL") + ","
                    + (options.fileAuthForm ? db.escape(options.fileAuthForm) : "NULL") + ","
                    + (options.orderReferenceNumber ? db.escape(options.orderReferenceNumber) : "NULL") + ","
                    + (options.registeredOwnerName ? db.escape(options.registeredOwnerName) : "NULL") + ","
                    + (options.registeredOwnerIdType ? db.escape(options.registeredOwnerIdType) : "NULL") + ","
                    + (options.registeredOwnerIdNumber ? db.escape(options.registeredOwnerIdNumber) : "NULL") + ","
                    + (options.planName && options.planName != 'DEFAULT' ? db.escape(options.planName) : "NULL") + ")";

                db.query_err(insertQuery, (err, result) => {
                    if (err) {
                        common.error("ProfileManager", "Can not insert Port-In request, error=" + err.message);
                        return handleResult(new BaseError("Failed to register Port-In request (" + err.message + ")",
                            "ERROR_DB_ERROR"));
                    }

                    var requestId = result.insertId;

                    var insertQuery = "UPDATE customerPortIn SET old=1 WHERE id<>" + db.escape(requestId)
                        + " AND profile_id=" + db.escape(profile.profileId);

                    db.query_err(insertQuery, (err) => {
                        if (err) {
                            common.error("ProfileManager", "Can not update old port-in request, error=" + err.message);
                            // do not do anything, minor error
                        }

                        if (options.inactivePortIn) {
                            // inactive request does not require port-in start event
                            return handleResult(undefined, {requestId: requestId, inactive: true});
                        }

                        if (options.alreadyScheduled) {
                            sendNotification(undefined, status);
                            return handleResult(undefined, {requestId: requestId});
                        }

                        if (options.planName !== 'CirclesSwitch') {                       
                            const data = {
                                prefix,
                                number,
                                orderReferenceNumber: options.orderReferenceNumber ? 
                                    options.orderReferenceNumber : null,
                                startDate,
                                requestId,    
                            }
                            checkAndAddActivePromo(data, (error, promoLogId) => {
                                if (error) {
                                    return handleResult(error);
                                }
                                const updateQuery = `UPDATE customerPortIn SET promoLogId=${ 
                                    promoLogId ? db.escape(promoLogId) : null },
                                start_ts=${ db.escape(startDate) } WHERE id= ${ db.escape(requestId) }`;

                                db.query_err(updateQuery, (err, rows) => {
                                    if (err) {
                                        common.error("PortinManager", `Failed to update request status, 
                                        err=${ err.message }`);
                                    } else {
                                        schedulePortIn(profile.serviceInstanceNumber, 
                                            prefix, number, result.insertId, portingNumber,
                                            donorNetworkCode, startDate, initiator, (err, result) => {
                                                obj.changes.schedulePortInResult = result;
                
                                                if (err) {
                
                                                    var newStatus = "FAILED";
                                                    var errorStatus = err.status;
                                                    var updateQuery = "UPDATE customerPortIn SET status=" + db.escape(newStatus) +
                                                        ", error_status=" + db.escape(errorStatus) +
                                                        ", status_update_ts=" + db.escape(new Date()) +
                                                        " WHERE id=" + db.escape(requestId);
                
                                                    db.query_err(updateQuery, (err, rows) => {
                                                        if (err) {
                                                            common.error("PortinManager", "Failed to update request status, err=" + err.message);
                                                        }
                                                    });
                
                                                    sendNotification(status, newStatus, errorStatus);
                                                    return handleResult(new BaseError("Failed to schedule Port-In request ("
                                                    + err.message + ")", "ERROR_SCHEDULER"));
                                                    
                                                }
                                                sendNotification(undefined, status);
                                                return handleResult(undefined, {requestId: requestId});
                                            });
                                    }
                                })
                            })
                        }else {
                            schedulePortIn(profile.serviceInstanceNumber, prefix, number, 
                                result.insertId, portingNumber,
                                donorNetworkCode, startDate, initiator, (err, result) => {
                                    obj.changes.schedulePortInResult = result;
    
                                    if (err) {
    
                                        var newStatus = "FAILED";
                                        var errorStatus = err.status;
                                        var updateQuery = "UPDATE customerPortIn SET status=" + db.escape(newStatus) +
                                            ", error_status=" + db.escape(errorStatus) +
                                            ", status_update_ts=" + db.escape(new Date()) +
                                            " WHERE id=" + db.escape(requestId);
    
                                        db.query_err(updateQuery, (err, rows) => {
                                            if (err) {
                                                common.error("PortinManager", "Failed to update request status, err=" + err.message);
                                            }
                                        });
                                        sendNotification(status, newStatus, errorStatus);
                                        return handleResult(new BaseError("Failed to schedule Port-In request ("
                                        + err.message + ")", "ERROR_SCHEDULER"));
                                    }
                                    sendNotification(undefined, status);
                                    return handleResult(undefined, {requestId: requestId});
                                });
                        }
                    });
                });
            });
        });
    },

    /**
     *
     */
    cancelRequestByServiceInstanceNumber: (sin, options, callback) => {
        if (!callback) callback = () => {
        }
        if (!options) options = {};

        module.exports.loadActiveRequestByServiceInstanceNumber(sin, options, function (err, request) {
            if (err) {
                return callback(err);
            }
            if (!request || request.status == "NOT_FOUND") {
                return callback(new BaseError("Active port-in request not found", "ERROR_NO_ACTIVE_PORT_IN"));
            }

            elitecoreUserService.loadUserInfoByServiceInstanceNumber(sin, function (err, cache) {
                if (err) {
                    return callback(err);
                }
                if (!cache) {
                    return callback(new BaseError("Customer not found", "ERROR_CUSTOMER_NOT_FOUND"));
                }

                module.exports.cancelRequest(cache.prefix, cache.number, request.country_code,
                    request.porting_number, callback, options.key, options.reason, options.initiator, options.onlyWaiting);
            });
        });
    },

    /**
     *
     */
    cancelRequest: (prefix, number, portInPrefix, portInNumber,
                    callback, executionKey, reason, initiator, onlyWaiting) => {
        if (!executionKey) executionKey = config.NOTIFICATION_KEY;
        if (!initiator) initiator = "[CMS][INTERNAL]";

        var obj = {
            type: "CANCEL_REQUEST",
            initiator: initiator,
            executionKey: executionKey,
            params: {
                number: number,
                portInNumber: portInNumber,
                reason: reason
            },
            changes: {}
        }

        var handleResult = (err, result) => {
            obj.status = err ? (err.status ? err.status : "ERROR") : "OK";
            obj.errorMessage = err ? err.message : undefined;
            obj.ts = new Date().getTime();

            if (log) common.log("PortinManager", "cancelRequest: save log");
            db.notifications_port_in.insert(obj, function (errLog) {
                if (errLog) common.error("PortinManager", "cancelRequest: save log error, error=" + errLog.message);
                if (callback) callback(err, result);
            });
        }

        module.exports.loadActiveRequest(prefix, number, executionKey, (err, activeItem) => {
            if (err) {
                return handleResult(err);
            }

            delete activeItem.start_dates;
            delete activeItem.donor_networks;
            delete activeItem.auth_form_url;
            obj.activeRequest = activeItem;
            obj.serviceInstanceNumber = activeItem.serviceInstanceNumber;

            if (activeItem && activeItem.status !== "NOT_FOUND") {
                if (onlyWaiting && activeItem.status != "WAITING" && activeItem.status != "WAITING_APPROVE") {
                    return handleResult(new BaseError("Can not cancel initiated port-in request", "ERROR_PORT_IN_REQUEST_IN_PROGRESS"));
                }

                if (activeItem.porting_number !== portInNumber) {
                    return handleResult(new Error("Please cancel existing port-in request for " + activeItem.porting_number + " first."));
                }

                module.exports.updateRequestStatus(activeItem.requestId, "CANCELED", false, false, (err, result) => {
                    obj.changes.updateRequestStatusResult = result;
                    handleResult(err, result);
                }, executionKey, reason, initiator);
            } else {
                // cancel port-in on EC side
                // legacy call for old port-in requests

                elitecoreConnector.cancelPortInOrder(portInNumber, "1", function (err, result) {
                    obj.changes.cancelPortInOrderResult = result;

                    sendStatusUpdateNotification("WAITING", "CANCELED", prefix, number,
                        portInNumber, undefined, undefined, undefined, undefined, executionKey, function (err, result) {
                            obj.changes.sendCancelNotificationResult = result;

                            if (err) common.error("PortinManager", "Failed to send notification, err=" + err.message);

                            elitecoreUserService.loadUserInfoByPhoneNumber(number, function (err, cache) {
                                if (cache) obj.serviceInstanceNumber = cache.serviceInstanceNumber;

                                handleResult(err, result);
                            });
                        });
                });
            }
        });
    },

    /**
     *
     */
    onPortInNotification: function (prefix, number, status, postponed, callback, executionKey, providedReason, initiator) {
        if (!executionKey) executionKey = config.NOTIFICATION_KEY;
        if (!initiator) initiator = "[CMS][INTERNAL]";

        var obj = {
            type: "HANDLE_NOTIFICATION",
            initiator: initiator,
            executionKey: executionKey,
            params: {
                portInNumber: number,
                status: status,
                postponed: postponed,
                providedReason: providedReason
            },
            changes: {}
        };

        var handleResult = (err, result) => {
            obj.status = err ? (err.status ? err.status : "ERROR") : "OK";
            obj.errorMessage = err ? err.message : undefined;
            obj.ts = new Date().getTime();

            if (log) common.log("PortinManager", "onPortInNotification, save log");
            db.notifications_port_in.insert(obj, function (errLog) {
                if (errLog) common.error("PortinManager", "onPortInNotification, save log error, error=" + errLog.message);
                if (callback) callback(err, result);
            });
        }

        ec.getCustomerDetailsNumber(prefix, number, false, function (err, cache) {
            if (err) {
                if (log) common.error("PortinManager", "Failed to load customer info");
                return handleResult(new BaseError("Failed to load customer info"), cache);
            }

            var query = "SELECT customerProfile.service_instance_number, customerPortIn.* FROM customerPortIn " +
                " LEFT OUTER JOIN customerProfile ON (customerProfile.id = customerPortIn.profile_id) " +
                " WHERE service_instance_number=" + db.escape(cache.serviceInstanceNumber) +
                " AND status IN ('IN_PROGRESS', 'DONOR_APPROVED', 'WAITING', 'WAITING_APPROVE', 'WAITING_RETRY') LIMIT 1";

            obj.params.status = status;
            obj.serviceInstanceNumber = cache.serviceInstanceNumber;

            db.query_err(query, function (err, rows) {
                if (err) {
                    if (log) common.error("PortinManager", "Failed to load pending request, err=" + err.message);
                    return handleResult(new BaseError("Failed to load current request status", "ERROR_DB"));
                }

                var handleNotification = (reason) => {
                    if (!rows || rows.length < 1) {

                        // legacy check
                        // if is required to notify customer is no port in requests found

                        sendStatusUpdateNotification("IN_PROGRESS", status, cache.prefix, cache.number,
                            number, undefined, undefined, reason, undefined, executionKey, function (err) {
                                if (err) {
                                    common.error("PortinManager", "Failed to send notification, err=" + err.message);
                                }
                            });

                        if (log) common.error("PortinManager", "No port-in requests found");
                        return handleResult(new BaseError("No port-in requests found", "ERROR_NOT_FOUND"));
                    }

                    var activeItem = rows[0];
                    delete activeItem.start_dates;
                    delete activeItem.donor_networks;
                    delete activeItem.auth_form_url;
                    obj.activeRequest = activeItem;

                    // if a FAILED notification is not postponed, means we delayed
                    // the one we got from EC in order to find a port-in failure reason

                    if (status === "FAILED" && !postponed) {
                        schedulePortInFailureCheck(cache.serviceInstanceNumber, cache.prefix, cache.number, activeItem.id,
                            activeItem.number, initiator, function (err, result) {
                                obj.params.status = err ? err.status : "FAILURE_POSTPONED";
                                obj.changes.schedulePortInFailureCheckResult = result; // add result
                                handleResult(err, result);
                            }, executionKey, reason, initiator);
                    } else {
                        module.exports.updateRequestStatus(activeItem.id, status, true, false, function (err, result) {
                            obj.changes.updateRequestStatusResult = result;
                            handleResult(err, result);
                        }, executionKey, reason, initiator);
                    }
                }

                if (!providedReason && status === "FAILED") {

                    // load failure status from EC replica database
                    // status will appear after 1h from the moment a failure notification is received
                    var start_ts = (rows && rows[0] && rows[0].start_ts) ? rows[0].start_ts : undefined;
                    if (!start_ts) {
                        if (log) common.error("PortinManager", "Failed to load pending request, Empty");
                        return handleResult(new BaseError("Failed to load current request status", "ERROR_DB"));
                    }

                    elitecorePortInService.loadPortInFailureReason(number, start_ts, (err, result) => {
                        var reason = result && result.reason ? result.reason : "ERROR_UNKNOWN";
                        obj.params.reason = reason;
                        obj.changes.loadPortInFailureReasonResult = result;

                        handleNotification(reason);
                    });
                } else {
                    handleNotification(providedReason);
                }
            });
        });
    },

    /**
     *
     */
    updateRequestOwner: function (id, ownerName, ownerIdType, ownerIdNumber, callback, executionKey, initiator) {
        if (!executionKey) executionKey = config.NOTIFICATION_KEY;
        if (!initiator) initiator = "[CMS][INTERNAL]";

        var obj = {
            type: "UPDATE_NUMBER_OWNER",
            initiator: initiator,
            executionKey: executionKey,
            params: {
                id: id,
                ownerName: ownerName,
                ownerIdType: ownerIdType,
                ownerIdNumber: ownerIdNumber
            },
            changes: {}
        };

        var handleResult = (err, result) => {
            obj.status = err ? (err.status ? err.status : "ERROR") : "OK";
            obj.errorMessage = err ? err.message : undefined;
            obj.ts = new Date().getTime();

            if (log) common.log("PortinManager", "updateRequestOwner: save log");
            db.notifications_port_in.insert(obj, function (errLog) {
                if (errLog) common.error("PortinManager", "updateRequestOwner: save log error, error=" + errLog.message);
                if (callback) callback(err, result);
            });
        }

        var saveRequestStatus = (cache, request) => {
            var status = "WAITING";
            var warning = getWarningInfo(status, request.startDate);
            var updateQuery = "UPDATE customerPortIn SET status=" + db.escape(status) +
                ", warning_type=" + db.escape(warning.type) +
                ", warning_after_ts=" + db.escape(warning.after) +
                ", status_update_ts=" + db.escape(new Date()) +
                " WHERE id=" + db.escape(id) + " AND status in ('WAITING', 'WAITING_APPROVE')";

            db.query_err(updateQuery, function (err, rows) {
                if (err) {
                    common.error("PortinManager", "updateRequestOwner: failed to update request status, err=" + err.message);
                    return handleResult(new BaseError("Failed to update request status", "DB_ERROR"));
                }

                if (rows.affectedRows == 0) {
                    common.error("PortinManager", "updateRequestOwner: request status has not been updated");
                    return handleResult(new BaseError("Request status has not been updated", "DB_ERROR"));
                }

                obj.params.status = "WAITING";
                handleResult();
            });
        }

        var saveDocApprovalStatus = (cache, request) => {
            var originalCustomerName = cache.customerAccountFullName;
            var originalIdType = cache.customerAccountIdType;
            var originalIdNumber = cache.customerAccountIdNumber;

            obj.params.originalCustomerName = originalCustomerName;
            obj.params.originalIdType = originalIdType;
            obj.params.originalIdNumber = originalIdNumber;

            var updateQuery = "UPDATE customerPortIn SET owner_name=" + db.escape(ownerName) +
                ", owner_id_type=" + db.escape(ownerIdType) + ", owner_id_number=" + db.escape(ownerIdNumber) +
                ", customer_name=" + db.escape(originalCustomerName) + ", customer_id_type=" + db.escape(originalIdType) +
                ", customer_id_number=" + db.escape(originalIdNumber) + ", doc_update_status=" + db.escape("UPDATED")
                + " WHERE id=" + db.escape(id);

            db.query_err(updateQuery, function (err, rows) {
                if (err) {
                    common.error("PortinManager", "updateRequestOwner: failed to update doc status, err=" + err.message);
                    return handleResult(new BaseError("Failed to update doc request status", "DB_ERROR"));
                }

                if (rows.affectedRows == 0) {
                    common.error("PortinManager", "updateRequestOwner: doc status has not been updated");
                    return handleResult(new BaseError("Request doc status has not been updated", "DB_ERROR"));
                }

                obj.params.docUpdateStatus = "UPDATED";
                saveRequestStatus(cache, request);
            });
        }

        var updateNumberOwner = (cache, request) => {
            elitecoreUserService.updateAllCustomerAccountsDetails(cache.customerAccountNumber, {name: ownerName}, (err, result) => {
                obj.changes.updateCustomerNameResult = result;
                if (err) {
                    common.error("PortinManager", "updateRequestOwner: customer name update failed");
                    return handleResult(err);
                }

                elitecoreUserService.updateCustomerID(cache.customerAccountNumber, ownerIdType, ownerIdNumber, (err) => {
                    obj.changes.updateCustomerIDResult = result;
                    if (err) {
                        common.error("PortinManager", "updateRequestOwner: customer id");
                        return handleResult(err);
                    }

                    saveDocApprovalStatus(cache, request);
                });

            }, false, true, true);
        }

        loadRequestById(id, function (err, request) {
            if (err) {
                common.error("PortinManager", "updateRequestOwner: err=" + err.message);
                return handleResult(err);
            }

            if (!request || !request.requestId) {
                common.error("PortinManager", "updateRequestOwner: no requests found");
                return handleResult(new BaseError("No port-in requests found", "ERROR_NOT_FOUND"));
            }

            if (request.status !== "WAITING_APPROVE" && request.status !== "WAITING") {
                common.error("PortinManager", "updateRequestOwner: invalid request status " + request.status + ", can not update");
                return handleResult(new BaseError("Request is in invalid status, details can be updated " +
                "while in WAITING_APPROVE and WAITING states only", "ERROR_INVALID_STATUS"));
            }

            ec.getCustomerDetailsService(request.serviceInstanceNumber, false, function (err, cache) {
                if (err) {
                    common.error("PortinManager", "updateRequestOwner: failed to load customer request " +
                    "status, err=" + err.message);
                    return handleResult(err);
                }

                if (!cache) {
                    common.error("PortinManager", "updateRequestOwner: no customer found");
                    return handleResult(new BaseError("No customer found", "ERROR_NOT_FOUND"));
                }

                obj.serviceInstanceNumber = cache.serviceInstanceNumber;
                if (!cache.billingFullName || !cache.customerAccountFullName
                    || cache.billingFullName.toUpperCase() !== cache.customerAccountFullName.toUpperCase()) {
                    common.error("PortinManager", "updateRequestOwner: account is in inconsistent " +
                    "state, can not update with owner data temporary");
                    return handleResult(new BaseError("Customer information is in inconsistent state", "ERROR_INCONSISTENT_STATE"));
                }

                updateNumberOwner(cache, request);
            });
        });
    },

    /**
     *
     */
    reScheduleActiveRequest: function (prefix, number, newPortInDate, callback, executionKey, initiator, description) {
        module.exports.loadActiveRequest(prefix, number, executionKey, (err, activeItem) => {
            if (err) {
                if (callback) callback(err);
                return;
            }

            if (!activeItem || activeItem.status == "NOT_FOUND") {
                if (callback) callback(new BaseError("No active port-In request found", "ERROR_NOT_ACTIVE_PORT_IN"));
                return;
            }

            module.exports.reScheduleRequest(activeItem.requestId, newPortInDate, false, (err, result) => {
                if (callback) callback(err, result);
            }, executionKey, initiator, description)
        });
    },

    /**
     *
     */
    reScheduleRequest: function (id, newPortInDate, silent, callback, executionKey, initiator, description) {
        if (!description) description = "";
        if (!executionKey) executionKey = config.NOTIFICATION_KEY;
        if (!initiator) initiator = "[CMS][INTERNAL]";

        var obj = {
            type: "UPDATE_START_DATE",
            initiator: initiator,
            executionKey: executionKey,
            params: {
                id: id,
                description: description,
                portInDate: newPortInDate
            },
            changes: {}
        };

        var handleResult = (err, result) => {
            obj.changes = result;
            obj.status = err ? (err.status ? err.status : "ERROR") : "OK";
            obj.errorMessage = err ? err.message : undefined;
            obj.ts = new Date().getTime();

            if (log) common.log("PortinManager", "updateRequestStatus: save log");
            db.notifications_port_in.insert(obj, function (errLog) {
                if (errLog) common.error("PortinManager", "updateRequestStatus: save log error, error=" + errLog.message);
                if (callback) callback(err, result);
            });
        }

        var reschedule = (cache, request) => {
            var requestId = request.requestId;
            var serviceInstanceNumber = request.serviceInstanceNumber;
            var portingNumber = request.portingNumber;
            var donorNetwork = request.donorNetwork;
            var donorNetworkCode = request.donorNetworkCode;

            // get available dates to re-schedule port-in
            var validDates = module.exports.getAvailablePortInDates(cache.prefix, newPortInDate);
            if (!validDates || !validDates[0] || !validDates[0].date) {
                return handleResult(new BaseError("No valid date found", "ERROR_NO_AVAILABLE_DATE"));
            }

            var startDate = new Date(validDates[0].date);
            var rescheduleResult = {
                startDate: startDate
            };

            // cancel all existing port-in events
            updateScheduledPortInAsProcessed(requestId, (err, result) => {
                if (err) rescheduleResult.removeEventError = err.message;
                rescheduleResult.removeEventResult = result;

                var warning = getWarningInfo("WAITING", startDate);
                var updateQuery = "UPDATE customerPortIn SET start_ts=" + db.escape(startDate) +
                    ", warning_type=" + db.escape(warning.type) +
                    ", warning_after_ts=" + db.escape(warning.after) +
                    ", status_update_ts=" + db.escape(new Date()) +
                    " WHERE id=" + db.escape(requestId);

                // update start date in db
                db.query_err(updateQuery, function (err, rows) {
                    if (err) {
                        common.error("PortinManager", "updateRequestStatus: failed to update " +
                        "request start date, err=" + err.message);
                        return handleResult(new Error("Failed to update request start " +
                        "date (" + err.message + ")", "DB_ERROR"));
                    }

                    // create new port-in event
                    schedulePortIn(serviceInstanceNumber, cache.prefix, cache.number, requestId, portingNumber,
                        donorNetworkCode, startDate, initiator, (schedulerErr, result) => {
                            if (schedulerErr) rescheduleResult.createEventError = schedulerErr.message;
                            rescheduleResult.createEventResult = result;

                            if (silent) {
                                return handleResult(undefined, rescheduleResult);
                            }

                            sendNotificationInternal("port_in_request_rescheduled", cache.prefix, cache.number, portingNumber,
                                donorNetwork, startDate, undefined, description, executionKey, (err, result) => {
                                    if (err) rescheduleResult.notificationError = err.message;
                                    rescheduleResult.notificationResult = result;

                                    return handleResult(undefined, rescheduleResult);
                                });
                        });
                });
            }, true, initiator);
        }

        loadRequestById(id, function (err, request) {
            if (err) {
                common.error("PortinManager", "reScheduleRequest: err=" + err.message);
                return handleResult(err);
            }

            if (!request || !request.requestId) {
                common.error("PortinManager", "reScheduleRequest: no requests found");
                return handleResult(new BaseError("No port-in requests found", "ERROR_NOT_FOUND"));
            }

            if (request.status !== "WAITING") {
                common.error("PortinManager", "reScheduleRequest: invalid request status " + request.status + ", can not re-schedule");
                return handleResult(new BaseError("Request is in invalid status (no WAITING status), " +
                "start date can not be updated", "ERROR_INVALID_STATUS"));
            }

            var circlesSwitchMessage = "reScheduleRequest: Customer is on circles switch, hence not re scheduling the request";
            if (request.planName === "CIRCLES_SWITCH") {
                common.log("PortinManager", circlesSwitchMessage);
                return handleResult(undefined, {message: circlesSwitchMessage});
            }

            obj.activeRequest = request;
            obj.serviceInstanceNumber = request.serviceInstanceNumber;

            ec.getCustomerDetailsService(request.serviceInstanceNumber, false, function (err, cache) {
                if (err) {
                    common.error("PortinManager", "reScheduleRequest: failed to load customer request " +
                    "status, err=" + err.message);
                    return handleResult(err);
                }

                if (!cache) {
                    common.error("PortinManager", "reScheduleRequest: no customer found");
                    return handleResult(new BaseError("No customer found", "ERROR_NOT_FOUND"));
                }

                if (cache.serviceInstanceBasePlanName === 'CirclesSwitch') {
                    common.error("PortinManager", "reScheduleRequest: can not reschedule for ");
                    return handleResult(undefined, {status:"IGNORED", reason: "Plan is CirclesSwitch"});
                }

                reschedule(cache, request);
            });
        });
    },

    /**
     *
     */
    updateRequestStatus: function (id, status, donorResponse, canReschedule, callback,
                                   executionKey, reason, initiator, description, portInRecoverType) {
        if (!reason) reason = "";
        if (!executionKey) executionKey = config.NOTIFICATION_KEY;
        if (!initiator) initiator = "[CMS][INTERNAL]";

        var obj = {
            type: "UPDATE_REQUEST_STATUS",
            initiator: initiator,
            executionKey: executionKey,
            params: {
                id: id,
                status: status,
                reason: reason,
                donorResponse: donorResponse,
                canReschedule: canReschedule,
                description: description,
                portInRecoverType: portInRecoverType
            },
            changes: {}
        };

        var key = "port_in_request_id_" + id;

        var handleResult = (err, result) => {
            obj.status = err ? (err.status ? err.status : "ERROR") : "OK";
            obj.errorMessage = err ? err.message : undefined;
            obj.ts = new Date().getTime();

            if (!err || err.status != "ERROR_OPERATION_LOCKED") {
                db.unlockOperation(key);
            }

            if (log) common.log("PortinManager", "updateRequestStatus: save log");
            db.notifications_port_in.insert(obj, function (errLog) {
                if (errLog) common.error("PortinManager", "updateRequestStatus: save log error, error=" + errLog.message);
                if (callback) callback(err, result);
            });
        }

        db.lockOperation(key, {requestId: id, status: status}, 30000, function (err) {
            if (err) {
                common.error("PortinManager", "updateRequestStatus: failed for start operation, err=" + err.message);
                return handleResult(err);
            }

            loadRequestById(id, updateRequestStatus);
        });

        var updateRequestStatus = function (err, request) {
            if (err) {
                common.error("PortinManager", "updateRequestStatus: failed for lead active request, err=" + err.message);
                return handleResult(err);
            }

            if (!request || !request.requestId) {
                common.error("PortinManager", "updateRequestStatus: no requests found");
                return handleResult(new BaseError("No port-in requests found", "ERROR_NOT_FOUND"));
            }

            // log active request
            delete request.start_dates;
            delete request.donor_networks;
            delete request.auth_form_url;
            obj.activeRequest = request;
            obj.serviceInstanceNumber = request.serviceInstanceNumber;

            var requestId = request.requestId;
            var serviceInstanceNumber = request.serviceInstanceNumber;
            var prevStatus = request.status;
            var portingNumber = request.portingNumber;
            var tempNumber = request.tempNumber;
            var donorNetwork = request.donorNetwork;
            var donorNetworkCode = request.donorNetworkCode;
            var startDate = request.startDate;

            var invalidTransition = false;
            if (prevStatus === "WAITING_APPROVE") {
                if (status === "WAITING" || status === "WAITING_RETRY"
                    || status === "CANCELED" || status === "IN_PROGRESS"
                    || status === "FAILED" || status === "DONOR_APPROVED"
                    || status === "DONE") {
                    //correct transition
                } else {
                    invalidTransition = true;
                }
            } else if (prevStatus === "WAITING") {
                if (status === "CANCELED" || status === "IN_PROGRESS"
                    || status === "FAILED" || status === "DONOR_APPROVED") {
                    //correct transition
                } else {
                    invalidTransition = true;
                }
            } else if (prevStatus === "WAITING_RETRY") {
                if (status === "CANCELED" || status === "IN_PROGRESS"
                    || status === "FAILED" || status === "DONOR_APPROVED"
                    || status === "WAITING_RETRY" || status === "DONE") {
                    //correct transition
                } else {
                    invalidTransition = true;
                }
            } else if (prevStatus === "IN_PROGRESS") {
                if (status === "FAILED" || status === "DONE"
                    || status === "CANCELED" || status === "WAITING_RETRY"
                    || status === "DONOR_APPROVED") {
                    //correct transition
                } else {
                    invalidTransition = true;
                }
            } else if (prevStatus === "DONOR_APPROVED") {
                if (status === "CANCELED" || status === "FAILED"
                    || status === "DONE" || status === "DONOR_APPROVED"
                    || status === "WAITING_RETRY") {
                    //correct transition
                } else {
                    invalidTransition = true;
                }
            } else if (prevStatus === "FAILED") {
                if (status === "WAITING_RETRY") {
                    //correct transition, required for recovery
                } else if (status === "DONE") {
                    //correct transition, required for recovery
                    portInRecoverType = "MNP";
                } else {
                    invalidTransition = true;
                }
            } else {
                invalidTransition = true;
            }

            if (initiator && initiator.indexOf("MOBILE") >= 0) {
                if (prevStatus === "IN_PROGRESS" && status === "CANCELED") {
                    return handleResult(new BaseError("Transition " + prevStatus + " -> " +
                    status + " is not allowed from Mobile", "ERROR_INVALID_TRANSITION"));
                }
            }

            if (invalidTransition) {
                common.error("PortinManager", "updateRequestStatus: invalid transition prev status="
                + prevStatus + ", new status=" + status);
                return handleResult(new BaseError("Transition " + prevStatus + " -> " +
                status + " is not allowed", "ERROR_INVALID_TRANSITION"));
            }

            ec.getCustomerDetailsService(serviceInstanceNumber, false, function (err, cache) {
                if (err) {
                    common.error("PortinManager", "updateRequestStatus: failed to update request " +
                    "status, err=" + err.message);
                    return handleResult(err);
                }
                if (!cache) {
                    common.error("PortinManager", "updateRequestStatus: failed to load customer details");
                    return handleResult(new BaseError("Customer not found", "ERROR_CUSTOMER_NOT_FOUND"));
                }

                var rescheduleInProgressEvent = (err) => {

                    // re-schedule initiation if port-in docs are not yet updated
                    // or plan is not yet active

                    // re-schedule only if called from scheduler
                    if (!canReschedule) {
                        return handleResult(err);
                    }

                    // new port-in date can be moved if it is Fr, Sat, Sun or a holiday
                    var newPortInDate = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);
                    module.exports.reScheduleRequest(requestId, newPortInDate, true, (schedulerErr, result) => {
                        obj.changes.reschedulingError = schedulerErr ? schedulerErr.message : undefined;
                        obj.changes.reschedulingResult = result;

                        return handleResult(err);
                    }, executionKey, initiator, err ? err.message : undefined);
                }

                if (status === "IN_PROGRESS") {
                    var currentLocalDate = new Date(new Date().getTime() + 8 * 60 * 60 * 1000);
                    var activationLocalDate = new Date(new Date(cache.serviceInstancePackageChangeDate).getTime() + 8 * 60 * 60 * 1000);

                    if (activationLocalDate.getTime() > 0
                        && currentLocalDate.getDate() == activationLocalDate.getDate()
                        && currentLocalDate.getMonth() == activationLocalDate.getMonth()
                        && currentLocalDate.getYear() == activationLocalDate.getYear()) {
                        common.error("PortinManager", "updateRequestStatus: plan has been activated today, can NOT start port-in");
                        return rescheduleInProgressEvent(new BaseError("Plan has been activated today, can NOT start port-in, " +
                        "temp number " + tempNumber, "ERROR_ACTIVATED_TODAY"));
                    } else if (cache.status == 'Terminated') {
                        common.error("PortinManager", "updateRequestStatus: invalid customer status, " +
                        "customer has been terminated, can NOT start port-in");

                        // mark port-in request as failed for terminated customer
                        // no need to re-schedule port-in requests for terminated customers
                        // no notification will be sent in such cases

                        var updateQuery = "UPDATE customerPortIn SET status='FAILED', " +
                            "error_status='ERROR_TERMINATED_ACCOUNT' WHERE id=" + db.escape(request.requestId);
                        db.query_err(updateQuery, function (err, rows) {
                            if (err) {
                                common.error("PortinManager", "updateRequestStatus: failed to update request " +
                                "status to failed status, err=" + err.message);
                                return handleResult(new Error("Failed to update request failed status for terminated customer " +
                                "status (" + err.message + ")", "ERROR_SQL"));
                            }

                            return handleResult(new BaseError("Customer has been terminated (" + serviceInstanceNumber + "), " +
                            "request marked as failed, no notification has been sent", "ERROR_CUSTOMER_TERMINATED"));
                        });

                        return;
                    } else if (!cache.serviceInstanceBasePlan || cache.serviceInstanceBasePlan.name === "CirclesRegistration") {
                        common.error("PortinManager", "updateRequestStatus: invalid base plan, " +
                        "plan is not yet activated, can NOT start port-in");
                        return rescheduleInProgressEvent(new BaseError("Invalid base plan, plan is NOT yet activated, " +
                        "temp number" + tempNumber, "ERROR_INACTIVE_PLAN"));
                    } else if (prevStatus === "WAITING_APPROVE" || request.docUpdateStatus === "PENDING") {
                        common.error("PortinManager", "updateRequestStatus: docs are not updated, " +
                        "plan is not yet activated, can NOT start port-in");
                        return rescheduleInProgressEvent(new BaseError("Invalid doc state, docs are NOT yet updated, " +
                        "temp number " + tempNumber, "ERROR_PENDING_DOC_UPDATE"));
                    }
                }

                var sendNotification = (prevStatus, newStatus, errorReason, callback) => {

                    // in case of manual recovery, (incorrect failure status is received)
                    // port-in number would become a main phone number and has to be reset in the cache

                    if (portInRecoverType === "MNP" && newStatus === "DONE") {
                        if (log) common.log("PortinManager", "port-in recovery successfully finished, " +
                        "update cache phone number with port-in number, portingNumber=" + portingNumber);

                        cache.number = portingNumber;
                        elitecoreUserService.clearUserInfo(cache.account);
                    }

                    sendStatusUpdateNotification(prevStatus, newStatus, cache.prefix, cache.number,
                        portingNumber, donorNetwork, startDate, errorReason, description, executionKey, function (err, result) {
                            if (err) {
                                common.error("PortinManager", "updateRequestStatus: failed to send notification, err=" + err.message);
                            }

                            if (callback) callback(err, result);
                        });
                }

                var deactivateApps = (deactivationCallback) => {
                    if (status === "DONE") {
                        var numbers = [portingNumber, tempNumber];
                        appManager.removeAppDevices("all", cache.prefix, numbers, () => {
                            return deactivationCallback(undefined, {message: "Successfully removed all devices apps"});
                        });
                    } else if (status === "FAILED" || status === "CANCELED") {
                        appManager.removeAppDevices("all", cache.prefix, portingNumber, () => {
                        });

                        const getPromoLogIdQuery = `SELECT promoLogId FROM customerPortIn where number=${portingNumber} ORDER BY id DESC limit 1`;
                        db.query_err(getPromoLogIdQuery, (err, rows) => {
                            if (err) {
                                common.error("PortinManager", `Failed to load current request status, err=${err.message}`);
                                return deactivationCallback(err, {message: "Failed to load promoLogId for the portInRequest"});
                            } else {
                                if (rows && rows.length && rows[0].promoLogId) {
                                    const setPromoLogIdQuery = `UPDATE customerPortIn 
                                    SET promoLogId = NULL where number=${portingNumber}`;
                                    const promologId = rows[0].promoLogId;

                                    db.query_err(setPromoLogIdQuery, (dbErr, rows) => {
                                        if (dbErr) {
                                            common.error("PortinManager", `Failed to update 
                                            promologId, err=${err.message}`); 
                                            return deactivationCallback(err, {message:
                                                "Failed to update promoLogId to null for the portInRequest"});
                                        }

                                        promotionsManager.removeCodeUsageLog(promologId)
                                        .then(resp => {
                                            sendStatusUpdateNotification(prevStatus, 
                                                status, cache.prefix, cache.number,
                                                portingNumber, donorNetwork, startDate, 'PORT_IN_PROMO_FAILED',
                                                undefined, executionKey, function (err, result) {
                                                    if (err) {
                                                        common.error("PortinManager", "Failed to send notification, err=" + err.message);
                                                    }
                                                    return deactivationCallback(undefined, {message:
                                                        "Successfully removed promoLog for the portIn request"});
                                                });
                                        })
                                    });
                                } else {
                                    common.log("PortinManager", `No PromologId found for number=${portingNumber}`)
                                    return deactivationCallback(undefined, {message: "No promoLogId found for request"});
                                }
                            }
                        });
                    } else {
                        return deactivationCallback(undefined, {message:
                            "No deactivation is required since the status is not a terminal state"});
                    }
                }

                var applyPendingPromoCodes = (cb) => {
                    if (status === "DONE") {
                        if (cache.serviceInstanceBasePlanName === 'CirclesSwitch') {
                            changePlanAndUnsubscribeAddon(cache, 'CirclesOne', 
                                serviceInstanceNumber, obj, (err, data) => {
                                    if (err) {
                                        return handleResult(err);
                                    }
                                    promotionsManager.applyPendingPromo(cache.prefix, cache.number, cache.orderReferenceNumber, 
                                        "PORT_IN", config.OPERATIONS_KEY, function (err, result) {
                                        if (err) {
                                            return cb(err, result);
                                        }

                                        if (!result || result.added !== true) {
                                            return cb(null, result);
                                        }

                                        let notification = {
                                            activity: "port_in_promo_code_added",
                                            name: cache.first_name,
                                            prefix: "65",
                                            number: portingNumber,
                                            email: cache.email,
                                            teamID: 5,
                                            teamName: "Operations"
                                        };
                                        notificationSend.deliver(notification, null,  (err, nfResult) => {
                                            if (err) {
                                                common.error("portinManager", "Failed to send promo code added " +
                                                    "notification for circles switch customer");
                                            }
                                            return cb(null, result);
                                        });
                                    });
                            });
                        } else {
                            promotionsManager.applyPendingPromo(cache.prefix, cache.number, cache.orderReferenceNumber, 
                                "PORT_IN", config.OPERATIONS_KEY, function (err, result) {
                                if (err) {
                                    return cb(err, result);
                                }

                                if (!result || result.added !== true) {
                                    return cb(null, result);
                                }

                                let notification = {
                                    activity: "port_in_promo_code_added",
                                    name: cache.first_name,
                                    prefix: "65",
                                    number: portingNumber,
                                    email: cache.email,
                                    teamID: 5,
                                    teamName: "Operations"
                                };
                                notificationSend.deliver(notification, null,  (err, nfResult) => {
                                    if (err) {
                                        common.error("portinManager", "Failed to send promo code added " +
                                            "notification for circles switch customer");
                                    }
                                    return cb(null, result);
                                });
                            });
                        }
                    } else {
                        return cb(undefined, {message: "No pending promos were applied since the status is not DONE"});
                    }

                }
                var changeStatus = () => {
                    var next = applyPendingPromoCodes;
                    var hasErrorReason = reason && (status === "FAILED" || status === "WAITING_RETRY");
                    var warning = getWarningInfo(status, startDate);
                    var updateQuery = "UPDATE customerPortIn SET status=" + db.escape(status) +
                        ", warning_type=" + db.escape(warning.type) +
                        ", warning_after_ts=" + db.escape(warning.after) +
                        ", status_update_ts=" + db.escape(new Date()) +
                        (hasErrorReason ? ", error_status=" + db.escape(reason) : ", error_status=NULL") +
                        " WHERE id=" + db.escape(request.requestId);

                    db.query_err(updateQuery, function (err, rows) {
                        if (err) {
                            common.error("PortinManager", "updateRequestStatus: failed to update request status, err=" + err.message);
                            return handleResult(new Error("Failed to update request status (" + err.message + ")", "ERROR_SQL"));
                        }

                        if (rows.affectedRows == 0) {
                            common.error("PortinManager", "updateRequestStatus: new status is required");
                            return handleResult(new Error("New status is required", "ERROR_DB_NOT_UPDATED"));
                        }

                        changeCustomerStatusForCirclesSwitchPortinFailureAndSendNotif(serviceInstanceNumber, invalidTransition, status, initiator, portingNumber, (err, result) => {
                            if(result){
                                obj.changes.circlesSwitchChanges = result;
                            }
                            sendNotification(prevStatus, status, reason, (err, notificationSentResult) => {
                                obj.changes.notificationSentError = err ? err.message : undefined;
                                obj.changes.notificationSentResult = notificationSentResult;
    
                                return next((err, promoResult) => {
                                    obj.changes.pendingPromotionError = err ? err.message : undefined;
                                    obj.changes.pendingPromotionResult = promoResult;
                                    deactivateApps((err, deactivateResult) => {
                                        obj.changes.deactivationError = err ? err.message : undefined;
                                        obj.changes.deactivationResult = deactivateResult;
                                        handleResult(undefined, {
                                            update: true,
                                            notification: {
                                                error: obj.changes.notificationSentError,
                                                result: obj.changes.notificationSentResult
                                            },
                                            promotion: {
                                                error: obj.changes.pendingPromotionError,
                                                result: obj.changes.pendingPromotionResult
                                            },
                                            deactivation: {
                                                error: obj.changes.deactivationError,
                                                result: obj.changes.deactivationResult
                                            }
                                        });
                                    }); 
                                });
                            });
                        });
                    });
                }

                var revertTemporaryOwnerName = () => {
                    var next = changeStatus;

                    if (request.docUpdateStatus != "UPDATED"
                        || (status !== "DONE" && status !== "FAILED" && status !== "CANCELED")) {
                        return next();
                    }

                    if (!request.customerName || !request.customerIdType || !request.customerIdNumber) {
                        common.error("PortinManager", "updateRequestStatus: failed to revert back " +
                        "customer profile info, some information is empty");
                        return handleResult(new Error("Failed to revert back customer profile info", "ERROR_MISSING_CUSTOMER_DATA"));
                    }

                    elitecoreUserService.updateAllCustomerAccountsDetails(cache.customerAccountNumber, {name: request.customerName}, (err, result) => {
                        elitecoreUserService.clearUserInfo(cache.account);
                        obj.changes.updateCustomerNameResult = result;

                        if (err) {
                            common.error("PortinManager", "updateRequestStatus: failed to update customer name, err=" + err.message);
                            return handleResult(err);
                        }

                        elitecoreUserService.updateCustomerID(cache.customerAccountNumber,
                            request.customerIdType, request.customerIdNumber, (err, result) => {
                                elitecoreUserService.clearUserInfo(cache.account);
                                obj.changes.updateCustomerIDResult = result;

                                if (err) {
                                    common.error("PortinManager", "updateRequestStatus: failed to update " +
                                    "customer id, error=" + err.message);
                                    return handleResult(err);
                                }

                                var updateQuery = "UPDATE customerPortIn SET doc_update_status=" + db.escape("REVERTED")
                                    + " WHERE id=" + db.escape(request.requestId);
                                db.query_err(updateQuery, function (err, rows) {
                                    if (err) {
                                        common.error("PortinManager", "updateRequestStatus: failed to update " +
                                        "doc status, err=" + err.message);
                                        return handleResult(new Error("Failed to update doc request status", "DB_ERROR"));
                                    }

                                    if (rows.affectedRows == 0) {
                                        common.error("PortinManager", "updateRequestStatus: doc status has not been updated");
                                        return handleResult(new Error("Request doc status has not been updated", "DB_ERROR"));
                                    }

                                    obj.params.docUpdateStatus = "REVERTED";
                                    next();
                                });
                            });

                    }, false, true, true);
                }

                var initPortIn = () => {
                    var next = revertTemporaryOwnerName;
                    if (status !== "IN_PROGRESS" && (status != "DONE" || prevStatus != "FAILED"))  return next();

                    updateScheduledPortInAsProcessed(requestId, () => {
                        var tempNumber = cache.serviceInstanceInventoryPrimary
                            ? cache.serviceInstanceInventoryPrimary.number : undefined;

                        var portInProgressHandler = (err, result) => {
                            elitecoreUserService.clearUserInfo(cache.account);
                            obj.changes.portInNumberImmediateResult = result;

                            if (err) {
                                var newStatus = err.status === "ERROR_INITIATION_INACTIVE" ? "FAILED" : "WAITING_RETRY";
                                var errorStatus = err.status;
                                var updateQuery = "UPDATE customerPortIn SET status=" + db.escape(newStatus) +
                                    ", error_status=" + db.escape(errorStatus) +
                                    ", status_update_ts=" + db.escape(new Date()) +
                                    " WHERE id=" + db.escape(requestId);

                                obj.changes.portInNumberImmediateError = {
                                    newStatus: newStatus,
                                    errorStatus: errorStatus,
                                    errorMessage: err.message
                                };

                                db.query_err(updateQuery, (err, rows) => {
                                    if (err) {
                                        common.error("PortinManager", "updateRequestStatus: failed to update " +
                                        "request status, err=" + err.message);
                                    }

                                    common.error("PortinManager", "updateRequestStatus: failed to initiate port-in request");
                                    sendNotification(prevStatus, newStatus, errorStatus);
                                    return handleResult(err);
                                });

                                return;
                            } else {
                                next();
                            }
                        };

                        if (cache.base.id === constants.CIRCLES_SWITCH_PLAN_ID) {
                            if (cache.status === "Active") {
                                elitecorePortInService.portInNumberImmediate(serviceInstanceNumber, tempNumber, portingNumber,
                                    donorNetworkCode, requestId, portInProgressHandler);
                            } else {
                                // temporarily suspend customer to perform reinitiation
                                accountActions.changeStatus(cache.serviceInstanceNumber, "Active", "Activate Circles Switch Temp",
                                    "ACTIVE_DUE_TO_REINIT", "portin", config.OPERATIONS_KEY, (err, result) => {
                                    if(err){
                                        portInProgressHandler(new BaseError("Customer is Inactive", "ERROR_INITIATION_INACTIVE"));
                                    }else{
                                        // perform portin reinit and callback to handler, we don't care if account suspension afterwards is ok
                                        elitecorePortInService.portInNumberImmediate(serviceInstanceNumber, tempNumber, portingNumber,
                                            donorNetworkCode, requestId, (pErr, pRes) => {
                                            accountActions.changeStatus(cache.serviceInstanceNumber,
                                               "Suspended", "Suspend Circles Switch", "SUSPENDED_MANUAL", "portin", config.OPERATIONS_KEY, () => {});
                                            portInProgressHandler(pErr, pRes);
                                        });
                                    }
                                });
                            }
                        } else if (cache.serviceInstanceCustomerStatus && cache.serviceInstanceCustomerStatus.toUpperCase() === "INACTIVE") {

                            // it is not allowed to port-in inactive customers
                            // mark as error and send an error notification

                            portInProgressHandler(new BaseError("Customer is Inactive", "ERROR_INITIATION_INACTIVE"));
                        } else if (portInRecoverType && portInRecoverType === "TIMEOUT") {

                            // in case if provisioning notification has not reached M1 but number was successfully attached
                            // we need to resend provisioning notification only

                            portInProgressHandler(undefined, {ignored: true})
                        } else if (portInRecoverType && portInRecoverType === "PROVISION") {

                            // in case if provisioning notification has not reached M1 but number was successfully attached
                            // we need to resend provisioning notification only

                            elitecoreConnector.serviceProvisioningNotification(requestId, tempNumber, portInProgressHandler);
                        } else if (portInRecoverType && portInRecoverType === "MNP") {

                            // MNP is for cases when port in finished successfully
                            // but number has not been properly paired

                            elitecorePortInService.portInNumberImmediate(serviceInstanceNumber, tempNumber, portingNumber,
                                donorNetworkCode, requestId, portInProgressHandler, true);
                        } else {
                            elitecorePortInService.portInNumberImmediate(serviceInstanceNumber, tempNumber, portingNumber,
                                donorNetworkCode, requestId, portInProgressHandler);
                        }
                    });
                }

                var loadCustomerInfo = () => {
                    var next = initPortIn;

                    if (status !== "FAILED" && status !== "CANCELED"
                        && status !== "IN_PROGRESS" && status !== "DONOR_APPROVED"
                        && (status != "DONE" || prevStatus != "FAILED")) {
                        return next();
                    }

                    ec.updateCustomerAccountData({
                        accountNumber: cache.account,
                        accountProfiles: {
                            "strcustom3": status === "IN_PROGRESS" || status === "DONOR_APPROVED"
                            || status === "DONE" ? "MNP" : "NORMAL"
                        }
                    }, function (err, result) {
                        elitecoreUserService.clearUserInfo(cache.account);
                        obj.changes.updateCustomerAccountPortInStatusResult = result;

                        if (err) {
                            common.error("PortinManager", "updateRequestStatus: failed to set / remove MNP status on EC side request");
                            return handleResult(new BaseError("Failed to set / remove MNP status on " +
                            "EC side request (" + err.message + ")", "ERROR_EC_UPDATE_PROFILE"));
                        }

                        next();
                    });
                }

                var removeAttachedNumber = () => {
                    var next = loadCustomerInfo;
                    if (status !== "FAILED" && status !== "CANCELED") return next();

                    elitecorePortInService.deactivatePortInNumber(cache.serviceInstanceNumber, portingNumber, function (err, result) {
                        elitecoreUserService.clearUserInfo(cache.account);
                        obj.changes.deactivatePortInNumberResult = result;

                        // no need to remove number that is attached to another customer
                        if (err && err.status != 'ERROR_ALREADY_IN_USE') {
                            return handleResult(err);
                        }
                        next();
                    });
                }

                var cancelEvent = () => {
                    var next = removeAttachedNumber;

                    if (status !== "CANCELED" || !(prevStatus === "IN_PROGRESS" || prevStatus === "DONOR_APPROVED")) return next();
                    
                        if (cache.serviceInstanceCustomerStatus 
                            && cache.serviceInstanceCustomerStatus.toUpperCase() === "INACTIVE") {
                            accountActions.changeStatus(cache.serviceInstanceNumber, "Active", "Activate Circles Switch Temp",
                                    "ACTIVE_DUE_TO_REINIT", "portin", config.OPERATIONS_KEY, (err, result) => {
                                    if(err){
                                        return handleResult(new BaseError("Customer is Inactive", 
                                            "ERROR_INITIATION_INACTIVE"));
                                    }else{
                                        elitecoreConnector.cancelPortInOrder(portingNumber, "1", function (err, result) {
                                            elitecoreUserService.clearUserInfo(cache.account);
                                            obj.changes.cancelPortInOrderResult = result;

                                            if (err) return handleResult(err);
                                            accountActions.changeStatus(cache.serviceInstanceNumber,
                                                "Suspended", "Suspend Circles Switch", "SUSPENDED_MANUAL", "portin", config.OPERATIONS_KEY, () => {})
                                            updateScheduledPortInAsProcessed(requestId, () => {
                                                if (err) return handleResult(err);
                                                next();
                                            }, true, initiator);
                                        });
                                    }
                                });
                        }else {
                            elitecoreConnector.cancelPortInOrder(portingNumber, "1", function (err, result) {
                                elitecoreUserService.clearUserInfo(cache.account);
                                obj.changes.cancelPortInOrderResult = result;
        
                                if (err) return handleResult(err);
                                updateScheduledPortInAsProcessed(requestId, () => {
                                    if (err) return handleResult(err);
                                    next();
                                }, true, initiator);
                            });
                        }
                }

                var clearEvents = () => {
                    var next = cancelEvent;
                    if (status != "CANCELED") return next();

                    updateScheduledPortInAsProcessed(requestId, (err, result) => {
                        if (err) {
                            common.error("PortinManager", "updateRequestStatus: failed to set " +
                            "scheduler event status");
                            // no need to stop cancellation, event is not critical,
                            // it will not be started in case if even does not have proper date in db
                        }

                        next();
                    });
                }

                clearEvents();
            });
        };
    },

    /**
     *
     */
    schedulePortIn: (serviceInstanceNumber, prefix, number, requestId,
                     portInNumber, donor, startDate, initiator, callback) => {
        schedulePortIn(serviceInstanceNumber, prefix, number, requestId,
            portInNumber, donor, startDate, initiator, callback)
    },

    /**
     *
     */
    schedulePortInFailureCheck: (serviceInstanceNumber, prefix, number, requestId,
                                 portInNumber, initiator, callback) => {
        schedulePortInFailureCheck(serviceInstanceNumber, prefix, number, requestId,
            portInNumber, initiator, callback);
    },

    /**
     *
     */
    syncGAReport: function (fileId, type, options, callback) {
        if (!callback) callback = () => {
        }

        if (!fileId || !type) {
            return callback(new BaseError("Month date is missing", "ERROR_INVALID_PARAMS"))
        }

        Promise.resolve({metadata: {}}).then((info) => {
            return new Promise((resolve, reject) => {
                loadGAResultsFromXLS(fileId, type, (err, result) => {
                    if (err) {
                        common.error("PortInManager", "syncGAReport->loadGAResultsFromXLS: failed to load " +
                        "statuses from payment file, err=" + err.message);
                        return reject(err);
                    }

                    if (log) common.log("PortInManager", "syncGAReport->loadGAResultsFromXLS: count=" + result.data.length);
                    info.report = result;
                    resolve(info);
                });
            });
        }).then((info) => {
            return new Promise((resolve, reject) => {
                var report = info.report;

                var tasks = [];
                var updatePaymentStatus = (reportItem, callback) => {
                    var query = "UPDATE customerPortIn SET order_source=" + db.escape(reportItem.source) +
                        " WHERE order_reference_number=" + db.escape(reportItem.orn);
                    db.query_err(query, function (err, result) {
                        if (err) {
                            common.error("PortInManager", "syncGAReport->update: attempt 1, failed to load "
                            + reportItem.orn + ", error=" + err.message);

                            if (err.message && err.message.indexOf("ER_LOCK_DEADLOCK") >= 0) {
                                common.error("PortInManager", "syncGAReport->update: attempt 2, failed to load "
                                + reportItem.orn + ", error=" + err.message);

                                setTimeout(() => {
                                    db.query_err(query, function (err) {
                                        if (err) return callback(err);
                                        callback();
                                    });
                                }, 500);
                                return;
                            }

                            return callback(err);
                        }

                        return callback();
                    });
                }

                report.data.forEach((item) => {
                    (function (reportItem) {
                        tasks.push((callback) => {
                            updatePaymentStatus(reportItem, callback);
                        });
                    }(item));
                });

                async.parallelLimit(tasks, 10, (err, result) => {
                    if (err) {
                        return reject(err);
                    }

                    if (log) common.log("InvoiceManager", "syncPaymentsFromFile->insertEntries: " +
                    "updated, total count=" + report.data.length);

                    info.metadata.update = {
                        totalCount: report.data.length
                    };
                    resolve(info);
                });
            });
        }).then((info) => {
            callback(undefined, info.metadata);
        }).catch(function (err) {
            callback(err);
        });
    },
    getPortedInStatus: (id) => {
        let loadDataById = bluebird.promisify(loadRequestById);

        return loadDataById(id)
          .then((result) => {            
            const {portingNumber, status} = result;

            return bluebird.resolve(elitecorePortInService.getPortInStatus(portingNumber, status))
          })
          .catch((err) => {
              throw new Error(err);
          });
    },

    activateRequest: (sin, startDate, callback) => {
        if (!callback) callback => {
        }
        if (!sin || !startDate) {
            return callback(new BaseError("Invalid params", "ERROR_INVALID_PARAMS"));
        }
        const executionKey = config.NOTIFICATION_KEY;

        module.exports.loadActiveRequestByServiceInstanceNumber(sin, {}, (err, request) => {
            if (err) {
                return callback(err);
            }
            if (!request || request.status == "NOT_FOUND") {
                return callback(new BaseError("Active port-in request not found", "ERROR_NO_ACTIVE_PORT_IN"));
            }
            const requestId = request.requestId;
            const donorNetworkCode = request.donor_network.code;
            const networkName = request.donor_network.name;

            elitecoreUserService.loadUserInfoByServiceInstanceNumber(sin, function (err, cache) {
                if (err) {
                    common.error("PortinManager", "activateRequest: failed to load customer request " +
                    "status, err=" + err.message);
                    return callback(err);
                }
                if (!cache) {
                    common.error("PortinManager", "activateRequest: no customer found");
                    return callback(new BaseError("No customer found", "ERROR_CUSTOMER_NOT_FOUND"));
                }

                var activateRequestInternal = (promoLogId) => {
                    const updateQuery = `UPDATE customerPortIn SET promoLogId=${ db.escape(promoLogId ? promoLogId : null) },
                                start_ts=${db.escape(startDate)} WHERE id= ${ db.escape(request.requestId) }`;

                    db.query_err(updateQuery, (err, rows) => {
                        if (err) {
                            common.error("PortinManager", "Failed to update request status, err=" + err.message);
                        }

                        schedulePortIn(sin, cache.prefix, cache.number, request.requestId, request.porting_number,
                            donorNetworkCode, startDate, '[CMS][INTERNAL]', (err, result) => {
                                if (err) {
                                    return callback(err);
                                }

                                sendStatusUpdateNotification('WAITING', 'WAITING', cache.prefix, cache.number,
                                    request.porting_number, networkName, startDate, 'PORT_IN_ACTIVATION',
                                    undefined, executionKey, function (err, result) {
                                        if (err) {
                                            common.error("PortinManager", "Failed to send notification, err=" + err.message);
                                            return callback();
                                        }

                                        return callback(undefined, result);
                                    });
                            });
                    });
                }

                configManager.loadConfigMapForKey('bonus', 'bonus_port_in_promo_campaign', function (err, item) {
                    if (err) {
                       return callback(err);
                    }

                    var promoTag = item ? item.options.promo_category_tag : undefined;
                    var startDate = item && item.options.start_date ? new Date(item.options.start_date) : undefined;
                    var endDate = item && item.options.end_date ? new Date(item.options.end_date) : undefined;
                    var enabled = item ? item.options.enabled : false;
                    var currentDate = new Date();

                    if (promoTag && enabled && startDate && startDate.getTime() < currentDate.getTime() &&
                        endDate && endDate.getTime() > currentDate.getTime()) {
                        promotionsManager.copyCode(item.options.promo_category_tag, (err, promoResult) => {
                            if (err) {
                                return callback(err);
                            }

                            promotionsManager.useCode('CMS', cache.prefix, cache.number, promoResult.code,
                                cache.orderReferenceNumber, executionKey, (err, data) => {
                                    if (err) {
                                        return callback(err);
                                    }

                                    const promoLogId = data.promoLogId;
                                    activateRequestInternal(promoLogId);
                                });
                        });
                    } else {
                        activateRequestInternal();
                    }
                })
            });
            
        });

    },

    loadActiveRequestByPortinNumber: (number) => {
        const query = `SELECT * FROM customerPortIn where number = ${number} AND status IN (
            ${db.escape("WAITING")}, 
            ${db.escape("WAITING_RETRY")},
            ${db.escape("WAITING_APPROVE")},
            ${db.escape("IN_PROGRESS")},
            ${db.escape("DONOR_APPROVED")}
        )`;

        return new bluebird((resolve, reject) => {
            db.query_err(query, (err, result) => {
                if (err) {
                    common.error('PortinManager.loadActiveRequestByPortinNumber() Error: ', err);
                    reject(err);
                }else {
                    resolve(result);
                }
            });
        });
    },

    loadActivePortoutRequest: (number) => {
        const query = `SELECT * FROM customerPortOut where number = ${number} AND status IN (
            ${db.escape("WAITING")}, 
            ${db.escape("WAITING_RETRY")},
            ${db.escape("WAITING_APPROVE")},
            ${db.escape("IN_PROGRESS")},
            ${db.escape("DONOR_APPROVED")}
        )`;

        return new bluebird((resolve, reject) => {
            db.query_err(query, (err, result) => {
                if (err) {
                    common.error('PortinManager.loadActiveRequestByPortOutNumber() Error: ', err);
                    reject(err);
                }else {
                    resolve(result);
                }
            });
        });
    },

}


function loadRequestById(id, callback) {
    var selectQuery = "SELECT customerProfile.service_instance_number, customerPortIn.* " +
        " FROM customerPortIn LEFT OUTER JOIN customerProfile ON (customerProfile.id = customerPortIn.profile_id) " +
        " WHERE customerPortIn.id=" + db.escape(id);

    db.query_err(selectQuery, function (err, rows) {
        if (err) {
            common.error("PortinManager", "Failed to load current request status, err=" + err.message);
            return callback(new BaseError("Failed to load current request status", "ERROR_DB"));
        }

        if (!rows || rows.length < 1) {
            common.error("PortinManager", "No requests found");
            return callback(new BaseError("No port-in requests found", "ERROR_NOT_FOUND"));
        }


        if (callback) callback(undefined, {
            planName: rows[0].planName,
            requestId: rows[0].id,
            serviceInstanceNumber: rows[0].service_instance_number,
            status: rows[0].status,
            errorStatus: rows[0].error_status,
            docUpdateStatus: rows[0].doc_update_status,
            portingNumber: rows[0].number,
            tempNumber: rows[0].temp_number,
            donorNetwork: rows[0].donor_network,
            donorNetworkCode: rows[0].donor_network_code,
            startDate: new Date(rows[0].start_ts),

            ownerName: rows[0].owner_name,
            ownerIdType: rows[0].owner_id_type,
            ownerIdNumber: rows[0].owner_id_number,
            customerName: rows[0].customer_name,
            customerIdType: rows[0].customer_id_type,
            customerIdNumber: rows[0].customer_id_number
        });
    });
}

function changeCustomerStatusForCirclesSwitchPortinFailureAndSendNotif(serviceInstanceNumber, invalidTransition, status, initiator, portingNumber, callback){
    elitecoreUserService.loadUserInfoByServiceInstanceNumber(serviceInstanceNumber, (err, cache) => {
        if(!cache){
            callback(null, {"message": "error occoured when trying to load customer cache"});
        } else if(cache.serviceInstanceBasePlanName === 'CirclesSwitch'){
            if((status === "FAILED" || status === "CANCELED") && !invalidTransition){
                var scheduleTermination = (terminationScheduledTime, cb) => {
                    var schedule = terminationManager.getTerminationScheduleForSpesificDate(terminationScheduledTime, cache.prefix, cache.number, serviceInstanceNumber);                    
                    schedulerManager.saveRawSchedule(schedule, (err, result) => {
                        if(err){
                            logStatusChangeAction(initiator, "CIRCLES_SWITCH_PORT_IN_FAILURE" ,"terminateCirclesSwitch", serviceInstanceNumber, err);
                        }else{
                            logStatusChangeAction(initiator, "CIRCLES_SWITCH_PORT_IN_FAILURE", "terminateCirclesSwitch", serviceInstanceNumber, result);
                        }
                        cb(err, result);
                    });
                };
                var performSuspension = () => {
                    accountActions.changeStatus(serviceInstanceNumber, "Suspended", "Suspend Circles Switch", "SUSPENDED_MANUAL", "portin", config.OPERATIONS_KEY,
                    (err, result) => {
                        if(err){
                            logStatusChangeAction(initiator, "CIRCLES_SWITCH_PORT_IN_FAILURE", "suspendCirclesSwitch", serviceInstanceNumber, err);
                        }else{
                            logStatusChangeAction(initiator, "CIRCLES_SWITCH_PORT_IN_FAILURE", "suspendCirclesSwitch", serviceInstanceNumber, result);
                        }
                    });
                };
                terminationManager.getScheduledTerminationBySIN(serviceInstanceNumber)
                .then((event) => {
                    if(!event && cache.status === "Active"){
                        let terminationScheduledTime = new Date().getTime() + 14 * 24 * 3600000;
                        scheduleTermination(terminationScheduledTime, (err, result) => {
                            if(!err){
                                sendCirclesSwitchPortinFailureNotification(cache, portingNumber, terminationScheduledTime);
                            }
                        });
                        performSuspension();
                        callback(null, {"message": "customer suspended and scheduled for termination"});
                    }else{
                        callback(null, {"message": "customer not active and/or already termination scheduled"});
                    }
                })
                .catch((err) => {
                    common.error("portinManager", "Error occured when trying to search for scheduled termination");
                    callback(null, {"message": "error occoured when checking for existing termination schedules"});
                });
            } else if(status == "DONE"){
                logStatusChangeAction(initiator, "CIRCLES_SWITCH_PORT_IN_SUCCESS", "noTerminationOrSuspension", serviceInstanceNumber, null);
                if (cache.status === "Active") {
                    schedulerManager.removeCirclesSwitchSchedules(cache.serviceInstanceNumber, () => {
                        callback(null, {"message": "circles switch portin request success"});
                    });
                } else {
                    accountActions.changeStatus(cache.serviceInstanceNumber, "Active", "Activate Circles Switch Temp",
                        "ACTIVE_DUE_TO_REINIT", "portin", config.OPERATIONS_KEY, (err, result) => {
                        schedulerManager.removeCirclesSwitchSchedules(cache.serviceInstanceNumber, () => {
                            callback(null, {"message": "circles switch portin request success"});
                        });
                    });
                }
            } else {
                callback(null, {"message": "circles switch portin request status change with " + status});
            }
        } else {
            callback(null, {"message": "not a circles switch customer"});
        }
    });
}

function sendCirclesSwitchPortinFailureNotification(cache, portingNumber, scheduledTime){
    let displayDate = new Date(scheduledTime + 3600000 * 8);
    let notification = {
        activity: "circles_switch_port_in_failed",
        name: cache.first_name,
        number: cache.number,
        porting_number: portingNumber,
        prefix: "65",
        number: cache.number,
        email: cache.email,
        teamID: 4,
        teamName: "Operations",
        termination_scheduled_time: common.getDate(displayDate) + " " + common.getTimeString(displayDate)
    };
    notificationSend.deliver(notification, null,  (err, result) => {
        if (err) {
            common.error("portinManager", "Failed to send termination scheduled notification for circles switch customer");
        } else {
            common.log("portinManager", "Termination scheduled notification success");
        }
    });
}

function logStatusChangeAction(initiator, type, logMessage, serviceInstanceNumber, actionResult){
    db.notifications_port_in.insert({
        "ts": new Date().getTime(),
        "type": type,
        "initiator": initiator,
        "serviceInstanceNumber": serviceInstanceNumber,
        "action": logMessage,
        "result": actionResult,
    });
}



function updateScheduledPortInAsProcessed(requestId, callback, canceled, disablingInitiator) {
    if (!callback) callback = function () {
    };

    var myfqdn = config.MYFQDN;
    db.scheduler.update({
            action: {"$in": ["portin", "portin_failure_check"]},
            requestId: requestId, "$or": [{processed: {"$exists": false}}, {processed: false}]
        },
        {
            $set: {
                processed: true, color: "yellow", myfqdn: myfqdn, canceled: canceled,
                disablingInitiator: disablingInitiator
            }
        },
        {multi: true}, function (err, result) {
            if (err) {
                return callback(new BaseError("Scheduler Error", "ERROR_REMOVE_FROM_SCHEDULER"));
            }
            callback();
        });
}

function schedulePortInFailureCheck(serviceInstanceNumber, prefix, number, requestId, portInNumber, initiator, callback) {
    if (!callback)  callback = function () {
    };

    if (!requestId || !portInNumber) {
        return callback(new BaseError("Invalid params (requestId, portInNumber)", "ERROR_INVALID_PARAMS"));
    }

    var startDate = new Date(new Date().getTime() + 20 * 60 * 60 * 1000);
    db.scheduler.insert({
        start: startDate.getTime(),
        parallelLimitOne: true,
        requestId: requestId,
        prefix: prefix,
        number: number,
        serviceInstanceNumber: serviceInstanceNumber,
        portInPrefix: prefix,
        portInNumber: portInNumber,
        action: "portin_failure_check",
        allDay: false,
        trigger: initiator,
        title: "Port-In " + common.escapeHtml(portInNumber) + " (RequestId " + common.escapeHtml(requestId) + ")"
    }, {writeConcern: {w: 'majority'}}, function (err, result) {
        if (err) {
            common.error("PortInManager schedulePortInFailureCheck", err);
            return callback(new BaseError("Scheduler Error", "ERROR_ADD_TO_SCHEDULER"));
        }
        if (!result || !result.insertedIds || !result.insertedIds[0]) {
            return callback(new BaseError("Scheduler Error", "ERROR_FAILED_TO_ADD_TO_SCHEDULER"));
        }
        callback(undefined, {added: true, eventId: result.insertedIds[0]});
    });
}

function schedulePortIn(serviceInstanceNumber, prefix, number, requestId, portInNumber, donor, startDate, initiator, callback) {
    if (!callback)  callback = function () {
    };

    if (!requestId || !portInNumber || !donor || !startDate) {
        return callback(new BaseError("Invalid params (requestId, portInNumber, donor, startDate)", "ERROR_INVALID_PARAMS"));
    }

    var startDate = new Date(startDate);
    if (startDate.getTime() - 50 * 60 * 1000 < new Date().getTime()) {
        if (callback) callback(new BaseError("Invalid start date provided (" + startDate + ")", "ERROR_INVALID_EVENT_DATES"));
    } else {
        db.scheduler.insert({
            start: startDate.getTime(),
            parallelLimitOne: true,
            requestId: requestId,
            prefix: prefix,
            number: number,
            serviceInstanceNumber: serviceInstanceNumber,
            portInPrefix: prefix,
            portInNumber: portInNumber,
            donor: donor,
            action: "portin",
            allDay: false,
            trigger: initiator,
            title: "Port-In " + common.escapeHtml(portInNumber) + " (Donor " + common.escapeHtml(donor) + ")"
        }, {writeConcern: {w: 'majority'}}, function (err, result) {
            if (err) {
                common.error("PortInManager schedulePortIn", err);
                return callback(new BaseError("Scheduler Error", "ERROR_ADD_TO_SCHEDULER"));
            }
            if (!result || !result.insertedIds || !result.insertedIds[0]) {
                return callback(new BaseError("Scheduler Error", "ERROR_FAILED_TO_ADD_TO_SCHEDULER"));
            }
            callback(undefined, {added: true, eventId: result.insertedIds[0]});
        });
    }
}

function getWarningInfo(status, startDate) {
    if (!startDate || !status) {
        return {
            after: new Date(),
            type: "INVALID_WARING_PARAMS"
        }
    }

    var result = {};
    if (status === "WAITING_APPROVE") {
        result.after = new Date(startDate.getTime() - 5 * 24 * 60 * 60 * 1000);
        result.type = "WAITING_DOC_APPROVE";
        if (result.after.getTime() < new Date().getTime()) {
            result.after = new Date();
        }
    } else if (status === "WAITING") {
        result.after = new Date(startDate.getTime() + 2 * 60 * 60 * 1000);
        result.type = "WAITING_IN_PROGRESS";
    } else if (status === "IN_PROGRESS") {
        result.after = new Date(new Date().getTime() + 3 * 60 * 60 * 1000);
        result.type = "WAITING_DONOR_APPROVE";
    } else if (status === "WAITING_RETRY") {
        result.after = new Date();
        result.type = "WAITING_RETRY";
    } else if (status === "DONOR_APPROVED") {
        // 16h is midnight in SGT, if received after midnight port in will happen on the next day only
        if (new Date().getHours() >= 16) {
            result.after = new Date(new Date().setHours(16, 0, 0, 0) + 34 * 60 * 60 * 1000);
        } else {
            result.after = new Date(new Date().setHours(16, 0, 0, 0) + 10 * 60 * 60 * 1000);
        }
        result.type = "WAITING_SUCCESS";
    } else {
        result.after = new Date("2038-01-01");
        result.type = "";
    }
    return result;
}

function sendStatusUpdateNotification(prevStatus, newStatus, prefix, number, portingNumber, donorNetwork,
                                      startDate, reason, description, notificationKey, callback) {
    var activity;
    if (!prevStatus) {
        if (newStatus === "WAITING") {
            activity = "port_in_request_registered";
        } else if (newStatus === "WAITING_APPROVE") {
            activity = "port_in_request_registered_with_docs";
        } else if (newStatus === "IN_PROGRESS") {
            // this case may happened if port-in is initiated from eComm
            // port in request would be marked as InProgress without waiting option
            activity = "port_in_request_registered_and_started";
        }
    } else if (prevStatus === "WAITING" || prevStatus === "WAITING_APPROVE") {
        if (newStatus === "IN_PROGRESS" || prevStatus === "DONOR_APPROVED") {
            activity = "port_in_request_initiated";
        } else if (newStatus === "CANCELED") {
            activity = "port_in_request_canceled";
        } else if (newStatus === "FAILED") {
            activity = "port_in_request_failure";
        } else if (newStatus === "WAITING_RETRY") {
            activity = "port_in_request_need_retry";
        } else if (newStatus === "DONE") {
            activity = "port_in_request_success";
        } else if (newStatus === "WAITING" && reason =='PORT_IN_ACTIVATION') {
            activity = "circles_switch_port_in_activated";
        } else if(newStatus === 'FAILED' && reason === 'PORT_IN_PROMO_FAILED') {
            activity = 'port_in_promo_code_not_added';
        }
    } else if (prevStatus === "WAITING_RETRY") {
        if (newStatus === "IN_PROGRESS" || prevStatus === "DONOR_APPROVED") {
            activity = "port_in_request_initiated";
        } else if (newStatus === "FAILED") {
            activity = "port_in_request_failure";
        } else if (newStatus === "DONE") {
            activity = "port_in_request_success";
        } else if (newStatus === "CANCELED") {
            activity = "port_in_request_canceled";
        } else if (newStatus === "WAITING_RETRY") {
            activity = "port_in_request_need_retry";
        }
    } else if (prevStatus === "IN_PROGRESS" || prevStatus === "DONOR_APPROVED") {
        if (newStatus === "FAILED") {
            activity = "port_in_request_failure";
        } else if (newStatus === "DONE") {
            activity = "port_in_request_success";
        } else if (newStatus === "CANCELED") {
            activity = "port_in_request_canceled_active";
        }
    } else if (prevStatus === "FAILED") {
        if (newStatus === "DONE") {
            activity = "port_in_request_success_after_invalid_failure";
        }
    }

    if (activity === "port_in_request_failure") {
        if (reason === "ERROR_DONOR_TELCO") {
            activity = "portin_failure_donor_telco";
        } else if (reason === "ERROR_ID_MISTMATCH") {
            activity = "portin_failure_id_mismatch";
        } else if (reason === "ERROR_INACTIVE" || reason === "ERROR_SUSPENDED") {
            activity = "portin_failure_inactive";
        } else if (reason === "ERROR_OTHER" || reason === "ERROR_PENDING_ORDER"
            || reason === "ERROR_INCORRECT_TRANSFER_TIME" || reason === "ERROR_MSISDN_NOT_EXISTS") {
            activity = "port_in_request_failure";
        } else if (reason === "ERROR_PREPAID") {
            activity = "portin_failure_prepaid";
        } else if (reason === "ERROR_DOC_REJECTED") {
            activity = "portin_failure_id_doc_rejected";
        } else if (reason === "ERROR_INITIATION_INACTIVE") {
            activity = "portin_failure_initiation_inactive";
        }
    }

    if (activity === "port_in_request_success") {
        if (!donorNetwork || !startDate) {
            activity = "port_in_request_success_short";
        }
    } else if (activity === "port_in_request_failure") {
        if (!donorNetwork || !startDate) {
            activity = "port_in_request_failure_short";
        }
    } else if (activity === "port_in_request_canceled") {
        if (!donorNetwork || !startDate) {
            activity = "port_in_request_canceled_short";
        }
    }

    if (log) common.log("PortinManager", "Send status update notification, prevStatus=" + prevStatus +
    ", newStatus=" + newStatus + ", prefix=" + prefix + ", currentNumber=" + number + ", reason=" + reason +
    ", portingNumber=" + portingNumber + ", donorNetwork=" + donorNetwork + ", startDate=" + startDate +
    ", activity=" + activity + ", description=" + description);

    if (activity) {
        sendNotificationInternal(activity, prefix, number, portingNumber, donorNetwork,
            startDate, reason, description, notificationKey, callback)
    } else {
        if (callback) callback(undefined, {activity: "undefined"});
    }
}

function sendNotificationInternal(activity, prefix, number, portingNumber, donorNetwork,
                                  startDate, reason, description, notificationKey, callback) {
    var startDateSG = !startDate ? undefined
        : new Date(new Date(startDate).getTime() + 8 * 60 * 60 * 1000);
    // TODO: remove notificationKey - move logic to ROLE
    const notification = {
        teamID: (process.env.ROLE == 'mobile' ? 1 : 5),
        prefix,
        number,
        activity,
        notificationKey,
        reason: reason,
        porting_number: portingNumber,
        donor_network: donorNetwork,
        description: description ? description : 'None',
        start_date: startDateSG && startDateSG.getTime() ? dateformat(startDateSG, 'dS mmmm') : undefined,
        number_selected: portingNumber
    }
    notificationSend.deliver(notification, undefined, function (err, result) {
        if (err) {
            if (callback) callback(err, { activity, variables: notification });
            return;
        } else {
            if (callback) callback(undefined, { activity, variables: notification, result });
            return;
        }
    });
}

function loadGAResultsFromXLS(fileId, type, callback) {
    fileManager.loadParsedXLSFile(fileId, type, (err, content) => {
        if (err) {
            return callback(err);
        }
        if (!content[1] || !content[1].data[0]) {
            return callback(new BaseError("Invalid response"));
        }

        var transactionIdColumn = -1;
        var sourceMediumColumn = -1;

        var headers = content[1].data[0];
        headers.forEach((item, pos) => {
            if (item == 'Transaction ID') {
                transactionIdColumn = pos;
            } else if (item == 'Source / Medium') {
                sourceMediumColumn = pos;
            }
        });

        if (!transactionIdColumn || !sourceMediumColumn) {
            return callback(new BaseError("Some columns are not found"));
        }

        var items = [];

        content[1].data.forEach((item, pos) => {
            if (pos == 0) {
                // skip headers
                return;
            }

            var orn = item[transactionIdColumn];
            var source = item[sourceMediumColumn];

            items.push({
                orn: orn,
                source: source
            });
        });

        callback(undefined, {data: items});
    });
}

function checkAndAddActivePromo(details, callback) {
    const executionKey = config.NOTIFICATION_KEY;
    const { prefix, number, orderReferenceNumber, startDate, requestId } = details;

    configManager.loadConfigMapForKey('bonus', 'bonus_port_in_promo_campaign', (err, item) => {
        if (err) {
           return callback(err);
        }

        var promoTag = item ? item.options.promo_category_tag : undefined;
        var startDate = item && item.options.start_date ? new Date(item.options.start_date) : undefined;
        var endDate = item && item.options.end_date ? new Date(item.options.end_date) : undefined;
        var enabled = item ? item.options.enabled : false;
        var currentDate = new Date();

        if (promoTag && enabled && startDate.getTime() < currentDate.getTime() && 
            endDate.getTime() > currentDate.getTime()) {
            promotionsManager.copyCode(item.options.promo_category_tag, (err, promoResult) => {
                if (err) {
                    return callback(err);
                }

                if (promoResult && promoResult.code) {
                    promotionsManager.useCode('CMS', prefix, number, promoResult.code,
                    orderReferenceNumber, executionKey, (err, data) => {
                        if (err) {
                            return callback(err);
                        }

                        const promoLogId = data.promoLogId ? data.promoLogId : null;
                        return callback(undefined, promoLogId);
                    });
                } else {
                    return callback(undefined, null);
                }
                
            });
        } else {
            return callback(undefined, null);
        }
    })
}

function changePlanAndUnsubscribeAddon(cache, packageName, sin, obj, cb) {
    var activationDate = new Date(new Date().getTime() + 8 * 60 * 60 * 1000).toISOString().split('.')[0];
    ec.changeServicePlan({
        billingAccountNumber: cache.billingAccountNumber,
        effectiveDate: activationDate,
        packageName: packageName,
        remarks: "Customer Request",
        serviceInstanceNumber: sin
    }, (err, result) => {
        if (err) {
            return cb(new BaseError("Failed to switch plan: " + err.message, "ERROR_BSS"));
        }
        if (!result || !result.return) {
            return cb(new BaseError("Failed to switch plan: empty response", "ERROR_BSS_INVALID_RESPONSE"));
        }
        if (result.return.responseCode !== "0") {
            return cb(new BaseError("Failed to switch plan: " + result.return.responseMessage
                + " (" + result.return.responseCode + ")",
                "ERROR_BSS_UNKNOWN"));
        }
        const { params, initiator, executionKey } = obj;
        const saveObj = {
            type: 'CIRCLE_SWITCH_PLAN_CHANGE',
            initiator,
            executionKey,
            params,
            result,
            changes: {},
        };
        db.notifications_port_in.insert(saveObj, (errLog) => {
            if (errLog) common.error("PortinManager", "changePlanAndUnsubscribeAddon: save log error, error=" + errLog.message);
        });

        var addonIdList = constants.CIRCLES_ONE_ADDON_SUBSCRIPTION_LIST;
        var addonsList = [];
        addonIdList.forEach(function (id) {
            var ecPackage = ec.findPackage(ec.packages(), id);
            if (!ecPackage) {
                common.error("Addon not found for id", id);
            }
            var addon = {
                id: id,
                action: "PUT",
                historyIds: undefined,
                suppressNotify: true,
                prefix: "65",
                // All these addon should be subscribed with immediate effect
                overrideEffect: "0",
                // All these addons are by default recurrent since they are free addons for circlesone
                overrideRecurrent: "true",
                options: {
                    paymentsDisabled: true,
                    allowMultiple: true
                }
            }
            addonsList.push(addon);
        });
        const product = ec.findPackageName(ec.packages(), 'Free Plus 2020');
        const historyIds = packageManager.getCustomerGeneralHistoryIds(product.id, cache);
        var free2020Addon = {
            id: product.id,
            action: "unsubscribe",
            historyIds: historyIds,
            prefix: "65",
            suppressNotify: undefined,
            overrideEffect: "0",
            overrideRecurrent: undefined,
            options: {}
        }
        addonsList.push(free2020Addon);
        result = [];
        async.eachLimit(addonsList, 1, function (addon, nextAddon) {
            packageManager.addonUpdate(addon.prefix, cache.number, addon.action, addon.id,
                addon.historyIds, addon.suppressNotify, (err, addonUpdateResult) => {
                if (err) {
                    common.log('PortInManager.changePlanAndUnsubscribeAddon() Error on addonUpdate', err);
                    return nextAddon(err);
                }
                result.push(addonUpdateResult);
                return nextAddon();
            }, addon.overrideEffect, addon.overrideRecurrent, addon.options);
        }, function (err) {
            return cb(err, result);
        });
    });
}
