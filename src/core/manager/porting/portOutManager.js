var common = require(__lib + '/common');
var db = require(__lib + '/db_handler');
var ec = require(__lib + '/elitecore');
var packageManager = require(__manager + '/packages/packageManager');
var elitecoreConnector = require(__lib + '/manager/ec/elitecoreConnector');

module.exports = {

    loadRequest: function (serviceInstanceNumber, options, callback, limit, offset,
                           filter, orderColumn, orderDirection, warningsOnly, startDate, endDate) {
        if (!callback) callback = () => {
        }
        if (!options) {
            options = {}
        }

        var donorCode = options.donorCode;
        var source = options.source;
        var status = options.status;
        var uniqueCustomer = options.uniqueType == "CUSTOMER";

        var statuses = "";
        if (status === "ACTIVE") {
            statuses = db.escape("WAITING") + "," + db.escape("WAITING_APPROVE") + ","
            + db.escape("WAITING_RETRY") + "," + db.escape("IN_PROGRESS") + "," + db.escape("DONOR_APPROVED");
        } else if (status === "ALL") {
            statuses = db.escape("WAITING") + "," + db.escape("WAITING_APPROVE") + ","
            + db.escape("WAITING_RETRY") + "," + db.escape("IN_PROGRESS") + "," + db.escape("DONOR_APPROVED")
            + "," + db.escape("FAILED") + "," + db.escape("DONE") + "," + db.escape("CANCELED");
        } else {
            statuses = db.escape(status);
        }

        if (orderColumn == "UPDATE_DATE") {
            orderColumn = "status_update_ts";
        } else if (orderColumn == "START_DATE") {
            orderColumn = "start_ts";
        } else if (orderColumn == "WARNING_DATE") {
            orderColumn = "warning_after_ts";
        } else {
            orderColumn = "id";
        }
        if (!orderDirection) {
            orderDirection = "DESC";
        }

        var selectQueryWhereSection = "customerPortOut.status IN (" + statuses + ")" +
            (warningsOnly ? " AND warning_after_ts < " + db.escape(new Date()) : "") +
            (startDate ? " AND start_ts > " + db.escape(startDate) : "") +
            (endDate ? " AND start_ts < " + db.escape(endDate) : "") +
            (source ? " AND customerPortOut.source=" + db.escape(source) : "") +
            (donorCode ? " AND customerPortOut.donor_network_code=" + db.escape(donorCode) : "") +
            (uniqueCustomer ? " AND customerPortOut.old=0" : "") +
            (serviceInstanceNumber ? " AND customerProfile.service_instance_number=" + db.escape(serviceInstanceNumber) : "") +
            (filter ? " AND (customerProfile.service_instance_number LIKE " + db.escape("%" + filter + "%") +
            " OR customerPortOut.donor_network LIKE " + db.escape("%" + filter + "%") +
            " OR customerPortOut.donor_network_code LIKE " + db.escape("%" + filter + "%") +
            " OR customerPortOut.number LIKE " + db.escape("%" + filter + "%") +
            " OR customerPortOut.temp_number LIKE " + db.escape("%" + filter + "%") +
            " OR customerPortOut.source LIKE " + db.escape("%" + filter + "%") +
            " OR customerPortOut.status LIKE " + db.escape("%" + filter + "%") +
            " OR customerPortOut.error_status LIKE " + db.escape("%" + filter + "%") + ")"
                : "");

        var selectQuery = "SELECT " +
            " customerPortOut.id as id, customerProfile.service_instance_number, customerPortOut.* " +
            " FROM customerPortOut LEFT OUTER JOIN customerProfile ON (customerProfile.id = customerPortOut.profile_id) " +
            " WHERE " + selectQueryWhereSection +
            " ORDER BY " + orderColumn + " " + orderDirection +
            (limit > 0 ? " LIMIT " + db.escape(limit) : "") +
            (offset > 0 ? " OFFSET " + db.escape(offset) : "");
        db.query_err(selectQuery, function (err, rows) {
            if (err) {
                common.error("PortinManager", "Failed to load port in requests, err=" + err.message);
                return callback(new Error("Bonus is not added into db", "DB_ERROR"));
            }

            var selectCount = "SELECT count(*) as count FROM customerPortOut LEFT OUTER JOIN customerProfile ON " +
                "(customerProfile.id = customerPortOut.profile_id) WHERE " + selectQueryWhereSection;

            db.query_err(selectCount, function (err, countResult) {
                if (err) {
                    common.error("PortinManager", "Failed to load port in requests, err=" + err.message);
                    return callback(new Error("Bonus is not added into db", "DB_ERROR"));
                }

                var totalCount = countResult && countResult.length > 0
                    ? countResult[0].count : 0;

                if (!rows || !rows.length) {
                    return callback(undefined, {
                        totalCount: totalCount,
                        data: []
                    });
                }

                callback(undefined, {
                    totalCount: totalCount,
                    data: rows
                });
            });
        });
    },

    reattachInventory: function (msisdn, serviceInstanceNumber, callback) {
        var result = new Object;
        Promise.resolve().then(() => {
            return elitecoreConnector.getPromise(elitecoreConnector.doInventoryStatusOperation, {
                "inventoryNumber" : msisdn,
                "operationAlias" : "FREE",
                "validResponses" : [ { "code" : 1 } ]
            });
        })
        .then((updateStatusResult) => {
            if (!updateStatusResult) return new Error("Update Inventory Failed");
            result.updateStatusResult = updateStatusResult;
            return elitecoreConnector.getPromise(elitecoreConnector.addInventory, {
                "inventoryNumber" : msisdn,
                "serviceInstanceNumber" : serviceInstanceNumber,
                "validResponses" : [ { "code" : 0 } ]
            });
        })
        .then((addInventoryResult) => {
            result.addInventoryResult = addInventoryResult;
            return callback(undefined, result);
        })
        .catch((err) => {
            return callback(err);
        });
    },

    scheduleTermination: function(serviceInstanceNumber, callback) {
        var effective = new Date(new Date().getFullYear(), new Date().getMonth()+1, 0, 2, 45); // last day of month SGT 10:45 am
        var checkIfCharge = function(cb) {
            ec.getCustomerDetailsServiceInstance(serviceInstanceNumber, false, function (err, cache) {
                if (err || !cache) return cb(new Error("Cache not found"));
                var earliest = new Date(cache.serviceInstanceEarliestTerminationDate);
                if (new Date(earliest.getFullYear(), earliest.getMonth(), earliest.getDate()) <=
                    new Date(effective.getFullYear(), effective.getMonth(), effective.getDate()) ) {
                    cb(undefined, { "charge" : false });
                } else {
                    // earliest is next month, so need to charge
                    packageManager.addonUpdate(cache.prefix, cache.number, "subscribe", packageManager.prematureTermCharge, undefined, true,
                            function(pErr, pResult) {
                        cb(undefined, { "charge" : true, "chargeError" : pErr, "chargeResult" : pResult });
                    });
                }
            });
        }
        var obj = {
            "start" : effective.getTime(),
            "serviceInstanceNumber" : serviceInstanceNumber,
            "action" : "terminate",
            "activity" : "termination_portout",
            "allDay" : false,
            "trigger" : "[portout]",
            "parallelLimitOne" : true,
            "title" : "Terminate " + common.escapeHtml(serviceInstanceNumber),
        }
        db.scheduler.find(obj).toArray(function(fErr, fRes) {
            if (fRes && fRes.length > 0) return callback(new Error("Multiple schedule!"));
            checkIfCharge(function(err, result) {
                obj.createdDate = new Date().getTime();
                obj.charges = result;
                db.scheduler.insert(obj, callback);
            });
        });
    }

}
