var request = require('request');
var async = require('async');

var config = require('../../../../config');
var common = require('../../../../lib/common');
var db = require('../../../../lib/db_handler');
var ec = require('../../../../lib/elitecore');

var BaseError = require('../../errors/baseError');

var LOG = config.LOGSENABLED;

var DEFAULT_VALUE = [
    {option_key: "autoboost_limit", value: -1, type: "Integer"},
    {option_key: "roaming_cap_notification_amount", value: 20, type: "Integer"},
    {option_key: "golden_circles", value: false, type: "Boolean"},
    {option_key: "location_based_service_enable", value: true, type: "Boolean"}
];

module.exports = {

    /**
     *
     *
     */
    updateProfileDetails: function (prefix, number, profileFirstName, profileLastName, profileNickName, profileImage,
                                    executionKey, callback) {
        ec.getCustomerDetailsNumber(prefix, number, false, function (err, cache) {
            if (err || !cache) {
                return callback(new Error("User is not found in db", "USER_NOT_FOUND"));
            }

            checkDefaultProfile(cache.serviceInstanceNumber, function (err) {
                if (err) {
                    return callback(err);
                }

                var fields = "";
                var updatedFields = [];
                var addParam = function (key, value) {
                    if (fields.length > 0) fields += ",";
                    fields += (key + "=" + db.escape(value));
                    updatedFields.push(key);
                }

                if (profileFirstName || profileFirstName === "") addParam("first_name", profileFirstName);
                if (profileLastName || profileLastName === "") addParam("last_name", profileLastName);
                if (profileNickName || profileNickName === "") addParam("nick_name", profileNickName);
                if (profileImage || profileImage === "") addParam("picture", profileImage);

                var insertQuery = "UPDATE customerProfile SET " + fields +
                    " WHERE service_instance_number=" + db.escape(cache.serviceInstanceNumber);

                db.query_err(insertQuery, function (err, rows) {
                    if (err || !rows || !rows.affectedRows) {
                        if (LOG) common.log("ProfileManager", "can not update profile in db");
                        return callback(new Error("Profile update failed in db", "DB_ERROR"));
                    }

                    return callback(undefined, {
                        prefix: prefix,
                        number: number,
                        serviceInstanceNumber: cache.serviceInstanceNumber,
                        updatedFields: updatedFields
                    });
                });
            });
        });
    },

    /**
     *
     *
     */
    loadProfileDetails: function (prefix, number, executionKey, callback) {
        ec.getCustomerDetailsNumber(prefix, number, false, function (err, cache) {
            if (err || !cache) {
                return callback(new Error("User is not found in db", "USER_NOT_FOUND"));
            }

            module.exports.loadProfileDetailsBySI(cache.serviceInstanceNumber, callback);
        });
    },

    /**
     *
     *
     */
    loadProfileDetailsBySI: function (serviceInstanceNumber, callback) {
        ec.getCustomerDetailsServiceInstance(serviceInstanceNumber, false, function (err, cache) {
            if (err) {
                return callback(err);
            }
            if (!cache) {
                return callback(new Error("User is not found in EC", "ERROR_CUSTOMER_NOT_FOUND"));
            }

            checkDefaultProfile(serviceInstanceNumber, function (err, profileData) {
                if (err) {
                    return callback(err);
                }

                profileData.profileId = profileData.id;
                return callback(undefined, profileData);
            });
        });
    },

    /**
     *
     *
     */
    saveSetting: function (prefix, number, appType, optionId, optionType, optionValue, executionKey, callback) {
        ec.getCustomerDetailsNumber(prefix, number, false, function (err, cache) {
            if (err || !cache) {
                return callback(new Error("User is not found in db", "USER_NOT_FOUND"));
            }

            checkDefaultProfile(cache.serviceInstanceNumber, function (err, profile) {
                if (err) {
                    return callback(err);
                }

                if (optionType === "Integer") {
                    optionValue = "" + optionValue;
                } else if (optionType === "Boolean") {
                    optionValue = (optionValue == true || optionValue == "true"
                    || optionValue == 1 || optionValue == "1") ? "1" : "0";
                } else if (optionType === "String") {
                    // do nothing
                } else {
                    return callback(new Error("Unsupported type type, optionType=" + optionType));
                }

                var insertQuery = "INSERT INTO customerOptions (profile_id, option_key, app_type, value, type) "
                    + "VALUES (" + db.escape(profile.id) + "," + db.escape(optionId) + "," + db.escape(appType) + ","
                    + db.escape(optionValue) + "," + db.escape(optionType) + ") "
                    + "ON DUPLICATE KEY UPDATE value=" + db.escape(optionValue) + ", type=" + db.escape(optionType);

                db.query_err(insertQuery, function (err, result) {
                    if (err) {
                        if (LOG) common.error("ProfileManager", "Can not save setting in db, error=" + err.message);
                        return callback(new Error("Can not save setting in db", "DB_ERROR"));
                    }

                    if (!result || result.affectedRows <= 0) {
                        if (LOG) common.log("ProfileManager", "Setting is not inserted");
                        return callback(new Error("Setting is not inserted", "NOT_FOUND"));
                    }

                    var result = new Object;
                    // reset dependent notifications

                    if (appType === "selfcare" && optionId === "autoboost_limit" && optionValue != 0) {
                        module.exports.saveSetting(prefix, number, "selfcare", "autoboost_disabled_notified",
                            "Boolean", false, executionKey, function () {
                                // do nothing
                            });

                        // if a user enables autoboost we have to add an autoboost
                        // addon after checking if the user has run out of data before
                        result.addAutoBoost = true;
                    }

                    return callback(undefined, result);
                });
            });
        });
    },

    /**
     *
     *
     */
    loadSetting: function (prefix, number, appType, executionKey, callback) {
        ec.getCustomerDetailsNumber(prefix, number, false, function (err, cache) {
            if (err || !cache) {
                return callback(new Error("User is not found in db", "USER_NOT_FOUND"));
            }

            module.exports.loadSettingsBySI(cache.serviceInstanceNumber, appType, callback);
        });
    },

    /**
     *
     *
     */
    loadSettingsBySI: function (serviceInstanceNumber, appType, callback) {
        if (!callback) callback = () => {
        }

        ec.getCustomerDetailsService(serviceInstanceNumber, false, (err, cache) => {
            if (err) {
                return callback(err);
            }
            if (!cache) {
                return callback(new BaseError("Customer not found", "ERROR_CUSTOMER_NOT_FOUND"));
            }

            checkDefaultProfile(cache.serviceInstanceNumber, (err, profile) => {
                if (err) {
                    return callback(err);
                }
                if (!profile) {
                    return callback(new BaseError("Customer profile not found", "ERROR_PROFILE_NOT_FOUND"));
                }

                var selectQuery = "SELECT option_key, value, type  FROM customerOptions WHERE profile_id="
                    + db.escape(profile.id) + " AND app_type=" + db.escape(appType);

                db.query_err(selectQuery, (err, rows) => {
                    if (err) {
                        if (LOG) common.error("ProfileManager", "Failed to load customer settings, error=" + err.message);
                        return callback(err);
                    }

                    var keys = [];
                    var result = [];
                    if (rows) rows.forEach((item) => {
                        var optionKey = item.option_key;
                        var optionType = item.type;
                        var optionValue;

                        if (optionType === "Integer") {
                            optionValue = parseInt(item.value);
                        } else if (optionType === "Boolean") {
                            optionValue = (item.value === "1") ? true : false;
                        } else if (optionType === "String") {
                            optionValue = item.value;
                        }

                        keys.push(optionKey);
                        result.push({
                            option_id: optionKey,
                            value_type: optionType,
                            value: optionValue
                        })
                    });

                    DEFAULT_VALUE.forEach(function (item) {
                        if (keys.indexOf(item.option_key) < 0) {
                            result.push({
                                option_id: item.option_key,
                                value_type: item.type,
                                value: item.value
                            })
                        }
                    });

                    return callback(undefined, {settings: result});
                });
            });
        });
    },

    /**
     *
     *
     */
    loadASelfcareSetting: function (prefix, number, key, executionKey, callback) {
        module.exports.loadSetting(prefix, number, "selfcare", executionKey, function (err, result) {
            if (err) {
                return callback(err);
            }

            var type;
            var value;
            if (result && result.settings) {
                result.settings.forEach(function (item) {
                    if (item.option_id === key) {
                        type = item.value_type;
                        value = item.value;
                    }
                });
            }

            if (callback) callback(undefined, type, value);
        });
    },

    /**
     *
     *
     */
    loadASelfcareSettingBySI: function (serviceInstanceNumber, key, callback) {
        if (!callback) callback = () => {
        }

        module.exports.loadSettingsBySI(serviceInstanceNumber, "selfcare", (err, result) => {
            if (err) {
                return callback(err);
            }

            var type;
            var value;
            if (result && result.settings) result.settings.forEach((item) => {
                if (item.option_id === key) {
                    type = item.value_type;
                    value = item.value;
                }
            });

            callback(undefined, type, value);
        });
    },

    /**
     *
     *
     */
    hlrLoadSettings: function (serviceInstanceNumber, callback) {
        var selectQuery = "SELECT MAX(CASE WHEN co.option_key='roaming_auto' THEN co.value END) roaming_auto, " +
            " MAX(CASE WHEN co.option_key='roaming_data' THEN co.value END) roaming_data FROM " +
            " customerOptions co LEFT JOIN customerProfile cp ON co.profile_id = cp.id " +
            " WHERE co.option_key IN ('roaming_auto', 'roaming_data') " +
            " AND cp.service_instance_number=" + db.escape(serviceInstanceNumber);
        db.query_err(selectQuery, function (err, rows) {
            if (rows && rows[0]) callback(err, rows[0]);
            else if (err) callback(err);
            else callback(new Error("empty"))
        });
    },

    /**
     *
     *
     */
    loadHLRPending: function (appType, optionId, optionType, executionKey, callback) {
        var selectQuery = "SELECT cp.service_instance_number, co.value FROM customerOptions co LEFT JOIN customerProfile cp "
            + "ON co.profile_id = cp.id WHERE co.option_key = " + db.escape(optionId) + " AND co.type = "
            + db.escape(optionType) + " AND co.app_type = " + db.escape(appType) + " AND co.value != '0' AND co.value != '1'";
        db.query_err(selectQuery, function (err, rows) {
            if (err) {
                if (LOG) common.error("ProfileManager", "Can not load serviceInstanceNumber from db, error=" + err.message);
                return callback(new Error("Can not load serviceInstanceNumber from db", "DB_ERROR"));
            }
            var list = new Array;
            rows.forEach(function (item) {
                if (item && item.value) {
                    var cmd = item.value.split('_')[0];
                    var productId = item.value.split('_')[1];
                    var historyId = item.value.split('_')[2];
                    var m1RefId = item.value.split('_')[3];
                    var old = item.value.split('_')[4];
                    // backward compat
                    if (typeof(old) == "undefined") {
                        old = m1RefId;
                        m1RefId = historyId;
                    }
                    list.push({
                        "serviceInstanceNumber": item.service_instance_number, "cmd": cmd,
                        "productId": productId, "historyId": historyId, "m1RefId": m1RefId, "old": old
                    });
                }
            });
            return callback(undefined, {"optionId": optionId, "list": list});
        });
    },

    /**
     *
     */
    loadAllProfilesBaseInfo: (callback) => {
        if (!callback) callback = () => {
        }

        var query = "SELECT id, service_instance_number FROM customerProfile";
        db.query_err(query, function (err, rows) {
            if (err) {
                if (LOG) common.log("ProfileManager", "Can not load profile from db");
                return callback(new Error("Profile is not loaded from db", "DB_ERROR"));
            }

            var info = [];
            if (rows) rows.forEach((row) => {
                info.push({
                    id: row.id,
                    serviceInstanceNumber: row.service_instance_number
                });
            });

            callback(undefined, info);
        });
    },
    checkDefaultProfile,
}

function checkDefaultProfile(serviceInstanceNumber, callback) {
    ec.getCustomerDetailsServiceInstance(serviceInstanceNumber, false, function (err, cache) {
        if (err) {
            return callback(err);
        }
        if (!cache) {
            return callback(new Error("User is not found in EC", "ERROR_CUSTOMER_NOT_FOUND"));
        }

        var selectQuery = "SELECT id, first_name as firstname, last_name as lastname, nick_name as nickname, picture as app_picture FROM customerProfile " +
            " WHERE service_instance_number=" + db.escape(cache.serviceInstanceNumber);

        db.query_err(selectQuery, function (err, rows) {
            if (err) {
                common.error('ProfileManager.checkDefaultProfile() Error: Can not load profile from db ', err);
                return callback(new Error("Profile is not loaded from db", "DB_ERROR"));
            }

            if (rows && rows.length > 0) {
                return callback(undefined, {
                    id: rows[0].id,
                    prefix: cache.prefix,
                    number: cache.number,
                    app_picture: rows[0].app_picture,
                    nickname: rows[0].nickname,
                    firstname: rows[0].firstname,
                    lastname: rows[0].lastname,
                    serviceInstanceNumber: cache.serviceInstanceNumber,
                    createdNew: false
                });
            }

            var fields = "";
            var values = "";

            var addParam = function (key, value) {
                if (fields.length > 0) fields += ",";
                if (values.length > 0) values += ",";
                fields += key;
                values += db.escape(value);
            }

            var info = generateCustomerDefaultRandomInfo(cache.billingFullName);
            addParam("nick_name", info.nickName);
            addParam("picture", info.defaultImage);
            addParam("service_instance_number", cache.serviceInstanceNumber);

            var insertQuery = "INSERT INTO customerProfile (" + fields + ") VALUES (" + values + ")";

            db.query_err(insertQuery, function (err, rows) {
                if (err && err.code === 'ER_DUP_ENTRY') {
                    common.error('ProfileManager.checkDefaultProfile() Error: Duplicate profile entry ', err);

                    db.query_err(selectQuery, (err, rows1) => {
                        if (err) {
                            common.error('ProfileManager.checkDefaultProfile() Error: Can not load profile from db ', err);
                            return callback(new Error("Profile is not loaded from db after retry", "DB_ERROR"));
                        }

                        if (rows1 && rows1.length > 0) {
                            return callback(undefined, {
                                id: rows1[0].id,
                                prefix: cache.prefix,
                                number: cache.number,
                                app_picture: rows1[0].app_picture,
                                nickname: rows1[0].nickname,
                                firstname: rows1[0].firstname,
                                lastname: rows1[0].lastname,
                                serviceInstanceNumber: cache.serviceInstanceNumber,
                                createdNew: false
                            });
                        }else {
                            common.error('ProfileManager.checkDefaultProfile() Error: Can not load profile from db', err);
                            return callback(new Error("After second retry profile not found", "DB_ERROR"));
                        }
                    });
                }else if (err || !rows || !rows.affectedRows) {
                    common.error('ProfileManager.checkDefaultProfile() Error: Profile is not added into db ', err);
                    return callback(new Error("Profile is not added into db", "DB_ERROR"));
                }

                // timeout is required due to db errors, sometimes
                // we can not get query value right after saving that in db

                setTimeout(()=> {
                    return callback(undefined, {
                        id: rows.insertId,
                        prefix: cache.prefix,
                        number: cache.number,
                        app_picture: info.defaultImage,
                        nickname: info.nickName,
                        serviceInstanceNumber: cache.serviceInstanceNumber,
                        createdNew: true
                    });
                }, 1000);
            });
        });
    });
}

function generateCustomerDefaultRandomInfo(name) {
    var nameArray = name ? name.split(' ') : undefined;
    var firstName = nameArray && nameArray.length > 0 ? nameArray[0].toLowerCase() : undefined;
    firstName = firstName && firstName.length > 1 ? (firstName.charAt(0).toUpperCase() + firstName.slice(1)) : "Customer";

    var nickName = firstName + Math.floor(Math.random() * 899 + 100);
    var defaultImage = "default" + Math.floor(Math.random() * 5 + 1);

    return {
        nickName: nickName,
        defaultImage: defaultImage
    }
}


