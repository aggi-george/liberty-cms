var async = require('async');

var config = require('../../../../config');
var common = require('../../../../lib/common');
var db = require('../../../../lib/db_handler');
var betaUsersManager = require('../beta/betaUsersManager');
var profileManager = require('../profile/profileManager');
var elitecoreUserService = require('../../../../lib/manager/ec/elitecoreUserService');
var notificationSend = require('../notifications/send');

var BaseError = require('../../errors/baseError');

const moment = require('moment');

var LOG = config.LOGSENABLED;

module.exports = {

    /**
     *
     */
    loadProvider: (id, callback) => {
        module.exports.loadProviders({id: id}, (err, result) => {
            callback(err, result ? result[0] : undefined);
        })
    },

    /**
     *
     */
    loadProviders: function (options, callback) {
        if (!callback) callback = () => {
        }
        if (!options) options = () => {
        }

        var where = (options.id ? " AND id=" + db.escape(options.id) : "");

        var query = "SELECT * FROM locationProviders WHERE 1=1 " + where + " ORDER BY id DESC" +
            (options.limit > 0 ? " LIMIT " + db.escape(options.limit) : "") +
            (options.offset > 0 ? " OFFSET " + db.escape(options.offset) : "");
        db.query_err(query, function (err, rows) {
            if (err) {
                return callback(err);
            }

            var selectCount = "SELECT count(*) as count FROM locationProviders WHERE 1=1 " + where;
            db.query_err(selectCount, function (err, countResult) {
                if (err) return callback(err);

                var totalCount = countResult && countResult.length > 0 ? countResult[0].count : 0;
                callback(undefined, {
                    totalCount: totalCount,
                    data: rows
                });
            });
        });
    },

    /**
     *
     */
    loadPoint: (id, callback) => {
        module.exports.loadPoints({id: id}, (err, result) => {
            callback(err, result && result.data ? result.data[0] : undefined);
        })
    },

    /**
     *
     */
    loadNearByPoints: (latitude, longitude, radius, limit, callback) => {
        if (!radius) {
            return callback(new BaseError("Invalid params, radius is not defined"));
        }
        if (!latitude || !longitude) {
            return callback(new BaseError("Invalid params, latitude or longitude is not defined"));
        }
        if (!limit) {
            limit = 20;
        }

        module.exports.loadPoints({
            latitude: latitude,
            longitude: longitude,
            radius: radius,
            limit: limit
        }, (err, result) => {
            callback(err, result);
        });
    },

    /**
     *
     */
    loadPoints: function (options, callback) {
        if (!callback) callback = () => {
        }
        if (!options) options = () => {
        }

        var from = "locationPoints JOIN locationProviders ON locationProviders.id=locationPoints.providerId";
        var whereL1 = (options.id ? " AND locationPoints.id=" + db.escape(options.id) : "") +
            (options.providerId ? " AND locationPoints.providerId=" + db.escape(options.providerId) : "") +
            (options.enabled ? " AND locationPoints.enabled=1  AND locationProviders.enabled=1" : "");
        var fields = "locationPoints.*, locationProviders.enabled as providerEnabled, " +
            "locationProviders.name as providerName, locationProviders.activity as defaultActivity, " +
            "locationProviders.redemptionTitle, locationProviders.redemptionMessage," +
            "locationProviders.redemptionImageId, locationProviders.redemptionCodeLength," +
            "locationProviders.instruction," +
            "locationProviders.validUnitl," +
            "locationProviders.redeemedMessage," +
            "locationProviders.generateRedemptionCode as providerGenerateRedemptionCode";

        var whereL2 = "";
        if (options.radius && options.latitude > 0 && options.longitude > 0) {
            whereL2 = " AND distance<" + db.escape(options.radius);
            fields += ', ROUND(SQRT((latitude-' + db.escape(options.latitude) + ')*(latitude-' + db.escape(options.latitude) + ')' +
            '+(longitude-' + db.escape(options.longitude) + ')*(longitude-' + db.escape(options.longitude) + ')) * 100000) as distance';
        }

        var query = "SELECT * FROM (SELECT " + fields + " FROM " + from + " WHERE 1=1 " + whereL1 + ") p1 WHERE 1=1 " +
            whereL2 + " ORDER BY " + (options.radius ? "distance ASC" : "id DESC") +
            (options.limit > 0 ? " LIMIT " + db.escape(options.limit) : "") +
            (options.offset > 0 ? " OFFSET " + db.escape(options.offset) : "");

        db.query_err(query, function (err, rows) {
            if (err) {
                return callback(err);
            }

            var selectCount = "SELECT count(*) as count FROM " + from + " WHERE 1=1 " + whereL1;
            db.query_err(selectCount, function (err, countResult) {
                if (err) return callback(err);

                var totalCount = countResult && countResult.length > 0 ? countResult[0].count : 0;
                callback(undefined, {
                    totalCount: totalCount,
                    data: rows
                });
            });
        });
    },

    /**
     *
     */
    createUpdateProvider: function (options, callback) {
        var id = options.id;
        var update;
        var query;

        if (id) {
            update = true;
            query = "UPDATE locationProviders SET name=" + db.escape(options.name) +
            ", enabled=" + db.escape(options.enabled) +
            ", activity=" + db.escape(options.activity) +
            ", redemptionCodeLength=" + (options.redemptionCodeLength ? db.escape(options.redemptionCodeLength) : 0) +
            ", redemptionImageId=" + (options.redemptionImageId ? db.escape(options.redemptionImageId) : "NULL") +
            ", redemptionTitle=" + (options.redemptionTitle ? db.escape(options.redemptionTitle) : "NULL") +
            ", redemptionMessage=" + (options.redemptionMessage ? db.escape(options.redemptionMessage) : "NULL") +
            ", generateRedemptionCode=" + db.escape(options.generateRedemptionCode) +
            ", instruction=" + db.escape(options.instruction) +
            ", validUnitl=" + db.escape(options.validUntil) +
            ", redeemedMessage=" + db.escape(options.redeemedMessage) +
            " WHERE id=" + db.escape(parseInt(id));
        } else {
            update = false;
            query = "INSERT INTO locationProviders (name, enabled, activity, redemptionCodeLength, " +
            "redemptionImageId, redemptionTitle, redemptionMessage, generateRedemptionCode) " +
            "VALUES (" + db.escape(options.name) +
            "," + db.escape(options.enabled) +
            "," + db.escape(options.activity) +
            "," + (options.redemptionCodeLength ? db.escape(options.redemptionCodeLength) : 0) +
            "," + (options.redemptionImageId ? db.escape(options.redemptionImageId) : "NULL") +
            "," + (options.redemptionTitle ? db.escape(options.redemptionTitle) : "NULL") +
            "," + (options.redemptionMessage ? db.escape(options.redemptionMessage) : "NULL") +
            "," + db.escape(options.generateRedemptionCode) + ")";
        }

        db.query_err(query, function (err, result) {
            if (err) {
                return callback(err);
            }

            callback(undefined, {
                id: !update ? result.insertId : id,
                added: !update,
                updated: update
            });
        });
    },

    /**
     *
     */
    createUpdatePoint: function (options, callback) {
        var id = options.id;
        var update;
        var query;

        if (id) {
            update = true;
            query = "UPDATE locationPoints SET label=" + db.escape(options.label) +
            ", providerId=" + db.escape(options.providerId) +
            ", enabled=" + db.escape(options.enabled) +
            ", activity=" + db.escape(options.activity) +
            ", latitude=" + db.escape(options.latitude) +
            ", longitude=" + db.escape(options.longitude) +
            ", radius=" + db.escape(options.radius) +
            ", generateRedemptionCode=" + db.escape(options.generateRedemptionCode) +
            ", redemptionCode=" + db.escape(options.redemptionCode) +
            " WHERE id=" + db.escape(parseInt(id));
        } else {
            update = false;
            query = "INSERT INTO locationPoints (label, providerId, enabled, activity, " +
            "latitude, longitude, radius, generateRedemptionCode, redemptionCode) " +
            "VALUES (" + db.escape(options.label) +
            "," + db.escape(options.providerId) +
            "," + db.escape(options.enabled) +
            "," + db.escape(options.activity) +
            "," + db.escape(options.latitude) +
            "," + db.escape(options.longitude) +
            "," + db.escape(options.radius) +
            "," + db.escape(options.generateRedemptionCode) +
            "," + db.escape(options.redemptionCode) + ")";
        }

        db.query_err(query, function (err, result) {
            if (err) {
                return callback(err);
            }

            callback(undefined, {
                id: !update ? result.insertId : id,
                added: !update,
                updated: update
            });
        });
    },

    /**
     *
     */
    loadRedemptionDialog: (promoId, callback) => {
        if (!callback) callback = () => {
        }
        if (!promoId) {
            return callback(new BaseError("Invalid params", "ERROR_INVALID_PARAMS"));
        }

        var query = " SELECT lpr.id, lpr.profileId, lpr.providerId, lpr.redeemCode, lp.name as providerName, lp.redemptionTitle as title, " +
            "lp.redemptionMessage as message, lp.redemptionImageId as imageId, redemptionCodeLength as codeLength, " +
            "lp.generateRedemptionCode as generation FROM locationPromoRedemptions lpr LEFT JOIN locationProviders lp " +
            "ON lpr.providerId=lp.id WHERE lpr.id=" + db.escape(promoId);

        db.query_err(query, function (err, result) {
            if (err) {
                return callback(err);
            }
            if (!result || !result[0]) {
                return callback(new BaseError("Redemption dialog is not found",
                    "ERROR_REDEMPTION_DIALOG_NOT_FOUND"));
            }
            if (!result[0].generation) {
                return callback(new BaseError("Redemption dialog has been disabled for th promo provider",
                    "ERROR_REDEMPTION_DISABLED"));
            }

            callback(undefined, result[0]);
        });
    },

    /**
     *
     */
    redeemPromo: function (promoId, redemptionCode, callback) {
        if (!callback) callback = () => {
        }
        if (!promoId || !redemptionCode) {
            return callback(new BaseError("Invalid params, redemptionCode or promoId is not defined", "ERROR_INVALID_PARAMS"));
        }

        var query = "SELECT * FROM locationPoints WHERE redemptionCode=" + db.escape(redemptionCode) +
            " AND providerId IN (SELECT providerId FROM locationPromoRedemptions WHERE id=" + db.escape(parseInt(promoId)) + ")";
        db.query_err(query, function (err, result) {
            if (err) {
                return callback(err);
            }
            if (!result || !result[0]) {
                return callback(new BaseError("Redemption code " + redemptionCode +
                " can not be used for point with id " + promoId, "ERROR_INVALID_CODE"));
            }

            var update = "UPDATE locationPromoRedemptions SET redeemCode=" + db.escape(redemptionCode) +
                ", redeemAt=" + db.escape(new Date()) + ", uniqueKey=NULL WHERE id=" + db.escape(parseInt(promoId));
            db.query_err(update, function (err, result) {
                if (err) {
                    return callback(err);
                }

                callback();
            });
        });
    },

    /**
     *
     */
    removeProvider: function (id, callback) {
        var query = "DELETE FROM locationProviders WHERE id=" + db.escape(id);
        db.query_err(query, function (err, result) {
            if (err) {
                return callback(err);
            }

            callback();
        });
    },

    /**
     *
     */
    removePoint: function (id, callback) {
        var query = "DELETE FROM locationPoints WHERE id=" + db.escape(id);
        db.query_err(query, function (err, result) {
            if (err) {
                return callback(err);
            }

            callback();
        });
    },

    /**
     *
     */
    removePromo: function (id, callback) {
        var query = "DELETE FROM locationPromoRedemptions WHERE id=" + db.escape(id);
        db.query_err(query, function (err, result) {
            if (err) {
                return callback(err);
            }

            callback();
        });
    },

    /**
     *
     */
    saveCustomerLocation: function (locationId, serviceInstanceNumber, options, callback) {
        if (!callback) callback = () => {
        }
        if (!options) options = {};

        module.exports.loadPoint(locationId, (err, point) => {
            if (err) {
                return callback(err);
            }
            if (!point) {
                return callback(new BaseError("Location with " + locationId + " ID is not found",
                    "ERROR_LOCATION_NOT_FOUND"));
            }

            elitecoreUserService.loadUserInfoByServiceInstanceNumber(serviceInstanceNumber, (err, cache) => {
                if (err) {
                    return callback(err);
                }
                if (!cache) {
                    return callback(new BaseError("Customer with SIN " + serviceInstanceNumber + " is not found",
                        "ERROR_CUSTOMER_NOT_FOUND"));
                }

                profileManager.loadProfileDetailsBySI(serviceInstanceNumber, (err, result) => {
                    if (err) {
                        return callback(err);
                    }
                    if (!result) {
                        return callback(new BaseError("Customer profile with SIN " + serviceInstanceNumber + " is not found",
                            "ERROR_PROFILE_NOT_FOUND"));
                    }

                    var key = "location_based_service_enable";
                    profileManager.loadASelfcareSettingBySI(serviceInstanceNumber, key, (err, type, lbsEnable) => {
                        if (err) {
                            return callback(err);
                        }

                        var profileId = result.profileId;
                        betaUsersManager.isBetaCustomer(cache.number, 'circles_internal', (betaErr, betaCustomer) => {

                            var obj = {
                                pointId: point.id,
                                providerId: point.providerId,
                                latitude: options.latitude,
                                longitude: options.longitude,
                                radius: options.radius,
                                prefix: cache.prefix,
                                number: cache.number,
                                betaCustomer: betaCustomer,
                                lbsEnable: lbsEnable,
                                customerAccountNumber: cache.customerAccountNumber,
                                serviceInstanceNumber: cache.serviceInstanceNumber,
                                billingAccountNumber: cache.billingAccountNumber,
                                profileId: profileId,
                                point: point,
                                ts: new Date().getTime()
                            };

                            var saveLog = (err) => {
                                if (err) {
                                    obj.error = err.message;
                                }

                                db.customerLocations.insert(obj, function (err) {
                                    if (err) {
                                        return callback(err);
                                    }
                                    callback();
                                });
                            }

                            var sendNotification = (locationPromoId) => {
                                const validUntil = moment(point.validUnitl);
                                var notification = {
                                    teamID: 5,
                                    teamName: "Operations",
                                    activity: activity,
                                    delay: -1,
                                    prefix: "65",
                                    number: cache.number,
                                    name: cache.billingFullName,
                                    email: cache.email,
                                    customerAccountNumber: cache.customerAccountNumber,
                                    serviceInstanceNumber: cache.serviceInstanceNumber,
                                    billingAccountNumber: cache.billingAccountNumber,
                                    pointLabel: point.label,
                                    pointProvider: point.providerName,
                                    locationPromoId: parseInt(locationPromoId),
                                    redeemedMessage: point.redeemedMessage,
                                    validUntil: `${validUntil.date()} ${validUntil.format('MMMM')} ${validUntil.year()}`,
                                    instruction: point.instruction,
                                };

                                obj.notification = common.deepCopy(notification);
                                notificationSend.deliver(notification, null, (err) => {
                                    if (err) common.error("LocationManager", "saveCustomerLocation: " +
                                    "failed to send notification, error=" + err.message);
                                });
                            }

                            var activity = point.activity ? point.activity : point.defaultActivity;
                            if (!activity || !lbsEnable || !point.providerEnabled || !point.enabled) {
                                return saveLog();
                            }

                            if (!point.providerGenerateRedemptionCode) {
                                // simply send a notification if promos are not enabled for a provider
                                // such notifications should contain information only
                                sendNotification();
                                return saveLog();
                            }

                            if (!point.generateRedemptionCode || !point.redemptionCode) {
                                // do not send notification if generation is disabled for
                                // a specific point but enabled for the provided of that point
                                return saveLog(err);
                            }

                            module.exports.loadUpdateExisting(profileId, point.providerId, (err, result) => {
                                if (err) {
                                    common.error("LocationManager", "saveCustomerLocation: " +
                                    "failed to send notification, error=" + err.message);
                                    return saveLog(err);
                                }

                                obj.existingPromo = result;

                                if (result.exists && result.outdated) {
                                    sendNotification(result.id);
                                    return saveLog(err);
                                } else if (!result.exists) {
                                    module.exports.createRedeemOption({
                                        profileId: profileId,
                                        pointId: point.id,
                                        providerId: point.providerId
                                    }, (err, result) => {
                                        if (err) {
                                            common.error("LocationManager", "saveCustomerLocation: " +
                                            "failed to send notification, error=" + err.message);
                                            return saveLog(err);
                                        }

                                        obj.createdPromo = result;
                                        sendNotification(result.id);
                                        return saveLog();
                                    });
                                } else {
                                    // do not send notification if a promo has recently been sent
                                    return saveLog();
                                }
                            });
                        });
                    });
                });
            });
        });
    },

    /**
     *
     */
    createRedeemOption: (options, callback) => {
        var query = "INSERT INTO locationPromoRedemptions (profileId, pointId, uniqueKey, providerId, createdAt, sentAt) " +
            "VALUES (" + db.escape(options.profileId) + "," + db.escape(options.pointId) + "," + db.escape("SINGLE_OPEN") +
            "," + db.escape(options.providerId) + "," + db.escape(new Date()) + "," + db.escape(new Date()) + ")";

        db.query_err(query, function (err, result) {
            if (err) {
                return callback(err);
            }

            callback(undefined, {
                id: result.insertId
            });
        });
    },

    /**
     *
     */
    getGeneratedPromosForPoints: (callback) => {
        var query = "select name, label, total from (select * from (select pointId, count(profileId) as total from locationPromoRedemptions group by pointId) as a join locationPoints ON a.pointId = locationPoints.id) as b join locationProviders ON b.providerId = locationProviders.id";
        
        db.query_err(query, function (err, result) {
            if (err) {
                return callback(err);
            }

            callback(undefined, {
                list: result
            });
        });
    },

    /**
     *
     */
    getRedeemedPromosForPoints: (callback) => {
        var query = "select name, label, total from (select * from (select pointId, count(profileId) as total from locationPromoRedemptions where redeemAt > 0 group by pointId) as a join locationPoints ON a.pointId = locationPoints.id) as b join locationProviders ON b.providerId = locationProviders.id";
        
        db.query_err(query, function (err, result) {
            if (err) {
                return callback(err);
            }

            callback(undefined, {
                list: result
            });
        });
    },

    /**
     *
     */
    getGeneratedPromosForProvider: (callback) => {
        var query = "select name, total from (select providerId, count(profileId) as total from locationPromoRedemptions group by providerId) as a join locationProviders ON a.providerId = locationProviders.id";

        db.query_err(query, function (err, result) {
            if (err) {
                return callback(err);
            }

            callback(undefined, {
                list: result
            });
        });
    },

     /**
     *
     */
    getRedeemedPromosForProvider: (callback) => {
        var query = "select name, total from (select providerId, count(profileId) as total from locationPromoRedemptions where redeemAt > 0 group by providerId) as a join locationProviders ON a.providerId = locationProviders.id";

        db.query_err(query, function (err, result) {
            if (err) {
                return callback(err);
            }

            callback(undefined, {
                list: result
            });
        });
    },

    /**
     *
     */
    loadUpdateExisting: (profileId, providerId, callback) => {
        var query = "SELECT id, sentAt FROM locationPromoRedemptions " +
            "WHERE redeemCode is NULL AND providerId=" + db.escape(providerId) + " AND profileId=" + db.escape(profileId);

        db.query_err(query, function (err, result) {
            if (err) {
                return callback(err);
            }

            if (!result || !result[0]) {
                return callback(undefined, {
                    exists: false
                });
            } else {
                var sentAt = new Date(result[0].sentAt);
                var locationPromoId = result[0].id;

                if (sentAt.getTime() > new Date().getTime() - 10 * 24 * 60 * 60 * 1000) {
                    return callback(undefined, {
                        exists: true,
                        outdated: false,
                        id: locationPromoId
                    });
                }

                var query = "UPDATE locationPromoRedemptions SET sentAt=" + db.escape(new Date()) +
                    "WHERE id=" + db.escape(locationPromoId);
                db.query_err(query, function (err, result) {
                    if (err) {
                        return callback(err);
                    }

                    return callback(undefined, {
                        exists: true,
                        outdated: true,
                        id: locationPromoId
                    });
                });
            }
        });
    },

    /**
     *
     */
    loadLogs: (options, callback) => {
        if (!callback) callback = () => {
        }
        if (!options) options = {};

        var match = {};
        if (options.filter) {
            var search = options.filter.replace("\t", "").trim();
            if (search.length > 64) {
                search = search.slice(0, 64);
            }
            match["$or"] = [
                {"number": new RegExp(common.escapeRegExp(search), "i")},
                {"serviceInstanceNumber": new RegExp(common.escapeRegExp(search), "i")}];
        }
        if (options.providerId) {
            match.providerId = options.providerId;
        }
        if (options.pointId) {
            match.pointId = options.pointId;
        }

        db.customerLocations.find(match, {}).skip(options.offset).sort({"_id": -1})
            .limit(options.limit).toArray((err, items) => {
                if (err) {
                    return callback(err);
                }
                if (!items) {
                    items = [];
                }

                db.customerLocations.count(match, (err, count) => {
                    if (err) {
                        return callback(err);
                    }
                    if (!count) {
                        count = 0;
                    }

                    callback(undefined, {data: items, totalCount: count});
                });
            });
    },

    /**
     *
     */
    loadRedemptions: (options, callback) => {
        if (!callback) callback = () => {
        }
        if (!options) options = () => {
        }

        var from = "locationPromoRedemptions lpr LEFT JOIN customerProfile cp ON (lpr.profileId=cp.id)";
        var where = "1=1" +
            (options.providerId ? " AND lpr.providerId=" + db.escape(options.providerId) : "") +
            (options.filter ? " AND (cp.service_instance_number LIKE " + db.escape("%" + options.filter + "%") +
            " OR lpr.redeemCode LIKE " + db.escape("%" + options.filter + "%") + ")" : "");

        var query = "SELECT lpr.*, cp.service_instance_number as serviceInstanceNumber " +
            "FROM " + from + " WHERE " + where + " ORDER BY id DESC" +
            (options.limit > 0 ? " LIMIT " + db.escape(options.limit) : "") +
            (options.offset > 0 ? " OFFSET " + db.escape(options.offset) : "");

        db.query_err(query, function (err, rows) {
            if (err) {
                return callback(err);
            }

            var selectCount = "SELECT count(*) as count FROM " + from + " WHERE " + where;
            db.query_err(selectCount, function (err, countResult) {
                if (err) return callback(err);

                var totalCount = countResult && countResult.length > 0 ? countResult[0].count : 0;
                callback(undefined, {
                    totalCount: totalCount,
                    data: rows
                });
            });
        });
    }
}
