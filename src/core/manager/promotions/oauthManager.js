var request = require('request');
var async = require('async');

var config = require('../../../../config');
var common = require('../../../../lib/common');
var db = require('../../../../lib/db_handler');
var ec = require('../../../../lib/elitecore');

var log = config.LOGSENABLED;

var BaseError = require('../../errors/baseError');


module.exports = {

    /**
     *
     *
     */
    getSupported: function () {
        return ["OAUTH_UBER_DRIVER_ID", "OAUTH_POEMS_ID"];
    },

    /**
     *
     *
     */
    verifyOAuth: function (type, params, redirectUrl, callback) {
        if (!callback) callback = () => {
        }

        if (!redirectUrl) {
            return callback(new BaseError("Redirect url is empty", "ERROR_EMPTY_OAUTH_REDIRECT_URL"))
        }

        if (type === "OAUTH_UBER_DRIVER_ID") {
            if (!params || !params.code) {
                return callback(new BaseError("Code params is required to continue with Uber Driver OAUth",
                    "ERROR_INVALID_UBER_OAUTH_PARAMS"));
            }

            return module.exports.verifyUberDriverOAuth(params.code, redirectUrl, callback);
        } else if (type === "OAUTH_POEMS_ID") {
            if (!params || !params.uid || !params.msgcode) {
                return callback(new BaseError("Params uid and msgcode are required to continue with POEMS OAUth",
                    "ERROR_INVALID_POEMS_OAUTH_PARAMS"));
            }

            return module.exports.verifyPoemsOAuth(params.msgcode, params.uid, redirectUrl, callback);
        } else {
            return callback(new BaseError("Not supported OAuth type " + type, "ERROR_INVALID_OAUTH_TYPE"));
        }
    },

    /**
     *
     *
     */
    verifyUberDriverOAuth: function (code, redirectUrl, callback) {
        if (!callback) callback = () => {
        }

        if (log) common.log("OAuthManager", "uber: load auth token, code=" + code);
        request({
            uri: "https://login.uber.com/oauth/v2/token",
            method: 'POST',
            timeout: 20000,
            form: {
                client_secret: config.UBER_CLIENT_SECRET,
                client_id: config.UBER_CLIENT_ID,
                grant_type: "authorization_code",
                redirect_uri: redirectUrl,
                code: code
            }
        }, function (err, response, body) {
            if (err) {
                common.error("OAuthManager", "uber: failed to load access token, error=" + err.message);
                return callback(err);
            }

            var result = common.safeParse(body);
            if (!result) {
                common.error("OAuthManager", "uber: failed to load access token (response is empty)");
                return callback(new BaseError("Failed to load access token (response is empty)",
                    "ERROR_INVALID_UBER_OAUTH_RESPONSE"));
            }

            if (result.error) {
                common.error("OAuthManager", "uber: failed to load access token (uber error: " + result.error + ")");
                return callback(new BaseError("Failed to load access token (uber error: "
                + result.error + ")", "ERROR_INVALID_UBER_OAUTH_RESPONSE"));
            }

            if (!result.access_token || !result.token_type) {
                common.error("OAuthManager", "uber: failed to load access token (access token or token type is empty)");
                return callback(new BaseError("Failed to load access token (access token or token type is empty)",
                    "ERROR_INVALID_UBER_OAUTH_RESPONSE"));
            }

            var accessToken = result.access_token;
            var tokenType = result.token_type;
            var refreshToken = result.refresh_token;

            if (log) common.log("OAuthManager", "uber: auth token loaded, code=" + code +
            ", tokenType=" + tokenType + ", accessToken=" + accessToken);

            request({
                uri: "https://api.uber.com/v1/partners/me",
                method: 'GET',
                timeout: 20000,
                headers: {
                    'Authorization': "Bearer " + accessToken
                }
            }, function (err, response, body) {
                if (err) {
                    common.error("OAuthManager", "uber: failed to load profile, error=" + err.message);
                    return callback(err);
                }

                var result = common.safeParse(body);
                if (!result) {
                    common.error("OAuthManager", "uber: failed to load profile (response is empty)");
                    return callback(new BaseError("Failed to load profile (response is empty)",
                        "ERROR_INVALID_UBER_PROFILE_RESPONSE"));
                }

                if (result.error) {
                    common.error("OAuthManager", "uber: failed to load profile (uber error: " + result.error + ")");
                    return callback(new BaseError("Failed to load profile (uber error: "
                    + result.error + ")", "ERROR_INVALID_UBER_PROFILE_RESPONSE"));
                }

                if (result.errors && result.errors.length > 0) {
                    var uberError = result.errors[0];
                    common.error("OAuthManager", "uber: failed to load profile (code: " + result.code + ", title: " + result.title + ")");
                    return callback(new BaseError("Failed to load profile (code: " + uberError.code + ")",
                        (uberError.code === "no_partner_for_user"
                            ? "ERROR_UBER_NO_DRIVER_PROFILE"
                            : "ERROR_INVALID_UBER_PROFILE_RESPONSE")));
                }

                if (result.code === "unauthorized") {
                    common.error("OAuthManager", "uber: failed to load profile (uber error: " + result.message + ")");
                    return callback(new BaseError("Failed to load profile (uber error: "
                    + result.message + ")", "ERROR_INVALID_UBER_PROFILE_UNAUTHORISED"));
                }

                if (!result.driver_id) {
                    common.error("OAuthManager", "uber: failed to load profile (driver_id is empty)" + JSON.stringify(result));
                    return callback(new BaseError("Failed to load profile (driver id is empty)",
                        "ERROR_INVALID_UBER_PROFILE_RESPONSE"));
                }

                if (log) common.log("OAuthManager", "uber: driver info " + JSON.stringify(result));

                var uniqueId = result.driver_id;
                var metadata = {
                    accessToken: accessToken,
                    refreshToken: refreshToken,
                    tokenType: tokenType,
                    profile: result
                };

                if (callback) callback(undefined, {uniqueId: uniqueId, metadata: metadata});
            });
        });
    },

    /**
     *
     *
     */
    verifyPoemsOAuth: function (msgcode, uid, redirectUrl, callback) {
        if (!callback) callback = () => {
        }


        if (msgcode != 1) {
            common.error("OAuthManager", "poems: invalid msg code, msgcode=" + msgcode);
            return callback(new BaseError("Invalid msg code " + msgcode,
                "ERROR_INVALID_POEMS_OAUTH_RESPONSE"));
        }

        if (!uid) {
            common.error("OAuthManager", "uber: invalid uid");
            return callback(new BaseError("Invalid uid " + uid,
                "ERROR_INVALID_POEMS_OAUTH_RESPONSE"));
        }

        var metadata = {
            msgcode: msgcode,
            uid: uid
        };

        callback(undefined, {uniqueId: uid, metadata: metadata});
    }
}
