var async = require('async');
var config = require('../../../../config');
var common = require('../../../../lib/common');
var db = require('../../../../lib/db_handler');
var ec = require('../../../../lib/elitecore');

var promotionsManager = require('./promotionsManager');

var BaseError = require('../../errors/baseError');

var LOG = config.LOGSENABLED;

module.exports = {

    /**
     *
     *
     */
    loadCustomerByUniqueID: function (id, source, callback) {
        module.exports.loadCustomerInfo(undefined, undefined, id, source, callback);
    },

    /**
     *
     *
     */
    loadCustomerInfoByCode: function (code, callback) {
        module.exports.loadCustomerInfo(undefined, code, undefined, undefined, callback);
    },

    /**
     *
     *
     */
    loadCustomerInfo: function (internalId, code, id, source, callback) {
        var selectQuery = "SELECT ec.*, pcL1.category as categoryId, pcL1.max as usageLimit, " +
            "pcL1.code as code, pcL2.code as codeL2, pcL3.code as codeL3," +
            " (SELECT count(*) FROM promoCodesLogs WHERE promoCodesLogs.code = pcL1.code) as usedCountL1, " +
            " (SELECT count(*) FROM promoCodesLogs WHERE promoCodesLogs.code = pcL2.code) as usedCountL2, " +
            " (SELECT count(*) FROM promoCodesLogs WHERE promoCodesLogs.code = pcL3.code) as usedCountL3 " +
            " FROM externalCustomers ec " +
            " LEFT JOIN promoCodes pcL1 ON (pcL1.id=ec.promo_code_id) " +
            " LEFT JOIN promoCodes pcL2 ON (pcL2.id=ec.promo_code_id_level_2) " +
            " LEFT JOIN promoCodes pcL3 ON (pcL3.id=ec.promo_code_id_level_3) " +
            " WHERE " +
            (internalId ? "ec.id=" + db.escape(internalId) :
                (id ? " customer_source_id=" + db.escape(id) + " AND source=" + db.escape(source)
                    : (" (pcL1.code=" + db.escape(code) + " OR pcL2.code=" + db.escape(code) + " OR pcL3.code=" + db.escape(code) + ")")));

        db.query_err(selectQuery, function (err, result) {
            if (err) {
                if (LOG) common.error("PartnerManager", "loadCustomerInfo: can not load customers, error=" + err.message);
                return callback(err);
            }

            if (result && result[0]) {
                var item = result[0];
                callback(undefined, {
                    id: item.id,
                    codeCategoryId: item.categoryId,
                    code: item.code,
                    codeL2: item.codeL2,
                    codeL3: item.codeL3,
                    uniqueId: item.customer_source_id,
                    nric: item.nric,
                    prefix: item.prefix,
                    number: item.number,
                    email: item.email,
                    name: item.name,
                    source: item.source,
                    usageLimit: item.usageLimit,
                    usageCount: item.usedCountL1 + item.usedCountL2 + item.usedCountL3
                });
            } else {
                return callback(undefined, undefined);
            }
        });
    },

    /**
     *
     *
     */
    loadCustomers: function (callback, offset, limit, filter) {

        var selectQueryWhereSection = filter
            ? "(ec.source LIKE " + db.escape("%" + filter + "%") +
        " OR ec.nric LIKE " + db.escape("%" + filter + "%") +
        " OR ec.prefix LIKE " + db.escape("%" + filter + "%") +
        " OR ec.number LIKE " + db.escape("%" + filter + "%") +
        " OR ec.name LIKE " + db.escape("%" + filter + "%") +
        " OR ec.email LIKE " + db.escape("%" + filter + "%") +
        " OR pcL1.code LIKE " + db.escape("%" + filter + "%") + ")"
            : "1=1";

        var where = " FROM externalCustomers ec " +
            " LEFT JOIN promoCodes pcL1 ON (pcL1.id=ec.promo_code_id) " +
            " LEFT JOIN promoCodes pcL2 ON (pcL2.id=ec.promo_code_id_level_2) " +
            " LEFT JOIN promoCodes pcL3 ON (pcL3.id=ec.promo_code_id_level_3) " +
            " WHERE " + selectQueryWhereSection + " ORDER BY ec.id DESC";

        var fields = "ec.*, pcL1.max as usageLimit, pcL1.code as code, pcL2.code as codeL2, pcL3.code as codeL3," +
            " (SELECT count(*) FROM promoCodesLogs WHERE promoCodesLogs.code = pcL1.code) as usedCountL1, " +
            " (SELECT count(*) FROM promoCodesLogs WHERE promoCodesLogs.code = pcL2.code) as usedCountL2, " +
            " (SELECT count(*) FROM promoCodesLogs WHERE promoCodesLogs.code = pcL3.code) as usedCountL3"

        var selectQuery = "SELECT " + fields + " " + where + " " +
            (limit > 0 ? " LIMIT " + db.escape(limit) : "") +
            (offset > 0 ? " OFFSET " + db.escape(offset) : "");

        db.query_err(selectQuery, function (err, result) {
            if (err) {
                common.error("PartnerManager", "loadCustomersByNRIC: can not load customers, err=" + err.message);
                return callback(err);
            }

            var customers = [];
            result.forEach(function (item) {
                customers.push({
                    id: item.id,
                    promoCode: item.code,
                    promoCodeL2: item.codeL2,
                    promoCodeL3: item.codeL3,
                    nric: item.nric,
                    prefix: item.prefix,
                    number: item.number,
                    email: item.email,
                    name: item.name,
                    source: item.source,
                    type: item.type,
                    usageLimit: item.usageLimit,
                    usageCountL1: item.usedCountL1,
                    usageCountL2: item.usedCountL2,
                    usageCountL3: item.usedCountL3
                });
            })

            var selectCount = "SELECT count(*) as count " + where;
            db.query_err(selectCount, function (err, countResult) {
                if (err) {
                    common.error("PortinManager", "Failed to load port in requests, err=" + err.message);
                    return callback(new Error("Bonus is not added into db", "DB_ERROR"));
                }

                var totalCount = countResult && countResult.length > 0 ? countResult[0].count : 0;
                return callback(undefined, {totalCount: totalCount, data: customers});
            });
        });
    },

    /**
     *
     *
     */
    addCustomers: function (list, partner, callback) {
        if (!callback) callback = () => {
        }
        if (partner) {
            partner = partner.toLowerCase();
        }

        var pushTask = function (tasks, uniqueId, fields, category) {
            tasks.push(function (callback) {
                var insertCustomer = function (data, callback) {

                    var name = "";
                    if (fields.firstName) {
                        name += fields.firstName.toUpperCase();
                    }
                    if (fields.lastName) {
                        if (name.length > 0) name += " ";
                        name += fields.lastName.toUpperCase();
                    }
                    if (!data) {
                        data = [];
                    }

                    if (fields.phone) {
                        fields.number = fields.phone;
                    }

                    var insertQuery = "INSERT INTO externalCustomers (source, customer_source_id, prefix, number, " +
                        "name, email, nric, promo_code_id, promo_code_id_level_2, promo_code_id_level_3) VALUES ("
                        + db.escape(partner) + ", "
                        + db.escape(uniqueId) + ", "
                        + (fields.prefix ? db.escape(fields.prefix) : "NULL") + ","
                        + (fields.number ? db.escape(fields.number) : "NULL") + ","
                        + (name ? db.escape(name) : "NULL") + ","
                        + (fields.email ? db.escape(fields.email) : "NULL") + ","
                        + (fields.nric ? db.escape(fields.nric) : "NULL") + ","
                        + (data[0] && data[0].id ? db.escape(data[0].id) : "NULL") + ","
                        + (data[1] && data[1].id ? db.escape(data[1].id) : "NULL") + ","
                        + (data[2] && data[2].id ? db.escape(data[2].id) : "NULL") + ")";

                    if (LOG) common.log("PartnerManager", "addCustomers: uniqueId=" + uniqueId + ", partner=" + partner);
                    db.query_err(insertQuery, function (err, result) {
                        if (err) {
                            if (LOG) common.error("PartnerManager", "addCustomers: can not save customer, error=" + err.message);

                            var alreadyAdded = (err.code === 'ER_DUP_ENTRY');
                            var deleteUnusedCode = "DELETE FROM promoCodes WHERE"
                                + " id=" + db.escape(data[0] && data[0].id ? data[0].id : -1) + " OR "
                                + " id=" + db.escape(data[1] && data[1].id ? data[1].id : -1) + " OR "
                                + " id=" + db.escape(data[2] && data[2].id ? data[2].id : -1);

                            // clear created codes in case of any error
                            db.query_err(deleteUnusedCode, function (deleteErr) {
                                if (deleteErr) {
                                    return callback(deleteErr);
                                }
                                if (alreadyAdded) {
                                    return callback(new BaseError("External customer already exists",
                                        "ERROR_CUSTOMER_ALREADY_EXISTS"));
                                }

                                return callback(err);
                            });

                            return;
                        }

                        return callback(undefined, {
                            id: result.insertId,
                            partner: partner,
                            uniqueId: uniqueId,
                            name: name,
                            prefix: fields.prefix,
                            number: fields.number,
                            email: fields.email
                        });
                    });
                }

                var patterns = [];
                if (category.codePatternL1) patterns.push(category.codePatternL1);
                if (category.codePatternL2) patterns.push(category.codePatternL2);
                if (category.codePatternL3) patterns.push(category.codePatternL3);

                promotionsManager.generatePromoFromPattern(category.id, category.codeValidDays, category.codePrefix,
                    patterns, 5, (err, codeResult) => {
                        if (err) {
                            return callback(err);
                        }

                        insertCustomer(codeResult.data, (err, customerResult) => {
                            if (err) {
                                return callback(err);
                            }

                            callback(undefined, {customer: customerResult, code: codeResult});
                        });
                    });
            });
        }

        var tasks = [];
        list.forEach(function (item) {
            pushTask(tasks, item.uniqueId, item.fields, item.category);
        });

        async.parallelLimit(tasks, 1, function (err, result) {
            if (err) {
                return callback(err);
            }

            return callback(undefined, result);
        });
    },

    /**
     *
     *
     */
    deleteCustomer: function (id, callback) {
        module.exports.loadCustomerInfo(id, undefined, undefined, undefined, (err, customer) => {
            if (err) {
                return callback(err);
            }
            if (!customer) {
                return callback(new Error("Customer not found"));
            }
            if (customer.usageCount > 0) {
                return callback(new Error("Can not remover a customer whose referral " +
                "codes has already been used, code used " + customer.usageCount + " times"));
            }

            var q = "DELETE FROM externalCustomers WHERE id=" + db.escape(id);
            db.query_err(q, function (err) {
                if (err) {
                    return callback(err);
                }

                var q = "DELETE FROM promoCodes WHERE code IN (" + db.escape(customer.code) + ","
                    + db.escape(customer.codeL2) + "," + db.escape(customer.codeL3) + ")";

                db.query_err(q, function (err) {
                    if (err) {
                        return callback(err);
                    }

                    return callback(undefined);
                });
            });
        });


    }
}
