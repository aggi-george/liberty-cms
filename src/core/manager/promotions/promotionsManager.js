var async = require('async');
var crypto = require('crypto');
var config = require('../../../../config');
var common = require('../../../../lib/common');
var db = require('../../../../lib/db_handler');
var ec = require('../../../../lib/elitecore');
var bonusTypesService = require('../../../../res/bonus/bonusTypesService');
var bonusManager = require('../bonus/bonusManager');
var configManager = require('../config/configManager');
var waiverManager = require('../bonus/waiverManager');
var packageManager = require('../packages/packageManager');
const notificationSend = require('../notifications/send');
var constants = require('./../../constants');

var BaseError = require('../../errors/baseError');
var Bluebird = require('bluebird');

var LOG = config.LOGSENABLED;

module.exports = {

    /**
     *
     *
     */
    loadWebPages: function (callback) {
        var query = "SELECT * FROM promoCodesCategories WHERE enable_web_registration=1 AND enabled=1";

        db.query_err(query, function (err, rows) {
            if (err) {
                if (LOG) common.log("PromotionsManager", "query error, error=" + err.message);
                return callback(new BaseError("Internal error", "INTERNAL_ERROR"),
                    {status: "INTERNAL_ERROR"});
            }

            var result = [];

            if (rows) rows.forEach((item) => {

                var fields = [];
                var row = 0;
                var personalEmailField;
                var corporateEmailField;

                if (item.field_name) {
                    fields.push({key: "firstName", title: "First Name", type: "text", limit: 50, row: row, column: 0});
                    fields.push({key: "lastName", title: "Last Name", type: "text", limit: 50, row: row, column: 1});
                    row++;
                }
                if (item.field_email) {
                    personalEmailField = {key: "email", title: "Email", hint: "ex. name@gmail.com", type: "email", limit: 200, row: row};
                    fields.push(personalEmailField);
                    row++;
                }
                if (item.field_password) {
                    fields.push({key: "password", title: "Password", type: "password", limit: 50, row: row});
                    row++;
                }
                if (item.field_nric) {
                    fields.push({key: "nric", title: "NRIC", hint: "ex. G1234567M", type: "text", limit: 12, row: row});
                    row++;
                }
                if (item.field_phone) {
                    fields.push({
                        key: "phone",
                        title: "Phone",
                        hint: "ex. 80880000",
                        type: "number",
                        min: 10000000,
                        max: 99999999,
                        row: row
                    });
                    row++;
                }

                if (item.field_corporate_email) {
                    if (item.unique_field == "schoolEmail") {
                        corporateEmailField = {
                            key: "corporateEmail", title: "School Email (This email is only used to send " +
                            "your student discount sign-up code)", hint: "ex. name@myschool.edu.sg", type: "email", limit: 200, row: row
                        }
                    } else {
                        corporateEmailField = {
                            key: "corporateEmail", title: "Corporate Email (This email is only used to send " +
                            "your corporate plan sign-up code)", hint: "ex. name@mycompany.com", type: "email", limit: 200, row: row
                        }
                    }
                    fields.push(corporateEmailField);
                    row++;
                }

                var supportedExtensions = [];
                if (item.email_extension && item.email_extension.replace(/ /g, "")) {
                    supportedExtensions = item.email_extension.replace(/ /g, "").replace(/@/g, "").split(',');
                }

                if (corporateEmailField && personalEmailField) {
                    personalEmailField.title = "Personal Email (Your account will be set up and managed through this email)"
                    personalEmailField.titleLoggedIn = "Personal Email (The email you’ve already registered with Circles.Life)"
                }

                result.push({
                    path: item.registration_url_path,
                    partner: item.partner,
                    campaignId: item.id,
                    tacUrl: item.tac_url,
                    faqUrl: item.faq_url,
                    registration: {
                        headerImageUrl: item.header_image_url,
                        footerImageUrl: item.footer_image_url,
                        backgroundImageUrl: item.background_image_url,
                        fields: fields,
                        buttons: [{
                            key: "registration",
                            title: item.registration_button_text,
                            titleLoggedIn: item.registration_button_text_logged_in
                                ? item.registration_button_text_logged_in : item.registration_button_text,
                            type: item.registration_type,
                            auth20Url: item.registration_oauth20_url
                        }]
                    },
                    confirmation: {
                        backgroundImageUrl: item.background_image_url,
                        successImageUrl: item.success_image_url
                    },
                    metadata: {
                        supportedEmailExtensions: supportedExtensions
                    }
                });
            });


            return callback(undefined, result);
        });
    },

    /**
     *
     */
    loadCategories: function (id, callback, limit, offset, filter) {
        if (!callback) callback = () => {
        }

        var q = "SELECT id, start as startDate, end as endDate, enabled, name, max, title, message, image1, type, tag, " +
            "partner, multiple_codes_allowed as multipleCodesAllowed, max_level_2 as maxL2, max_level_3 as maxL3, " +
            "enable_web_registration as enableWebRegistration, registration_url_path as registrationUrlPath, tac_url as tacUrl, " +
            "faq_url as faqUrl, header_image_url as headerImageUrl, footer_image_url as footerImageUrl, " +
            "background_image_url as backgroundImageUrl, success_image_url as successImageUrl, field_name as fieldName, " +
            "field_phone as fieldPhone, field_email as fieldEmail, field_password as fieldPassword, field_nric as fieldNric, " +
            "field_corporate_email as fieldCorporateEmail, registration_button_text_logged_in as registrationButtonTextLoggedIn, " +
            "registration_type as registrationType, registration_button_text as registrationButtonText, " +
            "registration_oauth20_url as registrationOAuth20Url, code_prefix as codePrefix, code_valid_days as codeValidDays, " +
            "code_pattern_level_1 as codePatternL1, " +
            "code_pattern_level_2 as codePatternL2, code_pattern_level_3 as codePatternL3, public_name as publicName, " +
            "reminder_date_1 as reminderDate1, reminder_date_2 as reminderDate2, generate_activity as generateActivity, " +
            "reminder_on_day_1 as reminderOnDay1, reminder_on_day_2 as reminderOnDay2, " +
            "reminder_activity_1 as reminderActivity1, reminder_activity_2 as reminderActivity2, usage_activity as usageActivity," +
            "welcome_activity as welcomeActivity, email_extension as emailExtension, unique_field as uniqueField " +
            "FROM promoCodesCategories" + (id ? " WHERE (id=" + db.escape(id) + " OR tag=" + db.escape(id) + ")" : "") + " ORDER BY id DESC" +
            (limit > 0 ? " LIMIT " + db.escape(limit) : "") + (offset > 0 ? " OFFSET " + db.escape(offset) : "");
        db.query_err(q, function (err, rows) {
            if (err) return callback(err);

            // return single category in case if only 1 is reqired
            if (id) return callback(undefined, rows ? rows[0] : undefined);

            var selectCount = "SELECT count(*) as count FROM promoCodesCategories";
            db.query_err(selectCount, function (err, countResult) {
                if (err) return callback(err);

                var totalCount = countResult && countResult.length > 0
                    ? countResult[0].count : 0;

                callback(undefined, {
                    totalCount: totalCount,
                    data: rows
                });
            });
        });
    },

    /**
     *
     */
    copyCode: (tag, callback) => {
        if (!callback) callback = () => {
        }
        if (!tag) {
            return callback(new BaseError("Category with tag " + tag + " is not found", "ERROR_CATEGORY_NOT_FOUND"));
        }

        module.exports.loadCategories(tag, (err, category) => {
            if (err) {
                return callback(err);
            }
            if (!category) {
                return callback(new BaseError("Category with tag " + tag + " is not found", "ERROR_CATEGORY_NOT_FOUND"));
            }
            if (!category.codePrefix || !category.codePatternL1) {
                return callback(new BaseError("Category with tag " + tag + " is not configured properly, " +
                "code prefix or pattern is missing", "ERROR_CATEGORY_INVALID_CONFIG"));
            }

            var patterns = [];
            if (category.codePatternL1) patterns.push(category.codePatternL1);
            if (category.codePatternL2) patterns.push(category.codePatternL2);
            if (category.codePatternL3) patterns.push(category.codePatternL3);

            module.exports.generatePromoFromPattern(category.id, category.codeValidDays, category.codePrefix, patterns, 5,
                (err, codeResult) => {
                    if (err) {
                        return callback(err);
                    }
                    if (!category || !codeResult.data || !codeResult.data[0]) {
                        return callback(new BaseError("Failed to generate code, empty response", "ERROR_INVALID_RESPONSE"));
                    }

                    callback(undefined, codeResult.data[0]);
                }, 0, true);
        });
    },

    /**
     *
     *
     */
    generatePromoFromPattern: function (categoryId, codeValidDays, codePrefix, patterns, randomLength, callback, attempt, addDelay) {
        if (!callback) callback = () => {
        }

        if (!categoryId || !codePrefix || !patterns) {
            return callback(new BaseError("Can not create a promo code, " +
            "mandatory params missing (categoryId, codePrefix, pattern)"))
        }

        if (!attempt) {
            attempt = 0;
        }

        var startDate = codeValidDays > 0 ? new Date() : undefined;
        var endDate = codeValidDays > 0 ? new Date(new Date().getTime() + codeValidDays * 24 * 60 * 60 * 1000) : undefined;

        var addTask = (tasks, code, pattern, level) => {
            tasks.push((callback) => {
                var query = "SELECT * FROM promoCodes WHERE code=" + db.escape(pattern);
                db.query_err(query, function (err, rows) {
                    if (err) {
                        return callback(err)
                    }

                    if (!rows || !rows[0]) {
                        return callback(new BaseError("Pattern " + pattern + " for code creation has not been found",
                            "ERROR_CODE_PATTERN_NOT_FOUND"));
                    }

                    var item = rows[0];
                    var currentCode = code;
                    if (level > 1) {
                        currentCode += ("#" + level);
                    }

                    module.exports.createUpdatePromoCode({
                        code: currentCode,
                        categoryId: categoryId,
                        productId: item.product_id,
                        max: item.max,
                        waiverCents: item.waiver_cents,
                        waiverMonths: item.waiver_month,
                        regDiscountCents: item.reg_discount_cents,
                        deviceDiscountCents: item.device_discount_cents,
                        monthsCount: item.months_count,
                        skuName1: item.sku_name_1,
                        skuKey1: item.sku_key_1,
                        skuQuantity1: item.sku_quantity_1,
                        freeAddon1: item.free_addon_1,
                        freeAddonMonths1: item.free_addon_months_1,
                        extraBillDay: item.extraBillDay,
                        secondVerificationType: item.second_verification_type,
                        enabled: true,
                        start: startDate,
                        end: endDate,
                        type: item.type,
                        bonus_data: item.bonus_data,
                        bonus_data_type: item.bonus_data_type,
                        overrideEffect: item.overrideEffect,
                        paidAddonNextMonth: item.paidAddonNextMonth
                    }, function (err, result) {
                        if (err) {
                            return callback(err);
                        }

                        if (addDelay) {
                            setTimeout(() => {
                                return callback(undefined, result);
                            }, 1000);
                        } else {
                            return callback(undefined, result);
                        }
                    });
                });
            });
        }

        var promoCode = codePrefix + randomPredefinedString(randomLength);
        var tasks = [];

        for (var i = 0; i < patterns.length; i++) {
            addTask(tasks, promoCode, patterns[i], i + 1);
        }

        async.parallelLimit(tasks, 1, function (err, asyncResult) {
            if (err) {
                if (err.code === "ER_DUP_ENTRY" && attempt < 5) {
                    return module.exports.generatePromoFromPattern(categoryId, codeValidDays, codePrefix,
                        patterns, randomLength, callback, attempt + 1, addDelay);
                }

                if (attempt && err.message) {
                    err.message += " (" + attempt + " attempts made)";
                }
                return callback(err);
            }

            if (asyncResult && asyncResult[0] && asyncResult[0].code) {
                return callback(undefined, {code: asyncResult[0].code, endDate: endDate, data: asyncResult});
            } else {
                return callback(new BaseError("No codes were created", "ERROR_FAILED_CREATE_CODES"));
            }
        });
    },

    /**
     *
     *
     */
    generateAPromoCode: function (prefix, postfixLength, params, callback, attempt) {
        params.code = prefix + randomPredefinedString(postfixLength);
        module.exports.createUpdatePromoCode(params, function (err, result) {
            if (err) {
                if (err.code === "ER_DUP_ENTRY" && (!attempt || attempt < 5)) {
                    return module.exports.generateAPromoCode(prefix, postfixLength, params,
                        callback, attempt ? attempt + 1 : 1);
                }

                if (attempt && err.message) {
                    err.message += " (" + attempt + " attempts made)";
                }

                return callback(err);
            }

            return callback(undefined, {
                id: result.id,
                code: params.code
            });
        });
    },

    /**
     *
     *
     */
    createUpdatePromoCode: function (options, callback) {
        var id = options.id;
        var code = options.code;
        var categoryId = options.categoryId;
        var productId = options.productId;
        var max = options.max;
        var waiverCents = options.waiverCents;
        var waiverMonths = options.waiverMonths;
        var regDiscountCents = options.regDiscountCents;
        var deviceDiscountCents = options.deviceDiscountCents;
        var monthsCount = options.monthsCount;
        var skuName1 = options.skuName1;
        var skuKey1 = options.skuKey1;
        var skuQuantity1 = options.skuQuantity1;
        var freeAddon1 = options.freeAddon1;
        var freeAddonMonths1 = options.freeAddonMonths1;
        var extraBillDay = options.extraBillDay;
        var secondVerificationType = options.secondVerificationType;
        var enabled = options.enabled;
        var start = options.start;
        var end = options.end;
        var bonus_data = options.bonus_data;
        var bonus_data_type = options.bonus_data_type;
        var overrideEffect = options.overrideEffect;
        var paidAddonNextMonth = options.paidAddonNextMonth;
        var type = options.type;

        var q = "SELECT start, end FROM promoCodesCategories WHERE id=" + db.escape(categoryId);
        db.query_noerr(q, function (rows) {
            if (rows && rows[0]) {
                var productValue = productId ? db.escape(productId) : "NULL";
                var monthCountValue = monthsCount !== "" ? db.escape(parseInt(monthsCount)) : "0";
                var freeAddonMonths1Value = freeAddonMonths1 !== "" ? db.escape(parseInt(freeAddonMonths1)) : "0";

                var q;
                var update;
                if (id) {
                    update = true;
                    q = "UPDATE promoCodes SET code=" + db.escape(code) +
                    ", product_id=" + productValue +
                    ", category=" + db.escape(parseInt(categoryId)) +
                    ", max=" + db.escape(parseInt(max)) +
                    ", waiver_cents=" + db.escape(parseInt(waiverCents)) +
                    ", waiver_month=" + (waiverMonths ? db.escape(parseInt(waiverMonths)) : db.escape(0)) +
                    ", reg_discount_cents=" + db.escape(parseInt(regDiscountCents)) +
                    ", device_discount_cents=" + db.escape(parseInt(deviceDiscountCents)) +
                    ", months_count=" + monthCountValue +
                    ", sku_name_1=" + db.escape(skuName1) +
                    ", sku_key_1=" + db.escape(skuKey1) +
                    ", sku_quantity_1=" + db.escape(skuQuantity1) +
                    ", free_addon_1=" + db.escape(freeAddon1) +
                    ", free_addon_months_1=" + db.escape(freeAddonMonths1Value) +
                    ", extraBillDay=" + db.escape(extraBillDay > 0 ? extraBillDay : 0) +
                    ", second_verification_type=" + db.escape(secondVerificationType) +
                    ", enabled=" + db.escape(enabled ? 1 : 0) + //make sure we pass boolean
                    ", bonus_data=" + db.escape(bonus_data ? bonus_data : 0) +
                    ", bonus_data_type=" + db.escape(bonus_data_type) +
                    ", overrideEffect=" + db.escape((overrideEffect >= 0) ? overrideEffect : null) +
                    ", paidAddonNextMonth=" + db.escape((paidAddonNextMonth >=0 ) ? paidAddonNextMonth : null) +
                    ", start=" + db.escape(start ? start : "0000-00-00 00:00:00") +
                    ", end=" + db.escape(end ? end : "2999-01-01 00:00:00") +
                    ", type=" + db.escape(type ? type : "DEFAULT") +
                    " WHERE id=" + db.escape(parseInt(id));
                } else {
                    update = false;
                    q = "INSERT INTO promoCodes (code, product_id, category, max, waiver_cents" +
                    ", waiver_month, reg_discount_cents, device_discount_cents, months_count, sku_name_1, sku_key_1, sku_quantity_1" +
                    ", free_addon_1, free_addon_months_1, extraBillDay, second_verification_type, enabled, bonus_data, bonus_data_type, start, end " +
                    ", type, overrideEffect, paidAddonNextMonth) VALUES (" + db.escape(code) + "," + productValue + ","
                    + db.escape(parseInt(categoryId)) + ", " + db.escape(parseInt(max)) + ", "
                    + db.escape(parseInt(waiverCents)) + ", " + db.escape(parseInt(waiverMonths)) + ", "
                    + db.escape(parseInt(regDiscountCents)) + ", "
                    + db.escape(parseInt(deviceDiscountCents)) + ", "
                    + db.escape(monthCountValue) + ", "
                    + db.escape(skuName1) + ", " + db.escape(skuKey1) + ", "
                    + db.escape(skuQuantity1) + ", "
                    + db.escape(freeAddon1) + ", "
                    + db.escape(freeAddonMonths1Value) + ", "
                    + db.escape(extraBillDay > 0 ? extraBillDay : 0) + ", "
                    + db.escape(secondVerificationType) + ", "
                    + db.escape(enabled ? 1 : 0) + ", "  //make sure we pass boolean
                    + db.escape(bonus_data > 0 ? bonus_data : 0) + ", "
                    + db.escape(bonus_data_type) + ", "
                    + db.escape(start ? start : "0000-00-00 00:00:00") + ","
                    + db.escape(end ? end : "2999-01-01 00:00:00") + ","
                    + db.escape(type ? type : "DEFAULT") + ","
                    + db.escape((overrideEffect >= 0) ? overrideEffect : null) + ","
                    + db.escape((paidAddonNextMonth >= 0) ? paidAddonNextMonth : null) + ")";
                }

                db.query_err(q, function (err, result) {
                    if (err) {
                        callback(err);
                        return;
                    }
                    callback(undefined, {
                        id: !update ? result.insertId : id,
                        code: code,
                        added: !update ? true : undefined,
                        updated: update ? true : undefined
                    });
                });
            } else {
                callback(new BaseError("Category is not found", "CATEGORY_NOT_FOUND"));
            }
        });
    },

    /**
     *
     *
     */
    loadPromoCampaignOffer: function (tag, callback) {
        if (!tag) {
            return callback(new BaseError("Category tag is empty", "ERROR_CATEGORY_NOT_FOUND"));
        }

        module.exports.loadCategories(tag, (err, category) => {
            if (err) {
                return callback(err);
            }
            if (!category) {
                return callback(new BaseError("Category with tag " + tag + " is not found",
                    "ERROR_CATEGORY_NOT_FOUND"));
            }
            if (!category.codePatternL1) {
                return callback(new BaseError("Category with tag " + tag + " has no pattern code L1",
                    "ERROR_CATEGORY_PATTERN_L1_NOT_FOUND"));
            }

            var code = category.codePatternL1;
            module.exports.loadPromoCode(code, (err, result) => {
                callback(err, result);
            }, undefined, undefined, undefined, undefined, true);
        });
    },

    /**
     *
     *
     */
    loadPromoCode: function (code, callback, eCommOnly, level, level1Max, prevLevelErrorStatus, ignoreCodeValidity) {
        var maxLevel = (!level || level < 1) ? 1 : level;

        var cMaxField;
        if (maxLevel == 1) {
            cMaxField = "max";
        } else if (maxLevel == 2) {
            cMaxField = "max_level_2";
        } else if (maxLevel == 3) {
            cMaxField = "max_level_3";
        } else {
            if (LOG) common.log("PromotionsManager", "invalid level, level=" + level);
            return callback(new BaseError("Invalid code level", "ERROR_INVALID_PROMO_LEVEL"),
                {status: "ERROR_INVALID_PROMO_LEVEL"});
        }

        var searchCode = code + (maxLevel > 1 ? "#" + maxLevel : "");

        var query = "SELECT pc.max as max_code, pcc." + cMaxField + " as max_category, pcc.type, pcc.partner, pcc.title, pcc.message, " +
            "pc.sku_name_1 as skuName1, pc.sku_key_1 as skuKey1, pc.sku_quantity_1 as skuQuantity1, pc.free_addon_1 as freeAddon1, " +
            "pc.free_addon_months_1 as freeAddonMonths1, " +
            "pc.second_verification_type as secondVerificationType, pc.device_discount_cents as deviceDiscountCents, " +
            "pc.waiver_cents as waiverCents, pc.waiver_month as waiverMonth, pc.reg_discount_cents as regDiscountCents, pcc.image1, " +
            "pcc.multiple_codes_allowed as multipleCodesAllowed, pcc.public_name as publicName, pcc.multiple_codes_allowed, " +
            "pcc.tag, pc.reg_discount_cents, pc.device_discount_cents, pc.extraBillDay as extraBillDay, " +
            "pcc.start as categoryValidFrom, pcc.end as categoryValidTill, pc.type as promoCodeType, pc.start as codeValidFrom, pc.end as codeValidTill, " +
            "pc.product_id, pc.waiver_cents, pc.waiver_month, p.sub_effect, pc.code, pc.category, pc.months_count as months_count," +
            "pc.bonus_data, pc.bonus_data_type, pcc.customMNumber, pcc.bankName, pc.overrideEffect, pc.paidAddonNextMonth, " +
            "(SELECT count(*) FROM promoCodesLogs npcl WHERE npcl.code=" + db.escape(code) + ") count_code, " +
            "(SELECT count(*) FROM promoCodesLogs npcl WHERE npcl.code=" + db.escape(code + '#2') + ") count_code_level_2, " +
            "(SELECT count(*) FROM promoCodesLogs npcl WHERE npcl.code=" + db.escape(code + '#3') + ") count_code_level_3, " +
            "(SELECT count(*) FROM promoCodesLogs npcc WHERE npcc.category=pc.category AND npcc.level="
            + db.escape(maxLevel) + ") count_category " +
            "FROM promoCodes pc LEFT JOIN promoCodesCategories pcc ON pcc.id=pc.category " +
            "LEFT JOIN promoCodesLogs pcl ON pc.code=pcl.code " +
            "LEFT JOIN packages p ON p.product_id=pc.product_id " +
            "WHERE 1=1 " + (ignoreCodeValidity ? "" : (" AND pc.enabled=1 ")) +
            "AND pcc.enabled=1 AND pc.code=" + db.escape(searchCode) + " GROUP BY pc.code";

        // in order to provide correct error status remove
        // date validation check from query and add it to code

        if(LOG) common.log("PromotionsManager", query);
        db.query_err(query, function (err, result) {
            if (err) {
                if (LOG) common.log("PromotionsManager", "query error, error=" + err.message);
                return callback(new BaseError("Internal error", "INTERNAL_ERROR"),
                    {status: "INTERNAL_ERROR"});
            }

            if (!result || !result[0]) {
                if (prevLevelErrorStatus === "CATEGORY_LIMIT_EXCEEDED") {
                    return callback(new BaseError("Category limit was exceeded", "CATEGORY_LIMIT_EXCEEDED"),
                        {status: "CATEGORY_LIMIT_EXCEEDED"});
                } else if (prevLevelErrorStatus === "CODE_LIMIT_EXCEEDED") {
                    return callback(new BaseError("Code limit was exceeded", "CODE_LIMIT_EXCEEDED"),
                        {status: "CODE_LIMIT_EXCEEDED"});
                } else {
                    common.log("PromotionsManager", "code is not found, code=" + code + ", searchCode=" + searchCode + ", Time=" + new Date().getTime());
                    return callback(new BaseError("Code is not found", "CODE_NOT_FOUND"),
                        {status: "CODE_NOT_FOUND"});
                }
            }

            if (LOG) common.log("PromotionsManager", "code is found, searchCode=" + searchCode + ", code=" + JSON.stringify(result[0]));
            var item = result[0];
            var now = new Date();

            // L2 and L3 code are limited by max from L1

            var codeMaxLimit = maxLevel == 1 ? item.max_code : level1Max;
            var codeUsedCount = item.count_code + item.count_code_level_2 + item.count_code_level_3;

            if (eCommOnly && item.type === "internal") {

                common.error("PromotionsManager", "internal code can is not allowed, categoryValidFrom=" + item.type);
                return callback(new BaseError("Internal code can is not allowed", "APP_CODE_NOT_ALLOWED"),
                    {status: "APP_CODE_NOT_ALLOWED"});

            } else if (item.categoryValidFrom !== "0000-00-00 00:00:00" && new Date(item.categoryValidFrom).getTime() > now.getTime()) {

                common.error("PromotionsManager", "category is no valid yet, categoryValidFrom=" + item.categoryValidFrom);
                return callback(new BaseError("Category is not valid yet", "CODE_NOT_FOUND"),
                    {status: "CODE_NOT_FOUND"});

            } else if (item.categoryValidTill !== "0000-00-00 00:00:00" && new Date(item.categoryValidTill).getTime() < now.getTime()) {

                common.error("PromotionsManager", "category inactive, expired, categoryValidTill=" + item.categoryValidTill);
                return callback(new BaseError("Category inactive, expired", "CATEGORY_INACTIVE_EXPIRED"),
                    {status: "CATEGORY_INACTIVE_EXPIRED", expiration: item.categoryValidTill});

            } else if (item.codeValidFrom !== "0000-00-00 00:00:00" && new Date(item.codeValidFrom).getTime() > now.getTime()) {

                common.error("PromotionsManager", "code is no valid yet, codeValidFrom=" + item.codeValidFrom);
                return callback(new BaseError("Code is not valid yet", "CODE_NOT_FOUND"),
                    {status: "CODE_NOT_FOUND"});

            } else if (item.codeValidTill !== "0000-00-00 00:00:00" && new Date(item.codeValidTill).getTime() < now.getTime()) {

                common.error("PromotionsManager", "code inactive, expired, count codeValidTill=" + item.codeValidTill);
                return callback(new BaseError("Code inactive, expired", "CODE_INACTIVE_EXPIRED"),
                    {status: "CODE_INACTIVE_EXPIRED", expiration: item.codeValidTill});

            } else if (!ignoreCodeValidity && codeUsedCount >= codeMaxLimit && codeMaxLimit >= 0) {
                // if max is -1, it means no limit is defined

                common.error("PromotionsManager", "code limit was exceeded, count added=" +
                codeUsedCount + ", max allowed=" + codeMaxLimit);

                if (maxLevel < 3) {
                    return module.exports.loadPromoCode(code, callback, eCommOnly, maxLevel + 1,
                        codeMaxLimit, "CODE_LIMIT_EXCEEDED");
                } else {
                    return callback(new BaseError("Code limit was exceeded", "CODE_LIMIT_EXCEEDED"),
                        {status: "CODE_LIMIT_EXCEEDED"});
                }

            } else if (item.count_category >= item.max_category && item.max_category >= 0) {

                common.error("PromotionsManager", "category limit was exceeded, count added=" +
                item.count_category + ", max allowed=" + item.max_category);

                if (maxLevel < 3) {
                    return module.exports.loadPromoCode(code, callback, eCommOnly, maxLevel + 1,
                        codeMaxLimit, "CATEGORY_LIMIT_EXCEEDED");
                } else {
                    return callback(new BaseError("Category limit was exceeded", "CATEGORY_LIMIT_EXCEEDED"),
                        {status: "CATEGORY_LIMIT_EXCEEDED"});
                }
            }

            item.level = maxLevel;
            return callback(undefined, item);
        });
    },

    /**
     *
     *
     */
    useCode: function (platform, prefix, number, code, orderReferenceNumber, executionKey, callback) {
        module.exports.loadPromoCode(code, function (err, item) {
            if (err) {
                return callback(err, item);
            }

            if (item.type == "internal_ecomm") {

                // some codes are allowed to use on both platforms eComm and App
                // if orderReferenceNumber is presented the code is been used from eComm
                // if number is presented the code is been used from App

                if (platform == 'ECOMM' || platform == 'CMS') {
                    item.type = "partner";
                } else if (platform == 'APP') {
                    item.type = "internal";
                    item.validIFHasCirclesBonusesOnly = true;
                } else {
                    return callback(new BaseError("Incorrect platform for internal_ecomm code type", "ERROR_PLATFORM_TYPE"));
                }
            }

            // internal - App codes
            // partner - eComm codes

            if (item.type === 'internal' && (!number || !prefix)) {
                if (LOG) common.error("PromotionsManager", "number or prefix are missing");
                return callback(new BaseError("Incorrect params, number or prefix are missing", "ERROR_INCORRECT_PARAMS"));
            } else if (item.type === 'partner' && !orderReferenceNumber) {
                if (LOG) common.error("PromotionsManager", "order reference number is missing");
                return callback(new BaseError("Incorrect params, order reference number is missing", "ERROR_INCORRECT_PARAMS"));
            }

            if ((platform == 'APP' && item.type === 'partner') || (platform == 'ECOMM' && item.type === 'internal')) {
                common.error("PromotionsManager", "used from invalid platform");
                return callback(new BaseError("Code can not be used from " + platform + " platform", "ERROR_PLATFORM_USED"));
            }

            var product = item.product_id ? ec.findPackage(ec.packages(), item.product_id) : undefined;
            var bonusData = item.bonus_data;
            var bonusDataType = item.bonus_data_type;
            var validIFHasCirclesBonusesOnly = item.validIFHasCirclesBonusesOnly;
            var waiverCents = item.waiver_cents;
            var waiverMonth = item.waiver_month;
            var monthsCount = item.months_count;
            var level = !item.level ? 1 : item.level;
            var searchCode = level > 1 ? code + "#" + level : code;
            var multipleCodesAllowed = item.multiple_codes_allowed;
            var tag = item.tag;

            if ((product || bonusData > 0) && item.months_count < -1 && item.months_count > 12) {
                if (LOG) common.log("PromotionsManager", "code has invalid config, months_count=" + item.months_count);
                return callback(new BaseError("Invalid config", "ERROR_INVALID_CONFIG"), {status: "ERROR_INVALID_CONFIG"});
            }

            var freeAddon1 = item.freeAddon1;
            var freeAddonMonths1 = item.freeAddonMonths1;
            if (freeAddon1 && freeAddonMonths1 < -1 && freeAddonMonths1 > 12) {
                if (LOG) common.log("PromotionsManager", "code has invalid config, freeAddonMonths1=" + freeAddonMonths1);
                return callback(new BaseError("Invalid config", "ERROR_INVALID_CONFIG"), {status: "ERROR_INVALID_CONFIG"});
            }

            if (freeAddonMonths1 > 0 && shouldGiveExtraBillCycleForPromo(item)) {
                freeAddonMonths1 = freeAddonMonths1 + 1;
            }

            if (monthsCount > 0 && shouldGiveExtraBillCycleForPromo(item)) {
                monthsCount = monthsCount + 1;
            }

            var checkIfOriginalUsed = (internal, sin, orn, done) => {
                var searchCodeL1 = code;
                var searchCodeL2 = code + "#2";
                var searchCodeL3 = code + "#3";

                var whereIds = internal ? "(service_instance_number=" + db.escape(sin) +
                (orn ? " OR order_reference_number=" + db.escape(orn) : "") + ")" : "(order_reference_number=" + db.escape(orn) + ")";

                var query = "SELECT count(*) as count FROM promoCodesLogs WHERE code IN (" + db.escape(searchCodeL1) +
                    "," + db.escape(searchCodeL2) + "," + db.escape(searchCodeL3) + ") AND " + whereIds;

                common.log("PromotionsManager", "query=" + query);
                db.query_err(query, function (err, result) {
                    if (err) {
                        common.error("PromotionsManager", "can not load promo code information from db, " +
                        "code=" + err.code + ", message=" + err.message);
                        var status = 'ERROR_DB_INSERTION';
                        var message = 'Failed to save code, ' + err.message;
                        return callback(new BaseError(message, status), {status: status});
                    }

                    if (result && result[0] && result[0].count > 0) {
                        var status = 'ERROR_CODE_USED';
                        var message = 'Code has already been used';
                        return callback(new BaseError(message, status), {status: status});
                    }

                    if (done) done();
                });
            }

            var saveCodeLog = (internal, sin, orn, done)=> {

                checkIfOriginalUsed(internal, sin, orn, () => {

                    var idFieldName = internal ? "service_instance_number, order_reference_number" : "order_reference_number";
                    var idFieldValues = internal ? (db.escape(sin) + ", " + (orn ? db.escape(orn) : "NULL")) : db.escape(orn);
                    var status = internal ? 0 : 2;
                    var bonusData = item.bonus_data;
                    var bonusDataType = item.bonus_data_type;

                    var insertQuery = "INSERT INTO promoCodesLogs (" + idFieldName + ", code, product_id, "
                        + "bonus_data, bonus_data_type, waiver_cents, waiver_month, months_count, "
                        + "free_addon_1, free_addon_months_1, category, unique_in_category, status, used_ts, level) VALUES ("
                        + idFieldValues + ", "
                        + db.escape(searchCode) + ", "
                        + (product ? db.escape(product.id) : "NULL") + ", "
                        + (bonusData ? db.escape(bonusData) : "0") + ", "
                        + (bonusDataType ? db.escape(bonusDataType) : "NULL") + ", "
                        + (waiverCents ? db.escape(waiverCents) : "0") + ", "
                        + (waiverMonth ? db.escape(waiverMonth) : "0") + ", "
                        + (monthsCount ? db.escape(monthsCount) : "0") + ", "
                        + (freeAddon1 ? db.escape(freeAddon1) : "NULL") + ", "
                        + (freeAddonMonths1 ? db.escape(freeAddonMonths1) : "0") + ", "
                        + db.escape(item.category) + ", "
                        + (multipleCodesAllowed ? "NULL" : db.escape(1)) + ","
                        + db.escape(status) + ","
                        + db.escape(new Date()) + ","
                        + db.escape(level) + ")";

                    common.log("PromotionsManager", "insertQuery=" + insertQuery);
                    db.query_err(insertQuery, function (err, result) {
                        if (err) {
                            common.error("PromotionsManager", "can not save promo code information into db, " +
                            "code=" + err.code + ", message=" + err.message);
                            var status = 'ERROR_DB_INSERTION';
                            var message = "Database error, code=" + err.code + ", " + err.message;

                            // same code usage - Error: ER_DUP_ENTRY: Duplicate entry 'XXXXXXXX' for key 'service_instance_number'
                            // same code usage - Error: ER_DUP_ENTRY: Duplicate entry 'XXXXXXXX' for key 'order_reference_number'
                            // same category usage - Error: ER_DUP_ENTRY: Duplicate entry 'XXXXXXXX' for key 'code'

                            if (err.code === 'ER_DUP_ENTRY') {
                                var singlePerCategory = err.message.indexOf("for key 'service_instance_number'") >= 0
                                    || err.message.indexOf("for key 'order_reference_number'") >= 0;
                                if (singlePerCategory) {
                                    status = 'ERROR_CATEGORY_USED';
                                    message = 'One code per category only';
                                } else {
                                    status = 'ERROR_CODE_USED';
                                    message = 'Code has already been used';
                                }
                            }

                            return callback(new BaseError(message, status), {status: status});
                        }
                        if (done) done(result);
                    });
                });
            }

            var addInternalBonus = function () {

                ec.getCustomerDetailsNumber(prefix, number, false, function (err, cache) {
                    if (err) {
                        return callback(err);
                    }
                    if (!cache) {
                        common.error("PromotionsManager", "customer not found is not supported");
                        return callback(new BaseError("Customer not found, number=" + number, "INCORRECT_TYPE"));
                    }

                    bonusManager.loadHistoryBonusListBySI(cache.serviceInstanceNumber, true, (err, result) => {
                        if (err) {
                            return callback(err);
                        }

                        // in case if code is can be used on eComm and app we have to add
                        // an extra protection so customers can not accumulate multiple 'forever' third party bonuses

                        if (validIFHasCirclesBonusesOnly && result &&
                            item.tag !== constants.TERMINATION_WINBACK_PROMO_TAG) {
                            var circlesInternalBonuses = bonusTypesService.getCirclesInternalTypes();

                            var nonCirclesBonus;
                            result.some((item) => {
                                if (item.product_type != "unknown" && item.leaderboard_included
                                    && circlesInternalBonuses.indexOf(item.product_type) == -1) {
                                    nonCirclesBonus = item.product_type;
                                    return true;
                                }
                            });

                            if (nonCirclesBonus) {
                                common.error("PromotionsManager", "Customer already has one third party bonus " + nonCirclesBonus);
                                return callback(new BaseError("Customer already has one third party bonus, " +
                                "nonCirclesBonuses=" + nonCirclesBonus, "ERROR_ALREADY_HAS_THIRD_PARTY_BONUS"));
                            }
                        }

                        saveCodeLog(true, cache.serviceInstanceNumber, cache.orderReferenceNumber, function (result) {

                            var insertId = result.insertId;
                            if (LOG) common.log("PromotionsManager", "subscribe for a package, promo log insertId=" + insertId +
                            ", serviceInstanceNumber=" + cache.serviceInstanceNumber);

                            var productError = false;
                            var freeAddonError = false;
                            var waiverError = false;

                            var operationSucceed = function () {
                                if (LOG) common.log("PromotionsManager", "succeed to subscribe for an addon" +
                                ", product_id=" + item.product_id +
                                ", productError=" + productError +
                                ", waiverError=" + waiverError +
                                ", freeAddonError=" + freeAddonError +
                                ", bonus_data=" + item.bonus_data +
                                ", bonus_data_type=" + item.bonus_data_type);

                                var status = freeAddonError || productError || waiverError ? 0 : 1;
                                var updateQuery = "UPDATE promoCodesLogs SET status=" + status + " WHERE id=" + insertId;
                                if (LOG) common.log("PromotionsManager", "updateQuery=" + updateQuery);

                                db.query_err(updateQuery, function () {
                                    return callback(undefined, {
                                        codeValid: true,
                                        title: item.title,
                                        message: item.message,
                                        images: [{url: item.image1}],
                                        promoLogId: insertId
                                    });
                                });
                            }

                            var addWaiver = function () {
                                if (waiverCents <= 0) {
                                    return operationSucceed();
                                }

                                waiverManager.addCreditNotes(prefix, number, "Promo Code " + code,
                                    waiverCents, function (err, result) {
                                        if (err) {
                                            waiverError = true;
                                            common.error("PromotionsManager", "failed add waiver, " +
                                            "waiverCents=" + waiverCents + ", error message=" + err.message );
                                        } else {
                                            sendNotification(waiverMonth > 1 ? "bonus_voucher_waived_multiple" : "bonus_voucher_waived",
                                                prefix, number, code, waiverCents, waiverMonth, function () {
                                                    // do nothing
                                                });
                                        }

                                        operationSucceed();
                                    });
                            }

                            var addFreeAddon = function () {
                                if (!freeAddon1 || freeAddonMonths1 == 0) {
                                    return addWaiver();
                                }

                                var freeAddonProduct1 = ec.findPackage(ec.packages(), freeAddon1);
                                if (!freeAddonProduct1) {
                                    freeAddonError = true;
                                    common.error("PromotionsManager", "failed to find free addon, productId=" + freeAddon1);
                                    return addWaiver();
                                }

                                var paidAddonProduct1 = ec.findPackageWithFreeId(ec.packages(), freeAddon1);
                                if (!paidAddonProduct1) {
                                    freeAddonError = true;
                                    common.error("PromotionsManager", "failed to find paid version of " +
                                    "free addon, productId=" + freeAddon1);
                                    return addWaiver();
                                }

                                var prefix = cache.prefix;
                                var number = cache.number;
                                var freeAddonId = freeAddonProduct1.id;
                                var paidAddonId = paidAddonProduct1.id;
                                var historyIds = packageManager.getCustomerGeneralHistoryIds(paidAddonProduct1.id, cache);

                                var subscribeFreeAddon = (overrideEffect) => {
                                    if (LOG) common.log("PromotionsManager", "add free version of addon, freeAddonId=" + freeAddonId);

                                    var duration = freeAddonMonths1 > 0 ? freeAddonMonths1 + " mo" : "forever";
                                    var notification = packageManager.getNotificationActivities("FREE", {
                                        params: {}, paramsReminder: {code: code}
                                    });

                                    // By default we subscribe to the paidAddon from the next month,
                                    // if paidAddon is false then then we dont subscribe to the paid addon from the next month
                                    var paidAddonNextMonth = true;
                                    if (item.paidAddonNextMonth == "0") {
                                        paidAddonNextMonth = false;
                                    }
                                    packageManager.addonUpdate(prefix, number, "PUT", freeAddonId, historyIds, true, (err, sub) => {
                                        if (err) {
                                            freeAddonError = true;
                                            common.error("PromotionsManager", "failed to add free " +
                                            "addon, productId=" + freeAddonProduct1.id + ", error message=" + err.message);
                                            return addWaiver();
                                        }

                                        return addWaiver();
                                    }, overrideEffect, false, {
                                        activeMonths: freeAddonMonths1,
                                        notification: notification,
                                        paidAddonNextMonth: paidAddonNextMonth
                                    });
                                }

                                if (!historyIds) {
                                    if (item.overrideEffect >= 0) {
                                        return subscribeFreeAddon(item.overrideEffect);
                                    } else {
                                        return subscribeFreeAddon();
                                    }
                                }

                                if (LOG) common.log("PromotionsManager", "remove paid version of addon, " +
                                "historyIds=" + historyIds + ", paidAddonId=" + paidAddonId);
                                packageManager.addonUpdate(prefix, number, "DELETE", paidAddonId, historyIds, true, (err, sub) => {
                                    if (err) {
                                        freeAddonError = true;
                                        common.error("PromotionsManager", "failed to remove subscribed " +
                                        "addon, productId=" + paidAddonProduct1.idx + ", error message=" + err.message);
                                        return addWaiver();
                                    }

                                    // subscription supposed to be overridden because account may not be re-cached
                                    // after paid version is unsubscribed, so we have to make sure that if there is a current
                                    // month paid addon we will be subscribing free starting next month only

                                    var overrideEffect = (item.overrideEffect >= 0) ? item.overrideEffect : "2";
                                    return subscribeFreeAddon(overrideEffect);
                                }, (item.overrideEffect >= 0 ) ? item.overrideEffect : undefined);
                            }

                            var addAddonsDone = function () {
                                if (!product && !bonusData) {
                                    return addFreeAddon();
                                }

                                if (LOG) common.log("PromotionsManager", "add product, account=" + cache.account
                                + ", monthsCount=" + item.months_count);

                                var continueMonth = item.months_count == -1 ? -1 : item.months_count;
                                bonusManager.addBonus({
                                    noneRecurrentBonusId: product ? product.id : undefined,
                                    bonusData: bonusData,
                                    bonusDataType: bonusDataType,
                                    continueRecurrent: item.months_count == -1,
                                    continueMonth: continueMonth,
                                    prefix: cache.prefix,
                                    number: cache.number,
                                    executionKey: config.MOBILE_APP_KEY,
                                    metadata: {data1: code},
                                    waitForResponse: false
                                }, function (err, result) {
                                    if (err) {
                                        productError = true;
                                        common.error("PromotionsManager", "failed to subscribe a bonus, error=" + err.message);
                                    }
                                    addFreeAddon();
                                });
                            }

                            addAddonsDone();
                        });
                    });
                });
            }

            var addPartnerBonus = function () {

                saveCodeLog(false, undefined, orderReferenceNumber, function (result) {
                    return callback(undefined, {
                        codeValid: true,
                        promoLogId: result.insertId ? result.insertId : null
                    });
                });
            }

            if (item.type === 'internal') {
                addInternalBonus();
            } else if (item.type === 'partner') {
                addPartnerBonus();
            } else {
                common.error("PromotionsManager", "type is not supported");
                return callback(new BaseError("Not supported item type, type=" + item.type, "INCORRECT_TYPE"));
            }
        });
    },


    /**
     *
     *
     */
    loadPendingProducts: function (addonsIds, orderReferenceNumber, type, callback) {
        if (!callback) callback = () => {
        }

        if (type !== 'DEFAULT' && type !== 'PORT_IN') {
            common.error("PromotionsManager loadPendingProducts", "Promo code type is wrong or missing in the param");
            return callback(new BaseError("Incorrect params, promo code type is wrong or missing", "ERROR_INCORRECT_PARAMS"));
        }

        var selectPending = "SELECT promoCodes.code, promoCodesLogs.free_addon_1 as freeAddon1, " +
            "promoCodesLogs.free_addon_months_1 as freeAddonMonths1 " +
            "FROM promoCodesLogs LEFT JOIN promoCodes ON promoCodesLogs.code=promoCodes.code " +
            "WHERE status=2 AND order_reference_number=" + db.escape(orderReferenceNumber) + " AND type=" + db.escape(type);

        if (LOG) common.log("PromotionsManager", "loadPendingProducts: orderReferenceNumber=" + orderReferenceNumber);
        db.query_err(selectPending, function (err, result) {
            if (err) {
                common.error("PromotionsManager", "can not load pending promo from db, err.code=" +
                err.code + ", message=" + err.message);
                return callback(err);
            }

            var freeAddons = {};
            var paidAddons = {};
            var updatedAddons = [];
            var updatedAddonsMetadata = {};

            if (result) result.forEach((item) => {
                var code = item.code;
                var freeAddonId = item.freeAddon1;
                var freeAddonMonths = item.freeAddonMonths1;
                var paidAddonProduct = freeAddonId ? ec.findPackageWithFreeId(ec.packages(), freeAddonId) : undefined;
                var paidAddonId = paidAddonProduct ? paidAddonProduct.id : undefined;

                if (freeAddonId) {
                    freeAddons[freeAddonId] = {
                        code: code,
                        paidAddonId: paidAddonId,
                        freeAddonId: freeAddonId,
                        freeAddonMonths: freeAddonMonths
                    }
                }

                if (paidAddonId) {
                    paidAddons[paidAddonId] = {
                        code: code,
                        paidAddonId: paidAddonId,
                        freeAddonId: freeAddonId,
                        freeAddonMonths: freeAddonMonths
                    }
                }
            });

            if (addonsIds) addonsIds.forEach((addonId) => {
                var freeAddonInfo;
                if (freeAddons[addonId]) {
                    freeAddonInfo = freeAddons[addonId];
                }
                if (paidAddons[addonId]) {
                    freeAddonInfo = paidAddons[addonId];
                }

                if (freeAddonInfo) {
                    updatedAddons.push(freeAddonInfo.freeAddonId);
                    updatedAddonsMetadata[freeAddonInfo.freeAddonId] = freeAddonInfo;
                } else {
                    updatedAddons.push(addonId);
                }
            });

            Object.keys(freeAddons).forEach((freeAddonId) => {
                if (updatedAddons.indexOf(freeAddonId) == -1) {
                    updatedAddons.push(freeAddonId);
                    updatedAddonsMetadata[freeAddonId] = freeAddons[freeAddonId];
                }
            });

            return callback(undefined, {
                addons: updatedAddons,
                metadata: updatedAddonsMetadata
            })
        });
    },

    /**
     *
     *
     */
    applyPendingPromo: function (prefix, number, orderReferenceNumber, type, executionKey, callback) {

        if (type !== 'DEFAULT' && type !== 'PORT_IN') {
            common.error("PromotionsManager", "Promo code type is wrong or missing in the param");
            return callback(new BaseError("Incorrect params, promo code type is wrong or missing", "ERROR_INCORRECT_PARAMS"));
        }

        var selectPending = "SELECT promoCodes.code, promoCodesLogs.product_id, promoCodesLogs.bonus_data," +
            " promoCodesLogs.bonus_data_type, promoCodesLogs.waiver_cents, promoCodes.months_count, " +
            " promoCodesLogs.free_addon_1, promoCodesLogs.free_addon_months_1, promoCodes.extraBillDay," +
            " promoCodes.waiver_month, promoCodes.overrideEffect, promoCodes.paidAddonNextMonth, promoCodesLogs.id as log_id" +
            " FROM promoCodesLogs LEFT JOIN promoCodes ON promoCodesLogs.code=promoCodes.code " +
            " WHERE status=2 AND order_reference_number=" + db.escape(orderReferenceNumber) + " AND type=" + db.escape(type);

        if (LOG) common.log("PromotionsManager", "applyPendingPromo: number=" + number +
        ", orderReferenceNumber=" + orderReferenceNumber);

        db.query_err(selectPending, function (err, result) {
            if (err) {
                common.error("PromotionsManager", "can not load pending promo from db, err.code=" +
                err.code + ", error message=" + err.message);
                return callback(err);
            }

            if (!result || result.length == 0) {
                return callback(undefined, {
                    added: false,
                    countAdded: 0
                });
            }

            ec.getCustomerDetailsNumber(prefix, number, false, function (err, cache) {
                if (!cache) {
                    common.error("PromotionsManager", "user details has not been found");
                    return callback(new BaseError("Can not load user details", "EC_ERROR"));
                }

                var updateStatus = function (promoInfo, productError, waiverError, freeAddonError, statusCallback) {
                    // mark as used if successfully subscribed (status==1)
                    // or return back to pending status if failed (status==0)
                    var logId = promoInfo.logId;
                    var newStatus = (productError || waiverError || freeAddonError ? 0 : 1);
                    var updateQuery = "UPDATE promoCodesLogs SET status=" + db.escape(newStatus)
                        + ", service_instance_number=" + db.escape(cache.serviceInstanceNumber)
                        + " WHERE id=" + db.escape(logId);

                    if (LOG) common.log("PromotionsManager", "updateQuery=" + updateQuery);
                    db.query_err(updateQuery, function (err) {
                        if (err) {
                            common.error("PromotionsManager", "failed to mark promo as added, code=" +
                            err.code + ", error message=" + err.message);
                            promoInfo.statusUpdateError = err;
                            return statusCallback(err, promoInfo);
                        }

                        if (productError || waiverError) {
                            common.error("PromotionsManager", "log is not marked as used, some error is encountered");
                        }

                        statusCallback(undefined, promoInfo);
                    });
                }

                var addWaiver = function (promoInfo, productError, freeAddonError, waiverCallback) {
                    var code = promoInfo.code;
                    var logId = promoInfo.logId;
                    var waiverCents = promoInfo.waiverCents;
                    var waiverMonth = promoInfo.waiverMonth;
                    var waiverError = false;
                    if (waiverCents > 0) {
                        waiverManager.addCreditNotes(prefix, number, "Promo Code " + code,
                            waiverCents, function (err, result) {
                                if (err) {
                                    waiverError = true;
                                    common.error("PromotionsManager", "failed to add waiver, " +
                                    "waiverCents=" + waiverCents + ", err=" + err.message);
                                    promoInfo.waiverError = err;
                                } else {
                                    if (LOG) common.log("PromotionsManager", "succeed to add waiver, " +
                                    "waiverCents=" + waiverCents);

                                    sendNotification(waiverMonth > 1 ? "bonus_voucher_waived_multiple" : "bonus_voucher_waived",
                                        prefix, number, code, waiverCents, waiverMonth, function () {
                                            // do nothing
                                        });
                                }

                                promoInfo["waiver"] = {
                                    err: err ? err.message : undefined,
                                    result: result
                                }

                                updateStatus(promoInfo, productError, waiverError, freeAddonError, function (err, result) {
                                    waiverCallback(err, result);
                                });
                            });
                    } else {
                        promoInfo["waiver"] = {
                            result: {
                                waived: false,
                                reason: "NO_WAIVER"
                            }
                        };
                        updateStatus(promoInfo, productError, waiverError, freeAddonError, function (err, result) {
                            waiverCallback(err, result);
                        });
                    }
                }

                var addFreeAddon = function (promoInfo) {
                    var code = promoInfo.code;
                    var logId = promoInfo.logId;
                    var productId = promoInfo.productId;
                    var bonusData = promoInfo.bonusData;
                    var bonusDataType = promoInfo.bonusDataType;
                    var waiverCents = promoInfo.waiverCents;
                    var waiverMonth = promoInfo.waiverMonth;
                    var monthCount = promoInfo.monthCount;
                    var overrideEffect = promoInfo.overrideEffect;
                    var paidAddonNextMonth = promoInfo.paidAddonNextMonth;
                    var freeAddon1 = promoInfo.freeAddon1;
                    var freeAddonMonths1 = promoInfo.freeAddonMonths1;
                    var freeAddonError = false;
                    tasks.push(function (cb) {
                        if (!freeAddon1 || freeAddonMonths1 == 0) {
                            return addWaiver(promoInfo, null, false, function (err, result) {
                                cb(err, result);
                            });
                        }

                        var freeAddonProduct1 = ec.findPackage(ec.packages(), freeAddon1);
                        if (!freeAddonProduct1) {
                            freeAddonError = true;
                            common.error("PromotionsManager", "failed to find free addon, productId=" + freeAddon1);
                            return addWaiver(promoInfo, null, freeAddonError, function (err, result) {
                                cb(err, result);
                            });
                        }

                        var paidAddonProduct1 = ec.findPackageWithFreeId(ec.packages(), freeAddon1);
                        if (!paidAddonProduct1) {
                            freeAddonError = true;
                            common.error("PromotionsManager", "failed to find paid version of " +
                            "free addon, productId=" + freeAddon1);
                            return addWaiver(promoInfo, null, freeAddonError, function (err, result) {
                                cb(err, result)
                            });
                        }

                        var prefix = cache.prefix;
                        var number = cache.number;
                        var freeAddonId = freeAddonProduct1.id;
                        var paidAddonId = paidAddonProduct1.id;
                        var historyIds = packageManager.getCustomerGeneralHistoryIds(paidAddonProduct1.id, cache);
                        var subscribeFreeAddon = (overrideEffect) => {
                            if (LOG) common.log("PromotionsManager", "add free version of addon, freeAddonId=" + freeAddonId);
                            var duration = freeAddonMonths1 > 0 ? freeAddonMonths1 + " mo" : "forever";
                            var notification = packageManager.getNotificationActivities("FREE", {
                                params: {}, paramsReminder: {code: code}
                            });
                            // By default we subscribe to the paidAddon from the next month,
                            // if paidAddon is false then then we dont subscribe to the paid addon from the next month
                            if (paidAddonNextMonth == "0") {
                                paidAddonNextMonth = false;
                            } else {
                                paidAddonNextMonth = true;
                            }
                            var allowMultiple = false;
                            if (freeAddonId === constants.FREE_2020_ADDON_ID)
                                allowMultiple = true;

                            packageManager.addonUpdate(prefix, number, "PUT", freeAddonId, historyIds, true, (err, sub) => {
                                if (err) {
                                    freeAddonError = true;
                                    promoInfo.addonSubscriptionError = err;
                                    common.error("PromotionsManager", "failed to add free " +
                                    "addon, productId=" + freeAddonProduct1.id + ", error message=" + err.message);
                                    return addWaiver(promoInfo, null, freeAddonError,  function (err, result) {
                                        cb(err, result)
                                    });
                                }
                                return addWaiver(promoInfo, null, freeAddonError,  function (err, result) {
                                    cb(err, result)
                                });
                            }, overrideEffect, false, {
                                activeMonths: freeAddonMonths1,
                                notification: notification,
                                paidAddonNextMonth: paidAddonNextMonth,
                                allowMultiple: allowMultiple
                            });
                        }

                        if (!historyIds) {
                            if (overrideEffect >= 0) {
                                return subscribeFreeAddon(overrideEffect);
                            } else {
                                return subscribeFreeAddon();
                            }
                        }

                        if (LOG) common.log("PromotionsManager", "remove paid version of addon, " +
                        "historyIds=" + historyIds + ", paidAddonId=" + paidAddonId);
                        packageManager.addonUpdate(prefix, number, "DELETE", paidAddonId, historyIds, true, (err, sub) => {
                            if (err) {
                                freeAddonError = true;
                                promoInfo.addonUnsubscriptionError = err;
                                common.error("PromotionsManager", "failed to remove subscribed " +
                                "addon, productId=" + paidAddonProduct1.id + ", error message=" + err.message);
                                return addWaiver(promoInfo, null, null,  function (err, result) {
                                    cb(err, result)
                                });
                            }

                            // Sometimes after the unsubscription, the cache is not updated, so 
                            // we put a timeout of 1 second and then we force reload the cache
                            setTimeout(function () {
                                ec.getCustomerDetailsNumber(prefix, number, true, function () {
                                    // subscription supposed to be overridden because account may not be re-cached
                                    // after paid version is unsubscribed, so we have to make sure that if there is a current
                                    // month paid addon we will be subscribing free starting next month only
                                    var overrideEffect = (overrideEffect >= 0) ? overrideEffect : "2";
                                    return subscribeFreeAddon(overrideEffect);
                                });
                            }, 1000);
                        }, (overrideEffect >= 0 ) ? overrideEffect : undefined);
                    });
                }

                var applyBonus = function (promoInfo) {
                    var code = promoInfo.code;
                    var logId = promoInfo.logId;
                    var productId = promoInfo.productId;
                    var bonusData = promoInfo.bonusData;
                    var bonusDataType = promoInfo.bonusDataType;
                    var waiverCents = promoInfo.waiverCents;
                    var waiverMonth = promoInfo.waiverMonth;
                    var monthCount = promoInfo.monthCount;
                    var overrideEffect = promoInfo.overrideEffect;
                    var paidAddonNextMonth = promoInfo.paidAddonNextMonth;
                    var freeAddon1 = promoInfo.freeAddon1;
                    var freeAddonMonths1 = promoInfo.freeAddonMonths1;
                    tasks.push(function (callback) {
                        var updateQuery = "UPDATE promoCodesLogs SET status=0 "
                            + ", service_instance_number=" + db.escape(cache.serviceInstanceNumber)
                            + " WHERE id=" + db.escape(logId) + " AND status=2";

                        if (LOG) common.log("PromotionsManager", "mark promo log as pending, updateQuery=" + updateQuery);
                        db.query_err(updateQuery, function (err, result) {
                            if (err) {
                                common.error("PromotionsManager", "failed to mark promo as pending err code=" +
                                err.code + "error message = " + err.message);
                                promoInfo.updateQueryError = err;
                                return callback(err);
                            }

                            if (!result || result.changedRows <= 0) {
                                promoInfo["error"] = "Promo log items were not marked as pending (status==0)," +
                                " probably a race condition";
                                return callback(undefined, promoInfo);
                            }

                            if (LOG) common.log("PromotionsManager", "apply a promo, account=" + cache.account + ", number=" +
                            cache.number + ", productId=" + productId + ", bonusData=" + bonusData + ", bonusDataType=" +
                            bonusDataType + ", monthsCount=" + monthCount + ", waiver=" + waiverCents);

                            var productError = false;

                            var addBonus = function () {
                                if (productId || bonusData) {
                                    bonusManager.addBonus({
                                        noneRecurrentBonusId: productId,
                                        bonusData: bonusData,
                                        bonusDataType: bonusDataType,
                                        continueRecurrent: monthCount == -1,
                                        continueMonth: monthCount,
                                        prefix: cache.prefix,
                                        number: cache.number,
                                        executionKey: executionKey,
                                        metadata: {data1: code},
                                        waitForResponse: true
                                    }, function (err, result) {
                                        if (err) {
                                            productError = true;
                                            promoInfo.bonusError = err;
                                            common.error("PromotionsManager", "failed to subscribe for an addon, " +
                                            "productId=" + productId + ", err=" + err);
                                        } else {
                                            if (LOG) common.log("PromotionsManager", "succeed to subscribe for an addon, " +
                                            "productId=" + productId);
                                        }

                                        promoInfo["product"] = {
                                            err: err ? err.message : undefined,
                                            result: result
                                        }

                                        // operation can be finished without cleaning history
                                        // in case if bonus is not added it will be recovered after some time

                                        addWaiver(promoInfo, productError, null,  function (err, result) {
                                            callback(err, result)
                                        });
                                    });
                                } else {
                                    promoInfo["product"] = {
                                        result: {
                                            added: false,
                                            reason: "NO_PRODUCT_ID"
                                        }
                                    };
                                    addWaiver(promoInfo, productError, null,  function (err, result) {
                                        callback(err, result)
                                    });
                                }
                            }

                            addBonus();
                        });
                    });
                }

                var tasks = [];
                result.forEach(function (item) {
                    var product  = item.product_id ? ec.findPackage(ec.packages(), item.product_id) : undefined;
                    var promoInfo = {
                        code: item.code,
                        logId: item.log_id,
                        productId: product ? product.id : undefined,
                        bonusData: item.bonus_data,
                        bonusDataType: item.bonus_data_type,
                        monthCount: item.months_count,
                        waiverCents: item.waiver_cents,
                        waiverMonth: item.waiver_month,
                        overrideEffect: item.overrideEffect,
                        paidAddonNextMonth: item.paidAddonNextMonth,
                        freeAddon1: item.free_addon_1,
                        freeAddonMonths1: item.free_addon_months_1,
                        extraBillDay: item.extraBillDay
                    }

                    if (promoInfo.productId || promoInfo.bonusData) {
                        applyBonus(promoInfo);
                    } else if (promoInfo.freeAddon1 && promoInfo.freeAddonMonths1) {
                        addFreeAddon(promoInfo)
                    } else {
                        addWaiver(promoInfo, null, null,  function (err, result) {
                            return;
                        });
                    }
                });

                async.parallelLimit(tasks, 1, function (err, asyncResult) {
                    if (err) {
                        common.error("PromotionsManager", "error occurred while applying bonuses, err=" + err);
                        return callback(new BaseError("Failed to add all pending promo", "EC_ERROR"));
                    }

                    callback(undefined, {
                        added: true,
                        countAdded: asyncResult.length,
                        result: asyncResult
                    })
                });
            });
        });
    },

    removeCodeUsageLog: function (logId) {
        return new Bluebird ((resolve, reject) => {
            if (!logId) {
                reject(new BaseError("LogId not present in the params", "ERROR_INVALID_PARAM"),
                    {status: "ERROR_INVALID_PARAM"});
            }
            let id = logId;
            let q = "DELETE FROM promoCodesLogs WHERE id=" + db.escape(id);
            db.query_err(q, function (err, result) {
                if (err) {
                    return reject(err);
                }
                return resolve(result);
            });
        });
    },

}

function shouldGiveExtraBillCycleForPromo (promoInfo) {
    // When applying promo codes, if the promo code is configured in such a way that
    // an additional cycle is added if the promo is applied after a specific date.
    var currentDate = new Date().getDate();
    if ((promoInfo.extraBillDay > 0) && (currentDate > promoInfo.extraBillDay)) {
        return true;
    } else {
        return false;
    }

}

function randomString(length, chars) {
    if (!chars) {
        throw new Error('Argument \'chars\' is undefined');
    }

    var charsLength = chars.length;
    if (charsLength > 256) {
        throw new Error('Argument \'chars\' should not have more than 256 characters'
        + ', otherwise unpredictability will be broken');
    }

    var randomBytes = crypto.randomBytes(length);
    var result = new Array(length);

    var cursor = 0;
    for (var i = 0; i < length; i++) {
        cursor += randomBytes[i];
        result[i] = chars[cursor % charsLength];
    }

    return result.join('');
}

function randomPredefinedString(length) {
    return randomString(length,
        'ABCDEFGHJKLMNPQRSTUVWXYZ2356789');
}

function sendNotification(activity, prefix, number, referralCode, waiveAmount, waiveMonth, callback) {
    const notification = {
        teamID: (process.env.ROLE == 'mobile' ? 1 : 5),
        prefix,
        number,
        activity,
        referral_code: referralCode,
        waived_amount: (waiveAmount / 100),
        waived_count: waiveMonth
    }
    notificationSend.deliver(notification, undefined, function (err, result) {
        if (err) {
            common.error("PromotionsManager", "Can not send notification, err=" + err);
            if (callback) callback(err);
            return;
        }
        if (LOG) common.log("PromotionsManager", "Notification sent, result=" + JSON.stringify(result));
        if (callback) callback();
    });
}
