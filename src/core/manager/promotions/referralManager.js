var request = require('request');
var async = require('async');
var config = require('../../../../config');
var common = require('../../../../lib/common');
var db = require('../../../../lib/db_handler');
var ec = require('../../../../lib/elitecore');
var randomstring = require("randomstring");
var configManager = require('../config/configManager');
var bonusManager = require('../bonus/bonusManager');
var leaderboardManager = require('../bonus/leaderboardManager');
var subscriptionManager = require('../bonus/subscriptionManager');
var forbiddenDictionary = require('../../../../res/dictionary/forbidden');

var BaseError = require('../../errors/baseError');

var LOG = config.LOGSENABLED;

module.exports = {

    /**
     *
     */
    loadUserAccount: function (code, callback) {
        configManager.loadConfigMapForKey("bonus", "referral_bonus", function (err, item) {
            if (err) {
                return callback(err);
            }

            var discountCents;
            if (item && item.options) {
                discountCents = item.options.discount_cents_joined;
            } else {
                return callback(new BaseError("Config is not found", "CONFIG_NOT_FOUND"));
            }

            if (code) {
                code = code.toUpperCase();
            }

            var selectQuery = "SELECT * FROM referralCodes WHERE code=" + db.escape(code);
            common.log(selectQuery);
            db.query_err(selectQuery, function (err, rows) {
                if (err) {
                    if (LOG) common.error("ReferralManger", "can not query code, DB error, err=" + err);
                    return callback(new BaseError("Can not query code", "DB_ERROR"));
                }

                if (!rows || rows.length == 0) {
                    common.log("ReferralManger", "account not found, code=" + code);
                    if (callback) {
                        callback(undefined, {
                            valid: false,
                            reason: "NOT_FOUND",
                            discountCents: discountCents,
                            rowsCount: (!rows ? -1 : rows.length)
                        });
                    }
                } else {
                    var item = rows[0];
                    common.log("ReferralManger", "loaded accountNumber=" + item.account_number +
                    ", code=" + code);

                    var response;
                    response = {
                        valid: (item.valid ? true : false),
                        accountNumber: item.account_number,
                        discountCents: discountCents,
                        rowsCount: (!rows ? -1 : rows.length)
                    };

                    if (callback) {
                        callback(undefined, response);
                    }
                }
            });
        });
    },

    /**
     *
     */
    createUserReferralCode: function (prefix, number, newCode, callback) {
        if (!prefix || !number || !newCode) {
            return callback(new BaseError("Invalid params", "INVALID_PARAMS"));
        }

        if (newCode.length < 5 || newCode.length > 10) {
            return callback(new BaseError("Code length is not correct", "CODE_LENGTH_INVALID"));
        }

        if (!forbiddenDictionary.isAllowed(newCode)) {
            return callback(new BaseError("Invalid params", "INAPPROPRIATE_CONTENT"));
        }

        if (forbiddenDictionary.hasSpecialCharacters(newCode) || forbiddenDictionary.hasSpace(newCode)) {
            return callback(new BaseError("Invalid params", "INVALID_CHARS"));
        }

        newCode = newCode.toUpperCase();
        ec.getCustomerDetailsNumber(prefix, number, false, function (err, cache) {
            if (err || !cache) {
                if (LOG) common.error("ReferralManger", "user is not found, number=" + number + ", error=" + err.message);
                return callback(new BaseError("User is not found", "USER_NOT_FOUND"));
            }

            var selectQuery = "SELECT * FROM referralCodes WHERE account_number=" + db.escape(cache.account);
            db.query_err(selectQuery, function (err, rows) {
                if (err) {
                    if (LOG) common.error("ReferralManger", "can not query code, DB error, err=" + err.message);
                    return callback(new BaseError("Can not query code", "DB_ERROR"));
                }

                if (rows && rows.length >= 2) {
                    return callback(new BaseError("User already has > 2 referral codes", "CODE_MAX_LIMIT_REACHED"));
                }

                var selectQuery = "SELECT * FROM referralCodes WHERE code=" + db.escape(newCode);
                db.query_err(selectQuery, function (err, rows) {
                    if (err) {
                        if (LOG) common.error("ReferralManger", "can not query code, DB error, err=" + err.message);
                        return callback(new BaseError("Can not query code", "DB_ERROR"));
                    }

                    if (rows && rows[0]) {
                        if (rows[0].account_number !== cache.account) {
                            return callback(new BaseError("Code is already taken", "CODE_ALREADY_TAKEN"));
                        } else {
                            return callback(undefined, {code: newCode, new: false});
                        }
                    }

                    var insertQuery = "INSERT INTO referralCodes (account_number, code, generator) VALUES ("
                        + db.escape(cache.account) + ", " + db.escape(newCode) + ", " + db.escape("user") + ")";
                    db.query_err(insertQuery, function (err, rows) {
                        if (err) {
                            if (LOG) common.error("ReferralManger", "can not create new code, err=" + err.message);
                            return callback(new BaseError("Can not crete referral code", "DB_ERROR"));
                        }

                        callback(undefined, newCode, false);
                    });
                });

            });
        });
    },

    /**
     *
     */
    loadUserReferralCode: function (prefix, number, callback) {

        var createCode = function (accountNumber, attempt, callback) {
            var newCode = randomstring.generate(5).toUpperCase();
            var insertQuery = "INSERT INTO referralCodes (account_number, code) VALUES ("
                + db.escape(accountNumber) + ", " + db.escape(newCode) + ")";

            db.query_err(insertQuery, function (err, rows) {
                if (err) {
                    if (err.code === "ER_DUP_ENTRY") {
                        if (LOG) common.error("ReferralManger", "attempt=" + attempt +
                        ", duplicated code, DB error, err=" + err);
                        return callback(undefined, undefined, true);
                    } else {
                        if (LOG) common.error("ReferralManger", "attempt=" + attempt +
                        ", can not create referral code, DB error, err=" + err);
                        return callback(new BaseError("Can create referral code", "DB_ERROR"));
                    }
                }

                if (!rows) {
                    return callback(new BaseError("Can not create referral code", "ERROR"));
                }

                callback(undefined, newCode, false);
            });
        }

        var loadCode = function (accountNumber, serviceInstanceNumber, attempt, callback) {

            var selectQuery = "SELECT * FROM referralCodes WHERE " +
                "referralCodes.account_number=" + db.escape(accountNumber);

            db.query_err(selectQuery, function (err, rows) {
                if (err) {
                    if (LOG) common.error("ReferralManger", "attempt=" + attempt +
                    ", can not query code, DB error, err=" + err);
                    return callback(new BaseError("Can not query code", "DB_ERROR"));
                }

                if (!rows || rows.length == 0) {
                    if (attempt < 5) {
                        if (LOG) common.log("ReferralManger", "attempt=" + attempt +
                        ", no code presented, need to create new");

                        return createCode(accountNumber, attempt, function (err, code, duplicate) {
                            if (err) {
                                callback(err);
                            } else {
                                if (LOG) common.log("ReferralManger", "attempt=" + attempt +
                                ", created code=" + code + ", duplicate=" + duplicate);

                                loadCode(accountNumber, serviceInstanceNumber, attempt + 1, callback);
                            }
                        });
                    } else {
                        return callback(new BaseError("Code is not found", "NOT_FOUND"));
                    }
                }

                var code;
                var codeOld;
                rows.forEach(function (item) {
                    if (item.generator == "user") {
                        code = item.code;
                    }
                    if (item.generator == "random") {
                        codeOld = item.code;
                    }
                });

                if (!code) {
                    code = codeOld;
                    codeOld = undefined;
                }

                var queryReferrer = "SELECT id, service_instance_number " +
                    "FROM historyBonuses WHERE bonus_type='referral' AND data6=" + db.escape(serviceInstanceNumber);

                db.query_err(queryReferrer, function (err, rows) {
                    if (err) {
                        if (LOG) common.error("ReferralManger", "attempt=" + attempt +
                        ", can not query code, DB error, err=" + err);
                        return callback(new BaseError("Can not query code", "DB_ERROR"));
                    }

                    var referrer = rows && rows[0] ? rows[0] : undefined;
                    if (referrer && referrer.service_instance_number) {
                        ec.getCustomerDetailsServiceInstance(referrer.service_instance_number, false, function (err, cache) {
                            if (!err && cache) {

                                referrer.name = cache.billingFullName;
                                referrer.prefix = cache.prefix;
                                referrer.number = cache.number;

                                if (LOG) common.log("ReferralManger", "attempt=" + attempt + ", loaded code=" + code);
                                callback(undefined, {code: code, codeOld: codeOld, referrer: referrer});
                            } else {
                                callback(undefined, {code: code, codeOld: codeOld, referrer: referrer});
                            }
                        }, true);
                    } else {
                        if (LOG) common.log("ReferralManger", "attempt=" + attempt + ", loaded code=" + code);
                        callback(undefined, {code: code, codeOld: codeOld, referrer: referrer});
                    }
                });
            });
        }

        ec.getCustomerDetailsNumber(prefix, number, false, function (err, cache) {
            if (err || !cache) {
                if (LOG) common.error("ReferralManger", "user is not found, number=" + number +
                ", error=" + JSON.stringify(err));

                return callback(new BaseError("User is not found", "USER_NOT_FOUND"));
            }

            loadCode(cache.account, cache.serviceInstanceNumber, 0, function (err, codeResult) {
                if (err) {
                    return callback(err);
                }

                configManager.loadConfigMapForKey("bonus", "referral_bonus", function (err, item) {
                    if (err) {
                        return callback(err);
                    }

                    var bonusKb;
                    var discountAmount;

                    if (item && item.options) {

                        var referrerUserBonus = ec.findPackage(ec.packages(), item.options.bonus_product_referrer);
                        bonusKb = referrerUserBonus ? referrerUserBonus.kb : 0;

                        var discountAmountCents = item.options.discount_cents_joined;
                        discountAmount = discountAmountCents / 100;
                    } else {
                        return callback(new BaseError("Config is not found", "CONFIG_NOT_FOUND"));
                    }

                    var result = {
                        code: codeResult.code,
                        codeOld: codeResult.codeOld,
                        referrer: codeResult.referrer,
                        bonusKb: bonusKb,
                        bonusRecurrent: true,
                        discountAmount: discountAmount
                    };

                    if (callback) {
                        callback(undefined, result);
                    }
                });
            });

        });
    },

    /**
     *
     */
    addReferralBonus: function (prefix, joinedNumber, referralCode, executionKey, callback) {
        if (!callback) callback = () => {
        }

        var processBonus = function (options) {

            var joinedUser = options.joinedUser;
            var referrerUser = options.referrerUser;
            var referrerBonus = options.referrerBonus;

            var flashSaleBonus = options.flashSaleBonus;
            var flashSaleStartDate = options.flashSaleStartDate;
            var flashSaleEndDate = options.flashSaleEndDate;

            if (!joinedUser || !referrerUser) {
                return callback(new BaseError("Some user information is not found", "ERROR_CUSTOMER_NOT_FOUND"));
            }
            if (!referrerBonus) {
                return callback(new BaseError("Referrer user bonus product not found", "ERROR_PRODUCT_NOT_FOUND"));
            }

            var orn = joinedUser.orderReferenceNumber;
            var activationDate = new Date(joinedUser.serviceInstanceCreationDate);

            bonusManager.loadRegistrationDateByORN(orn, activationDate, (err, joinDate) => {

                var flashSale = 0;
                if (joinDate && flashSaleStartDate && flashSaleEndDate
                    && flashSaleStartDate.getTime() < joinDate.getTime()
                    && flashSaleEndDate.getTime() > joinDate.getTime()) {

                    if (LOG) common.log("ReferralManger", "update bonus, use 'flash sale' bonus, "
                    + (flashSaleBonus ? flashSaleBonus.name : "NONE"));

                    // flash sale, need to use flash sale bonus
                    if (flashSaleBonus) {
                        flashSale = 1;
                        referrerBonus = flashSaleBonus;
                    }
                }

                var metadata = {
                    data1: joinedUser.billingFullName,
                    data2: joinedUser.prefix,
                    data3: joinedUser.number,
                    data4: "referral_referred",
                    data5: joinedUser.account,
                    data6: joinedUser.serviceInstanceNumber,
                    data7: joinedUser.nric,
                    dataInt1: 0, // waiver
                    dataInt2: flashSale, // flash sale
                    dataDate1: joinDate
                };

                leaderboardManager.loadLeaderboardRatingBySI(referrerUser.serviceInstanceNumber, (err, result) => {
                    if (err) {
                        return callback(err);
                    }

                    var noDataHistoryItem = false;
                    if (leaderboardManager.getGoldenCircleConfig().dataGB <= result.dataGb) {
                        noDataHistoryItem = true;
                    }

                    bonusManager.addBonus({
                        noneRecurrentBonusId: referrerBonus.id,
                        noDataHistoryItem: noDataHistoryItem,
                        continueRecurrent: true,
                        continueMonth: -1,
                        prefix: referrerUser.prefix,
                        number: referrerUser.number,
                        executionKey: executionKey,
                        metadata: metadata,
                        bonusDataType: "referral",
                        bonusSubType: joinedUser.serviceInstanceNumber,
                        waitForResponse: true
                    }, function (err, result) {
                        if (err) {
                            return callback(err, {added: false, result: result});
                        }

                        if (LOG) common.log("ReferralManger", "saved subscription state, referralCode=" + referralCode);
                        callback(undefined, {added: true, result: result});

                        // in case if customers reaches specific amount of bonus data gb the customers are being moved
                        // from traditional leaderboard to 'golden ticket' leaderboard, all 'golden ticket'
                        // related notifications are being sent from LeaderboardManager

                        if (!noDataHistoryItem) {
                            sendNotification("bonus_referral_added", {
                                referralCode: referralCode,
                                joinedUser: joinedUser,
                                referrerUser: referrerUser,
                                referrerUserBonus: referrerBonus,
                                notificationKey: executionKey
                            }, function () {
                                // do nothing
                            });
                        }
                    });
                });
            });
        }

        var tasks = [];
        tasks.push(function (callback) {
            ec.getCustomerDetailsNumber(prefix, joinedNumber, false, function (err, cache) {
                if (err) {
                    common.error("ReferralManger", "Failed to load customer[joined]," +
                    " joinedNumber=" + joinedNumber + ", error=" + err.message);
                    return callback(new BaseError("Failed to load customer[joined] " + joinedNumber +
                    " (" + err.message + ")", "ERROR_CUSTOMER_NOT_FOUND"));
                }
                if (!cache) {
                    common.error("ReferralManger", "Customer[joined] not found, joinedNumber=" + joinedNumber);
                    return callback(new BaseError("Customer[joined] " + joinedNumber + " is not found",
                        "USER_NOT_FOUND"));
                }
                callback(undefined, {joinedUser: cache});
            });
        });
        tasks.push(function (callback) {
            var selectQuery = "SELECT * FROM referralCodes WHERE referralCodes.code=" + db.escape(referralCode);
            db.query_err(selectQuery, function (err, rows) {
                if (err) {
                    common.error("ReferralManger", "Failed to load code of customer[referrer]," +
                    " referralCode=" + referralCode + ", error=" + err.message);
                    return callback(new BaseError("Failed to load code of customer[referrer] " + referralCode
                    + " (" + err.message + ")", "DB_ERROR"));
                }

                if (!rows || !rows[0]) {
                    return callback(new BaseError("Referrer code is not found", "CODE_NOT_FOUND"));
                }

                var accountNumber = rows[0].account_number;
                ec.getCustomerDetailsAccount(accountNumber, false, function (err, cache) {
                    if (err) {
                        common.error("ReferralManger", "Failed to load customer[referrer]," +
                        " accountNumber=" + accountNumber + ", error=" + err.message);
                        return callback(new BaseError("Failed to load customer[referrer] " + accountNumber
                        + " (" + err.message + ")", "ERROR_CUSTOMER_NOT_FOUND"));
                    }
                    if (!cache) {
                        common.error("ReferralManger", "Customer[referrer] not found," +
                        " accountNumber=" + accountNumber);
                        return callback(new BaseError("Customer[referrer] " + accountNumber + " is not found",
                            "USER_NOT_FOUND"));
                    }

                    callback(undefined, {referredByUser: cache, codeId: rows[0].id});
                });
            });
        });

        async.parallelLimit(tasks, 2, (err, asyncResult) => {
            if (err) {
                return callback(err);
            }

            var joinedUser;
            var referrerUser;
            var codeId;
            asyncResult.forEach(function (item) {
                if (item.joinedUser) {
                    joinedUser = item.joinedUser;
                }
                if (item.referredByUser) {
                    referrerUser = item.referredByUser;
                }
                if (item.codeId) {
                    codeId = item.codeId;
                }
            });

            if (!joinedUser || !referrerUser) {
                return callback(new BaseError("Some user information is not found", "ERROR_CUSTOMER_NOT_FOUND"));
            }

            if (LOG) common.log("ReferralManger", "joined user number " + joinedUser.number
            + ", referred by user " + referrerUser.number);

            if (joinedUser.account === referrerUser.account) {
                return callback(new BaseError("Can not refer her/himself",
                    "ERROR"), {added: false});
            }

            configManager.loadConfigMapForKey("bonus", "referral_bonus", function (err, item) {
                if (err) {
                    return callback(err);
                }
                if (!item || !item.options || !item.options.enabled) {
                    return callback(undefined, {added: false});
                }

                var getProduct = function (productId) {
                    return productId ? ec.findPackage(ec.packages(), productId) : undefined;
                }
                var getDate = function (dateValue) {
                    return dateValue ? new Date(dateValue) : undefined;
                }

                processBonus({
                    joinedUser: joinedUser,
                    referrerUser: referrerUser,
                    referrerBonus: getProduct(item.options.bonus_product_referrer),
                    flashSaleBonus: getProduct(item.options.flash_sale_bonus_product),
                    flashSaleStartDate: getDate(item.options.flash_sale_start_date),
                    flashSaleEndDate: getDate(item.options.flash_sale_end_date)
                });
            });
        });
    },

    /**
     *
     */
    deactivateReferrerBonus: function (prefix, number, executionKey, callback) {
        ec.getCustomerDetailsNumber(prefix, number, false, function (err, cache) {
            if (err || !cache) {
                if (LOG) common.error("ReferralManger", "Terminated number is not found in EC, err=" + err);
                return callback(new BaseError("Terminated number user is not found", "USER_NOT_FOUND"));
            }

            var selectQuery = "SELECT id, service_instance_number FROM historyBonuses WHERE data6="
                + db.escape(cache.serviceInstanceNumber) + " AND bonus_type=" + db.escape("referral")
                + " AND deactivated=" + db.escape(0);
            db.query_err(selectQuery, function (err, result) {
                if (err) {
                    if (LOG) common.error("ReferralManger", "can not query code, DB error, err=" + err);
                    return callback(new BaseError("Can not query code", "DB_ERROR"));
                }

                if (!result || result.length < 1) {
                    return callback(undefined, {
                        unsubscribed: false,
                        reason: "No personal referral code has been used"
                    });
                }

                var serviceInstancesToUpdate = [];
                result.forEach(function (item) {
                    if (serviceInstancesToUpdate.indexOf(item.service_instance_number) == -1) {
                        serviceInstancesToUpdate.push(item.service_instance_number);
                    }
                });

                var resubscribeBonuses = function (tasks, referrerInstanceNumber) {
                    tasks.push(function (callback) {
                        ec.getCustomerDetailsService(referrerInstanceNumber, false, function (err, cache) {
                            if (err || !cache) {
                                if (LOG) common.error("ReferralManger", "Terminated number is not found in EC, err=" + err);
                                return callback(undefined, {
                                    unsubscribed: false,
                                    serviceInstanceNumber: referrerInstanceNumber,
                                    error: "User not found, EC"
                                });
                            }

                            subscriptionManager.updateNextBillBonuses(cache.prefix, cache.number, "[CMS][ReferralManager]",
                                executionKey, function (err, result) {
                                    return callback(undefined, {
                                        unsubscribed: err ? false : true,
                                        err: err ? err.message : undefined,
                                        prefix: cache.prefix,
                                        number: cache.number,
                                        serviceInstanceNumber: referrerInstanceNumber,
                                        result: result
                                    });
                                });
                        });
                    });
                };

                var markDeactivated = function (callback) {
                    var updateQuery = "UPDATE historyBonuses SET display_until_ts=" +
                        db.escape(common.getDateTime(ec.nextBill(cache.bill_cycle).endDateUTC)) +
                        " WHERE data6=" + db.escape(cache.serviceInstanceNumber) + " AND bonus_type=" + db.escape("referral");

                    db.query_err(updateQuery, function (err, result) {
                        if (err) {
                            if (LOG) common.error("ReferralManger", "Can not mark as deactivated, DB error, err=" + err);
                            return callback(new BaseError("Can not mark as deactivated", "DB_ERROR"),
                                {markedAsDeactivated: false, err: err.message});
                        }

                        callback(undefined, {markedAsDeactivated: true})
                    });
                };

                if (LOG) common.log("ReferralManger", "Resubscribe bonuses, instances="
                + JSON.stringify(serviceInstancesToUpdate));

                markDeactivated(function (err, result) {
                    if (err) {
                        return callback(err, {unsubscribed: false, err: err.message});
                    }

                    var tasks = [];
                    serviceInstancesToUpdate.forEach(function (serviceInstanceNumber) {
                        resubscribeBonuses(tasks, serviceInstanceNumber);
                    });

                    async.parallelLimit(tasks, 10, function (err, asyncResult) {
                        if (err) {
                            if (LOG) common.error("ReferralManger", "failed to unsubscribe referral bonuses");
                            return callback(err, {unsubscribed: false, result: asyncResult});
                        }

                        callback(undefined, {unsubscribed: true, result: asyncResult});
                    });
                });
            });
        });
    }
}

function sendNotification(activity, options, callback) {
    if (!options) options = {};

    var url = "http://" + config.MYSELF + ":" + config.WEBPORT + "/api/1/webhook/notifications/internal/" +
        (options.notificationKey ? options.notificationKey : config.NOTIFICATION_KEY);

    var prettifiedData = common.prettifyKBUnit(options.referrerUserBonus ? options.referrerUserBonus.kb : 0);
    var value = Math.round(prettifiedData.value * 10) / 10;
    var unit = prettifiedData.unit;

    request({
        uri: url,
        method: 'POST',
        timeout: 20000,
        json: {
            activity: activity,
            prefix: options.referrerUser ? options.referrerUser.prefix : undefined,
            number: options.referrerUser ? options.referrerUser.number : undefined,
            variables: {
                referral_code: options.referralCode,
                joined_user_name: options.joinedUser ? options.joinedUser.billingFullName : undefined,
                joined_user_prefix: options.joinedUser ? options.joinedUser.prefix : undefined,
                joined_user_number: options.joinedUser ? options.joinedUser.number : undefined,
                addonname: options.referrerUserBonus ? options.referrerUserBonus.name : undefined,
                kb: options.referrerUserBonus ? options.referrerUserBonus.kb : 0,
                recurrentsuffix: "/mo",
                value: value,
                unit: unit
            }
        }
    }, (err, response, body) => {
        if (err) {
            if (LOG) common.error("ReferralManger", "Can not send notification, err=" + err);
            if (callback) callback(err);
            return;
        }
        if (LOG) common.log("ReferralManger", "Notification sent, body=" + JSON.stringify(body)
        + ", response=" + JSON.stringify(response));
        if (callback) callback();
    });
}
