var request = require('request');
var async = require('async');

var config = require('../../../../config');
var common = require('../../../../lib/common');
var db = require('../../../../lib/db_handler');
var ec = require('../../../../lib/elitecore');

var log = config.LOGSENABLED;

var promotionsManager = require('./promotionsManager');
var referralManager = require('./referralManager');
var BaseError = require('../../errors/baseError');


module.exports = {

    /**
     *
     *
     */
    getSupportedTypes: function () {
        return ["STANCHART_BIN", "REFERRER_NUMBER"];
    },

    /**
     *
     *
     */
    getVerificationDialogTypes: function (type) {
        if (type === "STANCHART_BIN") {
            return {
                type: type,
                overrideType: "CREDIT_CARD_BIN",
                title: "Verify your credit/debit card",
                message: "Enter first 6 digits of your Standard Chartered credit/debit card to claim the promotion. " +
                "You must use a Standard Chartered credit/debit card to enjoy the promotion benefits.",
                input: {
                    hint: "123456",
                    type: "number",
                    limit: 6
                },
                button: "Verify"
            };
        } else if (type === "REFERRER_NUMBER") {
            return {
                type: type,
                overrideType: "NONE",
                title: "Referral Code Check",
                message: "Hey there! That referral code is quite popular! Since we want referrals to be kept within " +
                "friends and family, please verify by inputting the referrer's phone number.",
                input: {
                    hint: "88080000",
                    type: "number",
                    limit: 8
                },
                button: "Verify"
            };
        } else {
            return undefined;
        }
    },

    /**
     *
     *
     */
    verifyCode: function (code, value, checkPersonal, callback) {
        if (!callback) callback = () => {
        }

        if (!code) {
            return callback(new BaseError("Invalid params", "ERROR_INVALID_PARAMS"));
        }

        promotionsManager.loadPromoCode(code, (err, result) => {
            if (err) {
                if (err.status != "CODE_NOT_FOUND" || !checkPersonal) {
                    return callback(err);
                }

                return referralManager.loadUserAccount(code, function (err, result) {
                    if (err) {
                        return callback(err);
                    }
                    if (!result || !result.valid || !result.accountNumber) {
                        return callback(new BaseError("Oops! The code you've entered is not valid anymore.",
                            "CODE_NOT_FOUND"));
                    }

                    ec.getCustomerDetailsAccount(result.accountNumber, false, function (err, cache) {
                        if (err) {
                            return callback(err);
                        }
                        if (!cache || cache.number != value) {
                            return callback(new BaseError("Oops! You've failed to verify this code.",
                                "ERROR_REFERRER_NUMBER_VERIFICATION"));
                        }

                        return callback(undefined, {
                            valid: true,
                            verificationType: result.secondVerificationType,
                            message: "Success! Please continue to checkout."
                        });
                    });
                });
            }

            if (!result.secondVerificationType) {
                return callback(undefined, {valid: true});
            }

            if (result.secondVerificationType === "STANCHART_BIN") {
                return module.exports.verifyStandardCharterCard(value, (err, result) => {
                    if (err) {
                        return callback(err);
                    }
                    if (!result || !result.valid) {
                        return callback(new BaseError("This promotion is only valid for Standard Chartered credit/debit card. " +
                        "Please check your card and try again.", "ERROR_STANCHART_BIN_VERIFICATION"));
                    }

                    return callback(undefined, {
                        valid: true,
                        verificationType: result.secondVerificationType,
                        message: "Your credit/debit card has been successfully verified. Thank you!"
                    });
                });
            } else {
                return callback(new BaseError("Not supported verification type " + result.secondVerificationType,
                    "ERROR_INVALID_VERIFICATION_TYPE"));
            }
        });
    },

    /**
     *
     *
     */
    verifyStandardCharterCard: function (binNumber, callback) {
        if (!callback) callback = () => {
        }

        var bin = binNumber ? parseInt(binNumber) : -1;
        var binNumbers = [
            {value: 423179},
            {value: 430092},
            {value: 450873},
            {value: 450934},
            {min: 486418, max: 486419},
            {value: 514916},
            {min: 524355, max: 524358},
            {value: 540275},
            {value: 542113},
            {value: 549834},
            {value: 552352},
            {value: 553402},
            {value: 558855},
            {value: 558964},
        ];
        var valid = binNumbers.some((item) => {
            if (item.value == bin) {
                return true;
            } else if (bin >= item.min && bin <= item.max) {
                return true;
            }
        });

        return callback(undefined, {valid: valid});
    }
}
