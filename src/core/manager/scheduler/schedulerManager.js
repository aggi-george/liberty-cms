var async = require('async');
var db = require('../../../../lib/db_handler');
var ec = require('../../../../lib/elitecore');
var elitecoreUsageService = require('../../../../lib/manager/ec/elitecoreUsageService');
var common = require('../../../../lib/common');
const config = require('../../../../config');
var constants = require('../../../core/constants');
var packageManager = require(__core + '/manager/packages/packageManager');
var BaseError = require('../../errors/baseError');
const terminationManager = require('../account/terminationManager');
const elitecoreUserService = require('../../../../lib/manager/ec/elitecoreUserService');
const LOG = config.LOGSENABLED;

const SG_PREFIX = '65';

exports.createSchedule = createSchedule;
exports.updateSchedule = updateSchedule;
exports.deleteSchedule = deleteSchedule;
exports.getSchedules = getSchedules;
exports.scheduleCirclesSwitchNotifications = scheduleCirclesSwitchNotifications;
exports.removeCirclesSwitchSchedules = removeCirclesSwitchSchedules;
exports.saveRawSchedule = saveRawSchedule;

const circlesSwitchActivities = {
    notification: {
        circles_switch_1_day_active: 'circles_switch_1_day_active',
        circles_switch_7_days_active: 'circles_switch_7_days_active',
        circles_switch_14_days_active: 'circles_switch_14_days_active',
        circles_switch_21_days_active: 'circles_switch_21_days_active',
        circles_switch_low_data_usage: 'circles_switch_low_data_usager',
        circles_switch_21_days_before_end: 'circles_switch_21_days_before_end',
        circles_switch_1_days_before_end: 'circles_switch_1_days_before_end'
    },
    terminate: {
        termination_effective: 'termination_effective'
    }
}


function createSchedule(params, callback){
    var action = params.action;
    var activity = params.activity;
    var start = params.start;
    var serviceInstanceNumber = params.serviceInstanceNumber;
    if (invalidDate(start)) {
        return callback(new BaseError('Date is Invalid', 'INVALID_DATE'));
    }
    start = parseInt(start);
    if(action == "terminate") {
        ec.getCustomerDetailsServiceInstance(serviceInstanceNumber, true, function (err, cache) {
            let earliest = terminationManager.setTerminationTime(cache.serviceInstanceEarliestTerminationDate);
            const earliestTimestamp = new Date(earliest).getTime();
            const thresholdTimestamp = new Date(terminationManager.getThresholdTime(start)).getTime();

            if (!isValidTerminationDate(start, earliestTimestamp)) {
                callback(new BaseError('start is less than earliest possible termination date', 'INVALID_DATE'));
            } else if(isValidThresholdTime(thresholdTimestamp)) {
                callback(new BaseError('Too late to request for termination for this month. Please select next month', 'INVALID_SCHEDULE_TIME'));
            }else {
                checkForValidTerminationDate(serviceInstanceNumber, start)
                    .then(isValid => {
                        if (isValid) {
                            db.scheduler.findAndModify(
                                {
                                    start: {
                                        $gt: new Date().getTime()
                                    },
                                    serviceInstanceNumber: serviceInstanceNumber,
                                    action: 'terminate',
                                    activity: 'termination_effective',
                                    processed: {
                                        $exists: false
                                    }
                                },
                                [],
                                {
                                    start: start,
                                    serviceInstanceNumber: serviceInstanceNumber,
                                    action: "terminate",
                                    activity: "termination_effective",
                                    allDay: false,
                                    trigger: "ecomm",
                                    parallelLimitOne: true,
                                    title: "Terminate " + common.escapeHtml(serviceInstanceNumber),
                                    createdDate: new Date().getTime()
                                }, 
                                {
                                    upsert: true,
                                    new: true
                                }, 
                                function (err, result) {
                                    if (err && !result) {
                                        callback(err, null);
                                    }else {
                                        if (!result.value) {
                                            const id = result.lastErrorObject.upserted
                                            callback(null, id)
                                        }else {
                                            const id = result.value._id;
                                            callback(null, id);
                                        }
                                    }
                                });
                        }else {
                            callback(new BaseError('Invalid termination date', 'INVALID_DATE'));
                        }
                    })
                    .catch(err => {
                        common.error(err);
                        callback(new BaseError('Internal API ERROR', 'INTERNAL_API_ERROR'));
                    })
            }
        }, false);
    }else{
        callback(new BaseError('Action not allowed', 'INVALID_ACTION'));
    }
}

function updateSchedule(params, callback){
    var id = params.id;
    var newStart = params.newStart;
    var serviceInstanceNumber = params.serviceInstanceNumber;
    if (invalidDate(newStart)) {
        return callback(new BaseError('Date is Invalid', 'INVALID_DATE'));
    }
    newStart = parseInt(newStart);
    ec.getCustomerDetailsServiceInstance(serviceInstanceNumber, true, function (err, cache) {
        let earliest = terminationManager.setTerminationTime(cache.serviceInstanceEarliestTerminationDate);
        const earliestTimestamp = new Date(earliest).getTime();
        const thresholdTimestamp = new Date(terminationManager.getThresholdTime(newStart)).getTime();

        
        if (!isValidTerminationDate(start, earliestTimestamp)) {
            callback(new BaseError('start is not greater or equal to earliest possible termination date', 'INVALID_DATE'));
        } else if(isValidThresholdTime(thresholdTimestamp)) {
            callback(new BaseError('Too late to request for termination for this month. Please select next month', 'INVALID_SCHEDULE_TIME'));
        } else {
            checkForValidTerminationDate(serviceInstanceNumber, newStart)
                .then(isValid => {
                    if (isValid) {
                        db.scheduler.update({_id: db.objectID(id)}, {"$set": {"start": newStart}}, 
                        (err, result) => {
                            if(err){
                                callback(new BaseError("Update in mongo", "Error updating scheduler in mongo"));
                            }else{
                                callback();
                            }
                        });
                    }else {
                        callback(new BaseError('Invalid termination date', 'INVALID_DATE'));
                    }
                })
                .catch(err => {
                    common.error(err);
                    callback(new BaseError('Internal API ERROR', 'INTERNAL_API_ERROR'));
                })
        }
    });
}

function deleteSchedule(params, callback){
    db.scheduler.findOne({_id: db.objectID(params.id)}, function(err, item){
        if(err){
            return callback(err);
        }
        db.scheduler.update(
            {
                _id: db.objectID(params.id)
            }, 
            {
                "$set": {
                    "start": 0, 
                    "oldStart": item.start,
                    "processed": {
                        "result": "deleted"
                    }
                }
            }, 
            function(err, result){
                if(err){
                    callback(new BaseError("Deleting in mongo", "Error deleteing scheduler in mongo"));
                }else{
                    callback();
                }
        });
    });
}

function getSchedules(params, callback){
    if(params.id && params.id != "null"){
        db.scheduler.findOne({_id: db.objectID(params.id), start: {"$ne": 0}}, function(err, schedule){
            if(err){
                callback(new BaseError("Get scheduler", "Error fetching schedule for id in mongo"));
            }else if(!schedule){
                callback(new BaseError("Get scheduler", "Schedule deleted or not available"));
            }else{
                callback(null, schedule);
            }
        });
    } else {
        db.scheduler.find({start: {"$ne": 0}}).limit(100).toArray(function(err, schedules){
            if(err){
                callback(new BaseError("Get schedules", "Error fetching schedules in mongo"));
            }else{
                callback(null, schedules);
            }
        });
    }
}

function scheduleCirclesSwitchNotifications(serviceInstanceNumber, portInNumber, callback){
    elitecoreUserService.loadUserInfoByServiceInstanceNumber(serviceInstanceNumber, function (err, cache) {
        if(err){
            common.error("SchedulerManager Failed to load customer cache to schedule notifications for circles switch", err);
            return callback(err);
        }else{
            let currentTime = new Date().getTime();
            let schedulerItems = [];
            let baseNotificationEvent = {
                action: "notification",
                serviceInstanceNumber: serviceInstanceNumber,
                prefix: SG_PREFIX,
                email: cache.email,
                name: cache.billingFullName,
                allDay: false,
                trigger: "[CMS][INTERNAL]",
                title: "Circles Switch"
            };

            let circles_switch_1_day_active_start = currentTime + 3600000 * 24;
            let circles_switch_7_days_active_start = currentTime + 3600000 * 24 * 7;
            let circles_switch_14_days_active_start = currentTime + 3600000 * 24 * 14;
            let circles_switch_21_days_active_start = currentTime + 3600000 * 24 * 21;
            let circles_switch_low_data_usage_start = currentTime + 3600000 * 24 * 14;
            let circles_switch_21_days_before_end_start = new Date(constants.CIRCLES_SWITCH_REMINDER_TIMESTAMP_21_DAYS_LEFT).getTime();
            let circles_switch_1_days_before_end_start = new Date(constants.CIRCLES_SWITCH_REMINDER_TIMESTAMP_1_DAYS_LEFT).getTime();
            let circles_switch_reminder_schedule_max_date = new Date(constants.CIRCLES_SWITCH_REMINDER_MAX_DATE).getTime();
        
            if(circles_switch_1_day_active_start < circles_switch_reminder_schedule_max_date){
                baseNotificationEvent.activity = circlesSwitchActivities.notification.circles_switch_1_day_active;
                baseNotificationEvent.start = circles_switch_1_day_active_start;
                baseNotificationEvent.number = cache.number;
                schedulerItems.push(common.safeParse(JSON.stringify(baseNotificationEvent)));
            
                if(portInNumber){
                    baseNotificationEvent.number = portInNumber;
                    schedulerItems.push(common.safeParse(JSON.stringify(baseNotificationEvent)));
                }
            }
            
            if(circles_switch_7_days_active_start < circles_switch_reminder_schedule_max_date){
                baseNotificationEvent.activity = circlesSwitchActivities.notification.circles_switch_7_days_active;
                baseNotificationEvent.start = circles_switch_7_days_active_start;
                baseNotificationEvent.number = cache.number;
                schedulerItems.push(common.safeParse(JSON.stringify(baseNotificationEvent)));
            
                if(portInNumber){
                    baseNotificationEvent.number = portInNumber;
                    schedulerItems.push(common.safeParse(JSON.stringify(baseNotificationEvent)));
                }
            }

            if(circles_switch_14_days_active_start < circles_switch_reminder_schedule_max_date){
                baseNotificationEvent.activity = circlesSwitchActivities.notification.circles_switch_14_days_active;
                baseNotificationEvent.start = circles_switch_14_days_active_start;
                baseNotificationEvent.number = cache.number;
                schedulerItems.push(common.safeParse(JSON.stringify(baseNotificationEvent)));
            
                if(portInNumber){
                    baseNotificationEvent.number = portInNumber;
                    schedulerItems.push(common.safeParse(JSON.stringify(baseNotificationEvent)));
                }
            }

            if(circles_switch_21_days_active_start < circles_switch_reminder_schedule_max_date){
                baseNotificationEvent.activity = circlesSwitchActivities.notification.circles_switch_21_days_active;
                baseNotificationEvent.start = circles_switch_21_days_active_start;
                baseNotificationEvent.number = cache.number;
                schedulerItems.push(common.safeParse(JSON.stringify(baseNotificationEvent)));
            
                if(portInNumber){
                    baseNotificationEvent.number = portInNumber;
                    schedulerItems.push(common.safeParse(JSON.stringify(baseNotificationEvent)));
                }
            }

            if(circles_switch_low_data_usage_start < circles_switch_reminder_schedule_max_date){
                baseNotificationEvent.activity = circlesSwitchActivities.notification.circles_switch_low_data_usage;
                baseNotificationEvent.start = circles_switch_low_data_usage_start;
                baseNotificationEvent.number = cache.number;
                schedulerItems.push(common.safeParse(JSON.stringify(baseNotificationEvent)));
            
                if(portInNumber){
                    baseNotificationEvent.number = portInNumber;
                    schedulerItems.push(common.safeParse(JSON.stringify(baseNotificationEvent)));
                }
            }

            if(currentTime < circles_switch_21_days_before_end_start){
                baseNotificationEvent.activity = circlesSwitchActivities.notification.circles_switch_21_days_before_end;
                baseNotificationEvent.start = circles_switch_21_days_before_end_start;
                baseNotificationEvent.number = cache.number;
                schedulerItems.push(common.safeParse(JSON.stringify(baseNotificationEvent)));
            
                if(portInNumber){
                    baseNotificationEvent.number = portInNumber;
                    schedulerItems.push(common.safeParse(JSON.stringify(baseNotificationEvent)));
                }
            }
        
            if(currentTime < circles_switch_1_days_before_end_start){
                baseNotificationEvent.activity = circlesSwitchActivities.notification.circles_switch_1_days_before_end;
                baseNotificationEvent.start = circles_switch_1_days_before_end_start;
                baseNotificationEvent.number = cache.number;
                schedulerItems.push(common.safeParse(JSON.stringify(baseNotificationEvent)));
            
                if(portInNumber){
                    baseNotificationEvent.number = portInNumber;
                    schedulerItems.push(common.safeParse(JSON.stringify(baseNotificationEvent)));
                }
            }

            saveBulkEvents(schedulerItems, callback);
        }
    });
}

function removeCirclesSwitchSchedules(serviceInstanceNumber, callback){
    elitecoreUserService.loadUserInfoByServiceInstanceNumber(serviceInstanceNumber, function (err, cache) {
        if (err) {
            common.error('SchedulerManager Failed to load customer cache to remove schedules for circles switch', err);
            return callback(err);
        } else {
            let activities = Object.keys(circlesSwitchActivities.notification).concat(Object.keys(circlesSwitchActivities.terminate));
            const query = {
                start: { '$gt': new Date().getTime() },
                serviceInstanceNumber: serviceInstanceNumber,
                activity: { '$in': activities }
            }
            const set = {
                '$set': {
                    start: 0,
                    processed: {
                        status: false,
                        reason: 'removed due to port in success'
                    }
                }
            }
            const options = {
                multi: true
            }
            if (LOG) common.log('schedulerManager remove activities circles switch', query);
            db.scheduler.update(query, set, options, callback);
        }
    });
}

function saveRawSchedule(schedule, callback){
    if(schedule.start < new Date().getTime() + 7200000){
        callback({message: "scheduler event start date older than current date"})
    }else{
        db.scheduler.insert(schedule, callback);
    }
}

function saveBulkEvents(events, callback){
    var eventsToInsert = [];
    events.forEach(function(item){
        if(item.start > new Date().getTime() + 3600000){
            eventsToInsert.push(item);
        }
    });
    db.scheduler.insertMany(eventsToInsert, callback);
}
function invalidDate(start) {
    return new Date().getTime() > start || start != parseInt(start);
}

function isValidTerminationDate(start, earliestTimestamp) {
    return start >= earliestTimestamp;
}

function isValidThresholdTime(thresholdTimestamp) {
    return new Date().getTime() > thresholdTimestamp;
}

function checkForValidTerminationDate(serviceInstanceNumber, start) {
    const datesArray = [];
    return terminationManager.getTerminationDate(serviceInstanceNumber, 2)
        .then(result => {
            if (result.eligibility && result.eligibility.value) {
                result.termationDates.forEach(val => {
                    datesArray.push(new Date(val.date).getTime());
                });

                if ( datesArray.indexOf(start) > -1 ) {
                    return true;
                }
            }
            return false;
        });
}
