

const sip = require('sip');
const MD5 = require('MD5');
const Bluebird = require('bluebird');
const config = require(__base + '/config');
const common = require(__lib + '/common');
const db = require(__lib + '/db_handler');
const notificationSend = require(__manager + '/notifications/send');
const constants = require(__core + '/constants');

const DEBUG = false;
const LOG = config.LOGSENABLED;
const PORT = config.SIP && config.SIP.client && Object.prototype.hasOwnProperty.call(config.SIP.client, 'port')
    ? config.SIP.client.port : 5060;        // ideally, this should be random port

let options = {
    protocol: 'UDP',
    address: config.MYSELF,
    port: PORT
};
if (DEBUG) options.logger = {
    send: (m) => { common.log('send', JSON.stringify(m, null, 6)); },
    recv: (m) => { common.log('recv', JSON.stringify(m, null, 6)); },
    error: (e) => { common.error('sip', e.stack); },
};

sip.start(options, dialogHandler);
let dialogs = {};

class SipClient {
    static register(src, password, callback) {
        let auth = {
            scheme: 'Digest',
            username: `'${src}'`,
            realm: `'${config.REALM}'`,
            nonce: '\'\'',
            uri: `'sip:${config.REALM}'`,
            response: '\'\''
        };
        sendRegister(src, `${config.MYSELF}:${options.port}`, auth, config.ACTIVATEPROXY, (res) => {
            if (res.status == 200) {
                if (LOG) common.log('sip register', `ok ${src}`);
                callback();
            } else if (res.status == 401) {
                if (LOG) common.log('sip register', `register challenge ${src}`);
                auth.nonce = res.headers['www-authenticate'][0].nonce;
                let ha1str = `${src}:${auth.realm.replace(/"/g,'')}:${password}`;
                let ha2str = `REGISTER:${auth.uri.replace(/"/g,'')}`;
                let ha1 = MD5(ha1str);    // change this to simulate a user's hash
                let ha2 = MD5(ha2str);
                let responsestr = `${ha1}:${auth.nonce.replace(/"/g,'')}:${ha2}`;
                let responsehash = MD5(responsestr);
                auth.response = `'${responsehash}'`;
                sendRegister(src, `${config.MYSELF}:${options.port}`, auth, config.ACTIVATEPROXY, () => {
                    // if ok?
                    callback();
                });
            } else {
                var err = new Error(`unknown status ${res.status}`);
                common.error('sip register', err);
                callback(err);
            }
        });
    }

    static activate(src, dst, callback) {
        new Bluebird.Promise((resolve, reject) => {
            const timeout = setTimeout(() => {
                return reject(new Error('Timeout'));
            }, 30 * 1000);    // absolute timeout 30 seconds

            sendInvite(src, `${config.MYSELF}:${options.port}`, dst, config.ACTIVATEPROXY, (res) => {
                if (res.status >= 300) {
                    const err = new Error(`call failed with status ${res.status}`);
                    common.error('sip activate', err);
                    return reject(err);
                } else if (res.status < 200) {
                    if (LOG) common.log('sip activate', `call progress status ${res.status}`);
                } else {
                    if (LOG) common.log('sip activate', `call answered with tag ${res.headers.to.params.tag}`);
                    sendAck(res, () => { });
                    sendBye(res, src, `${config.MYSELF}:${options.port}`, () => {
                        clearTimeout(timeout);
                        resolve();
                    });
                    let id = [ res.headers['call-id'], res.headers.from.params.tag, res.headers.to.params.tag ].join(':');
                    if (!dialogs[id]) dialogs[id] = responseHandler;
                }
            });
        }).then(() => {
            callback(undefined, 'OK');
        }).catch((err) => {
            callback(err);
        });
    }

    static callVoicemail(callback) {
        let checkCDR = (callid, cb) => {
            setTimeout(() => {
                db.cdrs.find({ 'call_id' : callid }).toArray((errList, list) => {
                    let cdr = (list && list.length > 0) ? true : false;
                    cb(undefined, cdr);
                });
            }, 1000);
        };
        let call = (number, cb) => {
            sendInvite(number, `${config.MYSELF}:${options.port}`, number, config.ACTIVATEPROXY, (res) => {
                if (res.status == 200) {
                    sendAck(res, () => { });
                    setTimeout(() => {
                        sendBye(res, number, `${config.MYSELF}:${options.port}`, () => {
                            if (res.status == 200) {
                                checkCDR(res.headers['call-id'], (err, cdr) => {
                                    cb(undefined, {
                                        number: number,
                                        status: res.status,
                                        reason: res.reason,
                                        cdr: cdr
                                    });
                                });
                            }
                        });
                    }, 20 * 1000);    // hangup after 20 seconds
                }    // else ignore
            });
        };
        let getPromise = (number) => {
            return new Bluebird.Promise((resolve, reject) => {
                const timeout = setTimeout(() => {
                    return reject(new Error('Timeout'));
                }, 30 * 1000);    // absolute timeout 30 seconds
                call(number, (err, result) => {
                    clearTimeout(timeout);
                    return resolve(result);
                });
            });
        };
        const jobs = [];
        constants.VOICEMAIL.numbers.forEach((item) => {
            jobs.push(getPromise(item));
        });
        Bluebird.all(jobs).then((result) => {
            callback(undefined, result);
        }).catch((error) => {
            callback(error);
        });
    }

    static testVoicemail(callback) {
        const notifyPromise = Bluebird.promisify(notificationSend.deliver);
        let variables = {
            activity: 'generic_report',
            teamID: 5,
            reportSubject: 'Voicemail Test',
            labelA: '',
            tableA: '',
            labelB: '',
            tableB: '',
            labelC: '',
            tableC: '' };
        SipClient.callVoicemail((error, result) => {
            if (error) {
                variables.labelA = error.stack;
                return callback(undefined, notifyPromise(variables, null));
            }
            var template = {
                style: {
                    table: 'border-collapse:collapse',
                    th: 'border:1px solid black',
                    td: 'border:1px solid black' },
                head: [ 'number', 'status', 'reason', 'cdr' ],
                body: [ ] };
            let foundError = false;
            result.forEach((item) => {
                if (item.status != 200 || item.reason != 'OK' || !item.cdr) foundError = true;
                template.body.push([item.number, item.status, item.reason, item.cdr]);
            });
            variables.tableA = template;
            if (foundError) return callback(undefined, notifyPromise(variables, null));
            else callback();
        });
    }

}

function dialogHandler(req) {
    if (req.headers.to.params.tag) {    // in-dialog
        if (LOG) common.log('dialogHandler', `in dialog ${req.method}`);
        const id = [ req.headers['call-id'], req.headers.to.params.tag, req.headers.from.params.tag ].join(':');
        if (dialogs[id]) {
            dialogs[id](req, function() {
                if (req.method === 'BYE') delete dialogs[id];
            });
        } else sip.send(sip.makeResponse(req, 481, 'Call doesn\'t exists'));
    } else {
        sip.send(sip.makeResponse(req, 405, 'Method not allowed'));
    }
}

function responseHandler(req, callback) {
    if (req.method === 'BYE') {
        if (LOG) common.log('sip responseHandler', 'call received bye');
        sip.send(sip.makeResponse(req, 200, 'Ok'), callback);
    } else {
        sip.send(sip.makeResponse(req, 405, 'Method not allowed'), callback);
    }
}

function sendRegister(fromUser, fromDomain, auth, toDomain, callback) {
    const rstring = Math.floor(Math.random()*1e6).toString();
    const host = config.MYFQDN.split('.')[0];
    const datehash = MD5(new Date().toISOString());
    const message = {
        method: 'REGISTER',
        uri: `sip:${toDomain}`,
        headers: {
            to: { uri: `sip:${fromUser}@${fromDomain}` },
            from: { uri: `sip:${fromUser}@${fromDomain}`, params: { 'tag' : rstring() } },
            'call-id': `${host}-${datehash}-${rstring}`,
            cseq: { method: 'REGISTER', seq: Math.floor(Math.random() * 1e5) },
            contact: [ { uri: `sip:${fromUser}@${fromDomain}` } ],
            authorization: [ auth ],
            supported: 'path',
            path: [ { uri: `sip:${fromDomain};lr` } ]
        }
    };
    sip.send(message, callback);
}

function sendInvite(fromUser, fromDomain, toUser, toDomain, callback) {
    const rstring = Math.floor(Math.random()*1e6).toString();
    const host = config.MYFQDN.split('.')[0];
    const datehash = MD5(new Date().toISOString());
    let message = {
        method: 'INVITE',
        uri: `sip:${toUser}@${toDomain}`,
        headers: {
            to: { uri: `sip:${toUser}@${toDomain}` },
            from: { uri: `sip:${fromUser}@${fromDomain}`, params: { tag: rstring } },
            'call-id': `${host}-${datehash}-${rstring}`,
            cseq: { method: 'INVITE', seq: Math.floor(Math.random() * 1e5) },
            'content-type': 'application/sdp',
            contact: [ { uri: `sip:${fromUser}@${fromDomain}` } ]
        },
        content:
            'v=0\r\n' +
            'o=- 13374 13374 IN IP4 ${config.MYSELF}\r\n' +
            's=-\r\n' +
            'c=IN IP4 ' + config.MYSELF + '\r\n' +
            't=0 0\r\n' +
            'm=audio 16424 RTP/AVP 0 8 101\r\n' +
            'a=rtpmap:0 PCMU/8000\r\n' +
            'a=rtpmap:8 PCMA/8000\r\n' +
            'a=rtpmap:101 telephone-event/8000\r\n' +
            'a=fmtp:101 0-15\r\n' +
            'a=ptime:30\r\n' +
            'a=sendrecv\r\n'
    };
    sip.send(message, callback);
}

function sendAck(res, callback) {
    let message = {
        method: 'ACK',
        uri: res.headers.contact[0].uri,
        headers: {
            to: res.headers.to,
            from: res.headers.from,
            'call-id': res.headers['call-id'],
            cseq: { method: 'ACK', seq: res.headers.cseq.seq },
            via: [],
        }
    };
    const recordRoute = res.headers['record-route'] || undefined;
    if (recordRoute && (recordRoute.length > 0)) message.headers.route = recordRoute.reverse();
    sip.send(message, callback);
}

function sendBye(res, fromUser, fromDomain, callback) {
    let message = {
        method: 'BYE',
        uri: res.headers.contact[0].uri,
        headers: {
            to: res.headers.to,
            from: res.headers.from,
            'call-id': res.headers['call-id'],
            cseq: { method: 'BYE', seq: (res.headers.cseq.seq + 1) },
            contact: [ { uri: `sip:${fromUser}@${fromDomain}` } ]
        },
    };
    const recordRoute = res.headers['record-route'] || undefined;
    if (recordRoute && (recordRoute.length > 0)) message.headers.route = recordRoute;    // for bye, no need to reverse
    sip.send(message, callback);
}

module.exports = SipClient;
