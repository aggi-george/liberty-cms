var sip = require('sip');
var proxy = require('sip/proxy');
var config = require(__base + '/config');
var common = require(__lib + '/common');

var DEBUG = true;
var LOG = config.LOGSENABLED;

var options = {
    "protocol" : "UDP",
    "address" : config.MYSELF,
    "port" : 5080,
}
if (DEBUG) options.logger = {
    "send" : function(m) { common.log("send", JSON.stringify(m, null, 6)); },
    "recv" : function(m) { common.log("recv", JSON.stringify(m, null, 6)); }
}

var dialogs = {};
var contacts = {};

module.exports = {

    init: function() {
        proxy.start(options, messageHandler);
    }

}

function messageHandler(req) {
    if (!req.headers['p-ingress']) {
        var user = sip.parseUri(req.headers.to.uri).user;
        if (req.method === 'REGISTER') authReg(user, req);
        else if (req.method === 'INVITE') authInvite(user, req);
    }
}

function authReg(user, req) {
}

function auth(req) {

        contacts[user] = req.headers.contact;
        var res = sip.makeResponse(req, 200, 'Ok');
        res.headers.to.tag = Math.floor(Math.random() * 1e6);

        proxy.send(res);
}
