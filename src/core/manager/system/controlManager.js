var config = require('../../../../config');
var common = require('../../../../lib/common');
var db = require('../../../../lib/db_handler');
var hostManager = require(__manager + '/system/hostManager');
var child = require('child_process');
var exec = child.exec;
var spawn = child.spawn;

var LOG = config.LOGSENABLED;

module.exports = {

    bssControl: function(serverName, action, args, callback) {
        getHost(serverName, function(err, server) {
            if (err) return callback(err);
            var allowedActions = [ "block", "allow", "show", "syncBills" ];
            if (allowedActions.indexOf(action) == -1) {
                return callback(new Error("Invalid Action"));
            }
            if (action == "syncBills") {
                if (args) {
                    syncBills(server, args, callback);
                } else {
                    db.cache_get("cache", "sync_bills_" + server, function(cache) {
                        callback(undefined, cache);
                    });
                }
            } else {
                var options = { "timeout" : 10000 };
                var cmd = "ssh -q -o StrictHostKeyChecking=no -o PasswordAuthentication=no nodeuser@" + server + " ./bss_control.sh " + action;
                exec(cmd, options, function(err, result) {
                    if (!err) {
                        var resObj = { "host" : server, "result" : result };
                        db.cache_get("cache", "sync_bills_" + server, function(cache) {
                            if (cache) resObj.syncBills = cache;
                            callback(err, resObj);
                        });
                    } else {
                        if (LOG) common.error("bssControl", err);
                        callback(err);
                    }
                });
            }
        });
    },

    serviceControl: function(serverName, action, args, callback) {
        getHost(serverName, function(err, server) {
            if (err) return callback(err);
            var allowedActions = [ "reload", "restart", "start", "stop", "jlist" ];
            if (allowedActions.indexOf(action) == -1) {
                return callback(new Error("Invalid Action"));
            }
            var options = { "timeout" : 10000 };
            exec("ssh -q -o StrictHostKeyChecking=no -o PasswordAuthentication=no nodeuser@" + server +
                " pm2 " + action + " " + args, options, function(err, result) {
                if (!err) {
                    var resObj = { "host" : server, "result" : result };
                    callback(err, resObj);
                } else {
                    if (LOG) common.error("serviceControl", err);
                    callback(err);
                }
            });
        });
    }
}

function getHost(serverName, callback) {
    var server = config[serverName.toUpperCase()];
    if (!server) {
        callback(new Error("Invalid Server"));
    } else if (config.DEV && hostManager.isProduction(serverName)) {
        callback(new Error("Invalid Server on Staging"));
    } else {
        callback(undefined, server);
    }
}

function execute (cmd, callback) {
    exec(cmd, function(error, stdout, stderr) {
        if (!error) {
            callback(undefined, common.safeParse(stdout));
            if (LOG) common.log("Execute", cmd);
        } else {
            callback(error);
            if (LOG) common.error("Execute", cmd);
        }
    });
}

function syncBills(server, args, callback) {
    var obj = { "start" : new Date().getTime(), "server" : server, "args" : args };
    db.cache_lock("cache", "sync_bills_" + server, obj, 10 * 60 * 60 * 1000, function(lock) {
        if (!lock) {
            if (LOG) common.log("syncBills", "Sync bills for " + server + " is locked");
            return callback(new Error("syncBills locked"));
        } else {
            var options = { "timeout" : 10000 };
            var cArgs = [ "-amz", "--progress", "nodeuser@" + server + ":/opt/crestelsetup/exportbills/live/pdf/all/" + args + "/", "/mnt/bills/live/all/" + args + "/" ];
            var rsync = spawn("rsync", cArgs, options);
            rsync.stdout.on("data", function(data) {
                var chunk = data.toString();
                var progressStr = chunk ? chunk.split("=")[1] : undefined;
                var remaining = progressStr ? progressStr.split("/")[0] : undefined;
                var total = progressStr && remaining ? progressStr.split("/")[1].split(")")[0] : undefined;
                if (remaining && total) {
                    if (!obj.total) {
                        obj.total = total;
                        callback(undefined, obj);
                    }
                    obj.remaining = remaining;
                    db.cache_put("cache", "sync_bills_" + server, obj, 5 * 60 * 60 * 1000); // lock 5 minutes after
                }
            });
            rsync.stderr.on("data", function(data) {
                if (!obj.error) {
                    obj.error = data.toString();
                    callback(new Error("syncBills error"), obj);
                    db.cache_del("cache", "sync_bills_" + server);
                }
                if (LOG) common.error("syncBills error", data.toString());
            });
            rsync.on("close", function(code) {
                if (!obj.total) {
                    obj.total = 0;
                    callback(undefined, obj);
                }
                if ( (code != 0) || (obj.remaining > 0) || (obj.total == 0) ) {
                    db.cache_del("cache", "sync_bills_" + server);
                }
                if (LOG) common.log("syncBills done", code);
            });
        }
    });
}

function bypass(req, res) {
    var server = "";
    var traffic = "";
    if (req.params.server == "epc01") {
        server = config.LWEPC01;
        traffic = config.TRAFFIC_EPC01;
    } else if (req.params.server == "epc02") {
        server = config.LWEPC02;
        traffic = config.TRAFFIC_EPC02;
    } else if (req.params.server == "bypass_server") {
        server = config.BYPASS;
        traffic = config.TRAFFIC_BYPASS;
    } else {
        return res.status(400).json({
            code: -1, status: "invalid server"
        })
    }

    if (correctKey.indexOf(req.params.key) == -1) {
        common.log("Bypass Control - Invalid Key"); 
        return res.status(400).json({
            code: -1, status: "invalid key"
        })	
    }

    if (req.params.status == "divert") {
        exec("$HOME/scripts/bash/divert_traffic.exp.sh " + server + " " + traffic, puts);
        common.log("Bypass Control: Divert traffic " + server +  " Successful")
    } else if (req.params.status == "flush"){
        exec("$HOME/scripts/bash/flush_iptables.exp.sh " + server, puts);
        common.log("Bypass Control: Flush IPTables " + server +  " Successful")
    } else if (req.params.status == "tcpkill"){
        exec("$HOME/scripts/bash/kill_tcpconnections.exp.sh " + server + " " + traffic, puts);
        common.log("Bypass Control: Kill TCP Connection " + server +  " Successful")
    } else if (req.params.status == "restart") {
        exec("$HOME/scripts/bash/restart_bypass.exp.sh " + server, puts);
        common.log("Bypass Service - " + server +  " Successful")
    } else if (req.params.status == "start"){
        exec("$HOME/scripts/bash/start_bypass.exp.sh " + server, puts);
        common.log("Bypass Service - " + server +  " Successful")
    } else if (req.params.status == "stop"){
        exec("$HOME/scripts/bash/stop_bypass.exp.sh " + server, puts);
        common.log("Bypass Service - " + server +  " Successful")
    } else {
        return res.status(400).json({
            code: -1, status: "invalid status"
        })	
    }

    return res.status(200).json({
        code: 0, status: "OK"
    })
}

function bypass_test(req, res) {
    var server = "";
    var traffic = "";
    if (req.params.server == "test_server") {
        server = config.LWTES01;
        traffic = config.TRAFFIC_TES01;
    } else if (req.params.server == "bypass_server") {
        server = config.BYPASS;
        traffic = config.TRAFFIC_BYPASS;
    } else {
        return res.status(400).json({
            code: -1, status: "invalid server"
        })
    }

    if (correctKey.indexOf(req.params.key) == -1) {
        common.log("Bypass Control - Invalid Key"); 
        return res.status(400).json({
            code: -1, status: "invalid key"
        })	
    }

    if (req.params.status == "divert") {
        exec("$HOME/scripts/bash/divert_traffic.exp.sh " + server + " " + traffic, puts);
        common.log("Bypass Control (Test): Divert traffic " + server +  " Successful");
    } else if (req.params.status == "flush"){
        exec("$HOME/scripts/bash/flush_iptables.exp.sh " + server, puts);
        common.log("Bypass Control (Test): Flush IPTables " + server +  " Successful")
    } else if (req.params.status == "tcpkill"){
        exec("$HOME/scripts/bash/kill_tcpconnections.exp.sh " + server + " " + traffic, puts);
        common.log("Bypass Control (Test): Kill TCP Connection " + server +  " Successful")
    } else if (req.params.status == "restart") {
        exec("$HOME/scripts/bash/restart_bypass_test.exp.sh " + server, puts);
        common.log("Bypass Service (Test) - " + server +  " Successful")
    } else if (req.params.status == "start"){
        exec("$HOME/scripts/bash/start_bypass_test.exp.sh " + server, puts);
        common.log("Bypass Service (Test) - " + server +  " Successful")
    } else if (req.params.status == "stop"){
        exec("$HOME/scripts/bash/stop_bypass_test.exp.sh " + server, puts);
        common.log("Bypass Service (Test) - " + server +  " Successful")
    } else {
        return res.status(400).json({
            code: -1, status: "invalid status"
        })	
    }

    return res.status(200).json({
        code: 0, status: "OK"
    })
}
