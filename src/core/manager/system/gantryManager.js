var config = require(__base + '/config');
var common = require(__lib + '/common');
var db = require(__lib + '/db_handler');
var slack = require('../../../../src/notifications/slack');
var constants = require(__core + '/constants');
var analyticsManager = require(__manager + '/analytics/analyticsManager');
var configManager = require(__manager + '/config/configManager');

var LOG = config.LOGSENABLED;

module.exports = {
    allowedActions: [
        "throttle",
        "block",
        "allow",
        "get"
    ],

    getTypes: function(id, hour, ampm, blockUntil) {
        var types = [
            {
                "id" : "error_generic",
                "httpStatus" : 500,
                "code" : 800,
                "details" : "Request timed out, server is taking too long to respond",
                "title" : "Request Timeout",
                "description" : "Server taking too long to respond, please try again.",
                "monkey" : 0,
                "img" : "https://s3-ap-southeast-1.amazonaws.com/kirk.circles.asia/images/robot_error_sample.png"
            },{
                "id" : "error_db",
                "httpStatus" : 500,
                "code" : 801,
                "details" : "Database timeout",
                "title" : "Hmm a small hiccup.",
                "description" : "A quick refresh and everything should be ok.",
                "monkey" : 0,
                "img" : "https://s3-ap-southeast-1.amazonaws.com/kirk.circles.asia/images/robot_error_sample.png"
            },{
                "id" : "retry_generic",
                "httpStatus" : 503,
                "code" : 900,
                "details" : "System maintenance is ongoing",
                "title" : "System is under maintenance",
                "description" : "Our team is working hard to get things running soon. Please check back after " + hour + " " + ampm + " SGT",
                "monkey" : 0,
                "img" : "https://s3-ap-southeast-1.amazonaws.com/kirk.circles.asia/images/robot_error_sample.png",
                "block_until" : blockUntil
            },{
                "id" : "retry_billcycle",
                "httpStatus" : 503,
                "code" : 900,
                "details" : "Bill Cycle Maintenance",
                "title" : "It's crunch time!",
                "description" : "Our team is now processing everyone's bills and the app may be a bit unresponsive for a few people. Please bear with us.",
                "monkey" : 0,
                "img" : "https://s3-ap-southeast-1.amazonaws.com/kirk.circles.asia/images/robot_error_sample.png",
                "block_until" : blockUntil
            },{
                "id" : "retry_bss",
                "httpStatus" : 500,
                "code" : 901,
                "details" : "BSS is down or unreachable",
                "title" : "Oops! Something went wrong.",
                "description" : "Our team is working hard to get things back and running. Please check back after 60 minutes",
                "monkey" : 0,
                "img" : "https://s3-ap-southeast-1.amazonaws.com/kirk.circles.asia/images/robot_error_sample.png",
                "block_until" : blockUntil
            }
        ];
        if (id && hour && ampm && blockUntil) {
            var obj = types.filter(function(o) { if (id == o.id) return o; else return undefined; })[0];
            if (obj) return obj;
            else return types.filter(function(o) { if ("error_generic" == o.id) return o; else return undefined; })[0]; 
        } else return types.map(function(o) { return { "id" : o.id, "httpStatus" : o.httpStatus } });
    },

    get: function(callback) {
        db.cache_get("cache", "mobile_gantry", function(cache) {
            callback(undefined, cache);
        });
    },

    allow: function(callback) {
        module.exports.get(function(err, cache) {
            if (cache) {
                slack.send({
                    "username" : "Throttle Bot (" + cache.id + ") " + config.MYFQDN,
                    "text" : "Throttle " + (parseFloat(cache.throttle) * 100) + " % [" + process.pid + "] - cleared"
                });
                db.cache_del("cache", "mobile_gantry");
            }
        });
        callback();
    },

    block: function(id, throttle, timeout, callback) {
        if (timeout != parseInt(timeout)) return callback(new Error("timeout invalid"));
        var blockUntil = new Date().getTime() + (timeout * 1000);
        var d = new Date(blockUntil + ( 9 * 60 * 60 * 1000));    // SGT with offset
        var noon = new Date(d.getFullYear(), d.getMonth(), d.getDate(), 12, 0, 0);
        var hour = d.getHours() > 12 ? d.getHours() - 12 : d.getHours();
        var ampm = (d.getTime() < noon.getTime()) ? "am" : "pm";
        if (hour == 0) hour = 12;

        var checkBack = new Date(blockUntil).getHours() + 1
        var blockObj = module.exports.getTypes(id, hour, ampm, blockUntil);
        if (blockObj.httpStatus == 503) throttle = 1;    // override
        if (throttle && (throttle < 1)) blockObj.throttle = throttle;
        db.cache_put("cache", "mobile_gantry", blockObj,  timeout * 1000);
        slack.send({
            "username" : "Throttle Bot (" + id + ") " + config.MYFQDN,
            "text" : "Throttle " + (parseFloat(throttle) * 100) + " % [" + process.pid + "] - " + timeout + " s"
        });
        callback();
    },

    ecTimeoutsCheck: function() {
        return setInterval(function() {
            db.cache_lock('cache', 'ecTimeoutsCheck', 1, constants.THRESHOLDS.ECTIMEOUTSINTERVAL - 5000, (lock) => {
                if (!lock) {
                    if (LOG) common.log('ecTimeoutsCheck', 'ecTimeoutsCheck is locked');
                } else {
                    checkStats();
                }
            });
        }, constants.THRESHOLDS.ECTIMEOUTSINTERVAL); 
    },

}

function checkStats() {
    var end = new Date().getTime();
    var start = end - constants.THRESHOLDS.ECTIMEOUTSWINDOW;
    analyticsManager.loadEcOverallStats(start, end, function(err, result) {
        if (!result) result = { "totalTimeouts" : 0 };
        if (config.SIMULATEECTIMEOUTS) result.totalTimeouts = (new Date().getTime() % 4) * 100;
        var throttle = 1;
        constants.THRESHOLDS.ECTIMEOUTS.forEach(function(item, idx) {
            if (item <= result.totalTimeouts) throttle = Math.round( (1 - (idx / 10)) * 100 ) / 100;
        });
        if (throttle < 1) {
            common.error("ecTimeoutsCheck", JSON.stringify(result));
            module.exports.block("retry_bss", throttle, 30 * 60, ()=>{});
        } else module.exports.allow(()=>{});

        let count = 0;
        let options = {};
        if (result.stats) result.stats.forEach((item) => {
            if (item && item.list && item.list[0] && item.list[0].totalTimeouts > 40) {
                // has to be in SGT fix this!
                const expire = common.getDateTime(end + (8 * 60 * 60 * 1000) + (2 * constants.THRESHOLDS.ECTIMEOUTSINTERVAL));
                let key = item.host == 'LWBSS01' ? 'bss1_disabled_until' :
                    item.host == 'LWBSS02' ? 'bss2_disabled_until' :
                    item.host == 'LWBSS03' ? 'bss3_disabled_until' :
                    item.host == 'LWBSS04' ? 'bss4_disabled_until' :
                    item.host == 'LWTES01' ? 'bssstaging_disabled_until' : undefined;
                if (LOG) common.log('checkStats', `${key} ${expire}`);
                if (key) {
                    if (count/result.stats.length < 0.5) {
                        options[key] = expire;
                        count++;
                    }
                    else common.error('checkStats', `Overflow count=${count} ${item.host} ${item.totalTimeouts}`);
                } else {
                    common.error('checkStats', `Unsupported host ${item.host}`);
                }
            } else {
                if (LOG) common.log('checkStats', `OK ${JSON.stringify(item)}`);
            }
        });
        if (count > 0) {
            if (LOG) common.log('checkStats', options);
            configManager.updateConfigItem('elitecore_bss_pool', options, () => {});
            Object.keys(options).forEach((key) => {
                slack.send({
                    username: `Throttle Bot (${key}) ${config.MYFQDN}`,
                    text: `Throttle until ${options[key]} [${process.pid}]`
                });
            });
        }
    });
}
