var config = require('../../../../config');
var common = require('../../../../lib/common');
var ec = require('../../../../lib/elitecore');
var db = require('../../../../lib/db_handler');
var async = require('async');
var exec = require('child_process').exec;
var request = require('request');
var blocked = require('blocked');
var slack = require('../../../../src/notifications/slack');
var notificationSend = require(__core + '/manager/notifications/send');
const constants = require('../../../../src/core/constants');

exports.executeBasicHealth = executeBasicHealth;
exports.extractImportantParamsFromPm2 = extractImportantParamsFromPm2;
exports.getPm2List = getPm2List;
exports.generateHealthStatus = generateHealthStatus;
exports.checkBlocking = checkBlocking;
exports.handleSchedulerUncaughtException = handleSchedulerUncaughtException;
exports.handleSchedulerUnhandledRejection = handleSchedulerUnhandledRejection;
exports.sendAlertWithTable = sendAlertWithTable;

var thresholdsList = {
	"cms": {"status": "online", "maxmem": 500, "maxcpu": 60},
	"bssmw": {"status": "online", "maxmem": 500, "maxcpu": 60},
	"hierarchy": {"status": "online", "maxmem": 500, "maxcpu": 60},
	"queue": {"status": "online", "maxmem": 500, "maxcpu": 60},
	"scheduler": {"status": "online", "maxmem": 500, "maxcpu": 60},
	"circles": {"status": "online", "maxmem": 500, "maxcpu": 60},
	"sync": {"status": "online", "maxmem": 500, "maxcpu": 60}
};

function executeBasicHealth(finalCallback) {
	//test for customer cache
	async.parallel([
		function(callback){
			var latency = new Date().getTime();
			ec.getCustomerDetailsNumber("65", constants.CUSTOMER_CACHE_TEST_NUMBER, true, function (err, cache) {
				if(err){
					callback(null, {code: -1, item: 'EC customer cache', latency: new Date().getTime() - latency, error: err, errorCode: 1});
				}else{
					callback(null, {code: 0, item: 'EC customer cache', latency: new Date().getTime() - latency, error: null, errorCode: 0});
				}
			});
		}, function(callback){
			var latency = new Date().getTime();
			ec.getCustomerDetailsNumber("65", constants.CUSTOMER_CACHE_TEST_NUMBER, true, function (err, cache) {
				if(err){
					callback(null, {code: -1, item: 'EC Revcovery customer cache', latency: new Date().getTime() - latency, error: err, errorCode: 1});
				}else{
					callback(null, {code: 0, item: 'EC Revcovery customer cache', latency: new Date().getTime() - latency, error: null, errorCode: 0});
				}
			}, true);
		}, function(callback){
			var latency = new Date().getTime();
			db.query_noerr("show databases", function(rows) {
				if(!rows){
					callback(null, {code: -1, item: 'Maria DB', latency: new Date().getTime() - latency, error: err, errorCode: 1});
				}else{
					callback(null, {code: 0, item: 'Maria DB', latency: new Date().getTime() - latency, error: null, errorCode: 0});
				}
			});
		}, function(callback){
			var latency = new Date().getTime();
			db.oracle.query("SELECT TABLESPACE_NAME FROM USER_TABLESPACES", function(err, result){
				if(err){
					callback(null, {code: -1, item: 'EC Database', latency: new Date().getTime() - latency, error: err, errorCode: 1});
				}else{
					callback(null, {code: 0, item: 'EC Database', latency: new Date().getTime() - latency, error: null, errorCode: 0});
				}
			});
		}, function(callback){
			var latency = new Date().getTime();
			db.testCollection.find({}).toArray(function(err, result){
				if(err){
					callback(null, {code: -1, item: 'Mongo DB', latency: new Date().getTime() - latency, error: err, errorCode: 1});
				}else{
					callback(null, {code: 0, item: 'Mongo DB', latency: new Date().getTime() - latency, error: null, errorCode: 0});
				}
			});
		}, function(callback){
			var latency = new Date().getTime();
			request({
				uri: 'https://api.circles.life/ping',
				method: 'GET',
				timeout: 20000
			}, function (err, response, body) {
				if(err || common.safeParse(body).status != 0){
					callback(null, {code: -1, item: 'Ecomm', latency: new Date().getTime() - latency, error: err, errorCode: 1});
				}else{
					callback(null, {code: 0, item: 'Ecomm', latency: new Date().getTime() - latency, error: null, errorCode: 0});
				}
			});
		}, function(callback){
			generateHealthStatus(function(err, results){
				if(err){
					callback(null, {code: -1, item: 'Pm2 Details', error: err});
				}else{
					callback(null, {code: 0, item: 'Pm2 Details', error: null, data: results});
				}
			});
		}],
		function(err, results){
            finalCallback({"ok":1, results: results});
		}
	); 
}

function generateHealthStatus(callback){
	extractImportantParamsFromPm2(function(err, results){
		if(err){
			callback(err, null);
		}else{
			healthOutput = [];
			healthOutputMap = {};
			results.forEach(function(item){
				if(!healthOutputMap[item.name]){
					healthOutputMap[item.name] = {name: item.name};
					healthOutputMap[item.name].numOfProcesses = 1;
					healthOutputMap[item.name].avgCpu = item.cpu;
					healthOutputMap[item.name].avgMemory = item.memory;
					healthOutputMap[item.name].numOfProcessesDead = (item.status == "online") ? 0 : 1;
				}else{
					healthOutputMap[item.name].numOfProcesses++;
					healthOutputMap[item.name].avgCpu += item.cpu;
					healthOutputMap[item.name].avgMemory += item.memory;
					if(item.status != "online"){
						healthOutputMap[item.name].numOfProcessesDead++;
					}
				}
			});
			for(var key in healthOutputMap){
				var singleEntry = healthOutputMap[key];
				singleEntry.avgCpu = singleEntry.avgCpu/(singleEntry.numOfProcesses - singleEntry.numOfProcessesDead);
				singleEntry.avgMemory = singleEntry.avgMemory/(singleEntry.numOfProcesses - singleEntry.numOfProcessesDead);
				if(thresholdsList[key] && singleEntry.avgCpu > thresholdsList[key].maxcpu){
					singleEntry.loadCode = 1;
				} else if(thresholdsList[key] && singleEntry.avgMemory > thresholdsList[key].maxmem){
					singleEntry.loadCode = 2;
				} else{
					singleEntry.loadCode = 0;
				}
				healthOutput.push(singleEntry);
			}
			callback(null, healthOutput);
		}
	});
}


function extractImportantParamsFromPm2(callback){
	getPm2List(function(err, result){
		if(err){
			callback(err, null);
		}else{
			var importantParamItems = [];
			result.forEach(function(item){
				if(constants.PM2_BLACKLIST_MODULES.indexOf(item.name) == -1){
					let entry = {};
					entry.pid = item.pid;
					entry.name = item.name;
					entry.status = item.pm2_env.status;
					entry.uptime = item.pm2_env.pm_uptime;
					entry.cpu = item.monit.cpu;
					entry.memory = (item.monit.memory/(1024*1024)).toFixed(2);
					importantParamItems.push(entry);
				}
			});
			callback(null, importantParamItems);
		}
	});
}

function getPm2List(callback){
	exec("pm2 jlist", { "timeout" : 10000 }, function(err, results){
		if(err){
			callback(err, null);
		}else{
			callback(null, common.safeParse(results));
		}
	});
}

function checkBlocking(type, threshold) {
	return blocked(function(ms) {
		slack.send({
			"username" : "Lag Bot (" + type + ") " + config.MYFQDN,
			"text" : "Event Loop Lag [" + process.pid + "] - " + ms + " ms"
		});
	}, { "threshold" : threshold });
}

function handleSchedulerUncaughtException(err, items){
	var abandonedItems = {
		"style" : { "table" : "border-collapse:collapse",
			"th" : "border:1px solid black",
			"td" : "border:1px solid black" },
		"head" : [ "Start", "Action","Activity"], 
		"body" : [ ] 
	};
	if(items.length > 0){
		items.forEach(function(item){
			abandonedItems.body.push([new Date(item.start).toString().substr(0, 24), item.action, item.activity]);
		});
	}
	sendCrashAlert("crash_alert_common", "Scheduler", err.stack, "Abandoned Items", abandonedItems);
}

function sendFailureAlert(alertActivityId, type, infoTitle, items){
	var notification = {
		activity: alertActivityId,
		type: type,
		teamID: 5,
		teamName: "Operations",
		infoTitle: infoTitle,
		items: items,
		myfqn: config.MYFQDN
	};
	notificationSend.deliver(notification, null, function (err, result) {
		if (err) {
			common.error(`Failure Alert failed ${alertActivityId}`, err);
		}
	});
}

function sendCrashAlert(alertActivityId, type, stackTrace, infoTitle, items){
	var notification = {
		activity: alertActivityId,
		type: type,
		teamID: 5,
		teamName: "Operations",
		stackTrace: stackTrace,
		infoTitle: infoTitle,
		items: items,
		myfqn: config.MYFQDN
	};
	notificationSend.deliver(notification, null, function (err, result) {
		if (err) {
			common.error(`Failure Alert failed ${alertActivityId}`, err);
		}
	});
}

function sendAlertWithTable(items, process, topic){
	var templateItems = {
		"style" : { "table" : "border-collapse:collapse",
			"th" : "border:1px solid black",
			"td" : "border:1px solid black" },
		"head" : [ "Start", "Action", "Activity", "Service Instance Number", "Number", "Repeat"], 
		"body" : items
	};
	sendFailureAlert("failure_alert_common", process, topic, templateItems);
}

function handleSchedulerUnhandledRejection(err, items){

}
