var config = require('../../../../config');
var common = require('../../../../lib/common');
var db = require('../../../../lib/db_handler');

exports.isProduction = isProduction;
exports.allowed = allowed;
exports.getNodejsServers = getNodejsServers;
var hostNameToIp = {
    "KIRK" : config.KIRK,
    "RIKER" : config.RIKER,
    "PICARD" : config.PICARD,
    "JANEWAY" : config.JANEWAY,
    "RIKER" : config.RIKER,
    "CHEKOV" : config.CHEKOV,
    "WORF" : config.WORF,
    "WESLEY" : config.WESLEY,
    "LWEPC01" : config.LWEPC01,
    "LWEPC02" : config.LWEPC02,
    "LWBSS01" : config.LWBSS01,
    "LWBSS02" : config.LWBSS02,
    "LWBSS03" : config.LWBSS03,
    "LWBSS04" : config.LWBSS04,
    "LWTES01" : config.LWTES01,
    "BYPASS" : config.BYPASS,
    "TRAFFIC_EPC01" : config.TRAFFIC_EPC01,
    "TRAFFIC_EPC02" : config.TRAFFIC_EPC02,
    "TRAFFIC_TES01" : config.TRAFFIC_TES01,
    "TRAFFIC_BYPASS" : config.TRAFFIC_BYPASS
};


function allowed(ip, hostNames){
    for(var i = 0; i < hostNames.length; i++){
        if(hostNameToIp[hostNames[i]] == ip){
            return true;
        }
    }
    return false;
}

function isProduction(host) {
    var staging = [ "RIKER", "CHEKOV" ];
    if (staging.indexOf(host.toUpperCase()) == -1) return true;
    else return false;
}

function getNodejsServers(){
    if(config.DEV){
        return [
            {"name": "riker", "ip": config.RIKER ? config.RIKER : "10.20.1.38", "webport": config.WEBPORT, "websport": config.WEBPORT, "mobileport": config.MOBILEPORT, "mobilesport": config.MOBILESPORT},
            {"name": "chekov", "ip": config.CHEKOV ? config.CHEKOV : "10.20.1.48", "webport": config.WEBPORT, "websport": config.WEBSPORT, "mobileport": config.MOBILEPORT, "mobilesport": config.MOBILESPORT}
        ];
    } else {
        return [
            {"name": "kirk", "ip": config.KIRK ? config.KIRK : "10.20.1.37", "webport": config.WEBPORT, "websport": config.WEBSPORT, "mobileport": "6080", "mobilesport": "6443"},
            {"name": "picard", "ip": config.PICARD ? config.PICARD : "10.20.1.47", "webport": config.WEBPORT, "websport": config.WEBSPORT, "mobileport": "6080", "mobilesport": "6443"},
            {"name": "worf", "ip": config.WORF ? config.WORF : "10.20.1.19", "webport": config.WEBPORT, "websport": config.WEBSPORT, "mobileport": "6080", "mobilesport": "6443"},
            {"name": "wesley", "ip": config.WESLEY ? config.WESLEY : "10.20.1.29", "webport": config.WEBPORT, "websport": config.WEBSPORT, "mobileport": "6080", "mobilesport": "6443"},
            {"name": "janeway", "ip": config.JANEWAY ? config.JANEWAY : "10.20.1.57", "webport": config.WEBPORT, "websport": config.WEBSPORT, "mobileport": "6080", "mobilesport": "6443"}
        ];
    }
}
