var db = require('../../../../lib/db_handler');
//Install packages
//var mongodb = require('mongodb').MongoClient;
var zendesk = require('node-zendesk');
var config = require('../../../../config');
var async = require('async');
var constants = require('../../constants');
var common = require('../../../../lib/common');

	//Open a client
var client = zendesk.createClient({
  username:  config.authZendesk ? config.authZendesk.username : "",
  token:     config.authZendesk ? config.authZendesk.token : "",
  remoteUri: config.authZendesk ? config.authZendesk.remoteUri : ""
});

var count = 1000;
var number = 0;
//var startTime = 1502330000
//var startTime = configZendesk.time.start_time;

exports.getZendeskTickets = getZendeskTickets;

function getZendeskTickets(startTime, callback){
    getZendeskPage(startTime, 0, callback);
}


function getZendeskPage(startTime, callcount, callback){
    if(callcount==constants.MAX_PAGE_ZENDESK){
        callback(null, {"message": "max call count exceed"});
    }else{
        client.tickets.export(startTime, function (err, req, res) {
            if (err) {
                if(err.retryAfter){
                    setTimeout(function(){
                        getZendeskPage(startTime, callcount, callback);
                    }, 1000*(parseInt(err.retryAfter)+2));
                }else{
                    callcount++;
                    common.error("ZENDESK_ERROR", err);
                    getZendeskPage(startTime, callcount, callback);
                }
            }else if(res.results.length > 0){
                var count = res.results.length;
                console.log(count);
                tickets = res.results;
                newStartTime = res.results[count-1].generated_timestamp;
                saveZenTicketsAsync(tickets, function(err){
                    callcount++;
                    if(err){
                        common.error("ZENDESK_ERROR", err);
                        getZendeskPage(startTime, callcount, callback);
                    }else{
                        getZendeskPage(newStartTime, callcount, callback);
                    }
                });
            }
        });
    }
}

function saveZenTicketsAsync(tickets, callback2){
    ticketSavingErrors = [];
    async.forEach(tickets, function(ticket, cb){
        var formattedTicket = formatTicket(ticket);
        if(formattedTicket){
            db.zendeskTickets.update({id : ticket.id}, formattedTicket, {upsert: true}, function(err, res){
                if(err){
                    db.weblog.insert({
                        "ts" : new Date().getTime(),
                        "type" : "zendesk",
                        "id" :  ticket.id,
                        "action" : "saveZendeskTicket",
                        "username" : "scheduler",
                        "err": JSON.stringify(err)
                    });
                }
                cb();   
            });
        }else{
            cb();
        }
    }, function(){
        callback2();
    });
}

function formatTicket(ticket){
    try{
        ticket.first_resolution_time_in_minutes = parseInt(ticket.first_resolution_time_in_minutes);
        ticket.first_reply_time_in_minutes = parseInt(ticket.first_reply_time_in_minutes);
        ticket.first_reply_time_in_minutes_within_business_hours = parseInt(ticket.first_reply_time_in_minutes_within_business_hours);
        ticket.first_resolution_time_in_minutes_within_business_hours = parseInt(ticket.first_resolution_time_in_minutes_within_business_hours);
        ticket.full_resolution_time_in_minutes = parseInt(ticket.full_resolution_time_in_minutes);
        ticket.full_resolution_time_in_minutes_within_business_hours = parseInt(ticket.full_resolution_time_in_minutes_within_business_hours);
        ticket.agent_wait_time_in_minutes = parseInt(ticket.agent_wait_time_in_minutes);
        ticket.agent_wait_time_in_minutes_within_business_hours = parseInt(ticket.agent_wait_time_in_minutes_within_business_hours);
        ticket.reopens = parseInt(ticket.reopens);
        ticket.replies = parseInt(ticket.replies);
        ticket.requester_wait_time_in_minutes = parseInt(ticket.requester_wait_time_in_minutes);
        ticket.requester_wait_time_in_minutes_within_business_hours = parseInt(ticket.requester_wait_time_in_minutes_within_business_hours);
        ticket.on_hold_time_in_minutes = parseInt(ticket.on_hold_time_in_minutes);
        ticket.on_hold_time_in_minutes_within_business_hours = parseInt(ticket.on_hold_time_in_minutes_within_business_hours);
        ticket.updated_at = new Date(ticket.updated_at).getTime() + 8*3600000;
        ticket.created_at = new Date(ticket.created_at).getTime() + 8*3600000;
        ticket.assigned_at = new Date(ticket.assigned_at).getTime() + 8*3600000;
        ticket.initially_assigned_at = new Date(ticket.initially_assigned_at).getTime() + 8*3600000;
        ticket.field_29116437 = parseInt(ticket.field_29116437);
        ticket.field_29116427 = parseInt(ticket.field_29116427);
        ticket.current_tags = ticket.current_tags.split(' ');
    }catch(e){
        return null;
    }
    return ticket;                          
}