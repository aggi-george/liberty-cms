var db = require('../../../../lib/db_handler');
var ec = require('../../../../lib/elitecore');
var config = require('./../../../../config');
var accountManager = require('../../manager/account/accountManager');
const common = require('../../../../lib/common');
let retryOneTime = true;
exports.release = function (tEvent, callback) {
    ec.doInventoryStatusOperation({
        inventoryNumber: tEvent.number,
        operationAlias: "FREE"
    }, function (err, result) {
        if (!err && result) {
            db.scheduler.update({"_id": tEvent._id}, {"$set": {"processed": result, "color": "green", "myfqdn" : config.MYFQDN}});
            callback(undefined, {"id": tEvent._id, "processed": result});
        } else {
            db.scheduler.update({"_id": tEvent._id}, {"$set": {"processed": err, "color": "red", "myfqdn" : config.MYFQDN}});
            callback(undefined, {"id": tEvent._id, "processed": err});
        }
    });
}

exports.changeAccount = function (tEvent, callback) {
    accountManager.handleSchedulerEvent(tEvent, config.OPERATIONS_KEY, function(err, result) {
        if (err && err.status && err.status === 'TERMINATE_ERROR') {
            var errorObj = {"error": err.message, result: result};
            terminationCheck(tEvent);
            return callback(undefined, errorObj);
        }else if(err && (err.status === 'INVALID_SIN' || err.status === 'ERROR_CUSTOMER_NOT_FOUND')) {
            var errorObj = {"error": err.message, result: result};
            updateScheduler(tEvent._id, "green", errorObj);
            
            return callback(undefined, errorObj);
        }else if (err) {
            var errorObj = {"error": err.message, result: result};
            updateScheduler(tEvent._id, "red", errorObj);
            return callback(undefined, errorObj);
        }
        updateScheduler(tEvent._id, "green", {});
        return callback(undefined, {"id": tEvent._id, "processed": result});
    });
}


function terminationCheck(tEvent) {
    const prefix = (tEvent.prefix) ? tEvent.prefix : "65";
    const sin = tEvent.serviceInstanceNumber;

    if (sin) {
        ec.getCustomerDetailsServiceInstance(sin, false, (err, cache) => {
            if (err) {
                updateScheduler(tEvent._id, "green", result);
            }else {
                ec.getAccountNumber(prefix, cache.number, (err, result) => {
                    if (err) {
                        const errorObj = {"error": err.message, result: result};
                        updateScheduler(tEvent._id, "green", errorObj);
                    }else {
                        if (retryOneTime) {
                            const key = "terminate-pending";
                            const terminateCache = "cache";
                            const ongoing = { "result" : "ongoing", "error" : null }
                            updateScheduler(tEvent._id, "maroon", ongoing);
                            db.cache_get(terminateCache, key, (value) => {
                                if (value && value.length) {
                                    value.push(tEvent);
                                    db.cache_put(terminateCache, key, value, 3600);
                                }else {
                                    db.cache_put(terminateCache, key, [tEvent], 3600);
                                }
                            });
                            retryOneTime = false;
                        }else {
                            const errorObj = {"error": 'Failed even after retry', result: null}
                            updateScheduler(tEvent._id, "red", errorObj);
                        }
                    }
                });
            }
        });
    }else {
        common.log('terminationCheck: SIN not available in tEvent')
    }
}

function updateScheduler(id, color, result) {
    db.scheduler.update(
        {
            "_id": db.objectID(id)
        }, 
        {
            "$set": {
                "processed": result, 
                "color": color, 
                "myfqdn" : config.MYFQDN
            }
        }
    );
}
