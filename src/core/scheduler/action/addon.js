const async = require('async');
const configWrapper = require('../../manager/config/configManager');
const bonusManager = require('../../manager/bonus/bonusManager');
const hlrManager = require('../../manager/hlr/hlrManager');
const config = require('../../../../config');
const db = require('../../../../lib/db_handler');
const ec = require('../../../../lib/elitecore');
const common = require('../../../../lib/common');
const dateTime = require('../../../../src/utils/dateTime');

const LOG = config.LOGSENABLED;
const SG_PREFIX = "65";

module.exports = {

    addon: function (tEvent, callback) {
        var prefix = (tEvent.prefix) ? tEvent.prefix : SG_PREFIX;
        var handleResult = function(err, result) {
            var error = (err && (typeof(err) == "object")) ? err.message :
                err ? err : undefined;
            if (tEvent && tEvent._id) {
                var color = err ? "red" : "green";
                db.scheduler.update({ "_id" : tEvent._id }, {
                    "$set" : { "processed" : result, "error" : error, "color" : color, "myfqdn" : config.MYFQDN }
                });
            }
            callback(undefined, { "id" : tEvent._id, "error" : error, "processed" : result });
        };
        if ( parseInt(tEvent.start) != tEvent.start ) return handleResult(new Error("Invalid Start Date"));
        if ( tEvent.activity == "birthday_bonus" ) {
            handleBirthday(tEvent.start, handleResult);
        } else if ( tEvent.activity == "loyalty_bonus" ) {
            handleLoyalty(tEvent.start, handleResult);
        } else if ( tEvent.activity == 'retention_bonus_3_months' ) {
            handleThreeMonthRetentionBonus(tEvent.start, handleResult);
        } else if ( tEvent.activity == "roaming_off" ) {
            hlrManager.set(hlrManager.roamingOff, SG_PREFIX, tEvent.number, true, handleResult);
        } else {
            handleResult(new Error("Unknown Addon Activity"));
        }
    }
}

function getSGDate(ts) {
    var d = new Date(ts);
    var d1 = d.getHours() >= 16 ? d.getDate() + 1 : d.getDate();
    return common.getDate(new Date(d.getFullYear(), d.getMonth(), d1));
}

function handleLoyalty(start, callback) {
    var currentDate = getSGDate(start);
    var obj = { "action" : "addon", "activity" : "loyalty_bonus", "error" : [], "result" : [] };
    var query = "SELECT t.serviceinstanceaccount, TIMESTAMPDIFF(MONTH, t.sg1, DATE(" + db.escape(currentDate) + ")) months FROM (" +
        " SELECT s.serviceinstanceaccount, s.sg1, MONTH(s.sg1) m1, DAY(s.sg1) d1, MONTH(s.sg2) m2, DAY(s.sg2) d2 FROM (" +
        " SELECT * FROM (SELECT SUM(IF(n.numberstatus='Active',1,0)) numberstatus, a.serviceinstanceaccount, " +
        " a.joindate sg1, DATE_ADD(a.joindate, INTERVAL 6 MONTH) sg2 " +
        " FROM ecAccounts a LEFT JOIN ecNetworkServiceInstance n ON a.serviceinstanceaccount=n.serviceinstanceaccount WHERE " +
        " a.accountstatus='Active' AND a.joindate IS NOT NULL GROUP BY a.serviceinstanceaccount) r WHERE r.numberstatus > 0) s WHERE " +
        " DATE(s.sg1)<DATE(" + db.escape(currentDate) + ")) t WHERE " +
        " (t.m1=MONTH(" + db.escape(currentDate) + ") AND t.d1=DAY(" + db.escape(currentDate) + ")) OR " +
        " (t.m2=MONTH(" + db.escape(currentDate) + ") AND t.d2=DAY(" + db.escape(currentDate) + "))";
    if (LOG) common.log("handleLoyalty", query);
    
    db.query_err(query, function(err, rows) {
        if (err) return callback(err);
        var loyaltyQ = async.queue(function(task, cb) {
            bonusManager.addLoyaltyBonusSI(task.serviceInstanceNumber, task.milestoneMonth, config.OPERATIONS_KEY, function(e,r) {
                if (e) obj.error.push({ "task" : task, "error" : e.message });
                if (r) obj.result.push({ "task" : task, "result" : r });
                cb();
            });
        }, 1);
        if (rows && rows.length>0) {
            rows.forEach(function(item) {
                loyaltyQ.push({ "serviceInstanceNumber" : item.serviceinstanceaccount, "milestoneMonth" : item.months }); 
            });
            loyaltyQ.drain = function() {
                callback(undefined, obj);
            };
        } else {
            callback(undefined, obj);
        }
    });
}

function handleThreeMonthRetentionBonus(start, callback) {
    let currentDate = dateTime.getSGTime(new Date(start), config.COUNTRY_ISOCODE);
    let getQueryDate = (ts) => {
        let d = new Date(ts);
        return common.getDate(dateTime.getSGTime(new Date(d.getFullYear(), d.getMonth() - 3, d.getDate())), config.COUNTRY_ISOCODE);
    };
    let queryDate = getQueryDate(start);
    let obj = { 'action' : 'addon', 'activity' : 'retention_bonus_3_months', 'error' : [], 'result' : [] };
    let query = `SELECT * FROM (SELECT SUM(IF(n.numberstatus='Active',1,0)) numberstatus, a.serviceinstanceaccount, a.joindate, 
        TIMESTAMPDIFF(MONTH, a.joindate, DATE(${db.escape(currentDate)})) months FROM 
        ecAccounts a LEFT JOIN ecNetworkServiceInstance n ON a.serviceinstanceaccount=n.serviceinstanceaccount WHERE 
        YEAR(a.joindate)=YEAR(${db.escape(queryDate)}) AND 
        MONTH(a.joindate)=MONTH(${db.escape(queryDate)}) AND 
        DAY(a.joindate)=DAY(${db.escape(queryDate)}) AND a.accountstatus='Active' 
        GROUP BY a.serviceinstanceaccount) r WHERE r.numberstatus > 0`;
    if (LOG) common.log('threeMonthCustomer', query);

    db.query_err(query, (err, rows) => {
        if (err) return callback(err);
        let threeMonthsRetentionQ = async.queue((task, cb) => {
            let options = {
                continueRecurrent: false,
                continueMonth: 3
            }
            bonusManager.addRetentionBonusSI(task.serviceInstanceNumber, task.milestoneMonth, options,
                config.OPERATIONS_KEY, (e,r) => {
                if (e) obj.error.push({ 'task' : task, 'error' : e.message });
                if (r) obj.result.push({ 'task' : task, 'result' : r });
                return cb();
            });
        }, 1);
        if (rows && rows.length>0) {
            rows.forEach((item) => {
                threeMonthsRetentionQ.push({ 'serviceInstanceNumber' : item.serviceinstanceaccount, 'milestoneMonth' : item.months }); 
            });
            threeMonthsRetentionQ.drain = () => {
                return callback(undefined, obj);
            };
        } else {
            return callback(undefined, obj);
        }
    });
}

function handleBirthday(start, callback) {
    var currentDate = getSGDate(start);
    var obj = { "action" : "addon", "activity" : "birthday_bonus", "error" : [], "result" : [] };
    var query = "SELECT r.serviceinstanceaccount FROM (SELECT SUM(IF(n.numberstatus='Active',1,0)) numberstatus, a.serviceinstanceaccount FROM " +
        " ecAccounts a LEFT JOIN ecNetworkServiceInstance n ON a.serviceinstanceaccount=n.serviceinstanceaccount WHERE " +
        " MONTH(a.birthday)=MONTH(" + db.escape(currentDate) + ")" +
        " AND DAY(a.birthday)=DAY(" + db.escape(currentDate) + ") AND a.accountstatus='Active' AND a.activationdate IS NOT NULL " +
        " GROUP BY a.serviceinstanceaccount) r WHERE r.numberstatus > 0";
    if (LOG) common.log("handleBirthday", query);
    db.query_err(query, function(err, rows) {
        if (err) return callback(err);
        var birthdayQ = async.queue(function(task, cb) {
            bonusManager.addBirthdayBonusSI(task.serviceInstanceNumber, config.OPERATIONS_KEY, function(e,r) {
                if (e) obj.error.push({ "task" : task, "error" : e.message });
                if (r) obj.result.push({ "task" : task, "result" : r });
                cb();
            });
        }, 1);
        if (rows && rows.length>0) {
            rows.forEach(function(item) {
                birthdayQ.push({ "serviceInstanceNumber" : item.serviceinstanceaccount }); 
            });
            birthdayQ.drain = function() {
                callback(undefined, obj);
            };
        } else {
            callback(undefined, obj);
        }
    });
}
