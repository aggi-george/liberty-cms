var async = require('async');
var config = require('../../../../config');
var ec = require('../../../../lib/elitecore');
var db = require('../../../../lib/db_handler');
var common = require('../../../../lib/common');
var notificationSend = require('../../manager/notifications/send');
var bonusAggregate = require('../../../../tests/bonus_aggregate');
var sm = require('../../manager/bonus/subscriptionManager');
var classManager = require('../../manager/analytics/classManager');

var SG_PREFIX = "65";

exports.detail = function (tEvent, callback) {
    var activity = tEvent.activity;
    switch(activity) {
        case "plan_change_fix":
            planChangeFix(tEvent, callback);
            break;
        case "bonus_anomaly_report":
            bonusAnomalyReport(tEvent, callback);
            break;
        case "classify_customers":
            classifyCustomers(tEvent, callback);
            break;
        default:
    }
}

function planChangeFix(tEvent, callback) {
    var getEx = function (cbEx) {    //breakup because this query can get very heavy
        var findEx = { "serviceInstanceNumber" : { "$exists" : true, "$ne" : null },
                "activity" : { "$in" : [ "validate_base_plan", "termination_effective", "portoutsuccess" ] } };
        var projEx = { "_id" : 0, "serviceInstanceNumber" : 1 };
        db.notifications_webhook.find(findEx, projEx).toArray(function (errEx,resEx) {
            var list = (resEx) ? resEx.map(function(a) { return a.serviceInstanceNumber }) : [];
            cbEx(errEx, list);
        });
        };
    var getIn = function (cbIn) {
        getEx(function(errList, resList) {
            if (resList.length == 0) return cbIn(new Error("Empty List"));
            var aggIn = [
                { "$match" : { "serviceInstanceNumber" : { "$not" : { "$in" : resList }, "$ne" : null },
                    "activity" : { "$in" : [
                        "delivery_success",
                        "plus_added_immediate",
                        "plus_removed_immediate",
                        "selfcare_install" ] } } },
                { "$sort" : { "serviceInstanceNumber" : 1 } },
                { "$group" : { "_id" : "$serviceInstanceNumber" } }
            ];
            db.notifications_webhook.aggregate(aggIn, function(errIn, resIn) {
                cbIn(errIn, resIn);
            });
        });
    };
    getIn(function(numberErr, numberList) {
        if (numberErr && !numberList) {
            db.scheduler.update({ "_id" : tEvent._id },
                { "$set" : { "processed" : { "error" : numberErr }, "color" : "red", "myfqdn" : config.MYFQDN } });
            return callback(undefined, { "id" : tEvent._id, "processed" : numberErr });
        }
        var tasks = new Array;
        numberList.forEach(function (item) {
            elitecoreGet(tasks, item._id);
        });
        async.parallelLimit(tasks, 10, function (err, result) {
            var validate = new Array;
            result.filter(function(item) {
                if (item.serviceInstanceInventories) return item;
                else return undefined;
            }).forEach(function(item) {
                validate.push(function (cb) {
                    notificationSend.internal(SG_PREFIX, item.number, undefined,
                            undefined, "validate_base_plan", undefined, undefined, function(e, r) {
                        cb(undefined, r);
                    });
                });
            });
            async.series(validate, function (err, result) {
                db.scheduler.update({ "_id" : tEvent._id },
                    { "$set" : { "processed" : result, "color" : "green", "myfqdn" : config.MYFQDN } });
                callback(undefined, { "id" : tEvent._id, "processed" : result });
            });
        });
    });
}

function elitecoreGet(tasks, service_instance_number) {
    tasks.push(function (callback) {
        ec.getCustomerDetailsServiceInstance(service_instance_number, false, function (err, result) {
            if (!err && result) callback(undefined, result);
            else callback(undefined, { "serviceInstanceNumber" : service_instance_number });
        });
    });
}

function bonusAnomalyReport(tEvent, callback) {
    var higher = { "style" : { "table" : "border-collapse:collapse",
                "th" : "border:1px solid black",
                "td" : "border:1px solid black" },
            "head" : [ "Service Instance Number", "Mobile Number", "Result" ],
            "body" : [ ] };
    var error = { "style" : { "table" : "border-collapse:collapse",
                "th" : "border:1px solid black",
                "td" : "border:1px solid black" },
            "head" : [ "Service Instance Number", "Mobile Number", "Result" ],
            "body" : [ ] };
    bonusAggregate.run(function (err, result) {
        if (!err && result) {
            result.sort(function (a, b) {
                if (a.errorCache < b.errorCache) return -1;
                else if (a.errorUsage < b.errorUsage) return -1;
                else if (a.diff < b.diff) return -1;
                else return 1;
            });
            var tasks = new Array;
            result.forEach(function (item) {
                var number = (item && item.number) ? item.number : "";
                if (item && item.errorUsage) {
                    error.body.push([ item.serviceInstanceNumber, number, "", "",
                            JSON.stringify(item.errorUsage), item.status ]);
                } else if (item && item.tester) {
                } else if (item.totalPretty != item.historyPretty) {
                    if ( item && (item.total == 0 || (item.total && item.diff && (item.diff > 1))) ) {
                        if ( item.status == "Active" ) tasks.push(function (cb) {
                            sm.updateCurrentBillBonuses(SG_PREFIX, number,
                                "[CMS][scheduler]", undefined, function (bErr, bResult) {
                                cb(undefined, { "serviceInstanceNumber" : item.serviceInstanceNumber,
                                        "number" : item.number, "result" : (bErr ? bErr : bResult)
                                });
                            });
                        });
                    }
                }
            });
            async.series(tasks, function (aErr, aResult) {
                if (aResult) aResult.forEach(function (item) {
                    higher.body.push([
                        item.serviceInstanceNumber,
                        item.number,
                        (item.result && item.result.message ? item.result.message : item.result.status ? item.result.status : JSON.stringify(item.result))
                    ]);
                });
                var variables = {
                    "reportSubject" : "Undersubscribed Autorepair",
                    "labelA" : "Autorepaired",
                    "tableA" : higher,
                    "labelB" : "",
                    "tableB" : "",
                    "labelC" : "Errors",
                    "tableC" : error };
                if (higher.body.length == 0) {
                    variables.labelA = "";
                    variables.tableA = "";
                }
                if (error.body.length == 0) {
                    variables.labelC = "";
                    variables.tableC = "";
                }
                if ( (higher.body.length == 0) &&
                        (error.body.length == 0) ) {
                    db.scheduler.update({ "_id" : tEvent._id },
                        { "$set" : { "processed" : result, "color" : "green", "myfqdn" : config.MYFQDN } });
                    callback(undefined, { "id" : tEvent._id, "processed" : result });
                } else {
                    notificationSend.internal(undefined, undefined, undefined, undefined,
                            "generic_report", variables, undefined, function(err, result) {
                        db.scheduler.update({ "_id" : tEvent._id },
                            { "$set" : { "processed" : result, "color" : "green", "myfqdn" : config.MYFQDN } });
                        callback(undefined, { "id" : tEvent._id, "processed" : result });
                    });
                }
            });
        } else {
            db.scheduler.update({ "_id" : tEvent._id },
                { "$set" : { "processed" : { "error" : err, "result" : result }, "color" : "red", "myfqdn" : config.MYFQDN} });
            callback(undefined, { "id" : tEvent._id, "processed" : { "error" : err, "result" : result } });
        }
    });
}

function classifyCustomers(tEvent, callback) {
    classManager.classifyCustomers(function(err) {
        if (err) {
            db.scheduler.update({ "_id" : tEvent._id },
                { "$set" : { "processed" : { "error" : err }, "color" : "red", "myfqdn" : config.MYFQDN} });
        } else {
            db.scheduler.update({ "_id" : tEvent._id },
                { "$set" : { "processed" : { "error" : err }, "color" : "green", "myfqdn" : config.MYFQDN} });
        }
        callback(undefined, { "id" : tEvent._id, "processed" : { "error" : err } });
    });
}
