var db = require('../../../../lib/db_handler');
var bi = require('../../../api/web/bi');
var config = require('../../../../config');


exports.executeBiRun = function (tEvent, callback) {
    bi.generateKpiValuesFromSchedulerEvent(tEvent, function(err, result) {
        if (err) {
            var errorObj = {"error": err.message, result: result};
            db.scheduler.update({"_id": tEvent._id}, {"$set": {"processed": errorObj, "color": "red", "myfqdn" : config.MYFQDN}});
            callback(undefined, errorObj);
        } else {
            db.scheduler.update({"_id": tEvent._id}, {"$set": {"processed": result, "color": "green", "myfqdn" : config.MYFQDN}});
            callback(undefined, {"id": tEvent._id, "processed": result});
        }
    });
}

exports.crashTest = function (tEvent, callback) {
    bi.crashTestErroredMethod(tEvent, function(err, results){
        db.scheduler.update({"_id": tEvent._id}, {"$set": {"processed": results, "color": "green", "myfqdn" : config.MYFQDN}});
        if(err){
            callback(null, true);
        }else{
            callback(null, false);
        }
    });
}


exports.replicateTest = function (tEvent, callback) {
    setTimeout(function(){
        db.scheduler.update({"_id": tEvent._id}, {"$set": {"processed": {"status": "OK"}, "color": "green", "myfqdn" : config.MYFQDN}});
        callback(null, true);
    }, 3000);
}