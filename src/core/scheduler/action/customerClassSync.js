const db = require('../../../../lib/db_handler');
const syncQueueService = require(`${global.__queue}/sync`);
const config = require('../../../../config');


class customerClassSync{
    static syncCustomerClasses(tEvent, callback){
        if(new Date().getDate() < 3){
            db.scheduler.update({"_id": tEvent._id}, {"$set": {"processed": {"message": "skipped due to bill run"}, "color": "green", "myfqdn" : config.MYFQDN}});
            callback(undefined, {"id": tEvent._id, "processed": {"message": "skipped due to bill run"}});
        }else{
            syncQueueService.run("SYNC_CUSTOMER_CLASSES", {}, {
                errorIfRunning: true,
                initiator: "scheduler"
            }, (err, result) => {
                if (err) {
                    var errorObj = {"error": err, result: result};
                    db.scheduler.update({"_id": tEvent._id}, {"$set": {"processed": errorObj, "color": "red", "myfqdn" : config.MYFQDN}});
                    callback(undefined, errorObj);
                } else {
                    db.scheduler.update({"_id": tEvent._id}, {"$set": {"processed": result, "color": "green", "myfqdn" : config.MYFQDN}});
                    callback(undefined, {"id": tEvent._id, "processed": result});
                }
            });
        }
    }
}

module.exports = customerClassSync;
