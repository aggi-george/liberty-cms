const async = require('async');
const config = require(global.__base + '/config');
const notificationSend = require(global.__core + '/manager/notifications/send');
const db = require(global.__lib + '/db_handler');
const common = require(global.__lib + '/common');
const ec = require(global.__lib + '/elitecore');
const elitecoreUsageService = require(global.__bss + '/elitecore/elitecoreUsageService');
const configManager = require(`${global.__core}/manager/config/configManager`);
const dateTime = require(`${global.__base}/src/utils/dateTime`);
const LOG = config.LOGSENABLED;

class Notifications {
    static detail(tEvent, callback) {
        const handleResult = (err, result) => {
            const error = (err && (typeof(err) == 'object')) ? err.message :
                err ? err : undefined;
            if (tEvent && tEvent._id) {
                const color = err ? 'red' : 'green';
                db.scheduler.update({ _id: tEvent._id },
                    { '$set': { processed: result, error, color, myfqdn: config.MYFQDN } });
            }
            callback(undefined, { id: tEvent._id, error, processed: result });
        };
        if ( parseInt(tEvent.start) != tEvent.start ) return handleResult(new Error('Invalid Start Date'));
        if (tEvent.activity == 'hourly_aggregate') aggregate(tEvent.start, handleResult);
        else if (tEvent.activity == 'bonus_loyalty_countdown') bonusLoyaltyCountdown(tEvent.start, handleResult);
        else if (tEvent.activity == 'three_month_customer') threeMonthCustomer(tEvent.start, handleResult);
        else if (tEvent.activity == 'two_month_customer') twoMonthCustomer(tEvent.start, handleResult);
        else if (tEvent.activity == 'one_week_user') oneWeekUser(tEvent.start, handleResult);
        else if (tEvent.activity == 'bill_upcoming') billUpcoming(tEvent.start, handleResult);
        else if (tEvent.activity == 'no_app_registered') noAppRegistered(tEvent.start, handleResult);
        else if ( tEvent.activity.startsWith('circles_switch')) handleCirclesSwitchNotificationEvent(tEvent, handleResult);        
        else if (tEvent.activity === 'suspension_notice') sendSuspensionNotice(tEvent, handleResult);
        else if (tEvent.activity === 'suspension_over_notice') sendActivationNotice(tEvent, handleResult);
        else send(tEvent, handleResult);
    }

}

function send(tEvent, callback) {
    let params = JSON.parse(JSON.stringify(tEvent));
    params.teamID = 5;
    params.teamName = 'Operations';
    params.delay = 2000;
    delete params._id;
    delete params.start;
    delete params.action;
    delete params.allDay;
    delete params.trigger;
    delete params.title;
    delete params.parallelLimitOne;
    delete params.createdDate;

    notificationSend.deliver(params, undefined, callback);
}

function aggregate(tStart, callback) {
    const hour = `${new Date(tStart).toISOString().split(':')[0]}:00:00.000Z`;
    const end =  new Date(hour).getTime();
    const start = end - (60 * 60 * 1000);
    let count = 0;
    const agg = (match, callback) => {
        const project = { '$project': {
            stamp: { '$dateToString': {
                format: '%Y-%m-%dT%H:00:00.000Z',
                date: { '$add': [ new Date(0), '$ts' ] }
            } },
            activity: 1,
            teamName: 1,
            sms: { '$setIsSubset': [ '$transport', [ 'sms' ] ] },
            email: { '$setIsSubset': [ '$transport', [ 'email' ] ] },
            selfcare: { '$setIsSubset': [ '$transport', [ 'selfcare' ] ] },
            gentwo: { '$setIsSubset' : [ '$transport', [ 'gentwo' ] ] },
            ussd: { '$setIsSubset' : [ '$transport', [ 'ussd' ] ] }
        } };
        const group = { '$group': {
            _id: { stamp: '$stamp',
                activity: '$activity',
                teamName: '$teamName',
                sms: '$sms',
                email: '$email',
                selfcare: '$selfcare',
                gentwo: '$gentwo',
                ussd: '$ussd' },
            count: { '$sum' : 1 }
        } };
        const combine = { '$group' : {
            _id: '$_id.stamp',
            groups: { '$addToSet': {
                activity: '$_id.activity',
                teamName: '$_id.teamName',
                sms: '$_id.sms',
                email: '$_id.email',
                selfcare: '$_id.selfcare',
                gentwo: '$_id.gentwo',
                ussd: '$_id.ussd',
                count: '$count'
            } }
        } };
        db.notifications_logbook.aggregate([ match, project, group, combine ]).toArray((err, result) => {
            if ( !err && result && (result.length > 0) ) {
                result[0].ts = new Date(result[0]._id).getTime();
                delete result[0]._id;
                db.agg.updateOne({ ts: result[0].id }, result[0], { upsert: true }, () => {
                    callback();
                });
            } else {
                callback();
            }
        });
    };
    let match = { '$match': { transport: { '$ne': null } } };
    for (let i=start; i<end; i=i+(60 * 60 * 1000)) {        // 1 hour
        count++;
        match['$match'].ts = { '$gte': i, '$lt': i + (60 * 60 * 1000) };
        agg(match, () => {
            count--;
            if (count==0) callback(undefined, JSON.stringify(match));
        });
    }
}

function bonusLoyaltyCountdown(start, callback) {
    const getSGDate = (ts) => {
        const d = new Date(ts);
        const d1 = d.getHours() >= 16 ? d.getDate() + 1 : d.getDate();
        return common.getDate(new Date(d.getFullYear(), d.getMonth(), d1 + 45));
    };
    const currentDate = getSGDate(start);
    const query = `SELECT t.serviceinstanceaccount, TIMESTAMPDIFF(MONTH, t.sg1, DATE(${db.escape(currentDate)})) months FROM (
        SELECT s.serviceinstanceaccount, s.sg1, MONTH(s.sg1) m1, DAY(s.sg1) d1, MONTH(s.sg2) m2, DAY(s.sg2) d2 FROM (
        SELECT * FROM (SELECT SUM(IF(n.numberstatus='Active',1,0)) numberstatus, a.serviceinstanceaccount,
        a.joindate sg1, DATE_ADD(a.joindate, INTERVAL 6 MONTH) sg2
        FROM ecAccounts a LEFT JOIN ecNetworkServiceInstance n ON a.serviceinstanceaccount=n.serviceinstanceaccount WHERE
        a.accountstatus='Active' AND a.joindate IS NOT NULL GROUP BY a.serviceinstanceaccount) r WHERE r.numberstatus > 0) s WHERE
        DATE(s.sg1)<DATE(${db.escape(currentDate)})) t WHERE
        (t.m1=MONTH(${db.escape(currentDate)}) AND t.d1=DAY(${db.escape(currentDate)})) OR
        (t.m2=MONTH(${db.escape(currentDate)}) AND t.d2=DAY(${db.escape(currentDate)}))`;
    if (LOG) common.log('loyaltyCountdown', query);
    db.query_err(query, (err, rows) => {
        let sendQ = async.queue(send);
        if (rows && rows.length > 0) rows.forEach((row) => {
            sendQ.push({ serviceInstanceNumber: row.serviceinstanceaccount, activity: 'bonus_loyalty_countdown' });
        });
        sendQ.drain = () => {
            callback(undefined, rows);
        };
    });
}

function threeMonthCustomer(start, callback) {
    const getSGDate = (ts) => {
        const d = new Date(ts);
        const d1 = d.getHours() >= 16 ? d.getDate() + 1 : d.getDate();
        return common.getDate(new Date(d.getFullYear(), d.getMonth() - 3, d1));
    };
    const currentDate = getSGDate(start);
    const query = `SELECT * FROM (SELECT SUM(IF(n.numberstatus='Active',1,0)) numberstatus, a.serviceinstanceaccount, a.joindate FROM
        ecAccounts a LEFT JOIN ecNetworkServiceInstance n ON a.serviceinstanceaccount=n.serviceinstanceaccount WHERE
        YEAR(a.joindate)=YEAR(${db.escape(currentDate)}) AND
        MONTH(a.joindate)=MONTH(${db.escape(currentDate)}) AND
        DAY(a.joindate)=DAY(${db.escape(currentDate)}) AND a.accountstatus='Active'
        GROUP BY a.serviceinstanceaccount) r WHERE r.numberstatus > 0`;
    if (LOG) common.log('threeMonthCustomer', query);
    db.query_err(query, (err, rows) => {
        let sendQ = async.queue(send);
        if (rows && rows.length > 0) rows.forEach((row) => {
            sendQ.push({ serviceInstanceNumber: row.serviceinstanceaccount, activity: 'three_month_customer' });
        });
        sendQ.drain = () => {
            callback(undefined, rows);
        };
    });
}

function twoMonthCustomer(start, callback) {
    const getSGDate = (ts) => {
        const d = new Date(ts);
        const d1 = d.getHours() >= 16 ? d.getDate() + 1 : d.getDate();
        return common.getDate(new Date(d.getFullYear(), d.getMonth() - 2, d1));
    };
    const currentDate = getSGDate(start);
    const query = `SELECT * FROM (SELECT SUM(IF(n.numberstatus='Active',1,0)) numberstatus, 
        a.serviceinstanceaccount, a.customername, a.joindate FROM 
        ecAccounts a LEFT JOIN ecNetworkServiceInstance n ON 
        a.serviceinstanceaccount=n.serviceinstanceaccount WHERE
        YEAR(a.joindate)=YEAR(${db.escape(currentDate)}) AND
        MONTH(a.joindate)=MONTH(${db.escape(currentDate)}) AND
        DAY(a.joindate)=DAY(${db.escape(currentDate)}) AND a.accountstatus='Active'
        GROUP BY a.serviceinstanceaccount) r WHERE r.numberstatus > 0`;
    if (LOG) common.log('twoMonthCustomer', query);
    db.query_err(query, (err, rows) => {
        configManager.loadConfigMapForKey('bonus', 'retention_bonus', function (error, item) {
            if (error) {
                return callback(error);
            }

            if (!item.options.enabled) {
                if (LOG) common.log('Notification', 'Retention bonus disabled');
                return callback(undefined, {sent: false, reason: 'not enabled'});
            }
            let sendQ = async.queue(send);
            if (rows && rows.length > 0) rows.forEach((row) => {
                sendQ.push({
                    name: row.customername,
                    serviceInstanceNumber: row.serviceinstanceaccount,
                    activity: '500mb_notify' });
            });
            sendQ.drain = () => {
                callback(undefined, rows);
            };
        });
    });
}

function oneWeekUser(start, callback) {
    const getSGDate = (ts) => {
        const d = new Date(ts);
        const d1 = d.getHours() >= 16 ? d.getDate() + 1 : d.getDate();
        return common.getDate(new Date(d.getFullYear(), d.getMonth(), d1 - 7));
    };
    const currentDate = getSGDate(start);
    const query = `SELECT * FROM (SELECT SUM(IF(n.numberstatus='Active',1,0)) numberstatus, a.serviceinstanceaccount, a.joindate FROM
        ecAccounts a LEFT JOIN ecNetworkServiceInstance n ON a.serviceinstanceaccount=n.serviceinstanceaccount WHERE
        YEAR(a.joindate)=YEAR(${db.escape(currentDate)}) AND
        MONTH(a.joindate)=MONTH(${db.escape(currentDate)}) AND
        DAY(a.joindate)=DAY(${db.escape(currentDate)}) AND a.accountstatus='Active'
        GROUP BY a.serviceinstanceaccount) r WHERE r.numberstatus > 0`;
    if (LOG) common.log('oneWeekUser', query);
    db.query_err(query, (err, rows) => {
        let sendQ = async.queue(send);
        if (rows && rows.length > 0) rows.forEach((row) => {
            sendQ.push({ serviceInstanceNumber: row.serviceinstanceaccount, activity: 'one_week_user' });
        });
        sendQ.drain = () => {
            callback(undefined, rows);
        };
    });
}

function billUpcoming(start, callback) { // run once a month, 3 days before end of month
    const lastDate = new Date(new Date().setDate(0));
    const dayOfMonth = (lastDate.getDate().toString().length == 1) ? `0${lastDate.getDate().toString()}` : lastDate.getDate().toString();
    const monthOfYear = ((lastDate.getMonth() + 1).toString().length == 1) ? `0${(lastDate.getMonth() + 1).toString()}` : (lastDate.getMonth() + 1).toString();
    const currentDate = `${lastDate.getFullYear()}-${monthOfYear}-${dayOfMonth}`;
    const query = `SELECT * FROM (SELECT SUM(IF(n.numberstatus='Active',1,0)) numberstatus, a.serviceinstanceaccount FROM
        ecAccounts a LEFT JOIN ecNetworkServiceInstance n ON a.serviceinstanceaccount=n.serviceinstanceaccount WHERE
        YEAR(a.joindate)=YEAR(${db.escape(currentDate)}) AND
        MONTH(a.joindate)=MONTH(${db.escape(currentDate)}) AND
        a.accountstatus='Active' AND a.baseplan = 'CirclesOne'
        GROUP BY a.serviceinstanceaccount) r WHERE r.numberstatus > 0`;
    if (LOG) common.log('billUpcoming', query);
    db.query_err(query, (err, rows) => {
        let sendQ = async.queue(send);
        if (rows && rows.length > 0) rows.forEach((row) => {
            sendQ.push({
                month: dateTime.getMonthString(new Date().getTime(), config.COUNTRY_ISOCODE),
                serviceInstanceNumber: row.serviceinstanceaccount,
                activity: 'bill_upcoming'
            });
        });
        sendQ.drain = () => {
            callback(undefined, rows);
        };
    });
}

function noAppRegistered(start, callback) {
    const getSGDate = (ts) => {
        const d = new Date(ts);
        const d1 = d.getHours() >= 16 ? d.getDate() + 1 : d.getDate();
        return common.getDate(new Date(d.getFullYear(), d.getMonth(), d1 - 3));
    };
    const currentDate = getSGDate(start);
    const query = `SELECT s.serviceinstanceaccount FROM (SELECT a.serviceinstanceaccount, a.joindate,
        SUM(IF(p.id IS NOT NULL, 1, 0)) mark, SUM(IF(n.numberstatus='Active',1,0)) numberstatus FROM ecAccounts a LEFT JOIN
        ecNetworkServiceInstance n ON a.serviceinstanceaccount=n.serviceinstanceaccount LEFT JOIN
        app p ON p.number=CONCAT('65',n.subscriberidentifier) WHERE
        a.joindate IS NOT NULL AND
        a.joindate=DATE(${db.escape(currentDate)}) GROUP BY a.serviceinstanceaccount) s WHERE s.numberstatus > 0 AND s.mark=0`;
    if (LOG) common.log('noAppRegistered', query);
    db.query_err(query, (err, rows) => {
        let sendQ = async.queue(send);
        if (rows && rows.length > 0) rows.forEach((row) => {
            sendQ.push({ serviceInstanceNumber: row.serviceinstanceaccount, activity: 'no_app_registered' });
        });
        sendQ.drain = () => {
            callback(undefined, rows);
        };
    });
}

function handleCirclesSwitchNotificationEvent(event, callback) {
    ec.getCustomerDetailsServiceInstance(event.serviceInstanceNumber, true,  (err, cache) => {
        if (!cache){
            callback({ message: 'customer cache could not be loaded' });
        } else if (cache.status !== 'Active') {
            callback(null, { message: `customer inactive with status: ${cache.status}` });
        } else if (cache.serviceInstanceBasePlanName !== 'CirclesSwitch') {
            callback(null, { message: 'customer not in circles switch anymore' });
        } else {
            switch (event.activity) {
            case 'circles_switch_1_day_active': {
                send(event, ()=>{});
                callback(null, { message: 'notification added to send' });
                break;
            }
            case 'circles_switch_7_days_active': {
                send(event, ()=>{});
                callback(null, { message: 'notification added to send' });
                break;
            }
            case 'circles_switch_14_days_active': {
                send(event, ()=>{});
                callback(null, { message: 'notification added to send' });
                break;
            }
            case 'circles_switch_low_data_usage': {
                const plus2020 = ec.findPackageName(ec.packages(), 'Plus 2020').id;
                const ccInstall = ec.findPackageName(ec.packages(), 'Bonus 1GB Circles Care Install').id;
                elitecoreUsageService.loadRemainingDataForPackage(cache.number, plus2020, (err1, amount1) => {
                    elitecoreUsageService.loadRemainingDataForPackage(cache.number, ccInstall, (err2, amount2) => {
                        if (err1 || err2 || !Number.isInteger(amount1) || !Number.isInteger(amount2)) {
                            callback(null, { message: 'Error fetching balance data' });
                        } else if ((amount1 + amount2) * 1024 < 100) {
                            send(event, ()=>{});
                            callback(null, { message: 'notification added to send' });
                        } else {
                            callback(null, { message: 'Customer have enough data in the plan' });
                        }
                    });
                });
                break;
            }
            case 'circles_switch_15_days_before_end': {
                send(event, ()=>{});
                callback(null, { message: 'notification added to send' });
                break;
            }
            case 'circles_switch_1_days_before_end': {
                send(event, ()=>{});
                callback(null, { message: 'notification added to send' });
                break;
            }
            default: {
                callback({ message: 'unknown notification activity from circles switch' });  
            }
            }
        }
    });
}

function sendSuspensionNotice(tEvent, callback) {
    const params = {
        name: tEvent.name,
        prefix: tEvent.prefix,
        number: tEvent.number,
        serviceInstanceNumber: tEvent.serviceInstanceNumber,
        email: tEvent.email,
        activity: tEvent.activity,
        suspension_amount_paid: tEvent.suspension_amount_paid,
        suspension_starting: tEvent.suspension_starting,
        suspension_ending: tEvent.suspension_ending,
        teamID: 5,
        teamName: 'Operations'
    };
    notificationSend.deliver(params, undefined, callback);
}

function sendActivationNotice(tEvent, callback) {
    const params = {
        name: tEvent.name,
        prefix: tEvent.prefix,
        number: tEvent.number,
        serviceInstanceNumber: tEvent.serviceInstanceNumber,
        email: tEvent.email,
        activity: tEvent.activity,
        suspension_period: tEvent.suspension_period,
        month_after_suspension: tEvent.month_after_suspension,
        teamID: 5,
        teamName: 'Operations'
    };
    notificationSend.deliver(params, undefined, callback);
}

module.exports = Notifications;
