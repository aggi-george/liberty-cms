var config = require('../../../../config');
var db = require('../../../../lib/db_handler');
var common = require('../../../../lib/common');

var paymentManager = require('../../manager/account/paymentManager');

exports.initiateInvoicePayment = function (tEvent, callback) {
    var billingAccountNumber = tEvent.billingAccountNumber;
    var invoicesInfo = tEvent.invoicesInfo;
    var options = tEvent.options;
    var myfqdn = config.MYFQDN;

    paymentManager.queueBillPaymentByBA(billingAccountNumber, invoicesInfo, options, (err, result) => {
        if (err) {
            common.error("QueueAction[payment]", "initiateInvoicePayment: failed to queue " +
            "payment request, billingAccountNumber=" + billingAccountNumber);
        }

        var color = err ? "red" : "green";
        var processed = {
            result: result,
            error: err ? err.message : undefined
        };
        if (tEvent._id) {
            db.scheduler.update({_id: tEvent._id},
                {$set: {processed: processed, color: color, myfqdn: myfqdn}});
        }

        callback(undefined, {id: tEvent._id, processed: processed});
    });
}