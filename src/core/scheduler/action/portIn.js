var config = require('../../../../config');
var db = require('../../../../lib/db_handler');
var common = require('../../../../lib/common');

var portInManager = require('../../manager/porting/portInManager');

exports.initiatePortIn = function (tEvent, callback) {
    var requestId = tEvent.requestId;
    var portInNumber = tEvent.portInNumber;
    var donor = tEvent.donor;
    var initiator = tEvent.trigger;
    var myfqdn = config.MYFQDN;

    portInManager.updateRequestStatus(requestId, "IN_PROGRESS", false, true, (err, result) => {
        if (err) {
            common.error("QueueAction[portin]", "initiatePortIn: failed to Port-In" +
            ", portInNumber= " + portInNumber + ", donor=" + donor);
        }

        var color = err ? "red" : "green";
        var processed = {result: result, error: err ? err.message : undefined};

        if (tEvent._id) {
            db.scheduler.update({_id: tEvent._id},
                {$set: {processed: processed, color: color, myfqdn: myfqdn}});
        }

        callback(undefined, {id: tEvent._id, processed: processed});
    }, undefined, undefined, initiator);
}

exports.initiatePortInFailureCheck = function (tEvent, callback) {
    var requestId = tEvent.requestId;
    var portInPrefix = tEvent.portInPrefix;
    var portInNumber = tEvent.portInNumber;
    var initiator = tEvent.trigger;
    var myfqdn = config.MYFQDN;

    portInManager.onPortInNotification(portInPrefix, portInNumber, "FAILED", true, (err, result) => {
        if (err) {
            common.error("QueueAction[portin]", "initiatePortInFailureCheck: failed to Port-In" +
            ", portInNumber= " + portInNumber + ", requestId=" + requestId);
        }

        var color = err ? "red" : "green";
        var processed = {result: result, error: err ? err.message : undefined};

        if (tEvent._id) {
            db.scheduler.update({_id: tEvent._id},
                {$set: {processed: processed, color: color, myfqdn: myfqdn}});
        }

        callback(undefined, {id: tEvent._id, processed: processed});
    }, undefined, undefined, initiator);
}

exports.initiatePortInCompletionStatusCheck = function (tEvent, callback) {
    var requestId = tEvent.requestId;
    var portInPrefix = tEvent.portInPrefix;
    var portInNumber = tEvent.portInNumber;
    var initiator = tEvent.trigger;
    var myfqdn = config.MYFQDN;

    var handlerResultStatus = (err, status, result) => {
        var color = err ? "red" : "green";
        var processed = {result: result, error: err ? err.message : undefined};

        if (tEvent._id) {
            db.scheduler.update({_id: tEvent._id},
                {$set: {processed: processed, status: status, color: color, myfqdn: myfqdn}});
        }

        return callback(undefined, {id: tEvent._id, processed: processed});
    }

    portInManager.loadRecentRequest(portInPrefix, portInNumber, (err, result) => {
        if (err) {
            common.error("QueueAction[portin]", "initiatePortInCompletionStatusCheck: failed to load active port-in request" +
            ", portInNumber= " + portInNumber + ", requestId=" + requestId);
            return handlerResultStatus(err, "ERROR_REQUEST_LOADING");
        }

        if (!result || !result.active) {
            return handlerResultStatus(undefined, "PORT_IN_NOT_ACTIVE");
        }

        elitecorePortInService.loadPortInCompleationStatus(portInNumber, (err, result) => {
            if (err) {
                common.error("QueueAction[portin]", "initiatePortInCompletionStatusCheck: failed to check port-in completion status" +
                ", portInNumber= " + portInNumber + ", requestId=" + requestId);
                return handlerResultStatus(err, "ERROR_REQUEST_LOADING")
            }

            if (!result || result.status == "NOT_FOUND") {
                return handlerResultStatus(undefined, "COMPLETION_STATUS_NOT_FOUND");
            }

            portInManager.onPortInNotification(portInPrefix, portInNumber, result.status, false, (err, result) => {
                if (err) {
                    common.error("QueueAction[portin]", "initiatePortInCompletionStatusCheck: failed to check port-in completion status" +
                    ", portInNumber= " + portInNumber + ", requestId=" + requestId);
                    return handlerResultStatus(err, "ERROR_REQUEST_LOADING")
                }

                return handlerResultStatus(undefined, "OK", result);

            }, undefined, undefined, initiator);
        });
    });
}
