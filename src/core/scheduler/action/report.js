var async = require('async');
var config = require('../../../../config');
var ec = require('../../../../lib/elitecore');
var db = require('../../../../lib/db_handler');
var common = require('../../../../lib/common');
var notificationSend = require('../../manager/notifications/send');
var bonusAggregate = require('../../../../tests/bonus_aggregate');
var bonusDoubles = require('../../../../tests/bonus_doubles');
var ecommDashboard = require('../../../../tests/ecomm_dashboard');
var generalDoubles = require('../../../../tests/general_doubles');
var portInManager = require('../../manager/porting/portInManager');
var ecommManager = require('../../manager/ecomm/ecommManager');
const anomalyManager = require('../../manager/account/anomalyManager');
const exec = require('child_process').exec;

exports.detail = function (tEvent, callback) {
    var activity = tEvent.activity;
    switch(activity) {
        case "daily_scheduler_report":
            schedulerReport(tEvent, callback);
            break;
        case "monthly_gentwo_report":
            gentwoReport(tEvent, callback);
            break;
        case "bonus_anomaly_report":
            bonusAnomalyReport(tEvent, callback);
            break;
        case "bonus_doubles_report":
            bonusDoublesReport(tEvent, callback);
            break;
        case "general_doubles_report":
            generalDoublesReport(tEvent, callback);
            break;
        case "ecomm_dashboard_report":
            ecommDashboardReport(tEvent, callback);
            break;
        case "porting_report":
            portingReport(tEvent, callback);
            break;
        case "customer_provisioning_report":
            customerProvisioningReport(tEvent, callback);
            break;
        case "customer_status_change_report":
            customerStatusChangeReport(tEvent, callback);
            break;
        case "customer_activation_report":
            customerActivationReport(tEvent, callback);
            break;
        case "ndp_anomally_report":
            ndpAnomally(tEvent, callback);
            break;
        case "voicemail_test":
            voicemailTest(tEvent, callback);
            break;
        default:
    }
}

function customerActivationReport(tEvent, callback) {
    var template = {
        "style" : { "table" : "border-collapse:collapse",
            "th" : "border:1px solid black",
            "td" : "border:1px solid black" },
        "head" : [ "Created Date", "Start", "Number", "Service Instance Number", "Trigger" , "Months", "Color"],
        "body" : [ ] };

    
    var date = new Date(tEvent.start);
    var start = new Date(date.getFullYear(), date.getMonth() - 1, 1).getTime() + 24 * 3600000;
    var end = date.getTime();
    var from = common.getDate(new Date(start + 3600000 * 8)) + " " + common.getTimeString(new Date(start + 3600000 * 8));
    var to = common.getDate(new Date(end + 3600000 * 8)) + " " + common.getTimeString(new Date(end + 3600000 * 8));
    db.scheduler.find({"action": "activate", "activity": "suspension_over","start": {"$gte": start, "$lte": end}}).toArray(function(err, results){
        results.forEach(function(item){
            if(item.createdDate){
                var createdDateFormatted = common.getDate(new Date(item.createdDate + 3600000 * 8)) + " " + common.getTimeString(new Date(item.createdDate + 3600000 * 8));
            }else{
                var createdDateFormatted = "N/A";
            }
            var startDateFormatted = common.getDate(new Date(item.start + 3600000 * 8)) + " " + common.getTimeString(new Date(item.start + 3600000 * 8));
            var color = item.color ? item.color : "blue";
            template.body.push([ createdDateFormatted, startDateFormatted, item.number, item.serviceInstanceNumber, item.trigger, item.suspension_period, color]);
        });
        if (template.body.length > 0) {
            var variables = {
                "reportSubject" : "Customer Activation Report: " + from + " - " + to,
                "label" : "Total Activations: " + template.body.length,
                "table" : template
            };
            notificationSend.internal(undefined, undefined, undefined, undefined,
                    "customer_activation_report", variables, undefined, function(err, result) {
                if (tEvent._id) db.scheduler.update({ "_id" : tEvent._id },
                    { "$set" : { "processed" : result, "color" : "green", "myfqdn" : config.MYFQDN } });
                callback(undefined, { "id" : tEvent._id, "processed" : result });
            });
        } else {
            var result = {"message": "no records found for the report"};
            if (tEvent._id) db.scheduler.update({ "_id" : tEvent._id },
                { "$set" : { "processed" : result, "color" : "green", "myfqdn" : config.MYFQDN } });
            callback(undefined, { "id" : tEvent._id, "processed" : result });
        }
    });
}

function customerStatusChangeReport(tEvent, callback) {
    var suspendTemplate = {
        "style" : { "table" : "border-collapse:collapse",
            "th" : "border:1px solid black",
            "td" : "border:1px solid black" },
        "head" : [ "Created Date", "Start", "Number", "Service Instance Number", "Trigger" , "Ending", "Color"],
        "body" : [ ] };
    var activateTemplate = {
        "style" : { "table" : "border-collapse:collapse",
            "th" : "border:1px solid black",
            "td" : "border:1px solid black" },
        "head" : [ "Created Date", "Start", "Number", "Service Instance Number", "Trigger" , "Months", "Color"],
        "body" : [ ] };
    var terminateTemplate = {
        "style" : { "table" : "border-collapse:collapse",
            "th" : "border:1px solid black",
            "td" : "border:1px solid black" },
        "head" : [ "Created Date", "Start", "Number", "Service Instance Number", "Trigger", "Color"],
        "body" : [ ] };
    var aggArray = [];
    var date = new Date(tEvent.start);
    var start = new Date(date.getFullYear(), date.getMonth(), 1).getTime();
    var end = tEvent.start;
    var from = common.getDate(new Date(start + 8 * 3600000 + 1000)) + " " + common.getTimeString(new Date(start + 8 * 3600000 + 1000));
    var to = common.getDate(new Date(end + 8 * 3600000)) + " " + common.getTimeString(new Date(end + 8 * 3600000));
    aggArray.push({"$match": {"createdDate": {"$gt": start, "$lt": end}, "action": {"$in": ["suspend", "activate", "terminate"]}}});
    aggArray.push({"$sort": {"createdDate": -1}});
    aggArray.push({"$group": {_id : "$action", "items": {"$push": {"createdDate": "$createdDate", "start": "$start", "number": "$number", "serviceInstanceNumber": "$serviceInstanceNumber", "trigger": "$trigger", "months":"$suspension_period", "ending":"$suspension_ending", "color": "$color"}}}});
    db.scheduler.aggregate(aggArray, function(err, result){
        result.forEach(function(bigItem){
            bigItem.items.forEach(function(item){
                var createdDateFormatted = common.getDate(new Date(item.createdDate + 8 * 3600000)) + " " + common.getTimeString(new Date(item.createdDate + 8 * 3600000));
                var startDateFormatted = common.getDate(new Date(item.start + 8 * 3600000)) + " " + common.getTimeString(new Date(item.start + 8 * 3600000));
                var color = item.color ? item.color : "blue";
                if(bigItem._id == "suspend"){
                    var suspensionEnding = common.getDate(new Date(item.ending + 8 * 3600000)) + " " + common.getTimeString(new Date(item.ending+ 8 * 3600000));
                    suspendTemplate.body.push([ createdDateFormatted, startDateFormatted, item.number, item.serviceInstanceNumber, item.trigger, suspensionEnding, color]);
                }else if(bigItem._id == "activate"){
                    activateTemplate.body.push([ createdDateFormatted, startDateFormatted, item.number, item.serviceInstanceNumber, item.trigger, item.months, color]);
                }else if(bigItem._id == "terminate"){
                    terminateTemplate.body.push([ createdDateFormatted, startDateFormatted, item.number, item.serviceInstanceNumber, item.trigger, color]);
                }
            })
        });
        if (suspendTemplate.body.length > 0 || activateTemplate.body.length > 0 || terminateTemplate.body.length > 0) {
            var variables = {
                "reportSubject" : "Customer Status Change Report: " + from + " - " + to,
                "labelA" : "Total Suspensions: " + suspendTemplate.body.length,
                "tableA" : suspendTemplate,
                "labelB" : "Total Activations: " + activateTemplate.body.length,
                "tableB" : activateTemplate,
                "labelC" : "Total Terminations: " + terminateTemplate.body.length,
                "tableC" : terminateTemplate
            };
            notificationSend.internal(undefined, undefined, undefined, undefined,
                    "customer_status_change_report", variables, undefined, function(err, result) {
                if (tEvent._id) db.scheduler.update({ "_id" : tEvent._id },
                    { "$set" : { "processed" : result, "color" : "green", "myfqdn" : config.MYFQDN } });
                callback(undefined, { "id" : tEvent._id, "processed" : result });
            });
        } else {
            var processed = {"message": "no records found for the report"};
            if (tEvent._id) db.scheduler.update({ "_id" : tEvent._id },
                { "$set" : { "processed" : processed, "color" : "green", "myfqdn" : config.MYFQDN } });
            callback(undefined, { "id" : tEvent._id, "processed" : processed });
        }
    });
}

function ndpAnomally(tEvent, callback){
    var startTimestring = '2017-09-01 00:00:00';
    var q = "select distinct serviceinstanceaccount from ecAccounts where orderrefno >= '000001501516904022' and activationdate > " + db.escape(startTimestring);
    var alreadySubscribeToPaid = [];
    var alreadyNotSubscribeToAnyTwenty = [];
    db.query_noerr(q, function(rows){
        async.forEach(rows, function(row, cb){
            db.addonlog.findOne({"serviceInstanceNumber": row.serviceinstanceaccount, "type": "PRD00746"}, function(err, item){
                if(item && (!item.args.addOnPlans[0].fromDate || (new Date(item.args.addOnPlans[0].fromDate).getTime() < new Date().getTime()))){
                    alreadySubscribeToPaid.push(row.serviceinstanceaccount);
                }
                db.addonlog.findOne({"serviceInstanceNumber": row.serviceinstanceaccount, "type": "PRD00800"}, function(err, item){
                    if(!item){
                        if(alreadySubscribeToPaid.indexOf(row.serviceinstanceaccount) == -1){
                            alreadyNotSubscribeToAnyTwenty.push(row.serviceinstanceaccount);
                        }
                    }
                    cb();
                })
            })
        }, function(){
            sendAnomalyNotification(alreadySubscribeToPaid, alreadyNotSubscribeToAnyTwenty);
            if (tEvent._id) db.scheduler.update({ "_id" : tEvent._id },
                { "$set" : { "processed" : {}, "color" : "green", "myfqdn" : config.MYFQDN } });
            callback(undefined, { "id" : tEvent._id, "processed" : "done" });
        });
    });
}

function sendAnomalyNotification(alreadySubscribeToPaid, alreadyNotSubscribeToAnyTwenty){
	var notification = {
		activity: "ndp_promo_anomalies",
		teamID: 5,
        teamName: "Operations",
        since: "2017-08-15 00:00:00",
        alreadySubscribeToPaid: alreadySubscribeToPaid.length > 0 ? alreadySubscribeToPaid.join(",") : "Nothing",
        alreadyNotSubscribeToAnyTwenty: alreadyNotSubscribeToAnyTwenty.length > 0 ? alreadyNotSubscribeToAnyTwenty.join(",") : "Nothing"
	};
	notificationSend.deliver(notification, null, function (err, result) {
		if (err) {
			common.error("Failure Alert failed", "Activity id: " + alertActivityId);
		}
	});
}

function schedulerReport(tEvent, cb) {
    var ref = new Date(tEvent.start);
    var y = ref.getFullYear();
    var m = ref.getMonth();
    var d = ref.getDate();
    var now = new Date(y, m, d).getTime();        // start of day

    var yesterday = now - (24 * 60 * 60 * 1000);
    var tomorrow = now + (24 * 60 * 60 * 1000);
    var tasks = new Array;
    tasks.push(function (callback) {
        db.scheduler.aggregate([ { "$match" : { "start" : { "$gte" : yesterday, "$lt" : now } } },
                { "$group" : { "_id" : { "action" : "$action", "activity" : "$activity", "color" : "$color" },
                    "count" : { "$sum" : 1 } } } ], function (err, result) {
            callback(err, { "yesterday" : result });
        });
    });
    tasks.push(function (callback) {
        db.scheduler.aggregate([ { "$match" : { "start" : { "$gte" : now, "$lt" : tomorrow } } },
                { "$group" : { "_id" : { "action" : "$action", "activity" : "$activity" },
                    "count" : { "$sum" : 1 } } } ], function (err, result) {
            callback(err, { "today" : result });
        });
    });
    async.parallel(tasks, function (err, result) {
        var yesterdayList;
        var todayList;
        result.forEach(function (item) {
            if (item && item.yesterday) yesterdayList = item.yesterday;
            if (item && item.today) todayList = item.today;
        });
        var yesterdayObj = {
            "style" : { "table" : "border-collapse:collapse",
                "th" : "border:1px solid black",
                "td" : "border:1px solid black" },
            "head" : [ "Action", "Activity", "Status", "Count" ], "body" : [ ] };
        var todayObj = {
            "style" : { "table" : "border-collapse:collapse",
                "th" : "border:1px solid black",
                "td" : "border:1px solid black" },
            "head" : [ "Action", "Activity", "Count" ], "body" : [ ] };
        yesterdayList.forEach(function (item) {
            yesterdayObj.body.push([ item._id.action, item._id.activity, item._id.color, item.count ]);
        });
        todayList.forEach(function (item) {
            todayObj.body.push([ item._id.action, item._id.activity, item.count ]);
        });
        notificationSend.internal(undefined, undefined, undefined, undefined, "daily_scheduler_report", { "reportYesterday" : yesterdayObj, "reportToday" : todayObj }, undefined, function(err, result) {
            if (tEvent._id) db.scheduler.update({ "_id" : tEvent._id },
                { "$set" : { "processed" : result, "color" : "green", "myfqdn" : config.MYFQDN } });
            cb(undefined, { "id" : tEvent._id, "processed" : result });
        });
    });
}

function gentwoReport(tEvent, callback) {
    var ref = new Date(tEvent.start);
    var y = ref.getFullYear();
    var m = ref.getMonth();
    var monthStart = new Date(y, m, 1).getTime();

    db.cdrs.aggregate( [
        { "$match" : { "start_time" : { "$lt" : monthStart } } },
        { "$project" : { "ingress_cost" : 1,
                "egress_cost" : 1,
                "profit" : { "$subtract" : [ "$ingress_cost", "$egress_cost" ] },
                "nonzero" : { "$cond" : { "if" : { "$gt" : [ "$duration", 0 ] },
                            "then" : 1, "else" : 0 } },
                "billtime" : { "$divide" : [ "$ingress_billtime", 60 ] },
                "month" : { "$dateToString" : { "format":"%Y-%m",
                        "date" : { "$add" : [ new Date(0),
                                { "$multiply" : [ 1000, "$start_time" ] } ] } } } } },
        { "$group" : { "_id" : { "year" : { "$substr" : [ "$month", 0, 4 ] },
                        "month" : { "$substr" : [ "$month", 5, 2 ] } },
                "calls" : { "$sum" : 1 },
                "nonzero" : { "$sum" : "$nonzero" },
                "minutes" : { "$sum" : "$billtime" },
                "profit" : { "$sum" : "$profit" },
                "cost" : { "$sum" : "$egress_cost" },
                "charge" : { "$sum" : "$ingress_cost" } } },
        { "$sort" : { "_id" : 1 } },
        { "$project" : { "_id" : 0,
                "year" : "$_id.year",
                "month" : "$_id.month",
                "calls" : 1,
                "nonzero" : 1,
                "minutes" : { "$subtract" : [ "$minutes", { "$mod" : [ "$minutes", 0.01 ] } ] },
                "profit" : { "$subtract" : [ "$profit", { "$mod" : [ "$profit", 0.01 ] } ] },
                "cost" : { "$subtract" : [ "$cost", { "$mod" : [ "$cost", 0.01 ] } ] },
                "charge" : { "$subtract" : [ "$charge", { "$mod" : [ "$charge", 0.01 ] } ] },
                "acd" : { "$subtract" : [ { "$divide" : [ "$minutes", "$nonzero" ] },
                        { "$mod" : [ { "$divide" : [ "$minutes", "$nonzero" ] }, 0.01 ] } ] } } } ],
            function(err, result) {
        var obj = {
            "style" : { "table" : "border-collapse:collapse",
                "th" : "border:1px solid black",
                "td" : "border:1px solid black" },
            "head" : [ "Year", "Month", "Calls", "Nonzero",
                    "Minutes", "Profit", "Cost", "Charge", "ACD" ],
            "body" : [ ] };
        result.forEach(function (item) {
            if (item) obj.body.push([ item.year, item.month, item.calls, item.nonzero,
                    item.minutes, item.profit, item.cost, item.charge, item.acd ]);
        });
        var variables = {
            "reportSubject" : "Gentwo Report",
            "labelA" : "Monthly",
            "tableA" : obj,
            "labelB" : "",
            "tableB" : "",
            "labelC" : "",
            "tableC" : "" };
        notificationSend.internal(undefined, undefined, undefined, undefined,
                "generic_report", variables, undefined, function(err, result) {
            if (tEvent._id) db.scheduler.update({ "_id" : tEvent._id },
                { "$set" : { "processed" : result, "color" : "green", "myfqdn" : config.MYFQDN } });
            callback(undefined, { "id" : tEvent._id, "processed" : result });
        });
    });
}

function bonusAnomalyReport(tEvent, callback) {
    var lower = {
        "style" : { "table" : "border-collapse:collapse",
            "th" : "border:1px solid black",
            "td" : "border:1px solid black" },
        "head" : [ "Service Instance Number", "Mobile Number", "Kirk", "EC", "Error", "Status" ],
        "body" : [ ] };
    var higher = {
        "style" : { "table" : "border-collapse:collapse",
            "th" : "border:1px solid black",
            "td" : "border:1px solid black" },
        "head" : [ "Service Instance Number", "Mobile Number", "Kirk", "EC", "Error", "Status" ],
        "body" : [ ] };
    var error = {
        "style" : { "table" : "border-collapse:collapse",
            "th" : "border:1px solid black",
            "td" : "border:1px solid black" },
        "head" : [ "Service Instance Number", "Mobile Number", "Kirk", "EC", "Error", "Status" ],
        "body" : [ ] };
    bonusAggregate.run(function (err, result) {
        if (!err && result) {
            result.sort(function (a, b) {
                if (a.errorCache < b.errorCache) return -1;
                else if (a.errorUsage < b.errorUsage) return -1;
                else if (a.diff < b.diff) return -1;
                else return 1;
            });
            var lowerTotal = 0;
            var lowerHistory = 0;
            var higherTotal = 0;
            var higherHistory = 0;
            result.forEach(function (item) {
                var number = (item && item.number) ? item.number : "";
                if (item && item.errorUsage) {
                    error.body.push([ item.serviceInstanceNumber, number, "", "",
                            JSON.stringify(item.errorUsage), item.status ]);
                } else if (item && item.tester) {
                } else if (item.totalPretty != item.historyPretty) {
                    if ( item && item.total && item.diff && (item.diff < 1) ) {
                        lowerTotal += item.total;
                        lowerHistory += item.history;
                        lower.body.push([ item.serviceInstanceNumber, number, item.historyPretty,
                                item.totalPretty, parseInt(item.diff * 100) + " %",
                                item.status ]);
                    } else if ( item && item.total && item.diff && (item.diff > 1) )  {
                        if (item.status == "Active") {
                            higherTotal += item.total;
                            higherHistory += item.history;
                            higher.body.push([ item.serviceInstanceNumber, number, item.historyPretty,
                                    item.totalPretty, parseInt(item.diff * 100) + " %",
                                    item.status ]);
                        }
                    }
                }
            });
            var variables = {
                "reportSubject" : "Bonus Anomaly Report",
                "labelA" : "Kirk - " + prettifyDataKb(lowerHistory) + " vs. EC - "
                        + prettifyDataKb(lowerTotal) + " ("
                        + prettifyDataKb(lowerTotal - lowerHistory) + " oversubscription)",
                "tableA" : lower,
                "labelB" : "Kirk - " + prettifyDataKb(higherHistory) + " vs. EC - "
                        + prettifyDataKb(higherTotal) + " ("
                        + prettifyDataKb(higherHistory - higherTotal) + " undersubscription)",
                "tableB" : higher,
                "labelC" : "Errors",
                "tableC" : error };
            if (lower.body.length == 0) {
                variables.labelA = "";
                variables.tableA = "";
            }
            if (higher.body.length == 0) {
                variables.labelB = "";
                variables.tableB = "";
            }
            if (error.body.length == 0) {
                variables.labelC = "";
                variables.tableC = "";
            }
            if ( (lower.body.length == 0) &&
                    (higher.body.length == 0) &&
                    (error.body.length == 0) ) {
                if (tEvent._id) db.scheduler.update({ "_id" : tEvent._id },
                    { "$set" : { "processed" : result, "color" : "green", "myfqdn" : config.MYFQDN } });
                callback(undefined, { "id" : tEvent._id, "processed" : result });
            } else {
                notificationSend.internal(undefined, undefined, undefined, undefined,
                        "generic_report", variables, undefined, function(err, result) {
                    if (tEvent._id) db.scheduler.update({ "_id" : tEvent._id },
                        { "$set" : { "processed" : result, "color" : "green", "myfqdn" : config.MYFQDN } });
                    callback(undefined, { "id" : tEvent._id, "processed" : result });
                });
            }
        } else {
            if (tEvent._id) db.scheduler.update({ "_id" : tEvent._id },
                { "$set" : { "processed" : { "error" : err, "result" : result }, "color" : "red", "myfqdn" : config.MYFQDN } });
            callback(undefined, { "id" : tEvent._id, "processed" : { "error" : err, "result" : result } });
        }
    });
}

function ecommDashboardReport(tEvent, callback) {
    var general = {
        "style" : { "table" : "border-collapse:collapse",
            "th" : "border:1px solid black",
            "td" : "border:1px solid black" },
        "head" : [ "Error", "List" ],
        "body" : [ ] };
    var duplicates = {
        "style" : { "table" : "border-collapse:collapse",
            "th" : "border:1px solid black",
            "td" : "border:1px solid black" },
        "head" : [ "Key", "List" ],
        "body" : [ ] };
    var missing = {
        "style" : { "table" : "border-collapse:collapse",
            "th" : "border:1px solid black",
            "td" : "border:1px solid black" },
        "head" : [ "Key", "Item" ],
        "body" : [ ] };
    ecommDashboard.run(function (error) {
        if (error) {
            if (error.noLink.length > 0)
                general.body.push([ "noLink", JSON.stringify(error.noLink) ]);
            if (error.unknown.length > 0)
                general.body.push([ "unknown", JSON.stringify(error.unknown) ]);
            if (error.terminated.length > 0)
                general.body.push([ "terminated", JSON.stringify(error.terminated) ]);
            if (error.listNotEqual.length > 0)
                general.body.push([ "listNotEqual", JSON.stringify(error.listNotEqual) ]);
            if (error.countNotEqual.length > 0)
                general.body.push([ "countNotEqual", JSON.stringify(error.countNotEqual) ]);
            if (error.noCount.length > 0)
                general.body.push([ "noCount", JSON.stringify(error.noCount) ]);
            if (error.duplicate.length > 0) {
                error.duplicate.forEach(function (item) {
                    Object.keys(item).forEach(function (key) {
                        duplicates.body.push([ key, item[key] ]);
                    });
                });
            }
            if (error.missing.length > 0) {
                error.missing.forEach(function (item) {
                    Object.keys(item).forEach(function (key) {
                        missing.body.push([ key, item[key] ]);
                    });
                });
            }
            var variables = {
                "reportSubject" : "Ecomm Dashboard Report",
                "labelA" : "General Anomalies",
                "tableA" : general,
                "labelB" : "Duplicates",
                "tableB" : duplicates,
                "labelC" : "Missing",
                "tableC" : missing };
            if (general.body.length == 0) {
                variables.labelA = "";
                variables.tableA = "";
            }
            if (duplicates.body.length == 0) {
                variables.labelB = "";
                variables.tableB = "";
            }
            if (missing.body.length == 0) {
                variables.labelC = "";
                variables.tableC = "";
            }

            notificationSend.internal(undefined, undefined, undefined, undefined,
                    "generic_report", variables, undefined, function(err, result) {
                if (tEvent._id) db.scheduler.update({ "_id" : tEvent._id },
                    { "$set" : { "processed" : result, "color" : "green", "myfqdn" : config.MYFQDN } });
                callback(undefined, { "id" : tEvent._id, "processed" : result });
            });
        }
    });
}

function prettifyDataKb(kb) {
    if (kb >= 1024 * 1024) {
        return (Math.round((kb / 1024 / 1024) * 10) / 10) + " GB";
    } else if (kb >= 1024) {
        return (Math.round(kb / 1024)) + " MB";
    } else {
        return "0 MB";
    }
}

function generalDoublesReport(tEvent, callback) {
    var obj = {
        "style" : { "table" : "border-collapse:collapse",
            "th" : "border:1px solid black",
            "td" : "border:1px solid black" },
        "head" : [ "Number", "Incoming", "Whatsapp", "CID", "Plan Change", "4G" ],
        "body" : [ ] };
    generalDoubles.run(function (err, result) {
        if (!err && result) result.forEach(function (item) {
            if (item && !item.tester)
            obj.body.push([ item.number, item.incoming, item.whatsapp, item.cid, item.planchange, item.fourg ]);
        });
        if (obj.body.length > 0) {
            var variables = {
                "reportSubject" : "General Addon Anomaly",
                "labelA" : "General Addon Anomaly",
                "tableA" : obj,
                "labelB" : "",
                "tableB" : "",
                "labelC" : "",
                "tableC" : "" };
            notificationSend.internal(undefined, undefined, undefined, undefined,
                    "generic_report", variables, undefined, function(err, result) {
                if (tEvent._id) db.scheduler.update({ "_id" : tEvent._id },
                    { "$set" : { "processed" : result, "color" : "green", "myfqdn" : config.MYFQDN } });
                callback(undefined, { "id" : tEvent._id, "processed" : result });
            });
        } else {
            if (tEvent._id) db.scheduler.update({ "_id" : tEvent._id },
                { "$set" : { "processed" : result, "color" : "green", "myfqdn" : config.MYFQDN } });
            callback(undefined, { "id" : tEvent._id, "processed" : result });
        }
    });
}

function bonusDoublesReport(tEvent, callback) {
    var obj = {
        "style" : { "table" : "border-collapse:collapse",
            "th" : "border:1px solid black",
            "td" : "border:1px solid black" },
        "head" : [ "Number", "Service Instance Number", "Anomaly" ],
        "body" : [ ] };
    bonusDoubles.run(function (err, result) {
        if (!err && result) result.forEach(function (item) {
            if (item && !item.tester)
            obj.body.push([ item.number, item.serviceInstanceNumber, item.anomaly ]);
        });
        if (obj.body.length > 0) {
            var variables = {
                "reportSubject" : "Bonus Doubles Anomaly",
                "labelA" : "Bonus Doubles Anomaly",
                "tableA" : obj,
                "labelB" : "",
                "tableB" : "",
                "labelC" : "",
                "tableC" : "" };
            notificationSend.internal(undefined, undefined, undefined, undefined,
                    "generic_report", variables, undefined, function(err, result) {
                if (tEvent._id) db.scheduler.update({ "_id" : tEvent._id },
                    { "$set" : { "processed" : result, "color" : "green", "myfqdn" : config.MYFQDN } });
                callback(undefined, { "id" : tEvent._id, "processed" : result });
            });
        } else {
            if (tEvent._id) db.scheduler.update({ "_id" : tEvent._id },
                { "$set" : { "processed" : result, "color" : "green", "myfqdn" : config.MYFQDN } });
            callback(undefined, { "id" : tEvent._id, "processed" : result });
        }
    });
}

function portingReport(tEvent, callback) {
    var obj = {
        "style" : { "table" : "border-collapse:collapse",
            "th" : "border:1px solid black",
            "td" : "border:1px solid black" },
        "head" : [ "Service Instance #", "Added", "Started", "Updated", "Temp #", "Ported #", "Donor", "Warning" ],
        "body" : [ ] };
    var handleCallback = function (err, result) {
        if (!err && result && result.data) result.data.forEach(function (item) {
            obj.body.push([
                item.service_instance_number,
                item.added_ts,
                item.start_ts,
                item.status_update_ts,
                item.temp_number,
                item.number,
                item.donor_network,
                item.warning_type ]);
        });
        if (obj.body.length > 0) {
            var variables = {
                "reportSubject" : "Porting Report",
                "labelA" : "Warnings",
                "tableA" : obj,
                "labelB" : "",
                "tableB" : "",
                "labelC" : "",
                "tableC" : "" };
            notificationSend.internal(undefined, undefined, undefined, undefined,
                    "generic_report", variables, undefined, function(err, result) {
                if (tEvent._id) db.scheduler.update({ "_id" : tEvent._id },
                    { "$set" : { "processed" : result, "color" : "green", "myfqdn" : config.MYFQDN } });
                callback(undefined, { "id" : tEvent._id, "processed" : result });
            });
        } else {
            if (tEvent._id) db.scheduler.update({ "_id" : tEvent._id },
                { "$set" : { "processed" : result, "color" : "green", "myfqdn" : config.MYFQDN } });
            callback(undefined, { "id" : tEvent._id, "processed" : result });
        }
    }
    portInManager.loadRequest(undefined, {status: "ACTIVE"}, handleCallback, 0, 0, undefined, "id", "asc", true);
}

function customerProvisioningReport(tEvent, callback) {
    var courier = tEvent.courier ? tEvent.courier : "SINGPOST";
    var ecommPushTask = function (tasks, today, slot, courier) {
        tasks.push(function (cb) { ecommManager.deliveryList(today, slot, courier, cb) });
    }
    var ecommTasks = new Array;
    if ( new Date().getHours() > 12 ) {
        var today = (new Date().getHours() < 16) ?
            common.getDate(new Date(common.msOffsetSG(new Date().getTime() + (24 * 60 * 60 * 1000)))) :
            common.getDate(new Date(common.msOffsetSG(new Date().getTime())));
        ecommPushTask(ecommTasks, today, "S1", courier);
    } else {
        var today = common.getDate(new Date(common.msOffsetSG(new Date().getTime())));
        ecommPushTask(ecommTasks, today, "S2", courier);
        ecommPushTask(ecommTasks, today, "S3", courier);
    }
    async.series(ecommTasks, function(err, result) {
        var numbers = new Array;
        if (result) result.forEach(function(obj) {
            if (obj && obj.sales_orders) obj.sales_orders.forEach(function (item) {
                if (item && item.number) numbers.push(item.number);
            });
        });
        anomalyManager.getCustomerProvisioningReport({
                "numbers" : numbers,
                "sendEmail" : true,
                "courier" : courier,
                }, (err, result) => {
            if (err) {
                common.error('customerProvisioningReport() Error in scheduler: ', err);
            }
            updateScheduler(tEvent._id, err, result);
            callback(undefined, { "id" : tEvent._id, "processed" : result });
        })
    })
}

function voicemailTest(tEvent, callback) {
    exec(`${__scripts}/internal/voicemail_test.js`, ()=>{});
    updateScheduler(tEvent._id, undefined, 'OK');
    callback(undefined, { id: tEvent._id, processed: 'OK' });
}

function updateScheduler(id, error, result) {
    if (id) {
        var color = error ? 'red' : 'green';
        var message = error && error.message ? error.message : error ? error : undefined;
        db.scheduler.update({ '_id' : id }, {
            '$set' : {
                'processed' : { 'error' : message, 'result' : result },
                'color' : color,
                'myfqdn' : config.MYFQDN
            }
        });
    }
}
