const db = require(`${__lib}/db_handler`);
const config = require(`${__base}/config`);
const ecSyncManager = require(`${__manager}/account/ecSyncManager`);

class sync {

    static detail(tEvent, callback) {
        const handleResult = function(err, result) {
            const error = (err && (typeof(err) == 'object')) ? err.message :
                err ? err : undefined;
            if (tEvent && tEvent._id) {
                const color = err ? 'red' : 'green';
                db.scheduler.update({ _id: tEvent._id }, {
                    '$set': { processed: result, error, color, myfqdn: config.MYFQDN }
                });
            }
            callback(undefined, { id: tEvent._id, error, processed: result });
        }
        if (tEvent.activity == 'elitecoreCustomerDetails') ecSyncManager.elitecoreCustomerDetails(handleResult);
        else handleResult(new Error('Unknown Activity'));
    }

}

module.exports = sync;
