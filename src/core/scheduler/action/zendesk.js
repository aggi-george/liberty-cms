var config = require('../../../../config');
var db = require('../../../../lib/db_handler');
var zendeskTicketsManager = require('../../manager/zendesk/zendeskTicketsManager') 

//export the function
exports.zendeskTicketsRun = function (tEvent, callback) {
	startTime = Math.round(tEvent.start / 1000)-24*60*60;
	zendeskTicketsManager.getZendeskTickets(startTime, function(err, result) {
        if (err) {
            var errorObj = {"error": err.message, result: result};
            db.scheduler.update({"_id": tEvent._id}, {"$set": {"processed": errorObj, "color": "red", "myfqdn" : config.MYFQDN}});
            callback(undefined, errorObj);
        } else {
            db.scheduler.update({"_id": tEvent._id}, {"$set": {"processed": result, "color": "green", "myfqdn" : config.MYFQDN}});
            callback(undefined, {"id": tEvent._id, "processed": result});
        }
    });
}