class Utils {

    static prefixes(str, num, quoted) {
        let out = '';
        str.split('').every(function (ch, idx) {
            if (quoted) {
                out += `'${str.substring(0, idx+1)}',`;
            } else {
                out += `${str.substring(0, idx+1)},`;
            }
            if (idx + 1 >= num) return false;
            else return true;
        });
        return out.slice(0, -1);
    }

}

module.exports = Utils;
