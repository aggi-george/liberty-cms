const apn = require('apn');
const fs = require('fs');
const common = require(`${__lib}/common`);
const config = require(`${__base}/config`);
const LOG = config.LOGSENABLED;

const path = `${__lib}/ios_certificates`;
const gentwoPath = `${path}/com.circles.gentwo`;
const selfcarePath = `${path}/com.circles.selfcare`;
const cert = 'cert.pem';
const key = 'key.pem';
const scert = 'scert.pem';
const skey = 'skey.pem';
const vcert = 'vcert.pem';
const vkey = 'vkey.pem';

const certs = {
    gentwo: {
        prod: {
            cert: fs.readFileSync(`${gentwoPath}/prod/${cert}`),
            key: fs.readFileSync(`${gentwoPath}/prod/${key}`),
            scert: fs.readFileSync(`${gentwoPath}/prod/${scert}`),
            skey: fs.readFileSync(`${gentwoPath}/prod/${skey}`),
            vcert: fs.readFileSync(`${gentwoPath}/prod/${vcert}`),
            vkey: fs.readFileSync(`${gentwoPath}/prod/${vkey}`),
        },
        qa: {
            cert: fs.readFileSync(`${gentwoPath}/qa/${cert}`),
            key: fs.readFileSync(`${gentwoPath}/qa/${key}`),
            scert: fs.readFileSync(`${gentwoPath}/qa/${scert}`),
            skey: fs.readFileSync(`${gentwoPath}/qa/${skey}`),
            vcert: fs.readFileSync(`${gentwoPath}/qa/${vcert}`),
            vkey: fs.readFileSync(`${gentwoPath}/qa/${vkey}`),
        },
        staging: {
            cert: fs.readFileSync(`${gentwoPath}/staging/${cert}`),
            key: fs.readFileSync(`${gentwoPath}/staging/${key}`),
            scert: fs.readFileSync(`${gentwoPath}/staging/${scert}`),
            skey: fs.readFileSync(`${gentwoPath}/staging/${skey}`),
            vcert: fs.readFileSync(`${gentwoPath}/staging/${vcert}`),
            vkey: fs.readFileSync(`${gentwoPath}/staging/${vkey}`),
        },
        develop: {
            cert: fs.readFileSync(`${gentwoPath}/develop/${cert}`),
            key: fs.readFileSync(`${gentwoPath}/develop/${key}`),
            scert: fs.readFileSync(`${gentwoPath}/develop/${scert}`),
            skey: fs.readFileSync(`${gentwoPath}/develop/${skey}`),
            vcert: fs.readFileSync(`${gentwoPath}/develop/${vcert}`),
            vkey: fs.readFileSync(`${gentwoPath}/develop/${vkey}`),
        },
    },
    
    selfcare: {
        prod: {
            cert: fs.readFileSync(`${selfcarePath}/prod/${cert}`),
            key: fs.readFileSync(`${selfcarePath}/prod/${key}`),
            scert: fs.readFileSync(`${selfcarePath}/prod/${scert}`),
            skey: fs.readFileSync(`${selfcarePath}/prod/${skey}`),
        },
        qa: {
            cert: fs.readFileSync(`${selfcarePath}/qa/${cert}`),
            key: fs.readFileSync(`${selfcarePath}/qa/${key}`),
            scert: fs.readFileSync(`${selfcarePath}/qa/${scert}`),
            skey: fs.readFileSync(`${selfcarePath}/qa/${skey}`),
        },
        staging: {
            cert: fs.readFileSync(`${selfcarePath}/staging/${cert}`),
            key: fs.readFileSync(`${selfcarePath}/staging/${key}`),
            scert: fs.readFileSync(`${selfcarePath}/staging/${scert}`),
            skey: fs.readFileSync(`${selfcarePath}/staging/${skey}`),
        },
        develop: {
            cert: fs.readFileSync(`${selfcarePath}/develop/${cert}`),
            key: fs.readFileSync(`${selfcarePath}/develop/${key}`),
            scert: fs.readFileSync(`${selfcarePath}/develop/${scert}`),
            skey: fs.readFileSync(`${selfcarePath}/develop/${skey}`),
        },
    }
};

function getProvider(release, appType, options) {
    if (!certs[appType] || !certs[appType][release]) return undefined;
    let production = options && options.sandbox ? false : true;
    
    let providerCert = certs[appType][release].cert;
    let providerKey = certs[appType][release].key;
    if (options.vapn) {
        providerCert = certs[appType][release].vcert;
        providerKey = certs[appType][release].vkey;
        production = true;
    }   
    return new apn.Provider({ cert: providerCert, key: providerKey, production });
}

class ApplePush {

    static send(flavor, token, data, options, callback) {
        let arr = flavor.split('.');
        const appType = arr[2];
        const release = arr[3] ? arr[3] : 'prod';
        const sandbox = arr[4] && arr[4] == 'sandbox' ? true : false;  
        const vapn = options && options.platform == 'vapn' ? true : false;

        const note = new apn.Notification();
        note.expiry = Math.floor(Date.now() / 1000) + 3600; // Expires 1 hour from now.
        note.category = data.mime_type;
        note.topic = flavor;

        if (options && options.badge) {
            note.badge = options.badge;
        }
        if(data.serviceInstanceNumber){
            note.serviceInstanceNumber = data.serviceInstanceNumber;
        }

        //adding payload
        var keys = Object.keys(data);
        keys.forEach((key) => {
            note.payload[`acme-${key}`] = data[key];
        });

        let handleResult = (response, provider) => {
            provider.shutdown();
            const err = response && (
                (response.failed && response.failed.length > 0) ||
                (response.status || response.error)) ? response : undefined;
            const result = response;
            if (err) {
                common.error('ApplePush error', `err=${JSON.stringify(err)} result=${JSON.stringify(result)}`);
                return callback(new Error(JSON.stringify(err)));
            }
            if (LOG) common.log('ApplePush result', result);
            callback(undefined, result);
        };

        if (vapn) {
            if (LOG) common.log('ApplePush send', `send vapn, release=${release} push_token=${token}`);
            note.alert = 'voip';
            const provider = getProvider(release, appType, { vapn });
            if (provider) provider.send(note, token).then((result) => { handleResult(result, provider) });
            else callback(new Error('Unknown Provider'));
        } else if (options.platform == 'apn') {
            if (data.mime_type == common.MIME_TYPE_TEXT) {
                note.alert = `${data.title}: ${data.text}`;
            /*} else if (data.mime_type == common.MIME_TYPE_BONUS) {
                note.alert = data.text;
            } else if (data.mime_type == common.MIME_TYPE_DATA_WARNING) {
                note.alert = data.text;
            } else if (data.mime_type == common.MIME_TYPE_BONUS_DATA) {
                note.alert = data.text;*/
            } else if ( data.text && (data.text != '') ) {
                note.alert = data.text;
            } else {
                note.alert = {
                    'loc-key': data.loc_key,
                    'loc-args': [data.sender]
                };
            }
            if (LOG) common.log('ApplePush send', `send apn, release=${release} push_token=${token}`);
            const provider = getProvider(release, appType, { sandbox });
            if (provider) provider.send(note, token).then((result) => { handleResult(result, provider) });
            else callback(new Error('Unknown Provider'));
        } else {
            const err = `platform is not supported ${flavor} ${options ? options.platform : 'undefined'}`;
            common.error('ApplePush send', err);
            return callback(new Error(err));
        }
    }

}

module.exports = ApplePush;
