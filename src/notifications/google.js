const gcm = require('node-gcm');
const common = require(`${__lib}/common`);
const config = require(`${__base}/config`);

const LOG = config.LOGSENABLED;

class GooglePush {

    static send(gcm_regID, data, options, callback) {
        let message = new gcm.Message();
        const talkSender = new gcm.Sender('AIzaSyBsVU0cdUqMwEoJpLVW9tSbQzOdad3n3M8');
        const careSender = new gcm.Sender('AIzaSyBonEEWNjB7pWzFg_sfbL-I6MYD_I5t6yg');
        let regIds = [];

        if (data.text && !data.message) {
            data.message = data.text;
        }

        message.addData(data);
        regIds.push(gcm_regID);
        const sender = options && options.app_type == 'gentwo' ? talkSender :
            options && options.app_type == 'selfcare' ? careSender : undefined;
        if (sender) sender.sendNoRetry(message, regIds, (err, result) => {
            if (LOG) common.log(`gcm_send ${regIds.length}`, `result=${JSON.stringify(result)}`);
            callback(err, result);
        });
    }

}

module.exports = GooglePush;
