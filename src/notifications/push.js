const google = require('./google');
const apple = require('./apple');
const common = require(`${__lib}/common`);
const db = require(`${__lib}/db_handler`);
const config = require(`${__base}/config`);

const LOG = config.LOGSENABLED;

class Push {

    static sendWithBadge(id, data, platform, flavor, app_type, badge) {
        if (!app_type) app_type = 'gentwo';    // backwards compatibility with old behavior
        if (platform == 'gcm') {
            google.send(id, data, { app_type }, () => { });
        } else if ((platform == 'apn') || (platform == 'vapn')) {
            if ( (app_type == 'selfcare') && (flavor == '') ) flavor = 'com.circles.selfcare.sandbox';    // failsafe
            apple.send(flavor, id, data, { platform, app_type, badge }, () => { });
        }
    }

    static send(id, data, platform, flavor, app_type) {
        Push.sendWithBadge(id, data, platform, flavor, app_type);
    }

    static sendByNumber(prefix, number, app_type, badge, msg, callback) {
        const fullnum = `${prefix}${number}`;
        const q = `SELECT d.token, d.flavor, d.type token_type FROM app a, device d WHERE a.id=d.app_id AND d.status='A'
            AND a.number=${db.escape(fullnum)} AND a.app_type=${db.escape(app_type)}`;
        if (LOG) common.log('sendByNumber', q);
        db.query_noerr(q, (rows) => {
            let pushMessages = [];
            if (rows) rows.forEach((push) => {
                pushMessages.push({
                    token: push.token,
                    tokenType: push.token_type,
                    flavor: push.flavor,
                    appType: app_type,
                    badge: badge
                });
                Push.sendWithBadge(push.token, msg, push.token_type,
                    push.flavor, app_type, badge);
            });
            if (callback) callback(undefined, {
                message: msg,
                count: pushMessages.length,
                list: pushMessages
            });
        });
    }

}

module.exports = Push;
