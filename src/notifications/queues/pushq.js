var config = require('../../../config');
var common = require('../../../lib/common');
var db = require('../../../lib/db_handler');

var LOG = config.LOGSENABLED;

// exports

exports.send = send;

// functions

function send(push, callback) {
    var job = db.queue.create('push', push)
        .events(false)
        .removeOnComplete(true)
        .delay(0)
        .attempts(2)
        .backoff({"delay": 10 * 1000, "type": "fixed"})
        .save(function (err) {
            if (LOG) common.log("push job", job.id);
            if (callback) callback(err, job.id);
        });
}
