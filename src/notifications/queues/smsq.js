var config = require('../../../config');
var common = require('../../../lib/common');
var db = require('../../../lib/db_handler');

var LOG = config.LOGSENABLED;

// exports

exports.send = send;

// functions

function send(sms, callback) {
    var job = db.queue.create('sms', sms)
        .events(false)
        .removeOnComplete(true)
        .delay(0)
        .attempts(2)
        .backoff({"delay": 10 * 1000, "type": "fixed"})
        .save(function (err) {
            if (LOG) common.log("sms", job.id);
            if (callback) callback(err, job.id);
        });
}
