var soap = require('soap');
var MD5 = require('MD5');
var js2xmlparser = require("js2xmlparser");
var db = require('../../lib/db_handler');
var config = require('../../config');
var common = require('../../lib/common');

var logsEnabled = true;

var soapClient;

var js2xmloptions = { "declaration" : { "include" : false }, "prettyPrinting" : { "enabled" : false } };

var url_options = {
	// this is to add attributes to a header but is not used for that purpose 
	// it has a side effect of suppressing RequestHeaderElement in soap:Body
	attributesKey: 'attribute'
}
if (config.SDPHOST) {
	soap.createClient("http://" + config.SDPHOST + "/services/smsSendService?wsdl" , url_options, function(err, client) {
		if ( err || !client ) {
			common.error("WSDL Error", "smsSendService");
		} else {
			soapClient = client;
		}
	});
}

//exports

exports.send = send;

//functions

function soapCall(method, parameters, start, callback) {
	if (!soapClient) {
		return callback(new Error("Soap client is not initialised"));
	}

	soapClient.wsdl.definitions.xmlns.ns3 = 'http://hsenidmobile/sdp/ws/sms/wsdl/application';	// add to envelope
	soapClient.wsdl.definitions.xmlns.ns5 = 'http://hsenidmobile/sdp/ws/sms/wsdl/common';	// add to envelope
	soapClient.wsdl.xmlnsInEnvelope = soapClient.wsdl._xmlnsMap();
	var soapHeader = {"ns1:Auth": {"acm:Password": MD5("LWsdp71")}, "ns1:Application": {"ns3:ApplicationId": "PD_CS_L0428"}};
	soapHeader['@'] = {		// add these attributes to RequestHeaderElement
		"xmlns:acm": "http://hsenidmobile/sdp/ws/sms/wsdl/acm",
		"xmlns:tns": "http://hsenidmobile/sdp/ws/sms/wsdl/send",
		"xmlns:ns1": "http://hsenidmobile/sdp/ws/sms/wsdl/header",
		"xmlns:ns2": "http://hsenidmobile/sdp/ws/sms/wsdl/error",
		"xmlns:ns3": "http://hsenidmobile/sdp/ws/sms/wsdl/application",
		"xmlns:ns5": "http://hsenidmobile/sdp/ws/sms/wsdl/common"
	};
	soapClient.addSoapHeader(js2xmlparser('ns1:RequestHeaderElement', soapHeader, js2xmloptions));
	soapClient[method](parameters, function (err, result) {
		if (err) {
			common.error(method, JSON.stringify(err));
			callback(err);
		} else {
			var end = new Date();
			result.querytime = end - start;
			callback(undefined, result);
		}
	}, { timeout: 25 });
}

function send(from, to, msg, callback) {
	var start = new Date();
	var method = "SendSms";
	var args = new Object;
	args["tns:Destinations"] = [{ "tns:destination" : to }];
	var segments = new Array;
	if ( msg.length <= 160 ) {
		segments.push({ "ns5:Message" : msg });
	} else {
		var max = 153;
		var chunks = Math.ceil(msg.length/max);
		for (var i=0; i<msg.length; i+=max) {
			var j = (i/max) + 1;
			/*	HEX
				05 - total length  of the header
				00 ‐ header type  (In this case it says Concat)
				03 ‐ concat header length
				12 ‐ one octet length message reference number ??? why 71???
				02 ‐ total number of segments
				01 ‐ index of current message  
			*/
			var header = "05000371";
			var count = ("0" + chunks.toString(16)).slice(-2);
			var idx = ("0" + j.toString(16)).slice(-2);
			segments.push({ "ns5:Header" : header + "" + count + "" + idx, 
					"ns5:Message" : msg.substring(i, (max*j))
				});
		}
	}
	args["tns:Segments"] = { "tns:segment" : segments };
	args["tns:SourceAddress"] = from;
	args["tns:StatusRequest"] = "true";
	args["tns:Encoding"] = "0";
	var parameters = js2xmlparser('tns:SendSmsRequestElement', args, js2xmloptions)
	soapCall(method, parameters, start, callback);
}
