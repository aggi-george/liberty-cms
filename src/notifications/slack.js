var request = require('request');
var config = require('../../config');

exports.send = function (data, callback) {
    if(!data.channel){
        data.channel = config.DEV ? "#rikererrors" : "#kirk";
    }
    if ( typeof(data) != "object" ) return false;
    if (!data.channel || !data.username || !data.text) return false;
    if (!data.icon_emoji) data.icon_emoji = "ghost";
    var url = "https://hooks.slack.com/services/" + config.SLACK.channel + "/" + config.SLACK.id + "/" + config.SLACK.key;
    request({
        uri: url,
        method: "post",
        form: { "payload" : JSON.stringify(data) },
        timeout: 20000
    }, function(err, response, body) {
        if (callback) callback(err, body);
    });
}

