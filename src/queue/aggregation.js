const db = require(`${global.__lib}/db_handler`);
const common = require(`${global.__lib}/common`);
const config = require(`${global.__base}/config`);
const logger = require(`${global.__queue}/logger`); 
const subscriptionManager = require(`${global.__manager}/bonus/subscriptionManager`);

const LOG = config.LOGSENABLED;

class Aggregation {

    static init() {
        db.queue.process('aggregation', (job, done) => {
            const startTime = new Date();
            if (LOG) common.log('QueueManager[aggregation]', `process, ${JSON.stringify(job.data)}`);

            const info = common.safeParse(job.data);
            const prefix = info.prefix;
            const number = info.number;
            const initiator = info.initiator;
            const executionKey = info.executionKey;

            if (!prefix || !number) {
                return done(new Error('Invalid params'));
            }
            new Promise((resolve, reject) => {
                const timeout = setTimeout(() => {
                    common.error('QueueManager[aggregation]', `timeout, ${JSON.stringify(job.data)}`);
                    reject(new Error('Timeout'));
                }, 60 * 1000);

                subscriptionManager.updateNextBillBonuses(prefix, number, initiator, executionKey, (err, result) => {
                    clearTimeout(timeout);
                    const queryTime = new Date() - startTime;
                    logger.saveLogs('aggregation', err, job.data, result, queryTime, () => {

                        // extra timeout to make process slower
                        if (err && err.message == 'Re-subscription is in progress') {
                            return resolve({reSubscriptionInProgress: true});
                        }
                        setTimeout(() => {
                            if (err) reject(err);
                            else resolve(result);
                        }, 1500);
                    });
                });
            }).then((result) => {
                done(undefined, result);
            }).catch((err) => {
                done(err);
            });
        });
    }

}

module.exports = Aggregation;
