const db = require(`${global.__lib}/db_handler`);
const common = require(`${global.__lib}/common`);
const config = require(`${global.__base}/config`);
const paymentManager = require(`${global.__manager}/account/paymentManager`);
const logger = require('./logger');

const LOG = config.LOGSENABLED;

class CreditCapPayment {

    static init() {
        db.queue.process('creditCapPayment', 2, (job, done) => {
            const startTime = new Date();
            if (LOG) common.log('QueueManager[creditCapPayment]', `process, ${JSON.stringify(job.data)}`);

            const info = common.safeParse(job.data);
            const billingAccountNumber = info.billingAccountNumber;
            const options = info.options;

            new Promise((resolve, reject) => {
                const timeout = setTimeout(() => {
                    common.error('QueueManager[creditCapPayment]', `timeout, ${JSON.stringify(job.data)}`);
                    reject(new Error(`Cap Payment operation for ${billingAccountNumber} timed out`));
                }, 120 * 1000);
 
                paymentManager.makeCreditCapPaymentByBA(billingAccountNumber, options, (err, result) => {
                    clearTimeout(timeout);
                    const queryTime = new Date() - startTime;
                    logger.saveLogs('creditCapPayment', err, job.data, result, queryTime, () => {
                        if (err && err.status == 'ERROR_OPERATION_LOCKED') {
                            return resolve({operationInProgress: true});
                        } else if (err && (err.status == 'ERROR_PAYMENT_NOT_ALLOWED'
                            || err.status == 'ERROR_PAYMENT_NOT_REQUIRED')) {
                            return resolve({operationBlocked: true});
                        }

                        if (err) reject(err);
                        else resolve(result);
                    });
                });
            }).then((result) => {
                done(undefined, result);
            }).catch((err) => {
                done(err);
            });
        });
    }

}

module.exports = CreditCapPayment;
