const fs = require('fs');
const nodemailer = require('nodemailer');
const nodemailerSesTransport = require('nodemailer-ses-transport');
const db = require(`${global.__lib}/db_handler`);
const common = require(`${global.__lib}/common`);
const config = require(`${global.__base}/config`);
const logger = require('./logger');

const LOG = config.LOGSENABLED;

const options = {
    accessKeyId: config.AWS_SES_KEY,
    secretAccessKey: config.AWS_SES_SECRET,
    rateLimit: 7,
    maxConnections: 3
};

const sesTransport = nodemailer.createTransport(nodemailerSesTransport(options));

class Email {

    static init() {
        db.queue.process('email', 7, (job, done) => {
            const startTime = new Date();
            if (LOG) common.log('QueueManager[email]', `process email ${job.data.to} ${job.data.subject}`);
        
            new Promise((resolve, reject) => {
                const timeout = setTimeout(() => {
                    common.error('QueueManager[email]', `timeout sending ${job.data.to} ${job.data.subject}`);
                    reject(new Error('Timeout'));
                }, 90 * 1000);
        
                send(job.data, (err, result) => {
                    clearTimeout(timeout);
                    const queryTime = new Date() - startTime;
                    logger.saveLogs('email', err, job.data, result, queryTime, () => {
                        if (err) reject(err);
                        else resolve(result);
                    });
                });
            }).then((result) => {
                done(undefined, result);
            }).catch((err) => {
                done(err);
            });
        });
    }

}

function send(data, callback) {
    const mail = common.safeParse(data);

    // load file if it is not yet loaded
    if (!mail.attachments && mail.files && mail.files.length > 0) {

        if (!mail.fileNames || mail.fileNames.length == 0) {
            common.error('QueueAction[email]', `send: file names array is empty, email=${mail.to}`);

            if (callback) callback(new Error('No file name provided'));
            return;
        }

        //added support for 1 file only
        const filePath = mail.files[0];
        const fileName = mail.fileNames[0];

        try {
            const content = fs.readFileSync(filePath);
            mail.attachments = [{
                filename: fileName,
                content
            }];
        } catch (e) {
            common.error('QueueAction[email]', `send: email is has invalid attachments,
                can not load file, email=${mail.to}, filePath=${filePath}, error=${e.message}`);

            if (callback) callback(new Error(`Can not load file (${JSON.stringify(e)})`));
            return;
        }
    } else if (mail.attachments) {
        mail.attachments = mail.attachments.filter((att) => {
            if (att.content && (att.content.type == 'Buffer')) {
                att.content = new Buffer(att.content);
                return att;
            } else {
                common.error('QueueAction[email]', `email has invalid attachments,
                    can not load buffer, email=${mail.to}`);
                return undefined;
            }
        });

        if (!mail.attachments || mail.attachments.length == 0) {
            common.error('QueueAction[email]', `send: attachments array is empty, email=${mail.to}`);
            if (callback) callback(new Error('No attachments loaded'));
            return;
        }
    }

    //if (LOG) common.log('QueueAction[email]', 'FAKE EMAIL START !!!!!!!!!!!!!!!!!!!!!!');
    //setTimeout(function () {
    //    if (LOG) common.log('QueueAction[email]', 'FAKE EMAIL END !!!!!!!!!!!!!!!!!!!!!!');
    //    var response = {message: 'FAKE EMAIL'};
    //
    //    if (LOG) common.log('QueueAction[email]', 'email is sent, to=' + mail.to + ', activity=' + mail.activity + ', subject='
    //    + mail.subject + ', response=' + JSON.stringify(response));
    //    if (callback) callback(false, response);
    //    return;
    //}, 500);

    sesTransport.sendMail(mail, (error, response) => {
        if (error) {
            common.error('QueueAction[email]', `email is NOT sent, to=${mail.to}, activity=${mail.activity}, subject=${mail.subject}, error=${error.message}`);
            if (callback) callback(new Error(`Failed to send email, ses transport error, ${error.message}`));
            return;
        }

        if (LOG) common.log('QueueAction[email]', `email is sent, to=${mail.to}, activity=${mail.activity}, subject=${mail.subject}, response=${JSON.stringify(response)}`);
        if (callback) callback(undefined, response);
        return;
    });
}

module.exports = Email;
