const bluebird = require('bluebird');
const common = require(`${global.__lib}/common`);
const db = require(`${global.__lib}/db_handler`);
const config = require(`${global.__base}/config`);
const connector = require(`${global.__core}/manager/hlr/connector`);
const hlrManager = require(`${global.__core}/manager/hlr/hlrManager`);
const profileManager = require(`${global.__core}/manager/profile/profileManager`);
const notificationSend = require(`${global.__core}/manager/notifications/send`);

const LOG = config.LOGSENABLED;

const pending = [ 'Received', 'IL Sent', 'IL New', 'Operator New', 'Operator New Failed' ];

class HLR {

    static init() {
        db.queue.process('hlr', 3, (job, done) => {
            if (LOG) common.log('QueueManager[hlr]', JSON.stringify(job.data));
    
            return new bluebird.Promise((resolve, reject) => {
                const timeout = setTimeout(() => {
                    common.error('QueueManager[hlr]', `timeout, ${JSON.stringify(job.data)}`);
                    reject(new Error('Timeout'));
                }, 30 * 1000);
    
                send(job.data, (err, result) => {
                    clearTimeout(timeout);
                    if (err) reject(err);
                    else resolve(result);
                });
            }).then((result) => {
                done(undefined, result);
            }).catch((err) => {
                done(err);
            });
        });
    }

}

function send(data, callback) {
    if (data.action) return put(data, callback);
    const find = {
        number: data.msisdn,
        serviceInstanceNumber: data.serviceInstanceNumber,
        status: 'IN_PROGRESS',
        m1RefId: { '$gt': data.m1RefId }
    };
    if ( (data.method == 'AddDataRoaming') || (data.method == 'RemoveDataRoaming') ) {
        find['$or'] = [ { method: 'AddDataRoaming' }, { method: 'RemoveDataRoaming' } ];
    } else if ( (data.method == 'AddAutoRoaming') || (data.method == 'RemoveAutoRoaming') ) {
        find['$or'] = [ { method: 'AddAutoRoaming' }, { method: 'RemoveAutoRoaming' } ];
    } else if ( (data.method == 'Suspension') || (data.method == 'Reconnection') ) {
        find['$or'] = [ { method: 'Suspension' }, { method: 'Reconnection' } ];
        data.ignoreSave = true;
    } else if ( (data.method == 'AddCallerIDNonDisplay') || (data.method == 'RemoveCallerIDNonDisplay') ) {
        find['$or'] = [ { method: 'AddCallerIDNonDisplay' }, { method: 'RemoveCallerIDNonDisplay' } ];
        data.ignoreSave = true;
    } else if ( (data.method == 'AddCallerID') || (data.method == 'RemoveCallerID') ) {
        find['$or'] = [ { method: 'AddCallerID' }, { method: 'RemoveCallerID' } ];
        data.ignoreSave = true;
    } else {
        const error = 'Unknown Method';
        common.error('hlr send', error);
        return callback(new Error(error));
    }
    db.addonlog.find(find, { sort: [ 'ts', 'desc' ], limit: 1 }).toArray((err, docs) => {
        if (err || (docs && docs.length == 0)) return get(data, callback);
        if (LOG) common.log('hlr ignore', JSON.stringify(data));
        logUpdate(data.m1RefId, data.serviceInstanceNumber, data.msisdn, data.type, new Error('IGNORED'), undefined, () => {
            callback(undefined, { code: 0, ignored: true });
        });
    });
}

function get(data, callback) {
    connector.CheckOrderStatus(data, (err, result) => {
        logUpdate(data.m1RefId, data.serviceInstanceNumber, data.msisdn, data.type, err, result, (error, logStatus) => {
            if ( (logStatus == 'OK') || (logStatus=='ERROR') ) {
                updateProfile(data.m1RefId, logStatus, data.suppressNotify, data.ignoreSave);
            }
            if ( err || !result || pending.indexOf(result.status) > -1) return callback('retry');
            if ( result.status == 'TR Completed' ) {
                callback(undefined, { 'code' : 1, 'result' : result });
            } else if ( result.status == 'TR Failed' ) {
                callback(undefined, { 'code' : -1, 'result' : result });
            } else if ( result.status == '' ) {
                callback(undefined, { 'code' : -1, 'result' : result });
            } else {
                common.error('checker', `Unknown End State ${JSON.stringify(result)}`);
                callback(undefined, { 'code' : 0, 'result' : result });
            }
        });
    });
}

function put(args, callback) {
    connector.VASMaintenance(args, (err, result) => {
        if (err) return callback(new Error('Error - Invalid VAS result'));
        else if (!result || !result.status) return (new Error('Error - Invalid VAS result'));
        callback(undefined, { 'args' : args, 'result' : result });
        let checkArgs = JSON.parse(JSON.stringify(args));
        checkArgs.m1RefId = result.m1RefId;
        startCheck(checkArgs, 60000, () => {});
        profileManager.hlrLoadSettings(args.serviceInstanceNumber, (oError, oResult) => {
            const optionId = (args.action.substring(args.action.length-11, args.action.length) == 'AutoRoaming') ?
                'roaming_auto' :
                (args.action.substring(args.action.length-11, args.action.length) == 'DataRoaming') ?
                    'roaming_data' : undefined;
            if (optionId) {
                if (oResult) {
                    args.optionId = optionId;
                    args.optionValue = oResult[optionId];
                } else {
                    args.optionId = optionId;
                    args.optionValue = '0';
                }
                profileManager.saveSetting(args.prefix, args.msisdn, 'selfcare', optionId,
                    'String', result.m1RefId, undefined, () => { });
            }
            hlrManager.logTransaction(args, err, result);
        });
    });
}

function startCheck(args, msDelay, callback) {
    args.method = args.action;
    args.action = undefined;
    const job = db.queue.create('hlr', args)
        .events(false)
        .removeOnComplete(true)
        .delay(msDelay)
        .attempts(60)
        .backoff({ 'delay' : 60*1000, 'type' : 'fixed' })
        .save(() => { if (LOG) common.log('startCheck job', job.id); });
    db.queue.on('job failed', (id) => {
        if ( job && (job.id != id) ) return;
        const error = 'Error - Invalid Checker result';
        callback(new Error(error));
    });
    db.queue.on('job complete', (id, result) => {
        if ( job && (job.id != id) ) {
            return;
        }
        callback(undefined, result);
    });
}

function logUpdate(m1RefId, serviceInstanceNumber, number, type, error, result, callback) {
    let logStatus = 'IN_PROGRESS';
    if (error && error.message == 'IGNORED') {
        logStatus = error.message;
    } else if ( result && (pending.indexOf(result.status) == -1) ) {
        if (result.status == 'TR Completed') {
            logStatus = 'OK';
        } else {
            logStatus = 'ERROR';
            common.error('logUpdate - Operation failed', `${serviceInstanceNumber} ${number} ${type} ${m1RefId}`);
            db.cache_put('cache', `roaming_error_${serviceInstanceNumber}_${number}_${type}`, 1, 60 * 60 * 1000);
        }
    }
    db.addonlog.update({ 'm1RefId' : m1RefId },
        { '$set' : {
            'status' : logStatus,
            'result' : result,
            'error' : (error && error.message ? error.message : error),
        }, '$push' : {
            'trace' : {
                'ts' : new Date().getTime(),
                'myfqdn' : config.MYFQDN,
                'error' : (error && error.message ? error.message : error),
                'result' : result
            }
        }
        }, () => {
            callback(undefined, logStatus);
        });
}

function updateProfile(m1RefId, logStatus, suppressNotify, ignoreSave) {
    db.addonlog.findOne({ 'm1RefId' : m1RefId }, (err, doc) => {
        if (!doc || !doc.method) return;
        const value = (doc.method.substring(0, 3) == 'Add') ? '1' : 
            (doc.method.substring(0, 6) == 'Remove') ? '0' : '1';    // watch out!
        const saveProfile = (saveValue, cb) => {
            if (ignoreSave) {
                if (LOG) common.log('updateProfile', `Ignore save ${JSON.stringify(doc.args)}`);
                return cb();
            }
            profileManager.saveSetting(doc.args.prefix, doc.number, 'selfcare', doc.args.optionId,
                'String', saveValue, undefined, cb);
        };
        if (logStatus == 'OK') {
            saveProfile(value, () => {
                if (!suppressNotify) notifyRoaming(doc.serviceInstanceNumber, doc.number, doc.type);
            });
        } else {
            if (doc.args.optionValue == null) doc.args.optionValue = 0;    // assume disabled on provisioning
            saveProfile(doc.args.optionValue, () => {
                if ( (doc.args.optionValue == '0') || (doc.args.optionValue == '1') ) {
                    if (!suppressNotify) notifyRoaming(doc.serviceInstanceNumber, doc.number, doc.type);
                } else {
                    if (LOG) common.log('updateProfile', `Old job is stuck in pending state - restarting m1RefId=${doc.args.optionValue}`);
                    db.addonlog.findOne({ 'm1RefId' : doc.args.optionValue }, (err, old) => {
                        if (old && old.args) {
                            let args = old.args;
                            args.m1RefId = doc.args.optionValue;
                            args.suppressNotify = true;
                            args.ignoreSave = true;
                            startCheck(old.args, 60000, () => { });
                        } else {
                            common.error('updateProfile', `Old m1refId not found ${doc.args.optionValue} ${doc.number}`);
                        }
                    });
                }
            });
        }
    });
}

function notifyRoaming(serviceInstanceNumber, number, type) {
    if (LOG) common.log('notifyRoaming', `${serviceInstanceNumber} ${number} ${type}`);
    db.cache_lock('cache', `roaming_notify_${serviceInstanceNumber}_${number}_${type}`, 1, 120000, (lock) => {
        if (lock) return;
        db.addonlog.find({ serviceInstanceNumber, number, status: 'IN_PROGRESS' }).toArray((error, docs) => {
            if (error) return common.error('notifyRoaming find error', `${serviceInstanceNumber} - ${number}`);
            else if (docs && (docs.length > 1)) return;
            profileManager.hlrLoadSettings(serviceInstanceNumber, (err, result) => {
                if (err || !result) {
                    return common.error('notify profileManager.loadSetting', JSON.stringify(err));
                }
                let data;
                let auto;
                if ( (result.roaming_auto == '1') && (result.roaming_data == '1') ) {
                    data = true;
                    auto = true;
                } else if ( (result.roaming_auto == '1') && (result.roaming_data == '0') ) {
                    data = false;
                    auto = true;
                } else if ( result.roaming_auto == '0' ) {
                    data = false;
                    auto = false;
                }
                if ( (typeof(data) == 'undefined') || (typeof(auto) == 'undefined') ) {
                    return common.error(`notifyRoaming error ${serviceInstanceNumber} ${number} ${type}`, JSON.stringify(result));
                }
                let activity = (!auto && !data) ? 'roaming_off' :
                    (auto && data) ? 'roaming_voice_sms_data_on' :
                        (auto) ? 'roaming_voice_sms_on' : undefined;
                if (activity) {
                    db.cache_get('cache', `roaming_error_${serviceInstanceNumber}_${number}_${type}`, (cache) => {
                        if (cache) {
                            if (LOG) common.log('notifyRoaming', `cache found, operation failed ${serviceInstanceNumber}_${number}_${type}`);
                            activity = 'roaming_change_failure';
                            db.cache_del('cache', `roaming_error_${serviceInstanceNumber}_${number}_${type}`);
                        }
                        const obj = {
                            'teamID' : 5,
                            'teamName' : 'Operations',
                            'activity' : activity,
                            'number' : number,
                            'serviceInstanceNumber' : serviceInstanceNumber
                        };
                        notificationSend.deliver(obj, null, () => { });
                    });
                }
            });
        });
    });
}

module.exports = HLR;
