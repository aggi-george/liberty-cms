const common = require(`${global.__lib}/common`);
const db = require(`${global.__lib}/db_handler`);
const config = require(`${global.__base}/config`);
const notificationSend = require(`${global.__manager}/notifications/send`);
const notificationManager = require(`${global.__manager}/notifications/notificationManager`);

const LOG = config.LOGSENABLED;

class Ingress {

    static init() {
        db.queue.process('ingress', 3, (job, done) => {
            job.data.qJobID = job.id;
            const startTime = new Date();
            if (job.data.isActivityInvalid) {
                const queryTime = new Date() - startTime;
                const err = new Error(`Invalid activity ${job.data.originalActivity}`);
                notificationManager.saveLogs(err, job.data, undefined, queryTime, () => {
                    return done(err);
                });
            }
            if (LOG) common.log('QueueManager[ingress]', `process, ${JSON.stringify(job.data)}`);
        
            new Promise((resolve, reject) => {
                const delay = 60;
                const timeout = setTimeout(() => {
                    const err = new Error(`Timeout ${job.data.activity}`);
                    common.error('QueueManager[ingress] setTimeout', err);
                    return reject(err);
                }, delay * 1000);
        
                handle(job.data, (err, obj, product) => {
                    clearTimeout(timeout);
                    if (err) {
                        if (err.message && err.message.indexOf('Customer not found') > -1) return resolve(obj);    // do not retry
                        else if (err.message != 'autoboost locked') common.error('QueueManager[ingress] handle', err);
                        return reject(err);
                    } else {
                        notificationManager.saveLogs(err, obj, product, (new Date() - startTime), () => {
                            if (obj.isIgnored) {
                                //some notifications can be ignored, refer to IGNORED_ADD_ON_SUBSCRIPTION_PRODUCTS
                                if (LOG) common.log('QueueManager[ingress] handle', `message ignored ${obj.activity}`);
                            } else {
                                notificationSend.deliver(obj, product);
                            }
                            return resolve(obj);
                        });
                    }
                });
            }).then((result) => {
                return done(undefined, result);
            }).catch((err) => {
                notificationManager.saveLogs(err, job.data, undefined, (new Date() - startTime), () => {
                    return done(err);
                });
            });
        });
    }

}

function handle(data, callback) {
    notificationManager.triage(data, (error, obj, product) => {
        if (error) {
            return callback(error, obj, product);
        }

        notificationManager.isAddonNotificationsSuppressed(obj.serviceInstanceNumber, obj.addonname, (supresErr, cache) => {
            if (supresErr) {
                // ignore error, for some notifications
                // add-on name is empty what can cause an error
            }

            if (cache) {
                obj.isIgnored = true;
                obj.suppressReason = cache.reason;
                return callback(null, obj, product);
            }

            if (notificationManager.suppressNotificationsList().indexOf(obj.activity) == -1) {
                return callback(null, obj, product);
            }

            notificationManager.suppressNotificationsStatus(obj.number, obj.serviceInstanceNumber, (cache) => {
                if (cache) {
                    obj.isIgnored = true;
                    obj.suppressReason = cache.reason;
                    return callback(null, obj, product);
                }

                callback(null, obj, product);
            });
        });
    });

}

module.exports = Ingress;
