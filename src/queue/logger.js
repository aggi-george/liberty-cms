const async = require('async');
const db = require(`${global.__lib}/db_handler`);
const common = require(`${global.__lib}/common`);
const config = require(`${global.__base}/config`);

const LOG = config.LOGSENABLED;

class Logger {

    static saveLogs(type, error, data, result, queryTime, callback) {
        const internal = data.type === 'INTERNAL';
        let log = {
            status: !error ? 'OK' : 'ERROR',
            error: error ? error.message : undefined,
            activity: data.activity,
            from: data.from,
            to: data.to,
            subject: data.subject,
            html: data.html,
            files: data.files,
            plain: data.plain,
            notificationId: data.notificationId,
            prefix: data.prefix,
            number: data.number,
            internal: internal,
            queryTime: queryTime,
            customerAccountNumber: data.customerAccountNumber,
            serviceInstanceNumber: data.serviceInstanceNumber,
            result: result
        };
    
        if (type === 'push') {
            log.appType = data.appType;
            if (result) {
                log.countDevices = result.count;
            }
        }
    
        db.notifications_insert(type, log, (err) => {
            if (LOG) {
                let info = JSON.stringify(log);
                if (info.length > 300) info = `${info.substring(0, 300)}.....`;
                common.log(`QueueManager[${type}]`, `log is added to '${type} logbook' collection, activity=${log.activity}, log=${info}`);
            }
    
            const updateSMSRelatedLog = () => {
                db.notifications_logbook.update({notificationId: log.notificationId},
                    {'$set': {statusSMS: log.status, queueSMSError: log.error}}, {}, (err) => {
                        if (err) common.error(`QueueManager[${type}]`, `failed to update SMS related log, error=${err.message}`);
                        if (callback) callback();
                    });
            };
    
            const updatePushRelatedLog = () => {
                if (data.appType === 'selfcare') {
                    db.notifications_logbook.update({notificationId: log.notificationId},
                        {'$set': {statusSelfcare: log.status, queuePushSelfcareError: log.error}}, {}, (err) => {
                            if (err) common.error(`QueueManager[${type}]`, `failed to update selfcare push related log, error=${err.message}`);
                            if (callback) callback();
                        });
                } else if (data.appType === 'gentwo') {
                    db.notifications_logbook.update({notificationId: log.notificationId},
                        {'$set': {statusGentwo: log.status, queuePushGentwoError: log.error}}, {}, (err) => {
                            if (err) common.error(`QueueManager[${type}]`, `failed to update gentwo push related log, error=${err.message}`);
                            if (callback) callback();
                        });
                } else {
                    if (err) common.error(`QueueManager[${type}]`, `failed to save push log, unsupported appType=${data.appType}`);
                    if (callback) callback();
                }
            };
    
            const updateBillRelatedLog = () => {
                db.notifications_bills.update({notificationId: log.notificationId},
                    {'$set': {statusEmail: log.status, queueEmailError: log.error}}, {}, (err) => {
                        if (err) common.error(`QueueManager[${type}]`, `failed to update bill related log, error=${err.message}`);
                        if (callback) callback();
                    });
            };
    
            const updateEmailRelatedLog = () => {
                const setParams = internal
                    ? {statusInternal: log.status, queueInternalError: log.error}
                    : {statusEmail: log.status, queueEmailError: log.error};
    
                db.notifications_logbook.update({notificationId: log.notificationId},
                    {'$set': setParams}, {}, (err) => {
                        if (err) common.error(`QueueManager[${type}]`, `failed to update email related log, error=${err.message}`);
                        updateBillRelatedLog();
                    });
            };
    
            const saveSecondaryNotificationStatuses = () => {
                if (!log.notificationId) {
                    if (callback) callback();
                } else if (type === 'email') {
                    setTimeout(() => {
                        updateEmailRelatedLog();
                    }, 1000);
                } else if (type === 'sms') {
                    setTimeout(() => {
                        updateSMSRelatedLog();
                    }, 1000);
                } else if (type === 'push') {
                    setTimeout(() => {
                        updatePushRelatedLog();
                    }, 1000);
                } else {
                    if (callback) callback();
                }
            };
    
            let tasks = [];
            if (data.updateTriggers && data.updateTriggers.forEach) {
                data.updateTriggers.forEach((item) => {
                    let query = `UPDATE ${item.table} SET `;
                    let hasParams = false;
    
                    const addParam = (key, value) => {
                        if (hasParams) query += ', ';
                        query += `${key}=${db.escape(value)}`;
                        hasParams = true;
                    };
                    if (item.statusField) {
                        addParam(item.statusField, log.status);
                    }
                    if (item.timeField) {
                        addParam(item.timeField, new Date());
                    }
    
                    query += ` WHERE id=${db.escape(item.id)}`;
    
                    if (!hasParams) {
                        return;
                    }
    
                    (function (query) {
                        tasks.push((callback) => {
                            db.query_err(query, (err) => {
                                if (err) common.error(`QueueManager[${type}]`, `failed to update trigger status, error=${err.message}`);
                                callback();
                            });
                        });
                    }(query));
                });
            }
    
            if (tasks.length > 0) {
                async.parallelLimit(tasks, 10, () => {
                    saveSecondaryNotificationStatuses();
                });
            } else {
                saveSecondaryNotificationStatuses();
            }
        });
    }

}

module.exports = Logger;
