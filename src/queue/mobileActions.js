const config = require(`${global.__base}/config`);
const common = require(`${global.__lib}/common`);
const db = require(`${global.__lib}/db_handler`);
const BaseError = require(`${global.__core}/errors/baseError`);
const resourceManager = require(`${global.__res}/resourceManager`);
const addons = require(`${global.__mobile}/account/addons`);
const constants = require(`${global.__core}/constants`);

const LOG = config.LOGSENABLED;

class MobileActions { 

    static init() {
        db.queue.process(constants.MOBILE_ACTIONS.JOB_TYPE_BOOST, function (job, done) {
            if (LOG) common.log(`ActionsManager[${constants.MOBILE_ACTIONS.JOB_TYPE_BOOST}]`, JSON.stringify(job.data));
            const params = job.data ? job.data.params : undefined;
            const actionId = params ? params.actionId : undefined;
        
            new Promise(function (resolve, reject) {
                updateJobProgressState(actionId, 'PROCESSING');
        
                const timeout = setTimeout(function () {
                    common.error(`ActionsManager[${constants.MOBILE_ACTIONS.JOB_TYPE_BOOST}]`,
                        `timeout, ${JSON.stringify(job.data)}`);
                    reject(new Error('Timeout'));
                }, 5 * 60 * 1000);
        
                addons.addBoostHandlerInternal(params, (err, result) => {
                    clearTimeout(timeout);
                    if (err) {
                        updateJobProgressErrorStatus(actionId, err);
                        reject(err);
                    } else {
                        updateJobProgressStatusAndState(actionId, 'DONE', '', result);
                        resolve(result);
                    }
                });
            }).then(function (result) {
                done(undefined, result);
            }).catch(function (err) {
                done(err);
            });
        });
        
        db.queue.process(constants.MOBILE_ACTIONS.JOB_TYPE_PLUS_MODIFICATION, function (job, done) {
            if (LOG) common.log(`ActionsManager[${constants.MOBILE_ACTIONS.JOB_TYPE_PLUS_MODIFICATION}]`,
                JSON.stringify(job.data));
            const params = job.data ? job.data.params : undefined;
            const actionId = params ? params.actionId : undefined;
        
            new Promise(function (resolve, reject) {
                updateJobProgressState(actionId, 'PROCESSING');
        
                const timeout = setTimeout(function () {
                    common.error(`ActionsManager[${constants.MOBILE_ACTIONS.JOB_TYPE_PLUS_MODIFICATION}]`,
                        `timeout, ${JSON.stringify(job.data)}`);
                    reject(new Error('Timeout'));
                }, 5 * 60 * 1000);
        
                addons.generalUpdateInternal(params, (err, result) => {
                    clearTimeout(timeout);
                    if (err) {
                        updateJobProgressErrorStatus(actionId, err);
                        reject(err);
                    } else {
                        updateJobProgressStatusAndState(actionId, 'DONE', '', result);
                        resolve(result);
                    }
                });
            }).then(function (result) {
                done(undefined, result);
            }).catch(function (err) {
                done(err);
            });
        });
        
        db.queue.process(constants.MOBILE_ACTIONS.JOB_TYPE_PLAN_MODIFICATION, function (job, done) {
            if (LOG) common.log(`ActionsManager[${constants.MOBILE_ACTIONS.JOB_TYPE_PLAN_MODIFICATION}]`, JSON.stringify(job.data));
            const params = job.data ? job.data.params : undefined;
            const actionId = params ? params.actionId : undefined;
        
            new Promise(function (resolve, reject) {
                updateJobProgressState(actionId, 'PROCESSING');
        
                const timeout = setTimeout(function () {
                    common.error(`ActionsManager[${constants.MOBILE_ACTIONS.JOB_TYPE_PLAN_MODIFICATION}]`,
                        `timeout, ${JSON.stringify(job.data)}`);
                    reject(new Error('Timeout'));
                }, 5 * 60 * 1000);
        
                addons.extraUpdateBatchSetInternal(params, (err, result) => {
                    clearTimeout(timeout);
                    if (err) {
                        updateJobProgressErrorStatus(actionId, err);
                        reject(err);
                    } else {
                        updateJobProgressStatusAndState(actionId, 'DONE', '', result);
                        resolve(result);
                    }
                });
            }).then(function (result) {
                done(undefined, result);
            }).catch(function (err) {
                done(err);
            });
        });
    }

    static createNewUniqueJob(sin, jobType, actionType, params, options, callback) {
        const lockKey = `UNIQUE_JOB_CREATION_${sin}`;
        const actionId = `JOB_PROGRESS_ID_${actionType}_${sin}`;

        db.lockOperation(lockKey, {}, 5 * 60 * 1000, function (err) {
            if (err) {
                common.error('ActionsQueueService', `createNewUniqueJob: failed for start operation, err=${err.message}`);
                return callback(new BaseError('Job creation is locked', 'ERROR_JOB_CREATION_LOCKED'));
            }

            db.cache.get('cache', actionId, (info) => {
                if (info && info.status == 'IN_PROGRESS') {
                    common.error('ActionsQueueService', `createNewUniqueJob: action in progress, progressId=${actionId}`);
                    db.unlockOperation(lockKey);
                    return callback(new BaseError('Job creation is locked', 'ERROR_OTHER_JOB_IS_ACTIVE'));
                }

                db.cache.put('cache', actionId, {
                    actionId: actionId,
                    status: 'IN_PROGRESS',
                    state: 'QUEUING',
                    jobType: jobType,
                    params: params
                }, 5 * 60 * 1000);

                params.actionId = actionId;
                run(jobType, params, options, (err, jobId) => {

                    db.unlockOperation(lockKey);
                    callback(undefined, {actionId: actionId, jobId: jobId});
                });
            });
        });
    }

    static loadJobProgress(actionId, callback) {
        db.cache.get('cache', actionId, (info) => {
            if (!info) {
                return callback(undefined, {});
            }

            return callback(undefined, info);
        });
    }

    static clearJobProgress(actionId) {
        db.cache.expire('cache', actionId, 0);
    }

}

function updateJobProgressState(actionId, state) {
    if (!actionId) return;

    db.cache.get('cache', actionId, (info) => {
        if (!info) {
            common.error('ActionsQueueService', `createNewUniqueJob: job info is not found, actionId=${actionId}`);
        }

        info.state = state;
        db.cache.put('cache', actionId, info, 5 * 60 * 1000);
    });
}

function updateJobProgressErrorStatus(actionId, err) {
    if (!actionId || !err) return;

    db.cache.get('cache', actionId, (info) => {
        if (!info) {
            common.error('ActionsQueueService', `updateJobProgressErrorStatus: job info is not found, actionId=${actionId}`);
        }

        info.status = err.status ? err.status : 'ERROR', '';
        info.state = '';
        if (err) info.result = resourceManager.getErrorValues(err);
        db.cache.put('cache', actionId, info, 30 * 60 * 1000);
    });
}

function updateJobProgressStatusAndState(actionId, status, state, result) {
    if (!actionId) return;

    db.cache.get('cache', actionId, (info) => {
        if (!info) {
            common.error('ActionsQueueService', `updateJobProgressStatusAndState: job info is not found, actionId=${actionId}`);
        }

        info.status = status;
        info.state = state;
        if (result) info.result = {code: 0, result: result};
        db.cache.put('cache', actionId, info, 30 * 60 * 1000);
    });
}

function run(jobType, params, options, callback) {
    const job = db.queue
        .create(jobType, {
            params: params
        })
        .attempts(1)
        .delay(options.delay > 0 ? options.delay : 0)
        .removeOnComplete(true)
        .events(false)
        .save((err) => {
            if (LOG) common.log('ActionsQueueService', `run: new job created, job id=${job.id}`);
            if (callback) callback(err, job.id);
        });
}

module.exports = MobileActions;
