const db = require(`${global.__lib}/db_handler`);
const common = require(`${global.__lib}/common`);
const config = require(`${global.__base}/config`);
const paymentManager = require(`${global.__manager}/account/paymentManager`);
const logger = require('./logger');

const LOG = config.LOGSENABLED;

class Payments {

    static init() {
        db.queue.process('invoicePayment', 4, function (job, done) {
            const startTime = new Date();
            if (LOG) common.log('QueuePaymentManager[invoicePayment]', `process, ${JSON.stringify(job.data)}`);
        
            const info = common.safeParse(job.data);
            const billingAccountNumber = info.billingAccountNumber;
            const invoicesInfo = info.invoicesInfo;
            const options = info.options;
        
            new Promise((resolve, reject) => {
                const timeout = setTimeout(function () {
                    common.error('QueuePaymentManager[invoicePayment]', `timeout, ${JSON.stringify(job.data)}`);
                    reject(new Error(`Invoice payment operation for ${billingAccountNumber} timed out`));
                }, 150 * 1000);
    
                paymentManager.makeBillPaymentByBA(billingAccountNumber, invoicesInfo, options, (err, result) => {
                    clearTimeout(timeout);
                    const queryTime = new Date() - startTime;
                    logger.saveLogs('invoicePayment', err, job.data, result, queryTime, () => {
                        if (err && err.status == 'ERROR_OPERATION_LOCKED') {
                            return resolve({operationInProgress: true});
                        } else if (err && (err.status == 'ERROR_INVOICES_NOT_SYNCED'
                            || err.status == 'ERROR_INVOICE_SYNC_FAILED'
                            || err.status == 'ERROR_INVOICE_NOT_FOUND'
                            || err.status == 'ERROR_BAN_PAYMENTS_BLOCKED'
                            || err.status == 'ERROR_PAYMENTS_BLOCKED_NOT_ALL_DELIVERED'
                            || err.status == 'ERROR_ALL_INVOICES_PAYMENTS_DISABLED'
                            || err.status == 'ERROR_ALL_PAYMENTS_DISABLED'
                            || err.status == 'ERROR_OPEN_INVOICE_NOT_FOUND'
                            || err.status == 'ERROR_INVALID_PAYMENT_AMOUNT'
                            || err.status == 'ERROR_UNFINISHED_TRANSACTION')) {
                            return resolve({supportedErrorStatus: true});
                        }
    
                        if (err) reject(err);
                        else resolve(result);
                    });
                });
            }).then(function (result) {
                done(undefined, result);
            }).catch(function (err) {
                done(err);
            });
        });
    }

}

module.exports = Payments;
