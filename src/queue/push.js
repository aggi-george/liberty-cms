const db = require(`${global.__lib}/db_handler`);
const common = require(`${global.__lib}/common`);
const config = require(`${global.__base}/config`);
const push = require(`${global.__notifications}/push`);
const logger = require('./logger');

const LOG = config.LOGSENABLED;

class Push {

    static init() {
        db.queue.process('push', 7, (job, done) => {
            const startTime = new Date();
            if (LOG) common.log('QueueManager[push]', `process push, sin=${job.data.serviceInstanceNumber},
                number=${job.data.number}, activity=${job.data.activity}`);
        
            new Promise((resolve, reject) => {
                const timeout = setTimeout(() => {
                    common.error('QueueManager[push]', `timeout sending push, sin=${job.data.serviceInstanceNumber},
                        number=${job.data.number}, activity=${job.data.activity}`);
                    reject(new Error('Timeout'));
                }, 90 * 1000);

                send(job.data, (err, result) => {
                    if (LOG) common.log('QueueManager[push]', `processed push, sin=${job.data.serviceInstanceNumber},
                        number=${job.data.number}, activity=${job.data.activity}`);
        
                    clearTimeout(timeout);
                    const queryTime = new Date() - startTime;
                    logger.saveLogs('push', err, job.data, result, queryTime, () => {
                        if (err) reject(err);
                        else resolve(result);
                    });
                });
            }).then((result) => {
                done(undefined, result);
            }).catch((err) => {
                common.error('QueueManager[push]', `cache block, sending push, error=${err.message}`);
                done(err);
            });
        });
    }
}


function send(jobData, callback) {
    const data = common.safeParse(jobData);
    let msg = common.safeParse(data.message);
    if(data.serviceInstanceNumber){
        msg.serviceInstanceNumber = data.serviceInstanceNumber;
    }

    if (LOG) {
        let info = JSON.stringify(msg);
        if (info.length > 300) info = `${info.substring(0, 300)}.....`;
        common.log('NotificationManager', `sendPush: msg=${info}`);
    }

    getUserUnreadNotificationCount(data.number, data.appType, (badge) => {
        // we have to increase amount of unread notification
        // because the current one is not yet saved into db and not calculated
        badge = badge ? (badge + 1) : 1;

        // notification API has limit - shows max 50 messages
        if (badge > 49) {
            badge = 49;
        }

        if (LOG) common.log('NotificationManager', `unread notifications badge=${badge}`);
        if (!config.SHOW_NOTIFICATION_BADGE) {
            badge = 0;
        }

        push.sendByNumber(data.prefix, data.number, data.appType, badge, msg, callback);
    });
}

function getUserUnreadNotificationCount(number, transport, callback) {
    db.notifications_logbook_count_get({
        number: number,
        transport: transport,
        app: {
            '$exists': true
        },
        $or: [
            {read: false},
            {read: {'$exists': false}}
        ]
    }, (countUnread) => {
        if (callback) callback(countUnread);
    });
}

module.exports = Push;
