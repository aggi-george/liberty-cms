const db = require(`${global.__lib}/db_handler`);
const common = require(`${global.__lib}/common`);
const config = require(`${global.__base}/config`);
const smsq = require(`${global.__notifications}/queues/smsq`);
const mailq = require(`${global.__notifications}/queues/mailq`);
const pushq = require(`${global.__notifications}/queues/pushq`);

const LOG = config.LOGSENABLED;

class Slow {

    static init() {
        db.queue.process('slow', (job, done) => {
            if (LOG) common.log('QueueManager[slow]', `slow notification, sin=${job.data.serviceInstanceNumber},
                number=${job.data.length}, activity=${job.data.activity}, delay=${job.data.delay}`);
            const delay = job.data.delay ? parseInt(job.data.delay) : 5000;
            setTimeout(() => {
                if (!job.data || !job.data.items || !job.data.items.length) {
                    return done(new Error('No data to work with'));
                }
        
                let jobsFailed = [];
                let jobsAdded = [];
                const expectedLength = job.data.items.length;
        
                job.data.items.forEach((item) => {
        
                    const sendResponse = (err, jobId) => {
                        if (err) {
                            if (LOG) common.error('QueueManager[slow]', `failed to add delayed ${item.notificationType},
                                error=${err.message}, sin=${job.data.serviceInstanceNumber}, number=${job.data.number}, activity=${job.data.activity}`);
        
                            jobsFailed.push({
                                type: item.notificationType,
                                error: err.message,
                                item: item
                            });
                        } else {
                            if (LOG) common.log('QueueManager[slow]', `delayed ${item.notificationType} added to main queue,
                                sin=${job.data.serviceInstanceNumber}, number=${job.data.number}, activity=${job.data.activity}, jobId=${jobId}`);
        
                            jobsAdded.push({
                                type: item.notificationType,
                                jobId: jobId,
                                item: item
                            });
                        }
        
                        if ((jobsAdded.length + jobsFailed.length) == expectedLength) {
        
                            // consider operation successful in case
                            // if at least one task has been processed properly
                            // or else it may cause cyclical notifications when
                            // one part is resent > 1 times and another fails all the time
        
                            if (jobsAdded.length > 0) {
                                done(undefined, {success: jobsAdded, failed: jobsFailed});
                            } else {
                                done(new Error('Neither of tasks were processed'));
                            }
                        }
                    };
        
                    if (item.notificationType === 'push') {
                        pushq.send(item, sendResponse);
                    } else if (item.notificationType === 'email') {
                        mailq.send(item, sendResponse);
                    } else if (item.notificationType === 'sms') {
                        smsq.send(item, sendResponse);
                    } else {
                        sendResponse(new Error(`Unsupported Type ${item.notificationType}`));
                    }
                });
        
            }, delay);
        });
    }

}

module.exports = Slow;
