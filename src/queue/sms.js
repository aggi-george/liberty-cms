const nexmo = require('easynexmo');
const sdp = require(`${global.__notifications}/sdp`);
const db = require(`${global.__lib}/db_handler`);
const common = require(`${global.__lib}/common`);
const config = require(`${global.__base}/config`);
const logger = require('./logger');

const LOG = config.LOGSENABLED;

nexmo.initialize('a0f1aeff', '6acb8194', 'https', false);

class Sms {

    static init() {
        db.queue.process('sms', 5, (job, done) => {
            const startTime = new Date();
            if (LOG) common.log('QueueManager[sms]', `process, ${JSON.stringify(job.data)}`);
    
            new Promise((resolve, reject) => {
                const timeout = setTimeout(() => {
                    common.error('QueueManager[sms]', `timeout, ${JSON.stringify(job.data)}`);
                    reject(new Error('Timeout'));
                }, 30 * 1000);
    
                send(job.data, (err, result) => {
                    clearTimeout(timeout);
                    const queryTime = new Date() - startTime;
                    logger.saveLogs('sms', err, job.data, result, queryTime, () => {
                        if (err) reject(err);
                        else resolve(result);
                    });
                });
            }).then((result) => {
                done(undefined, result);
            }).catch((err) => {
                done(err);
            });
        });
    }

}

function send(data, callback) {
    let sms = common.safeParse(data);
    if (!sms.prefix) sms.prefix = '65';

    const prefix = sms.prefix;
    const number = sms.to;
    let from = sms.from;
    const msg = sms.plain;
    const carrier = sms.carrier;
    const donotfailover = carrier ? true : false;

    const sendViaNexmo = (sdpErr, sdpRes) => {
        if (prefix == '1') {
            from = '14086505299'; // override for US
        } else if (prefix == '91') {
            from = 'Circle'; // override for India
        }

        const sdpResult = sdpErr || sdpRes ? {error: sdpErr ? sdpErr.message : undefined, result: sdpRes} : undefined;
        nexmo.sendTextMessage(from, `${prefix}${number}`, msg, {'status-report-req': 1}, (err, result) => {
            if (err) {
                common.error('QueueAction[sms]', `nexmo, sms is NOT sent, to=${sms.to}, error=${err.message}`);
                if (callback) callback(err, {nexmo: result, sdp: sdpResult});
                return;
            }

            if (!result || !result.messages || !result.messages[0]) {
                common.error('QueueAction[sms]', `nexmo, sms is NOT sent, to=$sms.to}, error=Invalid content, response=${JSON.stringify(result)}`);
                if (callback) callback(new Error('Invalid nexmo content'), {nexmo: result, sdp: sdpResult});
                return;
            }

            if (result.messages[0].status !== '0') {
                common.error('QueueAction[sms]', `nexmo, sms is NOT sent, to=${sms.to}, error=Invalid status ${result.messages[0].status}, response=${JSON.stringify(result)}`);
                if (callback) callback(new Error(`Invalid status ${result.messages[0].status}`), { nexmo: result, sdp: sdpResult });
                return;
            }

            if (LOG) common.log('QueueAction[sms]', `nexmo, SMS is sent, to=${sms.to}`);
            if (callback) callback(undefined, {nexmo: result, sdp: sdpResult});
        });
    };

    const sendViaSdp = () => {
        sdp.send(from, number, msg, (err, result) => {
            const failover = donotfailover ? undefined : sendViaNexmo;

            if (err) {
                common.error('QueueAction[sms]', `sdp, sms is NOT sent, to=${sms.to}, error=${err.message}`);
                if (failover) failover(err);
                else if (callback) callback(err);
                return;
            }

            if (!result) {
                common.error('QueueAction[sms]', `sdp, sms is NOT sent, to=${sms.to}, error=Empty response, response=${JSON.stringify(result)}`);
                if (failover) failover(new Error('Empty response'), result);
                else if (callback) callback(new Error('Empty response'), result);
                return;
            }

            if (result.StatusDetail !== 'Success') {
                common.error('QueueAction[sms]', `sdp, sms is NOT sent, to=${sms.to}, error=Invalid status ${result.StatusDetail}, response=${JSON.stringify(result)}`);
                if (failover) failover(new Error(`Invalid status ${result.StatusDetail}`), result);
                else if (callback) callback(new Error(`Invalid status ${result.StatusDetail}`), result);
                return;
            }

            if (LOG) common.log('QueueAction[sms]', `sdp, SMS is sent, to=${sms.to}`);
            if (callback) callback(undefined, {sdp: result});
        });
    };

    if (carrier == 'sdp') {
        sendViaSdp();
    } else if (carrier == 'nexmo') {
        sendViaNexmo();
    } else if (prefix === '65') {
        sendViaSdp();
    } else {
        sendViaNexmo();
    }
}

module.exports = Sms;
