const common = require(`${global.__lib}/common`);
const db = require(`${global.__lib}/db_handler`);
const config = require(`${global.__base}/config`);
const validation = require(`${global.__manager}/account/validation`);
const logger = require('./logger');

const LOG = config.LOGSENABLED;

class StatusValidation {

    static init() {
        db.queue.process('statusValidation', 7, (job, done) => {
            const startTime = new Date();
            if (LOG) common.log('QueueManager[statusValidation]', `process, ${JSON.stringify(job.data)}`);

            const info = common.safeParse(job.data);
            const billingAccountNumber = info.billingAccountNumber;
            const options = info.options;

            new Promise((resolve, reject) => {
                const timeout = setTimeout(() => {
                    common.error('QueueManager[statusValidation]', `timeout, ${JSON.stringify(job.data)}`);
                    reject(new Error(`Invoice payment operation for ${billingAccountNumber} timed out`));
                }, 60 * 1000);

                validation.validateStatusByBA(billingAccountNumber, options, (err, result) => {
                    clearTimeout(timeout);
                    const queryTime = new Date() - startTime;
                    if (options && options.paymentLogId) {
                        db.notifications_pay_now.update({_id: db.objectID(options.paymentLogId)}, {
                            '$set': {
                                'result.statusValidation': {
                                    status: err ? (err.status ? err.status : 'ERROR') : 'OK',
                                    error: err ? err.message : undefined,
                                    result: result,
                                    validatedAt: new Date().toISOString(),
                                    ts: new Date().getTime()
                                }
                            }
                        }, {multi: true}, (err) => {
                            if (err) {
                                common.error('QueueManager[statusValidation]', `failed to update payment log, error=${err.message}`);
                            }
                        });
                    }

                    logger.saveLogs('invoicePayment', err, job.data, result, queryTime, () => {
                        if (err && err.status == 'ERROR_OPERATION_LOCKED') {
                            return resolve({operationInProgress: true});
                        }else if (err && (err.status == 'ERROR_CUSTOMER_IS_TERMINATED'
                            || err.status == 'ERROR_OPERATION_NOT_SUPPORTED')) {
                            return resolve({supportedErrorStatus: true});
                        }

                        if (err) reject(err);
                        else resolve(result);
                    });
                });
            }).then((result) => {
                done(undefined, result);
            }).catch((err) => {
                done(err);
            });
        });
    }

}

module.exports = StatusValidation;
