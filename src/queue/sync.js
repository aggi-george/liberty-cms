const config = require(`${global.__base}/config`);
const common = require(`${global.__lib}/common`);
const db = require(`${global.__lib}/db_handler`);
const invoiceManager = require(`${global.__manager}/account/invoiceManager`);
const invoiceNotificationManager = require(`${global.__manager}/account/invoiceNotificationManager`);
const classManager = require(`${global.__manager}/analytics/classManager`);
const classNotificationManager = require(`${global.__manager}/analytics/classNotificationManager`);
const BaseError = require(`${global.__core}/errors/baseError`);

const LOG = config.LOGSENABLED;
const jobType = 'INTERNAL_SYNC_OPERATION';
const timeoutDelay = 2 * 60 * 60 * 1000;

class Sync {

    static init() {
        // in case of restart all active tasks will be removed not to block the queue
        // some tasks will not be started if any active tasks of the same data type is presented in the queue
        db.queue.active(function (err, ids) {
            ids.forEach(function (id) {
                db.queueJobs.get(id, function (err, job) {
                    if (!err && job && job.type == 'INTERNAL_SYNC_OPERATION') {
                        job.remove();
                    }
                });
            });
            enableQueue();
        });
    }

    /**
     *
     * @param type
     * @param params
     * @param options
     * @param callback
     */
    static run(type, params, options, callback) {

        const createJob = () => {
            const job = db.queue
                .create(jobType, {
                    type: type,
                    params: params,
                    initiator: options.initiator
                })
                .attempts(1)
                .removeOnComplete(true)
                .events(false)
                .save((err) => {
                    if (LOG) common.log('SyncQueueService', `run: new job created, job id=${job.id}`);
                    if (callback) callback(err, job.id);
                });
        };

        if (options.errorIfRunning) {
            isInQueue(jobType, type, (err, result) => {
                if (err) {
                    return callback(err);
                }
                if (!result) {
                    return callback(new Error('Invalid queue status'));
                }
                if (result.running) {
                    return callback(new Error(`Task ${type} is already running, can not start new one`));
                }

                createJob();
            });
        } else {
            createJob();
        }
    }

    /**
     *
     * @param type
     * @param callback
     */
    static isRunning(type, callback) {
        isInQueue(jobType, type, callback);
    }

}

function enableQueue() {
    db.queue.process('INTERNAL_SYNC_OPERATION', function (job, done) {
        if (LOG) common.log('SyncApplication[INTERNAL_SYNC_OPERATION]', `start operation: job id=${job.id}, data=${JSON.stringify(job.data)}`);

        const startTime = new Date();
        new Promise((resolve, reject) => {
            if (!job.data) {
                return reject(new BaseError('Invalid params: empty data', 'ERROR_INVALID_PARAMS'));
            }
            if (!job.data.type) {
                return reject(new BaseError('Invalid params: empty type', 'ERROR_INVALID_PARAMS'));
            }
            if (!job.data.params) {
                job.data.params = {};
            }

            let timeout;
            const handleResult = (err, result) => {
                if (err) {
                    common.error('SyncApplication[INTERNAL_SYNC_OPERATION]',
                        `handleResult: job failed, error=${err.message}`);
                } else {
                    if (LOG) common.log('SyncApplication[INTERNAL_SYNC_OPERATION]',
                        `handleResult: finished job, job id=${job.id}`);
                }

                if (timeout) clearTimeout(timeout);
                const time = new Date().getTime() - startTime;
                saveLogs(err, job.data.type, job.data.initiator, job.data.params, result, time, () => {

                    // ignore errLog: we can not do anything if log is not saved
                    // ignore err: we don't need to re-run it again if operation failed,
                    // it has to be re-run manually

                    resolve(result);
                });
            };

            // let any operation run up-to 2 hours
            // do not terminate before that

            timeout = setTimeout(function () {
                handleResult(new BaseError(`Operation timeout ${timeoutDelay}`, 'ERROR_OPERATION_TIMEOUT'));
            }, timeoutDelay);

            if (job.data.type == 'SYNC_INVOICES') {
                invoiceManager.syncBills(job.data.params.monthDate ? new Date(job.data.params.monthDate) : undefined,
                    job.data.params.options, handleResult);
            } else if (job.data.type == 'SEND_ALL_PDF_FILES') {
                invoiceNotificationManager.sendAllBills(job.data.params.monthId, job.data.params.options, handleResult);
            } else if (job.data.type == 'SEND_NON_PAYMENT_REMINDERS') {
                invoiceNotificationManager.sendNonPaymentReminder(job.data.params.type, job.data.params.options, handleResult);
            } else if (job.data.type == 'SYNC_CUSTOMER_CLASSES') {
                classManager.classifyCustomers(handleResult);
            } else if (job.data.type == 'SEND_NOTIFICATION_TO_CLASS') {
                classNotificationManager.sendNotifications(job.data.params.classId, job.data.params.activity,
                    job.data.params.options, handleResult);
            } else {
                handleResult(new BaseError(`Operation type ${job.data.type} is not supported`, 'ERROR_INVALID_TYPE'));
            }
        }).then(function (result) {
            done(undefined, result);
        }).catch(function (err) {
            common.error('SyncApplication[INTERNAL_SYNC_OPERATION]', `promise failed, job id=${job.id} error=${err.message}`);
            common.error('SyncApplication[INTERNAL_SYNC_OPERATION]', err.stack);
            done(err);
        });
    });
}

function isInQueue(jobType, dataType, callback) {
    if (!callback) callback = () => {};

    if (!jobType || !dataType) {
        callback(new Error('Invalid params'));
    }

    const queueStates = ['delayed', 'inactive', 'active'];

    let foundCount = 0;
    let queueStatesCountTotal = 0;
    let queueStatesCountProcessed = 0;
    let queueJobsCountTotal = 0;
    let queueJobsCountProcessed = 0;

    // in order to speed the search up no async is used

    queueStatesCountTotal = queueStates.length;
    if (queueStatesCountTotal == 0) {
        return callback(undefined, {running: 0, count: 0});
    }

    queueStates.forEach((state) => {
        db.queue[state]((err, ids) => {

            queueJobsCountTotal += ids.length;
            queueStatesCountProcessed++;

            if (ids.length == 0) {
                if (queueStatesCountTotal == queueStatesCountProcessed
                    && queueJobsCountTotal == queueJobsCountProcessed) {
                    return callback(undefined, {running: foundCount > 0, count: foundCount});
                }
            } else {
                ids.forEach((id) => {
                    db.queueJobs.get(id, (err, job) => {
                        queueJobsCountProcessed++;
                        if (!err && job && job.data && job.type == jobType && job.data.type == dataType) {
                            foundCount++;
                        }

                        if (queueStatesCountTotal == queueStatesCountProcessed
                            && queueJobsCountTotal == queueJobsCountProcessed) {
                            callback(undefined, {running: foundCount > 0, count: foundCount});
                        }
                    });
                });
            }
        });
    });
}

function saveLogs(err, type, initiator, params, result, queryTime, callback) {
    const obj = {
        type: type,
        status: (err ? (err.status ? err.status : 'ERROR') : 'OK'),
        errorMessage: (err ? err.message : undefined),
        initiator: initiator ? initiator : 'UNKNOWN',
        params: params,
        result: result,
        fqdn: config.MYFQDN,
        queryTime: queryTime,
        ts: new Date().getTime()
    };

    db.notifications_sync.insert(obj, (errLog) => {
        if (errLog) common.error(`SyncApplication[${type}]`, `failed to save logs, error=${errLog.message}`);
        if (callback) callback();
    });
}

module.exports = Sync;
