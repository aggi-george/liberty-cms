const moment = require('moment');
const OFFSET = {
    'SG': 8 * 60 * 60 * 1000
};

class DateTime {
    static resetDateTimeZeroISO(date) {
        const m = moment(date).utcOffset(0);
        m.set({ hour: 0, minute: 0, second: 0, millisecond: 0 });
        m.toISOString();
        m.format();

        return m;
    }

    static getMonthString(date, ISOCode) {
        let offset = OFFSET[ISOCode];
        if (!offset) offset = 0;
        if (!date) return moment(new Date().getTime() +  offset).format('MMMM');
        else return moment(new Date(date).getTime() +  offset).format('MMMM');
    }
    static msOffsetSG (d, ISOCode) {
        let offset = OFFSET[ISOCode];
        if (!offset) offset = 0;
        if (!d) return new Date().getTime() + offset;
        else return new Date(d).getTime() + offset;
    }

    static getSGTime (d, ISOCode) {
        let offset = OFFSET[ISOCode];
        if (!offset) offset = 0;
        if (!d) return new Date(new Date().getTime() + offset);
        else return new Date(new Date(d).getTime() + offset);
    }

    static generateDate(dateObj) {
        const { date, hour, minute, second, millisecond } = dateObj;
        const m = moment(date).utcOffset(0);
        m.set({ hour, minute, second, millisecond });

        return m.toISOString();
    }

    static addMonthToDate(date, noOfMonths) {
        return moment(date).add(noOfMonths, 'month').toISOString();
    }

    static getLastDayOfMonth(date) {
        return moment(date).endOf('month').toISOString();
    }

    static getDaysDiff(start, end) {
        const startDate = moment(start);
        const endDate = moment(end);

        return endDate.diff(startDate, 'days');
    }

    static getCurrentDate() {
        return moment(new Date()).toISOString();
    }

    /**
     * To format date
     * @param date - This can be a date object or any string that moment can convert into date.
     * @param format -
     * 	Token	Output
     Month
     M	1 2 ... 11 12
     Mo	1st 2nd ... 11th 12th
     MM	01 02 ... 11 12
     MMM	Jan Feb ... Nov Dec
     MMMM	January February ... November December

     Quarter
     Q	1 2 3 4
     Qo	1st 2nd 3rd 4th

     Day of Month
     D	1 2 ... 30 31
     Do	1st 2nd ... 30th 31st
     DD	01 02 ... 30 31
     Day of Year	DDD	1 2 ... 364 365
     DDDo	1st 2nd ... 364th 365th
     DDDD	001 002 ... 364 365

     Day of Week
     d	0 1 ... 5 6
     do	0th 1st ... 5th 6th
     dd	Su Mo ... Fr Sa
     ddd	Sun Mon ... Fri Sat
     dddd	Sunday Monday ... Friday Saturday
     Day of Week (Locale)	e	0 1 ... 5 6
     Day of Week (ISO)	E	1 2 ... 6 7

     Week of Year
     w	1 2 ... 52 53
     wo	1st 2nd ... 52nd 53rd
     ww	01 02 ... 52 53
     Week of Year (ISO)	W	1 2 ... 52 53
     Wo	1st 2nd ... 52nd 53rd
     WW	01 02 ... 52 53

     Year
     YY	70 71 ... 29 30
     YYYY	1970 1971 ... 2029 2030
     Y	1970 1971 ... 9999 +10000 +10001  Note: This complies with the ISO 8601 standard for dates past the year 9999

     Week Year
     gg	70 71 ... 29 30
     gggg	1970 1971 ... 2029 2030
     Week Year (ISO)	GG	70 71 ... 29 30
     GGGG	1970 1971 ... 2029 2030

     AM/PM
     A	AM PM
     a	am pm

     Hour
     H	0 1 ... 22 23
     HH	00 01 ... 22 23
     h	1 2 ... 11 12
     hh	01 02 ... 11 12
     k	1 2 ... 23 24
     kk	01 02 ... 23 24

     Minute
     m	0 1 ... 58 59
     mm	00 01 ... 58 59

     Second
     s	0 1 ... 58 59
     ss	00 01 ... 58 59

     Fractional Second
     S	0 1 ... 8 9
     SS	00 01 ... 98 99
     SSS	000 001 ... 998 999
     SSSS ... SSSSSSSSS	000[0..] 001[0..] ... 998[0..] 999[0..]

     Time Zone
     z or zz	EST CST ... MST PST     Note: as of 1.6.0, the z/zz format tokens have been deprecated from plain moment objects. Read more about it here. However, they *do* work if you are using a specific time zone with the moment-timezone addon.
     Z	-07:00 -06:00 ... +06:00 +07:00
     ZZ	-0700 -0600 ... +0600 +0700

     Unix Timestamp
     X	1360013296

     Unix Millisecond Timestamp
     x	1360013296123

     * @returns {string}
     */
    static format(date, formatString){
        return moment(date).format(formatString);
    }
    /**
     * To get the month cycle information
     * @referenceDate - the date for reference - ISO String format : YYYY-MM-DDTHH:mm:ss.SSSSZ, ex : 2018-01-09T06:50:33.660Z
     * @cycleStartsOn - the day the month cycle starts in a month - ex : 10
     * @ISOCode - optional - OFFSET
     * @formatString - optional - all the formats that moment supports- refer format method above
     * returns json with startDate and endDate
     */
    static getMonthCycle(referenceDate, cycleStartsOn, ISOCode, formatString) {
        let startDate = moment.utc(referenceDate);

        if (!startDate.isValid()) return undefined;
        let currentDate = startDate.format('D'); // get the date of the month.

        //check if the cycles start this month or previous month.
        let cycleStartsThisMonth = cycleStartsOn <= currentDate;

        //reset the cycle startdate
        startDate.set({
            hour: 0,
            minute: 0,
            second: 0,
            milliseconds: 0,
            date: cycleStartsOn
        });

        //may be we need to navigate a month back
        if (!cycleStartsThisMonth) startDate.subtract(1, 'months');

        //now enddate is exactly the clone of start date
        let endDate = startDate.clone();

        //but a millisecond less
        endDate.add(1, 'months').subtract(1, 'milliseconds');

        //If there is an ISOCode provide we must add offset to the ISO String.
        if(ISOCode){
            let offsetMillis = OFFSET[ISOCode];
            if(!isNaN(offsetMillis)){
                startDate.subtract(offsetMillis, 'milliseconds');
                endDate.subtract(offsetMillis, 'milliseconds');
            }
        }

        //if there is a format specified
        if(formatString){
            startDate = format(startDate, formatString);
            endDate = format(endDate, formatString);
        }else{
            startDate = startDate.toISOString();
            endDate = endDate.toISOString();
        }

        return {startDate,endDate};
    }

}

module.exports = DateTime;