var async = require('async');
var db = require('../lib/db_handler');
var ec = require('../lib/elitecore');
var common = require('../lib/common');
var config = require('../config');
var usageManager = require('../src/core/manager/account/usageManager');
var bonusManager=require('../src/core/manager/bonus/bonusManager');
var bonusAggregate = require('../tests/bonus_aggregate');
var TESTERS = require('./testers.js').testers;

var LOG = config.LOGSENABLED;
exports.run = run;

function run(callback) {
    var week = common.getDate(new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0)) == common.getDate(new Date) ?
        false : true;
    var distinctOptions = { "readPreference" : "NEAREST", "maxTimeMS" : 120000 };
    var getEx = function (cbEx) {   //breakup because this query can get very heavy
        var findEx = { "serviceInstanceNumber" : { "$exists" : true, "$ne" : null },
                       "activity" : { "$in" : [ "termination_effective", "portoutsuccess" ] } };
        db.notifications_webhook.distinct("serviceInstanceNumber", findEx, distinctOptions, function (errEx,resEx) {
            cbEx(errEx, resEx);
        });
    };
    var getIn = function (cbIn) {
        var findIn = { "serviceInstanceNumber" : { "$exists" : true, "$ne" : null },
                       "activity" : { "$in" : [
                           "delivery_success",
                           "add_on_unsubscription",
                           "add_on_subscription",
                           "plus_added_immediate",
                           "plus_removed_immediate",
                           "bonus_birthday_added",
                           "bonus_new_year_added",
                           "bonus_loyalty_added",
                           "bonus_referral_added",
                           "bonus_selfcare_install_added",
                           "selfcare_install" ] } };
        if (week) findIn.ts = { "$gt" : (new Date().getTime() - (7 * 24 * 60 * 60 * 1000)) }; // check only from 1 week onwards
        db.notifications_webhook.distinct("serviceInstanceNumber", findIn, distinctOptions, function (errIn,resIn) {
            cbIn(errIn, resIn);
        });
    };
    var getCombine = function (cbCombine) {
        var startEx = new Date();
        getEx(function(errEx, resEx) {
            if (errEx) return cbCombine(errEx);
            else if (resEx.length == 0) return cbCombine(new Error("Empty Ex List"));
            else common.log("getEx length " + resEx.length + "     query time : " + (new Date() - startEx));
            var startIn = new Date();
            getIn(function(errIn, resIn) {
                if (errIn) return cbCombine(errIn);
                else if (resIn.length == 0) return cbCombine(new Error("Empty In List"));
                else common.log("getIn length " + resIn.length + "     query time : " + (new Date() - startIn));
                var startCombine = new Date();
                var resCombine = resIn.filter(function(item) {
                    if (resEx.indexOf(item) > -1) return undefined;
                    else return item;
                });
                common.log("getCombine length " + resCombine.length + "     query time : " + (new Date() - startCombine));
                cbCombine(undefined, resCombine);
            });
        });
    }
    getCombine(function(numberErr, numberList) {
        if (numberErr) return callback(numberErr);
        var qError = new Array;
        var qResult = new Array;
        var q = async.queue(function(item, cb) {
            elitecoreGet(item, function(qe, qr) {
                if (qe) qError.push({ "item" : item, "error" : qe });
                else if (qr) qResult.push(qr);
                cb();
            });
        }, 10);
        numberList.forEach(function (item) {
            q.push(item, function(e) { if (e) qError.push({ "item" : item, "error" : e }) });
        });
        q.drain = function(e) {
            if (qError.length == 0) qError = undefined;
            if (qResult.length == 0) qResult = undefined;
            callback(qError, qResult);
        };
    });
}

function elitecoreGet(serviceInstanceNumber, callback) {
    var fetch = function(cb) {
        ec.getCustomerDetailsServiceInstance(serviceInstanceNumber, false, function (error, cache) {
            if (error && !cache) {
                return callback(undefined, { "errorCache" : error.message,
                    "serviceInstanceNumber" : serviceInstanceNumber });
            } else if (!cache.number) {
                return callback(undefined, { "serviceInstanceNumber" : serviceInstanceNumber });
            } else if ( TESTERS.indexOf(cache.email.toLowerCase()) > -1 ) {
                return callback(undefined, {
                    "serviceInstanceNumber" : serviceInstanceNumber,
                    "number" : cache.number,
                    "tester" : true });
            } else {
                bonusManager.loadHistoryBonusList(cache.prefix, cache.number, true, function (err, items) {
                    var total = 0;
                    var pretty = 0;
                    if (items) items.forEach(function(bonus) {
                        total += bonus.product_kb;
                        pretty += bonus.product_kb_pretty;
                    });
                    cb(err, cache, { "pretty" : pretty, "total" : total});
                });
            }
        });
    }
    fetch(function(error, cache, item) {
        usageManager.computeUsage(cache.prefix, cache.number, function (err, usageResult) {
            var totalBonus =  (usageResult && usageResult.data && usageResult.data.bonus) ?
                    usageResult.data.bonus.left + usageResult.data.bonus.used : undefined;
            callback(undefined, {
                        "errorUsage" : (err ? err.message : undefined ),
                        "serviceInstanceNumber" : serviceInstanceNumber,
                        "number" : cache.number,
                        "status" : cache.status,
                        "history" : item.total,
                        "total" : totalBonus,
                        "pretty" : item.pretty,
                        "historyPretty" : prettifyData(item.pretty),
                        "totalPretty" : prettifyDataKb(totalBonus),
                        "diff" : (!totalBonus && !item.total ? 0 : item.total/totalBonus) });
        });
    });
}

function prettifyData(kb) {
    if (kb >= 1000 * 1000) {
        return (Math.round((kb / 1000 / 1000) * 100) / 100) + " GB";
    } else if (kb >= 1000) {
        return (Math.round((kb / 1000 / 1000) * 100) / 100) + " GB";
//        return (Math.round(kb / 1000)) + " MB";
    } else {
        return "0 MB";
    }
}

function prettifyDataKb(kb) {
    if (Math.round(kb) >= ((1024 * 1024) - (10 * 1024))) {    // 10mb legroom
        return (Math.round((kb / 1024 / 1024) * 100) / 100) + " GB";
    } else if (Math.round(kb) >= 1024) {
        return (Math.round((kb / 1024 / 1024) * 100) / 100) + " GB";
//        return (Math.round(kb / 1024)) + " MB";
    } else {
        return "0 MB";
    }
}
