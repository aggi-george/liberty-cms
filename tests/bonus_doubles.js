var ec = require('../lib/elitecore');
var db = require('../lib/db_handler');
var async = require('async');
var TESTERS = require('./testers.js').testers;

exports.run = run;

function run(callback) {
    var q = "SELECT hb1.service_instance_number, '2 loyalty' anomaly FROM " +
            " historyBonuses hb1 LEFT JOIN historyBonuses hb2 ON " +
                " hb1.service_instance_number=hb2.service_instance_number AND " +
                " hb1.bonus_type=hb2.bonus_type AND " +
                " hb1.added_ts<hb2.added_ts WHERE " +
                " hb1.added_ts>=DATE_ADD(hb2.added_ts, interval -23 WEEK) AND hb1.bonus_type='loyalty' AND hb2.id IS NOT NULL " +
        " UNION SELECT s.service_instance_number, GROUP_CONCAT(s.tot, ' ', s.bonus_type) anomaly FROM " +
            " (SELECT COUNT(*) tot, service_instance_number, bonus_type FROM historyBonuses WHERE " +
                " bonus_type IN ('birthday', 'selfcare_install', 'gentwo_install') GROUP BY bonus_type,service_instance_number " +
            " UNION SELECT COUNT(*) tot, service_instance_number, 'bday_avail' bonus_type FROM availableBonuses WHERE " +
                " bonus_product_id = 'PRD00484' GROUP BY service_instance_number) s " +
        " WHERE s.tot>1 GROUP BY s.service_instance_number";
    db.query_noerr(q, function (rows) {
        var tasks = new Array;
        if (rows && rows.length) rows.forEach(function (item) { pushTask(tasks, item) });
//        pushTask(tasks, { "service_instance_number" : "LW875412", "anomaly" : "3 birthday" });
//        pushTask(tasks, { "service_instance_number" : "LW145540", "anomaly" : "29 birthday" });
        setTimeout(function() {
            async.parallelLimit(tasks, 20, function (err, result) {
                callback(err, result);
            });
        }, 1000);
    });
}

function pushTask(tasks, item) {
    tasks.push(function (callback) {
        ec.getCustomerDetailsServiceInstance(item.service_instance_number, false, function (err, result) {
            if (result) {
                if ( TESTERS.indexOf(result.email.toLowerCase()) > -1 ) result.tester = true;
                result.anomaly = item.anomaly;
            }
            callback(undefined, result);
        }, true);
    });
}

