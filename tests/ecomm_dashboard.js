var async = require('async');
var request = require('request');
var paas = require('../lib/paas');
var db = require('../lib/db_handler');
var ec = require('../lib/elitecore');
var common = require('../lib/common');
var config = require('../config');

var logOK = false;
var errorExists = { "noLink" : [ ], "duplicate" : [ ], "unknown" : [ ], "terminated" : [ ], "missing" : [ ],
			"listNotEqual" : [ ], "countNotEqual" : [ ], "noCount" : [ ] };

exports.run = run;

function elitecoreGet(tasks, number, temp) {
	if (parseInt(number) == number) tasks.push(function (callback) {
		ec.is_circles(number, "65", function (circles, baseplan) {
			if ( circles ) {
				callback(undefined, { "number" : number,
						"baseplan" : baseplan });
			} else {
				callback(undefined, { "number" : number,
							"temp" : temp });
			}
		});
	});
}

function installsReconcile(delivered, callback) {
	var q = "SELECT number FROM app WHERE app_type='selfcare'";
        db.query_noerr(q, function (rows) {
		// assuming 65XXXXXXXX, fix this
		var installs = rows ? rows.map(function (o) {
			if (parseInt(o.number) == o.number) return o.number.substring(2) }) : [];
		var noInstall = delivered.filter(function (o) {
			if (parseInt(o.number) != o.number) {
				return undefined;
			} else if ( (installs.indexOf(o.number) > -1) ||
					(installs.indexOf(o.temporary_number) > -1) ) {
				return undefined;
			} else {
				return o;
			}
		});
		var tasks = new Array;
		noInstall.forEach(function (item) { elitecoreGet(tasks, item.number, item.temporary_number); });
		async.parallelLimit(tasks, 10, function(errNoInst, resultNoInst) {
			tasks.length = 0;
			var terminated = new Array;
			resultNoInst.filter(function (o) { return (o.baseplan) ? undefined : o })
					.forEach(function (item) {
				if (item.temp) elitecoreGet(tasks, item.temp);
				else terminated.push(item.number);
			});
			async.parallelLimit(tasks, 10, function(errTemp, resultTemp) {
				resultTemp.forEach(function (item) {
					if (!item.baseplan) terminated.push(item.number);
				});
				callback(undefined, terminated);
			});
		});
	});
}

function getStats(callback) {
	var path = "/counts.json";
	var params = { "qs" : JSON.parse(JSON.stringify(config.PAAS_CREDS)) };
	paas.connect("get", path, params, function (err, result) {
		if (!err && result) {
//			if (result.code == 0) {
			if (result.stats) {
				callback(undefined, { "stats" : result.stats, "queryTime" : result.query_time });
			} else {
				callback({ "stats" : { }, "error" : result });
			}
		} else {
			callback({ "error" : err, "result" : result });
		}
	});
}

function getLink(link, cb) {
	var params = { "qs" : JSON.parse(JSON.stringify(config.PAAS_CREDS)) };
	var path = "/" + link.split("?")[0];
	var paramStr = link.split("?")[1];
	if ( paramStr && (paramStr != "") ) paramStr.split("&").forEach(function (o) {
		var key = o.split("=")[0];
		var val = o.split("=")[1];
		if ( key != "" ) params.qs[key] = val;
	});
	paas.connect("get", path, params, function (err, result) {
		if (!err && result) {
//			if (result.code == 0) {
			if (result.orders) {
				cb(result.orders);
			} else if (result.customers) {
				cb(result.customers);
			} else {
				cb(undefined);
			}
		} else {
			cb(undefined);
		}
	});
}

function compareList(stats, item) {
	var key = Object.keys(item)[0];
	if (stats[key]) {
		if ( stats[key].count != item[key] ) {
			errorExists.listNotEqual.push(key);
		}
	} else {
		errorExists.noCount.push(item);
	}
}

function compareCount(stats, key, list) {
	var total = list.reduce(function (tot, next) { return tot + stats[next].count; }, 0);
	if ( stats[key].count != total ) {
		errorExists.countNotEqual.push(key);
	}
}

function checkDuplicates(array) {
	var sorted = array.sort();
	var duplicate = "";
	for(var i=1; i<sorted.length; i++) {
		if ( sorted[i] && sorted[i-1] && (sorted[i] == sorted[i-1]) ) {
			duplicate += sorted[i] + ",";
		}
	}
	if ( duplicate != "" ) duplicate = duplicate.slice(0, -1);
	return duplicate.slice(0,-1);
}

function getCountDiscrepancy(stats) {
	var errors = errorExists.count
	errors.forEach(function (err) {
		var combined = err.list.reduce(function (tot, next) {
			if (!tot) return err[next];
			else return tot.concat(err[next]);
		}, []);
		var key = err.stats;
		var a = err[err.stats];
		var b = combined;
		if ( err[err.stats].length > combined.length ) {
			key = "Combined vs. " + err.stats;
			a = combined;
			b = err[err.stats];
		}
		var ref = a.map(function (o) { return o.reference_number; });
		b.forEach(function (item) {
			if (ref.indexOf(item.reference_number) == -1) {
				errorExists.missing.push({ "key" : key, "item" : item });
			}
		});
	});
}

function run(callback) {
	getStats(function (err, response) {
		if (err && !response) {
			return callback(err);
		}
		var stats = response.stats;
	
		compareCount(stats, "all_customers", 
				[ "customers_signed_on", "not_yet_customers_signed_on" ]);
		compareCount(stats, "customers_signed_on",
				[ "not_yet_ordered", "already_ordered" ]);
		compareCount(stats, "all_orders",
				[ "not_yet_paid", "payment_success" ]);
		compareCount(stats, "payment_success",
				[ "pending_approval_orders", "for_reupload", "cancelled", "terminated", "approved" ]);
		compareCount(stats, "approved",
				[ "so_error", "accepted" ]);
		compareCount(stats, "all_port_in",
				[ "porting_not_initiated", "porting_initiated" ]);
		compareCount(stats, "porting_initiated",
				[ "porting_initiation_success", "porting_initiation_fail" ]);
		compareCount(stats, "accepted",
				[ "not_yet_dispatched", "dispatched" ]);
		compareCount(stats, "dispatched",
				[ "not_yet_delivered", "for_redelivery", "delivered" ]);
	
		var tasks = new Array;
		var delivered;	
		Object.keys(stats).forEach(function (key) {
			if (stats[key].link) {
				tasks.push(function (callback) {
					getLink(stats[key].link, function (result) {
						var obj = new Object;
						if (result) {
							if ( key == "delivered" ) {
								delivered = result;
							}
							if (errorExists && errorExists.count) {
								var err = errorExists.count;
								err.forEach(function (item) {
									if ( (item.stats == key) ||
											(item.list.indexOf(key) > -1) ) {
										item[key] = result;
									}
								});
							}
							var noDuplicate = [ "approved", "all_port_in", "porting_initiated", "porting_initiation_success", "accepted", "dispatched", "delivered" ];
							if ( noDuplicate.indexOf(key) > -1 ) {
								var duplicate = checkDuplicates(result.map(function (o) { 
										return o.number;
									}).filter(function (p) {
										if (parseInt(p) == p) return p;
										else return undefined;
									})
								);
								if ( duplicate != "" ) {
									var dup = new Object;
									dup[key] = duplicate;
									errorExists.duplicate.push(dup);
								}
							}
							obj[key] = result.length;
							callback(undefined, obj);
						} else {
							obj[key] = 0;
							errorExists.unknown.push(key);
							callback(undefined, obj);
						}
					});
				});
			} else {
				var ignore = [ "customers_registered_last_week", 
						"orders_created_last_week",
						"orders_created_last_week_payment_success",
						"orders_created_last_week_payment_fail" ];
				if ( ignore.indexOf(key) == -1 ) {
					errorExists.noLink.push(key);
				}
			}
		});
		async.series(tasks, function(err, result) {
			if (result) result.forEach(function (item) {
				if (item) {
					compareList(stats, item);
				}
			});
			if (errorExists && errorExists.count) getCountDiscrepancy(stats);
			installsReconcile(delivered, function (err, terminated) {
				if (err) {
					errorExists.terminated.push(err);
				} else if (terminated.length > 0) {
					errorExists.terminated = terminated;
				}
				callback(errorExists);
			});
		});
	});
}

/*		var combined = list.reduce(function (tot, next) {
			if (tot.length == 0) return stats[next].
			return tot.concat(stats[next].);
		}, []);
*/
