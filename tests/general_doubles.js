var ec = require('../lib/elitecore');
var db = require('../lib/db_handler');
var async = require('async');
var TESTERS = require('./testers.js').testers;

exports.run = run;

function run(callback) {
    var start = new Date().getTime() - (7 * 24 * 60 * 60 * 1000); // check only from 1 week onwards
    var agg_array = [
        { "$match" : { "ts" : { "$gt" : start }, "serviceInstanceNumber" : { "$exists" : true } } },
        { "$project" : { "serviceInstanceNumber" : 1, "activity" : 1 } },
        { "$group" : { "_id" : "$serviceInstanceNumber", "activity" : { "$push" : "$activity" } } },
        { "$match" : { "$and" : [
            { "activity" : { "$eq" : "validate_base_plan" } },
            { "activity" : { "$ne" : "termination_effective" } },
            { "activity" : { "$ne" : "portoutsuccess" } },
            { "$or" : [
                { "activity" : "plus_removed_immediate" },
                { "activity" : "plus_added_immediate" } ] } ] } }
    ];
    db.notifications_webhook.aggregate(agg_array, function(numberErr, numberList) {
        var tasks = new Array;
        numberList.forEach(function (item) {
            elitecoreGet(tasks, item._id);
        });
        async.parallelLimit(tasks, 10, function (err, result) {
            var list = result.filter(function (o) {
                if ( o.errorCache ||
                   ((o.incoming < 2) &&
                   (o.whatsapp < 2) &&
                   (o.cid < 2) &&
                   (o.planchange < 2) &&
                   (o.fourg < 2)) ) return false; else return true;
            });
            callback(err, list);
        });
    });
}

function elitecoreGet(tasks, serviceInstanceNumber) {
    tasks.push(function (callback) {
        ec.getCustomerDetailsServiceInstance(serviceInstanceNumber, false, function (error, cache) {
            if (error && !cache) {
                return callback(undefined, {
                    "errorCache" : error,
                    "serviceInstanceNumber" : serviceInstanceNumber });
            } else if ( TESTERS.indexOf(cache.email.toLowerCase()) > -1 ) {
                return callback(undefined, {
                    "serviceInstanceNumber" : serviceInstanceNumber,
                    "number" : cache.number,
                    "tester" : true });
            }
            var incoming = cache.general.filter(function (o) {
                if (o.id == "PRD00463") return true; else return false; });
            var whatsapp = cache.general.filter(function (o) {
                if (o.id == "PRD00440") return true; else return false; });
            var cid = cache.general.filter(function (o) {
                if (o.id == "PRD00445") return true; else return false; });
            var planchange = cache.general.filter(function (o) {
                if (o.id == "PRD00317") return true; else return false; });
            var fourg = cache.general.filter(function (o) {
                if (o.id == "PRD00316") return true; else return false; });
            callback(undefined, { "incoming" : incoming.length,
                "whatsapp" : whatsapp.length,
                "cid" : cid.length,
                "planchange" : planchange.length,
                "fourg" : fourg.length,
                "serviceInstanceNumber" : serviceInstanceNumber,
                "number" : cache.number });
        });
    });
}

